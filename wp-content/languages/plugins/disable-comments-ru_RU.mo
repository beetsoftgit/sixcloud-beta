��          �            x     y     |     �     �     �  :   �  /      �   0  �   �     �     �     �     �  /   �  $   &  �  K       $     1   3  %   e     �  `   �  y   �  	  w  �  �  %   o
     �
     �
  $   �
  /   �
  -                            
         	                                             ,  Comments are closed. Custom post types: Delete Comments Disable Comments Disable Comments requires WordPress version %s or greater. Internal error occured. Please try again later. Note: The <em>Disable Comments</em> plugin is currently active, and comments are completely disabled on: %s. Many of the settings below will not be applicable for those post types. Only the built-in post types appear above. If you want to disable comments on other custom post types on the entire network, you can supply a comma-separated list of post types below (use the slug that identifies the post type). Save Changes Settings Tools Total Comments: https://wordpress.org/plugins/disable-comments/ settings menu titleDisable Comments PO-Revision-Date: 2020-08-19 07:15:43+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n % 10 == 1 && n % 100 != 11) ? 0 : ((n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 12 || n % 100 > 14)) ? 1 : 2);
X-Generator: GlotPress/3.0.0-alpha.2
Language: ru
Project-Id-Version: Plugins - Disable Comments &#8211; Remove Comments &amp; Protect From Spam - Stable (latest release)
 , Обсуждение закрыто. Произвольные типы записей: Удалить комментарии Disable Comments Для работы плагина требуется WordPress версии %s или выше. Произошла внутренняя ошибка. Пожалуйста, повторите попытку позже. Заметка: Плагин <em>Отключение комментариев</em> активирован и комментарии полностью отключены: %s. Настройки ниже неприменимы к этим типам записей. Только встроенные типы записей отображаются выше. Если вы хотите отключить комментарии для других типов записей во всей сети, укажите список пользовательских типов записей, разделяя их запятыми (используйте ярлык, который идентифицирует пользовательский тип записи). Сохранить изменения Настройки Инструменты Всего комментариев: https://wordpress.org/plugins/disable-comments/ Отключение комментариев 