<?php

if (!defined('ABSPATH'))
    die('Restricted Access');

class JSSTwphdnotification {

    function __construct( ) {

    }

    public function addSessionNotificationDataToTable($message, $msgtype, $sessiondatafor = 'notification',$ticketid = null){
        if($message == ''){
            if(!is_numeric($message))
                return false;
        }
        global $wpdb;
        $data = array();
        $update = false;
        if(isset($_COOKIE['_wpjshd_session_']) && isset(jssupportticket::$_jshdsession->sessionid)){
            if($sessiondatafor == 'notification'){
                $data = $this->getNotificationDatabySessionId($sessiondatafor);
                if(empty($data)){
                    $data['msg'][0] = $message;
                    $data['type'][0] = $msgtype;
                }else{
                    $update = true;
                    $count = count($data['msg']);
                    $data['msg'][$count] = $message;
                    $data['type'][$count] = $msgtype;
                }
            }elseif($sessiondatafor == 'submitform'){
                $data = $this->getNotificationDatabySessionId($sessiondatafor,true);
                $data = $message;
            }elseif($sessiondatafor == 'ticket_time_start_'){
                $data = $this->getNotificationDatabySessionId($sessiondatafor.$ticketid);
                $sessiondatafor = $sessiondatafor.$ticketid;
                if($data != ""){
                    $update = true;
                }
                $data = $message;
            }
            if($sessiondatafor == 'jssupportticket_spamcheckid'){
                $data = $this->getNotificationDatabySessionId($sessiondatafor);
                if($data != ""){
                    $update = true;
                    $data = $message;
                }else{
                    $data = $message;
                }
            }
            if($sessiondatafor == 'jssupportticket_rot13'){
                $data = $this->getNotificationDatabySessionId($sessiondatafor);
                if($data != ""){
                    $update = true;
                    $data = $message;
                }else{
                    $data = $message;
                }
            }
            if($sessiondatafor == 'jssupportticket_spamcheckresult'){
                $data = $this->getNotificationDatabySessionId($sessiondatafor);
                if($data != ""){
                    $update = true;
                    $data = $message;
                }else{
                    $data = $message;
                }
            }
            $data = json_encode($data , true);
            $sessionmsg = base64_encode($data);
            if(!$update){
                $wpdb->insert( "{$wpdb->prefix}js_ticket_jshdsessiondata", array("usersessionid" => jssupportticket::$_jshdsession->sessionid, "sessionmsg" => $sessionmsg, "sessionexpire" => jssupportticket::$_jshdsession->sessionexpire, "sessionfor" => $sessiondatafor) );
            }else{
                $wpdb->update( "{$wpdb->prefix}js_ticket_jshdsessiondata", array("sessionmsg" => $sessionmsg), array("usersessionid" => jssupportticket::$_jshdsession->sessionid , 'sessionfor' => $sessiondatafor) );
            }
        }
        return false;
    }

    public function getNotificationDatabySessionId($sessionfor , $deldata = false){
        if(jssupportticket::$_jshdsession->sessionid == '')
            return false;
        global $wpdb;
        $data = $wpdb->get_var( "SELECT sessionmsg FROM {$wpdb->prefix}js_ticket_jshdsessiondata WHERE usersessionid = '" . jssupportticket::$_jshdsession->sessionid . "' AND sessionfor = '" . $sessionfor . "' AND sessionexpire > '" . time() . "'");
        if(!empty($data)){
            $data = base64_decode($data);
            $data = json_decode( $data , true);
        }
        if($deldata){
            $wpdb->delete( "{$wpdb->prefix}js_ticket_jshdsessiondata", array( 'usersessionid' => jssupportticket::$_jshdsession->sessionid , 'sessionfor' => $sessionfor) );
        }
        return $data;
    }

}

?>
