=== WPSSO Core | Meta Tags and Schema Structured Data ===
Plugin Name: WPSSO Core
Plugin Slug: wpsso
Text Domain: wpsso
Domain Path: /languages
License: GPLv3
License URI: https://www.gnu.org/licenses/gpl.txt
Assets URI: https://surniaulula.github.io/wpsso/assets/
Tags: woocommerce, rich snippets, seo, schema, open graph, facebook, linkedin, twitter, google, pinterest, xml sitemap
Contributors: jsmoriss
Requires PHP: 7.0
Requires At Least: 5.0
Tested Up To: 5.8
WC Tested Up To: 5.6.0
Stable Tag: 8.38.0

Rank higher and improve click-through-rates by presenting your content at its best on social sites and in search results.

== Description ==

<h3 class="top">THE MOST ADVANCED STRUCTURED DATA PLUGIN FOR WORDPRESS</h3>

<p><img class="readme-icon" src="https://surniaulula.github.io/wpsso/assets/icon-256x256.png"><strong>WPSSO helps you rank higher and improves click-through-rates by presenting your content at its best on social sites and in search results - no matter how URLs are shared, re-shared, messaged, posted, embedded, or crawled.</strong></p>

<p><strong>WPSSO provides meta tags and structured data markup for:</strong></p>

<ul>
	<li>Facebook / Open Graph</li>
	<li>Google Knowledge Graph</li>
	<li>Google Rich Results (aka Rich Snippets)</li>
	<li>LinkedIn / oEmbed Data</li>
	<li>Mobile Web Browsers</li>
	<li>Pinterest Rich Pins</li>
	<li>Twitter Cards</li>
	<li>Schema.org Markup (for 500+ Schema Types)</li>
	<li>WhatsApp and Messaging Apps</li>
	<li>WordPress REST API and More!</li>
</ul>

<p><strong>WPSSO reads existing content, plugin, and service API data:</strong></p>

There's no need to manually re-enter descriptions, titles, product information, or re-select images and videos! WPSSO can read your existing WordPress content, plugin data, and fetch data from remote service APIs (Bitly, Facebook, Shopper Approved, Stamped.io, Vimeo, Wistia, YouTube, and many more).

WPSSO can be your only social and search optimization plugin, or combined to improve the structured data of another SEO plugin (like All in One SEO Pack, Jetpack SEO Tools, Rank Math SEO, SEO Ultimate, SEOPress, The SEO Framework, WP Meta SEO, Yoast SEO, and more).

<p><strong>WPSSO is FAST, reliable, and coded for performance:</strong></p>

<p>WPSSO is coded for SPEED and QUALITY ASSURANCE with advanced caching techniques, media optimization (ie. image and video SEO, 1:1, 4:3, and 16:9 images for Google, etc.), template validations, discreet contextual notices, and much more.</p>

<p>No cartoons or fancy marketing - just fast and reliable code. &#x1F609;</p>

<h3>Users Love the WPSSO Core Plugin</h3>

&#x2605;&#x2605;&#x2605;&#x2605;&#x2605; - "Unlike competitors, you can literally customize just about every aspect of SEO and Social SEO if you desire to. &#91;...&#93; This plugin has the most complete JSON-LD markup out of any of them, so you won’t have any errors and warnings in search console for WordPress or WooCommerce sites. You can go crazy customizing everything, or you can just set and forget. There aren’t many plugins that allow the best of both worlds." - [kw11](https://wordpress.org/support/topic/most-responsive-developer-ive-ever-seen/)

&#x2605;&#x2605;&#x2605;&#x2605;&#x2605; - "This plugin makes getting sites structured data ready extremely easy, and it works flawlessly without any issues. It shows messages on the top bar every step of the way to alert you of any issues until everything is set up correctly. It made all my ecommerce products pass Google's validation tests. Great work." - [marguy1](https://wordpress.org/support/topic/excellent-plugin-6825/)

&#x2605;&#x2605;&#x2605;&#x2605;&#x2605; - "What a fantastic plugin. If you want to fix all the errors in search console for structured data, this is the plugin to use. Love it." - [goviral](https://wordpress.org/support/topic/makes-schema-so-easy/)

&#x2605;&#x2605;&#x2605;&#x2605;&#x2605; - "This plugin saves me so much time, and it has really lifted my SERP rankings. Most of my keywords I now rank 1-3 position. I also noticed after about a week that my impressions have gone up at least 75%. I upgraded to the pro version which gave me even more options." - [playnstocks](https://wordpress.org/support/topic/excellent-plugin-and-support-200/)

<h3>WPSSO Core Plugin Features</h3>

Provides meta tags and Schema markup for WordPress posts / pages, plugin and theme custom post types, taxonomies, search results, shop pages, and much more.

Enhances the WordPress oEmbed data for LinkedIn, Discord, Drupal, Squarespace, and others.

Includes additional article and product meta tags for Slack.

Offers optimized image sizes for social sites and search engines:

* Open Graph (Facebook and oEmbed)
* Pinterest Pin It
* Schema 1:1 (Google)
* Schema 4:3 (Google)
* Schema 16:9 (Google)
* Schema Thumbnail
* Twitter Summary Card
* Twitter Large Image Summary Card

Built-in compatibility with AMP plugins:

* [AMP](https://wordpress.org/plugins/amp/)
* [AMP for WP](https://wordpress.org/plugins/accelerated-mobile-pages/)

Built-in compatibility with caching and optimization plugins:

* Autoptimize
* Cache Enabler
* Comet Cache
* Hummingbird Cache
* LiteSpeed Cache
* Pagely Cache
* Perfect Images + Retina
* SiteGround Cache
* W3 Total Cache (aka W3TC)
* WP Engine Cache
* WP Fastest Cache
* WP Rocket Cache
* WP Super Cache

Built-in compatibility with popular SEO plugins:

* All in One SEO Pack
* Jetpack SEO
* Rank Math SEO
* SEO Ultimate
* SEOPress
* Slim SEO
* Squirrly SEO
* The SEO Framework
* WP Meta SEO
* Yoast SEO
* Yoast WooCommerce SEO

Built-in compatibility for advanced WordPress configurations:

* WordPress MU Domain Mapping
* Network / Multisite Installations

Includes advanced QUALITY ASSURANCE features and options:

* Checks and warns of any missing PHP modules.
* Checks third-party plugin settings for possible conflicts.
* Checks for minimum / maximum image dimensions and aspect ratios.
* Shows notices for missing and required images.
* Validates theme header templates for correct HTML markup.
* Verifies webpage HTML for duplicate meta tags.

The WPSSO Core Standard plugin is designed to satisfy the requirements of most standard WordPress sites. If your site requires additional third-party plugin and service API integration, like WooCommerce shops, embedded video support, or advanced customization features, then you may want to get the [WPSSO Core Premium plugin](https://wpsso.com/extend/plugins/wpsso/) for those additional features.

**[Premium]** Detection of embedded videos in content text with API support for Facebook, Slideshare, Vimeo, Wistia, and Youtube videos.

**[Premium]** Support for the Twitter [Player Card](https://dev.twitter.com/cards/types/player) for embedded videos.

**[Premium]** Optional upscaling of small images to satisfy minimum image size requirements for social sharing and Schema markup.

**[Premium]** URL shortening with Bitly, DLMY.App, Google, Ow.ly, TinyURL, or YOURLS.

**[Premium]** Customize the default Open Graph and Schema document type for posts, pages, custom post types, taxonomy terms (categories, tags, etc.), and user profile pages.

**[Premium]** Customize the post and taxonomy types included in the WordPress sitemap XML.

**[Premium]** Complete Schema JSON-LD markup for WooCommerce products ([WPSSO JSON add-on required](https://wordpress.org/plugins/wpsso-schema-json-ld/)).

**[Premium]** Integrates with other plugins and service APIs for additional image, video, e-Commerce product details, SEO settings, etc. The following modules are included with the Premium version and automatically loaded if/when the supported plugins and/or services are required.

* **Reads data from third-party plugins:** 

	* All in One SEO Pack
	* bbPress
	* BuddyPress
	* Co-Authors Plus
	* Easy Digital Downloads
	* Gravity Forms + GravityView
	* NextCellent Gallery - NextGEN Legacy
	* NextGEN Gallery
	* Perfect WooCommerce Brands
	* Polylang
	* Product GTIN (EAN, UPC, ISBN) for WooCommerce
	* Rate my Post
	* SEOPress
	* Simple Job Board
	* The Events Calendar
	* The SEO Framework
	* WooCommerce
	* WooCommerce Brands
	* WooCommerce Currency Switcher
	* WooCommerce UPC, EAN, and ISBN
	* WooCommerce Show Single Variations
	* WP Job Manager
	* WP Meta SEO
	* WP-PostRatings
	* WP Product Review
	* WP Recipe Maker
	* WPML
	* YITH WooCommerce Brands Add-on
	* Yoast SEO
	* Yotpo Social Reviews for WooCommerce

* **Reads data from remote service APIs:**

	* Bitly
	* DLMY.App
	* Facebook Embedded Videos
	* Gravatar (Author Image)
	* Ow.ly
	* Shopper Approved (Ratings and Reviews)
	* Slideshare Presentations
	* Soundcloud Tracks (for the Twitter Player Card)
	* Stamped.io (Ratings and Reviews)
	* TinyURL
	* Vimeo Videos
	* Wistia Videos
	* WordPress Video Shortcode (and Self-Hosted Videos)
	* Your Own URL Shortener (YOURLS)
	* YouTube Videos and Playlists

<h3>Free Complementary Add-ons</h3>

<p><strong>Do you need even more advanced or unique features?</strong></p>

Activate any of the free complementary add-on(s) you require:

* [WPSSO FAQ Manager](https://wordpress.org/plugins/wpsso-faq/) to manage FAQ categories with Question and Answer pages.
* [WPSSO Inherit Parent Metadata](https://wordpress.org/plugins/wpsso-inherit-parent-meta/) to inherit featured and custom images.
* [WPSSO Mobile App Meta Tags](https://wordpress.org/plugins/wpsso-am/) to manage mobile App information.
* [WPSSO Organization Markup](https://wordpress.org/plugins/wpsso-organization/) to manage multiple organizations.
* [WPSSO Place and Local SEO Markup](https://wordpress.org/plugins/wpsso-plm/) to manage multiple locations.
* [WPSSO Product Metadata for WooCommerce](https://wordpress.org/plugins/wpsso-wc-metadata/) to add GTIN, GTIN-8, GTIN-12 (UPC), GTIN-13 (EAN), GTIN-14, ISBN, MPN, depth, and volume for WooCommerce products and variations.
* [WPSSO Ratings and Reviews](https://wordpress.org/plugins/wpsso-ratings-and-reviews/) to add ratings in WordPress comments.
* [WPSSO REST API](https://wordpress.org/plugins/wpsso-rest-api/) to add meta tags and Schema markup in REST API queries.
* [WPSSO Ridiculously Responsive Social Sharing Buttons](https://wordpress.org/plugins/wpsso-rrssb/) to add responsive share buttons.
* [WPSSO Schema Breadcrumbs Markup](https://wordpress.org/plugins/wpsso-breadcrumbs/) to add Breadcrumbs markup for Google.
* [WPSSO Schema JSON-LD Markup](https://wordpress.org/plugins/wpsso-schema-json-ld/) to provide Google with Schema markup in its preferred format.
* [WPSSO Shipping Delivery Time for WooCommerce](https://wordpress.org/plugins/wpsso-wc-shipping-delivery-time/) to provide Google with shipping rates and delivery time estimates.
* [WPSSO Strip Schema Microdata](https://wordpress.org/plugins/wpsso-strip-schema-microdata/) to strip incorrect markup from templates.
* [WPSSO Tune WP Image Editors](https://wordpress.org/plugins/wpsso-tune-image-editors/) for better looking WordPress thumbnails.
* [WPSSO User Locale Selector](https://wordpress.org/plugins/wpsso-user-locale/) to switch languages quickly and easily.

== Installation ==

<h3 class="top">Install and Uninstall</h3>

* [Install the WPSSO Core Plugin](https://wpsso.com/docs/plugins/wpsso/installation/install-the-plugin/)
* [Uninstall the WPSSO Core Plugin](https://wpsso.com/docs/plugins/wpsso/installation/uninstall-the-plugin/)

<h3>Plugin Setup</h3>

* [Setup Guide](https://wpsso.com/docs/plugins/wpsso/installation/setup-guide/)
* [Much Better Schema Markup for WooCommerce Products](https://wpsso.com/docs/plugins/wpsso/installation/better-schema-for-woocommerce/)
* [Integration Notes](https://wpsso.com/docs/plugins/wpsso/installation/integration/)
	* [BuddyPress Integration Notes](https://wpsso.com/docs/plugins/wpsso/installation/integration/buddypress-integration/)
	* [WooCommerce Integration Notes](https://wpsso.com/docs/plugins/wpsso/installation/integration/woocommerce-integration/)
* [Troubleshooting Guide](https://wpsso.com/docs/plugins/wpsso/installation/troubleshooting-guide/)
* [Developer Special: Buy one, Get one Free](https://wpsso.com/docs/plugins/wpsso/installation/developer-special-buy-one-get-one-free/)

== Frequently Asked Questions ==

<h3 class="top">Frequently Asked Questions</h3>

* [Does LinkedIn read Facebook / Open Graph meta tags?](https://wpsso.com/docs/plugins/wpsso/faqs/does-linkedin-read-the-open-graph-meta-tags/)
* [How can I fix a ERR_TOO_MANY_REDIRECTS error?](https://wpsso.com/docs/plugins/wpsso/faqs/how-can-i-fix-a-err_too_many_redirects-error/)
* [How can I fix a generic HTTP 500 error?](https://wpsso.com/docs/plugins/wpsso/faqs/how-can-i-fix-a-generic-http-500-error/)
* [How can I fix a PHP fatal "out of memory" error?](https://wpsso.com/docs/plugins/wpsso/faqs/how-can-i-fix-a-php-fatal-out-of-memory-error/)
* [How can I fix an HTTP error when uploading images?](https://wpsso.com/docs/plugins/wpsso/faqs/how-can-i-fix-an-http-error-when-uploading-images/)
* [How can I have smaller dimensions for the default image?](https://wpsso.com/docs/plugins/wpsso/faqs/how-can-i-have-smaller-dimensions-for-the-default-image/)
* [How can I see what the Facebook crawler sees?](https://wpsso.com/docs/plugins/wpsso/faqs/how-can-i-see-what-the-facebook-crawler-sees/)
* [How do I enable WordPress WP_DEBUG?](https://wpsso.com/docs/plugins/wpsso/faqs/how-do-i-enable-wordpress-wp_debug/)
* [How do I fix Google Structured Data &gt; hatom errors?](https://wpsso.com/docs/plugins/wpsso/faqs/how-do-i-fix-google-structured-data-hatom-errors/)
* [How do I remove duplicate meta tags?](https://wpsso.com/docs/plugins/wpsso/faqs/how-do-i-remove-duplicate-meta-tags/)
* [How does WPSSO Core find and select images?](https://wpsso.com/docs/plugins/wpsso/faqs/how-does-wpsso-find-detect-select-images/)
* [How does WPSSO Core find and select videos?](https://wpsso.com/docs/plugins/wpsso/faqs/how-does-wpsso-find-detect-select-videos/)
* [W3C says "there is no attribute 'property'"](https://wpsso.com/docs/plugins/wpsso/faqs/w3c-says-there-is-no-attribute-property/)
* [Why are some HTML elements missing or misaligned?](https://wpsso.com/docs/plugins/wpsso/faqs/why-are-some-html-elements-missing-misaligned-different/)
* [Why does Facebook show the wrong image / text?](https://wpsso.com/docs/plugins/wpsso/faqs/why-does-facebook-show-the-wrong-image-text/)
* [Why does the Schema Markup Validator show errors?](https://wpsso.com/docs/plugins/wpsso/faqs/why-does-google-structured-data-testing-tool-show-errors/)
* [Why shouldn't I upload small images to the media library?](https://wpsso.com/docs/plugins/wpsso/faqs/why-shouldnt-i-upload-small-images-to-the-media-library/)

<h3>Notes and Documentation</h3>

* [Developer Resources](https://wpsso.com/docs/plugins/wpsso/notes/developer/)
	* [Constants](https://wpsso.com/docs/plugins/wpsso/notes/developer/constants/)
	* [Filters](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/)
		* [All Filters](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/all/)
		* [Filter Examples](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/examples/)
			* [Define a Custom Post Type (CPT) as a Product](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/examples/define-a-custom-post-type-cpt-as-a-product/)
			* [Detect YouTube URL Links as Videos](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/examples/detect-youtube-url-links-as-videos/)
			* [Modify the "article:tag" Keywords / Names](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/examples/modify-the-articletag-keywords-names/)
			* [Modify the Default Article Section List](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/examples/modify-the-default-article-sections-list/)
			* [Remove / Fix 'hentry' Errors in your Theme Templates](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/examples/remove-hentry-from-theme-templates/)
			* [Strip Additional Shortcodes](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/examples/strip-additional-shortcodes/)
			* [Use the REQUEST_URI for Post URLs](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/examples/use-the-request_uri-for-post-urls/)
			* [Working with Pre-defined Meta Tags and Custom Post Types](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/examples/working-with-pre-defined-meta-tags-and-custom-post-types/)
		* [Filters by Category](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/by-category/)
			* [Head Filters](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/by-category/head/)
			* [Media Filters](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/by-category/media/)
			* [Facebook / Open Graph Filters](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/by-category/open-graph/)
			* [Twitter Card Filters](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/by-category/twitter-card/)
			* [Webpage Filters](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/by-category/webpage/)
		* [Other Filters](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/other/)
	* [The $mod Variable](https://wpsso.com/docs/plugins/wpsso/notes/developer/the-mod-variable/)
* [Inline Variables](https://wpsso.com/docs/plugins/wpsso/notes/inline-variables/)
* [Multisite / Network Support](https://wpsso.com/docs/plugins/wpsso/notes/multisite-network-support/)

== Screenshots ==

01. The Essential and General Settings pages provide a simple, fast, and easy setup.
02. The Document SSO metabox allows you to easily customize the details of articles, events, products, recipes, reviews, and much more.
03. The Preview and oEmbed tabs allow you to quickly preview an example social share.

== Changelog ==

<h3 class="top">Release Schedule</h3>

<p>New versions of the plugin are released approximately every week (more or less). New features are added, tested, and released incrementally, instead of grouping them together in a major version release. When minor bugs fixes and/or code improvements are applied, new versions are also released. This release schedule keeps the code stable and reliable, at the cost of more frequent updates.</p>

<p>See <a href="https://en.wikipedia.org/wiki/Release_early,_release_often">release early, release often (RERO) software development philosophy</a> on Wikipedia for more information on the benefits of smaller / more frequent releases.</p>

<h3>Version Numbering</h3>

Version components: `{major}.{minor}.{bugfix}[-{stage}.{level}]`

* {major} = Major structural code changes / re-writes or incompatible API changes.
* {minor} = New functionality was added or improved in a backwards-compatible manner.
* {bugfix} = Backwards-compatible bug fixes or small improvements.
* {stage}.{level} = Pre-production release: dev &lt; a (alpha) &lt; b (beta) &lt; rc (release candidate).

<h3>Standard Version Repositories</h3>

* [GitHub](https://surniaulula.github.io/wpsso/)
* [WordPress.org](https://plugins.trac.wordpress.org/browser/wpsso/)

<h3>Development Updates for Premium Users</h3>

<p>Development, alpha, beta, and release candidate updates are available for Premium users.</p>

<p>Under the SSO &gt; Update Manager settings page, select the "Development and Up" version filter for WPSSO Core and all its extensions (to satisfy any version dependencies). Save the plugin settings, and click the "Check for Plugin Updates" button to fetch the latest / current WPSSO version information. When new Development versions are available, they will automatically appear under your WordPress Dashboard &gt; Updates page. You can always re-select the "Stable / Production" version filter at any time to re-install the last stable / production version of a plugin.</p>

<h3>Changelog / Release Notes</h3>

**Version 8.39.0-dev.1 (2021/09/08)**

* **New Features**
	* None.
* **Improvements**
	* Moved all options from the WPSSO JSON add-on settings page to the SSO &gt; General and Advanced Settings pages.
	* Removed the SSO &gt; Advanced Settings &gt; Plugin Settings &gt; Interface &gt; Title / Name Column Width option.
	* Removed the SSO &gt; Advanced Settings &gt; Plugin Settings &gt; Interface &gt; Default for Posts / Pages List option.
* **Bugfixes**
	* None.
* **Developer Notes**
	* None.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v5.0.

**Version 8.38.0 (2021/09/07)**

* **New Features**
	* None.
* **Improvements**
	* Removed the "Sharing URL" option in the Document SSO metabox.
* **Bugfixes**
	* None.
* **Developer Notes**
	* Added new '%%canonical_url%%', '%%canonical_short_url%%', and '%%sharing_short_url%%' inline variables.
	* Changes to methods:
		* Added a new `WpssoUtil->shorten_url()` method.
		* Added a new '$atts' (third) argument to the `WpssoUtil->get_sharing_url()` method.
		* Renamed the `WpssoPost->get_sharing_shortlink()` method to `WpssoPost->get_canonical_shortlink()`.
		* Renamed the `WpssoUtil->replace_inline_vars()` method to `WpssoUtil->replace_inline_variables()`.
		* Renamed the `WpssoUtil->get_inline_vars()` method to `WpssoUtil->get_inline_variables()`.
		* Renamed the `WpssoUtil->get_inline_vals()` method to `WpssoUtil->get_inline_values()`.
		* Removed the `WpssoUtil->get_type_url()` private method.
		* Updated the `SucomForm->get_th_html()` method to get tooltips only for the CSS id.
		* Updated the `SucomUtil->get_url()` method to remove common tracking query arguments by default.
	* Changes to functions:
		* Added a new `wpsso_get_sharing_short_url()` function.
		* Added a new `wpsso_get_post_canonical_short_url()` function.
		* Renamed the `wpsso_get_short_url()` function to `wpsso_get_canonical_short_url()`.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v5.0.

**Version 8.37.0 (2021/09/02)**

* **New Features**
	* Deprecated the WPSSO PLM Premium add-on.
* **Improvements**
	* Added new "Default Country" and "Default Timezone" options under the SSO &gt; General Settings &gt; General Settings &gt; Site Information tab.
	* Added a new "Organization Schema Type" option under the SSO &gt; General Settings &gt; Social and Search Sites &gt; Google tab.
	* Removed the "Author / Person Name Format" option under the SSO &gt; General Settings &gt; Social and Search Sites &gt; Google tab.
* **Bugfixes**
	* None.
* **Developer Notes**
	* Added a jQuery enable/disable link between the '#select_schema_organization_id' and '#select_schema_place_id' options.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v5.0.

**Version 8.36.1 (2021/08/27)**

* **New Features**
	* Deprecated the WPSSO ORG Premium add-on.
* **Improvements**
	* Added a new "Organization Location" option under the SSO &gt; General Settings &gt; Social and Search Sites &gt; Google tab.
* **Bugfixes**
	* Fixed support for a home page custom Schema Type from the Document SSO metabox when getting the site organization options.
* **Developer Notes**
	* Added a new `WpssoUtilWooCommerce->has_meta()` method for the WPSSO WCMD add-on.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v5.0.

**Version 8.36.0 (2021/08/24)**

* **New Features**
	* Added new input fields to the user profile About Yourself / About the user sections:
		* Job Title
		* Honorific Prefix
		* Honorific Suffix
		* Middle or Additional Name
	* Added a new "About the User" metabox in the SSO &gt; Advanced Settings page (Premium version).
* **Improvements**
	* Added new properties for Schema Person markup of WordPress users:
		* 'jobTitle'
		* 'honorificPrefix'
		* 'honorificSuffix'
		* 'additionalName'
	* Added a new SSO &gt; General Settings &gt; Social and Search Sites &gt; Facebook &gt; Facebook Domain Verification ID option.
	* Moved the SSO &gt; Advanced Settings &gt; Plugin Settings &gt; Caching tab options to the Integration tab and removed the Caching tab (Premium version).
	* Added the SSO &gt; Advanced Settings &gt; Plugin Settings &gt; Integration tab in the network admin SSO settings (Premium version).
	* Added a "Plugin and Theme Integration" section under the SSO &gt; Advanced Settings &gt; Plugin Settings &gt; Integration tab (Premium version).
	* Added a new "Include VAT in Product Prices" option under the SSO &gt; Advanced Settings &gt; Plugin Settings &gt; Integration tab (Premium version).
* **Bugfixes**
	* Fixed the "Content Image Alt Prefix" option value for non-default locales.
* **Developer Notes**
	* None.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v5.0.

**Version 8.35.4 (2021/08/19)**

* **New Features**
	* None.
* **Improvements**
	* Updated the deprecated Google Structured Data Testing tool URL to the Schema Markup Validator tool URL.
	* Added automatic select/deselect site verification meta tags if/when verification IDs are entered in the plugin settings.
* **Bugfixes**
	* Fixed automatic disabling of the head and content cache if the request URL contains an unknown/extra query string.
* **Developer Notes**
	* Added a new `SucomUtil::get_url()` method to get the complete request URL.
	* Deprecated the `WpssoUtilCache->expire_zero_filters()` method.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v5.0.

**Version 8.35.3 (2021/08/10)**

* **New Features**
	* None.
* **Improvements**
	* Added optional throttling by host when retrieving URLs for caching.
	* Added 3 second throttling in the YouTube integration module (Premium version).
	* Added support for the "Product brand taxonomy" option value in the YITH WooCommerce Brands Add-on integration module (Premium version).
	* Added support for the 'yith_wcbr_taxonomy_slug' filter in the YITH WooCommerce Brands Add-on integration module (Premium version).
* **Bugfixes**
	* None.
* **Developer Notes**
	* Updated the `WpssoMedia->add_og_video_from_url()` method to include the itemprop video duration value.
	* Deprecated the `WpssoProMediaYoutube->add_og_video_from_data()` method since Google has removed access to the https://www.youtube.com/get_video_info API.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v5.0.

**Version 8.35.2 (2021/07/26)**

* **New Features**
	* None.
* **Improvements**
	* Added support for the current page number when adding Schema comments.
* **Bugfixes**
	* None.
* **Developer Notes**
	* Added a new `WPSSO_SCHEMA_COMMENTS_MAX` constant (default is 50).
* **Requires At Least**
	* PHP v7.0.
	* WordPress v5.0.

**Version 8.35.1 (2021/07/24)**

* **New Features**
	* None.
* **Improvements**
	* Added support for the WooCommerce product `is_on_backorder()` method (Premium version).
	* Minor http header filter and form input placeholder handling improvements.
* **Bugfixes**
	* None.
* **Developer Notes**
	* Added error reporting in `WpssoProEcomWoocommerceBrandsCommon` for cases where `get_the_terms()` returns an error.
	* Refactored the `WpssoAdmin->add_expect_header()` method to handle badly coded headers created by other plugins.
	* Refactored the `SucomForm->get_placeholder_attrs()` method to skip pre-populating input values for license Authentication IDs.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v5.0.

**Version 8.35.0 (2021/07/10)**

* **New Features**
	* None.
* **Improvements**
	* Updated the SSO &gt; Dashboard &gt; Cache Status metabox.
	* Removed the following options from the SSO &gt; Advanced Settings &gt; Caching tab:
		* Head Markup Cache Expiry
		* Filtered Content Cache Expiry
		* Image URL Info Cache Expiry
		* API Response Cache Expiry
		* Short URL Cache Expiry
		* Schema Index Cache Expiry
		* Form Selects Cache Expiry
		* Cache Attachment Markup
		* Cache Date Archive Markup
* **Bugfixes**
	* Fix for possible critical error when creating a new order in WooCommerce with The SEO Framework plugin.
* **Developer Notes**
	* Refactored the `WpssoAdmin->show_metabox_cache_status()` method.
	* Refactored and renamed the `WpssoHead->get_mt_mark()` method to `get_mt_data()`.
	* Refactored the `WpssoUtil->get_cache_exp_secs()` method for the new 'conditional_values' config element.
	* Added an 'is_attachment' element to the `$mod` array (true for attachment post types).
	* Added new functions for filter hooks in lib/functions.php:
		* `__return_hour_in_seconds()`
		* `__return_day_in_seconds()`
		* `__return_week_in_seconds()`
		* `__return_month_in_seconds()`
		* `__return_year_in_seconds()`
* **Requires At Least**
	* PHP v7.0.
	* WordPress v5.0.

**Version 8.34.0 (2021/07/01)**

* **New Features**
	* None.
* **Improvements**
	* To prevent multiple layers of caching, the head markup cache is now disabled if a caching plugin or service is detected.
	* Renamed the SSO &gt; General Settings &gt; Content and Text tab to Titles / Descriptions.
	* Removed the SSO &gt; General Settings &gt; Authorship tab.
* **Bugfixes**
	* Fixed the video query arguments for YouTube's latest API requirements (Premium version).
	* Fixed video detection for the block editor 'wp-block-embed' block.
* **Developer Notes**
	* Renamed the 'og_author_field' options key to 'fb_author_field'.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v5.0.

**Version 8.33.0 (2021/06/27)**

* **New Features**
	* Added a new integration module for Stamped.io ratings and reviews in the WPSSO Core Premium plugin.
* **Improvements**
	* Added new options under the SSO &gt; Advanced Settings &gt; Service APIs &gt; Ratings and Reviews tab:
		* Ratings and Reviews Service
		* Stamped.io Store Hash
		* Stamped.io API Key Public
	* Renamed the "Video API Info Cache Expiry" option to "API Response Cache Expiry".
* **Bugfixes**
	* None.
* **Developer Notes**
	* Added supported caching plugins to the array returned by `WpssoCheck->get_avail()`.
	* Renamed the 'wpsso_cache_expire_video_info' filter to 'wpsso_cache_expire_api_response'.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.32.0 (2021/06/23)**

* **New Features**
	* Added a new SSO &gt; Advanced Settings &gt; Caching &gt; Cache Attachment Markup option.
* **Improvements**
	* Removed the "Clear All Caches on Activate" option (now always enabled).
	* Removed the "Clear All Caches on Deactivate" option (now always disabled for WPSSO Core and always enabled for add-ons).
* **Bugfixes**
	* Fix to remove the Document SSO metabox from the WooCommerce orders and coupons editing pages.
* **Developer Notes**
	* Added new constants:
		* `WPSSO_CACHE_REFRESH_MAX_TIME`
		* `WPSSO_CACHE_REFRESH_SLEEP_TIME`
	* Added `_deprecated_function()` calls to all deprecated functions and methods.
	* Refactored the `WpssoUtil->get_cache_exp_secs()` method.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.31.0 (2021/06/18)**

* **New Features**
	* Added a new SSO &gt; Advanced Settings &gt; Caching &gt; Cache Date Archive Markup option.
* **Improvements**
	* Added a snackbar reminder in the block editor in case there are any important error messages under the SSO notification icon.
* **Bugfixes**
	* None.
* **Developer Notes**
	* None.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.30.2 (2021/06/16)**

* **New Features**
	* None.
* **Improvements**
	* Moved the SSO &gt; Features metaboxes to the SSO &gt; Dashboard page.
	* Added an error notice message in case the `is_admin_bar_showing()` function returns false.
* **Bugfixes**
	* Fixed a non-working and duplicated copy-to-clipboard icon issue.
* **Developer Notes**
	* None.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.30.1 (2021/06/12)**

* **New Features**
	* None.
* **Improvements**
	* None.
* **Bugfixes**
	* Fixed the missing text length message for textarea fields in the Document SSO metabox.
* **Developer Notes**
	* None.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.30.0 (2021/06/08)**

* **New Features**
	* None.
* **Improvements**
	* Added a thumbnail to all Image ID options.
	* Updated the YouTube video API URL query arguments (Premium version).
* **Bugfixes**
	* Fixed an HTTP 404 error for YouTube video API calls (Premium version).
* **Developer Notes**
	* Added new `SucomUtil::sanitize_css_class()` and `SucomUtil::sanitize_css_id()` methods.
	* Renamed all '\*_img_id_pre' option keys to '\*_img_id_lib'.
	* Refactored the `WpssoMedia::get_default_images()` method.
	* Refactored almost all methods in the SucomForm class.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.29.0 (2021/05/30)**

Please note that the WP Ultimate Recipe plugin is deprecated and support for this plugin has been removed from the Premium version.

* **New Features**
	* None.
* **Improvements**
	* Removed the deprecated WP Ultimate Recipe integration module (Premium version).
	* Updated the WP Recipe Maker integration module to include instruction sections and images (Premium version).
* **Bugfixes**
	* None.
* **Developer Notes**
	* None.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.28.2 (2021/05/15)**

* **New Features**
	* None.
* **Improvements**
	* Added cache refresh when a WooCommerce product changes on the front-end - for example, from in stock to out of stock (Premium version).
* **Bugfixes**
	* None.
* **Developer Notes**
	* Added a hook for the 'woocommerce_after_product_object_save' action in the WooCommerce integration module (Premium version).
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.28.1 (2021/05/05)**

* **New Features**
	* None.
* **Improvements**
	* Updated the SSO &gt; Advanced &gt; Plugin Settings &gt; Image Sizes tab information text.
	* Moved Priority Media default image selection to Premium version.
* **Bugfixes**
	* Fixed the missing jQuery sucomTextLen() function calls to display text limits after saving/updating in the block editor.
* **Developer Notes**
	* Removed the WpssoEdit->filter_metabox_sso_media_rows() method.
	* Renamed Schema image option keys ratio delimiter from '_' to 'x'.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.28.0 (2021/04/30)**

* **New Features**
	* Added a new SSO &gt; Advanced Settings &gt; WordPress Sitemaps metabox with options to customize the post and taxonomy types included in the WordPress sitemap XML.
* **Improvements**
	* Updated the metabox tabs jQuery to scroll the tabbed container into view if/when necessary.
* **Bugfixes**
	* None.
* **Developer Notes**
	* None.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.27.0 (2021/04/24)**

* **New Features**
	* Removed support for the rtMedia plugin (Premium version).
* **Improvements**
	* Fine-tuned the CSS for settings and Document SSO metabox tabs.
	* Renamed the "SSO" menu item to "SSO (Social and Search Optimization)".
* **Bugfixes**
	* Fixed merging of new plugin / add-on options keys during update.
* **Developer Notes**
	* Refactored the Yoast SEO integration module (Premium version).
	* Added a new 'wpsso_wpseo_replace_vars' filter for the Yoast SEO integration modules.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.26.3 (2021/04/17)**

* **New Features**
	* None.
* **Improvements**
	* Included support for custom post types that do not appear in the WordPress menu.
	* Allowed 'expired' posts to have an 'article:published_time' meta tag (expired posts are assumed to have been previously published).
* **Bugfixes**
	* None.
* **Developer Notes**
	* Added debug messages to the WpssoPost, WpssoTerm, and WpssoUser `add_meta_boxes()` method.
	* Added a new `SucomUtilWP::get_available_languages()` method.
	* Added a new `SucomForm->get_checklist_post_tax_user()` method.
	* Refactored the `SucomForm->get_checklist_post_types()` method.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.26.2 (2021/04/13)**

* **New Features**
	* None.
* **Improvements**
	* Added a new SSO &gt; Tools and Actions &gt; Clear Failed URL Connections button.
* **Bugfixes**
	* Fixed the default size values included in help text for the Pinterest Pin It image size option.
* **Developer Notes**
	* Refactored the `WpssoUtilCache->clear_cache_files()` method.
	* Added a new `WpssoUtilCache->count_cache_files()` method.
	* Added a new `WpssoUtilCache->get_cache_files()` method.
	* Added a new `WpssoUtilCache->clear_ignored_urls()` method.
	* Added a new `WpssoUtilCache->count_ignored_urls()` method.
	* Added a new `SucomCache->clear_ignored_urls()` method.
	* Added a new `SucomCache->count_ignored_urls()` method.
	* Added a new `SucomCache->get_ignored_urls()` method.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.26.1 (2021/04/02)**

* **New Features**
	* None.
* **Improvements**
	* Moved the SSO &gt; Image Sizes settings to the SSO &gt; Advanced Settings &gt; Image Sizes tab.
	* Moved the SSO &gt; Document Types settings to the SSO &gt; Advanced Settings &gt; Document Types metabox.
	* Removed the Add Pinterest "nopin" to Images option from the SSO &gt; Essential Settings page (available in General Settings).
	* Removed the Add Hidden Image for Pinterest option from the SSO &gt; Essential Settings page (available in General Settings).
* **Bugfixes**
	* Fixed an incorrect prefix for site verification meta tag names.
* **Developer Notes**
	* Added a new 'wpsso_og_add_wc_mt_rating' filter.
	* Added a new 'wpsso_og_add_wc_mt_reviews' filter.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.25.2 (2021/03/30)**

* **New Features**
	* None.
* **Improvements**
	* Added a 'wpsso_add_schema_head_attributes' filter check (true by default) before validating theme header templates.
* **Bugfixes**
	* Fixed plugin option pattern matching for taxonomy names ending with '_time'.
* **Developer Notes**
	* Added a PHP class object check before getting the post content reading time.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.25.1 (2021/03/19)**

* **New Features**
	* None.
* **Improvements**
	* Added an `is_embed()` check for the robots 'noindex' default.
* **Bugfixes**
	* None.
* **Developer Notes**
	* None.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.25.0 (2021/03/11)**

* **New Features**
	* None.
* **Improvements**
	* Added a new option in the Document SSO metabox for Open Graph articles:
		* Est. Reading Time
	* Added a new option under the SSO &gt; Advanced Settings &gt; Plugin Admin tab:
		* Use Local Plugin Translations
	* Added new options under the SSO &gt; General Settings &gt; Social and Search Sites &gt; Other Sites tab:
		* Ahrefs Website Verification ID
		* Baidu Website Verification ID
		* Bing Website Verification ID
		* Yandex Website Verification ID
	* Moved customization of image sizes to the Premium version. 
* **Bugfixes**
	* Fixed plugin conflict detection with All In One SEO v4.0.
* **Developer Notes**
	* None.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.24.0 (2021/03/04)**

* **New Features**
	* Added theme templates for iframe embed content with additional support for custom image URLs.
* **Improvements**
	* None.
* **Bugfixes**
	* None.
* **Developer Notes**
	* Added a new lib/theme-compat/embed.php template.
	* Added a new lib/theme-compat/embed-content.php template.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.23.0 (2021/02/25)**

* **New Features**
	* None.
* **Improvements**
	* Added business page URL options in the SSO &gt; Social Pages settings page:
		* Medium Business Page URL
		* TikTok Business Page URL
	* Added custom contact options in the SSO &gt; Advanced settings page (Premium version):
		* Medium URL
		* TikTok URL
	* Updated the banners and icons of WPSSO Core and its add-ons.
* **Bugfixes**
	* None.
* **Developer Notes**
	* None.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.22.0 (2021/02/09)**

* **New Features**
	* None.
* **Improvements**
	* None.
* **Bugfixes**
	* None.
* **Developer Notes**
	* Updated product offer methods in `WpssoSchema` for WPSSO JSON v4.14.0:
		* Added a new `add_offers_data()` method.
		* Renamed the `add_aggregate_offer_data()` method to `add_offers_aggregate_data()`.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.21.0 (2021/01/30)**

* **New Features**
	* None.
* **Improvements**
	* None.
* **Bugfixes**
	* None.
* **Developer Notes**
	* Added new functions to retrieve Facebook / Open Graph image URLs:
		* `wpsso_get_mod_og_image_url()`
		* `wpsso_get_post_og_image_url()`
		* `wpsso_get_term_og_image_url()`
		* `wpsso_get_user_og_image_url()`
	* Added a new `SucomUtil::get_first_og_image_url()` method.
	* Refactored error handling and error messages in URL shortening classes (Premium version).
	* Refactored the `WpssoErrorException` class and added a new `SucomErrorException` class.
	* Refactored the `WpssoCache->add_ignored_url()` method to report cURL and SSL error messages.
	* Removed the 'wpsso_version_updates' action.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.20.0 (2021/01/21)**

* **New Features**
	* None.
* **Improvements**
	* Added support for images in WooCommerce product reviews (Premium version).
* **Bugfixes**
	* Fixed jQuery "document ready" event incompatibility with the block editor.
* **Developer Notes**
	* Added a new `WpssoComment` class in lib/comment.php.
	* Added support for 'is_comment' in `WpssoUtil->get_type_url()`.
	* Added support for 'reviews-images' comment metadata in `WpssoWpMeta->get_mt_comment_review()`.
	* Renamed `WpssoPost->get_og_type_reviews()` to `WpssoPost->get_mt_reviews()`.
	* Moved `WpssoPost->get_og_comment_review()` to `WpssoWpMeta->get_mt_comment_review()`.
	* Changed `jQuery( document ).on( 'ready' )` event handlers to `jQuery()` for jQuery v3.0 compatibility.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

== Upgrade Notice ==

= 8.39.0-dev.1 =

(2021/09/08) Moved all options from the WPSSO JSON add-on settings page to the SSO &gt; General and Advanced Settings pages.

= 8.38.0 =

(2021/09/07) Removed the "Sharing URL" option in the Document SSO metabox.

= 8.37.0 =

(2021/09/02) Deprecated the WPSSO PLM Premium add-on. Added new options in the SSO &gt; General Settings page.

= 8.36.1 =

(2021/08/27) Deprecated the WPSSO ORG Premium add-on. Added a new "Organization Location" option. Fixed support for a home page custom Schema Type when getting the site organization options.

= 8.36.0 =

(2021/08/24) Added new input fields to the user editing page. Added a new "Facebook Domain Verification ID" option. Updated the Integration and Caching tabs in the Advanced Settings page (Premium version).

= 8.35.4 =

(2021/08/19) Updated the deprecated Google Structured Data Testing tool URL. Added automatic select/deselect site verification meta tags. Fixed automatic disabling of the head and content cache if the request URL contains an unknown/extra query string.

= 8.35.3 =

(2021/08/10) Updated the YouTube integration module (Premium version). Updated the YITH WooCommerce Brands Add-on integration module (Premium version).

= 8.35.2 =

(2021/07/26) Added support for the current page number when adding Schema comments.

= 8.35.1 =

(2021/07/24) Added support for the WooCommerce product `is_on_backorder()` method (Premium version).

= 8.35.0 =

(2021/07/10) Updated the SSO &gt; Dashboard &gt; Cache Status metabox. Removed options from the SSO &gt; Advanced Settings &gt; Caching tab. Fix for possible critical error when creating a new order in WooCommerce with The SEO Framework plugin.

= 8.34.0 =

(2021/07/01) Fixed the video query arguments for YouTube's latest API requirements (Premium version). Fixed video detection for the block editor 'wp-block-embed' block.

= 8.33.0 =

(2021/06/27) Added a new integration module for Stamped.io ratings and reviews in the WPSSO Core Premium plugin.

= 8.32.0 =

(2021/06/23) Added a new SSO &gt; Advanced Settings &gt; Caching &gt; Cache Attachment Markup option. Fix to remove the Document SSO metabox from the WooCommerce orders and coupons editing pages.

= 8.31.0 =

(2021/06/18) Added a snackbar reminder in the block editor in case there are any important error messages under the SSO notification icon. Added a new SSO &gt; Advanced Settings &gt; Caching &gt; Cache Date Archive Markup option.

= 8.30.2 =

(2021/06/16) Fixed a non-working and duplicated copy-to-clipboard icon issue. Moved the SSO &gt; Features metaboxes to the SSO &gt; Dashboard page.

= 8.30.1 =

(2021/06/12) Fixed the missing text length message for textarea fields in the Document SSO metabox.

= 8.30.0 =

(2021/06/08) Added a thumbnail to all Image ID options. Updated the YouTube video API URL query arguments (Premium version).

= 8.29.0 =

(2021/05/30) Removed the deprecated WP Ultimate Recipe integration module (Premium version). Updated the WP Recipe Maker integration module to include instruction sections and images (Premium version).

= 8.28.2 =

(2021/05/15) Added cache refresh when a WooCommerce product changes on the front-end - for example, from in stock to out of stock (Premium version).

= 8.28.1 =

(2021/05/05) Fixed the missing jQuery sucomTextLen() function calls to display text limits after saving/updating in the block editor.

= 8.28.0 =

(2021/04/30) Added a new SSO &gt; Advanced Settings &gt; WordPress Sitemaps metabox with options to customize the post and taxonomy types included in the WordPress sitemap XML.

= 8.27.0 =

(2021/04/24) Removed support for the rtMedia plugin (Premium version). Refactored the Yoast SEO integration module (Premium version). Fixed merging of new plugin / add-on options keys during update.

= 8.26.3 =

(2021/04/17) Included support for custom post types that do not appear in the WordPress menu. Allowed 'expired' posts to have an 'article:published_time' meta tag.

= 8.26.2 =

(2021/04/13) Added a new SSO &gt; Tools and Actions &gt; Clear Failed URL Connections button.

= 8.26.1 =

(2021/04/02) Moved the SSO &gt; Image Sizes and SSO &gt; Document Types settings to the SSO &gt; Advanced Settings page.

= 8.25.2 =

(2021/03/30) Added a 'wpsso_add_schema_head_attributes' filter check (true by default) before validating theme header templates.

= 8.25.1 =

(2021/03/19) Added an `is_embed()` check for the robots 'noindex' default.

= 8.25.0 =

(2021/03/11) Added new options in the Document SSO metabox and settings pages. Fixed plugin conflict detection with All In One SEO v4.0.

= 8.24.0 =

(2021/03/04) Added theme templates for iframe embed content with additional support for custom image URLs.

= 8.23.0 =

(2021/02/25) Added new business page URL and custom contact options. Updated the banners and icons of WPSSO Core and its add-ons.

= 8.22.0 =

(2021/02/09) Updated product offer methods in `WpssoSchema` for WPSSO JSON v4.14.0.

= 8.21.0 =

(2021/01/30) Added new functions to retrieve Facebook / Open Graph image URLs. Refactored error handling and error messages in URL shortening classes (Premium version). Removed the 'wpsso_version_updates' action.

= 8.20.0 =

(2021/01/21) Added support for images in WooCommerce product reviews (Premium version). Fixed jQuery "document ready" event incompatibility with the block editor.

