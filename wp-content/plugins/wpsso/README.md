<h1>WPSSO Core</h1><h3>Meta Tags and Schema Structured Data</h3>

<table>
<tr><th align="right" valign="top" nowrap>Plugin Name</th><td>WPSSO Core</td></tr>
<tr><th align="right" valign="top" nowrap>Summary</th><td>Rank higher and improve click-through-rates by presenting your content at its best on social sites and in search results.</td></tr>
<tr><th align="right" valign="top" nowrap>Stable Version</th><td>8.38.0</td></tr>
<tr><th align="right" valign="top" nowrap>Requires PHP</th><td>7.0 or newer</td></tr>
<tr><th align="right" valign="top" nowrap>Requires WordPress</th><td>5.0 or newer</td></tr>
<tr><th align="right" valign="top" nowrap>Tested Up To WordPress</th><td>5.8</td></tr>
<tr><th align="right" valign="top" nowrap>Tested Up To WooCommerce</th><td>5.6.0</td></tr>
<tr><th align="right" valign="top" nowrap>Contributors</th><td>jsmoriss</td></tr>
<tr><th align="right" valign="top" nowrap>License</th><td><a href="https://www.gnu.org/licenses/gpl.txt">GPLv3</a></td></tr>
<tr><th align="right" valign="top" nowrap>Tags / Keywords</th><td>woocommerce, rich snippets, seo, schema, open graph, facebook, linkedin, twitter, google, pinterest, xml sitemap</td></tr>
</table>

<h2>Description</h2>

<h3 class="top">THE MOST ADVANCED STRUCTURED DATA PLUGIN FOR WORDPRESS</h3>

<p><img class="readme-icon" src="https://surniaulula.github.io/wpsso/assets/icon-256x256.png"><strong>WPSSO helps you rank higher and improves click-through-rates by presenting your content at its best on social sites and in search results - no matter how URLs are shared, re-shared, messaged, posted, embedded, or crawled.</strong></p>

<p><strong>WPSSO provides meta tags and structured data markup for:</strong></p>

<ul>
    <li>Facebook / Open Graph</li>
    <li>Google Knowledge Graph</li>
    <li>Google Rich Results (aka Rich Snippets)</li>
    <li>LinkedIn / oEmbed Data</li>
    <li>Mobile Web Browsers</li>
    <li>Pinterest Rich Pins</li>
    <li>Twitter Cards</li>
    <li>Schema.org Markup (for 500+ Schema Types)</li>
    <li>WhatsApp and Messaging Apps</li>
    <li>WordPress REST API and More!</li>
</ul>

<p><strong>WPSSO reads existing content, plugin, and service API data:</strong></p>

<p>There's no need to manually re-enter descriptions, titles, product information, or re-select images and videos! WPSSO can read your existing WordPress content, plugin data, and fetch data from remote service APIs (Bitly, Facebook, Shopper Approved, Stamped.io, Vimeo, Wistia, YouTube, and many more).</p>

<p>WPSSO can be your only social and search optimization plugin, or combined to improve the structured data of another SEO plugin (like All in One SEO Pack, Jetpack SEO Tools, Rank Math SEO, SEO Ultimate, SEOPress, The SEO Framework, WP Meta SEO, Yoast SEO, and more).</p>

<p><strong>WPSSO is FAST, reliable, and coded for performance:</strong></p>

<p>WPSSO is coded for SPEED and QUALITY ASSURANCE with advanced caching techniques, media optimization (ie. image and video SEO, 1:1, 4:3, and 16:9 images for Google, etc.), template validations, discreet contextual notices, and much more.</p>

<p>No cartoons or fancy marketing - just fast and reliable code. &#x1F609;</p>

<h3>Users Love the WPSSO Core Plugin</h3>

<p>&#x2605;&#x2605;&#x2605;&#x2605;&#x2605; - "Unlike competitors, you can literally customize just about every aspect of SEO and Social SEO if you desire to. &#91;...&#93; This plugin has the most complete JSON-LD markup out of any of them, so you won’t have any errors and warnings in search console for WordPress or WooCommerce sites. You can go crazy customizing everything, or you can just set and forget. There aren’t many plugins that allow the best of both worlds." - <a href="https://wordpress.org/support/topic/most-responsive-developer-ive-ever-seen/">kw11</a></p>

<p>&#x2605;&#x2605;&#x2605;&#x2605;&#x2605; - "This plugin makes getting sites structured data ready extremely easy, and it works flawlessly without any issues. It shows messages on the top bar every step of the way to alert you of any issues until everything is set up correctly. It made all my ecommerce products pass Google's validation tests. Great work." - <a href="https://wordpress.org/support/topic/excellent-plugin-6825/">marguy1</a></p>

<p>&#x2605;&#x2605;&#x2605;&#x2605;&#x2605; - "What a fantastic plugin. If you want to fix all the errors in search console for structured data, this is the plugin to use. Love it." - <a href="https://wordpress.org/support/topic/makes-schema-so-easy/">goviral</a></p>

<p>&#x2605;&#x2605;&#x2605;&#x2605;&#x2605; - "This plugin saves me so much time, and it has really lifted my SERP rankings. Most of my keywords I now rank 1-3 position. I also noticed after about a week that my impressions have gone up at least 75%. I upgraded to the pro version which gave me even more options." - <a href="https://wordpress.org/support/topic/excellent-plugin-and-support-200/">playnstocks</a></p>

<h3>WPSSO Core Plugin Features</h3>

<p>Provides meta tags and Schema markup for WordPress posts / pages, plugin and theme custom post types, taxonomies, search results, shop pages, and much more.</p>

<p>Enhances the WordPress oEmbed data for LinkedIn, Discord, Drupal, Squarespace, and others.</p>

<p>Includes additional article and product meta tags for Slack.</p>

<p>Offers optimized image sizes for social sites and search engines:</p>

<ul>
<li>Open Graph (Facebook and oEmbed)</li>
<li>Pinterest Pin It</li>
<li>Schema 1:1 (Google)</li>
<li>Schema 4:3 (Google)</li>
<li>Schema 16:9 (Google)</li>
<li>Schema Thumbnail</li>
<li>Twitter Summary Card</li>
<li>Twitter Large Image Summary Card</li>
</ul>

<p>Built-in compatibility with AMP plugins:</p>

<ul>
<li><a href="https://wordpress.org/plugins/amp/">AMP</a></li>
<li><a href="https://wordpress.org/plugins/accelerated-mobile-pages/">AMP for WP</a></li>
</ul>

<p>Built-in compatibility with caching and optimization plugins:</p>

<ul>
<li>Autoptimize</li>
<li>Cache Enabler</li>
<li>Comet Cache</li>
<li>Hummingbird Cache</li>
<li>LiteSpeed Cache</li>
<li>Pagely Cache</li>
<li>Perfect Images + Retina</li>
<li>SiteGround Cache</li>
<li>W3 Total Cache (aka W3TC)</li>
<li>WP Engine Cache</li>
<li>WP Fastest Cache</li>
<li>WP Rocket Cache</li>
<li>WP Super Cache</li>
</ul>

<p>Built-in compatibility with popular SEO plugins:</p>

<ul>
<li>All in One SEO Pack</li>
<li>Jetpack SEO</li>
<li>Rank Math SEO</li>
<li>SEO Ultimate</li>
<li>SEOPress</li>
<li>Slim SEO</li>
<li>Squirrly SEO</li>
<li>The SEO Framework</li>
<li>WP Meta SEO</li>
<li>Yoast SEO</li>
<li>Yoast WooCommerce SEO</li>
</ul>

<p>Built-in compatibility for advanced WordPress configurations:</p>

<ul>
<li>WordPress MU Domain Mapping</li>
<li>Network / Multisite Installations</li>
</ul>

<p>Includes advanced QUALITY ASSURANCE features and options:</p>

<ul>
<li>Checks and warns of any missing PHP modules.</li>
<li>Checks third-party plugin settings for possible conflicts.</li>
<li>Checks for minimum / maximum image dimensions and aspect ratios.</li>
<li>Shows notices for missing and required images.</li>
<li>Validates theme header templates for correct HTML markup.</li>
<li>Verifies webpage HTML for duplicate meta tags.</li>
</ul>

<p>The WPSSO Core Standard plugin is designed to satisfy the requirements of most standard WordPress sites. If your site requires additional third-party plugin and service API integration, like WooCommerce shops, embedded video support, or advanced customization features, then you may want to get the <a href="https://wpsso.com/extend/plugins/wpsso/">WPSSO Core Premium plugin</a> for those additional features.</p>

<p><strong>[Premium]</strong> Detection of embedded videos in content text with API support for Facebook, Slideshare, Vimeo, Wistia, and Youtube videos.</p>

<p><strong>[Premium]</strong> Support for the Twitter <a href="https://dev.twitter.com/cards/types/player">Player Card</a> for embedded videos.</p>

<p><strong>[Premium]</strong> Optional upscaling of small images to satisfy minimum image size requirements for social sharing and Schema markup.</p>

<p><strong>[Premium]</strong> URL shortening with Bitly, DLMY.App, Google, Ow.ly, TinyURL, or YOURLS.</p>

<p><strong>[Premium]</strong> Customize the default Open Graph and Schema document type for posts, pages, custom post types, taxonomy terms (categories, tags, etc.), and user profile pages.</p>

<p><strong>[Premium]</strong> Customize the post and taxonomy types included in the WordPress sitemap XML.</p>

<p><strong>[Premium]</strong> Complete Schema JSON-LD markup for WooCommerce products (<a href="https://wordpress.org/plugins/wpsso-schema-json-ld/">WPSSO JSON add-on required</a>).</p>

<p><strong>[Premium]</strong> Integrates with other plugins and service APIs for additional image, video, e-Commerce product details, SEO settings, etc. The following modules are included with the Premium version and automatically loaded if/when the supported plugins and/or services are required.</p>

<ul>
<li><p><strong>Reads data from third-party plugins:</strong></p>

<ul>
<li>All in One SEO Pack</li>
<li>bbPress</li>
<li>BuddyPress</li>
<li>Co-Authors Plus</li>
<li>Easy Digital Downloads</li>
<li>Gravity Forms + GravityView</li>
<li>NextCellent Gallery - NextGEN Legacy</li>
<li>NextGEN Gallery</li>
<li>Perfect WooCommerce Brands</li>
<li>Polylang</li>
<li>Product GTIN (EAN, UPC, ISBN) for WooCommerce</li>
<li>Rate my Post</li>
<li>SEOPress</li>
<li>Simple Job Board</li>
<li>The Events Calendar</li>
<li>The SEO Framework</li>
<li>WooCommerce</li>
<li>WooCommerce Brands</li>
<li>WooCommerce Currency Switcher</li>
<li>WooCommerce UPC, EAN, and ISBN</li>
<li>WooCommerce Show Single Variations</li>
<li>WP Job Manager</li>
<li>WP Meta SEO</li>
<li>WP-PostRatings</li>
<li>WP Product Review</li>
<li>WP Recipe Maker</li>
<li>WPML</li>
<li>YITH WooCommerce Brands Add-on</li>
<li>Yoast SEO</li>
<li>Yotpo Social Reviews for WooCommerce</li>
</ul></li>
<li><p><strong>Reads data from remote service APIs:</strong></p>

<ul>
<li>Bitly</li>
<li>DLMY.App</li>
<li>Facebook Embedded Videos</li>
<li>Gravatar (Author Image)</li>
<li>Ow.ly</li>
<li>Shopper Approved (Ratings and Reviews)</li>
<li>Slideshare Presentations</li>
<li>Soundcloud Tracks (for the Twitter Player Card)</li>
<li>Stamped.io (Ratings and Reviews)</li>
<li>TinyURL</li>
<li>Vimeo Videos</li>
<li>Wistia Videos</li>
<li>WordPress Video Shortcode (and Self-Hosted Videos)</li>
<li>Your Own URL Shortener (YOURLS)</li>
<li>YouTube Videos and Playlists</li>
</ul></li>
</ul>

<h3>Free Complementary Add-ons</h3>

<p><strong>Do you need even more advanced or unique features?</strong></p>

<p>Activate any of the free complementary add-on(s) you require:</p>

<ul>
<li><a href="https://wordpress.org/plugins/wpsso-faq/">WPSSO FAQ Manager</a> to manage FAQ categories with Question and Answer pages.</li>
<li><a href="https://wordpress.org/plugins/wpsso-inherit-parent-meta/">WPSSO Inherit Parent Metadata</a> to inherit featured and custom images.</li>
<li><a href="https://wordpress.org/plugins/wpsso-am/">WPSSO Mobile App Meta Tags</a> to manage mobile App information.</li>
<li><a href="https://wordpress.org/plugins/wpsso-organization/">WPSSO Organization Markup</a> to manage multiple organizations.</li>
<li><a href="https://wordpress.org/plugins/wpsso-plm/">WPSSO Place and Local SEO Markup</a> to manage multiple locations.</li>
<li><a href="https://wordpress.org/plugins/wpsso-wc-metadata/">WPSSO Product Metadata for WooCommerce</a> to add GTIN, GTIN-8, GTIN-12 (UPC), GTIN-13 (EAN), GTIN-14, ISBN, MPN, depth, and volume for WooCommerce products and variations.</li>
<li><a href="https://wordpress.org/plugins/wpsso-ratings-and-reviews/">WPSSO Ratings and Reviews</a> to add ratings in WordPress comments.</li>
<li><a href="https://wordpress.org/plugins/wpsso-rest-api/">WPSSO REST API</a> to add meta tags and Schema markup in REST API queries.</li>
<li><a href="https://wordpress.org/plugins/wpsso-rrssb/">WPSSO Ridiculously Responsive Social Sharing Buttons</a> to add responsive share buttons.</li>
<li><a href="https://wordpress.org/plugins/wpsso-breadcrumbs/">WPSSO Schema Breadcrumbs Markup</a> to add Breadcrumbs markup for Google.</li>
<li><a href="https://wordpress.org/plugins/wpsso-schema-json-ld/">WPSSO Schema JSON-LD Markup</a> to provide Google with Schema markup in its preferred format.</li>
<li><a href="https://wordpress.org/plugins/wpsso-wc-shipping-delivery-time/">WPSSO Shipping Delivery Time for WooCommerce</a> to provide Google with shipping rates and delivery time estimates.</li>
<li><a href="https://wordpress.org/plugins/wpsso-strip-schema-microdata/">WPSSO Strip Schema Microdata</a> to strip incorrect markup from templates.</li>
<li><a href="https://wordpress.org/plugins/wpsso-tune-image-editors/">WPSSO Tune WP Image Editors</a> for better looking WordPress thumbnails.</li>
<li><a href="https://wordpress.org/plugins/wpsso-user-locale/">WPSSO User Locale Selector</a> to switch languages quickly and easily.</li>
</ul>


<h2>Installation</h2>

<h3 class="top">Install and Uninstall</h3>

<ul>
<li><a href="https://wpsso.com/docs/plugins/wpsso/installation/install-the-plugin/">Install the WPSSO Core Plugin</a></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/installation/uninstall-the-plugin/">Uninstall the WPSSO Core Plugin</a></li>
</ul>

<h3>Plugin Setup</h3>

<ul>
<li><a href="https://wpsso.com/docs/plugins/wpsso/installation/setup-guide/">Setup Guide</a></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/installation/better-schema-for-woocommerce/">Much Better Schema Markup for WooCommerce Products</a></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/installation/integration/">Integration Notes</a>

<ul>
<li><a href="https://wpsso.com/docs/plugins/wpsso/installation/integration/buddypress-integration/">BuddyPress Integration Notes</a></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/installation/integration/woocommerce-integration/">WooCommerce Integration Notes</a></li>
</ul></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/installation/troubleshooting-guide/">Troubleshooting Guide</a></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/installation/developer-special-buy-one-get-one-free/">Developer Special: Buy one, Get one Free</a></li>
</ul>


<h2>Frequently Asked Questions</h2>

<h3 class="top">Frequently Asked Questions</h3>

<ul>
<li><a href="https://wpsso.com/docs/plugins/wpsso/faqs/does-linkedin-read-the-open-graph-meta-tags/">Does LinkedIn read Facebook / Open Graph meta tags?</a></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/faqs/how-can-i-fix-a-err_too_many_redirects-error/">How can I fix a ERR_TOO_MANY_REDIRECTS error?</a></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/faqs/how-can-i-fix-a-generic-http-500-error/">How can I fix a generic HTTP 500 error?</a></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/faqs/how-can-i-fix-a-php-fatal-out-of-memory-error/">How can I fix a PHP fatal "out of memory" error?</a></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/faqs/how-can-i-fix-an-http-error-when-uploading-images/">How can I fix an HTTP error when uploading images?</a></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/faqs/how-can-i-have-smaller-dimensions-for-the-default-image/">How can I have smaller dimensions for the default image?</a></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/faqs/how-can-i-see-what-the-facebook-crawler-sees/">How can I see what the Facebook crawler sees?</a></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/faqs/how-do-i-enable-wordpress-wp_debug/">How do I enable WordPress WP_DEBUG?</a></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/faqs/how-do-i-fix-google-structured-data-hatom-errors/">How do I fix Google Structured Data &gt; hatom errors?</a></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/faqs/how-do-i-remove-duplicate-meta-tags/">How do I remove duplicate meta tags?</a></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/faqs/how-does-wpsso-find-detect-select-images/">How does WPSSO Core find and select images?</a></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/faqs/how-does-wpsso-find-detect-select-videos/">How does WPSSO Core find and select videos?</a></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/faqs/w3c-says-there-is-no-attribute-property/">W3C says "there is no attribute 'property'"</a></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/faqs/why-are-some-html-elements-missing-misaligned-different/">Why are some HTML elements missing or misaligned?</a></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/faqs/why-does-facebook-show-the-wrong-image-text/">Why does Facebook show the wrong image / text?</a></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/faqs/why-does-google-structured-data-testing-tool-show-errors/">Why does the Schema Markup Validator show errors?</a></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/faqs/why-shouldnt-i-upload-small-images-to-the-media-library/">Why shouldn't I upload small images to the media library?</a></li>
</ul>

<h3>Notes and Documentation</h3>

<ul>
<li><a href="https://wpsso.com/docs/plugins/wpsso/notes/developer/">Developer Resources</a>

<ul>
<li><a href="https://wpsso.com/docs/plugins/wpsso/notes/developer/constants/">Constants</a></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/">Filters</a>

<ul>
<li><a href="https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/all/">All Filters</a></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/examples/">Filter Examples</a>

<ul>
<li><a href="https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/examples/define-a-custom-post-type-cpt-as-a-product/">Define a Custom Post Type (CPT) as a Product</a></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/examples/detect-youtube-url-links-as-videos/">Detect YouTube URL Links as Videos</a></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/examples/modify-the-articletag-keywords-names/">Modify the "article:tag" Keywords / Names</a></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/examples/modify-the-default-article-sections-list/">Modify the Default Article Section List</a></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/examples/remove-hentry-from-theme-templates/">Remove / Fix 'hentry' Errors in your Theme Templates</a></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/examples/strip-additional-shortcodes/">Strip Additional Shortcodes</a></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/examples/use-the-request_uri-for-post-urls/">Use the REQUEST_URI for Post URLs</a></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/examples/working-with-pre-defined-meta-tags-and-custom-post-types/">Working with Pre-defined Meta Tags and Custom Post Types</a></li>
</ul></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/by-category/">Filters by Category</a>

<ul>
<li><a href="https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/by-category/head/">Head Filters</a></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/by-category/media/">Media Filters</a></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/by-category/open-graph/">Facebook / Open Graph Filters</a></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/by-category/twitter-card/">Twitter Card Filters</a></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/by-category/webpage/">Webpage Filters</a></li>
</ul></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/other/">Other Filters</a></li>
</ul></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/notes/developer/the-mod-variable/">The $mod Variable</a></li>
</ul></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/notes/inline-variables/">Inline Variables</a></li>
<li><a href="https://wpsso.com/docs/plugins/wpsso/notes/multisite-network-support/">Multisite / Network Support</a></li>
</ul>


