<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Auth;

class SampleTest extends TestCase
{
	public function testLogin()
    {
        $data['email_address'] = 'hanglee@yopmail.com';
        $data['password']      = '12345678Aa';
        $response = $this->post('/Dodistributorsignin', $data);
        $this->be($response->original['data']);
        $this->assertEquals('1', $response->original['status']);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
    	$data['email_address'] = 'hanglee@yopmail.com';
        $data['password']      = '12345678Aa';
        $response = $this->post('/Dodistributorsignin', $data);
        $this->be($response->original['data']);
        $data = $this->get('/Getmycourses');
        $this->assertEquals('1', $data->original['status']);
    }
}
