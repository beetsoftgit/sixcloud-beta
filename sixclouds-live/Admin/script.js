// script.js
// create the module and name it sixcloudApp
// also include ngRoute for all our routing needs
var sixcloudApp = angular.module('sixcloudApp', ['ngRoute']);

// configure our routes
sixcloudApp.config(function($routeProvider) {
    $routeProvider
    // route for the home page
        .when('/ELT-home', {
            templateUrl: 'pages/ELT/ELT-home.html',
            controller: 'mainController'
        })
        .when('/ELT-all-transactions', {
            templateUrl: 'pages/ELT/ELT-all-transactions.html',
            controller: 'ELT-all-transactionsController'
        })
        .when('/ELT-subscription-plan', {
            templateUrl: 'pages/ELT/ELT-subscription-plan.html',
            controller: 'ELT-subscription-planController'
        })
        .when('/PR-home', {
            templateUrl: 'pages/PR/PR-home.html',
            controller: 'PR-homeController'
        })
        .when('/PR-special-cases', {
            templateUrl: 'pages/PR/PR-special-cases.html',
            controller: 'PR-special-casesController'
        })
        .when('/PR-all-accessors', {
            templateUrl: 'pages/PR/PR-all-accessors.html',
            controller: 'PR-all-accessorsController'
        })
        .when('/PR-account-info', {
            templateUrl: 'pages/PR/PR-account-info.html',
            controller: 'PR-account-infoController'
        });
});
// create the controller and inject Angular's $scope
sixcloudApp.controller('mainController', function($scope) {
     $(".content-list > ul > li > .box-list").click(function(){
        $(this).toggleClass('remove');
    });
}); 
sixcloudApp.controller('ELT-all-transactionsController', function($scope) {
	$('[data-toggle="tooltip"]').tooltip();
});
sixcloudApp.controller('PR-homeController', function($scope) {
}); 
sixcloudApp.controller('ELT-subscription-planController', function($scope) {
    $('#plans.owl-carousel').owlCarousel({
    loop:true,
    margin:40,
    responsiveClass:true,
    navigation: true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:3,
            nav:false
        },
        1000:{
            items:3,
            nav:true,
            loop:false
        }
    },
    navText: ["<i class='sci-arrow-left'></i>","<i class='sci-arrow-right'></i>"]
	});
	var chart = Highcharts.chart('highchart', {
	    chart: {
	        type: 'column'
	    },
	     title: {
	        text: false
	    },
	    yAxis: {
	        visible: false
	    },
	    xAxis: {
	        categories: [ 'Sep', 'Aug', 'Jul', 'Jun' , 'May' , 'Apr']
	    },
	    plotOptions: {
	        series: {
	            allowPointSelect: true
	        }
	    },
	    series: [{
	        data: [29.9, 71.5, 106.4, 129.2 ,29.9, 71.5]
	    }]
	});
	var chart = Highcharts.chart('highchart1', {
	    chart: {
	        type: 'column'
	    },
	     title: {
	        text: false
	    },
	    yAxis: {
	        visible: false
	    },
	    xAxis: {
	        categories: [ 'Sep', 'Aug', 'Jul', 'Jun' , 'May' , 'Apr']
	    },
	    plotOptions: {
	        series: {
	            allowPointSelect: true
	        }
	    },
	    series: [{
	        data: [29.9, 71.5, 106.4, 129.2 ,29.9, 71.5]
	    }]
	});
	var chart = Highcharts.chart('highchart2', {
	    chart: {
	        type: 'column'
	    },
	     title: {
	        text: false
	    },
	    yAxis: {
	        visible: false
	    },
	    xAxis: {
	        categories: [ 'Sep', 'Aug', 'Jul', 'Jun' , 'May' , 'Apr']
	    },
	    plotOptions: {
	        series: {
	            allowPointSelect: true
	        }
	    },
	    series: [{
	        data: [29.9, 71.5, 106.4, 129.2 ,29.9, 71.5]
	    }]
	});
	var chart = Highcharts.chart('highchart3', {
	    chart: {
	        type: 'column'
	    },
	     title: {
	        text: false
	    },
	    yAxis: {
	        visible: false
	    },
	    xAxis: {
	        categories: [ 'Sep', 'Aug', 'Jul', 'Jun' , 'May' , 'Apr']
	    },
	    plotOptions: {
	        series: {
	            allowPointSelect: true
	        }
	    },
	    series: [{
	        data: [29.9, 71.5, 106.4, 129.2 ,29.9, 71.5]
	    }]
	});
	var chart = Highcharts.chart('highchart4', {
	    chart: {
	        type: 'column'
	    },
	     title: {
	        text: false
	    },
	    yAxis: {
	        visible: false
	    },
	    xAxis: {
	        categories: [ 'Sep', 'Aug', 'Jul', 'Jun' , 'May' , 'Apr']
	    },
	    plotOptions: {
	        series: {
	            allowPointSelect: true
	        }
	    },
	    series: [{
	        data: [29.9, 71.5, 106.4, 129.2 ,29.9, 71.5]
	    }]
	});
	var chart = Highcharts.chart('highchart5', {
	    chart: {
	        type: 'column'
	    },
	     title: {
	        text: false
	    },
	    yAxis: {
	        visible: false
	    },
	    xAxis: {
	        categories: [ 'Sep', 'Aug', 'Jul', 'Jun' , 'May' , 'Apr']
	    },
	    plotOptions: {
	        series: {
	            allowPointSelect: true
	        }
	    },
	    series: [{
	        data: [29.9, 71.5, 106.4, 129.2 ,29.9, 71.5]
	    }]
	});
}); 
sixcloudApp.controller('PR-all-accessorsController', function($scope) {
	  $('input[name="singledate-picker"]').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true
    });
});
sixcloudApp.controller('PR-account-infoController', function($scope) {
	  $('input[name="singledate-picker"]').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true
    });
});