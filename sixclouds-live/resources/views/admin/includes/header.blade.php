<header>
    <nav class="navbar">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="logo" href="#">
                    <img src="resources/assets/images/Logo-large.png">
                </a>
            </div>
            <div class="temp-show-header" style="" ng-if="header_menu == 'accessor'">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="manage/my-jobs">Proof Reading </a>
                    </li>
                    <li class="dropdown">
                        <a href="manage/accessors-my-account"> My Account</a>
                    </li>
                    <li class="dropdown">
                        <a href="" ng-click="prlogout()"> Log Out</a>
                    </li>
                </ul>
            </div>
            <div class="navbar-right" ng-if="header_menu == 'admin'">
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="manage/elt">Home</a></li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)">English Language Training
                                <i class="fa fa-angle-down drop-down-icon"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="manage/elt">Videos</a></li>
                                <li><a href="manage/subscription-plans">Subscription Plans</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)">Proof Reading
                                <i class="fa fa-angle-down drop-down-icon"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="manage/open-cases">Open Cases</a></li>
                                <li><a href="manage/special-cases">Special Cases</a></li>
                                <li><a href="manage/closed-cases">Closed Cases</a></li>
                                <li><a href="manage/all-accessor">All Accessors</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)">
                                [['lbl_marketplace'|translate]]
                                <i class="fa fa-angle-down drop-down-icon"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="#/all-jobs">All Jobs</a></li>
                                <li><a href="#/mp-all-designers">All Designers</a></li>
                                <li><a href="#/mp-all-contractors">All Contractors</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="after-login dropdown">
                    <div class="media">
                        <div class="media-left">
                            <div class="img">
                                <img ng-src="[[ADMINDATA.image]]">
                            </div>
                        </div>
                        <div class="media-body dropdown-toggle" type="button" data-toggle="dropdown">
                            <div class="name pull-left">
                                <b>Hi, [[ADMINDATA.first_name]] [[ADMINDATA.last_name]]</b>
                            </div>
                            <!-- <span class="caret pull-right"></span> -->
                            <span class="fa fa-angle-down pull-right drop-down-icon"></span>
                        </div>
                        <ul class="dropdown-menu">
                            <li class="temp-hide-header"><a href="manage/my-account"> My Account</a></li>
                            <li class="temp-show-header" style="display: none;"><a href="#/PR-accessor-my-account"> My Account</a></li>
                            <li class="temp-hide-header"><a href="manage/admin-team"> Admin Team</a></li>
                            <li class="temp-hide-header"><a href="#/promo-codes"> Promo Codes</a></li>
                            <li class="temp-hide-header"><a href="#"> Reports</a></li>
                            <li class="temp-show-header" ng-click="logout()"><a href="#"> Log Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</header>
