<!DOCTYPE html>
<html  ng-app="sixcloudIgniteTermsofServiceApp">
<head>
  <title>{{$title}}</title>
  <meta name="description" content="{{$meta}}">
  <meta name="robots" content="index,follow" />
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1" name="viewport"/>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="alternate" href="{{$alternate}}" hreflang="{{$alternatelang}}" />
  <link rel="canonical" href="{{$canonical}}" />
  <meta name="google-site-verification" content="F0XMjfssK8hcmTCfqJJcQFq80vAe8lHHegJBFA84XAQ" />
  <meta name="msvalidate.01" content="5789F571110EF5249F1B42276844821B" />
  <meta name="p:domain_verify" content="8475f8bbc093c663df8ead38f223dfac"/>

  <!-- Google Analytics -->
  <script>
  window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
  ga('create', 'UA-149665763-1');
  ga('send', 'pageview');
  </script>
  <script async src='https://www.google-analytics.com/analytics.js'></script>
  <!-- End Google Analytics -->

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127371237-1"></script>
  <script>
   window.dataLayer = window.dataLayer || [];
   function gtag(){dataLayer.push(arguments);}
   gtag('js', new Date());

   gtag('config', 'UA-127371237-1');
  </script>
  
  {{ HTML::script('resources/assets/js/common/jquery.min.js') }}
  <link rel="shortcut icon" type="image/x-icon" href="resources/assets/images/favicon.ico">
  <!--Animation Style-->
  {{ HTML::style('resources/assets/css/front/mp/animations.css',array('rel'=>'stylesheet')) }}

  <!--Slider Style-->
  {{ HTML::style('resources/assets/css/front/mp/swiper.min.css',array('rel'=>'stylesheet')) }}

  <!--Fonts Style-->
  {{ HTML::style('resources/assets/css/front/mp/custom-font.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/font-awesome.min.css',array('rel'=>'stylesheet')) }}


  <!--jquery.FlowupLabels Style-->
  {{ HTML::style('resources/assets/css/front/mp/jquery.FlowupLabels.css',array('rel'=>'stylesheet')) }}

  <!--bootstrap-datetimepicker Style-->
  {{ HTML::style('resources/assets/css/front/mp/bootstrap-datetimepicker.min.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/asRange.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/common/star-rating.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/bootstrap.css',array('rel'=>'stylesheet')) }}

  <!--Custom Style-->
  {{ HTML::style('resources/assets/css/front/mp/style.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/mp_custom.css',array('rel'=>'stylesheet')) }}
  
  {{ HTML::style('resources/assets/css/front/mp/responsive.css',array('rel'=>'stylesheet')) }}

  {{ HTML::script('resources/assets/js/common/angular.min.js') }}
  {{ HTML::script('resources/assets/js/common/angular-route.js') }}
  {{ HTML::script('resources/assets/js/common/angular-translate.min.js') }}
  {{ HTML::script('resources/assets/js/common/js.cookie.min.js') }}

    <!--[if lte IE 9]>
          <link href='assets/css/animations-ie-fix.css' rel='stylesheet'>
    <![endif]-->

  <!-- <script src="script.js"></script> -->
</head>
<body ng-controller="IgniteTermsofServiceController">
<div class="body-div buyer-and-seller-page">
  <!--Header section Started-->
  <header class="header-design3" data-spy="affix" data-offset-top="60">
    <div class="container">
      <nav class="navbar navbar-custom">
       <div class="container-fluid">
         <!-- Brand and toggle get grouped for better mobile display -->
         <div class="navbar-header">
           <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
             <span class="sr-only">Toggle navigation</span>
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
           </button>
           <a class="navbar-brand" href="<?=url('/')?>"><img style="height: auto;" src="resources/assets/images/logo.svg" width="170"   class="white-logo"><img style="height: auto;" src="resources/assets/images/logo.svg" width="170" class="blue-logo"></a>
         </div>

         <!-- Collect the nav links, forms, and other content for toggling -->
         <div class="collapse navbar-collapse hMenu" id="bs-example-navbar-collapse-1">
            <select ng-model="changeLangs" ng-change="changeLang(changeLangs)" class="nav navbar-nav navbar-right">
              <option value="chi">中文</option>
              <option value="en">English</option>
              <option value="ru">Русский</option>
            </select>
           <!-- <ul class="nav navbar-nav navbar-right">
              <li class="become-a-buyer buyer-button-length">
                <a href="javascript:;" ng-if="!eng" ng-click="changeLang('chi')" class="uppercase btn-medium-pink login-btn">[['lbl_lang_chng'|translate]]</a>
                <a href="javascript:;" ng-if="eng == true" ng-click="changeLang('en')" class="uppercase btn-medium-pink login-btn">[['lbl_lang_chng'|translate]]</a>  
              </li>
           </ul> -->

         </div><!-- /.navbar-collapse -->
       </div><!-- /.container-fluid -->
     </nav>

    </div>
  </header>

  <!--Header section End-->
  <div id="main">
    <section class="page-head">
      <div style="background-image:url(resources/assets/images/head-bg01.png); " class="img-div"></div>
      <div class="container">
        <h1 class="uppercase">[["lbl_sixclouds_ignite_terms_of_service" | translate]] </h1>
      </div>
    </section>
    <section class="the-company-section" id="TheCompany">
      <div class="container">
        <div class="inner-padding">
          <p>[['lbl_the_following_terms'|translate]]</p>

          <p>[['lbl_the_terms_of_service'|translate]] <a target="_blank" href="https://www.sixclouds.net/ignite">[['lbl_www_sixclouds_net'|translate]]</a> <!-- [['lbl_and'|translate]] <a target="_blank" href="http://www.sixclouds.cn/ignite">[['lbl_www_sixclouds_cn'|translate]]</a> --> [['lbl_the_site'|translate]]</p>

          <p>[['lbl_please_read_these'|translate]] <strong class="bold">[['lbl_by_using_the_site_you_are_accepting'|translate]]</strong></p>

          <p class="bold">[['lbl_by_using_the_site_you_are_accepting_warrant'|translate]]</p>

          <!-- <p class="bold">[['lbl_this_sixClouds_sixteen_website'|translate]]</p>
          
          <p class="bold">[['lbl_by_using_this_site_you_represent'|translate]]</p> -->

          <h3 class="bold">[['lbl_key_terms_to_note'|translate]]</h3>

          <p><span class="bold">[['lbl_sixclouds'|translate]][['lbl_ignite'|translate]]</span>[['lbl_means_the_features'|translate]]<a target="_blank" href="https://www.sixclouds.net/ignite">[['lbl_www_sixclouds_net'|translate]]</a> <!-- [['lbl_and'|translate]] <a target="_blank" href="http://www.sixclouds.cn/ignite">[['lbl_www_sixclouds_cn'|translate]]</a> -->[['lbl_and_any_other_platform_or_websites'|translate]]</p>

          <p><span class="bold">[['lbl_users'|translate]]</span> [['lbl_means_users_who_purchase'|translate]]</p>
          <p><span class="bold">[['lbl_non_subscriber'|translate]]</span> [['lbl_means_users_who_offer'|translate]]</p>
          <p><span class="bold">[['lbl_subscriber'|translate]]</span> [['lbl_are_creative_capabilities'|translate]]</p>
          <p><span class="bold">[['lbl_subscription_content'|translate]]</span>[['lbl_are_creative_services'|translate]]</p>
          <p><span class="bold">[['lbl_subscriber_generated_content'|translate]]</span> [['lbl_are_additional_services'|translate]]</p>
          <p><span class="bold">[['lbl_subs'|translate]]</span> [['lbl_is_where_buyers_and_sellers'|translate]]</p>
          <p><span class="bold">[['lbl_subs_term'|translate]]</span> [['lbl_means_period_of'|translate]]</p>
          
          <h3 class="bold">[['lbl_general_overview_of_terms_of_service'|translate]]</h3>
          <ul>
            <li>[['lbl_only_registered_users'|translate]]</li>
            <li>[['lbl_services_on_sixclouds_may_be'|translate]]</li>
            <li>[['lbl_payment_is_to_be_made_by'|translate]]</li>
            <li>[['lbl_seller_must_complete_the_project'|translate]]</li>
            <li>[['lbl_buyers_and_sellers_are_encouraged'|translate]]</li>
            <li>[['lbl_users_agree_to_abide_to'|translate]]</li>
              <ul>
                <li>[['lbl_illegal_or_fraudulent_services'|translate]]</li>
                <li>[['lbl_copyright_infringement'|translate]]</li>
                <li>[['lbl_publish_or_distribute'|translate]]</li>
                <li>[['lbl_intentional_copying'|translate]]</li>
                <li>[['lbl_ignite_will_not_reproduce'|translate]]</li>
                <li>[['lbl_ignite_will_not_delete_alter'|translate]]</li>
                <li>[['lbl_ignite_will_not_use_site'|translate]]</li>
              </ul>
              <li>[['lbl_users_recognizes'|translate]]</li>
              <li>[['lbl_users_agrees_applicable'|translate]]</li>
          </ul>

          <h3 class="bold">[['lbl_access_license_and_right_to_Use'|translate]]</h3>
          <ul>
            <li>[['lbl_subject_to_the_terms_of_service'|translate]]</li>
            <li>[['lbl_you_hereby_agree_to_grant_us_a_worldwide'|translate]]</li>
          </ul>

          <h3 class="bold">[['lbl_ignite_terms_termination'|translate]]</h3>
          <ul>
            <li>[['lbl_subscriber_may_subs'|translate]]</li>
            <li>[['lbl_ignite_reserves_right'|translate]]</li>
            <li>[['lbl_when_account_subs'|translate]]</li>
            <li>[['lbl_once_the_terms_of'|translate]]</li>
          </ul>

          <h3 class="bold">[['lbl_ignite_third_party'|translate]]</h3>
          <ul>
            <li>[['lbl_ignite_may_contain'|translate]]</li>
          </ul>

          <h3 class="bold">[['lbl_ignite_prop_rights'|translate]]</h3>
          <ul>
            <li>[['lbl_ignite_user_ack'|translate]]</li>
            <li>[['lbl_ignite_under_terms'|translate]]</li>
            <li>[['lbl_ignite_sixclouds_reserves'|translate]]</li>
          </ul>

          <h3 class="bold">[['lbl_ignite_general_terms'|translate]]</h3>
          <ul>
            <li>[['lbl_ignite_account_hold'|translate]]</li>
            <li>[['lbl_ignite_account_ownership'|translate]]</li>
            <li>[['lbl_ignite_changes_to_terms'|translate]]</li>
            <li>[['lbl_ignite_no_joint_venture'|translate]]</li>
            <li>[['lbl_ignite_provision_invalid'|translate]]</li>
            <li>[['lbl_ignite_faliure_of_sixclouds'|translate]]</li>
          </ul>

          <p class="bold">[['lbl_disclamer_warr'|translate]]</p>
          <p>[['lbl_content_any_service'|translate]]</p>

          <p class="bold">[['lbl_ignite_limitation'|translate]]</p>
          <p>[['lbl_ignite_affilates'|translate]]</p>
          <p class="bold">[['lbl_ignite_governing_law'|translate]]</p>
          <p>[['lbl_ignite_governed_by'|translate]]</p>
          <p>[['lbl_updated_at'|translate]]</p>
          

        </div>
      </div>
    </section>
  </div>

  <!--Footer Start-->
  <footer>
    <div class="footer-bg-repeat "></div>
    <div class="main-footer">
      <div class="container">
        <div class="row">
          <!-- <div class="col-sm-5">
            <div class="h-div">
              <div class="footer-logo"> <a href="ignite"><img src="resources/assets/images/logo.svg" width="173" height="70"></a> </div>
              <p class="l-link"><a href="#">sixclouds.com</a></p> 
              <div class="bottom-div"> <span class="copy">&copy;</span>All rights reserved </div>
            </div>
            
          </div> -->
          <div class="col-sm-4">
            <div class="h-div">
              <div class="footer-logo"> <a href="ignite"><img ng-src="resources/assets/images/logo.svg"></a> </div>
              <p class="l-link"><a class="hyper-color" target="_blank" href="<?=url('/') ?>">www.sixclouds.[[ @if(strpos(url('/'), 'sixclouds.cn') !== false ||  strpos(url('/'), 'localhost') !== false) ]]cn[[ @else ]]net[[ @endif ]]</a></p>
              <div class="bottom-div"> <span class="copy">&copy;</span>[['lbl_all_rights_reserved'|translate]] </div>
              <p>[[ @if(strpos(url('/'), 'sixclouds.cn') !== false ||  strpos(url('/'), 'localhost') !== false) ]] ICP号为：渝ICP备18001226号 [[  @endif ]]</p>
            </div>
          </div>
          <div class="col-sm-7">
            <table cellpadding="0" cellspacing="0" width="100%">
              <tr>
                <td width="50%" valign="top"><div class="h-div">
                    <h3>[['lbl_about_us'|translate]]</h3>
                    <ul>
                      <!-- <li><a target="_blank" href="<?=url('/') . '/about-us'?>">[['lbl_about_us'|translate]]</a></li> --> 
                      <li><a target="_blank" href="<?=url('/') . '/about-us/the-company'?>">[['lbl_the_company'|translate]]</a></li>
                      <li><a target="_blank" href="<?=url('/') . '/about-us/core-values'?>">[['lbl_core_values'|translate]]</a></li>
                      <li><a target="_blank" href="<?=url('/') . '/about-us/our-team'?>">[['lbl_our_team'|translate]]</a></li>
                    </ul>
                  </div>
                  </td>
                <td valign="top"><div class="h-div">
                    <h3>[['lbl_quick_links'|translate]]</h3>
                    <ul>
                      <li><a target="_blank" href="<?=url('/') . '/customer-support'?>">[["lbl_customer_support" | translate]] </a></li>
                      <li><a target="_blank" href="<?=url('/') . '/ignite-terms-of-service'?>">[["lbl_terms_service" | translate]]  </a></li>
                      <li><a target="_blank" href="<?=url('/') . '/privacy-policy'?>">[["lbl_privacy_policy" | translate]]</a></li>
                      <li><a href="<?=url('/') . '/distributor'?>">[["lbl_ignite_distributor_access" | translate]]</a></li>
                    </ul>
                  </div>
                  </td>
              </tr>
            </table>
          </div>
          
        </div>
      </div>
    </div>
  </footer>
</div>

{{ HTML::script('resources/assets/js/front/mp/popper.min.js') }}
{{ HTML::script('resources/assets/js/front/mp/bootstrap.js') }}
{{ HTML::script('resources/assets/js/front/mp/swiper.min.js') }}
{{ HTML::script('resources/assets/js/front/mp/css3-animate-it.js') }}
{{ HTML::script('resources/assets/js/front/mp/jquery.FlowupLabels.js') }}
{{ HTML::script('resources/assets/js/common/star-rating.js') }}

  <!--bootstrap-datetimepicker Start-->
{{ HTML::script('resources/assets/js/front/mp/moment.js') }}
{{ HTML::script('resources/assets/js/front/mp/bootstrap-datetimepicker.js') }}
{{ HTML::script('resources/assets/js/front/mp/jquery-asRange.js') }}

{{ HTML::script('resources/assets/js/front/controllers/IgniteTermsofServiceController.js') }}

<!-- <script type="text/javascript" src="assets/js/script.js"></script> -->
<script type="text/javascript">
  $('body').css('background','#fff');

    $(window).load(function() {
     if($(window).width() > 767) {

     } else {
       $(function(){
           var navMain = $(".hMenu");
           navMain.on("click", "a", null, function () {
               navMain.collapse('hide');
           });
       });
     }
  });

    $('.hMenu a').click(function() {
      $('.hMenu li').removeClass('active');
          var datanameC = $(this).attr('data-name');
          var headerHei = $('header').height();
         $(this).parent('li').addClass('active');
          $('html, body').animate({
              scrollTop: $("#"+datanameC).offset().top-headerHei
            }, 1000);
    });
</script>

</body>
</html>
</body>
</html>
