<!DOCTYPE html>
<html ng-app="sixcloudApp">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>[['lbl_homepage_title'|translate]]</title>
        {{ HTML::style('resources/assets/css/front/bootstrap.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/css/front/font-awesome.min.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/css/front/animate.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/css/front/style.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/plugins/owl-craousel/owl.carousel.min.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/plugins/boostrap-datepicker/css/daterangepicker.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/css/common/sweet-alert.min.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/css/common/uploadfile.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/css/front/custom.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/css/common/angular-page-loader.css',array('rel'=>'stylesheet')) }}
        {{ HTML::script('resources/assets/js/common/jquery-3.2.1.min.js') }}
        {{ HTML::script('resources/assets/js/common/angular.min.js') }}
        {{ HTML::script('resources/assets/js/common/angular-route.js') }}
        {{ HTML::script('resources/assets/js/common/angular-sanitize.min.js') }}
        {{ HTML::script('resources/assets/js/common/angular-page-loader.min.js') }}
        <link rel="shortcut icon" href="resources/assets/images/Logo-large.png"/>
        <base href="<?=url('/') . '/'?>">
        <style>
            [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
                display: none !important;
            }
        </style>
    </head>
    <body>
         <div class="loader">
            <div class="spinner">
              <div class="double-bounce1"></div>
              <div class="double-bounce2"></div>
            </div>
        </div>
        @include('front/includes.header')
    <!-- <page-loader></page-loader> -->
    <div id="main" ng-controller="MainController">
        <div ng-view></div>
    </div>
    @include('front/includes.footer')
    <a class="btn-top" style="">
        <i class="fa fa-arrow-up"></i>
    </a>
    <script>
        // set MODE for enable and disable console.log
        var project_mode = 'development'; //'production';
        var BASEURL = "<?=url('/') . '/'?>";
        var MAX_SIZE = "<?=config('constants.messages.MAX_UPLOAD_FILE_SIZE')?>";
        var protocol = "<?= empty($_SERVER['HTTPS']) ? 'http' : 'https'?>";
        var domain = "<?=$_SERVER['SERVER_NAME']?>";
        var completeUrl = protocol+'://'+domain+'/';
        var VIDEO_PLACEHOLDER = "<?=url('/') . config('constants.path.ELT_VIDOE_PLACEHOLDER')?>";
    </script>
    <!-- Core files -->
    
    {{ HTML::script('resources/assets/js/common/init.js') }}
{{ HTML::script('resources/assets/js/common/ng-img-crop.js') }}
{{ HTML::script('resources/assets/js/common/classes/crop-area-circle.js') }}
{{ HTML::script('resources/assets/js/common/classes/crop-area-square.js') }}
{{ HTML::script('resources/assets/js/common/classes/crop-area.js') }}
{{ HTML::script('resources/assets/js/common/classes/crop-canvas.js') }}
{{ HTML::script('resources/assets/js/common/classes/crop-exif.js') }}
{{ HTML::script('resources/assets/js/common/classes/crop-host.js') }}
{{ HTML::script('resources/assets/js/common/classes/crop-pubsub.js') }}
    {{ HTML::script('resources/assets/js/front/customangular.js') }}
    {{ HTML::script('resources/assets/js/front/core/bootstrap.min.js') }}
    {{ HTML::script('resources/assets/js/front/core/wow.min.js') }}
    {{ HTML::script('resources/assets/js/common/custom_alert.js') }}
    {{ HTML::script('resources/assets/js/common/jquery.validate.min.js') }}
    {{ HTML::script('resources/assets/js/common/sweetalert.min.js') }}
    {{ HTML::script('resources/assets/js/front/basic.js') }}
    {{ HTML::script('resources/assets/js/common/js.cookie.min.js') }}
    {{ HTML::script('resources/assets/js/common/lang-label.js') }}
    {{ HTML::script('resources/assets/js/common/angular-translate.min.js') }}
    <!-- Plugins files -->
    {{ HTML::script('resources/assets/plugins/owl-craousel/owl.carousel.min.js') }}
    {{ HTML::script('resources/assets/plugins/boostrap-datepicker/js/moment.min.js') }}
    {{ HTML::script('resources/assets/plugins/boostrap-datepicker/js/daterangepicker.js') }}
    <!-- Controller files -->
    {{ HTML::script('resources/assets/js/front/controllers/MainController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/HomeController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/SubscriptionPlanController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/SignupController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/EltVideoController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/EltSectionDetailController.js') }}
    {{ HTML::script('resources/assets/js/front/factory/factory.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/PRMyCasesController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/PRHomeController.js') }}
    {{ HTML::script('resources/assets/js/common/uploadfile.min.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/elt/ELTvideouploadController.js') }}
    <!-- factory file -->
    {{ HTML::script('resources/assets/js/front/factory/userfactory.js') }}
    {{ HTML::script('resources/assets/js/front/factory/eltfactory.js') }}

    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-sanitize/1.6.9/angular-sanitize.min.js">
    </script>
    <script type="text/javascript">

         $(window).on('load', function(){
             $(".loader").fadeOut('slow');
        })

        $(window).scroll(function () {
            if ($(this).scrollTop() > 200) {
                $('.btn-top').fadeIn();
            } else {
                $('.btn-top').fadeOut();
            }
        });
        $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if (scroll > 200) {
            $('header').addClass('slide');
        } else {
             $('header').removeClass('slide');
        }
        });
        $(window).scroll(function () {
            var scroll = $(window).scrollTop();
            if (scroll > 300) {
                $('header').addClass('fixed-header');
            } else {
                $('header').removeClass('fixed-header');
            }
        });
        $('.btn-top').click(function () {
            $("html, body").animate({
                scrollTop: 0
            }, 600);
            return false;
        });
        $("#main").scrollTop(900);
        $(".btn-top").css("opacity", "1");
        $(".btn-top").click(function () {
            $("#main").scrollTop();
        });
    </script>

</body>
{{ HTML::script('resources/assets/js/front/factory/factory.js') }}
{{ HTML::script('resources/assets/js/front/controllers/PRMyCasesController.js') }}
{{ HTML::script('resources/assets/js/front/controllers/PRHomeController.js') }}
{{ HTML::script('resources/assets/js/common/uploadfile.min.js') }}
</html>