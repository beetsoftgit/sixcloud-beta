<!DOCTYPE html>
<html  ng-app="sixcloudTermsofServiceApp">
<head>
  <title>{{$title}}</title>
  <meta name="description" content="{{$meta}}">
  <meta name="robots" content="index,follow" />
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1" name="viewport"/>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="alternate" href="{{$alternate}}" hreflang="{{$alternatelang}}" />
  <link rel="canonical" href="{{$canonical}}" />
  <meta name="google-site-verification" content="F0XMjfssK8hcmTCfqJJcQFq80vAe8lHHegJBFA84XAQ" />
  <meta name="msvalidate.01" content="5789F571110EF5249F1B42276844821B" />
  <meta name="p:domain_verify" content="8475f8bbc093c663df8ead38f223dfac"/>

  <!-- Google Analytics -->
  <script>
  window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
  ga('create', 'UA-149665763-1');
  ga('send', 'pageview');
  </script>
  <script async src='https://www.google-analytics.com/analytics.js'></script>
  <!-- End Google Analytics -->

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127371237-1"></script>
  <script>
   window.dataLayer = window.dataLayer || [];
   function gtag(){dataLayer.push(arguments);}
   gtag('js', new Date());

   gtag('config', 'UA-127371237-1');
  </script>
  
  {{ HTML::script('resources/assets/js/common/jquery.min.js') }}
  <link rel="shortcut icon" type="image/x-icon" href="resources/assets/images/favicon.ico">
  <!--Animation Style-->
  {{ HTML::style('resources/assets/css/front/mp/animations.css',array('rel'=>'stylesheet')) }}

  <!--Slider Style-->
  {{ HTML::style('resources/assets/css/front/mp/swiper.min.css',array('rel'=>'stylesheet')) }}

  <!--Fonts Style-->
  {{ HTML::style('resources/assets/css/front/mp/custom-font.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/font-awesome.min.css',array('rel'=>'stylesheet')) }}


  <!--jquery.FlowupLabels Style-->
  {{ HTML::style('resources/assets/css/front/mp/jquery.FlowupLabels.css',array('rel'=>'stylesheet')) }}

  <!--bootstrap-datetimepicker Style-->
  {{ HTML::style('resources/assets/css/front/mp/bootstrap-datetimepicker.min.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/asRange.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/common/star-rating.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/bootstrap.css',array('rel'=>'stylesheet')) }}

  <!--Custom Style-->
  {{ HTML::style('resources/assets/css/front/mp/style.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/mp_custom.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/responsive.css',array('rel'=>'stylesheet')) }}

  {{ HTML::script('resources/assets/js/common/angular.min.js') }}
  {{ HTML::script('resources/assets/js/common/angular-route.js') }}
  {{ HTML::script('resources/assets/js/common/angular-translate.min.js') }}
  {{ HTML::script('resources/assets/js/common/js.cookie.min.js') }}

    <!--[if lte IE 9]>
          <link href='assets/css/animations-ie-fix.css' rel='stylesheet'>
    <![endif]-->

  <!-- <script src="script.js"></script> -->
</head>
<body ng-controller="TermsofServiceController">
<div class="body-div buyer-and-seller-page">
  <!--Header section Started-->
  <header class="header-design3" data-spy="affix" data-offset-top="60">
    <div class="container">
      <nav class="navbar navbar-custom">
       <div class="container-fluid">
         <!-- Brand and toggle get grouped for better mobile display -->
         <div class="navbar-header">
           <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
             <span class="sr-only">Toggle navigation</span>
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
           </button>
           <a class="navbar-brand" href="<?=url('/sixteen')?>"><img style="height: auto;" src="resources/assets/images/logo-white.png" width="170"   class="white-logo"><img style="height: auto;" src="resources/assets/images/logo.png" width="170" class="blue-logo"></a>
         </div>

         <!-- Collect the nav links, forms, and other content for toggling -->
         <div class="collapse navbar-collapse hMenu" id="bs-example-navbar-collapse-1">
          
           <!-- <ul class="nav navbar-nav navbar-right">
              <li class="become-a-buyer buyer-button-length">
                <a href="javascript:;" ng-if="!eng" ng-click="changeLang('chi')" class="uppercase login-btn">[['lbl_lang_chng'|translate]]</a>
                <a href="javascript:;" ng-if="eng == true" ng-click="changeLang('en')" class="uppercase login-btn">[['lbl_lang_chng'|translate]]</a>  
              </li>
           </ul> -->
           <select ng-model="changeLangs" ng-change="changeLang(changeLangs)" class="nav navbar-nav navbar-right">
              <option value="chi">中文</option>
              <option value="en">English</option>
              <option value="ru">Русский</option>
            </select>
         </div><!-- /.navbar-collapse -->
       </div><!-- /.container-fluid -->
     </nav>

    </div>
  </header>

  <!--Header section End-->
  <div id="main">
    <section class="page-head">
      <div style="background-image:url(resources/assets/images/head-bg01.png); " class="img-div"></div>
      <div class="container">
        <h1 class="uppercase">[["lbl_sixclouds_sixteen_terms_of_service" | translate]] </h1>
      </div>
    </section>
    <section class="the-company-section" id="TheCompany">
      <div class="container">
        <div class="inner-padding">
          <p>[['lbl_the_following_terms'|translate]]</p>

          <p>[['lbl_the_terms_of_service'|translate]] <a target="_blank" href="https://www.sixclouds.net/sixteen">[['lbl_www_sixclouds_net'|translate]]</a> [['lbl_and'|translate]] <a target="_blank" href="http://www.sixclouds.cn/beta/sixteen">[['lbl_www_sixclouds_cn'|translate]]</a> [['lbl_the_site'|translate]]</p>

          <p>[['lbl_please_read_these'|translate]] <strong class="bold">[['lbl_by_using_the_site_you_are_accepting'|translate]]</strong></p>

          <p class="bold">[['lbl_by_using_the_site_you_are_accepting'|translate]]</p>

          <p class="bold">[['lbl_this_sixClouds_sixteen_website'|translate]]</p>
          
          <p class="bold">[['lbl_by_using_this_site_you_represent'|translate]]</p>

          <h3 class="bold">[['lbl_key_terms_to_note'|translate]]</h3>

          <p><span class="bold">[['lbl_sixclouds'|translate]] [['lbl_sixteen'|translate]]</span> [['lbl_means_the_features'|translate]] <a target="_blank" href="https://www.sixclouds.net/sixteen">[['lbl_www_sixclouds_net'|translate]]</a> [['lbl_and'|translate]] <a target="_blank" href="http://www.sixclouds.cn/beta/sixteen">[['lbl_www_sixclouds_cn'|translate]]</a> [['lbl_and_any_other_platform_or_websites'|translate]]</p>

          <p><span class="bold">[['lbl_buyers'|translate]]</span> [['lbl_means_users_who_purchase'|translate]]</p>
          <p><span class="bold">[['lbl_sellers'|translate]]</span> [['lbl_means_users_who_offer'|translate]]</p>
          <p><span class="bold">[['lbl_services'|translate]]</span> [['lbl_are_creative_capabilities'|translate]]</p>
          <p><span class="bold">[['lbl_projects'|translate]]</span>[['lbl_are_creative_services'|translate]]</p>
          <p><span class="bold">[['lbl_project_extras'|translate]]</span> [['lbl_are_additional_services'|translate]]</p>
          <p><span class="bold">[['lbl_project_page'|translate]]</span> [['lbl_is_where_buyers_and_sellers'|translate]]</p>
          <p ng-if="changeLangs=='ru'"><span class="bold">[['lbl_subscription_term'|translate]]</span> [['lbl_means_the_period'|translate]]</p>
          
          <h3 class="bold">[['lbl_general_overview_of_terms_of_service'|translate]]</h3>
          <ul ng-if="lbl_only_registered_users != ''||lbl_services_on_sixclouds_may_be!=''||lbl_payment_is_to_be_made_by!=''||lbl_seller_must_complete_the_project!=''||lbl_buyers_and_sellers_are_encouraged!=''|| lbl_users_agree_to_abide_to!='' lbl_sixclouds_retains_the_right!='' lbl_The_privacy_policy_is_a_part!=''">
            <li>[['lbl_only_registered_users'|translate]]</li>
            <li>[['lbl_services_on_sixclouds_may_be'|translate]]</li>
            <li>[['lbl_payment_is_to_be_made_by'|translate]]</li>
            <li>[['lbl_seller_must_complete_the_project'|translate]]</li>
            <li>[['lbl_buyers_and_sellers_are_encouraged'|translate]]</li>
            <li>[['lbl_users_agree_to_abide_to'|translate]]</li>
            <li>[['lbl_sixclouds_retains_the_right'|translate]]</li>
            <li>[['lbl_The_privacy_policy_is_a_part'|translate]]</li>
          </ul>

          <h3 class="bold">[['lbl_access_license_and_right_to_Use'|translate]]</h3>
          <ul>
            <li>[['lbl_subject_to_the_terms_of_service'|translate]]</li>
            <li>[['lbl_you_hereby_agree_to_grant_us_a_worldwide'|translate]]</li>
            <li>[['lbl_our_site_may_display_advertisements'|translate]]</li>
            <li>[['lbl_except_as_otherwise_agreed'|translate]]</li>
            <li>[['lbl_you_may_not_sell'|translate]]</li>
            <li ng-if="changeLangs!='ru'">[['lbl_any_use_of_the_contents_and_features'|translate]]</li>
            <li ng-if="changeLangs=='ru'">[['lbl_the_user_promises'|translate]]
              <ul>
                <li>[['lbl_the_user_promises_sub_1'|translate]]</li>
                <li>[['lbl_the_user_promises_sub_2'|translate]]</li>
                <li>[['lbl_the_user_promises_sub_3'|translate]]</li>
                <li>[['lbl_the_user_promises_sub_4'|translate]]</li>
                <li>[['lbl_the_user_promises_sub_5'|translate]]</li>
                <li>[['lbl_the_user_promises_sub_6'|translate]]</li>
                <li>[['lbl_the_user_promises_sub_7'|translate]]</li>
              </ul>
            </li>
            <li>[['lbl_we_reserve_all_rights'|translate]]</li>
            <li>[['lbl_The_licenses_granted_by_sixclouds'|translate]]</li>
          </ul>

          <p>[['lbl_if_you_are_a'|translate]] <span class="bold">[['lbl_seller'|translate]]</span> [['lbl_please_read_and_understand'|translate]]</p>
          <h3 class="bold" ng-if="changeLangs!='ru'">[['lbl_general'|translate]]</h3>
          <ul ng-if="changeLangs!='ru'">
            <li>[['lbl_you_will_offer_your'|translate]]</li>
            <li>[['lbl_you_will_hereby_agree'|translate]]</li>
            <li>[['lbl_you_may_offer_project_extras'|translate]]</li>
            <li>[['lbl_once_payment_is_confirmed'|translate]]</li>
            <li>[['lbl_you_must_deliver_all_completed_work'|translate]]</li>
            <li>[['lbl_you_must_not_abuse_the_delivery_of_project'|translate]]</li>
            <li>[['lbl_a_project_is_considered_complete'|translate]]</li>
            <li>[['lbl_a_project_will_be_automatically_marked_as_complete'|translate]]</li>
            <li>[['lbl_when_a_project_is_marked_as_complete'|translate]]</li>
            <li>[['lbl_sixclouds_will_deduct'|translate]]</li>
            <li>[['lbl_you_are_entitled_to_withdraw'|translate]]</li>
            <li>[['lbl_your_status_levels_are_calculated'|translate]]</li>
          </ul>

          <h3 class="bold" ng-if="changeLangs=='ru'">[['lbl_subscription_fee_and_payment'|translate]]</h3>
          <ul ng-if="changeLangs=='ru'">
            <li>[['lbl_subscription_1'|translate]]</li>
            <li>[['lbl_subscription_2'|translate]]</li>
          </ul>

          <h3 class="bold" ng-if="changeLangs!='ru'">[['lbl_seller_services_and_profiles'|translate]]</h3>
          <ul ng-if="changeLangs!='ru'">
            <li>[['lbl_you_could_post_services_and_upload'|translate]]</li>
            <li>[['lbl_services_offered_and_created'|translate]]</li>
            <li>[['lbl_you_may_be_removed_by_sixclouds'|translate]]</li>
            <ul >
              <li>[['lbl_illegal_or_fraudulent_services'|translate]]</li>
              <li>[['lbl_copyright_infringement'|translate]]</li>
              <li>[['lbl_publish_or_distribute'|translate]]</li>
              <li>[['lbl_intentional_copying'|translate]]</li>
              <li>[['lbl_services_and_profiles_which_is_misleading_to_buyers'|translate]]</li>
              <li>[['lbl_reselling_of_regulated_content'|translate]]</li>
            </ul>
            <li>[['lbl_your_account_will_be_suspended_until_further_notice'|translate]]</li>
            <li>[['lbl_your_services_and_profiles_may_be_removed'|translate]]</li>
            <li>[['lbl_you_are_required_to_publish_an_appropriate_service_image'|translate]]</li>
            <li>[['lbl_you_shall_not_include_any_statement_on_the_services'|translate]]</li>
          </ul>

          <h3 class="bold" ng-if="changeLangs=='ru'">[['lbl_term_and_termination'|translate]]</h3>
          <ul ng-if="changeLangs=='ru'">
            <li>[['lbl_terms_1'|translate]]</li>
            <li>[['lbl_terms_2'|translate]]</li>
            <li>[['lbl_terms_3'|translate]]</li>
            <li>[['lbl_terms_4'|translate]]</li>
          </ul>


          <h3 class="bold" ng-if="changeLangs!='ru'">[['lbl_eligible_services'|translate]]</h3>
          <ul ng-if="changeLangs!='ru'">
            <li>[['lbl_you_are_entitled_to_price_your_services'|translate]]</li>
            <li>[['lbl_you_may_offer_project_extras_in_addition'|translate]]</li>
            <li>[['lbl_services_offered_through_project_extras'|translate]]</li>
            <li>[['lbl_project_extras_may_cover_different_categories_of_services'|translate]]</li>
            <li>[['lbl_you_have_the_option_to_extend_the_duration_of_a_project'|translate]]</li>
            <li>[['lbl_you_can_request_an_extension_of_submission_deadline'|translate]]</li>
            <li>[['lbl_you_may_also_request_for_a_mutual_cancellation'|translate]]</li>
            <li>[['lbl_any_dispute_arising_from_cancellation'|translate]]</li>
          </ul>

          <h3 class="bold" ng-if="changeLangs=='ru'">[['lbl_third_party'|translate]]</h3>
          <ul ng-if="changeLangs=='ru'">
            <li>[['lbl_third_party_text'|translate]]</li>
          </ul>

          <h3 class="bold" ng-if="changeLangs=='ru'">[['lbl_property_rights'|translate]]</h3>
          <ul ng-if="changeLangs=='ru'">
            <li>[['lbl_property_rights_1'|translate]]</li>
            <li>[['lbl_property_rights_2'|translate]]</li>
            <li>[['lbl_property_rights_3'|translate]]</li>
          </ul>

          <h3 class="bold" ng-if="changeLangs!='ru'">[['lbl_seller_status'|translate]]</h3>
          <p ng-if="changeLangs!='ru'">[['lbl_sixclouds_sixteen_is_designed_to_help'|translate]]</p>
          <ul ng-if="changeLangs!='ru'">
            <li>[['lbl_you_can_gain_account_status_based_on_your_project_performance'|translate]]</li>
            <li>[['lbl_ratings_will_be_given_by_buyers'|translate]]</li>
            <li>[['lbl_rating_system_will_also_take_into_consideration'|translate]]</li>
            <li>[['lbl_progression_in_status_are_updated_periodically'|translate]]</li>
            <li>[['lbl_new_sellers_start_at_basic_level'|translate]]</li>
            <li>[['lbl_each_higher_level_earns_sellers_additional_albums'|translate]]</li>
            <li>[['lbl_new_additional_benefits_features_may_be_introduced_periodically'|translate]]</li>
            <li>[['lbl_you_will_risk_losing_your_status_level'|translate]]</li>
            <ul>
              <li>[['lbl_you_are_unable_to_maintain_high_quality_service'|translate]]</li>
              <li>[['lbl_there_is_a_severe_drop_in_your_services_ratings'|translate]]</li>
              <li>[['lbl_failure_to_deliver_and_complete_projects_on_time'|translate]]</li>
              <li>[['lbl_breach_of_any_of_the_terms_contained_herein'|translate]]</li>
            </ul>
          </ul>

          <h3 class="bold" ng-if="changeLangs!='ru'">[['lbl_seller_status_levels'|translate]]</h3>
          <table border="1" ng-if="changeLangs!='ru'">
            <tr>
              <th>[['lbl_status'|translate]]</th>
              <th>[['lbl_project_rating'|translate]]</th>
              <th>[['lbl_project_completion'|translate]]</th>
              <th>[['lbl_total_albums'|translate]]</th>
            </tr>
            <tr>
              <th>[['lbl_basic'|translate]]</th>
              <td>-</td>
              <td>-</td>
              <td>6</td>
            </tr>

            <tr>
              <th>[['lbl_level_1'|translate]]</th>
              <td>3.5</td>
              <td>80%</td>
              <td>10</td>
            </tr>
            <tr>
              <th>[['lbl_level_2'|translate]]</th>
              <td>4.2</td>
              <td>85%</td>
              <td>26</td>
            </tr>
            <tr>
              <th>[['lbl_level_3'|translate]]</th>
              <td>4.5</td>
              <td>95%</td>
              <td>50</td>
            </tr>
          </table>

          <h3 class="bold" ng-if="changeLangs!='ru'">[['lbl_seller_profiles'|translate]]</h3>
          <ul ng-if="changeLangs!='ru'">
            <li>[['lbl_you_will_be_granted_access_to_features'|translate]]</li>
            <li>[['lbl_you_may_showcase_and_promote_your_services'|translate]]</li>
            <li>[['lbl_project_milestones'|translate]]</li>
              <ul>
                <li>[['lbl_you_may_request_milestone_payments'|translate]]</li>
                <li>[['lbl_each_milestone_is_paid_and_delivered_separately'|translate]]</li>
              </ul>
            <li>[['lbl_you_will_be_given_access_to_our_database_of_design'|translate]]</li>
            <li>[['lbl_you_may_download_a_fixed_number_of_designs'|translate]]</li>
          </ul>
          
          <h3 class="bold" ng-if="changeLangs!='ru'">[['lbl_revenue_withdrawal'|translate]]</h3>
          <ul ng-if="changeLangs!='ru'">
            <li>[['lbl_you_may_withdraw_revenue_as_obtained'|translate]]</li>
            <li>[['lbl_you_may_withdraw_your_project_revenue'|translate]]</li>
            <li>[['lbl_Any_withdrawal_can_only_be_made_through_your_registered_account'|translate]]<span class="bold">[['lbl_paypal_alipay_wechatpay_bank_transfer'|translate]]</span></li>
            <li class="bold">[['lbl_prevailing_and_additional_fees'|translate]]</li>
            <li class="bold">[['lbl_accuracy_of_the_financial_information'|translate]]</li>
            <li>[['lbl_your_sixClouds_account_may_be_associated_with_only_one_account'|translate]]</li>
            <li>[['lbl_withdrawals_can_only_be_made_from_the_amount_available'|translate]]</li>
            <li>[['lbl_withdrawals_are_final'|translate]]</li>
            <li class="bold">[['lbl_you_are_liable_for_all_applicable_taxation'|translate]]</li>
          </ul>

          <p ng-if="changeLangs!='ru'">[['lbl_if_you_are_a'|translate]]<span class="bold">[['lbl_buyer'|translate]]</span>[['lbl_please_read_and_understand'|translate]]</p>
          <h3 class="bold" ng-if="changeLangs!='ru'">[['lbl_general'|translate]]</h3>
          <ul ng-if="changeLangs!='ru'">
            <li>[['lbl_you_shall_not_offer_any_direct_payments_to_your_seller'|translate]]</li>
            <li>[['lbl_you_may_engage_a_seller_for_a_project_large'|translate]]</li>
            <li>[['lbl_projects_submitted_must_be_an_available'|translate]]</li>
            <li>[['lbl_you_are_requested_to_complete_and_submit_a_rating_and_review'|translate]]</li>
            <li>[['lbl_you_shall_communicate_any_concerns'|translate]]</li>
            <li>[['lbl_upon_payment_for_the_project'|translate]]</li>
            <li>[['lbl_all_transfer_and_assignment_of_intellectual'|translate]]</li>
            <li>[['lbl_you_hereby_agree_to_grant_us_a_worldwide'|translate]]</li>
          </ul>

          <h3 class="bold" ng-if="changeLangs!='ru'">[['lbl_project_payment'|translate]]</h3>
          <ul ng-if="changeLangs!='ru'">
            <li>[['lbl_you_may_engage_a_seller_for_a_project'|translate]]</li>
            <li>[['lbl_you_may_post_a_new_project'|translate]]</li>
            <li>[['lbl_when_you_submit_a_project'|translate]]</li>
            <li>[['lbl_upon_Sellers_acceptance_of_the_invitation'|translate]]</li>
            <li>[['lbl_processing_fees_will_be_added_at_the_time_of_purchase'|translate]]</li>
            <li class="bold">[['lbl_processing_fee_rates_are'|translate]]</li>
            <li class="bold">[['lbl_you_will_bear_the_additional_fees'|translate]]</li>
            <li class="bold">[['lbl_in_addition_to_the_project_price'|translate]]</li>
            <li>[['lbl_payments_made_using_your_account_balance'|translate]]</li>
            <li>[['lbl_Any_available_funds_in_your_account_balance'|translate]]</li>
            <li>[['lbl_subject_to_your_agreement'|translate]]</li>
            <li class="bold">[['lbl_in_the_event_that_you_are_requested_to_make_any_payment'|translate]]</li>
            <li>[['lbl_We_do_not_collect_any_credit_information_in_order'|translate]]</li>
          </ul>

          <p ng-if="changeLangs!='ru'">[['lbl_applicable_to_both'|translate]] <span class="bold">[['lbl_buyer'|translate]]</span> [['lbl_and'|translate]] <span class="bold">[['lbl_seller'|translate]]</span> [['lbl_please_read_and_understand'|translate]]</p>
          <h3 class="bold" ng-if="changeLangs!='ru'">[['lbl_project_management'|translate]]</h3>
          <ul ng-if="changeLangs!='ru'">
            <li>[['lbl_when_a_buyer_submits_a_project_invitation'|translate]]</li>
            <li>[['lbl_sellers_are_required_to_accept_or_reject'|translate]]</li>
            <li>[['lbl_A_buyer_must_make_payment_upon_project_acceptance_by_seller'|translate]]</li>
            <li>[['lbl_should_a_project_invitation_be_rejected'|translate]]</li>
            <li>[['lbl_all_communications_should_be_conducted_on_the_project_page'|translate]]</li>
            <li>[['lbl_sellers_are_required_to_meet_the_delivery_timelines'|translate]]</li>
            <li>[['lbl_sellers_must_deliver_all_completed_work'|translate]]</li>
            <li>[['lbl_sellers_must_remove_all_copies_of_completed_work'|translate]]</li>
            <li>[['lbl_all_copies_of_completed_files'|translate]]</li>
            <li>[['lbl_both_the_buyer_and_seller_are_responsible_for_scanning'|translate]]</li>
            <li>[['lbl_buyers_may_request_for_revisions_to_the_delivered_work'|translate]]</li>
            <li>[['lbl_buyer_is_not_allowed_to_request_additional_services'|translate]]</li>
            <li>[['lbl_project_samples_may_be_displayed_as_seller_portfolio'|translate]]</li>
          </ul>

          <h3 class="bold" ng-if="changeLangs!='ru'">[['lbl_reviews'|translate]]</h3>
          <ul ng-if="changeLangs!='ru'">
            <li>[['lbl_reviews_provided_by_a_buyer_after_project_completion'|translate]]</li>
            <li>[['lbl_buyers_should_communicate_any_concerns'|translate]]</li>
            <li>[['lbl_to_prevent_any_misuse_of_the_rating_system'|translate]]</li>
            <li>[['lbl_withholding_the_delivery_of_services'|translate]]</li>
            <li>[['lbl_a_buyer_has_3_days_to_complete'|translate]]</li>
          </ul>

          <h3 class="bold" ng-if="changeLangs!='ru'">[['lbl_project_cancellations'|translate]]</h3>
          <p ng-if="changeLangs!='ru'">[['lbl_buyers_and_sellers_are_encouraged'|translate]]</p>
          <ul ng-if="changeLangs!='ru'">
            <li class="bold">[['lbl_you_agree_to_abide_to_all_arbitration'|translate]]</li>
            <li>[['lbl_project_cancellations_can_be_performed'|translate]]</li>
            <li>[['lbl_filing_a_transaction_dispute'|translate]]</li>
            <li>[['lbl_sixClouds_reserves_the_rights_to_cancel_projects'|translate]]</li>
            <li>[['lbl_if_a_project_is_cancelled'|translate]]</li>
            <li>[['lbl_requests_to_cancel_a_project_will_be_assessed'|translate]]</li>
            <li>[['lbl_projects_marked_as_complete_may_not_be_cancelled'|translate]]</li>
            <li>[['lbl_if_the_services_rendered_conform'|translate]]</li>
            <li>[['lbl_a_buyer_should_use_the_project_page'|translate]]</li>
            <li>[['lbl_any_non_permitted_use_of_the_sixclouds'|translate]]</li>
            <li>[['lbl_sixclouds_customer_support_holds'|translate]]</li>
            <li><span class="bold">[['lbl_active_projects'|translate]]</span> [['lbl_after_a_seller_accepts_a_project_invitation'|translate]]</li>
            <ul>
              <li>[['lbl_a_seller_is_late'|translate]] <span class="bold">[['lbl_and'|translate]]</span> [['lbl_unresponsive_for_more_than_24_hours'|translate]]</li>
              <li>[['lbl_users_are_abusive_towards_the_other_party'|translate]]</li>
              <li>[['lbl_users_supplied_or_included_copyright'|translate]]</li>
              <li>[['lbl_the_user_is_no_longer_an_active_sixclouds_user'|translate]]</li>
            </ul>

            <li><span class="bold">[['lbl_delivered_projects'|translate]]</span> [['lbl_after_a_seller_delivers_work_on_the_project'|translate]]</li>
            <ul>
              <li>[['lbl_A_seller_extends_the_delivery_due_date'|translate]]</li>
              <li>[['lbl_multiple_reported_offenses'|translate]]</li>
              <li>[['lbl_a_seller_delivers_no_files'|translate]]</li>
              <ul>
                <li>[['lbl_note_subjectivity_of_the_materials'|translate]]</li>
              </ul>
              <li>[['lbl_a_seller_requests_additional_payment'|translate]]</li>
              <li>[['lbl_a_seller_is_withholding_the_final_delivery'|translate]]</li>
              <li>[['lbl_a_buyer_who_abuses_the_request_revisions'|translate]]</li>
              <li>[['lbl_a_buyer_who_threatens_to_submit_a_poor_rating'|translate]]</li>
            </ul>

            <li><span class="bold">[['lbl_completed_projects'|translate]]</span> [['lbl_after_the_project_is_marked_as_complete_by_buyer'|translate]]</li>
            <ul>
              <li>[['lbl_users_who_have_been_reported_to_use_copyright'|translate]]</li>
              <li>[['lbl_sixclouds_customer_support_will_review_cases'|translate]]</li>
            </ul>

            <li>[['lbl_a_seller_may_request_for_a_mutual_cancellation'|translate]]</li>
            <li>[['lbl_should_a_cuyer_disagree_to_cutual_cancellation'|translate]]</li>
            <li>[['lbl_a_seller_who_initiates_forced_cancellations'|translate]]</li>
            <li>[['lbl_a_buyer_will_be_prompted_to_select_another'|translate]]</li>
            <li>[['lbl_a_buyer_can_engage_a_replacement_seller'|translate]]</li>
            <li>[['lbl_a_buyer_also_has_the_option_to_request'|translate]]</li>
            <li>[['lbl_for_projects_with_milestone_payments'|translate]]</li>
          </ul>

          <h3 class="bold" ng-if="changeLangs!='ru'">[['lbl_refunds_policy'|translate]]</h3>
          <ul ng-if="changeLangs!='ru'">
            <li>[['lbl_sixclouds_does_not_automatically_refund_payments'|translate]]</li>
            <li>[['lbl_funds_from_project_cancellations_are_refunded'|translate]]</li>
            <li>[['lbl_buyers_also_have_the_option_to_request_for_refund'|translate]]</li>
            <li>[['lbl_deposit_refunds_when_available_from_the_payment_provider'|translate]]</li>
            <li>[['lbl_such_funds_may_be_subject_to_and_additional_fee'|translate]]</li>
          </ul>

          <h3 class="bold" ng-if="changeLangs!='ru'">[['lbl_user_conduct_and_rotection'|translate]]</h3>
          <ul ng-if="changeLangs!='ru'">
            <li>[['lbl_users_of_sixclouds_are_expected_to_adhere'|translate]]</li>
            <li>[['lbl_to_report_a_violation_of_our_terms_of_service'|translate]]</li>
          </ul>

          <h3 class="bold" ng-if="changeLangs!='ru'">[['lbl_basics'|translate]]</h3>
          <ul ng-if="changeLangs!='ru'">
            <li>[['lbl_to_protect_user_privacy'|translate]]</li>
            <li>[['lbl_any_necessary_exchange_of_personal_information'|translate]]</li>
            <li>[['lbl_all_exchange_of_information_and_file'|translate]]</li>
            <li>[['lbl_rude_abusive_improper_language'|translate]]</li>
            <li>[['lbl_sixclouds_sixteen_site_is_open_to_everyone'|translate]]</li>
          </ul>

          <h3 class="bold" ng-if="changeLangs!='ru'">[['lbl_projects'|translate]]</h3>
          <ul ng-if="changeLangs!='ru'">
            <li>[['lbl_users_who_initiate_projects_with_competing_sellers'|translate]]</li>
            <li>[['lbl_users_are_refrained_from_spamming'|translate]]</li>
            <li>[['lbl_sixclouds_will_review_cases_of_payment'|translate]]</li>
          </ul>

          <h3 class="bold" ng-if="changeLangs!='ru'">[['lbl_reporting_violations'|translate]]</h3>
          <p ng-if="changeLangs!='ru'">[['lbl_users_who_come_across_any_content_that_may_violate'|translate]]</p>

          <h3 class="bold" ng-if="changeLangs!='ru'">[['lbl_violations'|translate]]</h3>
          <p ng-if="changeLangs!='ru'">[['lbl_users_may_receive_a_warning_for_violations'|translate]]</p>

          <h3 class="bold" ng-if="changeLangs!='ru'">[['lbl_non_permitted_usage'|translate]]</h3>
          <p ng-if="changeLangs!='ru'"><span class="bold">[['lbl_adult_services'|translate]] - </span> [['lbl_exchange_of_adult_oriented'|translate]]</p>
          <p ng-if="changeLangs!='ru'"><span class="bold">[['lbl_inappropriate_behaviour_language'|translate]] - </span> [['lbl_communication_on_all_sixclouds'|translate]]</p>
          <p ng-if="changeLangs!='ru'"><span class="bold">[['lbl_phishing_and_spam'|translate]] - </span> [['lbl_any_attempts_to_publish'|translate]]</p>
          <p ng-if="changeLangs!='ru'"><span class="bold">[['lbl_privacy_identity'|translate]] - </span> [['lbl_users_may_not_publish'|translate]]</p>
          <p ng-if="changeLangs!='ru'"><span class="bold">[['lbl_intellectual_property_claims'|translate]] - </span> [['lbl_sixclouds_treats_seriously'|translate]]</p>
          <p ng-if="changeLangs!='ru'"><span class="bold">[['lbl_fraud_unlawful_use'|translate]] - </span> [['lbl_you_may_not_use_any_sixclouds_sites'|translate]]</p>

          <h3 class="bold" ng-if="changeLangs!='ru'">[['lbl_abuse_and_spam'|translate]]</h3>
          <p ng-if="changeLangs!='ru'"><span class="bold">[['lbl_multiple_accounts'|translate]] - </span> [['lbl_to_prevent_fraud_and_abuse'|translate]]</p>
          <p ng-if="changeLangs!='ru'"><span class="bold">[['lbl_targeted_abuse'|translate]] - </span> [['lbl_sixclouds_sixteen_does_not_tolerate'|translate]]</p>
          <p ng-if="changeLangs!='ru'"><span class="bold">[['lbl_buying_and_or_selling_accounts'|translate]] - </span> [['lbl_users_may_not_buy_or_sell'|translate]]</p>

          <h3 class="bold">[['lbl_general_terms'|translate]]</h3>
          <ul>
            <li>[['lbl_sixclouds_reserves_the_right_to_put'|translate]]</li>
            <li>[['lbl_users_with_disabled_accounts'|translate]]</li>
            <li>[['lbl_users_who_have_violated'|translate]]</li>
            <li>[['lbl_users_must_be_able_to_verify_their_account'|translate]]</li>
            <li>[['lbl_sellers_will_be_able_to_withdraw'|translate]]</li>
            <li>[['lbl_disputes_should_be_handled_using_the_project'|translate]]</li>
            <li ng-if="changeLangs!='ru'">[['lbl_users_agree_to_abide_to_all_arbitration'|translate]]</li>
            <li ng-if="changeLangs!='ru'">[['lbl_sixclouds_may_make_changes_to_its_terms_of_service'|translate]]</li>
            <li ng-if="changeLangs!='ru'">[['lbl_users_understand_and_agree'|translate]]</li>
            <li ng-if="changeLangs!='ru'">[['lbl_no_joint_venture'|translate]]</li>
            <li ng-if="changeLangs!='ru'">[['lbl_if_any_provision_of_the_terms_of_service'|translate]]</li>
            <li ng-if="changeLangs!='ru'">[['lbl_the_failure_of_sixclouds'|translate]]</li>
          </ul>

          <h3 class="bold" ng-if="changeLangs!='ru'">[['lbl_user_generated_content'|translate]]</h3>
          <p ng-if="changeLangs!='ru'">[['lbl_user_generated_content_refers'|translate]]</p>
          <p ng-if="changeLangs!='ru'">[['lbl_furthermore_sixclouds_is_not_responsible'|translate]]</p>
          <p ng-if="changeLangs!='ru'">[['lbl_by_offering_a_service_the_seller_undertakes'|translate]]</p>

          <h3 class="bold" ng-if="changeLangs!='ru'">[['lbl_ownership_of_rights'|translate]]</h3>
          <p ng-if="changeLangs!='ru'">[['lbl_unless_clearly_stated_otherwise'|translate]]</p>
          <p ng-if="changeLangs!='ru'">[['lbl_the_delivered_work_shall_be_considered'|translate]]</p>
          <p ng-if="changeLangs!='ru'">[['lbl_sellers_further_confirm_that_whatever'|translate]]</p>
          <p ng-if="changeLangs!='ru'">[['lbl_furthermore_both_buyers_and_sellers_agree'|translate]]</p>

          <h3 class="bold">[['lbl_disclaimer_of_warranties'|translate]]</h3>
          <p ng-if="changeLangs!='ru'">[['lbl_use_of_the_sixclouds_sixteen_site'|translate]]</p>
          <p>[['lbl_the_foregoing_does_not_affect_any_warranties'|translate]]</p>

          <h3 class="bold">[['lbl_limitation_on_liability'|translate]]</h3>
          <p>[['lbl_in_no_event_will_sixclouds'|translate]]</p>
          <p>[['lbl_the_foregoing_does_not_affect_any_liability'|translate]]</p>

          <h3 class="bold">[['lbl_governing_law'|translate]]</h3>
          <p>[['lbl_this_terms_of_service_shall_be_governed_by'|translate]]</p>
          <p>[['lbl_updated_at'|translate]]</p>
          

        </div>
      </div>
    </section>
  </div>

  <!--Footer Start-->
  <footer class="animatedParent animateOnce">
    <div class="footer-bg-repeat animated fadeIn slowest"></div>
    <div class="main-footer animated fadeIn slowest">
      <div class="container">
        <div class="row">
          <div class="col-sm-4">
            <!-- <div class="h-div">
              <div class="footer-logo"> <a href="<?=url('/')?>"><img src="resources/assets/images/logo.png"></a> </div>
              <div class="bottom-div"> <span class="copy">&copy;</span><a href="javascript:;">[['lbl_sixclouds' | translate]]</a> [['lbl_all_rights_reserved' | translate]] </div>
              <p>[[ @if(strpos(url('/'), 'sixclouds.cn') !== false ||  strpos(url('/'), 'localhost') !== false) ]] ICP号为：渝ICP备18001226号 [[  @endif ]]</p>
            </div> -->
            <div class="h-div bs-copy-right">
              <div class="footer-logo"> <a href="sixteen"><img src="resources/assets/images/logo.png"></a> </div>
              <p><a target="_blank" href="<?=url('/')?>">www.sixclouds.[[ @if(strpos(url('/'), 'sixclouds.cn') !== false ||  strpos(url('/'), 'localhost') !== false) ]]cn[[ @else ]]net[[ @endif ]]</a>
              <div class="bottom-div"> <span class="copy">&copy;</span>[['lbl_allrights'|translate]]</div>
              <p>[[ @if(strpos(url('/'), 'sixclouds.cn') !== false ||  strpos(url('/'), 'localhost') !== false) ]] ICP号为：渝ICP备18001226号 [[  @endif ]]</p>
            </div>

          </div>
          <div class="col-sm-7">
            <table cellpadding="0" cellspacing="0" width="100%">
              <tr>
                <td width="50%" valign="top"><div class="h-div">
                    <h3>[["lbl_about_us" | translate]]</h3>
                    <ul>
                      <li><a target="_blank" href="<?=url('/') . '/about-us/the-company'?>">[['lbl_the_company'|translate]]</a></li>
                      <li><a target="_blank" href="<?=url('/') . '/about-us/core-values'?>">[['lbl_core_values'|translate]]</a></li>
                      <li><a target="_blank" href="<?=url('/') . '/about-us/our-team'?>">[['lbl_our_team'|translate]]</a></li>
                      <!-- <li><a href="javascript:;">[["lbl_ignite" | translate]] </a></li>
                      <li><a href="javascript:;">[["lbl_sixteen" | translate]] </a></li>
                      <li><a href="javascript:;">[["lbl_discover" | translate]] </a></li>
                      <li><a href="javascript:;">[["lbl_proof_reading" | translate]] </a></li> -->
                    </ul>
                  </div>
                </td>
                <td valign="top"><div class="h-div">
                    <h3>[["lbl_quick_links" | translate]]</h3>
                    <ul>
                      <li><a target="_blank" href="<?=url('/') . '/become-seller'?>">[["lbl_become_seller" | translate]]</a></li>
                      <li><a target="_blank" href="<?=url('/') . '/become-buyer'?>">[["lbl_become_buyer" | translate]]</a></li>
                      <li><a target="_blank" href="<?=url('/') . '/customer-support'?>">[["lbl_customer_support" | translate]] </a></li>
                      <li><a target="_blank" href="<?=url('/') . '/sixteen-terms-of-service'?>">[["lbl_terms_service" | translate]]  </a></li>
                      <li><a target="_blank" href="<?=url('/') . '/privacy-policy'?>">[["lbl_privacy_policy" | translate]]</a></li>
                    </ul>
                  </div>
                   </td>
              </tr>
            </table>
          </div>

        </div>
      </div>
    </div>
  </footer>
</div>

{{ HTML::script('resources/assets/js/front/mp/popper.min.js') }}
{{ HTML::script('resources/assets/js/front/mp/bootstrap.js') }}
{{ HTML::script('resources/assets/js/front/mp/swiper.min.js') }}
{{ HTML::script('resources/assets/js/front/mp/css3-animate-it.js') }}
{{ HTML::script('resources/assets/js/front/mp/jquery.FlowupLabels.js') }}
{{ HTML::script('resources/assets/js/common/star-rating.js') }}

  <!--bootstrap-datetimepicker Start-->
{{ HTML::script('resources/assets/js/front/mp/moment.js') }}
{{ HTML::script('resources/assets/js/front/mp/bootstrap-datetimepicker.js') }}
{{ HTML::script('resources/assets/js/front/mp/jquery-asRange.js') }}

{{ HTML::script('resources/assets/js/front/controllers/TermsofServiceController.js') }}

<!-- <script type="text/javascript" src="assets/js/script.js"></script> -->
<script type="text/javascript">
  $('body').css('background','#fff');

    $(window).load(function() {
     if($(window).width() > 767) {

     } else {
       $(function(){
           var navMain = $(".hMenu");
           navMain.on("click", "a", null, function () {
               navMain.collapse('hide');
           });
       });
     }
  });

    $('.hMenu a').click(function() {
      $('.hMenu li').removeClass('active');
          var datanameC = $(this).attr('data-name');
          var headerHei = $('header').height();
         $(this).parent('li').addClass('active');
          $('html, body').animate({
              scrollTop: $("#"+datanameC).offset().top-headerHei
            }, 1000);
    });
</script>

</body>
</html>
</body>
</html>
