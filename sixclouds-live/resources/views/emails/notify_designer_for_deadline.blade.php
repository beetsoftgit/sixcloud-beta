<html>
    <body>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,300' rel='stylesheet' type='text/css'>
        <table width="614" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:12px;color:#656565;background: #fff;-webkit-border-radius: 8px;-moz-border-radius: 8px;border-radius: 8px;-webkit-box-shadow: 0px -1px 5px #DDD;-moz-box-shadow: 0px -1px 3px #DDD;box-shadow: 0px -1px 5px #DDD;width: 168px;border: 1px solid #e2e2e2;">
            <tbody>
                <tr>
                    <td style="border-radius: 8px 8px 0 0; position: relative; text-align:center; background: #8972f0; padding: 10px; border-bottom: 2px solid #f3f3f3;">
                        <a href="<?php echo '#'; ?>" target="_blank">                            
                            <img src="{{ URL::to('resources/assets/images/logo-sixteen-white.png') }}" alt="Six-clouds" width="100" border="0" >
                        </a>
                    </td>
                </tr>
                <tr>
                    <td style="padding:10px; ">                        
                        <table width="576" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#656565">
                            <tbody>
                                <tr>
                                    <td style="padding:0 10px 20px 10px;">
                                        <table width="554" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        @if($designer['current_language']==1)
                                                        <div style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size: 14px; color: #6C6C6C; margin-bottom: 5px;">
                                                            <span style="font-size: 15px;">Hello {{ $designer['first_name'] }} {{ $designer['last_name'] }},</span> <br><br>deadline for project <strong>{{ $designer['project_title'] }}</strong> 
                                                            @if($designer['deadline_days'] > 1) 
                                                                is after <strong>{{ $designer['deadline_days'] }}</strong> days.

                                                            @elseif($designer['deadline_days'] == 1) 
                                                                is after <strong>{{ $designer['deadline_days'] }}</strong> day.

                                                            @else 
                                                                is <strong>today.</strong>
                                                            @endif
                                                            <br><br>Please do the needful for it.<br><br>
                                                        </div>
                                                        @else
                                                        <div style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size: 14px; color: #6C6C6C; margin-bottom: 5px;">
                                                            <span style="font-size: 15px;">你好 {{ $designer['first_name'] }} {{ $designer['last_name'] }},</span> <br><br>项目截止日期 <strong>{{ $designer['project_title'] }}</strong> 
                                                            @if($designer['deadline_days'] > 1) 
                                                                在之后 <strong>{{ $designer['deadline_days'] }}</strong> 天.

                                                            @elseif($designer['deadline_days'] == 1) 
                                                                在之后 <strong>{{ $designer['deadline_days'] }}</strong> 天.

                                                            @else 
                                                                是 <strong>今天.</strong>
                                                            @endif
                                                            <br><br>请做好必要的准备。<br><br>
                                                        </div>
                                                        @endif
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>        
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table width="576" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#6c6c6c">
                            <tbody>
                                <tr>
                                    <td style="padding:0 10px 20px 10px;">
                                        <table width="554" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size: 14px; color: #6C6C6C;">
                                                            <?php //echo $html; ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>        
                                    </td>
                                </tr>
                                <tr>
                                    @if($designer['current_language']==1)
                                    <td style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:14px;padding:10px;border-top: 1px solid #e6e6e6;    font-weight: 600;">
                                        From,<br /> SixClouds Sixteen
                                    </td>
                                    @else
                                    <td style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:14px;padding:10px;border-top: 1px solid #e6e6e6;    font-weight: 600;">
                                       六云十六
                                    </td>
                                    @endif
                                </tr>
                            </tbody>
                        </table>                        
                    </td>
                </tr>
                <tr>
                    <td style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size:11px; line-height:16px; padding:15px 18px; text-align:center; border-radius: 0 0 8px 8px; background-color: #8972f0; border-top: 3px solid #d7a343; color: #fff;">
                        <?php echo date('Y'); ?>&copy; SixClouds            
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>