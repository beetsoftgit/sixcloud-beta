<html>
    <body>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,300' rel='stylesheet' type='text/css'>
        <table width="614" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:12px;color:#656565;background: #fff;-webkit-border-radius: 8px;-moz-border-radius: 8px;border-radius: 8px;-webkit-box-shadow: 0px -1px 5px #DDD;-moz-box-shadow: 0px -1px 3px #DDD;box-shadow: 0px -1px 5px #DDD;width: 168px;border: 1px solid #e2e2e2;">
            <tbody>
                <tr>
                    <td style="border-radius: 8px 8px 0 0; position: relative; text-align:center; background: #8972f0; padding: 10px; border-bottom: 2px solid #f3f3f3;">
                        <a href="<?php echo '#'; ?>" target="_blank">
                            <img src="{{ URL::to('resources/assets/images/logo-sixteen-white.png') }}" alt="Six-clouds" width="100" border="0" >
                        </a>
                    </td>
                </tr>
                <tr>
                    <td style="padding:10px; ">
                        <table width="576" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#656565">
                            <tbody>
                                <tr>
                                    <td style="padding:0 10px 20px 10px;">
                                        <table width="554" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        @if($designer['current_language']==1)
                                                        <div style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size: 14px; color: #6C6C6C; margin-bottom: 5px;">
                                                            <span style="font-size: 15px;">

                                                            Hello {{ $designer['first_name'] }} {{ $designer['last_name'] }},</span> <br><br>
                                                            <span>
                                                                @if(isset($designer['after_hours']) && $designer['after_hours']==12)

                                                                Hi, you have a Project pending your acceptance. Click <a href="{{ URL::to('/seller-project-info/'.$projectInfo['slug']) }}">here</a>  to review the Project brief.
                                                                <br><br>
                                                                You have less than 12 hours to indicate your acceptance.
                                                                <br><br>
                                                                Don’t miss out on this opportunity！

                                                                @elseif(isset($designer['after_hours']) && $designer['after_hours']==18)

                                                                Hi, you have a Project pending your acceptance. Click <a href="{{ URL::to('/seller-project-info/'.$projectInfo['slug']) }}">here</a> to review the Project brief.
                                                                <br><br>
                                                                You have less than 6 hours to indicate your acceptance. Don’t miss out on this opportunity！

                                                                @else
                                                                <!-- normal flow of mail : original text -->
                                                                Congratulations! You have been selected for a Project.
                                                                <br><br>

                                                                Click <a href="{{ URL::to('/seller-project-info/'.$projectInfo['slug']) }}">here</a> to review the Project brief. You can liaise with the Buyer on the Project Page.
                                                                <br><br>
                                                                Remember, you have 24 hours to accept this challenge!
                                                                <br><br>
                                                                Grab this opportunity to showcase your talents!

                                                                @endif
                                                                <br><br>
                                                            </span>
                                                        </div>
                                                        @else
                                                        <!-- chinese -->
                                                        <div style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size: 14px; color: #6C6C6C; margin-bottom: 5px;">
                                                            <span style="font-size: 15px;">

                                                            你好 {{ $designer['first_name'] }} {{ $designer['last_name'] }},</span> <br><br>
                                                            <span>
                                                                @if(isset($designer['after_hours']) && $designer['after_hours']==12)

                                                                您好，您有一个项目等待接受。请点击 <a href="{{ URL::to('/seller-project-info/'.$projectInfo['slug']) }}">此处</a>查看项目简介。
                                                                <br><br>
                                                                您有少过12小时来表明你的接受。
                                                                <br><br>
                                                                不要错过这个机会！

                                                                @elseif(isset($designer['after_hours']) && $designer['after_hours']==18)

                                                                您好，您有一个项目等待接受。请点<a href="{{ URL::to('/seller-project-info/'.$projectInfo['slug']) }}">击此</a>查看项目简介。
                                                                <br><br>
                                                                您有少过6小时来表明你的接受。
                                                                <br><br>
                                                                不要错过这个机会！

                                                                @else
                                                                恭喜您！您已被挑选负责一个项目。
                                                                <br><br>

                                                                请点击 <a href="{{ URL::to('/seller-project-info/'.$projectInfo['slug']) }}">此处</a> 查看项目简介。
                                                                <br><br>
                                                                温馨提示，您有24小时来接受这份挑战！
                                                                <br><br>
                                                                赶紧抓住这次机会来展示您的才能！


                                                                @endif
                                                                <br><br>
                                                            </span>
                                                        </div>
                                                        @endif
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table width="576" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#6c6c6c">
                            <tbody>
                                <tr>
                                    <td style="padding:0 10px 20px 10px;">
                                        <table width="554" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size: 14px; color: #6C6C6C;">
                                                            <?php //echo $html; ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    @if($designer['current_language']==1)
                                    <td style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:14px;padding:10px;border-top: 1px solid #e6e6e6;    font-weight: 600;">
                                        From,<br /> SixClouds Sixteen
                                    </td>
                                    @else
                                    <td style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:14px;padding:10px;border-top: 1px solid #e6e6e6;    font-weight: 600;">
                                       六云十六
                                    </td>
                                    @endif
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    @if($designer['current_language']==1)
                    <td style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size:11px; line-height:16px; padding:15px 18px; text-align:center; border-radius: 0 0 8px 8px; background-color: #8972f0; border-top: 3px solid #d7a343; color: #fff;">
                        <?php echo date('Y'); ?>&copy; SixClouds            
                    </td>
                    @else
                    <td style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size:11px; line-height:16px; padding:15px 18px; text-align:center; border-radius: 0 0 8px 8px; background-color: #8972f0; border-top: 3px solid #d7a343; color: #fff;">
                        <?php echo date('Y'); ?>&copy; 六云            
                    </td>
                    @endif
                </tr>
            </tbody>
        </table>
    </body>
</html>