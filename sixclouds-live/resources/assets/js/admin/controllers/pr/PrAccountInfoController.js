/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 27th December 2017
 Name    : PrAccountInfoController
 Purpose : All the functions for PR module Closed cases
 */
angular.module('sixcloudAdminApp').controller('PrAccountInfoController', ['$http', '$scope', 'prfactory', '$timeout', '$rootScope', '$routeParams', '$filter', function ($http, $scope, prfactory, $timeout, $rootScope, $routeParams, $filter) {
        console.log('Pr Account Info controller loaded');
    $timeout(function(){
        $(".navbar-nav li").removeClass("active");
        $('#adminHeaderPr').addClass("active");
        $(".menu-list li").removeClass("active");
        $('#all_accesors').addClass("active");
    },1000);
    $scope.openCase = function(caseId){
        $('#case_'+caseId).parent('div').parent('li').toggleClass('open');
        $('#case_'+caseId).parent('div').next('.expand-content').slideToggle(); 
    }
    $scope.accessor_id = $routeParams.id;
    // $('input[name="singledate-picker"]').daterangepicker({
    //     singleDatePicker: true,
    //     showDropdowns: true,
    //     locale: {
    //       format: 'YYYY-MM-DD'
    //     },
    // });
    $( function() {
        $( 'input[name="singledate-picker"]' ).datetimepicker({
            pickTime: false,
            format: 'DD/MM/YYYY',
            maxDate: new Date(),
        });
    });
    prfactory.Getaccessorinfo({'accessor_id': $scope.accessor_id}).then(function(response){
        if(response.data.status==1)
        {
          $scope.accessor = response.data.data;
          $scope.accessorCompletedJobCount = $scope.accessor.accessment_time.length;
        }
    });
    var reportingOfficer = [];
    prfactory.Getaccessors().then(function(response){
        if(response.data.status==1)
        {
            $scope.accessors = response.data.data;
            $scope.accessors_count = response.data.count;
            for($i=0;$i<$scope.accessors.length;$i++ )
                if($scope.accessors[$i].level_type == 2)
                    reportingOfficer.push($scope.accessors[$i]);
        }
    });
    $scope.reportingOfficers = reportingOfficer;
    $scope.teams = [{'value':'Team 1','name':'Team 1'},{'value':'Team 2','name':'Team 2'},{'value':'Team 3','name':'Team 3'},{'value':'Team 4','name':'Team 4'}];
    $scope.updateInfo = function(accessor,id){
        var updateinfo = {};
        updateinfo.accessor_id = accessor.accessor_id;
        updateinfo.team = accessor.team;
        updateinfo.dob = accessor.dob;
        prfactory.Addaccessor({'newaccessor':updateinfo,'id':id}).then(function (response) {
            if (response.data.status == 1)
            {
                simpleAlert('success','Success',response.data.message);
                $('.close').click();
                $scope.newaccessor = '';
            }
        });
    }
    $(document).ready(function(){
        $scope.download = function(pr_case,index,whichFile="",whichClass){
            pr_case['whichFile'] = whichFile;
            $("."+whichClass).text(' ');
            $("."+whichClass).append("<i class='fa fa-spinner fa-spin'></i> Downloading");
            $("."+whichClass).attr("disabled", "disabled");
            prfactory.Download(pr_case).then(function (response) {
                if (response.data.status == 1)
                {
                    var url = response.data.data.url;
                    $('#download_accessed_file_'+pr_case.id).attr('href',url);
                    $('#download_accessed_file_'+pr_case.id).attr('target','_self');
                    $('#download_accessed_file_'+pr_case.id)[0].click();
                    $("."+whichClass).text(' ');
                    if(whichFile == 1)
                        $("."+whichClass).append("<i class='sci-download'></i> Original file");
                    else
                        $("."+whichClass).append("<i class='sci-download'></i> Download file");
                    $("."+whichClass).removeAttr("disabled");
                    prfactory.Unlinkdownloadedfile({'file_name': response.data.data.file_name}).then(function (response) {
                        if (response.data.status == 1)
                        {
                            var file_unlinked_success = response.data.message;
                        }
                    });
                }
            });
        }
    });
}]);