/*
 Author  : Komal Kapadi (komal@creolestudios.com)
 Date    : 28th December 2017
 Name    : AddAdminController
 Purpose : Add new Admin
 */
angular.module('sixcloudAdminApp').controller('AddAdminController', ['Adminlisting', 'adminfactory', '$http', '$scope', '$timeout', '$rootScope', '$filter', function(Adminlisting, adminfactory, $http, $scope, $timeout, $rootScope, $filter) {
    console.log('Add Admin controller loaded');
    $("#dob").daterangepicker({
        singleDatePicker: true,
        showDropdowns: true
    });
    $scope.ADMIN_PROFILE_URL = ADMIN_DEFAULT_URL;
    $scope.admins = Adminlisting.data.data;
    $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 5;
    $scope.$watch('currentPage + numPerPage', function() {
        var begin = (($scope.currentPage - 1) * $scope.numPerPage),
            end = begin + $scope.numPerPage;
        $scope.filteredTodos = $scope.admins.slice(begin, end);
    });
    adminfactory.Getreportingofficers().then(function(res) {
        $scope.reportingofficers = res.data.data;
        console.log($scope.reportingofficers);
    });
    function adminListing() {
        adminfactory.Adminlisting().then(function(res) {
            $scope.admins = res.data.data;
        });
    }
    /*
     * Add new admins
     */
    $scope.submitAdminData = function() {
        var formData = new FormData($("#admin_form")[0]);
        var modules = [];
        $('input[name="modules[]"]:checked').each(function() {
            modules.push(this.value);
        });
        formData.append('modules', modules.join(','));
        adminfactory.Addadmin(formData).then(function(response) {
            if (response.data.status == 1) {
                adminListing();
                simpleAlert('success', 'Add admin', response.data.message, true);
                $("#admin_form")[0].reset();
            } else {
                simpleAlert('error', 'Add admin', response.data.message, true);
            }
        });
    }
    /*
     * Clear all inputs
     */
    $scope.resetForm = function() {
        $('#admin_form')[0].reset();
    }
    /*
     * Delete Admin
     */
    function deleteSelectedAdmin() {
        adminfactory.Deleteadmin({
            'id': $scope.deletedAdmin
        }).then(function(res) {
            if (res.data.status == 1) {
                simpleAlert('success', 'Admin', res.data.message, true);
                adminListing();
            }
        });
    }
    $scope.deleteAdmin = function(adminId) {
        $scope.deletedAdmin = adminId;
        confirmDialogue('', '', '', deleteSelectedAdmin);
    }
    var checkboxes = $("input[type='checkbox']");
    $scope.disable = false;
    checkboxes.click(function() {
        $scope.disable = checkboxes.is(":checked");
    });
    $scope.disableButton = function(formValid) {
        if (formValid && $scope.disable) {
            alert();
            return false;
        } else return true;
    }
}]);