/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 3rd August 2018
 Name    : CreateNewVideoController
 Purpose : realted to creating new video
 */
angular.module('sixcloudAdminApp').controller('CreateNewVideoController', [ '$http', 'adminignitefactory', '$scope', '$timeout', '$rootScope', '$filter', function( $http, adminignitefactory, $scope, $timeout, $rootScope, $filter) {
    console.log('CreateNewVideo controller loaded');
    $(".navbar-nav li").removeClass("active");
    $('#adminHeaderIgnite').addClass("active");
    $timeout(function () {
        $(".ignite-header li").removeClass("active");
        $('#video_manger').addClass("active");
    }, 500);
    // $scope.CreateVideotitle = function(status,button){
    //     if($scope.video_title == undefined){
    //         simpleAlert('error','','Enter video title');
    //     } else {
    //         showLoader('#'+button);
    //         Disable('#unpublishTitle');
    //         Disable('#draftTitle');
    //         adminignitefactory.Addquiztitle({'title':$scope.video_title,'status':status,'what':'video'}).then(function(response) {
    //             (button=='draftTitle'?hideLoader('#'+button,'Save as Draft'):hideLoader('#'+button,'Ready to Publish'));
    //             Enable('#unpublishTitle');
    //             Enable('#draftTitle');
    //             if (response.data.code == 2) window.location.replace("manage/login");
    //             if (response.data.status == 1) {
    //                 window.location.replace("manage/ignite/video-manager");
    //             } else {
    //                 simpleAlert('error', '', response.data.message);
    //             }
    //         });
    //     }
    // }

    // adminignitefactory.Getvideocategories().then(function (response) {
    //     if (response.data.code == 2) window.location.replace("sixteen-login");
    //     if (response.data.status == 1) {
    //         $scope.videoCategories = response.data.data;
    //     }
    // });
    adminignitefactory.Getzones().then(function (response) {
        if (response.data.code == 2) window.location.replace("manage/login");
        if (response.data.status == 1) {
            $scope.zones = response.data.data.zones;
        }
    });

    $scope.getSubjects = function(count) {
        adminignitefactory.Getcategories({'zone_id':$scope.selectedZones.selected}).then(function (response) {
            if (response.data.code == 2) window.location.replace("manage/login");
            if (response.data.status == 1) {
                if(count == 1) {
                    $scope.videoCategories = undefined;
                    $scope.subCategories = undefined;
                }
                $scope.subjects = response.data.data.categories;
                if($scope.subjects) {
                    $scope.subjects.forEach(function(e){
                        e.zones = e.zones.split(',');
                        var index = e.zones.indexOf($scope.selectedZones.selected);
                        if (index !== -1) e.zones.splice(index, 1);
                        e.otherZones = e.zones;
                    });
                }
            }
        });
    }

    $scope.prerequisitesCounter = 1;
    $scope.showMoreDisable = false;
    $scope.Loadprerequisites = function (id) {
        adminignitefactory.Loadprerequisites({ 'video_category_id': id }).then(function (response) {
            if (response.data.code == 2) window.location.replace("manage/login");
            if (response.data.status == 1) {
                if(response.data.data.yesSubcategories){
                    $scope.video_ordering =  undefined;
                    $scope.subCategoriesPresent = true;
                    $scope.subCategories = response.data.data.yesSubcategories.subCategories;
                } else {
                    $scope.subCategories = undefined;
                    $scope.prerequisitesCounter = 1;
                    $scope.prerequisitesQuizCounter = 1;
                    $('#prerequisitesDiv').empty();
                    $('#preRequisitesQuizesDiv').empty();
                    $scope.video_ordering = response.data.data.noSubcategories.lastSequence;
                    $scope.preRequisites = response.data.data.noSubcategories.preRequisites;
                    abc = response.data.data.noSubcategories.preRequisites;
                    if ($scope.preRequisites.length > 1) {
                        $scope.showMoreDisable = true;
                    } else {
                        $scope.showMoreDisable = false;
                    }
                    $scope.preRequisitesBackup = response.data.data.noSubcategories.preRequisites;
                    $scope.quiz_ordering = response.data.data.noSubcategories.lastSequence;
                    $scope.preRequisitesQuizes = response.data.data.noSubcategories.preRequisites;
                    if ($scope.preRequisitesQuizes.length > 1) {
                        $scope.showMoreDisableQuiz = true;
                    } else {
                        $scope.showMoreDisableQuiz = false;
                    }
                    $scope.worksheet_ordering = response.data.data.noSubcategories.lastSequence;
                    return false;
                }
            }
        });
    }
    $scope.Loadcategoriesforsubject = function(subjectId){
        $scope.subCategories =  undefined;
        $scope.subCategoriesPresent = false;
        $scope.video_ordering =  undefined;
        adminignitefactory.Loadcategoriesforsubject({ 'subject_id': subjectId }).then(function (response) {
            if (response.data.code == 2) window.location.replace("manage/login");
            if (response.data.status == 1) {
                $scope.videoCategories = response.data.data.categories;
                $scope.videos = response.data.data.languages;
                $scope.subjectsList = response.data.data.subjects;
                    if($scope.videos.length>0){
                        $timeout(function(){
                            document.querySelector("html").classList.add('js');
                            var fileInput = document.querySelector(".input-file"),
                                button = document.querySelector(".input-file-trigger"),
                                the_return = document.querySelector(".file-return");

                            button.addEventListener("keydown", function (event) {
                                if (event.keyCode == 13 || event.keyCode == 32) {
                                    fileInput.focus();
                                }
                            });
                            button.addEventListener("click", function (event) {
                                fileInput.focus();
                                return false;
                            });
                            fileInput.addEventListener("change", function (event) {
                                var filename = this.value.split('\\');
                                filename = filename.pop();
                                // the_return.innerHTML = filename;
                            });
                        },1500);
                        $scope.showVideoDiv = true;
                    }
            }
        });
    }
    // Loadsubcategories

    /*$scope.Addmoreprerequisites = function () {
        category = $('input[name=video_category_id]:checked').val();
        if ($scope.prerequisitesCounter < $scope.preRequisites.length) {
            if (category != undefined) {
                $scope.showMoreDisable = true;
                var copy = $("#prerequisites_" + $scope.prerequisitesCounter).clone(true);
                selectedCategory = $('#prerequisites_' + $scope.prerequisitesCounter).val();
                $scope.prerequisitesCounter = $scope.prerequisitesCounter + 1;
                $('#prerequisitesDiv').append(copy);
                $timeout(function () {
                    $("#prerequisites_" + $scope.prerequisitesCounter).addClass('upper-space');
                }, 100);

            } else {
                if (category == undefined) {
                    simpleAlert('error', '', 'Select video category');
                }
                $scope.showMoreDisable = false;
            }
        } else {
            $scope.showMoreDisable = false;
        }
    }*/

    $scope.showVideoDiv = false;
    $(document).ready(function() {
        $('.languageselect').multiselect();
    });
    $(document).on('change', '.input-file', function(event) {
            var fileinputid = $(this).attr('id');
            fileinputid = fileinputid.split('_');
            fileinputid = fileinputid.pop();
            if(this.value.length) {
                var filename = this.value.split('\\');
                filename = filename.pop();
                $('#filereturn_'+fileinputid).html(filename);
            }
            else {
                $('#filereturn_'+fileinputid).html('Drop video file here');
            }
            
        });
    $scope.selectedZones = [];
    $scope.Addvideolanguages = function(){
        if($scope.selectedZones.length==0){
            simpleAlert('error', '', 'Select Zone(s)');
            return;
        } else {
            adminignitefactory.Loadlanguagesforzones({ 'selected_zones': $scope.selectedZones }).then(function (response) {
                if (response.data.code == 2) window.location.replace("manage/login");
                if (response.data.status == 1) {
                    $scope.videos = response.data.data;
                    if($scope.videos.length>0){
                        $timeout(function(){
                            document.querySelector("html").classList.add('js');
                            var fileInput = document.querySelector(".input-file"),
                                button = document.querySelector(".input-file-trigger"),
                                the_return = document.querySelector(".file-return");

                            button.addEventListener("keydown", function (event) {
                                if (event.keyCode == 13 || event.keyCode == 32) {
                                    fileInput.focus();
                                }
                            });
                            button.addEventListener("click", function (event) {
                                fileInput.focus();
                                return false;
                            });
                            fileInput.addEventListener("change", function (event) {
                                var filename = this.value.split('\\');
                                filename = filename.pop();
                                // the_return.innerHTML = filename;
                            });
                        },1500);
                        $scope.showVideoDiv = true;
                    }
                }
            });
        }
        /*language = $('#videolanguage').val();
        $scope.videos = undefined;
        language = $('#videolanguage').val();
        if(!jQuery.isEmptyObject(language)){
            $scope.videos = language;
            $timeout(function(){
                document.querySelector("html").classList.add('js');

                var fileInput = document.querySelector(".input-file"),
                    button = document.querySelector(".input-file-trigger"),
                    the_return = document.querySelector(".file-return");

                button.addEventListener("keydown", function (event) {
                    if (event.keyCode == 13 || event.keyCode == 32) {
                        fileInput.focus();
                    }
                });
                button.addEventListener("click", function (event) {
                    fileInput.focus();
                    return false;
                });
                fileInput.addEventListener("change", function (event) {
                    var filename = this.value.split('\\');
                    filename = filename.pop();
                    // the_return.innerHTML = filename;
                });
            },500);
            $scope.showVideoDiv = true;
        } else {
            $scope.videos = undefined;
            simpleAlert('error','','Select language');
        }*/
    }

    $scope.Uploadnewvideo = function (status, button) {
        // if ($scope.title == undefined || $scope.description == undefined || $scope.video_ordering == undefined) {
        //if ($scope.video_ordering == undefined) {
            // if ($scope.title == undefined) {
            //     simpleAlert('success', '', 'Video "title" is required');
            //     return;
            // }
            // if ($scope.description == undefined) {
            //     simpleAlert('success', '', 'Video "description" is required');
            //     return;
            // }
            /*if ($scope.video_ordering == undefined) {
                simpleAlert('success', '', 'Video position is required');
                return;
            }*/
        //} else {
            newVideo = new FormData($("#newVideoForm")[0]);
            (button=='draft'?showLoader(".draft"):showLoader(".publish"));
            newVideo.append('status',status);
            newVideo.append('subCategoriesPresent',$scope.subCategoriesPresent);
            newVideo.append('video_for_zones[]',$scope.selectedZones.selected);
            adminignitefactory.Uploadnewvideo(newVideo).then(function (response) {
                (button=='draft'?hideLoader(".draft", 'Save as Draft'):hideLoader(".publish", 'Publish'));
                if (response.data.code == 2) window.location.replace("sixteen-login");
                if (response.data.status == 1) {
                    window.location.replace("manage/ignite/video-manager");
                } else {
                    simpleAlert('error','',response.data.message);
                }
            });
        //}
    }

    $(document).on('click', '#video_thumbnail_select', function(event) {
        $('#video_thumbnail').click();
    });
        $('#video_thumbnail').change(function() {
            if(event.target.files.length>0){
                if(event.target.files[0].type == 'image/jpeg' || event.target.files[0].type == 'image/png'||event.target.files[0].type == 'image/jpg')
                    readURL(this);
                else{
                    simpleAlert('error', '', 'Only jpg, png and jpeg file formats are allowed for "Video Thumbnail"');
                }
            } else {
                $('#video_thumbnail_select').css('background-image', "url(resources/assets/images/up-img.png)");
                $('#video_thumbnail_select').removeClass('quiz_image_option');
                $('#video_thumbnail').val('');
            }
        });

    function readURL(input) {
        if (input.files && input.files[0]&&input.files.length>0) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#video_thumbnail_select').css('background-image', "url("+e.target.result+")");
                $('#video_thumbnail_select').addClass('quiz_image_option');
            }
            reader.readAsDataURL(input.files[0]);
        } else {
            $('#video_thumbnail_select').css('background-image', "url(resources/assets/images/up-img.png)");
            $('#video_thumbnail_select').removeClass('quiz_image_option');
            $('#video_thumbnail').val('');
        }
    }
}]);