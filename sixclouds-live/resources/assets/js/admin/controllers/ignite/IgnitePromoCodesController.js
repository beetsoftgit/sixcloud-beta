/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 20th Nov, 2018
 Name    : IgnitePromoCodesController
 Purpose : All the functions for promocode page
 */
angular.module('sixcloudAdminApp').controller('IgnitePromoCodesController', ['adminignitefactory','$http', '$scope', '$timeout', '$rootScope', '$filter','$routeParams', function(adminignitefactory,$http, $scope, $timeout, $rootScope, $filter,$routeParams) {
    // $scope.sections = Geteltsections.data.data;
    console.log('IgnitePricingPlans controller loaded');
    $(".navbar-nav li").removeClass("active");
    $('#adminHeaderIgnite').addClass("active");
    $timeout(function(){
        $(".ignite-header li").removeClass("active");
        $('#ignite_promo_code').addClass("active");
    },500);
    $scope.current_domain = current_domain;
    $scope.statusParam = $routeParams.status;

    $scope.GetAllPromoData = function (data, pageNumber) {
        adminignitefactory.GetAllPromoData(data, pageNumber).then(function (response) {
            if (response.data.status == 1) {
                $scope.promoContent = response.data.data.content.data;
                $scope.totalpromo = response.data.data.content.total;
                $scope.paginate = response.data.data.content;
                $scope.currentPromoPaging = response.data.data.content.current_page;
                $scope.rangePromopage = _.range(1, response.data.data.content.last_page + 1);    
                $scope.status = response.data.data.status;        
            }
        });
    }
    if($scope.statusParam==undefined)
        $scope.GetAllPromoData({'status':1}, 1);
    else{
        $scope.GetAllPromoData({'status':$scope.statusParam}, 1);
        $(".tablinks").removeClass("active");
        if($scope.statusParam==1)
            $("#live").addClass("active");
        else if($scope.statusParam==2)
            $("#on_hold").addClass("active");
        else
            $("#expired").addClass("active");
        $scope.status = $scope.statusParam;
    }
    $scope.Changepromotype = function(status,id){
        $scope.GetAllPromoData({'status': status,'search_by':$scope.search_by}, 1);
        $(".tablinks").removeClass("active");
        $("#"+id).addClass("active");
        $scope.status = status;
    }

    $scope.Searchby = function(status,search_by){
        $scope.GetAllPromoData({'status': status,'search_by':search_by}, 1);
    }

    $scope.ExportPromos = function(export_type) {
        if(export_type!= undefined && export_type!= ''){
            window.location = BASEURL + 'ExportPromos?export_type=' + export_type+'&status='+$scope.status+'&search_by='+$scope.search_by;
        } else {
            simpleAlert('error', '', 'Select export type');
        }
    }

    $scope.Suspendpromo = function(id){
        $scope.id = id;
        confirmDialogueDifferentButton('','','',suspendredirect);
    }

    function suspendredirect() {
        showLoader('.suspendpromo_'+$scope.id,"");
        adminignitefactory.Suspendpromo({'id':$scope.id}).then(function (response) {
            hideLoader('.suspendpromo_'+$scope.id,'<i class="fa fa-trash lg-icon" aria-hidden="true"></i>');
            if (response.data.status == 1) {
                $scope.GetAllPromoData({'status': $scope.status,'search_by':$scope.search_by}, $scope.currentPromoPaging);
            }
        });
    }
    

}]);