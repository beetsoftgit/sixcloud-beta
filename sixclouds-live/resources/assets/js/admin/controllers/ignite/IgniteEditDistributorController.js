/*
 Author  : Ketan Solanki (ketan@creolestudios.com)
 Date    : 16th August 2017
 Name    : IgniteEditDistributorController
 Purpose : All the functions for editing ignite distributor
 */
angular.module('sixcloudAdminApp').controller('IgniteEditDistributorController', ['$http', '$scope', '$timeout', '$rootScope', '$routeParams', '$filter', 'adminignitefactory', function ($http, $scope, $timeout, $rootScope, $routeParams, $filter, adminignitefactory) {  
    $(".navbar-nav li").removeClass("active");
    $('#adminHeaderIgnite').addClass("active");
    $timeout(function () {
        $(".ignite-header li").removeClass("active");
        $('#ignite_distributors').addClass("active");
    }, 500);
 
    $scope.slug = $routeParams.slug;    
    $scope.distributorDetails = function(){
        adminignitefactory.GetDistributorDetails({ 'id': $scope.slug }).then(function (response) {             
            if (response.data.status == 1) {
                $scope.distributorData = response.data.data;
                $scope.distributorid = response.data.data.id;
                if($scope.distributorData.referal_promo_code){
                    $scope.discount_type = $scope.distributorData.referal_promo_code.discount_type;
                    $scope.price = {};
                    $scope.price.discount_of = $scope.distributorData.referal_promo_code.discount_of;
                } else {
                    $scope.discount_type = 2;
                    $scope.price = {};
                    $scope.price.discount_of = undefined;
                }
                //Get default state dropdown
                adminignitefactory.Getstatesforcountry({ 'id': $scope.distributorData.country_id }).then(function (response) {
                    if (response.data.status == 1) {
                        $scope.states = response.data.data;
                    }
                });
                //Get default city dropdown
                adminignitefactory.Getcitiesforstate({ 'id': $scope.distributorData.state_id }).then(function (response) {
                    if (response.data.status == 1) {
                        $scope.cities = response.data.data;
                    }
                });
                adminignitefactory.Getcountry().then(function (response) {
                    if (response.data.status == 1) {
                        $scope.countrycitydata = response.data.data;
                    }
                });
            }
        });
    }
    $scope.distributorDetails();
    $('#country_id').on('change', function () {
        var id = $(this).val();
        adminignitefactory.Getstatesforcountry({ 'id': id }).then(function (response) {
            if (response.data.status == 1) {
                $scope.states = response.data.data;
            }
        });
    });
    $(document).on('change', '#state_id', function () {
        var id = $(this).val();
        adminignitefactory.Getcitiesforstate({ 'id': id }).then(function (response) {
            if (response.data.status == 1) {
                $scope.cities = response.data.data;
            }
        });
    });    
    
    var countDecimals = function(value) {
        if (Math.floor(value) != value){
            return value.toString().split(".")[1].length || 0;
        }
        return 0;
    }
    $scope.editDistributor = function () { 
        var companyName                 = $("#company_name").val();
        //var lastName                  = $("#last_name").val();
        var emailAddress                = $("#email_address").val();
        var phonecode                   = $("#phonecode").val();
        var contact                     = $("#contact").val();
        var commissionPercentage        = $("#commission_percentage").val();
        var renewalCommissionPercentage = $("#renewal_commission_percentage").val();
        var countryId                   = $("#country_id").val();
        var stateId                     = $("#state_id").val();
        var cityId                      = $("#city_id").val();
        var locality                    = $("#locality").val();
        if (emailAddress == undefined || emailAddress == "" || companyName == undefined || companyName == "" || commissionPercentage == undefined || commissionPercentage == "" || commissionPercentage > 100 || renewalCommissionPercentage == undefined || renewalCommissionPercentage == "" || renewalCommissionPercentage > 100 || countryId == undefined || countryId == "" || countryId == 0 || stateId == undefined || stateId == "" || stateId == 0 || cityId == undefined || cityId == "" || cityId == 0 || locality == undefined || locality == ""||phonecode==undefined||phonecode==''||contact==undefined||contact=='') {           
            if (companyName == undefined || companyName == "") {
                simpleAlert('success', '', 'First Name is required');
                return;
            }
            // if (lastName == undefined || lastName == "") {
            //     simpleAlert('success', '', 'Last Name is required');
            //     return;
            // }
            if (emailAddress == undefined || emailAddress == "") {
                simpleAlert('success', '', 'Email Address is required');
                return;
            }
            if (commissionPercentage == undefined || commissionPercentage == "" ) {
                simpleAlert('success', '', 'Commission Percentage is required');
                return;
            }
            if (commissionPercentage > 100) {
                simpleAlert('success', '', 'Commission Percentage should be less then 100');
                return;
            }  
            var count = countDecimals(commissionPercentage);                
            if (count > 2) {
                simpleAlert('success', '', 'Commission Percentage allows only 2 decimals');
                return;
            }  

            if (renewalCommissionPercentage == undefined || renewalCommissionPercentage == "" ) {
                simpleAlert('success', '', 'Renewal Commission Percentage is required');
                return;
            }
            if (renewalCommissionPercentage > 100) {
                simpleAlert('success', '', 'Renewal Commission Percentage should be less then 100');
                return;
            }  
            var count = countDecimals(renewalCommissionPercentage);                
            if (count > 2) {
                simpleAlert('success', '', 'Renewal Commission Percentage allows only 2 decimals');
                return;
            }  
            if (countryId == undefined || countryId == "" || countryId == 0) {
                simpleAlert('success', '', 'Country is required');
                return;
            }
            if (stateId == undefined || stateId == "" || stateId == 0) {
                simpleAlert('success', '', 'Province is required');
                return;
            }
            if (cityId == undefined || cityId == "" || cityId == 0) {
                simpleAlert('success', '', 'City is required');
                return;
            }
            if (locality == undefined || locality == "") {
                simpleAlert('success', '', 'Locality is required');
                return;
            }
            if (contact == undefined || contact == '') {
                simpleAlert('success', '', 'Contact is required');
                return;
            }
            if (phonecode == undefined || phonecode == '') {
                simpleAlert('success', '', 'Country code is required');
                return;
            }
        } else {
            if($scope.discount_type==0||$scope.discount_type==1){
                if($scope.price.discount_of==undefined||$scope.price.discount_of==''){
                    simpleAlert('error', '', 'Discount of is required');
                    return;
                }
            }
            editDistributorFormData = new FormData($("#editDistributorForm")[0]);
            editDistributorFormData.append('old_referal_code',$scope.distributorData.referal_code);
            showLoader(".edit-distributor");
            adminignitefactory.editDistributor(editDistributorFormData).then(function (response) {                
                hideLoader(".edit-distributor", 'Submit');
                if (response.data.code == 2) window.location.replace("manage/login");
                if (response.data.status == 1) {
                    window.location.replace("manage/ignite/ignite-distributor-details/"+response.data.data.slug);
                } else {
                    simpleAlert('error','',response.data.message);
                }
            });
        }
    }
}]);
