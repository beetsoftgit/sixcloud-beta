/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 3rd August 2018
 Name    : EditQuizManagerController
 Purpose : realted to editing existing quiz
 */
angular.module('sixcloudAdminApp').controller('EditQuizManagerController', [ '$http', 'adminignitefactory', '$scope', '$timeout', '$rootScope', '$routeParams', '$filter', function( $http, adminignitefactory, $scope, $timeout, $rootScope, $routeParams, $filter) {
    console.log('CreateNewQuiz controller loaded');
    $(".navbar-nav li").removeClass("active");
    $('#adminHeaderIgnite').addClass("active"); 
    $timeout(function () {
        $(".ignite-header li").removeClass("active");
        $('#quiz_manger').addClass("active");
        
        // $('.custom-accordion .panel-heading').click(function() {
        //   $(this).toggleClass('active');
        //   $(this).next('.panel-collapse').slideToggle();
        // });
    }, 500);
    $timeout(function () {
        $(document).ready(function() {
            $('.fileinput').fileinput();
        });
    }, 1000);
    $(function() {
        $( ".sortable" ).sortable();
        $( ".sortable" ).disableSelection();
    });

    $scope.slug = $routeParams.slug;
    $scope.quizDetails = function(){
        adminignitefactory.GetQuizDetails({ 'id': $scope.slug,'isEdit':true }).then(function (response) {
            if (response.data.status == 1) {
                $scope.quizData = response.data.data;
                $scope.getSubjects($scope.quizData.selectedZonesCount[0]);
                // $scope.selectedZones = $scope.quizData.selectedZonesCount;
                $scope.Loadcategoriesforsubject($scope.quizData.subject_id);
                if($scope.quizData.subcategory_id!=''){
                    $scope.Loadprerequisites($scope.quizData.category_id)
                }
                $scope.quizid = response.data.data.id;
            }
        });
    }
    $scope.quizDetails();

    // adminignitefactory.Getcategories().then(function (response) {
    //     if (response.data.code == 2) window.location.replace("manage/login");
    //     if (response.data.status == 1) {
    //         $scope.subjects = response.data.data.categories;
    //     }
    // });

    $scope.getSubjects = function(zone_id, count) {
        adminignitefactory.Getcategories({'zone_id':zone_id}).then(function (response) {
            if (response.data.code == 2) window.location.replace("sixteen-login");
            if (response.data.status == 1) {
                if(count == 1) {
                    $scope.quizData.subject_id = undefined;
                    $scope.quizData.category_id = undefined;
                    $scope.quizData.subcategory_id = undefined;
                    $scope.videoCategories = undefined;
                    $scope.subCategories = undefined;
                }
                $scope.subjects = response.data.data.categories;
                if($scope.subjects) {
                    $scope.subjects.forEach(function(e){
                        e.zones = e.zones.split(',');
                        // var index = e.zones.indexOf(zone_id);
                        // if (index == -1) e.zones.splice(index, 1);
                        var filtered = e.zones.filter(function(value){
                            return value != zone_id;
                        });
                        e.otherZones = filtered;
                    });
                }
            }
        });
    }
    // $scope.getSubjects($scope.quizData.selectedZonesCount[0])

    $scope.Addvideolanguages = function(){
        $scope.selectedZones1 = [];
        $.each($('input[name="video_for_zones[]"]:checked'), function(){            
            $scope.selectedZones1.push($(this).val());
        });
        if($scope.selectedZones1.length==0){
            simpleAlert('error', '', 'Select Zone(s)');
            return;
        } else {
            adminignitefactory.Loadlanguagesforzones({ 'selected_zones': $scope.selectedZones1,'isEdit':true,'for':'quiz','quiz_id':$scope.quizData.id }).then(function (response) {
                if (response.data.code == 2) window.location.replace("manage/login");
                if (response.data.status == 1) {
                    $scope.quizData.quizlanguages = response.data.data;

                }
            });
        }
    }
    $scope.Loadcategoriesforsubject = function(subjectId){
        $scope.subCategories =  undefined;
        $scope.subCategoriesPresent = false;
        $scope.video_ordering =  undefined;
        adminignitefactory.Loadcategoriesforsubject({ 'subject_id': subjectId }).then(function (response) {
            if (response.data.code == 2) window.location.replace("manage/login");
            if (response.data.status == 1) {
                $scope.videoCategories = response.data.data.categories;
            }
        });
    }
    $scope.Loadprerequisites = function (id) {
        $scope.quizData.category_id = id;
        $scope.quizData.subcategory_id = undefined;
        $scope.subCategories = undefined;
        $scope.quizData.subCategories = undefined;
        adminignitefactory.Loadprerequisites({ 'video_category_id': id }).then(function (response) {
            if (response.data.code == 2) window.location.replace("manage/login");
            if (response.data.status == 1) {
                if(response.data.data.yesSubcategories){
                    $scope.subCategoriesPresent = true;
                    $scope.subCategories = response.data.data.yesSubcategories.subCategories;
                }
            }
        });
    }
    $scope.setVariables = function(subjectId) {
        $scope.quizData.subject_id = subjectId;
        $scope.quizData.category_id = undefined;
        $scope.quizData.subcategory_id = undefined;
    }
    $scope.setSubCategory = function(subCatId) {
        $scope.quizData.subcategory_id = subCatId;
    }

    $scope.Createquiztitle = function(){
        // $scope.quiz_title = $('#quizTitleText').val();
        // $scope.description = $('#quizDescriptionText').val();
        // // $scope.quiz_status = $scope.quiz_status;
        if($scope.quizData.subject_id == undefined){
            simpleAlert('error','','Please select subject');
        } else if($scope.quizData.category_id == undefined) {
            simpleAlert('error','','Please select category');
        } else if($scope.subCategories && $scope.quizData.subcategory_id == undefined) {
            simpleAlert('error','','Please select subcategory');
        } else {
            showLoader('#quizTitle');
            var quizDetails = new FormData($('#edit_form')[0]);
            quizDetails.append('id',$scope.quizid);
            // adminignitefactory.Addquiztitle({'title':$scope.quiz_title,'description':$scope.description,'quiz_status':$scope.quiz_status,'id':$scope.quizid}).then(function(response) {
            adminignitefactory.Editquiztitle(quizDetails).then(function(response) {
                hideLoader('#quizTitle','Edit');
                if (response.data.code == 2) window.location.replace("manage/login");
                if (response.data.status == 1) {
                } else {
                    simpleAlert('error', '', response.data.message);
                }
            });
        }
    }
    // $(document).ready(function() {
    //     $(".clickquestionImage").click(function() {
    //         var id = $(this).attr('id');
    //         previewId = id;
    //         id = id.replace("click",'');
    //         $('#'+id).click();
    //         $('#'+id).change(function() {
    //           readURLQuestion(this,id,previewId);
    //         });
    //     });
    // });
    $(document).ready(function() {
        $(document).on('click', '.clickquestionImage', function(event) {
            var id = $(this).attr('id');
            previewId = id;
            id = id.replace("click",'');
            $('#'+id).click();
            $('#'+id).change(function() {
              readURLQuestion(this,id,previewId);
            });
        });
    });

    function readURLQuestion(input,id,previewId) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#'+previewId).css('background-image', "url("+e.target.result+")");
                $('#'+previewId).addClass('quiz_image_option');
                trashId = id.replace('questionImage_','removequestionImage_');
                $('#'+trashId).removeClass('hide');
            }
        reader.readAsDataURL(input.files[0]);
        }
    }

    function removeQuestionImageMethod(){
        $scope.idque = $scope.idque.replace("remove",'click');
        $('#'+$scope.idque).css('background-image', "url(resources/assets/images/up-img.png)");
        $('#'+$scope.idque).removeClass('quiz_image_option');
        $('#'+$scope.removeidque).addClass('hide');
        $scope.removeidque = $scope.removeidque.replace('remove','');
        $('#'+$scope.removeidque).val('');
        $('<input>').attr({type: 'hidden',name: 'removequestionImage', value:''+true+''}).appendTo('#questionform_'+$scope.formIdRemoveQueImage);
        return false;
    }   
    // $(document).ready(function() {
    //     $(".question_remove_image").click(function() {
    //         $scope.idque = $(this).attr('id');
    //         $scope.removeidque = $scope.idque;
    //         $scope.formIdRemoveQueImage = $(this).closest("form").attr('id');
    //         $scope.formIdRemoveQueImage = $scope.formIdRemoveQueImage.split('_').pop();
    //         confirmDialogue('','','',removeQuestionImageMethod);
    //         return false;
    //     });
    // });
    $(document).ready(function() {
        $(document).on('click', '.question_remove_image', function(event) {
            $scope.idque = $(this).attr('id');
            $scope.removeidque = $scope.idque;
            $scope.formIdRemoveQueImage = $(this).closest("form").attr('id');
            $scope.formIdRemoveQueImage = $scope.formIdRemoveQueImage.split('_').pop();
            confirmDialogue('','','',removeQuestionImageMethod);
            return false;
            
        });
    });

    function removeQuestionAudioMethod(){
        $('#'+$scope.removeidque).val('');
        $('<input>').attr({type: 'hidden',name: 'removequestionAudio', value:''+true+''}).appendTo('#'+$scope.formIdRemoveQueImage);
        var removeHideAudio = $scope.formIdRemoveQueImage.split('_').pop();
        $('#questionAudioSourceParentDiv_'+removeHideAudio).addClass('hide');
        $('#questionAudioSelectParentDiv_'+removeHideAudio).removeClass('hide');
        return false;
    }   
    $(document).ready(function() {
        $(document).on('click', '.question_remove_audio', function(event) {
            $scope.idque = $(this).attr('id');
            $scope.removeidque = $scope.idque;
            $scope.formIdRemoveQueImage = $(this).closest("form").attr('id');
            confirmDialogue('','','',removeQuestionAudioMethod);
            return false;
        });
    });

    $scope.questionSaved = false;
    $scope.Addnewquestion = function(value){
        value.push({});
        $timeout(function () {
            $(document).ready(function() {
                $('.fileinput').fileinput();
            });
        }, 1000);
        // var copy = $("#questionform").clone(true);
        // copy.removeAttr('id');
        // $scope.newQuestion = $('#questionbank > form').length+1;
        // copy.attr('id','questionform_'+$scope.newQuestion);
        // copy.removeClass('hide');
        // $('#questionbank').append(copy);
        
        // var lastform = $('#questionbank > form').last();
        // lastform = lastform.last();
        // lastdivformID = lastform[0].id;
        // console.log(lastdivformID);
        // lastdivformID = lastdivformID.split('_');
        // lastdivformID = lastdivformID[1]++;
        // $scope.newQuestion = lastdivformID+1;
        // $scope.newQuestionSequence = $('#questionbank > form').length+1;
        // var copy = '<form class="panel ui-state-default custom-accordion custom-accordion-new_'+$scope.newQuestion+'" id="questionform_'+$scope.newQuestion+'"><div class="panel ui-state-default" id="question_'+$scope.newQuestion+'"><div class="panel-heading toggledirective panel-heading-new_'+$scope.newQuestion+'"><h4 class="panel-title"> <a href="javascript:;">Question #'+$scope.newQuestionSequence+' <i id="checkcircle_'+$scope.newQuestion+'" class="hide fa fa-check-circle" aria-hidden="true"></i><span class="right-icon "><i class="fa fa-plus" aria-hidden="true"></i></span> </a> </h4></div><div class="panel-collapse"><div class="panel-body"><div class="cnq-form-div"><div class="form-group"><input type="hidden" name="quiz_id" value="'+$scope.quizid+'"><div class="row question_spacing"><div class="col-sm-3"><label class="label">Question</label></div><div class="col-sm-9"><input type="text" class="form-control question_spacing" name="question_title" placeholder="Question here"></div></div><div class="row"><div class="col-sm-3"><label class="label">Question Image</label></div><div class="col-sm-9"><div class="form-group ui-state-default" id="questionImage"><div class=" image-fields"><div class="ui-state-default"><input type="file" class="hide" id="questionImage_'+$scope.newQuestion+'" name="question_image" accept="image/*"><div style="background-image:url(resources/assets/images/up-img.png);" id="clickquestionImage_'+$scope.newQuestion+'" class="img-div clickquestionImage"><a href="javascript:;" id="removequestionImage_'+$scope.newQuestion+'" class="icon question_remove_image hide"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div></div></div></div></div></div><div class="row"><div class="col-sm-3"><label class="label">Question Audio</label></div><div class="col-sm-9"><div class="form-group ui-state-default" id="questionAudio"><div class="fileinput fileinput-new input-group" data-provides="fileinput"><div class="form-control" data-trigger="fileinput"><span class="fileinput-filename"></span></div><span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists disable_color">Change</span><input type="file" id="questionAudio_'+$scope.newQuestion+'" name="question_audio" accept="audio/*"></span><a href="#" class="input-group-addon btn btn-default fileinput-exists disable_color" data-dismiss="fileinput">Remove</a></div></div></div></div></div><hr><div class="" id="optionList_'+$scope.newQuestion+'"></div><div class="form-group"><div class="row"><div class="col-sm-3"><label class="label">New Option</label></div><div class="col-sm-9"><div class="new-option"> <a class="btn text_option" id="text_'+$scope.newQuestion+'"><i class="fa fa-file-text-o" aria-hidden="true"></i> Text</a><a class="btn image_option" id="image_'+$scope.newQuestion+'"><i class="fa fa-picture-o" aria-hidden="true"></i> Image</a><a class="btn audio_option" id="audio_'+$scope.newQuestion+'"><i class="fa fa-volume-up" aria-hidden="true"></i> Audio Clip</a></div></div></div></div><hr><div class="form-group"><div class="row"><div class="col-sm-3"><label class="label">Correct Answer(s)</label></div><div class="col-sm-9" id="correctAnswer_'+$scope.newQuestion+'"></div></div></div><div class="form-group"><div class="row"><div class="col-sm-3"><a href="#" id="saveQuestion_'+$scope.newQuestion+'" class="btn btn-purple savequestion">Save Question</a></div><div class="col-sm-9"><div class="btn-div"></div></div></div></div></div></div></div></div></form>';
        // $('#questionbank').append(copy);
        // $('.custom-accordion-new_'+$scope.newQuestion+' .panel-heading-new_'+$scope.newQuestion+'').click(function() {
        //   $(this).toggleClass('active');
        //   $(this).next('.panel-collapse').slideToggle();
        // });
        // console.log($('#questionbank > form').length);
    }
    /* For textbox */

    $(document).off().on('click', '.text_option', function(event) {
        var id = $(this).attr('id');
        id = id.replace("text_",'');
        $scope.optionId = $('#optionList_'+id+' > div').length;
        var lastdiv = $('#optionList_'+id+' > div').last();
        lastdiv = lastdiv.last();
        if($scope.optionId==0){
            $scope.optionId = 1;
        } else {
            lastdivID = lastdiv[0].id;
            lastdivID = lastdivID.split('_');
            lastdivID = lastdivID[2]++;
            $scope.optionId = lastdivID+1;
        }
        var formId = $(this).closest("form").attr('id');
        formId = formId.split('_').pop();
        var copy = '<div id="divNumber_'+formId+'_'+$scope.optionId+'"><div class="form-group ui-state-default optionDiv" id="optionText_'+$scope.optionId+'"> <div class="row"><div class="col-sm-3"><label class="label">Option #'+$scope.optionId+'</label></div> <div class="col-sm-9"><div class="fields"><input type="hidden" value="'+$scope.optionId+'" class="form-control" name="option['+$scope.optionId+'][order]"><input type="hidden" value="text" class="form-control" name="option['+$scope.optionId+'][type]"><input type="hidden" value="new" class="form-control" name="option['+$scope.optionId+'][which]"><input type="text" class="form-control" name="option['+$scope.optionId+'][text]" placeholder="Option text"> <a href="javascript:;" class="icon"><i class="fa fa-trash-o remove_option_text" id="textremove_'+formId+'_'+$scope.optionId+'" aria-hidden="true"></i></a> </div></div></div></div></div>';
        
        // var option = '<span class="custom-checkbox1" id="correctoption_'+formId+'_'+$scope.optionId+'"><input type="checkbox" name="correctAnswers['+$scope.optionId+'][value]" value="'+$scope.optionId+'" id="answer_'+$scope.optionId+'_'+$scope.questionId+'" /><label for="answer_'+$scope.optionId+'_'+$scope.questionId+'">Options #'+$scope.optionId+'</label> <input type="hidden" name="correctAnswers['+$scope.optionId+'][which]" value="new"> </span>';
        var option = '<div id="correctoption_'+formId+'_'+$scope.optionId+'" class="custom_checkbox_div"><input type="checkbox" name="correctAnswers['+$scope.optionId+'][value]" value="'+$scope.optionId+'" id="answer_'+$scope.optionId+'_'+$scope.questionId+'" /><span class="custom_checkbox_span"></span><span class="text">Options #'+$scope.optionId+ '</span><input type="hidden" name="correctAnswers['+$scope.optionId+'][which]" value="new"></div>';
        $('#optionList_'+id).append(copy);
        $('#correctAnswer_'+id).append(option);
    });
    function removeTextOptionMethod(){
        var id = $scope.textoptionId;
        id = id.replace("textremove_",'');
        id = id.split('_');
        console.log("divNumber_"+id[0]+"_"+id[1]);
        console.log("correctoption_"+id[0]+"_"+id[1]);
        $("#divNumber_"+id[0]+"_"+id[1]).remove();
        var optionval = $("#correctoption_"+id[0]+"_"+id[1]+" > input").val();
        var optionvalhidden = $("#correctoption_"+id[0]+"_"+id[1]+" > input:hidden").val();
        $("#correctoption_"+id[0]+"_"+id[1]).remove();
        if(optionvalhidden=='old')
            $('<input>').attr({type: 'hidden',name: 'removeOption[]', value:''+optionval+''}).appendTo('#questionform_'+id[0]);
    }   
    $(document).on('click', '.remove_option_text', function(event) {
        $scope.textoptionId = $(this).attr('id');
        confirmDialogue('','','',removeTextOptionMethod);
    });
    /* For Image */
    $(function(){
        $(document).on('click', '.image_option', function(event) {
            var id = $(this).attr('id');
            id = id.replace("image_",'');
            $scope.optionId = $('#optionList_'+id+' > div').length;
            var lastdiv = $('#optionList_'+id+' > div').last();
            lastdiv = lastdiv.last();
            if($scope.optionId==0){
                $scope.optionId = 1;
            } else {
                lastdivID = lastdiv[0].id;
                lastdivID = lastdivID.split('_');
                lastdivID = lastdivID[2]++;
                $scope.optionId = lastdivID+1;
            }
            var formId = $(this).closest("form").attr('id');
            formId = formId.split('_').pop();
            var copy = '<div id="divNumber_'+formId+'_'+$scope.optionId+'"><div class="form-group ui-state-default optionDiv" id="optionImage_'+$scope.optionId+'"><div class="row image-fields"><div class="col-sm-3"><label class="label">Option #'+$scope.optionId+'</label></div> <div class="col-xs-6 col-sm-9 ui-state-default"> <input type="file" class="hide" id="imageupload_'+$scope.optionId+'_'+$scope.questionId+'" name="option['+$scope.optionId+'][file]" accept="image/*"><input type="hidden" value="'+$scope.optionId+'" class="form-control" name="option['+$scope.optionId+'][order]"><input type="hidden" value="image" class="form-control" name="option['+$scope.optionId+'][type]"><input type="hidden" value="new" class="form-control" name="option['+$scope.optionId+'][which]"><div style="background-image:url(resources/assets/images/up-img.png);" id="imageuploadclick_'+$scope.optionId+'_'+$scope.questionId+'" class="img-div imageuploadclick"> <a href="javascript:;" id="imageremove_'+formId+'_'+$scope.optionId+'" class="icon remove_option_image"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div></div></div></div></div>';

            // var option = '<span class=" custom-checkbox1" id="correctoption_'+formId+'_'+$scope.optionId+'"><input type="checkbox" name="correctAnswers['+$scope.optionId+'][value]" value="'+$scope.optionId+'" id="answer_'+$scope.optionId+'_'+$scope.questionId+'" /><label for="answer_'+$scope.optionId+'_'+$scope.questionId+'">Options #'+$scope.optionId+'</label> <input type="hidden" name="correctAnswers['+$scope.optionId+'][which]" value="new"></span>';
            var option = '<div id="correctoption_'+formId+'_'+$scope.optionId+'" class="custom_checkbox_div"><input type="checkbox" name="correctAnswers['+$scope.optionId+'][value]" value="'+$scope.optionId+'" id="answer_'+$scope.optionId+'_'+$scope.questionId+'" /><span class="custom_checkbox_span"></span><span class="text">Options #'+$scope.optionId+ '</span><input type="hidden" name="correctAnswers['+$scope.optionId+'][which]" value="new"></div>';
            $('#optionList_'+id).append(copy);
            $('#correctAnswer_'+id).append(option);
        });
    });
    function removeImageOptionMethod(){
        var id = $scope.imageoptionId;
        id = id.replace("imageremove_",'');
        id = id.split('_');
        $("#divNumber_"+id[0]+"_"+id[1]).remove();
        var optionval = $("#correctoption_"+id[0]+"_"+id[1]+" > input").val();
        var optionvalhidden = $("#correctoption_"+id[0]+"_"+id[1]+" > input:hidden").val();
        $("#correctoption_"+id[0]+"_"+id[1]).remove();
        if(optionvalhidden=='old')
            $('<input>').attr({type: 'hidden',name: 'removeOption[]', value:''+optionval+''}).appendTo('#questionform_'+id[0]);
    }   
    $(document).on('click', '.remove_option_image', function(event) {
        $scope.imageoptionId = $(this).attr('id');
        confirmDialogue('','','',removeImageOptionMethod);
        return false;
    });
    $(document).on('click', '.imageuploadclick', function(event) {
        var id = $(this).attr('id');
        console.log(id);
        previewId = id;
        id = id.replace("imageuploadclick_",'imageupload_');
        $('#'+id).click();
        $('#'+id).change(function() {
          readURL(this,id,previewId);
        });
    });

    function readURL(input,id,previewId) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#'+previewId).css('background-image', "url("+e.target.result+")");
                $('#'+previewId).addClass('quiz_image_option');
            }
        reader.readAsDataURL(input.files[0]);
        }
    }
    /* For audio clip */
    $(function(){
        $(document).on('click', '.audio_option', function(event) {
            var id = $(this).attr('id');
            id = id.replace("audio_",'');
            $scope.optionId = $('#optionList_'+id+' > div').length;
            var lastdiv = $('#optionList_'+id+' > div').last();
            lastdiv = lastdiv.last();
            if($scope.optionId==0){
                $scope.optionId = 1;
            } else {
                lastdivID = lastdiv[0].id;
                lastdivID = lastdivID.split('_');
                lastdivID = lastdivID[2]++;
                $scope.optionId = lastdivID+1;
            }
            var formId = $(this).closest("form").attr('id');
            formId = formId.split('_').pop();
            var copy = '<div id="divNumber_'+formId+'_'+$scope.optionId+'"><div class="form-group ui-state-default optionDiv" id="optionAudioClip_'+$scope.optionId+'"><div class="row"><div class="col-sm-3"><label class="label">Option #'+$scope.optionId+'</label></div> <div class="col-sm-9"> <div class="fileinput fileinput-new input-group" data-provides="fileinput"><div class="form-control" data-trigger="fileinput"><span class="fileinput-filename"></span></div><span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists disable_color">Change</span><input type="file" name="option['+$scope.optionId+'][file]" accept="audio/*"></span><a href="#" class="input-group-addon btn btn-default fileinput-exists disable_color" data-dismiss="fileinput">Remove</a><a href="javascript:;" class="icon fields"><i class="fa fa-trash-o remove_option_audio" id="audioremove_'+formId+'_'+$scope.optionId+'" aria-hidden="true"></i></a></div><input type="hidden" value="'+$scope.optionId+'" class="form-control" name="option['+$scope.optionId+'][order]"><input type="hidden" value="audio" class="form-control" name="option['+$scope.optionId+'][type]"><input type="hidden" value="new" class="form-control" name="option['+$scope.optionId+'][which]"> </div></div></div></div>';
            
            // var option = '<span class=" custom-checkbox1" id="correctoption_'+formId+'_'+$scope.optionId+'"><input type="checkbox" name="correctAnswers['+$scope.optionId+'][value]" value="'+$scope.optionId+'" id="answer_'+$scope.optionId+'_'+$scope.questionId+'" /><label for="answer_'+$scope.optionId+'_'+$scope.questionId+'">Options #'+$scope.optionId+'</label> <input type="hidden" name="correctAnswers['+$scope.optionId+'][which]" value="new"> </span>';
            var option = '<div id="correctoption_'+formId+'_'+$scope.optionId+'" class="custom_checkbox_div"><input type="checkbox" name="correctAnswers['+$scope.optionId+'][value]" value="'+$scope.optionId+'" id="answer_'+$scope.optionId+'_'+$scope.questionId+'" /><span class="custom_checkbox_span"></span><span class="text">Options #'+$scope.optionId+ '</span><input type="hidden" name="correctAnswers['+$scope.optionId+'][which]" value="new"></div>';
                $('#optionList_'+id).append(copy);
                $('#correctAnswer_'+id).append(option);
            $('.fileinput').fileinput();
        });
    });
    function removeAudioOptionMethod(){
        var id = $scope.audiooptionId;
        console.log(id);
        id = id.replace("audioremove_",'');
        id = id.split('_');
        $("#divNumber_"+id[0]+"_"+id[1]).remove();
        var optionval = $("#correctoption_"+id[0]+"_"+id[1]+" > input").val();
        var optionvalhidden = $("#correctoption_"+id[0]+"_"+id[1]+" > input:hidden").val();
        $("#correctoption_"+id[0]+"_"+id[1]).remove();
        if(optionvalhidden=='old')
            $('<input>').attr({type: 'hidden',name: 'removeOption[]', value:''+optionval+''}).appendTo('#questionform_'+id[0]);
    }   
    $(document).on('click', '.remove_option_audio', function(event) {
        $scope.audiooptionId = $(this).attr('id');
        confirmDialogue('','','',removeAudioOptionMethod);
    });

    $(function(){
        $(document).on('click', '.savequestion', function(event) {
            var id = $(this).attr('id');
            console.log(id);
            id = id.split('_');
            button = id[0];
            id = id[1];
            var newQuestion = new FormData($("#questionform_"+id)[0]);
            newQuestion.append('question_order',$("#questionform_"+id).index()+1);
            showLoader('#saveQuestion_'+id,'Please wait');
            showLoader('#'+button+'_'+id,'Please wait');
            adminignitefactory.Addnewquestion(newQuestion).then(function(response) {
                hideLoader('#saveQuestion_'+id,'Save Question');
                hideLoader('#'+button+'_'+id,'Save Question');
                if (response.data.code == 2) window.location.replace("manage/login");
                if (response.data.status == 1) {
                    Disable('#'+button+'_'+id);
                    $("#questionform_"+id+" :input").prop("disabled", true);
                    $('#text_'+id).attr("disabled","disabled");
                    $('#image_'+id).attr("disabled","disabled");
                    $('#audio_'+id).attr("disabled","disabled");
                    $('#questionform_'+id).find('.icon').remove();
                    $('#questionform_'+id).find('.fileinput-exists').attr("disabled","disabled");
                    $('#questionform_'+id).find('#checkcircle_'+id).removeClass('hide');
                    $scope.questionSaved = true;
                } else {
                    simpleAlert('error', '', response.data.message);
                }
            });
        });
        $(document).on('click', '.savequestionOld', function(event) {
            var id = $(this).attr('id');
            console.log(id);
            id = id.split('_');
            button = id[0];
            id = id[1];
            var newQuestion = new FormData($("#questionform_"+id)[0]);
            newQuestion.append('question_order',$("#questionform_"+id).index()+1);
            newQuestion.append('quiz_id',$scope.quizid);
            newQuestion.append('may_be_new_question',true);
            showLoader('#'+button+'_'+id,'Please wait');
            adminignitefactory.Editquestion(newQuestion).then(function(response) {
                hideLoader('#'+button+'_'+id,'Save Question');
                if (response.data.code == 2) window.location.replace("manage/login");
                if (response.data.status == 1) {
                    $scope.quizDetails();
                    
                    $('#questionform_'+id).find('#checkcircle_'+id).removeClass('hide');
                    $scope.questionSaved = true;
                } else {
                    simpleAlert('error', '', response.data.message);
                }
            });
        });
    });
    $scope.Checkquestionsavedornot = function(quiz_published_status,button_text){
        if($scope.questionSaved){
            $scope.Quizpublishdraft(quiz_published_status,button_text);
        } else {
            simpleAlert('error','','No question saved yet. \n Save atleast one question to Un-publish the quiz.');
        }
    }
    $scope.Quizpublishdraft = function(quiz_published_status,button_text){
        if($scope.alreadyDraftedPublished==undefined||$scope.alreadyDraftedPublished==false){
            showLoader('#'+button_text);
            adminignitefactory.Quizpublishdraft({'quiz_published_status':quiz_published_status,'quizId':$scope.quizId}).then(function(response) {
                hideLoader('#'+button_text,button_text);
                if (response.data.code == 2) window.location.replace("manage/login");
                if (response.data.status == 1) {
                    $("#Unpublish").attr("disabled","disabled");
                    $("#Draft").attr("disabled","disabled");
                    $scope.alreadyDraftedPublished = true;
                    simpleAlert('success', '', response.data.message);
                } else {
                    simpleAlert('error', '', response.data.message);
                }
            });
        } else {
            simpleAlert('error', '', 'Quiz has been already '+button_text);
        }
    }
    $scope.doneediting = function(){
        window.location.replace("manage/ignite/quiz-manager/"+$scope.quizData.quiz_published_status+"/"+$scope.quizData.category_id);
    }
    $scope.ChangeQuizStatus = function(quizId, status, elementId,buttonText){
        showLoader('#'+elementId);
        editDetails = new FormData($("#edit_form")[0]);
        editDetails.append('quiz_published_status',status);
        editDetails.append('quizId',quizId);
        // selectedZones = [];
        // $.each($("input[name='video_for_zones[]']:checked"), function(){
        //     selectedZones.push($(this). val());
        // });
        adminignitefactory.ChangeQuizStatus(editDetails).then(function(response) {             
            hideLoader('#'+elementId,buttonText);
            if (response.data.code == 2) window.location.replace("manage/login");
            if (response.data.status == 1) {              
                window.location.replace("manage/ignite/quiz-manager/"+$scope.quizData.quiz_published_status+"/"+$scope.quizData.category_id);
            } else {
                simpleAlert('error', '', response.data.message);
            }
        });        
    }
}]);