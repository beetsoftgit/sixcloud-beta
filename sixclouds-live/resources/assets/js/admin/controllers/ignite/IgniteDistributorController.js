/*
 Author  : Ketan Solanki (ketan@creolestudios.com)
 Date    : 19th July 2017
 Name    : IgniteDistributorController
 Purpose : All the functions for ignite distributor page
 */
angular.module('sixcloudAdminApp').controller('IgniteDistributorController', ['$http', '$scope', '$timeout', '$rootScope', '$filter', 'adminignitefactory', function ($http, $scope, $timeout, $rootScope, $filter, adminignitefactory) {
    $(".navbar-nav li").removeClass("active");
    $('#adminHeaderIgnite').addClass("active");
    $timeout(function () {
        $(".ignite-header li").removeClass("active");
        $('#ignite_distributors').addClass("active");
    }, 500);
    $scope.sort = 'referralAsc';    
    $scope.search = '';
    $scope.filterData = function (sort,search) {
        $scope.GetallDistributorData({
            'sort': sort,            
            'search': search
        }, 1);
    }
    $scope.GetallDistributorData = function (data, pageNumber) {
        adminignitefactory.GetallDistributorData(data, pageNumber).then(function (response) {            

            if(response.data.status == 1){
                $scope.allDistributors = response.data.data.data;           
                $scope.totalDistributors = response.data.data.total;
                $scope.paginate = response.data.data;
                $scope.currentDistributorsPaging = response.data.data.current_page;
                $scope.rangeDistributorspage = _.range(1, response.data.data.last_page + 1);                
            }else if(response.data.status == 0){
                $scope.allDistributors = response.data.data.data;           
                $scope.totalDistributors = 0;
                $scope.paginate = {};
                $scope.currentDistributorsPaging = 0;
                $scope.rangeDistributorspage = _.range(1, 1);                
            }            
        });
    }
    $scope.GetallDistributorData({ 'sort': $scope.sort, 'search': $scope.search}, 1);     
}]);