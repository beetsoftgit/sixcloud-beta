/*
 Author  : Ketan Solanki (ketan@creolestudios.com)
 Date    : 13th August 2018
 Name    : WorkSheetManagerController
 Purpose : All the functions for worksheet manager page
 */
angular.module('sixcloudAdminApp').controller('WorkSheetManagerController', ['$http', '$scope', '$routeParams', '$timeout', '$rootScope', '$filter', 'adminignitefactory', function ($http, $scope, $routeParams, $timeout, $rootScope, $filter, adminignitefactory) {    
    console.log('WorkSheetManagerController loaded');
    $(".navbar-nav li").removeClass("active");
    $('#adminHeaderIgnite').addClass("active");
    $scope.status = $routeParams.status;
    $scope.category = $routeParams.category;
    $timeout(function () {
        $(".ignite-header li").removeClass("active");
        $('#worksheet_manger').addClass("active");
    }, 500);

    $scope.publishTypes = [{"value":1,"name":"Published WorkSheets"},{"value":2,"name":"Unpublished WorkSheets"}, {"value":3,"name":"Drafts"}];
    /* Old Version : Start */
        /*$scope.buzzTypes           = [{"value":1,"name":"Buzz 1"},{"value":2,"name":"Buzz 2"}, {"value":3,"name":"Buzz 3"}];
        if($scope.status==undefined && $scope.category==undefined){
            $scope.type                = $scope.publishTypes[0].value;
            $scope.buzzType            = $scope.buzzTypes[0].value;
        } else {
            $scope.type           = $scope.status;
            $scope.buzzType       = $scope.category;
        }
        $scope.GetAllWorkSheetData = function (data, pageNumber) {
            adminignitefactory.GetAllWorkSheetData(data, pageNumber).then(function (response) {            
                if (response.data.status == 1) {
                    $scope.allWorksheetContent = response.data.data.data;
                    $scope.totalWorksheet = response.data.data.total;
                    $scope.paginate = response.data.data;
                    $scope.currentWorksheetPaging = response.data.data.current_page;
                    $scope.rangeWorksheetpage = _.range(1, response.data.data.last_page + 1);     
                    $("#PublishedQuizzesTab").css("display", "block");           
                    $(".tablinks").removeClass("active");
                    $("#quizType_"+$scope.type).addClass("active");
                    $("#buzzType_"+$scope.buzzType).addClass("active");
                }
            });
        }
        $scope.GetAllWorkSheetData({ 'type': $scope.type,'worksheet_category_id':$scope.buzzType  }, 1);
        $scope.filterData = function (type, page) {        
            $scope.type =type;
            $scope.GetAllWorkSheetData({ 'type': $scope.type,'worksheet_category_id':$scope.buzzType  }, page);
        }  
        $scope.filterDataBuzz = function (buzz, page) {
            $scope.buzzType =buzz;
            $scope.GetAllWorkSheetData({ 'type': $scope.type,'worksheet_category_id':$scope.buzzType  }, page);
        }*/  
    /* Old Version : End */
    /* New Version : Start */
        $scope.getWorksheets = {};
        adminignitefactory.Getzones().then(function (response) {
            if (response.data.status == 1) {
                $scope.zones = response.data.data.zones;
                $scope.getWorksheets.type = $scope.publishTypes[0].value;
                $scope.type = $scope.getWorksheets.type;
                $scope.getWorksheets.zone = $scope.zones[0].id;
                $scope.getSubjects($scope.getWorksheets.zone);
                $scope.GetAllWorkSheetData($scope.getWorksheets, 1);   
            }
        });
        // adminignitefactory.Getcategories().then(function (response) {
        //     if (response.data.status == 1) {
        //         $scope.categories = undefined;
        //         $scope.subCategories = undefined;
        //         // $scope.subjects = response.data.data.categories;
        //     }
        // });
        $scope.Getcategories = function(subject) {
                adminignitefactory.Loadcategoriesforsubject({'subject_id':subject}).then(function (response) {
                if (response.data.status == 1) {
                    $scope.categories = undefined;
                    $scope.subCategories = undefined;
                    $scope.getWorksheets.category = undefined;
                    $scope.getWorksheets.subcategory = undefined;
                    $scope.categories = response.data.data.categories;
                }
            });
        }
        $scope.Getsubcategories = function(category, subCatOrNot) {
            if(subCatOrNot == 1) {
                $scope.getWorksheets.subcategory = undefined;
            }
            if(category != null) {
                adminignitefactory.Getsubcategories({'id':category, 'for':'Worksheet'}).then(function (response) {
                    if(response.data.status == 1){
                        $scope.subCategories = undefined;
                        $scope.getWorksheets.subcategory = undefined;
                        $scope.subCategories = response.data.data;
                        $scope.subCategoriesExist = $scope.subCategories.length>0?true:false;
                        $scope.selectSubcategory = true;
                    } else if(response.data.status == 0) {
                        if(response.data.code==2){
                            window.location.replace("manage/login");
                        } else if(response.data.code==308) {
                            if(subCatOrNot != 0) {
                                $scope.subCategoriesExist = false;
                            }
                            $scope.selectSubcategory = false;
                        } else {
                            if(subCatOrNot != 0) {
                                $scope.subCategoriesExist = false;
                            }
                            simpleAlert('error', '', response.data.message);
                        }
                    }
                });
            }
        }
        $scope.selectSubcategory = true;
        $scope.GetAllWorkSheetData = function (data, pageNumber) {
            adminignitefactory.GetAllWorkSheetData(data, pageNumber).then(function (response) {            
                if (response.data.status == 1) {
                    $scope.allWorksheetContent = response.data.data.data;
                    $scope.totalWorksheet = response.data.data.total;
                    $scope.paginate = response.data.data;
                    $scope.currentWorksheetPaging = response.data.data.current_page;
                    $scope.rangeWorksheetpage = _.range(1, response.data.data.last_page + 1);     
                    $("#PublishedQuizzesTab").css("display", "block");           
                    $(".tablinks").removeClass("active");
                    $("#quizType_"+$scope.type).addClass("active");
                    $("#buzzType_"+$scope.buzzType).addClass("active");
                }
            });
        }
        $scope.GetVideos = function(getWorksheets){
            // if($scope.status==undefined){
            //     getWorksheets.type = $scope.publishTypes[0].value;
            //     $scope.type = $scope.publishTypes[0].value;
            // }
            getWorksheets.subCategoryExists = $scope.selectSubcategory;
            $scope.GetAllWorkSheetData(getWorksheets, 1);
        }
        $scope.filterData = function (type, page) {
            $scope.type = type;
            $scope.getWorksheets.type = type;
            $scope.GetAllWorkSheetData($scope.getWorksheets, page);
        }
    /* New Version : End */  
    $scope.downloadWorksheetfile =function(fileName){
        var filePath = BASEURL + "public/uploads/worksheet_files/" + fileName;
        var http = new XMLHttpRequest();         
        http.open('HEAD', filePath, false);
        http.send();
        if (http.status == 200) {
            window.location = 'download-worksheet-file?filename=' + fileName;
        } else {
             simpleAlert('error', 'Some Error Occured', 'File Not Found', true);
        }       
    };
    $scope.getSubjects = function(zone) {
        if(zone != '') {
            adminignitefactory.Getcategories({'zone_id':zone}).then(function (response) {
            if (response.data.code == 2) window.location.replace("manage/login");
                if (response.data.status == 1) {
                    $scope.subjects = response.data.data.categories;
                }
            });
        } else {
            $scope.subjects = undefined;
        }
    }
}]);