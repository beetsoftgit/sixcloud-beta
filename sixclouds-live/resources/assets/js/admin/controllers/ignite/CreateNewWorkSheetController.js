/*
 Author  : Ketan Solanki (ketan@creolestudios.com)
 Date    : 14th August 2018
 Name    : CreateNewWorkSheetController
 Purpose : realted to creating new Work Sheet
 */
angular.module('sixcloudAdminApp').controller('CreateNewWorkSheetController', [ '$http', 'adminignitefactory', '$scope', '$timeout', '$rootScope', '$filter', function( $http, adminignitefactory, $scope, $timeout, $rootScope, $filter) {    
    $(".navbar-nav li").removeClass("active");
    $('#adminHeaderIgnite').addClass("active");
    $timeout(function () {
        $(".ignite-header li").removeClass("active");
        $('#worksheet_manger').addClass("active");
    }, 500);     

    // adminignitefactory.Getvideocategories().then(function (response) {
    //     if (response.data.code == 2) window.location.replace("sixteen-login");
    //     if (response.data.status == 1) {
    //         $scope.videoCategories = response.data.data;
    //     }
    // });
    adminignitefactory.Getzones().then(function (response) {
        if (response.data.code == 2) window.location.replace("manage/login");
        if (response.data.status == 1) {
            $scope.zones = response.data.data.zones;
        }
    });
    // adminignitefactory.Getcategories().then(function (response) {
    //     if (response.data.code == 2) window.location.replace("manage/login");
    //     if (response.data.status == 1) {
    //         $scope.subjects = response.data.data.categories;
    //     }
    // });

    $scope.getSubjects = function(count) {
        adminignitefactory.Getcategories({'zone_id':$scope.selectedZones.selected}).then(function (response) {
            if (response.data.code == 2) window.location.replace("manage/login");
            if (response.data.status == 1) {
                if(count == 1) {
                    $scope.videoCategories = undefined;
                    $scope.subCategories = undefined;
                }
                $scope.subjects = response.data.data.categories;
                if($scope.subjects) {
                    $scope.subjects.forEach(function(e){
                        e.zones = e.zones.split(',');
                        var index = e.zones.indexOf($scope.selectedZones.selected);
                        if (index !== -1) e.zones.splice(index, 1);
                        e.otherZones = e.zones;
                    });
                }
            }
        });
    }

    $scope.prerequisitesCounter = 1;
    $scope.showMoreDisable = false;
    // $scope.Loadprerequisites = function (id) {
    //     adminignitefactory.Loadprerequisites({ 'video_category_id': id }).then(function (response) {
    //         if (response.data.code == 2) window.location.replace("sixteen-login");
    //         if (response.data.status == 1) {
    //             $scope.prerequisitesCounter = 1;
    //             $scope.prerequisitesQuizCounter = 1;
    //             $('#prerequisitesDiv').empty();
    //             $('#preRequisitesQuizesDiv').empty();
    //             $scope.video_ordering = response.data.data.lastSequence;
    //             $scope.preRequisites = response.data.data.preRequisites;
    //             abc = response.data.data.preRequisites;
    //             if ($scope.preRequisites.length > 1) {
    //                 $scope.showMoreDisable = true;
    //             } else {
    //                 $scope.showMoreDisable = false;
    //             }
    //             $scope.preRequisitesBackup = response.data.data.preRequisites;
    //             $scope.quiz_ordering = response.data.data.lastSequence;
    //             $scope.preRequisitesQuizes = response.data.data.preRequisites;
    //             if ($scope.preRequisitesQuizes.length > 1) {
    //                 $scope.showMoreDisableQuiz = true;
    //             } else {
    //                 $scope.showMoreDisableQuiz = false;
    //             }
    //             $scope.worksheet_ordering = response.data.data.lastSequence;
    //             return false;
    //         }
    //     });
    // } 
    $scope.Loadprerequisites = function (id) {
        adminignitefactory.Loadprerequisites({ 'video_category_id': id }).then(function (response) {
            if (response.data.code == 2) window.location.replace("manage/login");
            if (response.data.status == 1) {
                if(response.data.data.yesSubcategories){
                    $scope.video_ordering =  undefined;
                    $scope.subCategoriesPresent = 1;
                    $scope.subCategories = response.data.data.yesSubcategories.subCategories;
                } else {
                    $scope.subCategories = undefined;
                    $scope.prerequisitesCounter = 1;
                    $scope.prerequisitesQuizCounter = 1;
                    $('#prerequisitesDiv').empty();
                    $('#preRequisitesQuizesDiv').empty();
                    $scope.video_ordering = response.data.data.noSubcategories.lastSequence;
                    $scope.preRequisites = response.data.data.noSubcategories.preRequisites;
                    abc = response.data.data.noSubcategories.preRequisites;
                    if ($scope.preRequisites.length > 1) {
                        $scope.showMoreDisable = true;
                    } else {
                        $scope.showMoreDisable = false;
                    }
                    $scope.preRequisitesBackup = response.data.data.noSubcategories.preRequisites;
                    $scope.quiz_ordering = response.data.data.noSubcategories.lastSequence;
                    $scope.preRequisitesQuizes = response.data.data.noSubcategories.preRequisites;
                    if ($scope.preRequisitesQuizes.length > 1) {
                        $scope.showMoreDisableQuiz = true;
                    } else {
                        $scope.showMoreDisableQuiz = false;
                    }
                    $scope.worksheet_ordering = response.data.data.noSubcategories.lastSequence;
                    return false;
                }
            }
        });
    }
    $scope.Loadcategoriesforsubject = function(subjectId){
        $scope.subCategories =  undefined;
        $scope.subCategoriesPresent = 0;
        $scope.video_ordering =  undefined;
        adminignitefactory.Loadcategoriesforsubject({ 'subject_id': subjectId }).then(function (response) {
            if (response.data.code == 2) window.location.replace("manage/login");
            if (response.data.status == 1) {
                $scope.videoCategories = response.data.data.categories;
                $scope.worksheetLanguages = response.data.data.languages;
                if($scope.worksheetLanguages.length>0){
                    $scope.showTitleDescriptionDiv = true;
                }
            }
        });
    }
    $scope.selectedZones = [];
    $scope.Addvideolanguages = function(){
        if($scope.selectedZones.length==0){
            simpleAlert('error', '', 'Select Zone(s)');
            return;
        } else {
            adminignitefactory.Loadlanguagesforzones({ 'selected_zones': $scope.selectedZones }).then(function (response) {
                if (response.data.code == 2) window.location.replace("manage/login");
                if (response.data.status == 1) {
                    $scope.worksheetLanguages = response.data.data;
                    console.log($scope.worksheetLanguages);
                    if($scope.worksheetLanguages.length>0){
                        $scope.showTitleDescriptionDiv = true;
                    }
                }
            });
        }
    }
    $scope.Uploadnewworksheet = function (status, buttonText, elementId) {                 
        var selectedCategory = $("input[name='worksheet_category_id']:checked").val(); 
        var fileElement = document.getElementById("worksheet_file");        
        var files = fileElement.files;                        
        // if ($scope.worksheet_title == undefined || $scope.description == undefined || $scope.worksheet_ordering == undefined || files.length == 0 || selectedCategory == "" || selectedCategory == undefined || $scope.worksheet_status == undefined) {
        if (files.length == 0 || selectedCategory == "" || selectedCategory == undefined || $scope.worksheet_status == undefined) {
            // if ($scope.worksheet_title == undefined) {
            //     simpleAlert('success', '', 'Worksheet "title" is required');
            //     return;
            // }
            // if ($scope.description == undefined) {
            //     simpleAlert('success', '', 'Worksheet "description" is required');
            //     return;
            // }            
            if (selectedCategory == undefined || selectedCategory == "") {
                simpleAlert('success', '', 'Worksheet "category" is required');
                return;
            }
            /*if ($scope.worksheet_ordering == undefined) {
                simpleAlert('success', '', 'Worksheet "position" is required');
                return;
            }*/
            if ($scope.worksheet_status == undefined) {
                simpleAlert('success', '', '"Worksheet Access" is required');
                return;
            }
            if (files.length == 0) {
                simpleAlert('success', '', 'Worksheet "file" is required');
                return;
            }             
        } else if(files.length > 0 && files[0].type != 'application/pdf'){
            simpleAlert('success', '', 'File format not allowed for worksheet file. (only PDF format allowed)');
            return;
        }else {
            newWorksheet = new FormData($("#newWorkSheetForm")[0]);
            newWorksheet.append("status",status);
            newWorksheet.append("subCategoriesPresent",$scope.subCategoriesPresent);
            newWorksheet.append('video_for_zones[]',$scope.selectedZones.selected);
            showLoader("#"+elementId);
            adminignitefactory.Uploadnewworksheet(newWorksheet).then(function (response) {                                                 
                hideLoader("#"+elementId, buttonText);
                if (response.data.code == 2) window.location.replace("sixteen-login");
                if (response.data.status == 1) {
                    window.location.replace("manage/ignite/worksheet-manager");
                } else {
                    simpleAlert('error','',response.data.message);
                }
            });
        }
    }

    $(document).on('click', '#video_thumbnail_select', function(event) {
        $('#video_thumbnail').click();
    });
        $('#video_thumbnail').change(function() {
            if(event.target.files.length>0){
                if(event.target.files[0].type == 'image/jpeg' || event.target.files[0].type == 'image/png'||event.target.files[0].type == 'image/jpg')
                    readURL(this);
                else{
                    simpleAlert('error', '', 'Only jpg, png and jpeg file formats are allowed for "Video Thumbnail"');
                }
            } else {
                $('#video_thumbnail_select').css('background-image', "url(resources/assets/images/up-img.png)");
                $('#video_thumbnail_select').removeClass('quiz_image_option');
                $('#video_thumbnail').val('');
            }
        });

    function readURL(input) {
        if (input.files && input.files[0]&&input.files.length>0) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#video_thumbnail_select').css('background-image', "url("+e.target.result+")");
                $('#video_thumbnail_select').addClass('quiz_image_option');
            }
            reader.readAsDataURL(input.files[0]);
        } else {
            $('#video_thumbnail_select').css('background-image', "url(resources/assets/images/up-img.png)");
            $('#video_thumbnail_select').removeClass('quiz_image_option');
            $('#video_thumbnail').val('');
        }
    }
    
}]);