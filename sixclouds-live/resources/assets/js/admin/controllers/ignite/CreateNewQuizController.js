/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 13th July 2018
 Name    : CreateNewQuizController
 Purpose : realted to creating new quiz
 */
angular.module('sixcloudAdminApp').controller('CreateNewQuizController', [ '$http', 'adminignitefactory', '$scope', '$timeout', '$rootScope', '$filter', function( $http, adminignitefactory, $scope, $timeout, $rootScope, $filter) {
    $(document).ready(function() {
    console.log('CreateNewQuiz controller loaded');
        $(".navbar-nav li").removeClass("active");
        $('#adminHeaderIgnite').addClass("active"); 
    $timeout(function () {
        $(".ignite-header li").removeClass("active");
        $('#quiz_manger').addClass("active");

        $(document).ready(function() {
            $('.fileinput').fileinput();
        });
    }, 1000);
    $(function() {
        $( ".sortable" ).sortable();
        $( ".sortable" ).disableSelection();
    });
    $('.custom-accordion .panel-heading').click(function() {
      $(this).toggleClass('active');
      $(this).next('.panel-collapse').slideToggle();
    });
    // adminignitefactory.Getvideocategories().then(function (response) {
    //     if (response.data.code == 2) window.location.replace("manage/login");
    //     if (response.data.status == 1) {
    //         $scope.videoCategories = response.data.data;
    //     }
    // });
    $scope.showQuestionBank = false;
    $scope.Createquiztitle = function(){
        // if($scope.quiz_title == undefined||$scope.quiz_title == ''||$scope.category_id == undefined||$scope.category_id == ''){
        if($scope.quiz_title == undefined||$scope.quiz_title == ''){
            if($scope.quiz_title == undefined||$scope.quiz_title == ''){
                simpleAlert('error','','Enter quiz title');
                return;
            }
            /*if($scope.category_id == undefined||$scope.category_id == ''){
                simpleAlert('error','','Select category');
                return;
            }*/
        } else {
            showLoader('#quizTitle');
            adminignitefactory.Addquiztitle({'title':$scope.quiz_title,'category_id':$scope.category_id}).then(function(response) {
                hideLoader('#quizTitle','Create Questions');
                if (response.data.code == 2) window.location.replace("manage/login");
                if (response.data.status == 1) {
                    Disable('#quizTitle');
                    $('#quizTitleText').attr('disabled','disabled');
                    $('#category_id').attr('disabled','disabled');
                    $('<input>').attr({type: 'hidden',id: 'quiz_id',name: 'quiz_id', value:''+response.data.data.id+''}).appendTo('form');
                    $scope.quizId = response.data.data.id;
                    $scope.showQuestionBank = true;
                } else {
                    simpleAlert('error', '', response.data.message);
                }
            });
        }
    }
    $(".clickquestionImage").click(function() {
        var id = $(this).attr('id');
        previewId = id;
        id = id.replace("click",'');
        $('#'+id).click();
        $('#'+id).change(function() {
          readURLQuestion(this,id,previewId);
        });
    });

    /*$(document).on('click', '.clickquestionImage', function(event) {
        alert();
        var id = $(this).attr('id');
        previewId = id;
        id = id.replace("click",'');
        $('#'+id).click();
        $('#'+id).change(function() {
          readURLQuestion(this,id,previewId);
        });
    });*/

    function readURLQuestion(input,id,previewId) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#'+previewId).css('background-image', "url("+e.target.result+")");
                $('#'+previewId).addClass('quiz_image_option');
                trashId = id.replace('questionImage_','removequestionImage_');
                $('#'+trashId).removeClass('hide');
            }
        reader.readAsDataURL(input.files[0]);
        }
    }

    $(".question_remove_image").click(function() {
        var idque = $(this).attr('id');
        var removeidque = idque;
        idque = idque.replace("remove",'click');
        $('#'+idque).css('background-image', "url(resources/assets/images/up-img.png)");
        $('#'+idque).removeClass('quiz_image_option');
        $('#'+removeidque).addClass('hide');
        removeidque = removeidque.replace('remove','');
        $('#'+removeidque).val('');
        return false;
    });
    // $(document).on('click', '.question_remove_image', function(event) {
    //     var idque = $(this).attr('id');
    //     var removeidque = idque;
    //     idque = idque.replace("remove",'click');
    //     $('#'+idque).css('background-image', "url(resources/assets/images/up-img.png)");
    //     $('#'+idque).removeClass('quiz_image_option');
    //     $('#'+removeidque).addClass('hide');
    //     removeidque = removeidque.replace('remove','');
    //     $('#'+removeidque).val('');
    //     return false;
    // });

    $scope.questionId = 1;
    $scope.questionSaved = false;
    $scope.Addnewquestion = function(){
        var copy = $("#questionform").clone(true);
        copy.removeAttr('id');
        copy.attr('id','questionform_'+$scope.questionId);
        copy.removeClass('hide');
        $('#questionbank').append(copy);
        $scope.questionId++;
    }
    $timeout(function(){
        $scope.Addnewquestion();
    },500);
    /* For textbox */
    $(function(){
        //$('.text_option').click(function(){
        $(document).off().on('click', '.text_option', function(){
            /*$timeout(function () {
                $scope.$apply(function () {*/
                    var id = $(this).attr('id');
                    console.log(id);
                    id = id.replace("text_",'');
                    $scope.optionId = $('#optionList_'+id+' > div').length;
                    var lastdiv = $('#optionList_'+id+' > div').last();
                    lastdiv = lastdiv.last();
                    if($scope.optionId==0){
                        $scope.optionId = 1;
                    } else {
                        lastdivID = lastdiv[0].id;
                        lastdivID = lastdivID.split('_');
                        lastdivID = lastdivID[1]++;
                        $scope.optionId = lastdivID+1;
                    }
                    var formId = $(this).closest("form").attr('id');
                    formId = formId.split('_').pop();
                    var copy = '<div class="form-group ui-state-default" id="optionText_'+$scope.optionId+'"> <div class="row"><div class="col-sm-3"><label class="label">Option #'+$scope.optionId+'</label></div> <div class="col-sm-9"><div class="fields"><input type="hidden" value="'+$scope.optionId+'" class="form-control" name="option['+$scope.optionId+'][order]"><input type="hidden" value="text" class="form-control" name="option['+$scope.optionId+'][type]"><input type="text" class="form-control" name="option['+$scope.optionId+'][text]" placeholder="Option text"> <a href="javascript:;" class="icon"><i class="fa fa-trash-o remove_option_text" id="textremove_'+$scope.optionId+'_'+formId+'" aria-hidden="true"></i></a> </div></div></div></div>';
                    // var option = '<span class="custom-checkbox1" id="correctoption_'+$scope.optionId+'_'+formId+'"><input type="checkbox" name="correctAnswers[]" value="'+$scope.optionId+'" id="answer_'+$scope.optionId+'_'+$scope.questionId+'" /><label for="answer_'+$scope.optionId+'_'+$scope.questionId+'">Options #'+$scope.optionId+'</label> </span>';
                    var option = '<div id="correctoption_'+$scope.optionId+'_'+formId+'" class="custom_checkbox_div"><input type="checkbox" name="correctAnswers[]" value="'+$scope.optionId+'" id="answer_'+$scope.optionId+'_'+$scope.questionId+'" /><span class="custom_checkbox_span" id="correctoption_'+$scope.optionId+'_'+formId+'"></span><span class="text">Options #'+$scope.optionId+ '</span></div>';
                    $('#optionList_'+id).append(copy);
                    $('#correctAnswer_'+id).append(option);
                /*})
            }, 500);*/
        });
    });
    /*$(".remove_option_text").click(function() {
        var id = $(this).attr('id');
        id = id.replace("textremove_",'');
        id = id.split('_');
        $(this).closest("#optionText_"+id[0]).remove();
        $("#correctoption_"+id[0]+"_"+id[1]).remove();
    });*/
    $(document).ready(function() {
        $(document).on('click', '.remove_option_text', function(event) {
            var id = $(this).attr('id');
            id = id.replace("textremove_",'');
            id = id.split('_');
            $(this).closest("#optionText_"+id[0]).remove();
            $("#correctoption_"+id[0]+"_"+id[1]).remove();
        });
    });
    /* For Image */
    $(function(){
        $('.image_option').click(function(){
            var id = $(this).attr('id');
            id = id.replace("image_",'');
            $scope.optionId = $('#optionList_'+id+' > div').length;
            var lastdiv = $('#optionList_'+id+' > div').last();
            lastdiv = lastdiv.last();
            if($scope.optionId==0){
                $scope.optionId = 1;
            } else {
                lastdivID = lastdiv[0].id;
                lastdivID = lastdivID.split('_');
                lastdivID = lastdivID[1]++;
                $scope.optionId = lastdivID+1;
            }
            var formId = $(this).closest("form").attr('id');
            formId = formId.split('_').pop();
            var copy = '<div class="form-group ui-state-default" id="optionImage_'+$scope.optionId+'"><div class="row image-fields"><div class="col-sm-3"><label class="label">Option #'+$scope.optionId+'</label></div> <div class="col-xs-6 col-sm-9 ui-state-default"> <input type="file" class="hide" id="imageupload_'+$scope.optionId+'_'+$scope.questionId+'" name="option['+$scope.optionId+'][file]" accept="image/*"><input type="hidden" value="'+$scope.optionId+'" class="form-control" name="option['+$scope.optionId+'][order]"><input type="hidden" value="image" class="form-control" name="option['+$scope.optionId+'][type]"><div style="background-image:url(resources/assets/images/up-img.png);" id="imageuploadclick_'+$scope.optionId+'_'+$scope.questionId+'" class="img-div imageuploadclick"> <a href="javascript:;" id="imageremove_'+$scope.optionId+'_'+formId+'" class="icon remove_option_image"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div></div></div></div>';

            // var option = '<span class=" custom-checkbox1" id="correctoption_'+$scope.optionId+'_'+formId+'"><input type="checkbox" name="correctAnswers[]" value="'+$scope.optionId+'" id="answer_'+$scope.optionId+'_'+$scope.questionId+'" /><label for="answer_'+$scope.optionId+'_'+$scope.questionId+'">Options #'+$scope.optionId+'</label> </span>';
            var option = '<div id="correctoption_'+$scope.optionId+'_'+formId+'" class="custom_checkbox_div"><input type="checkbox" name="correctAnswers[]" value="'+$scope.optionId+'" id="answer_'+$scope.optionId+'_'+$scope.questionId+'" /><span class="custom_checkbox_span" id="correctoption_'+$scope.optionId+'_'+formId+'"></span><span class="text">Options #'+$scope.optionId+ '</span></div>';
            $('#optionList_'+id).append(copy);
            $('#correctAnswer_'+id).append(option);
        });
    });
    /*$(".remove_option_image").click(function() {
        var id = $(this).attr('id');
        id = id.replace("imageremove_",'');
        id = id.split('_');
        $(this).closest("#optionImage_"+id[0]).remove();
        $("#correctoption_"+id[0]+"_"+id[1]).remove();
    });*/
    $(document).ready(function() {
        $(document).on('click', '.remove_option_image', function(event) {
            var id = $(this).attr('id');
            id = id.replace("imageremove_",'');
            id = id.split('_');
            $(this).closest("#optionImage_"+id[0]).remove();
            $("#correctoption_"+id[0]+"_"+id[1]).remove();
        });
    });

    /*$(document).ready(function() {
        $(".imageuploadclick").click(function() {
            alert();
            var id = $(this).attr('id');
            previewId = id;
            id = id.replace("imageuploadclick_",'imageupload_');
            $('#'+id).click();
            $('#'+id).change(function() {
              readURL(this,id,previewId);
            });
        });
    });*/
    $(document).ready(function() {
        $(document).on('click', '.imageuploadclick', function(event) {
            var id = $(this).attr('id');
            previewId = id;
            id = id.replace("imageuploadclick_",'imageupload_');
            $('#'+id).click();
            $('#'+id).change(function() {
              readURL(this,id,previewId);
            });
        });
    });

    function readURL(input,id,previewId) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#'+previewId).css('background-image', "url("+e.target.result+")");
                $('#'+previewId).addClass('quiz_image_option');
            }
        reader.readAsDataURL(input.files[0]);
        }
    }
    /* For audio clip */
    $(function(){
        $('.audio_option').click(function(){
            var id = $(this).attr('id');
            id = id.replace("audio_",'');
            $scope.optionId = $('#optionList_'+id+' > div').length;
            var lastdiv = $('#optionList_'+id+' > div').last();
            lastdiv = lastdiv.last();
            if($scope.optionId==0){
                $scope.optionId = 1;
            } else {
                lastdivID = lastdiv[0].id;
                lastdivID = lastdivID.split('_');
                lastdivID = lastdivID[1]++;
                $scope.optionId = lastdivID+1;
            }
            var formId = $(this).closest("form").attr('id');
            formId = formId.split('_').pop();
            var copy = '<div class="form-group ui-state-default" id="optionAudioClip_'+$scope.optionId+'"><div class="row"><div class="col-sm-3"><label class="label">Option #'+$scope.optionId+'</label></div> <div class="col-sm-9"> <div class="fileinput fileinput-new input-group" data-provides="fileinput"><div class="form-control" data-trigger="fileinput"><span class="fileinput-filename"></span></div><span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists disable_color">Change</span><input type="file" name="option['+$scope.optionId+'][file]" accept="audio/*"></span><a href="#" class="input-group-addon btn btn-default fileinput-exists disable_color" data-dismiss="fileinput">Remove</a><a href="javascript:;" class="icon fields"><i class="fa fa-trash-o remove_option_audio" id="audioremove_'+$scope.optionId+'_'+formId+'" aria-hidden="true"></i></a></div><input type="hidden" value="'+$scope.optionId+'" class="form-control" name="option['+$scope.optionId+'][order]"><input type="hidden" value="audio" class="form-control" name="option['+$scope.optionId+'][type]"> </div></div></div>';
            // var option = '<span class=" custom-checkbox1" id="correctoption_'+$scope.optionId+'_'+formId+'"><input type="checkbox" name="correctAnswers[]" value="'+$scope.optionId+'" id="answer_'+$scope.optionId+'_'+$scope.questionId+'" /><label for="answer_'+$scope.optionId+'_'+$scope.questionId+'">Options #'+$scope.optionId+'</label> </span>';
            var option = '<div id="correctoption_'+$scope.optionId+'_'+formId+'" class="custom_checkbox_div"><input type="checkbox" name="correctAnswers[]" value="'+$scope.optionId+'" id="answer_'+$scope.optionId+'_'+$scope.questionId+'" /><span class="custom_checkbox_span" id="correctoption_'+$scope.optionId+'_'+formId+'"></span><span class="text">Options #'+$scope.optionId+ '</span></div>';
            $('#optionList_'+id).append(copy);
            $('#correctAnswer_'+id).append(option);
            $('.fileinput').fileinput();
        });
    });
    
    /*$(".remove_option_audio").click(function() {
        var id = $(this).attr('id');
        id = id.replace("audioremove_",'');
        id = id.split('_');
        $(this).closest("#optionAudioClip_"+id[0]).remove();
        $("#correctoption_"+id[0]+"_"+id[1]).remove();
    });*/
    $(document).ready(function() {
        $(document).on('click', '.remove_option_audio', function(event) {
            var id = $(this).attr('id');
            id = id.replace("audioremove_",'');
            id = id.split('_');
            $(this).closest("#optionAudioClip_"+id[0]).remove();
            $("#correctoption_"+id[0]+"_"+id[1]).remove();
        });
    });

    $(function(){
        $('.savequestion').click(function(){
            var id = $(this).attr('id');
            id = id.split('_');
            button = id[0];
            id = id[1];
            var newQuestion = new FormData($("#questionform_"+id)[0]);
            newQuestion.append('question_order',$("#questionform_"+id).index()+1);
            showLoader('#'+button+'_'+id,'Please wait');
            adminignitefactory.Addnewquestion(newQuestion).then(function(response) {
                hideLoader('#'+button+'_'+id,'Save Question');
                if (response.data.code == 2) window.location.replace("manage/login");
                if (response.data.status == 1) {
                    Disable('#'+button+'_'+id);
                    $("#questionform_"+id+" :input").prop("disabled", true);
                    $('#text_'+id).attr("disabled","disabled");
                    $('#image_'+id).attr("disabled","disabled");
                    $('#audio_'+id).attr("disabled","disabled");
                    $('#questionform_'+id).find('.icon').remove();
                    $('#questionform_'+id).find('.fileinput-exists').attr("disabled","disabled");
                    $('#questionform_'+id).find('#checkcircle_'+id).removeClass('hide');
                    $scope.questionSaved = true;
                } else {
                    simpleAlert('error', '', response.data.message);
                }
            });
        });
    });
    $scope.Checkquestionsavedornot = function(quiz_published_status,button_text){
        if($scope.questionSaved){
            $scope.Quizpublishdraft(quiz_published_status,button_text);
        } else {
            simpleAlert('error','','No question saved yet. \n Save atleast one question to publish the quiz.');
        }
    }
    $scope.Quizpublishdraft = function(quiz_published_status,button_text){
        if($scope.alreadyDraftedPublished==undefined||$scope.alreadyDraftedPublished==false){
            showLoader('#'+button_text);
            adminignitefactory.Quizpublishdraft({'quiz_published_status':quiz_published_status,'quizId':$scope.quizId}).then(function(response) {
                hideLoader('#'+button_text,button_text);
                if (response.data.code == 2) window.location.replace("manage/login");
                if (response.data.status == 1) {
                    $("#Unpublish").attr("disabled","disabled");
                    $("#Draft").attr("disabled","disabled");
                    $scope.alreadyDraftedPublished = true;
                    simpleAlert('success', '', response.data.message);
                } else {
                    simpleAlert('error', '', response.data.message);
                }
            });
        } else {
            simpleAlert('error', '', 'Quiz has been already '+button_text);
        }
    }
    });

    $scope.showChangeRemove = function(files, queId) {
        var queId = queId.split('_')[1];
        var queIdOld = queId-1;
        var fileName = files[0].name;
        $('#file_name_'+queIdOld).text(fileName);
        $('#questionChooseButton_'+queIdOld).addClass('hide');
        $('#questionChangeButton_'+queIdOld).removeClass('hide');
        $('#questionRemoveButton_'+queIdOld).removeClass('hide');
    }

    $scope.chooseFile = function(id) {
        $('#questionAudio_'+id).trigger('click');
    }

    $scope.removeFile = function(questionId) {
        var queId = questionId-1;
        $('#file_name_'+queId).text("");
        $('#questionAudio_'+queId).val("");
        $('#questionChooseButton_'+queId).removeClass('hide');
        $('#questionChangeButton_'+queId).addClass('hide');
        $('#questionRemoveButton_'+queId).addClass('hide');
    }

}]);