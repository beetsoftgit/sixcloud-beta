/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 4th July 2017
 Name    : AllContentController
 Purpose : All the functions for home page
 */
angular.module('sixcloudAdminApp').controller('AllContentController', ['$http', '$scope', '$timeout', '$rootScope', '$filter', 'adminignitefactory', function ($http, $scope, $timeout, $rootScope, $filter, adminignitefactory) {
    // $scope.sections = Geteltsections.data.data;
    console.log('AllContentController controller loaded');
    $(".navbar-nav li").removeClass("active");
    $('#adminHeaderIgnite').addClass("active");  
    $scope.publishTypes = [{"value":1,"name":"Published"}, {"value":3,"name":"Drafted"}]; 
    $scope.type = $scope.publishTypes[0].value;
    $scope.current_domain = current_domain; 
    $timeout(function () {
        $(".ignite-header li").removeClass("active");
        $('#all_content').addClass("active");
        $(".sortable").sortable();
        $(".sortable").on( "sortupdate", function( event, ui ) {            
            var idArray = $(this).sortable('toArray', { attribute: 'data-id'});                                                
            $scope.UpdateContentOrder(idArray);        
        } );
        $(".sortable").disableSelection();
    }, 500);


    // adminignitefactory.Getvideocategories().then(function (response) {
    //     if (response.data.code == 2) window.location.replace("sixteen-login");
    //     if (response.data.status == 1) {
    //         $scope.videoCategories = response.data.data;
    //         $scope.type = $scope.videoCategories[0].id;
    //     }
    // });
    /* New Version : Start */
        $scope.getAllData = {};
        adminignitefactory.Getzones().then(function (response) {
            if (response.data.status == 1) {
                $scope.zones = response.data.data.zones;
                /*$scope.getAllData.type = $scope.publishTypes[0].value;
                $scope.type = $scope.getAllData.type;*/
                $scope.getAllData.zone = $scope.zones[0].id;
                $scope.getSubjects($scope.getAllData.zone);
                $scope.GetAllContentData($scope.getAllData, 1);
            }
        });
        // adminignitefactory.Getcategories().then(function (response) {
        //     if (response.data.status == 1) {
        //         $scope.categories = undefined;
        //         $scope.subCategories = undefined;
        //         // $scope.subjects = response.data.data.categories;
        //     }
        // });
        $scope.Getcategories = function(subject) {
            adminignitefactory.Loadcategoriesforsubject({'subject_id':subject}).then(function (response) {
                if (response.data.status == 1) {
                    $scope.categories = undefined;
                    $scope.subCategories = undefined;
                    $scope.getAllData.category = undefined;
                    $scope.getAllData.subcategory = undefined;
                    $scope.categories = response.data.data.categories;
                }
            });
        }
        $scope.Getsubcategories = function(category, subCatOrNot) {
            if(subCatOrNot == 1) {
                $scope.getAllData.subcategory = undefined;
            }
            if(category != null) {
                adminignitefactory.Getsubcategories({'id':category, 'for':'all'}).then(function (response) {
                    if(response.data.status == 1){
                        $scope.subCategories = undefined;
                        $scope.getAllData.subcategory = undefined;
                        $scope.subCategories = response.data.data;
                        $scope.subCategoriesExist = $scope.subCategories.length>0?true:false;
                        $scope.selectSubcategory = true;
                    } else if(response.data.status == 0) {
                        if(response.data.code==2){
                            window.location.replace("manage/login");
                        } else if(response.data.code==308) {
                            if(subCatOrNot != 0) {
                                $scope.subCategoriesExist = false;
                            }
                            $scope.selectSubcategory = false;
                        } else {
                            if(subCatOrNot != 0) {
                                $scope.subCategoriesExist = false;
                            }
                            simpleAlert('error', '', response.data.message);
                        }
                    }
                });
            }
        }
        $scope.selectSubcategory = true;
        $scope.GetAllContentData = function (data, pageNumber) {
            adminignitefactory.GetAllContentData(data).then(function (response) {            
                if (response.data.status == 1) {
                    $scope.allData = response.data.data;
                    $scope.allContent = response.data.data.allData;             
                    $('#get_data_btn').blur();
                } else {
                    $scope.allData = undefined;
                    $scope.allContent = undefined;
                }
            });
        }

        $scope.GetVideos = function(getAllData){
            getAllData.subCategoryExists = $scope.selectSubcategory;
            $scope.GetAllContentData(getAllData, 1);
        }
        /*$scope.filterData = function (type, page) {
            $scope.type = type;
            $scope.getAllData.type = type;
            $scope.GetAllContentData($scope.getAllData, page);
        }*/
    /* New Version : End */
    
    /*$scope.GetAllContentData = function (data, pageNumber) {
        adminignitefactory.GetAllContentData(data).then(function (response) {            
            if (response.data.status == 1) {
                $scope.allData = response.data.data[0];
                $scope.allContent = response.data.data[0].allData;
                // $scope.totalContent = $scope.allContent.total;
                // $scope.paginate = $scope.allContent;
                // $scope.currentContentPaging = $scope.allContent.current_page;
                // $scope.rangeContentpage = _.range(1, $scope.allContent.last_page + 1);
                $("#PublishedQuizzesTab").css("display", "block");           
                $(".tablinks").removeClass("active");
                $("#quizType_"+$scope.type).addClass("active");                
            }
        });
    }*/
    // $scope.GetAllContentData({ 'categoryId': $scope.type }, 1);
    $scope.filterData = function (type, page) {        
        $scope.type =type;
        $scope.GetAllContentData({ 'categoryId': type, 'page': 1 }, page);
    }
    $scope.expand = function (index) {                
        $('#content_' + index).toggleClass('active');
        $('#content_' + index).next('.new-class-content').slideToggle();
    }
    $scope.ShowVideo = function(data){
        $('#showVideoModal').modal('show');
        var videoUrl = data.video_urls[0].video_oss_url;
        if(Hls.isSupported()){         
            var video = document.getElementById('videoTag');
            var hls = new Hls();
            hls.loadSource(videoUrl);
            hls.attachMedia(video);
            video.play();
            hls.on(Hls.Events.MANIFEST_PARSED,function()
            {         
                video.play();
            });
        } else if (video.canPlayType('application/vnd.apple.mpegurl')){            
            video.src = videoUrl;
            video.addEventListener('canplay',function()
            {
                video.play();
            });
        }         
    }
    $scope.UpdateContentOrder = function(idArray) {
        adminignitefactory.UpdateContentOrder(idArray).then(function(response) {             
            if(response.data.status==1){
                simpleAlert('success', 'Order Updated', response.data.message, true);                
            }else{
                simpleAlert('error', 'Some Error Occured', response.data.message, true);
            }
        });
    }  
    $scope.getSubjects = function(zone) {
        if(zone != '') {
            adminignitefactory.Getcategories({'zone_id':zone}).then(function (response) {
            if (response.data.code == 2) window.location.replace("manage/login");
                if (response.data.status == 1) {
                    $scope.subjects = response.data.data.categories;
                }
            });
        } else {
            $scope.subjects = undefined;
        }
    }
}]);