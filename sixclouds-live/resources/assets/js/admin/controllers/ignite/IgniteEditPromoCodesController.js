/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 20th Nov, 2018
 Name    : IgniteAddPromoCodesController
 Purpose : All the functions for promocode page
 */
angular.module('sixcloudAdminApp').controller('IgniteEditPromoCodesController', ['adminignitefactory','$http', '$scope', '$timeout', '$rootScope', '$filter', '$routeParams', function(adminignitefactory,$http, $scope, $timeout, $rootScope, $filter, $routeParams) {
    // $scope.sections = Geteltsections.data.data;
    console.log('IgnitePricingPlans controller loaded');
    $(".navbar-nav li").removeClass("active");
    $('#adminHeaderIgnite').addClass("active");
    $timeout(function(){
        $(".ignite-header li").removeClass("active");
        $('#ignite_promo_code').addClass("active");
    },500);
    $scope.current_domain = current_domain;
    $scope.slug = $routeParams.slug;
    $scope.GetData = function(){
        adminignitefactory.Getallbuzzcategories().then(function (response) {
            if (response.data.code == 2) window.location.replace("manage/login");
            if (response.data.status == 1) {
                $scope.categories = response.data.data;
            } else {
                simpleAlert('error','',response.data.message);
            }
        });
        adminignitefactory.Getalldistributors().then(function (response) {
            if (response.data.code == 2) window.location.replace("manage/login");
            if (response.data.status == 1) {
                $scope.distributors = response.data.data;
            } else {
                simpleAlert('error','',response.data.message);
            }
        });
        adminignitefactory.Getpromodetaildata({'slug':$scope.slug}).then(function (response) {
            if (response.data.code == 2) window.location.replace("manage/login");
            if (response.data.status == 1) {
                $scope.promoData = response.data.data;
                $scope.promo = {};
                $scope.promo.promo_id = $scope.promoData.id;
                $scope.promo.promo_code = $scope.promoData.promo_code;
                $scope.promo.remarks = $scope.promoData.remarks;
                $scope.promo.category = $scope.promoData.video_category_id.split(',');
                $scope.promo.distributor = $scope.promoData.distributor_id;
                $scope.promo.discount_type = $scope.promoData.discount_type;
                if($scope.promo.discount_type==0)
                    $scope.promo.discount_percent = $scope.promoData.discount_of;
                else
                    $scope.promo.discount_price = $scope.promoData.discount_of;
                $scope.promo.from_date = $scope.promoData.from_date;
                $scope.promo.to_date = $scope.promoData.to_date;
                $scope.promo.redemption_count = $scope.promoData.number_of_redemptions;
                $scope.promo.promo_type = $scope.promoData.promo_type;
                $scope.promo.redemption_type = $scope.promoData.redemption_type==1?true:false;

                $( function() {
                    $( ".fromTime" ).datetimepicker({
                        minDate: new Date(),
                        setDate: $scope.promo.from_date,
                    });
                    $( ".toTime" ).datetimepicker({
                        minDate: new Date(),
                        setDate: $scope.promo.to_date,
                    });
                });
                $timeout(function(){
                    $(document).ready(function() {
                        $('.languageselect').multiselect();
                        $('.languageselect').val($scope.promo.category);
                        $('.languageselect').multiselect("refresh");

                    });
                },500);
            } else {
                simpleAlert('error','',response.data.message);
            }
        });
    }
    $scope.GetData();
    $(document).ready(function() {
        $(".category_price").on("keypress keyup blur", function(event) {
            $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
            $scope.project_budget_error = false;
        });
    });

    $scope.Generatepromo = function(){
        adminignitefactory.Generatepromo().then(function (response) {
            if (response.data.code == 2) window.location.replace("manage/login");
            if (response.data.status == 1) {
                $scope.promo.promo_code = response.data.data;
            } else {
                simpleAlert('error','',response.data.message);
            }
        });
    }
    $scope.Radiochange = function(type){
        if(type==0)
            $scope.promo.discount_price = undefined;
        else
            $scope.promo.discount_percent = undefined;
    }
    $scope.Savepromo = function(promo){
        if(promo==undefined){
            simpleAlert('error', '', 'Enter appropriate data for promo code.');
            return;
        }
        promo.category = $('#category').val();
        /*if(promo.promo_code==undefined||promo.promo_code==''||promo.remarks==undefined||promo.remarks==''||promo.category==undefined||promo.category==''||promo.category.length==0||promo.discount_type==undefined||promo.from_date==undefined||promo.from_date==''||promo.to_date==undefined||promo.to_date==''||promo.redemption_count==undefined||promo.redemption_count==''||promo.promo_type==undefined){
            if(promo.promo_code==undefined||promo.promo_code==''){
                simpleAlert('error', '', 'Generate promo code.');
                return;
            }
            if(promo.remarks==undefined||promo.remarks==''){
                simpleAlert('error', '', 'Add remarks to promo code');
                return;
            }
            if(promo.category==undefined||promo.category==''||promo.category.length==0){
                simpleAlert('error', '', 'Select category for promo code');
                return;
            }
            if(promo.discount_type==undefined){
                simpleAlert('error', '', 'Select discount type');
                return;
            } else {
                if(promo.discount_type==0){
                    if(promo.discount_percent==undefined||promo.discount_percent==''){
                        simpleAlert('error', '', 'Add discount percentage.');
                        return;
                    }
                } 
                else if(promo.discount_type==1) {
                    if(promo.discount_price==undefined||promo.discount_price==''){
                        simpleAlert('error', '', 'Add discount price.');
                        return;
                    }
                }
            }
            if(promo.from_date==undefined||promo.from_date==''){
                simpleAlert('error', '', 'Select "From" date and time.');
                return;
            }
            if(promo.to_date==undefined||promo.to_date==''){
                simpleAlert('error', '', 'Select "To" date and time.');
                return;
            }
            if(promo.redemption_count==undefined||promo.redemption_count==''){
                simpleAlert('error', '', 'Enter number of redemption count.');
                return;
            }
            if(promo.promo_type==undefined){
                simpleAlert('error', '', 'Select Promo type.');
                return;
            }
        } else {
            showLoader('.saveCategory');
            adminignitefactory.Savepromocode(promo).then(function (response) {
                showLoader('.saveCategory','Update');
                if (response.data.code == 2) window.location.replace("manage/login");
                if (response.data.status == 1) {
                    window.location.replace("manage/ignite/promo-codes")
                } else {
                    simpleAlert('error','',response.data.message);
                }
            });   
        }*/
        if(promo.promo_code==undefined||promo.promo_code==''||promo.remarks==undefined||promo.remarks==''||promo.category==undefined||promo.category.length==0||promo.discount_type==undefined||promo.from_date==undefined||promo.from_date==''||promo.to_date==undefined||promo.to_date==''||promo.promo_type==undefined){
            if(promo.promo_code==undefined||promo.promo_code==''){
                simpleAlert('error', '', 'Generate promo code.');
                return;
            }
            if(promo.remarks==undefined||promo.remarks==''){
                simpleAlert('error', '', 'Add remarks to promo code');
                return;
            }
            if(promo.category==undefined||promo.category.length==0){
                simpleAlert('error', '', 'Select category for promo code');
                return;
            }
            if(promo.discount_type==undefined){
                simpleAlert('error', '', 'Select discount type');
                return;
            } else {
                if(promo.discount_type==0){
                    if(promo.discount_percent==undefined||promo.discount_percent==''){
                        simpleAlert('error', '', 'Add discount percentage.');
                        return;
                    }
                } 
                else if(promo.discount_type==1) {
                    if(promo.discount_price==undefined||promo.discount_price==''){
                        simpleAlert('error', '', 'Add discount price.');
                        return;
                    }
                }
            }
            if(promo.from_date==undefined||promo.from_date==''){
                simpleAlert('error', '', 'Select "From" date and time.');
                return;
            }
            if(promo.to_date==undefined||promo.to_date==''){
                simpleAlert('error', '', 'Select "To" date and time.');
                return;
            }
            if(promo.promo_type==undefined){
                simpleAlert('error', '', 'Select Promo type.');
                return;
            }
        } else {
            showLoader('.saveCategory');
            if(promo.discount_type==0){
                if(promo.discount_percent==undefined||promo.discount_percent==''){
                    simpleAlert('error', '', 'Add discount percentage.');
                    hideLoader('.saveCategory','Update');
                    return;
                }
                if(promo.discount_percent>100){
                    simpleAlert('error', '', 'Discount percentage should be below 100%.');
                    hideLoader('.saveCategory','Update');
                    return;   
                }
            } 
            if(promo.discount_type==1) {
                if(promo.discount_price==undefined||promo.discount_price==''){
                    simpleAlert('error', '', 'Add discount price.');
                    hideLoader('.saveCategory','Update');
                    return;
                }
            }
            var date1 = new Date(promo.from_date);
            var date2 = new Date(promo.to_date);
            if(date1.getTime()>date2.getTime()){
                simpleAlert('error', '', 'From date cannot be greater than To date.');
                hideLoader('.saveCategory','Update');
                return;
            }
            if(date1.getTime()==date2.getTime()){
                simpleAlert('error', '', 'From date and To date cannot be equal.');
                hideLoader('.saveCategory','Update');
                return;
            }
            if(promo.redemption_type==0||promo.redemption_type==undefined){
                if(promo.redemption_count==undefined||promo.redemption_count==''){
                    simpleAlert('error', '', 'Enter number of redemption count.');
                    hideLoader('.saveCategory','Update');
                    return;
                }
            }
            adminignitefactory.Savepromocode(promo).then(function (response) {
                hideLoader('.saveCategory','Update');
                if (response.data.code == 2) window.location.replace("manage/login");
                if (response.data.status == 1) {
                    window.location.replace("manage/ignite/promo-codes")
                } else {
                    simpleAlert('error','',response.data.message);
                }
            });   
        }
    }

    $scope.Changeredemption = function(type){
        console.log(type);
        if(type==true)
            $scope.promo.redemption_count = undefined;
    }

    $scope.Suspendpromo = function(id){
        $scope.id = id;
        confirmDialogueDifferentButton('','','',suspendredirect);
    }

    function suspendredirect() {
        showLoader('.suspendPromo');
        adminignitefactory.Suspendpromo({'id':$scope.id}).then(function (response) {
            hideLoader('.suspendPromo','Suspend');
            if (response.data.status == 1) {
                location.replace('manage/ignite/promo-codes');
            }
        });
    }

}]);