/*
 Author  : Ketan Solanki (ketan@creolestudios.com)
 Date    : 24th July 2018
 Name    : QuizInfoController
 Purpose : Quiz Information page
 */
angular.module('sixcloudAdminApp').controller('QuizInfoController', ['$scope', 'adminignitefactory', '$routeParams', '$timeout', function ($scope, adminignitefactory, $routeParams, $timeout) {
	$(".navbar-nav li").removeClass("active");
	$('#adminHeaderIgnite').addClass("active");
	$timeout(function () {
		$(".ignite-header li").removeClass("active");
		$('#quiz_manger').addClass("active");
	}, 500);

	$(function() {
        $( ".sortable" ).sortable();
        $( ".sortable" ).disableSelection();
        $(".sortable").on( "sortupdate", function( event, ui ) {            
            var idArray = $(this).sortable('toArray', { attribute: 'data-id'});                                                
            $scope.UpdateContentOrder(idArray);        
        } );
    });

	$scope.params = $routeParams;
	$scope.getDetails = function(){
		adminignitefactory.GetQuizDetails({ 'id': $scope.params.slug }).then(function (response) {
			if (response.data.status == 1) {
				$scope.quizData = response.data.data;									
			}
		});
	}
	$scope.getDetails();

	$scope.expand = function (index) {
        $('#content_' + index).toggleClass('active');
        $('#content_' + index).next('.new-class-content').slideToggle();
    }
    
	function redirectToQuizManager(){
		window.location.replace("manage/ignite/quiz-manager/"+$scope.quizData.quiz_published_status+"/"+$scope.quizData.category_id);
	}
	function unpublishQuiz(){
		adminignitefactory.UnPublishQuiz({ 'id': $scope.params.slug }).then(function (response) {			 
			if (response.data.status == 1) {
				confirmDialogueAlert("Success",response.data.message,"success",false,'#8972f0','Ok','',true,'','',redirectToQuizManager);
			}
		});	 
	}

	function publishQuiz(){
		adminignitefactory.PublishQuiz({ 'id': $scope.params.slug }).then(function (response) {			 
			if (response.data.status == 1) {
				confirmDialogueAlert("Success",response.data.message,"success",false,'#8972f0','Ok','',true,'','',redirectToQuizManager);
			}
		});	 
	}

	$scope.UnPublishQuiz = function(){
		confirmDialoguePublish("Are you sure you want to Un Publish this quiz ?", "Yes, Un Publish it", '', unpublishQuiz);		
	}
	$scope.PublishQuiz = function(){
		confirmDialoguePublish("Are you sure you want to Publish this quiz ?", "Yes, Publish it", '', publishQuiz);		
	}
	function deleteQuiz(){
        adminignitefactory.Deletequiz({ 'id': $scope.quizId}).then(function(response) {                                	         	        	
            if (response.data.status == 1) {                                
                confirmDialogueAlert("Success",response.data.message,"success",false,'#8972f0','Ok','',true,'','',redirectToQuizManager);
            } 
        });
    }
	$scope.deleteQuiz = function(id){		
		$scope.quizId = id;
		confirmDialoguePublish("Are you sure you want to Delete this quiz ?", "Yes, Delete it", '', deleteQuiz);		
	}

	$scope.UpdateContentOrder = function(idArray) {
        adminignitefactory.UpdateQuizQuestionOrder(idArray).then(function(response) {             
            if(response.data.status==1){
            	location.reload();
                /*simpleAlert('success', 'Order Updated', response.data.message, true);
                $scope.getDetails();*/
            }else{
                simpleAlert('error', 'Some Error Occured', response.data.message, true);
            }
        });
    }

    function deletequestion(){
		adminignitefactory.Deletequestion({ 'id': $scope.toDelete }).then(function (response) {			 
			if (response.data.status == 1) {
				simpleAlert('success', response.data.message, response.data.message, true);
				$scope.getDetails();
			}
		});	 
	}

    $scope.Deletequestion = function(id) {
    	$scope.toDelete = id;
    	confirmDialoguePublish("Are you sure you want to delete this question ?", "Yes, Delete it", '', deletequestion);	
    }
}]);