/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 24th Dec 2018
 Name    : IgniteDistributorCommissionsController
 Purpose : All the functions for distributors commissions
 */
angular.module('sixcloudAdminApp').controller('IgniteDistributorCommissionsController', ['$sce', 'adminignitefactory', '$location', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', function($sce, adminignitefactory, $location, $routeParams, $http, $scope, $timeout, $rootScope, $filter) {
    $scope.slug = $routeParams.slug;
    $timeout(function(){
        $(".ignite-header li").removeClass("active");
        $('#ignite_distributors').addClass("active");
            adminignitefactory.Getteamforcommissions({'slug':$scope.slug}).then(function(response) {
                if(response.data.status==1){
                    $scope.teamData = response.data.data;
                } else {

                }
            });
    },1500);
    $scope.url = $location.url();
    $scope.type = 0;
    $scope.year = (new Date()).getFullYear();
    
    $scope.Getcommissions = function(year,type,team_member){
        adminignitefactory.Getcommissions({'year':year,'type':type,'team_member':team_member,'slug':$scope.slug}).then(function(response) {
            if(response.data.status==1){
                $scope.commissions = response.data.data;
            } else {

            }
        });
    }

    adminignitefactory.Gettypeandyear().then(function(response) {
        if(response.data.status==1){
            $scope.years = response.data.data.years;
            $scope.types = response.data.data.type;
            $scope.Getcommissions($scope.year,$scope.type,$scope.member_name);
        }
    });

}]);