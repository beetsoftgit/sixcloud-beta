/*
 Author  : Zalak Kapadia (zalak@creolestudios.com)
 Date    : 4th july 2018
 Name    : MPTicketDetailController
 Purpose : To get tickets detail at admin side
 */
app.controller('MPTicketDetailController', ['$scope', '$routeParams','adminmpfactory','$timeout', function($scope,$routeParams,adminmpfactory, $timeout) {
    $(".navbar-nav li").removeClass("active");
    $scope.params = $routeParams;
    $scope.Getticketdetails=function(){
    	adminmpfactory.Getticketdetails({'id':$scope.params.slug}).then(function(response) {
	        if(response.data.status==1){   
	            $scope.ticketDetail=response.data.data;
                if($scope.ticketDetail.ticket_for==1){
                    $('#adminHeaderIgnite').addClass("active");
                    $(".ignite-header li").removeClass("active");
                    $('#customer_li_ignite').addClass("active");
                } else if($scope.ticketDetail.ticket_for==2){
                    $('#adminHeaderMP').addClass("active");
                    $(".admin-header li").removeClass("active");
                    $('#customer_li_sixteen').addClass("active");
                } else{
                    $('#adminHeaderPr').addClass("active");
                    $(".pr-header li").removeClass("active");
                    $('#customer_li_pr').addClass("active");
                }
	        }
	    });
    }
	$scope.Getticketdetails();    

    $scope.sendMessageUser=function(ticketdetail,message){
    	ticketdetail['admin_message']=message;
    	$timeout(function(){
            showLoader('.send-btn','Please wait...');
        },500); 
    	adminmpfactory.Sendmessageuser(ticketdetail).then(function(response) {
            if(response.data.status==1){   
            	$timeout(function(){
    	            hideLoader('.send-btn','Send');
    	        },500);
                simpleAlert('success', 'Success', response.data.message);
                $scope.Getticketdetails();  
            }
        });
    }

    $scope.markAsCompleted=function(ticket){
        adminmpfactory.Markascompleted({ticket}).then(function(response){
            if(response.data.status==1){
                $scope.ticketDetail=response.data.data;    
            }
        });
    }

    $scope.Assignticket = function(id,name) {
        adminmpfactory.Assignticket({'ticket_id':id,'assignee_name':name}).then(function(response) {
            if(response.data.status==1){   
                $scope.ticketDetail=response.data.data;
            }
        });                
    }

    $scope.Closeticket = function(id,name) {
        adminmpfactory.Closeticket({'ticket_id':id,'ticket_closed_by':name,'time_zone':$scope.ticketDetail.time_zone}).then(function(response) {
            if(response.data.status==1){   
                if($scope.ticketDetail.ticket_for==1){
                    location.replace('manage/ignite/tickets/1');
                } else if($scope.ticketDetail.ticket_for==2){
                    location.replace('manage/sixteen/tickets/2');
                } else{
                    location.replace('manage/sixteen/tickets/3');
                }
            }
        });                        
    }

}]);