/*
 Author  : Komal Kapadi (komal@creolestudios.com)
 Date    : 15th December 2017
 Name    : HomeController
 Purpose : All the functions for home page
 */
angular.module('sixcloudAdminApp').controller('HomeController', ['eltfactory', 'Geteltsections', '$http', '$scope', '$timeout', '$rootScope', '$filter', function(eltfactory, Geteltsections, $http, $scope, $timeout, $rootScope, $filter) {
    $scope.sections = Geteltsections.data.data;
    console.log('home controller loaded');
    $scope.upload_video_text = 'Upload New Video';
    $scope.uploadDisable = false;
    /*
     * Add new sections
     */
    $scope.form = {};
    $scope.addNewSection = function(category_name, description) {
        $scope.submitted = true;
        eltfactory.Addnewsection({
            'category_name': category_name,
            'description': description
        }).then(function(response) {
            if (response.data.status == 1) {
                $scope.videoData = response.data.data;
                $scope.form.$valid = false;
                simpleAlert('success', 'New Section', response.data.message, true);
                eltfactory.Geteltsections().then(function(res) {
                    $scope.sections = res.data.data;
                });
            }
        });
    }
    /*
     * Add new video file
     */
    $scope.addNewVideo = function() {
        var formData = new FormData($("#form_add_video")[0]);
        //            var desc = CKEDITOR.instances.description.getData();
        formData.append('video_ordering', $('#video_ordering').val());
        formData.append('video_status', ($('#video_type').prop("checked") ? 1 : 2));
        formData.append('video_category_id', $('#sectionid').val());
        $scope.upload_video_text = 'Uploading.....';
        $scope.uploadDisable = true;
        $('.close').trigger('click');
        $.ajax({
            url: BASEURL + "Addnewvideo",
            type: "post",
            processData: false,
            contentType: false,
            data: formData,
            beforeSend: function() {},
            success: function(response) {
                $scope.uploadDisable = false;
                if (response.status == 1) {
                    $scope.upload_video_text = 'Upload New Video';
                    $('#form_add_video')[0].reset();
                    simpleAlert('success', 'New Video', response.data.message, true);
                }
            },
            error: function(data) {
                console.log('Error');
                $scope.upload_video_text = 'Upload New Video';
                $scope.uploadDisable = false;
            }
        });
    }
    /*
     * Get Videos based on section category selection
     */
    $scope.getVideos = function(id) {
        eltfactory.Getallvideoposition({
            'video_category_id': id
        }).then(function(response) {
            if (response.data.status == 1) {
                $scope.videoPositionData = response.data.data.position;
                $scope.last_position = response.data.data.last_position;
                console.log($scope.videoPositionData);
            }
        });
    }
}]);