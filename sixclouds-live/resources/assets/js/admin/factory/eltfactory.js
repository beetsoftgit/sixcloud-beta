/* 
 * Author: Komal Kapadi
 * Date  : 11th Dec 2017
 * ELT factory file
 */
angular.module('sixcloudAdminApp').factory('eltfactory', ['$http', '$rootScope',
    function ($http, $rootScope) {
        return {
            // check if user logged in or not
            Geteltsections: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getadmineltsections'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Addnewsection: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Addnewsection'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getallvideoposition: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getallvideoposition'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            }
        };
    }
]);