/* 
 * Author: Zalak Kapadia
 * Date  : 26th April 2018
 * MarketPlace factory file
 */
angular.module('sixcloudAdminApp').factory('adminmpfactory', ['$http', '$rootScope',
    function ($http, $rootScope) {
        return {
            // check if user logged in or not
            Getallcategories: function () {
                return $http({
                    method: 'POST',
                    url: 'Getallcategories',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getalljobsdata: function (category, sort, search, price, timeleft, pageNumber, status) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                return $http({
                    method: 'POST',
                    data: {
                        'category': category,
                        'sort': sort,
                        'search': search,
                        'price': price,
                        'timeleft': timeleft,
                        'status': status
                    },
                    url: 'Getalljobsdata?page=' + pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getalldesignerdata: function (data, pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getalldesignerdata?page=' + pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getallbuyerdata: function (data, pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getallbuyerdata?page=' + pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getadmincategories: function (data, pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getadmincategories?page=' + pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getprojectdetails: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getprojectdetails'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getprojectpaymentlogs: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getprojectpaymentlogs'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Sendmailforratereview: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Sendmailforratereview',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getmessagedata: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getmessagedata',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Sendmessage: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Sendmessageadmin',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Unlinkmsgbrdremovedfile: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Unlinkmsgbrdremovedfileadmin',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Infavorofbuyer: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Infavorofbuyer',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Infavorofseller: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Infavorofseller',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Cancelprojectbyadmin: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Cancelprojectbyadmin',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Addeditcategorydata: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Addeditcategorydata',
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getactiveprojectsbyskill: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getactiveprojectsbyskill',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Enabledeleteskill: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Enabledeleteskill',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Enableskill: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'EnableDeleteskill',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getinspirationcategorydata: function (data, pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getinspirationcategorydata?page=' + pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Addeditinscategorydata: function (data) {

                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Addeditinscategorydata',
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getinspirationuploadsbycategory: function (data) {

                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getinspirationuploadsbycategory',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Enabledeleteinscategory: function (data) {

                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Enabledeleteinscategory',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            //Code Added By Ketan Solanki for ID card download Date : 4th July 2018
            Download: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'DownloadId'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Unlinkdownloadedfile: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'UnlinkdownloadedfileIdProof'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Gettickettypes: function(data) {
               
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Gettickettypes',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getticketcategories: function(data) {
               
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getticketcategories',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getticketlisting: function(data,pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                return $http({
                    method: 'POST',
                    data:data,
                    url: 'Getticketlisting?page=' + pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getticketdetails: function(data) {
               
                return $http({
                    method: 'POST',
                    data:data,
                    url: 'Getticketdetails' 
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Sendmessageuser: function(data) {
               
                return $http({
                    method: 'POST',
                    data:data,
                    url: 'Sendmessageuser' 
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            //Code Added By ketan Solanki for User Added Country.Date : 5th July 2018
            GetUserCountriesListing: function(data,pageNumber) { 
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }              
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'GetUserCountriesListing?page='+ pageNumber,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            uploadCSVFileData: function(data){
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'uploadCSVFileData',
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getsellerdetail: function(data){
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getsellerdetail',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getservices: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getadminservices'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Designerworkhistoryandreviews:function(data,pageNumber){
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Designeradminworkhistoryandreviews?page='+pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getportfoliodata: function(data,pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                return $http({
                    method: 'POST',
                    data:data,
                    url: 'Getadminportfolio?page=' + pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Markascompleted: function(data) {
                return $http({
                    method: 'POST',
                    data:data,
                    url: 'Markascompleted',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getbuyerdetail: function(data){
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getbuyerdetail',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            GetBuyerProjects:function(data,pageNumber){
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'GetBuyerProjects?page='+pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Gettransactionlist:function(data,pageNumber){
                return $http({
                    method: 'POST',
                    data:data,
                    url: 'Gettransactionlist?page='+pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Withdrawamountpaidbyadmin:function(data){
                return $http({
                    method: 'POST',
                    data:data,
                    url: 'Withdrawamountpaidbyadmin',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Assignticket:function(data){
                return $http({
                    method: 'POST',
                    data:data,
                    url: 'Assignticket',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Closeticket:function(data){
                return $http({
                    method: 'POST',
                    data:data,
                    url: 'Closeticket',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
        };
    }
]);