/* 
 * Author: Komal Kapadi
 * Date  : 4th Dec 2017
 * user factory file
 */
angular.module('sixcloudscustomersupportApp').factory('customersupport', ['$http', '$rootScope',
    function ($http, $rootScope) {
        return {
            Gettickettype: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Gettickettype'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            GetProducttype: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'GetProducttype'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getticketcategoriesforcustomersupport: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getticketcategoriesforcustomersupport'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Createnewticket: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Createnewticket',
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
        };
    }
]);