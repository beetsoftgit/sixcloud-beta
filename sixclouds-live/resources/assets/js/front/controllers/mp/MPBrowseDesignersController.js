/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 4th April 2018
 Name    : MPBrowseDesignersController
 Purpose : All the functions for MP browse designers page
 */
angular.module('sixcloudApp').controller('MPBrowseDesignersController', ['$sce', 'mpfactory', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', function($sce, mpfactory, $routeParams, $http, $scope, $timeout, $rootScope, $filter) {
    console.log('MPLogin controller loaded');
    $('body').css('background','#e3eefd');
    $timeout(function(){
        $('.buyer-dashboard-menu').find('li').removeClass('active');
        $('#browse_designers').addClass('active');
    },500);
    $scope.sliderfunction = function(){
        $timeout(function(){
            var swiper = new Swiper('.browse-designers-swiper', {
                slidesPerView: 1,
                spaceBetween: 0,
                loop: true,
                 navigation: {
                   nextEl: '.browse-designers-button-next',
                   prevEl: '.browse-designers-button-prev',
                 },
            });
        }, 1000);
    }
    getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
    $scope.language=getLanguage;
    
    $('#chk_lang').change(function() {
        getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
        $scope.language=getLanguage;
        $scope.Getskills();
    });

    $("#fd1").asRange({
        /*range: true,
        limit: false,*/
        step: 0.1,
        range: false,
        min: 0,
        max: 5
    });

    $scope.current_domain = current_domain;
    // console.log($("#fd1").val());
    $timeout(function(){
        $scope.sliderfunction();
    },1000);
    $scope.mp_project_id = $routeParams.slug;
    $scope.assigned_designer_id = $routeParams.assigned_designer_id;
    /*$('<div class="quantity-nav"><div class="quantity-button quantity-up"><span class="caret"></span></div><div class="quantity-button quantity-down"><span class="caret"></span></div></div>').insertAfter('.quantity input');*/

    $scope.getDesignersData = function(skillId){
        if($scope.assigned_designer_id==undefined){
            mpfactory.Getdesigners({'skillId':skillId}).then(function(response) {

                console.log(response.data.data);
                if (response.data.status == 1)
                {
                    $scope.designers = response.data.data;
                    $scope.reset_designers = response.data.data;
                }
                else {
                }
            });
        } else {
            mpfactory.Getdesigners({'assigned_designer_id':$scope.assigned_designer_id,'skillId':skillId}).then(function(response) {
                if (response.data.status == 1)
                {
                    $scope.designers = response.data.data;
                    $scope.reset_designers = response.data.data;
                }
                else {
                }
            });
        }
    }
    $scope.Categoryfilter = function(skill){
            $('.sidebar-link').find('li').removeClass('active');
            $('#'+skill.id).addClass('active');
            $scope.filter = undefined;
            $scope.getDesignersData(skill.id);
            $timeout(function(){
                $scope.sliderfunction();
            },1000);
    }
    $scope.Getskills=function(argument) {
        mpfactory.Getskills().then(function(response) {
            if (response.data.status == 1)
            {
                $scope.skills = response.data.data;
                $timeout(function(){
                    $('.sidebar-link').find('li').removeClass('active');
                    $('#'+$scope.skills[0].id).addClass('active');
                    $scope.getDesignersData($scope.skills[0].id);
                },1000);
            }
            else {
            }
        });
    }
    $scope.Getskills();

    mpfactory.Getlanguages().then(function(response) {
        if (response.data.status == 1)
        {
          $scope.languages = response.data.data;
        }
        else {
        }
    });
    $scope.Searchbyname = function () {
            $scope.total_records = $('.designer-class').length;
            console.log($scope.total_records);
        }
    $(document).ready(function(){
        $("#project_min_budget").on("keypress keyup blur",function (event) {
          //this.value = this.value.replace(/[^0-9\.]/g,'');
          $(this).val($(this).val().replace(/[^0-9\.]/g,''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
              event.preventDefault();
            }
        });
        $("#project_max_budget").on("keypress keyup blur",function (event) {
          //this.value = this.value.replace(/[^0-9\.]/g,'');
          $(this).val($(this).val().replace(/[^0-9\.]/g,''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
              event.preventDefault();
            }
        });
    });

    $scope.limit = 8;
    $scope.scrollEventCallback = function () {
        if (($(window).scrollTop() + 200) >= $(document).height() - $(window).height() - 200) {
            $scope.limit = $scope.limit + 8;
        }
    }

    /*if(filter.rating == true || filter.finished_projects == true || filter.full_time == true || filter.part_time == true || filter.hobbyist == true || filter.casual == true || filter.min_budget > 0 || filter.max_budget > 0 || (filter.language != '' && filter.language != undefined)){*/

    $scope.Dofilter = function(){
       var filter = {};
        if($("#fd1").val() != ''){
            if($("#fd1").val() != '0'){
                filter.points = $("#fd1").val();
            } else{
                filter.points = undefined;    
            }
        } else {
            filter.points = undefined;
        }
        if($scope.min_budget > 0 && $scope.min_budget != undefined){
            filter.min_budget = $scope.min_budget;
        } else {
            filter.min_budget = undefined;
        }
        if($scope.max_budget > 0 && $scope.max_budget != undefined){
            filter.max_budget = $scope.max_budget;
        } else {
            filter.max_budget = undefined;
        }
        if($scope.language != '' && $scope.language != undefined){
            filter.language = $scope.language;
        } else {
            filter.language = undefined;
        }
        if($scope.sort != undefined){
            filter.sort = $scope.sort;
        } else {
            filter.sort = undefined;
        }
        if(filter.points != undefined || filter.min_budget != undefined || filter.max_budget != undefined || filter.language != undefined || filter.sort != undefined ) {
            showLoader('.update-button');
            Disable('.reset-button');
            mpfactory.Dofilter({filter:filter,'designers':$scope.reset_designers,'language':getLanguage}).then(function(response) {
                if(getLanguage=='en'){
                    text='Update';
                }else{
                    text='更新';
                }
                hideLoader('.update-button',text);
                Enable('.reset-button');
                if (response.data.status == 1)
                {
                    $scope.designers = response.data.data;
                }
                else {
                }
            });
            $timeout(function(){
                $scope.sliderfunction();
            },1000);
        } else {
            $scope.Doreset();
            $timeout(function(){
                $scope.sliderfunction();
            },1000);
            if(getLanguage=='en'){
                simpleAlert('info','','Select some filters');    
            }else{
                simpleAlert('info','','选择一些过滤器');    
            }
            
        }
    }

    $scope.Doreset = function(){
        $scope.name_filter = undefined;
        $("#fd1").asRange('set', 0);
        $scope.min_budget = undefined;
        $scope.max_budget = undefined;
        $scope.language = undefined;
        $scope.sort = undefined;
        $scope.designers = $scope.reset_designers;
        $('.sidebar-link').find('li').removeClass('active');
        $('#all_designer').addClass('active');
        $timeout(function(){
            $scope.sliderfunction();
        },1000);
    }

    $scope.Asssigndesigner = function(designer,mp_project_id){
        showLoader(".select_"+designer.id);
          mpfactory.Asssigndesigner({'mp_project_id':mp_project_id, 'designer':designer,'fromBrowseSellerPage':1}).then(function(response) {
            if(getLanguage=='en'){
                
                var text              = "Success!!";
                var confirmButtonText = 'Ok';
                var selectText        = 'Select';
                  
            }else{
                
                var text              = "成功!!";
                var confirmButtonText = '好。';
                var selectText        = '选择';
            }
            hideLoader(".select_"+designer.id,selectText);
            if (response.data.status == 1)
            {
                simpleAlert('success',text,response.data.message,true);
                swal({
                    title: text,
                    text: response.data.message,
                    type: "success",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    showLoaderOnConfirm: true,
                    confirmButtonText: confirmButtonText,
                    confirmButtonColor: '#8a75d0',
                }, function () {
                    window.location.replace("buyer-dashboard");
                });
            }
            else {
                simpleAlert('info','',response.data.message);
            }
        });     
    }

}]);