/*
 Author  : Komal Kapadi (komal@creolestudios.com)
 Date    : 16th April 2018
 Name    : MPDesignerPastProjectsController
 Purpose : All the functions for MP designer past projects
 */
angular.module('sixcloudApp').controller('MPDesignerPastProjectsController', ['$sce', 'mpfactory', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', function($sce, mpfactory, $routeParams, $http, $scope, $timeout, $rootScope, $filter) {
    console.log('MPDesignerPastProjectsController controller loaded');
    $('body').css('background','#e3eefd');
    $timeout(function(){
        if($rootScope.loginUserData.status == 0){
            window.location.replace("sixteen-login");
        }
        $('.designer-dashboard-menu').find('li').removeClass('active');
        $('#designer_past_project').addClass('active');
    },500);

    $scope.hidden = true;

    $scope.pastprojects = function(pageNumber) {
        mpfactory.Designerpastprojects(pageNumber).then(function(response) {
            $scope.projects = response.data.data;
            $scope.paginate = response.data.data;
            if(response.data.data.total==0){
                $("#no_data_msg").text("There are no past projects available");
            }
            $scope.currentPaging = response.data.data.current_page;
            $scope.rangepage = _.range(1, response.data.data.last_page + 1);
            
        });
    };
    $scope.pastprojects(1);

    $scope.Designercancelledprojects = function(pageNumber) {
        mpfactory.Designercancelledprojects(pageNumber).then(function(response) {
            if(response.data.status==1){
                $scope.cancelledprojects = response.data.data;
                $scope.paginateDesignerPastCancel = response.data.data;
                $scope.currentDesignerPastCancelPaging = response.data.data.current_page;
                $scope.rangepaginateDesignerPastCancelPage = _.range(1, response.data.data.last_page + 1);
            }  
        });
            
    };
    $scope.Designercancelledprojects(1);

    $scope.Designerprojectinvitationrejected = function(pageNumber) {
        mpfactory.Designerprojectinvitationrejected(pageNumber).then(function(response) {
            $scope.projectinvitationrejectedprojects = response.data.data;
            $scope.paginateprojectinvitationrejectedprojects = response.data.data;
            $scope.currentprojectinvitationrejectedprojectsPaging = response.data.data.current_page;
            $scope.rangepaginateprojectinvitationrejectedprojects = _.range(1, response.data.data.last_page + 1);
        });
    };
    $scope.Designerprojectinvitationrejected(1);

    $scope.returnRatingArray = function(count){
        var ratings = []; 

        for (var i = 0; i < count; i++) { 
            ratings.push(i) 
        } 

        return ratings;
    }

    $scope.strLimit = 100;
    // Event trigger on click of the Show more button.
    $scope.showMore = function(getReview) {
        $scope.strLimit = getReview.length;
    };

    // Event trigger on click on the Show less button.
    $scope.showLess = function() {
        $scope.strLimit = 100;
    };
}]);