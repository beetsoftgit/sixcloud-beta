/*
 Author  : Zalak Kapadia (zalak@creolestudios.com)
 Date    : 13th August 2018
 Name    : MPNotificationsController
 Purpose : Display System Notifications
 */
angular.module('sixcloudApp').controller('MPNotificationsController', function($scope, mpfactory) {
     $scope.endto = $scope.startfrom + 1;
     $scope.currentPage = $scope.currentPaginate = 1;
     $scope.range = $scope.rangepage = $scope.rangePaging = [];
     $scope.customNotificationList = function(pageNumber) {
         mpfactory.Getnotifications(pageNumber).then(function(response) { // get category list
             if (response.data.status === 0) {
                 return false;
             }
             $scope.notifications = response.data.data.data;
             $("#notification_number").text(response.data.data.total);
             $scope.pagination = response.data.data;
             $scope.currentActivePaging = response.data.data.current_page;
             $scope.rangeActivepage = _.range(1, response.data.data.last_page + 1);
         });
     };
     $scope.customNotificationList(1);

     $scope.Readnotifications=function(){
        mpfactory.Readnotifications().then(function(response) { 
            if (response.data.status === 0) {
                return false;
            }
            $("#notification_number").text(response.data.data.totalCount);
        });    
    }
    $scope.Readnotifications(); 

 });