/*
 Author  : Komal Kapadi (komal@creolestudios.com)
 Date    : 2nd May 2018
 Name    : designerProfileController
 Purpose : View designer profile page
 */
angular.module('sixcloudApp').controller('designerProfileController', ['$sce', 'mpfactory', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', function($sce, mpfactory, $routeParams, $http, $scope, $timeout, $rootScope, $filter) {
    console.log('designerProfileController controller loaded');
    $('body').css('background', '#e3eefd');
    $("#temp_credit").hide();
    
    $scope.purchaseButton = 'lbl_service_purchase';
    /* static changes senil : start */
    $('.accordion-div2 h5').click(function() {
	  $(this).toggleClass('active');
            $(this).next('.inner-div-content').slideToggle();
          });
    $scope.current_domain = current_domain;

    $scope.expand = function(service_id){
        $('#service_'+service_id).toggleClass('active');
        $('#service_'+service_id).next('.inner-div-content').slideToggle();
    }

    $scope.mp_project_id = $routeParams.mp_project_id;
    $scope.user_id = $routeParams.id;

    $scope.cartvalue = [];
    $scope.cart_total = 0;
    $scope.proceed=0;
    $scope.Changecart = function(whichService){
        // $('#'+whichService.id).addClass('button_color_change');
        $('#'+whichService.id).attr("disabled", "disabled");
        $scope.purchaseButton = 'lbl_service_purchased';
    	$scope.cartvalue.push(whichService);
        console.log($scope.cartvalue);
        $scope.cart_total = 0; 
        $("#temp_credit").show();
        $scope.temp_credit=$scope.credit;
        
    	angular.forEach($scope.cartvalue, function(value, key) {
    		$scope.cart_total =  parseInt($scope.cart_total)+ parseInt(value.price);
		});
        $scope.proceed=1;
        if(parseInt($scope.temp_credit)>parseInt($scope.cart_total)){
            $scope.display_credit=parseInt($scope.temp_credit)-parseInt($scope.cartvalue[0].price);
            $scope.temp_credit=parseInt($scope.cartvalue[0].price);
            $scope.cart_total=0;
        }
        else{
            $scope.display_credit=0;
            $scope.temp_credit=$scope.credit;
            $scope.cart_total=parseInt($scope.cart_total)-parseInt($scope.temp_credit);
        }
        $("#credit").text($scope.display_credit);
    }

    var getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
    changeLanguage(getLanguage);
    $('#chk_lang').change(function() {
      var getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
        changeLanguage(getLanguage);
    });

    $scope.Staticproceed = function(){
        showLoader('.proceed_button');
        if(getLanguage=='en'){
            var loadertext        = 'Proceed';     
        }else{
            var loadertext        = '继续';
        }
        if($scope.mp_project_id!=undefined){
            mpfactory.Storecart({'cart':$scope.cartvalue,'mp_project_id':$scope.mp_project_id}).then(function(response) {
                if (response.data.code == 2) window.location.replace("sixteen-login");
                if (response.data.status == 1)
                {
                    if($scope.mp_project_id!=undefined){
                        window.location.replace("create-new-project/"+$scope.mp_project_id+"/project-created/"+response.data.data+"/"+$scope.user_id+"");
                    } else {
                        window.location.replace("create-new-project/"+$scope.user_id+"/project-pending/"+$scope.cart_total+"");
                    }
                }
                else {
                    hideLoader(".proceed_button",loadertext);
                    simpleAlert('error', '', response.data.message);
                }
            });
        } else {
            mpfactory.Storecart({'cart':$scope.cartvalue}).then(function(response) {
                if (response.data.code == 2) window.location.replace("sixteen-login");
                if (response.data.status == 1)
                {
                    if($scope.mp_project_id!=undefined){
                        window.location.replace("create-new-project/"+$scope.mp_project_id+"/project-created/"+response.data.data+"/"+$scope.user_id+"");
                    } else {
                        window.location.replace("create-new-project/"+$scope.user_id+"/project-pending/"+response.data.data+"");
                    }
                }
                else {
                    // $scope.servicesTotal=response.data.data.length;
                }
            });
        }
    }

    /* static changes senil : end */

    
    
    
    function changeLanguage(lang){ 
        if(lang=="chi"){
            $timeout(function(){
                $scope.isEnglish = false;
            },500);
            $("#chi_profile_data").show();
            $("#en_profile_data").hide();
        }    
        else{
            $timeout(function(){
                $scope.isEnglish = true;
            },500);
            $("#en_profile_data").show();
            $("#chi_profile_data").hide();
      }
    }
    mpfactory.Designerprofileinfo({'id':$scope.user_id}).then(function(response) {
        console.log(response);
		$scope.designerdata = response.data.data[0];
		$scope.skillDetail=response.data.data[0].skills;
        // $scope.languageDetail= JSON.parse(response.data.data[0].languages_known);
        if(response.data.data[0].languages_known_chinese.length > 0){
            $scope.languageDetailChinese = JSON.parse(response.data.data[0].languages_known_chinese);
            $scope.languageDetailChineseLength = response.data.data[0].languages_known_chinese.length;
        }else{
            $scope.languageDetailChinese = {};
            $scope.languageDetailChineseLength = 0;
        }
        if(response.data.data[0].languages_known.length > 0){
            $scope.languageDetail = JSON.parse(response.data.data[0].languages_known);
            $scope.languageDetailLength = response.data.data[0].languages_known.length;
        }else{
            $scope.languageDetail = {};
            $scope.languageDetailLength = 0;
        }
    	
    	//$scope.about_us = $sce.trustAsHtml(response.data.data[0].about_us);

    	/*showing overall rating*/
    	var ratingValue = $scope.designerdata.average_ratings, rounded = (ratingValue | 0), str;

		for (var j = 0; j < 5; j++) {
		  str = '<span class="star-yellow"><i class="fa ';
		  if (j < rounded) {
		    str += "fa-star";
		  } else if ((ratingValue - j) > 0 && (ratingValue - j) < 1) {
		    str += "fa-star-half-o";
		  } else {
		    str += "fa-star-o";
		  }
		  str += '" aria-hidden="true"></i></span>';
		  $("#overAllRating").append(str);
		}
		/*showing overall rating end*/
    	
    });
    $('.kv-gly-star').rating({
	        containerClass: 'is-star',
	        showCaption: false,
	        readonly: true,
	        showClear: false,
	        size:'xs',
	    });
    
    

    $scope.Designerworkhistoryandreviews = function(data,pageNumber) {
	    mpfactory.Designerworkhistoryandreviews({'id':$scope.user_id},pageNumber).then(function(response) {
            console.log(response.data);
	    	if (response.data.status == 1)
	        {
	   			$scope.designerWorkReviewsData = response.data.data.data;
	   			$scope.paginateWorkHistory     = response.data.data;
	   			$scope.projectTotal            = response.data.data.total;
	        	$scope.currentWorkReviewPaging = response.data.data.current_page;
		        $scope.rangeWorkReviewpage = _.range(1, response.data.data.last_page + 1);
	        }
	        else {
	        }
	    });
	}
    $scope.Designerworkhistoryandreviews({'id':$scope.user_id},1);

    $scope.Getportfoliodata = function(data,pageNumber) {
	    mpfactory.Getportfoliodata({'id':$scope.user_id},pageNumber).then(function(response) {
	    	if (response.data.status == 1)
	        {
	   			$scope.designerPortfolio = response.data.data.data;
	   			$scope.baseurl           = BASEURL;
	   			$scope.paginatePortfolio          = response.data.data;
	   			$scope.protfolioTotal    = response.data.data.total;
	        	$scope.currentProfilePaging = response.data.data.current_page;
		        $scope.rangeProfilepage = _.range(1, response.data.data.last_page + 1);
	        }
	        else {
	        	$scope.protfolioTotal=response.data.data.total;
	        }
	    });
	}
    $scope.Getportfoliodata({'id':$scope.user_id},1);

    $scope.Designerinspirationbanklisting = function(data,pageNumber) {
	    mpfactory.Designerinspirationbanklisting({'id':$scope.user_id},pageNumber).then(function(response) {	
	    	if (response.data.status == 1)
	        {
	   			$scope.gallaryInspirationData = response.data.data.data;
	   			$scope.paginateInspiration = response.data.data;
	   			$scope.gallaryInspirationTotal=response.data.data.total;
	        	$scope.currentInspirationPaging = response.data.data.current_page;
		        $scope.rangeInspirationpage = _.range(1, response.data.data.last_page + 1);
	        }
	        else {
	        }
	    });
	}
    $scope.Designerinspirationbanklisting({'id':$scope.user_id},1);
 	
 	$scope.returnRatingArray = function(count){
        var ratings = []; 

        for (var i = 0; i < count; i++) { 
            ratings.push(i) 
        } 

        return ratings;
    }

    $scope.returnRatingAvg = function(getRating, getID){
        /*showing overall rating*/
        var ratingValue = getRating, rounded = (ratingValue | 0) ;
        var  str  = '';
        $timeout(function() {  
            for (var j = 0; j < 5; j++) {
                
              str = '<span class="star-yellow"><i class="fa ';
              if (j < rounded) {
                str += "fa-star";
              } else if ((ratingValue - j) > 0 && (ratingValue - j) < 1) {
                str += "fa-star-half-o";
              } else {
                str += "fa-star-o";
              }
              str += '" aria-hidden="true"></i></span>';
              
                $("#overAllRating_"+getID).append(str);
            }
        }, 500);
        /*showing overall rating end*/
    }


    $scope.strLimit = 100;
    // Event trigger on click of the Show more button.
    $scope.showMore = function(getReview) {
        $scope.strLimit = getReview.length;
    };

    // Event trigger on click on the Show less button.
    $scope.showLess = function() {
        $scope.strLimit = 100;
    };

    $scope.Getservices = function() {
        mpfactory.Getservices({'id':$scope.user_id,'mp_project_id':$scope.mp_project_id}).then(function(response) {
            if (response.data.status == 1)
            {
                $scope.services = response.data.data;
            }
            else {
                // $scope.servicesTotal=response.data.data.length;
            }
        });
    }
    $scope.Getservices({'id':$scope.user_id},1);

}]);