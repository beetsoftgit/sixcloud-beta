/*
 Author  : Ketan Solanki (ketan@creolestudios.com)
 Date    : 30th March 2018
 Name    : MPDesignerPortfolioItemEditController
 Purpose : All the functions for MP login page
 */
angular.module('sixcloudApp').controller('MPDesignerInspirationBankEditController', ['$sce', 'mpfactory', '$routeParams', '$http', '$scope', '$compile', '$timeout', '$rootScope', '$filter', function ($sce, mpfactory, $routeParams, $http, $scope, $compile, $timeout, $rootScope, $filter) {
  $('body').css('background', '#e3eefd');
  initSample();
  var getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
  changeLanguage(getLanguage);
  $scope.currentLanguage = getLanguage;
  $('#chk_lang').change(function () {
    var getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
    changeLanguage(getLanguage);
    $scope.currentLanguage = getLanguage;
    $scope.$apply();
  });  

  function changeLanguage(lang) {  
  // $scope.currentLanguage = lang;  
    if (lang == "chi") {  
      $scope.english = false;
      $scope.chinese = true;
      getLanguage='chi';
    }
    else {
      $scope.english = true;
      $scope.chinese = false;
      getLanguage='en';
    }
  }
  $scope.isDataChanged = false;    
  $timeout(function () {
    if ($rootScope.loginUserData.status == 0) {
      window.location.replace("sixteen-login");
    }
    $('.designer-dashboard-menu').find('li').removeClass('active');

    $('#designer_myuploads').addClass('active');
    $(".cover_image_class").change(function () { 
      $scope.isDataChanged = true;
      $scope.$apply();
      var imageId = $(this).attr("data-Imageid");
      readURL(this,imageId);
      
    });
     CKEDITOR.instances.editor.on('change', function() {
      var newData = CKEDITOR.instances.editor.getData();      
      var oldData = $scope.inspirationBankInfo.upload_description;
      if(newData === oldData){        
        $scope.isDataChanged = false;  
      } else{
        $scope.isDataChanged = true;  
        $scope.$apply();        
      }
    });
     $(".design-capabilities").on("click",function(){      
      $scope.isDataChanged = true;  
      $scope.$apply();
     })
  }, 500);
  $scope.changeValue = function(){
    $scope.isDataChanged = true;  
  }
  $scope.mp_portfolio_id = $routeParams.slug;
  $scope.getSlug = $routeParams.slug;
  $scope.getSlugArray = $routeParams.slug.split(':')
  $scope.inspiration_bank_details_title = $scope.getSlugArray[0];
  $scope.category_id = $scope.getSlugArray[1];  
  $scope.addMoretext = "";  
  $scope.coverImagetext = "";  
  $scope.$watch('currentLanguage', function() {    
    if($scope.currentLanguage == 'chi'){
      $("#addMoreLabel").html("添加更多:");
      $("#coverImageLabel").html("封面图片");
      $("#coverImageLabelNew").html("封面图片");     
      $scope.coverImagetext = "封面图片";  
      $scope.addMoretext = "添加更多:";  
    }else{
      $("#addMoreLabel").html("Add More:");
      $("#coverImageLabel").html("Cover Image");
      $("#coverImageLabelNew").html("Cover Image");
      $scope.addMoretext = "Add More";
      $scope.coverImagetext = "Cover Image";   
    }    
  });
  
  $scope.InspirationBankfunction = function () {
    mpfactory.InspirationBankinfo({ 'slug': $scope.getSlug, 'inspiration_bank_details_title': $scope.inspiration_bank_details_title, 'category_id': $scope.category_id }).then(function (response) {   
      if(getLanguage=='en'){
        var confirmButtonText = 'Ok';    
      }else{
        var confirmButtonText = '好。';
      }    
      if (response.data.code == 2)
        window.location.replace("sixteen-login");
      if (response.data.status == 1) {
        $scope.inspirationBankInfo = response.data.data;        
        $scope.availableSlot = $scope.inspirationBankInfo.inspiration_bank_files_attachments.length;
        $scope.availableSlot = 4-$scope.availableSlot;                
        $scope.newSlot = 1;//To add New Slot
        $scope.availableSlotCoverImage = 1- $scope.inspirationBankInfo.cover_images.length;        
        $timeout(function () {
          CKEDITOR.instances.editor.setData($scope.inspirationBankInfo.upload_description);          
        }, 500);
        $('.all-remove').remove();
        $timeout(function(){
        for (var i = 0; i < $scope.availableSlotCoverImage; i++) {          
            input = jQuery('<div class="col-xs-6 col-sm-4 all-remove"><span id="coverImageLabelNew">'+$scope.coverImagetext+' : </span><div style="border:2px dashed #f84c25; " class=" new-border-class uploading-album-image  new-class-inspiration-image file_attachment_cover_'+i+'"><div class="fafa_'+i+'"><i class="fa fa-picture-o fafaremove_'+i+'" aria-hidden="true"></i></div><input type="file" id="cover_'+i+'" name="cover_image[]"></div></div>');           
            jQuery('.image-slot1').append(input);            
            $("#cover_"+i).change(function() {
                      
              $scope.isDataChanged = true;
              $scope.$apply();
              readURLCover(this);
            });
            function readURLCover(input) {
              if (input.files && input.files[0] && input.files.length>0) {
                $scope.coverNotPresent = false;
                var reader = new FileReader();
                reader.onload = function(e) {
                  var id_number = input.id;
                  var id_number = id_number.split('_').pop();            
                  $('.file_attachment_cover_'+id_number).css('background-image', 'url('+e.target.result+')');
                  $('.fafaremove_'+id_number).addClass('hide');
                  $scope.isDataChanged = true;
                }
                reader.readAsDataURL(input.files[0]);
              } else {
                $scope.coverNotPresent = true;
                var id_number = input.id;
                var id_number = id_number.split('_').pop(); 
                $('.file_attachment_cover_'+id_number).css('background-image', 'url()');
                $('.fafaremove_'+id_number).removeClass('hide');
              }
            }
        }
        for (var i = 0; i < $scope.newSlot; i++) {           
          input = jQuery('<div class="col-xs-6 col-sm-4 all-remove"><label id="addMoreLabel">'+$scope.addMoretext+'</label><div class="uploading-album-image margin-top-20 new-class-inspiration-image new_file_attachment_'+i+'"><div class="fafa_'+i+'"><i class="fa fa-picture-o newfafaremove_'+i+'" aria-hidden="true"></i></div><input type="file" id="newAttachments_'+i+'" name="attachments[]"></div></div>');
          jQuery('.image-slot').append(input);      
          function readURL1(input) {
            if (input.files && input.files[0]) {
              var reader = new FileReader();
              reader.onload = function(e) {
                var id_number = input.id;
                var id_number = id_number.split('_').pop();
                $('.new_file_attachment_'+id_number).css('background-image', 'url('+e.target.result+')');
                $('.newfafaremove_'+id_number).remove();
                $scope.isDataChanged = true;
              }
              reader.readAsDataURL(input.files[0]);
              }
            }            
            $("#newAttachments_"+i).change(function() {              
              $scope.isDataChanged = true;
              $scope.$apply();
              readURL1(this);
            });
        }
      },200);
      }
      else {
        confirmDialogueAlert("Error", response.data.message, "error", false, '#8972f0', confirmButtonText, '', true, '', '', deleteredirect);
      }
    });
  }
  $scope.InspirationBankfunction();
  mpfactory.Portfoliocategory({'language':getLanguage}).then(function (response) {
    if (response.data.status == 1) {
      $scope.categories = response.data.data;
    } else {
    }
  });
  function readURL(input,imageId) {     
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        insertChnagedImagesID(imageId);
        $('.cover-image_'+imageId).css('background-image', 'url(' + e.target.result + ')');
        $scope.isDataChanged = true;
      }
      reader.readAsDataURL(input.files[0]);
    }
  } 
  $scope.ChangedCoverImages = [];
  function insertChnagedImagesID(id){    
    $scope.ChangedCoverImages.push(id);    
  }
  function deleteredirect() {
    window.location.replace('seller-inspiration-bank');
  }
  function deletefunction() {    
    mpfactory.Deleteinspirationbankimage({ 'image_id': $scope.image_id, 'project_id': $scope.project_id ,'language':getLanguage, 'imageType':$scope.imageType}).then(function (response) {      
      if (response.data.code == 2)
        window.location.replace("sixteen-login");
      if (response.data.status == 1) {
          $scope.InspirationBankfunction();        
          swal.close();         
      }     
      else {          
          simpleAlert('error', '', response.data.message);
      }      
    });
  }
  $scope.Deleteinspirationbankimage = function (image_id, project_id) {
    $scope.isDataChanged = true; 
    $scope.image_id      = image_id;
    $scope.project_id    = project_id;
    $scope.imageType     = 4;
    confirmDialogue('', 'Image Deleted', '', deletefunction);
    
  }


  $scope.Deleteinspirationbankcoverimage = function (image_id, project_id) {
    $scope.isDataChanged = true; 
    $scope.image_id      = image_id;
    $scope.project_id    = project_id;
    $scope.imageType     = 3;    
    confirmDialogue('', 'Image Deleted', '', deletefunction);
    $scope.coverNotPresent = true;
  }



  $scope.SaveinspirationBank = function () {  
    var categoryClickCount = 0;  
    var title = $scope.inspirationBankInfo.upload_title;    
    $scope.description = CKEDITOR.instances.editor.getData();   
    if(title == "" || title == undefined){
      if(getLanguage=='en'){
          text='The "Title" is required.';         
        }else{
          text='请输入“标题”。';          
        }
      simpleAlert('error', '', text);
      return;
    } 
    if($scope.description==undefined || $scope.description==""){
          if(getLanguage=='en'){
              simpleAlert('error','','The "Description" is required.');
          } else {
              simpleAlert('error','','“描述”是必需的。');
          }
          return;
      }   
    $("input[name='skills[]']").each( function () {
        if($(this)[0].checked == true){
            categoryClickCount++;
        }
    });  
    if(categoryClickCount==0){
        if(getLanguage=='en'){
            simpleAlert('error','','Please Select Atleast 1 Category.');
        } else {
            simpleAlert('error','','请选择至少1个类别。');
        }
        return;
    }
    if($scope.coverNotPresent == true){
        if(getLanguage=='en'){
            simpleAlert('error','','Upload Cover image.');
        } else {
            simpleAlert('error','','上传封面图片。');
        }
        return;
    } 
    showLoader('.edit-portfolio', 'Please wait');
    var newInspirationBank = new FormData($("#formupload")[0]);
    newInspirationBank.append("upload_description", CKEDITOR.instances.editor.getData());
    newInspirationBank.append('inspirationBankInfo', JSON.stringify($scope.inspirationBankInfo));
    newInspirationBank.append('changedCoverImages', JSON.stringify($scope.ChangedCoverImages));
    newInspirationBank.append('language', getLanguage);
    mpfactory.Editinspirationbank(newInspirationBank).then(function (response) {      
      if (response.data.status == 1) {
        hideLoader('.edit-portfolio', hideText);
        confirmDialogueAlert(text, response.data.message, "success", false, '#8972f0', confirmButtonText, '', true, '', '', deleteredirect);
      } else {       
        hideLoader('.edit-portfolio', hideText);
        simpleAlert('error', '', response.data.message);
      }
    });
  }
}]);