/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 30th March 2018
 Name    : MPBuyerProjectInfoPastController
 Purpose : All the functions for MP login page
 */
angular.module('sixcloudApp').controller('MPBuyerProjectInfoPastController', ['$sce', 'mpfactory', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', function($sce, mpfactory, $routeParams, $http, $scope, $timeout, $rootScope, $filter) {
    console.log('MPBuyerProjectInfoPast controller loaded');
    $('body').css('background','#e3eefd');
    $timeout(function(){
        if($rootScope.loginUserData.status == 0){
            window.location.replace("sixteen-login");
        }
        $('.buyer-dashboard-menu').find('li').removeClass('active');
        $('#past_projects').addClass('active');
    },500);
    $('#ProjectInfoBtn').click(function() {
        $('#projectInfoText').slideToggle();
    });
$scope.current_domain = current_domain;
    $scope.limit = 2;
    $scope.scrollEventCallback = function() {
        if (($(window).scrollTop() + 200) >= $(document).height() - $(window).height() - 200) {
            $scope.limit = $scope.limit + 2;
        }
    }

    $scope.mp_project_id = $routeParams.slug;
    $scope.Projectfunction = function(){
        mpfactory.Designerprojectinfo({'slug':$scope.mp_project_id}).then(function(response) {
            if(response.data.code == 2)
                window.location.replace("sixteen-login");
            if (response.data.status == 1)
            {
                $scope.projectInfo = response.data.data;
                $scope.messages_total = response.data.data.message_board.length
            }
            else {
            }
        });
    }
    $scope.Projectfunction();
 
}]);