

angular.module('sixcloudApp').controller('MPBuyerGalleryProductInfoController', function($scope) {
        $scope.message = 'Look! I am an about page.';
          $('body').css('background','#e3eefd');
          var galleryTop = new Swiper('.gallery-top', {
               spaceBetween: 15,
              // loop:true,
               //effect:'flip',
               navigation: {
                 nextEl: '.swiper-button-next',
                 prevEl: '.swiper-button-prev',
               },
             });
             var galleryThumbs = new Swiper('.gallery-thumbs', {
               loop:true,
                mousewheel: true,
               spaceBetween: 15,
               direction:'horizontal',
                slidesPerView: 4,
               //centeredSlides: true,
               //slidesPerView: 'auto',
               touchRatio: 0.2,
               slideToClickedSlide: true,


                breakpoints: {

                  767: {
                     direction:'horizontal',
                    slidesPerView: 4,
                    spaceBetween: 15,
                  },
                480: {
                   direction:'horizontal',
                    slidesPerView: 3,
                    spaceBetween: 15,
                  }
                }
             });
             galleryTop.controller.control = galleryThumbs;
             galleryThumbs.controller.control = galleryTop;
    });