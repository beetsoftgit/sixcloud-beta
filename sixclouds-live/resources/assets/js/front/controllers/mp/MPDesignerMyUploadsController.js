/*
 Author  : Komal Kapadi (komal@creolestudios.com)
 Date    : 9th April 2018
 Name    : MPDesignerMyUploadsController
 Purpose : All the functions for MP designer my uploads
 */
angular.module('sixcloudApp').controller('MPDesignerMyUploadsController', ['Designermyuploadscount', '$sce', 'mpfactory', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', function(Designermyuploadscount, $sce, mpfactory, $routeParams, $http, $scope, $timeout, $rootScope, $filter) {    
    console.log("MPDesignerMyUploads InspirationBank Controller");
    $scope.uploadCount = Designermyuploadscount.data.data;
    $scope.filterString = '';
    initSample();
    $('body').css('background', '#e3eefd');
    $ins_file_valid = true;
    var coverImageArray = [];
    $timeout(function() {
        if ($rootScope.loginUserData.status == 0) {
            window.location.replace("sixteen-login");
        }
        $('.designer-dashboard-menu').find('li').removeClass('active');
        $('#designer_myuploads').addClass('active');
        CKEDITOR.instances.editor;
    }, 500);
    getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
    changeLanguage(getLanguage);
    $('#chk_lang').change(function() {
        getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
        changeLanguage(getLanguage);
    });
    
    function changeLanguage(lang){
        if(lang=="chi"){
            $scope.english = false;
            $scope.chinese = true;
            getLanguage=='chi';
        }    
        else{
            $scope.english = true;
            $scope.chinese = false;
            getLanguage=='en';
      }
    }
    mpfactory.Portfoliocategory({'language':getLanguage}).then(function(response) {
        
        if (response.data.status == 1) {
            $scope.categories = response.data.data;            
        } else {}
    });

    (function() {
        $('.FlowupLabels').FlowupLabels({
            feature_onInitLoad: true,
            class_focused: 'focused',
            class_populated: 'populated'
        });
    })();
    $scope.showUploadForm = function(){
        $('upload-files-list1').html('');
        $('upload-files-list').html('');
        $('#InspirationBankModal').modal('show');
        CKEDITOR.instances.editor;
    }
    document.querySelector("html").classList.add('js');
    /*Tab functionality*/
    $('.dmu-gallery-tab').click(function() {
        $('.dmu-inspiration-bank-tab').removeClass('active');
        $(this).addClass('active');
        $('.dmu-inspiration-bank-content').hide();
        $('.btn-dmu-un-Ibc').hide();
        $('.dmu-gallery-content').fadeIn();
        $('.btn-dmu-un-g').show();
    });
    $('.dmu-inspiration-bank-tab').click(function() {
        $('.dmu-gallery-tab').removeClass('active');
        $(this).addClass('active');
        $('.dmu-gallery-content').hide();
        $('.btn-dmu-un-g').hide();
        $('.dmu-inspiration-bank-content').fadeIn();
        $('.btn-dmu-un-Ibc').show();
    });
    /*Tab functionality END */
    $scope.endto = $scope.startfrom + 1;
    $scope.currentPage = $scope.currentPaginate = 1;
    $scope.range = $scope.rangepage = $scope.rangePaging = [];
    $scope.customGalleryProductList = function(pageNumber) {
        mpfactory.Designergalleryuploads(pageNumber).then(function(response) {
            if (response.data.status === 0) {
                return false;
            }
            $scope.galleryProducts = response.data.data.data;
            $scope.pagination = response.data.data;
            $scope.currentGalleryPaging = response.data.data.current_page;
            $scope.rangeGallerypage = _.range(1, response.data.data.last_page + 1);
        });
    };
    $scope.customGalleryProductList(1);
    $scope.customInsBankProductList = function(pageNumber,searchBy) {
        searchBy =  $scope.filterString ? $scope.filterString : undefined;
        mpfactory.Designerinspirationbankuploads(pageNumber,searchBy).then(function(response) {
            if (response.data.status === 0) {
                return false;
            }
            $scope.bankProducts = response.data.data.data;
            $scope.paginate = response.data.data;
            $scope.currentBankPaging = response.data.data.current_page;
            $scope.rangeBankpage = _.range(1, response.data.data.last_page + 1);
        });
    };
    $scope.customInsBankProductList(1);
    var fileSize = 0;

    $scope.submitToInsBank = function() { 
        var categoryClickCount = 0;
        $scope.description = CKEDITOR.instances.editor.getData();
        if($scope.ins_upload_title==undefined || $scope.description == undefined || $scope.description == ""){
            if($scope.ins_upload_title==undefined){
                if(getLanguage=='en'){
                    simpleAlert('error','','The "Title" is required.');
                } else {
                    simpleAlert('error','','请输入“标题”。');
                }
                return;
            }            
            if($scope.description==undefined || $scope.description==""){
                if(getLanguage=='en'){
                    simpleAlert('error','','The "Description" is required.');
                } else {
                    simpleAlert('error','','“描述”是必需的。');
                }
                return;
            }
        } else {
            $("input[name='skills[]']").each( function () {
                if($(this)[0].checked == true){
                    categoryClickCount++;
                }
            });  
            if(categoryClickCount==0){
                if(getLanguage=='en'){
                    simpleAlert('error','','Please Select Atleast 1 Category.');
                } else {
                    simpleAlert('error','','请选择至少1个类别。');
                }
                return;
            }
            var totalFiles = 0;
            totalFiles = file_attachments.length + coverImageArray.length;
            if(coverImageArray.length == 0 ) {
                hideLoader(".submit_bank",'Submit');
                if(getLanguage=='en'){
                    simpleAlert('error','','Upload Cover image.');
                } else {
                    simpleAlert('error','','上传封面图片。');
                }
                return;     
            }
            if(file_attachments.length == 0){
                hideLoader(".submit_bank",'Submit');
                if(getLanguage=='en'){
                    simpleAlert('error','','Upload at least 1 Attachment image.');
                } else {
                    simpleAlert('error','','上传至少1个附件图片。');
                }
                return;
            }
            if(totalFiles==0){
                hideLoader(".submit_bank",loadertext);
                if(getLanguage=='en'){
                    simpleAlert('error','','Upload at least 1 image.');
                } else {
                    simpleAlert('error','','上传至少1个图像。');
                }
                return;
            }
            if(fileSize<30000000){
                var formData = new FormData($("#inspirationbank_form")[0]);
                formData.append('remove_attachments', filesToRemove);
                formData.append('remove_cover_files', coverFilesToRemove);
                formData.append('file_attachments', file_attachments);
                formData.append('cover_file_attachments', cover_file_attachments);
                formData.append("upload_description", CKEDITOR.instances.editor.getData());
                formData.append("language",getLanguage);
                showLoader('.submit_bank','Please wait');
                $.ajax({
                    url: BASEURL + "Designerinsbankupload",
                    type: "post",
                    processData: false,
                    contentType: false,
                    data: formData,
                    beforeSend: function() {},
                    success: function(response) {                            
                        if (response.status == 1) {
                            hideLoader('.submit_bank', 'Submit');
                            $('#inspirationbank_form')[0].reset();
                            $('#InspirationBankModal').modal('hide');
                            $('.upload-files-list1').empty();
                            $('.upload-files-list').empty();
                            CKEDITOR.instances.editor.setData("")
                            $scope.ins_upload_title=undefined;
                            $scope.category_id=undefined;
                            $scope.category_count=0;
                            coverImageArray = [];
                            file_attachments = [];
                            //simpleAlert('success', 'Inspiration Bank', response.message, true);
                            if(getLanguage=='en'){
                                simpleAlert('success', 'Inspiration Bank', response.message, true);
                            } else {
                                simpleAlert('success', '灵感银行', response.message, true);
                            }
                        } else {
                            if(getLanguage=='en'){
                                hideLoader('.submit_bank', 'Submit');    
                            }else{
                                hideLoader('.submit_bank', '提交');
                            }
                            
                            if(getLanguage=='en'){
                                simpleAlert('error', 'Inspiration Bank', response.message, true);
                            } else {
                                simpleAlert('error', '灵感银行', response.message, true);
                            }
                            
                        }
                        $scope.customInsBankProductList(1);
                    },
                    error: function(data) {
                        console.log('Error');
                    }
                });
            } else {
                hideLoader(".submit_bank",'Submit');
                if(getLanguage=='en'){
                    simpleAlert('error','','Files size limit exceeded.\n(Only 30mb allowed combining all files)');
                } else {
                    simpleAlert('error','','超出文件大小限制\n(只有30mb允许组合所有文件)');
                }
                
                return;
            }
        }
    };
    $scope.submitToGallery = function(data) {
        if(getLanguage=='en'){
            var loadertext        = 'Submit';
        }else{
            var loadertext        = '提交';
        }
        var formData = new FormData($("#gallery_form")[0]);
        formData.append('category_id', $('#select-cat option:selected').val());
        showLoader('.submit_gallery');
        $.ajax({
            url: BASEURL + "Designergallerynewupload",
            type: "post",
            processData: false,
            contentType: false,
            data: formData,
            beforeSend: function() {},
            success: function(response) {
                if (response.status == 1) {
                    $('#gallery_form')[0].reset();
                    $('.galleryclose').click();
                    simpleAlert('success', 'Gallery', response.message, true);
                } else {
                    simpleAlert('error', 'Gallery', response.message, true);
                }
                $scope.customGalleryProductList(1);
            },
            error: function(data) {
                console.log('Error');
            }
        });
        hideLoader('.submit_gallery', loadertext);
    };
    $scope.fileGalNames = 'Upload Images';
    $scope.changeGalFiles = function() {
        var files = $('#my-file2').prop("files")
        var names = $.map(files, function(val) {
            return val.name;
        });
        $scope.fileGalNames = names.join(',');
    }
    $scope.changeInsFiles = function() {
        var files = $('#ins_file_name').prop("files");
        var names = $.map(files, function(val) {
            if (val.size > MAX_SIZE) {
                //simpleAlert('Error', 'File is too big.', val.name, true);
                if(getLanguage=='en'){
                    simpleAlert('Error', 'File is too big.', '', true);
                }else{
                    simpleAlert('错误', '文件太大了', '', true);
                }
                $('.file-progress-bar').last().remove();
                $ins_file_valid = false;
                return false;
            }
            return val.name;
        });
        $scope.fileInsNames = names.join(',');
    }
    $scope.resetGalFiles = function() {
        $scope.fileGalNames = 'Upload Images';
        $scope.fileInsNames = 'Upload Images';
    }
    /***
    jQuery plugin to display selected images for inspiration bank
    ***/
    var filesToRemove = [];
    var coverFilesToRemove = [];
    file_attachments=[];
    cover_file_attachments=[];

    $("#addFileDynamicMyUploads").click(function(event) {
        event.stopImmediatePropagation();
        var nextID = 'id' + (new Date()).getTime();
        var html = "<div class='hide' id=\"div" + nextID + "\">";
        html += "<br><input multiple name=\"file_name[]\" id='" + nextID + "' type=\"file\" accept=\"*\" class=\"filestyle\" >";
        html += "<a href=\"javascript:void(0);\" id='rem" + nextID + "' class=\"remImg\">Remove</a></div></div>";
        $("#divImagesUploads").append(html);
        $('#' + nextID).click();
    });

    $(".upload-files-list").on('click', '.remImg', function(event) {
        event.stopImmediatePropagation();
        event.preventDefault();
        var fileName = $(this).attr('id');
        angular.forEach(files, function(value, key){
          if(value.name == fileName){
                fileSize = fileSize-value.size;
                files.splice(key,1);
          }
        });
        angular.forEach(file_attachments, function(value, key){  
            if(value == fileName){
                file_attachments.splice(key,1);
            }
        });
        $(this).parent('.removeFile').remove();
        filesToRemove.push(fileName);
    });

    files = [];
    $(document).on("change", ".filestyle", function(event) {
        event.stopImmediatePropagation();
        for (var i = 0; i < event.target.files.length; i++) {
            if(event.target.files[i].type == 'image/jpeg' || event.target.files[i].type == 'image/png'){
                files.push(event.target.files[i]);
                fileSize = fileSize+files[i].size;
                file_attachments.push(event.target.files[i].name);
                $('.upload-files-list').append("<li class='removeFile uploads_padding'>" + event.target.files[i].name + "<a class='remImg' href=\"javascript:void(0);\" id='" + event.target.files[i].name + "'><i class='fa fa-times'></i></a></li>");
            }else{
                if(getLanguage=='en'){
                    simpleAlert('error','','Only jpg, png and jpeg file formats are allowed for attachment image');
                } else {
                    simpleAlert('error','','附件图像只允许使用jpg，png和jpeg文件格式');
                }
            }    
        }
    });

    /* Cover image select : Start */
    $(document).on("change", "#cover_image", function(event) {
        coverImageArray.push(event.target.files[0].name);
        $('.cover-image-container').html("<li class='removeFile'>" + event.target.files[0].name + "<a class='removeCover close-btn' href=\"javascript:void(0);\" id='" + event.target.files[0].name + "'><i class='fa fa-times'></i></a></li><br/>");
    });

    $(document).on('click', '.removeCover', function(event) {
        var fileName = $(this).attr('id');
        coverImageArray = jQuery.grep(coverImageArray, function(value) {
          return value != fileName;
        });
        coverFilesToRemove.push(fileName);
        $(this).parent('.removeFile').remove();
        $('#cover_image').val('');
    });
    /* Cover image select : End */

    $scope.Searchbyname = function(){
        $scope.filterString = $scope.name_filter;
        if($scope.name_filter.length>5){
            $scope.customInsBankProductList(1,$scope.name_filter);
        } else {
            if($scope.name_filter.length<1){
                $scope.customInsBankProductList(1);
            }
        }
    }
}]);