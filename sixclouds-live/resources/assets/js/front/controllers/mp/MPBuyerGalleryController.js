/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 9th April 2018
 Name    : MPBuyerGalleryController
 Purpose : All the functions for MP buyer gallery page
 */
angular.module('sixcloudApp').controller('MPBuyerGalleryController', ['$sce', 'mpfactory', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', function($sce, mpfactory, $routeParams, $http, $scope, $timeout, $rootScope, $filter) {
    console.log('MPBuyerDashboard controller loaded');
    $('body').css('background','#e3eefd');
    $timeout(function(){
        $('.buyer-dashboard-menu').find('li').removeClass('active');
        $('#gallery').addClass('active');
    },500);
}]);