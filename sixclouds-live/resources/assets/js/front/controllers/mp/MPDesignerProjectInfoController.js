/*
 Author  : Komal Kapadi (komal@creolestudios.com)
 Date    : 11th April 2018
 Name    : MPDesignerProjectInfoController
 Purpose : All the functions for MP designer dashboard
 */
angular.module('sixcloudApp').controller('MPDesignerProjectInfoController', ['$sce', 'mpfactory', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', function($sce, mpfactory, $routeParams, $http, $scope, $timeout, $rootScope, $filter) {
    console.log('MPDesignerProjectInfoController controller loaded');
    $project_slug = $routeParams.slug;
    $scope.msg = {
        'message': ''
    };
    $timeout(function(){
        $('.designer-dashboard-menu').find('li').removeClass('active');
        $('#designer_dashboard').addClass('active');
    },700);
    $('body').css('background', '#e3eefd');
    $('#ProjectInfoBtn').click(function() {
        $('#projectInfoText').slideToggle();
    });
    $scope.current_domain = current_domain;
    (function() {
        $('.FlowupLabels').FlowupLabels({
            /*
             * These are all the default values
             * You may exclude any/all of these options
             * if you won't be changing them
             */
            // Handles the possibility of having input boxes prefilled on page load
            feature_onInitLoad: true,
            // Class when focusing an input
            class_focused: 'focused',
            // Class when an input has text entered
            class_populated: 'populated'
        });
    })();
    $scope.limit = 2;

    getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
    if(getLanguage=='en'){
        $scope.placeholder_messgae = "Type Here";  
        $scope.msg_lang= true;
    }else{
        $scope.placeholder_messgae = "在此输入";
        $scope.msg_lang= false;
    }
    $('#chk_lang').change(function() {
        getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
        if(getLanguage=='en'){
            $scope.placeholder_messgae = "Type Here";    
            $scope.msg_lang= true;
        }else{
            $scope.placeholder_messgae = "在此输入";
            $scope.msg_lang= false;
        }
    });

    function getData() {
        mpfactory.Designerprojectinfo({
            'slug': $project_slug
        }).then(function(response) {
            if (response.data.status == 1) {
                $scope.projectData = response.data.data;
                
                $scope.skill_array=[];
                angular.forEach($scope.projectData.skills, function (skill, index) {
                    if (skill.skill_detail.status==1) {
                        $scope.skill_array.push(skill);
                    }
                });
                $scope.Getunreadnotificationscount();
            } else {
                simpleAlert('error', 'Signup', response.data.message, true);
            }
        });
        mpfactory.Readmessages({
            'slug': $project_slug
        }).then(function(response) {
            if (response.data.status == 1) {
                console.log($scope.projectData);
            } else {
                // simpleAlert('error', 'Signup', response.data.message, true);
            }
        });
        $scope.Getunreadnotificationscount=function(){
            mpfactory.Getunreadnotificationscount().then(function(response) { 
                if (response.data.status === 0) {
                    return false;
                }
                $("#notification_number").text(response.data.data.totalCount);
            });    
        }
        
    }
    getData();
    var fi = $('#fileupload'); //file input 
    var process_url = 'Uploadmessagefile'; //PHP script
    var progressBar = $('<div/>').addClass('progress').append($('<div/>').addClass('progress-bar')); //progress bar
    uploadButton = $('<a/>').prop('href', 'javascript:;'); //upload button
    uploadButton.prop('id', "testid");
    uploadButton.append('<i class="fa fa-check-circle" aria-hidden="true"></i>');
    $timeout(function() {
        uploadButton.on('click', function(uploadButton) {
            var $this = $(uploadButton),
                data = $this.data();
            data.submit().always(function() {
                $this.parent().find('.progress').show();
                $this.parent().find('.remove').remove();
                $this.remove();
            });
        });
    }, 500);
    //initialize blueimp fileupload plugin
    fi.fileupload({
        url: process_url,
        dataType: 'json',
        autoUpload: false,
        acceptFileTypes: /(\.|\/)(pdf|doc|docx|ppt|psd|svg|ico|eps|icns|ai|png|jpeg|jpg|xlsx|PDF|DOC|DOCX|PPT|PSD|SVG|ICO|EPS|ICNS|AI|PNG|JPEG|JPG|XLSX'|gif)$/i,
        maxFileSize: 30000000, //1MB
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent),
        previewMaxWidth: 50,
        previewMaxHeight: 50,
        previewCrop: true,
        dropZone: $('#dropzone')
    });
    $scope.orignionalFileName = [];
    var allowedFormats = ['pdf', 'doc', 'docx', 'ppt', 'psd', 'svg', 'ico', 'eps', 'icns', 'ai', 'png','jpeg', 'jpg', 'xlsx', 'PDF', 'DOC', 'DOCX', 'PPT', 'PSD', 'SVG', 'ICO', 'EPS', 'ICNS', 'AI', 'PNG', 'JPEG', 'JPG', 'XLSX'];
    fi.on('fileuploadadd', function(e, data) {
        console.log(data.files[0]);
        var fileName = data.files[0].name;
        var ext = fileName.split('.').pop();
        if (jQuery.inArray(ext, allowedFormats) !== -1) {
            data.context = $('<div/>').addClass('file-progress-bar').appendTo('.outer-progress-bar-div');
            // return false;
            $.each(data.files, function(index, file) {
                $scope.fileNameUploaded = file.name;
                if ($scope.orignionalFileName.indexOf(file.name) !== -1) {
                    //
                    if(getLanguage=='en'){
                        simpleAlert('Error', 'Oops', 'File already added', true);
                    }else{
                        simpleAlert('错误', '哎呀', '文件已添加', true);
                    }
                    data.context.remove();
                    return false;
                } else {
                    var node = $('<div/>').addClass('file-extension');
                    // var h4 = $('<h4/>').addClass('uploaded_file');
                    var node1 = $('<div/>').addClass('H');
                    var removeBtn = $('<a/>').prop('href', 'javascript:;'); //upload button
                    // var removeBtn = $('<div/>').addClass('close-btn'); //
                    // .text('Remove');
                    removeBtn.append('<div class="close-btn"><strong></strong> <a href="javascript:;"><i class="fa fa-times-circle-o" aria-hidden="true"></i></a></div>');
                    // var removeBtn = $('<button/>').addClass('button btn-red remove').text('Remove');
                    removeBtn.on('click', function(e, data) {
                        var file_name = $(this).parent('.H').siblings('.realFileName').text();
                        mpfactory.Unlinkmsgbrdremovedfile({
                            'file_name': file_name
                        }).then(function(response) {
                            if (response.data.status == 1) {
                                var index = $scope.orignionalFileName.indexOf(file_name);
                                var indexfinal = $scope.finalFiles.indexOf(file_name);
                                $scope.orignionalFileName.splice(index, 1);
                                $scope.finalFiles.splice(indexfinal, 1);
                                //simpleAlert('success', 'Removed', '');
                                if(getLanguage=='en'){
                                    simpleAlert('Error', 'Removed', true);
                                }else{
                                    simpleAlert('错误', '删除', '', true);
                                }
                            } else {}
                        });
                        $(this).parent().parent().remove();
                    });
                    var file_txt = node.append('<span></span>');
                    // var file_txt = node.append('<span>' + file.name + '</span>');
                    var file_size = $('</div>');
                    // file_size.append('<h4 class="file_name">' + file.name + '<div class="close-btn"><strong></strong> <a href="javascript:;"><i class="fa fa-times-circle-o" aria-hidden="true"></i></a></div>' + '</h4>');
                    // nameformat_size(file.size)
                    node.append(file_txt);
                    // node1.appendTo(node);
                    node1.append(removeBtn);
                    // h4.append(file.name);
                    // h4.append(node1);
                    progressBar.clone().appendTo(data.context);
                    if (!index) {
                        node.prepend(file.preview);
                    }
                    node.appendTo(data.context);
                    data.context.append('<h4 class="uploaded_file">' + file.name + ' <div class="close-btn"></h4><strong class="realFileName hide"></strong><span>' + file.size + '</span></div>');
                    file_txt.prependTo(data.context).append(uploadButton.clone(true).data(data));
                    node1.appendTo(data.context);
                }
            });
        } else {
            //simpleAlert("info", "Info", "File not allowed. Allowed file types \n\n (pdf, doc, docx, ppt, psd, svg, ico, eps, icns, ai, jpeg, jpg, xlsx)");
            if(getLanguage=='en'){
                simpleAlert("info", "Info", "File not allowed. Allowed file types \n\n (pdf, doc, docx, ppt, psd, svg, ico, eps, icns, ai, png, jpeg, jpg, xlsx)");
            }else{
                simpleAlert('信息', '信息', "文件不被允许。允许的文件类型  \n\n (pdf, doc, docx, ppt, psd, svg, ico, eps, icns, ai, png, jpeg, jpg, xlsx)");    
            }
            return;
        }
    });
    fi.on('fileuploadprocessalways', function(e, data) {
        var fileName = data.files[0].name;
        var ext = fileName.split('.').pop();
        if (data.files[0].size > MAX_SIZE) {
            //simpleAlert('Error', 'File is too big.', '', true);
            if(getLanguage=='en'){
                simpleAlert('Error', 'File is too big.', '', true);
            }else{
                simpleAlert('错误', '文件太大了', '', true);
            }
            $('.file-progress-bar').last().remove();
            return false;
        }
        if (jQuery.inArray(ext, allowedFormats) !== -1) {
            var index = data.index,
                file = data.files[index],
                node = $(data.context.children()[index]);
            if (file.preview) {
                node.prepend(file.preview);
            }
            if (file.error) {
                node.append($('<span class="text-danger"/>').text(file.error));
            }
            if (index + 1 === data.files.length) {
                if (!!data.files.error) data.context.find('button.upload').prop('disabled');
                // data.context.find('button.upload').prop('disabled', !!data.files.error);
            }
            //uploadButton.click(uploadButton);
            /*var $this = $(uploadButton),
            data = $this.data();*/
            if ($scope.orignionalFileName.indexOf($scope.fileNameUploaded) !== -1) {
                return;
            } else {
                data.submit().always(function(response) {
                    $scope.orignionalFileName.push($scope.fileNameUploaded);
                    var getFileName = $(".uploaded_file:contains('" + file.name + "')"); //$('.uploaded_file').hasText();
                    getFileName.siblings('strong').text(response.data.message);
                });
            }
        } else {
            //simpleAlert("info", "Info", "File not allowed. Allowed file types \n\n (pdf, doc, docx, ppt, psd, svg, ico, eps, icns, ai, png, jpeg, jpg, xlsx)");
            if(getLanguage=='en'){
                simpleAlert("info", "Info", "File not allowed. Allowed file types \n\n (pdf, doc, docx, ppt, psd, svg, ico, eps, icns, ai, png, jpeg, jpg, xlsx)");
            }else{
                simpleAlert('信息', '信息', "文件不被允许。允许的文件类型  \n\n (pdf, doc, docx, ppt, psd, svg, ico, eps, icns, ai, png, jpeg, jpg, xlsx)");    
            }
            return;
        }
    });
    fi.on('fileuploadprogress', function(e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        if (data.context) {
            data.context.each(function() {
                $(this).find('.progress').attr('aria-valuenow', progress).children().first().css('width', progress + '%');
            });
        }
    });
    $scope.finalFiles = [];
    fi.on('fileuploaddone', function(e, data) {
        $scope.finalFiles.push(data.result.data);
        $.each(data.result.files, function(index, file) {
            if (file.url) {
                var link = $('<a>').attr('target', '_blank').prop('href', file.url);
                $(data.context.children()[index]).addClass('file-uploaded');
                $(data.context.children()[index]).find('canvas').wrap(link);
                $(data.context.children()[index]).find('.file-remove').hide();
                var done = $('<span class="text-success"/>').text('Uploaded!');
                $(data.context.children()[index]).append(done);
            } else if (file.error) {
                var error = $('<span class="text-danger"/>').text(file.error);
                $(data.context.children()[index]).append(error);
            }
        });
    });
    fi.on('fileuploadfail', function(e, data) {
        $('#error_output').html(data.jqXHR.responseText);
    });

    function format_size(bytes) {
        if (typeof bytes !== 'number') {
            return '';
        }
        if (bytes >= 1000000000) {
            return (bytes / 1000000000).toFixed(2) + ' GB';
        }
        if (bytes >= 1000000) {
            return (bytes / 1000000).toFixed(2) + ' MB';
        }
        return (bytes / 1000).toFixed(2) + ' KB';
    }
    //$scope.placeholder_messgae = "Type Here";
    
    $scope.Sendmessage = function(mp_project_id, user_id, email_id_to_send_mail, receiver_id) {
        if($scope.message!=undefined||$scope.finalFiles.length>0){;
            showLoader('.send_message', '');
            var formData = new FormData($('#message_form')[0]);
            formData.append('files', $scope.finalFiles);
                mpfactory.Sendmessageform({
                    'user_id': user_id,
                    'mp_project_id': mp_project_id,
                    'message': $scope.message,
                    'files': $scope.finalFiles,
                    'user_role': 1,
                    'receiver_emailid': email_id_to_send_mail,
                    'receiver_id': receiver_id
                }).then(function(response) {
                    hideLoader('.send_message', '<i class="fa fa-paper-plane-o"></i>');
                    if (response.data.status == 1) {
                        getData();
                        $scope.orignionalFileName = [];
                        $scope.finalFiles = [];
                        $scope.message = undefined;
                    } else {
                        $scope.orignionalFileName = [];
                        $scope.finalFiles = [];
                        //simpleAlert('error', '', response.data.message);
                        if(getLanguage=='en'){
                            simpleAlert('error', '', response.data.message);
                        }else{
                            simpleAlert('错误', '', response.data.message);
                        }
                    }
                    $('.outer-progress-bar-div').html('');
                });
        } else {
            //simpleAlert('error','','Type message or select file');
            if(getLanguage=='en'){
                simpleAlert('error','','Type message or select file');    
            }else{
                simpleAlert('错误','','输入消息或选择文件');
            }
        }
    }
    $scope.markCompleted = function(projectId) {
        if(getLanguage=='en'){
            title             = "Are you sure?";
            text              = "Are you really want to mark as Complete?";
            cancelButtonText  = 'Cancel';
            confirmButtonText = 'Ok';
        }else{
            title             = "你确定？";
            text              = "你真的想要标记为完整吗？";
            cancelButtonText  = '取消';
            confirmButtonText = '好';
        }

        swal({
            title: title,
            text: text,
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: true,
            showLoaderOnConfirm: true,
            confirmButtonText: confirmButtonText,
            confirmButtonColor: '#8a75d0',
            cancelButtonText:cancelButtonText,
        }, function() {
            showLoader('.mark-completed', '');
            mpfactory.Designermarkprojectcomplete({
                'projectId': projectId,
                'language':getLanguage,
            }).then(function(response) {
                if(getLanguage=='en'){
                    hideLoader('.mark-completed', 'Mark as completed');    
                }else{
                    hideLoader('.mark-completed', '标记为已完成');
                }
                
                if (response.data.status == 1) {

                    if(getLanguage=='en'){
                        simpleAlert('success', 'Complete Project', response.data.message + 'Redirecting...', false);    
                    }else{
                        simpleAlert('success', '完成项目', response.data.message + '重定向...', false);    
                    }
                    
                    $timeout(function() {
                        window.location = "seller-dashboard";
                    }, 1500);
                } else {}
            });
        });
    }
    $scope.Showmodal = function(whichModal) {
        $scope.whatAction = whichModal;
        $scope.modalTitle = whichModal;
        
        // if(getLanguage=='en'){
        //     $scope.placeholder = whichModal + " Reason";    
        // }else{
        //     $scope.placeholder = whichModal + " 原因";
        // }
        if(getLanguage=='en'){
            $scope.placeholder = " Reason";    
        }else{
            $scope.placeholder = " 原因";
        }
        $scope.reason = undefined;
        $('#Cancel_flag_modal').modal('show');
    }
    $scope.CancelProject = function(reason, projectId) {
        showLoader('.cancel_prj_btn', '');
        mpfactory.DesignerCancelProject({
            'reason': reason,
            'projectId': projectId,
            'language':getLanguage,
        }).then(function(response) {
            if (response.data.status == 1) {
                if (response.data.message == "Seller Reported") {
                    $('#Cancel_flag_modal').modal('hide');
                    $scope.reason = undefined;
                    simpleAlert('success', '', response.data.message);
                } else {
                    if(getLanguage=='en'){
                        title                 = 'Success';
                        var confirmButtonText = 'Ok';
                    }else{
                        title                 = '成功';
                        var confirmButtonText = '好。';
                    }
                    swal({
                        title: title,
                        text: response.data.message,
                        type: "success",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        showLoaderOnConfirm: true,
                        confirmButtonText: confirmButtonText,
                        confirmButtonColor: '#8a75d0',
                    }, function() {
                        window.location = "seller-dashboard";
                    });
                }
            } else {
                simpleAlert('error', '', response.data.message);
            }
        });
        if(getLanguage=='en'){
            hideLoader('.cancel_prj_btn', 'Submit');
        }else{
            hideLoader('.cancel_prj_btn', '提交');
        }
    }
    $scope.scrollEventCallback = function() {
        if (($(window).scrollTop() + 200) >= $(document).height() - $(window).height() - 200) {
            $scope.limit = $scope.limit + 2;
        }
    }
    $scope.submitExtension = function(form) {
        showLoader('.submit_extension');
        $('.extension_option').attr('disabled', 'disabled');
        form.project_id = $project_slug;
        form.language=getLanguage;
        mpfactory.Addextensionforproject(form).then(function(response) {
            if (response.data.status == 1) {
                getData();
                if(getLanguage=='en'){
                    simpleAlert('success', 'New extension', response.data.message, true);
                }else{
                    simpleAlert('成功', '新的扩展', response.data.message, true);
                }
                
            } else {}
            $('.close').click();
        });
        if(getLanguage=='en'){
            hideLoader('.submit_extension', 'SUBMIT');
        }else{
            hideLoader('.submit_extension', '提交');
        }
        
    }
    $scope.resetForm = function() {
        $('#extension_form')[0].reset();
    }
    $scope.Nomoreextension = function() {

        if(getLanguage=='en'){
            simpleAlert('info', '', 'No more extension allowed');
        }else{
            simpleAlert('信息', '信息', '不允许更多扩展');
        }
    }
    $scope.withdraw = function(amount, projectId) {
        
        if(getLanguage=='en'){
            showLoader('.withdraw-button');
        }else{
            showLoader('.withdraw-button','请稍候');
        }
        mpfactory.Addwithdrawrequest({
            'total_amount': amount,
            'projectId': projectId,
            'language':getLanguage,
        }).then(function(response) {
            $('#withdraw_modal').modal('hide');
            if(getLanguage=='en'){
                hideLoader('.withdraw-button', 'SUBMIT');
            }else{
                hideLoader('.withdraw-button', '提交');
            }
            $scope.total_amount = undefined;
            if (response.data.status == 1) {
                if(getLanguage=='en'){
                    simpleAlert('成功', '', response.data.message);    
                }else{
                    simpleAlert('成功', '', response.data.message);    
                }
            } else {
                simpleAlert('error', '', response.data.message);
            }
        });
    }
    $scope.Changecancelprojectstatus = function(accept_or_reject, mp_project_status, mp_project_detail_status, project_id, designer_id, creator_id, reason) {
        mpfactory.Changecancelprojectstatus({
            'accept_or_reject': accept_or_reject,
            'mp_project_status': mp_project_status,
            'mp_project_detail_status': mp_project_detail_status,
            'project_id': project_id,
            'designer_id': designer_id,
            'creator_id': creator_id,
            'reason': reason
        }).then(function(response) {
            /*if (accept_or_reject == 'accept') {
                response.data.message = "Successfully Accepted";
            } else {
                response.data.message = "Successfully Rejected";
            }
            simpleAlert('success', 'Success', response.data.message);*/
            if(response.data.status==1){
                if(getLanguage=='en'){
                    simpleAlert('success', 'Success', response.data.message);    
                }else{
                    simpleAlert('成功', '成功', response.data.message);    
                }
            }
            $("#accept").hide();
            $("#reject").hide();
            $('#cancel_request_reason_modal').modal('hide');
        });
    }
    $scope.getData = function(project_id, designer_id, creator_id) {
        $scope.project_id = project_id;
        $scope.designer_id = designer_id;
        $scope.creator_id = creator_id;
    }
}]);