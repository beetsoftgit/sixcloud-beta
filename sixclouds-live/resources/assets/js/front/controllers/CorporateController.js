
app = angular.module('sixcloudCorporateApp', ['ngRoute','pascalprecht.translate']);
/*var myApp = angular.module('sixcloudAboutApp', []);*/

// if (Cookies.get('language') == 'undefined' || Cookies.get('language') == '' || Cookies.get('language') == undefined) Cookies.set('language', 'en');
app.config(function($routeProvider, $translateProvider) {
    $translateProvider.translations('en', {
    	
    	lbl_lang_chng:'中文',
        lbl_sixclouds:'SixClouds',
    	lbl_the_company:'The Company',
    	lbl_mission:'Mission',
    	lbl_vision:'Vision',
    	lbl_our_products:'Our Products',
        lbl_about_us:'About Us',
    	lbl_mission_text:'Bringing quality content and knowledge programmes through technology to learners, to help them build confidence and strong foundations in the pursuit of personal excellence.',
    	lbl_vision_text:'Making learning for all ages fun and joyful.',   
    	lbl_ignite:'Ignite',
        lbl_ignite_text_1:'Need access to creative design talents?',
        lbl_ignite_text_2:'Want to showcase your creative flair?',
        lbl_ignite_text_3:'Sixteen is the sweet spot for folks who need creative',
        lbl_ignite_text_4:'services and those who have the creative juices.',
    	lbl_sixteen:'Sixteen',
        lbl_sixteen_text:'IGNITE, our learning platform offers access to quality educational content and learning programmes.',
        lbl_sixteen_text1:'They are designed using animation as a fun learning medium. Through thematic storytelling, lessons will never be dry and boring again.',
        lbl_sixteen_text2:'Available on this platform:',
        lbl_sixteen_li1:'English as a Second Language',
        lbl_sixteen_li2:'Singapore-model Mathematics ',
        lbl_sixteen_li3:'Let’s IGNITE your child’s learning today!',
        lbl_discover:'Discover',
        lbl_proof_reading:'Proof Reading',
        lbl_quick_links:'Quick Links',
        lbl_become_seller:'Become a Seller',
        lbl_become_buyer:'Become a Buyer',
        lbl_customer_support:'Customer Support',
        lbl_terms_service:'Terms of Service',
        lbl_privacy_policy:'Privacy Policy',
        lbl_all_rights_reserved:'All rights reserved',
        lbl_core_values:'Core Values',
        lbl_our_team:'Our Team',

        lbl_our_partners:'Our Partners',
        lbl_learn_more: 'Learn More',
        lbl_mathematics_programme:'Mathematics Programme',
        lbl_smile_sg: 'We translate complex Maths concepts into simple visual ideas and illustrations, to understand what’s taught and build strong foundations. Available for Primary 1 to Secondary 2, according to the Singapore MOE curriculum.',
        lbl_smile_intl:'Introducing Singapore Maths learning methodology to international learners, who want to discover a different approach to concept-building and problem-solving. Done in American-accented voice-overs, check out 6 grades of learning programmes and the topics covered.',
        lbl_maze:'Designed for students in Central Asia, our Maths learning programme introduces you to Singapore Maths learning methodology, with 8 grades of lessons in Russian.',
        lbl_smile_ph:'Bringing the joy of Math learning with Singapore Maths methodology. Creative learning at its best with fully animated concept videos, suitable for Grade 1 – 6 learners.',
        lbl_english_programme:'English Programme',
        lbl_buzz_description:'Our English learning programme lets you learn functional language through thematic storytelling and scenarios. Select from 3 levels of programmes, to help you progress with confidence.',
    }).translations('chi', {
    	lbl_lang_chng:'English',   
        lbl_sixclouds:'六云',
    	lbl_the_company:'六云简介',
    	lbl_mission:'任务',
    	lbl_vision:'愿景',
		lbl_our_products:'我们的产品',    	
		lbl_about_us:'关于我们',
		lbl_mission_text:'通过科技，把优质内容和知识项目带给学者，帮助他们建立信心和巩固基础。',
		lbl_vision_text:'让所有年龄层的学者都能快乐和愉悦的学习。',
		lbl_ignite:'智宝',
        lbl_ignite_text_1:'创意和设计的聚集点',
        lbl_ignite_text_2:'想找独特和多元化的设计？',
        lbl_ignite_text_3:'想展示自己的创意构想？',
        lbl_ignite_text_4:'十六就是您在寻找的平台。',
		lbl_sixteen:'十六',
        lbl_sixteen_text:'智宝，我们的学习平台提供优质教育内容和学习课程。',
        lbl_sixteen_text1:'课程设计使用动画作为一个有趣的学习媒介。通过主题故事的讲述，课程将不再枯燥乏味。',
        lbl_sixteen_text2:'平台提供：',
        lbl_sixteen_li1:'英语作为第二语言',
        lbl_sixteen_li2:'新加坡数学学习方式',
        lbl_sixteen_li3:'让我们点燃您孩子对学习的热诚吧！',
        lbl_discover:'发现',
        lbl_proof_reading:'证明阅读',
        lbl_quick_links:'快速链接',
        lbl_become_seller:'成为卖家',
        lbl_become_buyer:'成为买家',
        lbl_customer_support:'客服中心',
        lbl_terms_service:'服务条款',
        lbl_privacy_policy:'隐私政策',
        lbl_all_rights_reserved:'版权所有',
        lbl_core_values:'核心理念',
        lbl_our_team:'我们的团队',
        lbl_our_partners:'我们的合作伙伴',
        lbl_learn_more: '了解更多',
        lbl_mathematics_programme: '数学课程',
        lbl_smile_sg: '我们将复杂的数学概念转化为简单的视觉概念和插图，以便理解所学的知识并建立数学基础。依据新加坡教育部的课程表，适用于小学1至中学2年级。',
        lbl_smile_intl:'专为国际学生设计，把新加坡数学学习方式引进。让学生有与众不同的方法来建立概念构建与问题解决。在美国口音的配音中，拥有6个级别的课程让您挑选。',
        lbl_maze:'专为中亚学生设计，把新加坡数学学习方式引进。拥有8各俄语级别的课程让您挑选。',
        lbl_smile_ph:'用新加坡数学方法带来数学学习的乐趣。创意学习的最佳状态是全动画概念视频，适合一到六年级的学者。',
        lbl_english_programme:'英语课程',
        lbl_buzz_description:'我们的英语学习课程让您通过主题故事和情景来学习功能性语言。从3个级别的课程中选择，帮助您自信地进步。',
    }).translations('ru', {
        lbl_lang_chng:'中文',
        lbl_sixclouds:'SixClouds',
        lbl_the_company:'Компания',
        lbl_mission:'Цель',
        lbl_vision:'Введение',
        lbl_our_products:'Наш продукт',
        lbl_about_us:'О Нас',
        lbl_mission_text:'Доведение качественного контента и программ знаний с помощью технологий до учащихся, чтобы помочь им построить уверенность и прочные основы в стремлении к личному совершенству.',
        lbl_vision_text:'Делает обучение для любого возраста увлекательным и веселым.',   
        lbl_ignite:'Ignite',
        lbl_ignite_text_1:'Need access to creative design talents?',
        lbl_ignite_text_2:'Want to showcase your creative flair?',
        lbl_ignite_text_3:'Sixteen is the sweet spot for folks who need creative',
        lbl_ignite_text_4:'services and those who have the creative juices.',
        lbl_sixteen:'Sixteen',
        lbl_sixteen_text:'IGNITE, наша учебная платформа предлагает доступ к качественному образовательному контенту и учебным программам. уроки никогда больше не будут сухими и скучными.',
        lbl_sixteen_text1:'Они разработаны с использованием анимации в качестве веселой среды обучения. Благодаря тематическому повествованию ',
        lbl_sixteen_text2:'Доступно на этой платформе:',
        lbl_sixteen_li1:'Английский как второй язык ',
        lbl_sixteen_li2:'Сингапур-математическая модель',
        lbl_sixteen_li3:'Давайте ЗАЖЖЕМ обучение вашего ребенка сегодня!',
        lbl_discover:'Discover',
        lbl_proof_reading:'Proof Reading',
        lbl_quick_links:'Быстрые ссылки',
        lbl_become_seller:'Become a Seller',
        lbl_become_buyer:'Become a Buyer',
        lbl_customer_support:'Поддержка клиентов',
        lbl_terms_service:'Условия обслуживания',
        lbl_privacy_policy:'Конфиденциальность и безопасность',
        lbl_all_rights_reserved:'Все  права защищены',
        lbl_core_values:'Фундаментальные ценности',
        lbl_our_team:'Наша команда',
        lbl_our_partners:'Our Partners',
        lbl_learn_more: 'Узнать больше',
        lbl_mathematics_programme: 'Математическая программа',
        lbl_smile_sg: 'Мы переводим сложные математические понятия в простые визуальные идеи и иллюстрации, чтобы понять, что происходит, учим и строим крепкий фундамент. Доступно с  1-го по 8-го класс, согласно учебному плану Министерства образования Сингапура.',
        lbl_smile_intl:'Знакомство с методикой обучения математике в Сингапуре для иностранных студентов, кто хочет открыть другой подход к созданию концепции и решению проблем. Сделано в закадровом голосе с американским акцентом, проверьте 6 классов обучающих программ и затронутые темы.',
        lbl_maze:'Разработанная для студентов в Центральной Азии, наша программа обучения математике знакомит вас по методике изучения математики в Сингапуре, с 8 классами уроков нарусском языке.',
        lbl_smile_ph:'Принося радость  в обучение математики с помощью сингапурской методологии математики. Творческое обучение в лучшем виде с полностью анимированными концептуальными видеороликами, подходящими для учащихся 1-6 классов.',
        lbl_english_programme:'Английская Программа',
        lbl_buzz_description:'Наша программа изучения английского языка позволяет изучать функциональный язык с помощью тематических рассказов и сценариев. Выберите один из 3 уровней программ, чтобы помочь вам уверенно развиваться.',
    });
    $translateProvider.preferredLanguage(Cookies.get('language'));
});
/* To change the interolate provider need to change it's originonal brackets.*/
app.config(['$interpolateProvider', function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
}]);
/* for changing title */
app.run(['$location', '$rootScope', '$http', '$timeout', function($location, $rootScope, $http, $timeout) {
    $rootScope.$on('$routeChangeSuccess', function(event, current, previous) {
        if (current.$$route != undefined) $rootScope.title = current.$$route.title;
        document.body.scrollTop = document.documentElement.scrollTop = 0;
    });
}]);
app.controller('CorporateController', ['$scope', '$translate', '$location', '$rootScope', '$timeout', function ($scope, $translate, $location, $rootScope, $timeout){
	console.log("Corporate Controller");
	
	var ctrl = this;
    $scope.baseurl = BASEURL;
    if (Cookies.get('language') == 'undefined' || Cookies.get('language') == '' || Cookies.get('language') == undefined) {
        $scope.url = window.location.host;
        $scope.url = $scope.url.split('.');
        $scope.url = $scope.url[2];
        $timeout(function() {
            if($scope.url==undefined){
                $timeout(function() {
                    $scope.changeLang('en');
                },500);
            }
            if($scope.url == 'net' && $scope.url!=undefined){
                $timeout(function() {
                    $scope.changeLang('en');
                },500);
            }
            if($scope.url == 'cn' && $scope.url!=undefined){
                $timeout(function() {
                    $scope.changeLang('chi');
                },500);
            }
        },1000);
    }
    $scope.changeLangs = Cookies.get('language');
    $scope.changeLang = function(lang) {
        if (lang == 'en') $scope.changeLangs = 'en';
        else if (lang == 'chi') $scope.changeLangs = 'chi';
        else $scope.changeLangs = 'ru';
        var getLanguage = lang;
        ctrl.language = getLanguage;
        Cookies.set('language', ctrl.language);
        $translate.use(ctrl.language);
    }
   

}]);
