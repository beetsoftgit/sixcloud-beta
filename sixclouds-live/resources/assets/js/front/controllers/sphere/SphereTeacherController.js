/*
 Author  : Jainam Shah
 Date    : 22nd March, 2019
 Name    : SphereTeacherController
 Purpose : All the functions for sphere teachers
 */
angular.module('sixcloudApp').controller('SphereTeacherController', ['spherefactory', '$location', '$rootScope', '$http', '$scope', '$timeout', 'Getspheresubjects', function(spherefactory, $location, $rootScope, $http, $scope, $timeout, Getspheresubjects) {
    
    if(Getspheresubjects) {
        $scope.subjects = Getspheresubjects.data.data;
    }
    $scope.filters = {
        selectedSubject: '',
        searchText: ''
    }

    $scope.getTeachers = function(pageNumber) {
        if(pageNumber===undefined) {
            pageNumber = '1';
        }
        spherefactory.Getteacherslist({'pageNumber' : pageNumber, 'per_page' : 8, 'filters' : $scope.filters}).then(function(response){
            if(response.data.status==1) {
                $scope.totalPages   = response.data.data.total;
                $scope.paginate     = response.data.data;
                $scope.currentPage  = response.data.data.current_page;
                var pages = [];
                pages = _.range(1, response.data.data.last_page + 1);
                $scope.range = pages;
                $scope.teachersList = response.data.data.data;
            }
        });
    }
    $scope.getTeachers();

    $scope.filterList = function() {
        $scope.filters.searchText = $scope.searchText;
        $scope.filters.selectedSubject = $scope.selectedSubject;
        $scope.getTeachers();
    }

    $("#searchText").on("keydown", function (e) {
        if (e.keyCode === 13) {  //checks whether the pressed key is "Enter"
            if($scope.searchText != undefined && $scope.searchText != '') {
                $scope.filterList();
            }
        }
    });

}]);


app.directive('showMore', [function() {
   return {
       restrict: 'AE',
       replace: true,
       scope: {
           text: '=',
           limit:'='
       },
       template: '<div><div ng-show="largeText"> {{ text | subString :0 :end }}<span ng-show="isShowMore">...</span><a href="javascript:;" ng-click="showMore()" ng-show="isShowMore"></span><b>  Read More</b></a><a href="javascript:;" ng-click="showLess()" ng-hide="isShowMore"><b> Read Less</b> </a></div><div ng-hide="largeText">{{ text }}</div></div> ',
       link: function(scope, iElement, iAttrs) {
           scope.end = scope.limit;
           scope.isShowMore = true;

           scope.largeText = true;

           if (scope.text.length <= scope.limit) {
               scope.largeText = false;
           };
           scope.showMore = function() {
               scope.end = scope.text.length;
               scope.isShowMore = false;
           };
           scope.showLess = function() {
               scope.end = scope.limit;
               scope.isShowMore = true;

           };
       }
   };
}]);

app.filter('subString', function() {
   return function(str, start, end) {
       if (str != undefined) {
           return str.substr(start, end);
       }
   }
});