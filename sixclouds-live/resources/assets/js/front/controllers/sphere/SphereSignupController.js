/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 9th Jan, 2019
 Name    : SphereSignupController
 Purpose : All the functions for sphere signup
 */
angular.module('sixcloudApp').controller('SphereSignupController', ['$sce', 'spherefactory', '$location', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', function($sce, spherefactory, $location, $routeParams, $http, $scope, $timeout, $rootScope, $filter) {
    
    $scope.url = $location.url();
    /*spherefactory.Gettypeandyear().then(function(response) {
        if(response.data.status==1){
            $scope.years = response.data.data.years;
            $scope.types = response.data.data.type;
            $scope.Getcommissions($scope.year,$scope.type,$scope.team.member_name);
        }
    });*/
    /*$( function() {
        $( "#datepicker" ).datetimepicker({
            pickTime: false,
            format: 'DD/MM/YYYY',
            maxDate: new Date(),
        });
    });*/
    spherefactory.Getcountry().then(function(response){
        if(response.data.status==1)
        {
            $scope.countries = response.data.data.country;
        }      
    });
    $scope.getstates = function(id){
        spherefactory.Getstatesforcountry({'id': id}).then(function(response){
            if(response.data.status==1)
            {
                $scope.states = response.data.data;
                
            }
        });
    }
    $scope.getcities = function(id){
        spherefactory.Getcitiesforstate({'id': id}).then(function(response){
            if(response.data.status==1)
            {
                $scope.cities = response.data.data;
            }
        });
    }
    $('#country_dropdown').on('change',function(){
        var country = $('#country_dropdown').val();
        if(country!=''&&country!=undefined){
            country = JSON.parse(country);
            $scope.phonecode = country['phonecode'];
            country = country['id'];
            if(country!=''){
                $scope.getstates(country);
            } else {
                $scope.states = undefined;
                $scope.cities = undefined;
            }
            $('#state_dropdown').val('');
            $('#city_dropdown').val('');
        }
    });
    $('#state_dropdown').on('change',function(){
        var state = $('#state_dropdown').val();
        if(state!=''){
            $scope.getcities(state);
        } else {
            $scope.cities = undefined;
        }
        $('#city_dropdown').val('');
    });
    var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,16})");
    $scope.Checkexpression = function(password){
        if(strongRegex.test(password)) {
                $scope.new_password_expression_error = false;
        } else {
                $scope.new_password_expression_error = true;
        }
    }
    $scope.child = {};
    $scope.calculateAge = function(dob,forWhome) {
        var date = dob;
        var datearray = date.split("/");
        var newdate = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
        var ageDifMs = Date.now() - new Date(newdate);
        var ageDate = new Date(ageDifMs);
        if(forWhome==0){
            $scope.age = Math.abs(ageDate.getUTCFullYear() - 1970);
            if($scope.age<16){
                $scope.setFormInvalid();
            } else {
                $scope.setFormValid();
            }
        } else {
            $scope.child.childAge = Math.abs(ageDate.getUTCFullYear() - 1970);
            if($scope.child.childAge < 16 || $scope.age < $scope.child.childAge || $scope.age < 18) {
              $scope.setFormInvalid();  
            } else {
                $scope.setFormValid();   
            }
        }
    }
    $scope.setFormInvalid = function(){
        $timeout(function(){      
            $scope.sphereForm.$setValidity("age", false);
        },500);
    }
    $scope.setFormValid = function(){
        $timeout(function(){      
            $scope.sphereForm.$setValidity("age", true);
        },500);
    }
    $scope.Childreportcard = function(){
        $('#reportcard').click();
    }
    $scope.reportCard = {};
    $scope.reportCard.reportCardSelected = false;
    $(document).on('change', '#reportcard', function(event) {
        if(this.value.length) {
            var filename = this.value.split('\\');
            filename = filename.pop();
            $('#reportcardlabel').html(filename);
            $scope.reportCard.reportCardSelected = true;
            $scope.$apply();
        }
        else {
            $scope.reportCard.reportCardSelected = false;
            $('#reportcardlabel').html('Upload report card');
        }
        
    });

    $scope.Changerole = function(role){
        $scope.reportCard.reportCardSelected = false;
        if(role==0)
            if($scope.age<16)
                $scope.sphereForm.dob.$invalid = true;
        if(role==1)
            if($scope.age<18)
                $scope.sphereForm.dob.$invalid = true;
    }

    $scope.registerUser = function(){
        if($scope.sphereForm.$valid  && $scope.reportCard.reportCardSelected == true) {
            showLoader('.signup_btn');
            $scope.country = JSON.parse($scope.country_id);
            $scope.country = $scope.country['id'];
            var signupInfo = new FormData($("#sphereForm")[0]);
            signupInfo.append('country_id',$scope.country);
            spherefactory.Spheresignup(signupInfo).then(function(response){
                hideLoader('.signup_btn',Cookies.get('language')=='en'?'SIGN UP':'注册');
                if(response.data.status==1){
                    location.replace('sphere');
                } else {
                    simpleAlert('error', '', response.data.message);
                }
            });
        }    
    }
}]);