
app = angular.module('sixcloudPrivacyPolicyApp', ['ngRoute','pascalprecht.translate']);
/*var myApp = angular.module('sixcloudAboutApp', []);*/

// if (Cookies.get('language') == 'undefined' || Cookies.get('language') == '' || Cookies.get('language') == undefined) Cookies.set('language', 'en');
app.config(function($routeProvider, $translateProvider) {
    $translateProvider.translations('en', {
    	lbl_lang_chng:'中文',
        lbl_sixclouds:'SixClouds',
    	lbl_the_company:'The Company',
    	lbl_mission:'Mission',
    	lbl_vision:'Vision',
    	lbl_core_values:'Core Values',
    	lbl_about_us:'About Us',
    	lbl_company_text_1:'SixClouds specialises in delivering quality. educational programmes through a variety of bespoke multi-media platforms. SixClouds combines practical and fun educational content with the ease of technology enabled access to bring knowledge to the masses.',
    	lbl_company_text_2:'SixClouds was founded with the vision to make knowledge and learning accessible and affordable to everyone. When people improve in their personal capacity through knowledge gaining and experience, the community in which they reside in and contributes to, will also improve and become better.',
    	lbl_mission_text:'To deliver quality knowledge-based programmes through a variety of bespokemulti-mediaplatforms, by combining practical and fun knowledge-based content with the ease of technology enabled access to bring knowledge to the masses.',
    	lbl_vision_text:'Knowledge and learning should be accessible and affordable to everyone. When people improve in their intellectual capacity through knowledge gainingand experience, the community in which they reside in and contributes to, will also improve and become better.',
    	lbl_confidence_building:'Confidence Building',
    	lbl_confidence_building_text:'Facilitate the intellectual growth of learners to make confident strides forward, in their education, career and life.',
    	lbl_quality_affordability:'Quality and Affordability',
    	lbl_quality_affordability_text:'Build and curate high quality knowledge-based content that is affordable and accessible to everyone.',
    	lbl_constant_positive_evolution:'Constant Positive Evolution',
    	lbl_constant_positive_evolution_text:'Consistently evolve by harnessing and utilising technologies and information to bridge the gaps between knowledge and learning.',
    	lbl_trust:'Trust',
    	lbl_trust_text:'To be the trusted source of knowledge for learners and knowledge partners.',
    	lbl_tansparency:'Transparency',
    	lbl_tansparency_text:'To build sincere, fair and honest relationships with learners and knowledge partners.',
    	lbl_social_responsibility:'Social Responsibility',
    	lbl_social_responsibility_text:'A commitment to behave ethically and contribute to economic development whilst creating a positive impact on society and the environment at large.',
    	lbl_ignite:'Ignite',
    	lbl_sixteen:'Sixteen',
        lbl_discover:'Discover',
        lbl_proof_reading:'Proof Reading',
        lbl_quick_links:'Quick Links',
        lbl_become_seller:'Become a Seller',
        lbl_become_buyer:'Become a Buyer',
        lbl_customer_support:'Customer Support',
        lbl_terms_service:'Terms of Service',
        lbl_privacy_policy:'Privacy Policy',
        lbl_all_rights_reserved:'All rights reserved',
        lbl_this_policy_sets: 'This policy sets out the basis on which SixClouds Pte Ltd of Singapore (together with our subsidiaries, our holding company, subsidiaries of our holding company from time to time, collectively called “SIXCLOUDS” or “we”) collects personal data from you and how we process such data.',
        lbl_by_visiting_our_website1:'You accept and consent to the practices set out below when you;',
        // lbl_by_visiting_our_website_url:'www.sixclouds.net',
        // lbl_by_visiting_our_website2:'(the "Site") or using SIXCLOUDS or any other applications or software we provide from time to time (collectively our “App”), you accept and consent to the practices set out below.',
        lbl_by_visiting_our_website_a1:'visit any website belonging or associated with SixClouds (the “Site”) where "Belonging to SixClouds" means any website or document in the control of SixClouds and “Associated with SixClouds" means any website or document belonging to any third party company, used by SixClouds for the provision of it’s services;',
        // lbl_by_visiting_our_website_url:'www.sixclouds.net',
        // lbl_by_visiting_our_website_a2:'(the “Site”);',
        lbl_by_visiting_our_website_b:'use our applications (our “App”);',
        lbl_by_visiting_our_website_c:'use any third party applications, software and services we provide;',
        lbl_by_visiting_our_website_d:'participate in our offline activities, programs and services.',
        lbl_by_visiting_our_website_e:'Hereafter collectively known as our “Services”.',

        lbl_1 :'1 Collection of information',
        lbl_1_1 :'For the purposes outlined in Clause 2, we may collect and process the following information about you:',
        lbl_1_1_a:'Information you give us - information that you provide us (which may include your name, address, email address, telephone number, credit card information and other personal description) by filling in forms when using our Services, or by corresponding with us (by phone, email or otherwise), for example:',
        lbl_1_1_a_1:'when you register for an account with us on our Site and in our App;',
        lbl_1_1_a_2:'when you report any problem to us;',
        lbl_1_1_a_3:'when you use certain features on our Services;',
        lbl_1_1_a_4:'when you request any support from us; or',
        lbl_1_1_a_5:'when you complete any survey, questionnaire or proficiency tests we send you;',
        lbl_1_1_a_6:'when you submit a review on our products and services.',
        lbl_1_1_b:'Information we collect about you - information automatically collected when you visit our Site or use our App, for example:',
        lbl_1_1_b_1:'technical information, including the Internet protocol (IP) address used to connect your computer to the Internet and your log-in information, browser type and version, time zone setting, browser plug-in types and versions, operating system and platform;',
        lbl_1_1_b_2:'details of any transactions, purchases and payments you made on our Site and in our App; and',
        lbl_1_1_b_3:'information about your visit, including the full Uniform Resource Locators (URLs), clickstream to, through and from our site (including date and time), products you viewed or searched for, page response times, download errors, length of visits to certain pages, page interaction information (such as scrolling, clicks, and mouse-overs), methods used to browse away from the page, and any phone number used to call our customer service number; and',
        lbl_1_1_c:'Information we receive from third parties - We work with third parties and we may receive information about you from them, for example, business partners, sub-contractors in technical, payment and delivery services, advertising networks, analytics providers, search information providers, or credit reference agencies. We will notify you when we receive information about you from them and the purposes for which we intend to use that information.',
        lbl_1_2:'We only retain personal data for so long as it is necessary. Data may be archived as long as the purpose for which the data was used still exists.',

        lbl_2:'2 Uses made of the information',
        lbl_2_1:'We use information held about you for the following purposes:',
        lbl_2_1_a:'providing, improving and developing our services;',
        lbl_2_1_b:'researching, designing and launching new features or products;',
        lbl_2_1_c:'presenting content and information in our Site and our App in the most effective manner for you and for the device you use;',
        lbl_2_1_d:'providing you with alerts, updates, materials or information about our services or other types of information that you requested or signed up to;',
        lbl_2_1_e:'collecting overdue amounts;',
        lbl_2_1_f:'in any part of the world;',
        lbl_2_1_g:'responding or taking part in legal proceedings, including seeking professional advice;',
        lbl_2_1_h:'for direct marketing purposes (please see further details in Clause 2.2 below);',
        lbl_2_1_i:'communicating with you and responding to your questions or requests;',
        lbl_2_1_j:'anonymised data and statistics for product development and/or general marketing purposes; and',
        lbl_2_1_k:'purposes directly related or incidental to the above.',
        lbl_2_2:'We intend to use your personal data in direct marketing (i.e. offering or advertising products or services by communicating the relevant information directly to you). We require your consent specifically for this purpose and you may opt out any time. For the purpose of this clause:',
        lbl_2_2_a:'the personal data that may be used in direct marketing are those that you provide to us or we collect from you under Clause 1.1 above;',
        lbl_2_2_b:'the type of services or products that may be offered or advertised will be our products and services;',
        lbl_2_2_c:'the relevant information may be communicated to you via, but not limited to phone, SMS, email, in-app messages and/or, social media platforms;',
        lbl_2_2_d:'you may opt out any time by email to SixClouds Customer Support. We will cease to send you marketing information without charge.',

        lbl_3:'3 Disclosure of your information ',
        lbl_3_text:'We will keep your personal data we hold confidential but you agree we may provide information to:',
        lbl_3_a:'any member of our group, which means our subsidiaries, our ultimate holding company and its subsidiaries, as defined in Section 5 of the Companies Act (Chapter 50);',
        lbl_3_b:'personnel, agents, advisers, auditors, contractors, financial institutions, and service providers in connection with our operations or services (for example staff engaged in the fulfilment of your order, the processing of your payment and the provision of support services);',
        lbl_3_c:'our overseas offices, affiliates, business partners and counterparts (on a need-to-know basis only);',
        lbl_3_d:'persons under a duty of confidentiality to us;',
        lbl_3_e:'persons to whom we are required to make disclosure under applicable laws and regulations in any part of the world; or',
        lbl_3_f:'actual or proposed transferees of our operations (or a substantial part thereof) in any part of the world.',

        lbl_4:'4 Cookies',
        lbl_4_1:'Our Site uses cookies to distinguish you from other users of the Site. This helps us to provide you with a good experience when you browse our Site and also allows us to improve our Site.',
        lbl_4_2:'A cookie is a small file of letters and numbers that we store on your browser or the hard drive of your computer if you agree to the use of cookies. Cookies contain information that is transferred to your computer\'s hard drive.',
        lbl_4_3:'We use persistent cookies and session cookies. A persistent cookie stays in your browser and will be read by us when you return to our Site or a partner site that uses our services. Session cookies only last for as long as the session (usually the current visit to a website or a browser session).',
        lbl_4_4:'We use the following cookies: ',
        lbl_4_4_a:'Strictly necessary cookies – These are cookies that are required for the operation of our Site. They include, for example, cookies that enable you to log into secure areas of our website, use a shopping cart or make use of e-billing services.',
        lbl_4_4_b:'Analytical/performance cookies – They allow us to recognise and count the number of visitors and to see how visitors move around our Site when they are using it. This helps us to improve the way our Site works, for example, by ensuring that users are finding what they are looking for easily. ',
        lbl_4_4_c:'Functionality cookies – These are used to recognise you when you return to our Site. This enables us to personalise our content for you, greet you by name and remember your preferences (for example, your choice of language or region).',
        lbl_4_4_d:'Targeting cookies – These cookies record your visit to our Site, the pages you have visited and the links you have followed. We will use this information to make our Site and the information displayed on it more relevant to your interests.',
        lbl_4_5:'You can block cookies by activating the setting on your browser that allows you to refuse the setting of all or some cookies. However, if you do so, you may not be able to access all or parts of our Site.',
        lbl_4_6:'We may use third-party web services on our Site. The service providers that administer these services use technologies such as cookies (which are likely to be analytical/performance cookies or targeting cookies), web server logs and web beacons to help us analyse how visitors use our Site and make the information displayed on it more relevant to your interests. The information collected through these means (including IP addresses) is disclosed to these service providers. These analytics services may use the data collected to contextualise and personalise the marketing materials of their own advertising network.',

        lbl_5:'5 Third-party sites',
        lbl_5_text:'Our Site, our App or our communication with you may from time to time contain links to third-party websites over which we have no control. If you follow a link to any of these websites, please note that they have their own practices and policies. We encourage you to read the privacy policies or statements of these websites understand your rights. We accept no responsibility or liability for any practices of third-party websites.',

        lbl_6:'6 Security ',
        lbl_6_1:'All information you provide to us is stored on our secure servers.',
        lbl_6_2:'Any payment transactions will be encrypted using TLS/SSL technology.',
        lbl_6_3:'Where we have given you (or where you have chosen) a password that enables you to access certain parts of the Site, you are responsible for keeping this password confidential. We ask you not to share a password with anyone.',
        lbl_6_4:'We restrict access to personal information to our employees, service providers and contractors on a strictly need-to-know basis and ensure that those persons are subject to contractual confidentiality obligations.',
        lbl_6_5:'We review our information collection, storage and processing practices from time to time to guard against unauthorised access, processing or use.',
        lbl_6_6:'Please note, however, the transmission of information via the Internet is not completely secure. Although we will do our best to protect your personal data, we cannot guarantee the security of your data transmitted to our Site; any transmission is at your own risk.',

        lbl_7:'7 Data transfer',
        lbl_7_1:'The data that we collect from you may be transferred to, and stored at, a destination outside Singapore. It may also be processed by persons referred to in Clause 3 above who operate outside Singapore. ',
        lbl_7_2:'By submitting your personal data you agree to this transfer, storing or processing of data outside Singapore. We will take all steps reasonably necessary to ensure that your data is treated securely in accordance with this privacy policy.',

        lbl_8:'8 Your rights',
        lbl_8_1:'You have the right to:',
        lbl_8_1_a:'check whether we hold personal data about you;',
        lbl_8_1_b:'access any personal data we hold about you; and',
        lbl_8_1_c:'require us to correct any inaccuracy or error in any personal data we hold about you.',
        lbl_8_2:'Any request under Clause 8.1 may be subject to a small administrative fee to meet our cost in processing your request.',

        lbl_9:'9 Changes to our privacy policy',
        lbl_9_text:'We may amend this policy from time to time by posting the updated policy on our Site. By continuing to use our Site and our App after the changes come into effect means that you agree to be bound by the revised policy.',

        lbl_10:'10 Languages',
        lbl_10_text:'In the event of any divergence of interpretation, the English version shall prevail.',

        lbl_11:          '11 Contact us',
        lbl_11_text:     'If you have any questions, comments or requests regarding personal data, please address them to our',
        lbl_updated_at:  'Updated: May 2020',
        lbl_the_company: 'The Company',
        lbl_core_values: 'Core Values',
        lbl_our_team:    'Our Team',
        lbl_dot:         '.',
    }).translations('chi', {
    	
    	lbl_lang_chng:'English',   
        lbl_sixclouds:'六云',
    	lbl_the_company:'六云简介',
    	lbl_mission:'任务',
    	lbl_vision:'愿景',
		lbl_core_values:'核心理念',    	
		lbl_about_us:'关于我们',
		lbl_company_text_1:'SixClouds专注于通过各种定制的多媒体平台提供高质量的教育课程。 SixClouds利用科技，将实用和有趣的教育内容带给为大众。',
		lbl_company_text_2:'SixClouds成立的愿景是以合理的价格里，让每个人都能轻易的学习和增长知识。',
		lbl_mission_text:'通过各种定制的多媒体平台提供高质量的知识类节目，通过技术奖实用和有趣的知识内容带给大众。',
		lbl_vision_text:'知识和学习应该是每个人都可以得到并且负担得起的。当人们通过获取知识和经验来提高他们自己本身的智力时，当人们获取丰富的经验和知识时，他们能够更好的回馈社会。',
		lbl_confidence_building:'建立信心',
		lbl_confidence_building_text:'促进学习者的智力成长，让他们在学习、事业和生活中迈出自信的步伐。',
		lbl_quality_affordability:'高质量和可承受性',
		lbl_quality_affordability_text:'构建和管理高质量的知识内容，价格合理，所有人都可承受。',
		lbl_constant_positive_evolution:'正向进化 ',
		lbl_constant_positive_evolution_text:'利用科技和信息来弥合知识和学习之间的差距，持续进步。',
		lbl_trust:'信任',
		lbl_trust_text:'成为学习者和合作伙伴的可靠的信息来源。',
		lbl_tansparency:'透明',
		lbl_tansparency_text:'与学习者和知识合作伙伴建立真诚、公正和诚实的关系。',
		lbl_social_responsibility:'社会责任',
		lbl_social_responsibility_text:'致力于有道德的行为和为经济发展做贡献，同时在更大的层面上对社会和环境产生积极影响。',
		lbl_ignite:'点燃',
		lbl_sixteen:'十六',
        lbl_discover:'发现',
        lbl_proof_reading:'证明阅读',
        lbl_quick_links:'快速链接',
        lbl_become_seller:'成为卖家',
        lbl_become_buyer:'成为买家',
        lbl_customer_support:'客服中心',
        lbl_terms_service:'服务条款',
        lbl_privacy_policy:'隐私政策',
        lbl_all_rights_reserved:'版权所有',
        lbl_this_policy_sets:'此政策规定了新加坡六云私人有限公司（连同我们的子公司，我们的控股公司，我们控股公司不时的子公司，统称为“六云”或“我们”）收集您的个人资料的基础，以及我们如何处理这些资料。',
        lbl_by_visiting_our_website1:'您接受并同意以下规定的做法;',
        // lbl_by_visiting_our_website_url:'www.sixclouds.net',
        // lbl_by_visiting_our_website2:'（“本网站”）的相关域名或使用六云或我们不时提供的任何其他应用程序或软件（统称为我们的“应用程序”），即表示您接受 并同意下面列出的做法。',
        lbl_by_visiting_our_website_a1:'访问属于六云或与相关的任何网站（“本网站”），其中“属于六云”是指由六云控制的任何网站或文件，“与六云关联”是指属于任何第三方的任何网站或文件，由六云用于提供其服务；',
        // lbl_by_visiting_our_website_url:'www.sixclouds.net',
        // lbl_by_visiting_our_website_a2:'（“本网站”）；',
        lbl_by_visiting_our_website_b:'使用我们的（“应用程序”）；',
        lbl_by_visiting_our_website_c:'使用我们提供的任何其他第三方应用程序、软件和服务；',
        lbl_by_visiting_our_website_d:'参与我们的线下活动、计划或服务。',
        lbl_by_visiting_our_website_e:'以下统称为“服务”。',

        lbl_1 :'一 信息收集',
        lbl_1_1 :'为了第2条款中所述的目的，我们可以收集和处理以下关于您的信息：',
        lbl_1_1_a:'您给我们的信息 – 您提供的信息（包括您的姓名，地址，电子邮件地址，电话号码，信用卡信息和其他个人描述）通过在本网站或我们的应用程序和服务填写表格，或与我们对应（通过电话，电子邮件或其他方式），例如：',
        lbl_1_1_a_1:'当您在我们的网站和我们的应用程序中注册账号时；',
        lbl_1_1_a_2:'当您向我们举报任何问题时；',
        lbl_1_1_a_3:'当您在我们的服务中使用某些功能时；',
        lbl_1_1_a_4:'当您向我们请求任何支援时；',
        lbl_1_1_a_5:'当您完成我们向您发送的任何调查，问卷或测验时；',
        lbl_1_1_a_6:'当您对我们产品和服务做出反馈。',
        lbl_1_1_b:'我们收集关于您的信息 - 当您访问我们的网站或使用我们的应用程序时自动收集的信息，例如： ',
        lbl_1_1_b_1:'技术信息，包括互联网协议（IP）地址，用于连接您的电脑到互联网和您的登录信息，浏览器类型和版本，时区设置，浏览器插件类型和版本，操作系统和平台；',
        lbl_1_1_b_2:'您在我们的网站和我们的应用程序中进行的任何交易，购买和付款的详细信息; 和',
        lbl_1_1_b_3:'关于您访问的信息，包括完整的网址（URL），通过和来自我们网站的点击流量（包括日期和时间），您查看或搜索的产品，页面响应时间，下载错误，某些页面的访问时长 ，页面交互信息（如滚动，点击和鼠标悬停），用于浏览页面的方法以及用于拨打我们的客服中心号码的任何电话号码; 和 ',
        lbl_1_1_c:'我们从第三方获得的信息 - 我们与第三方合作而我们可能会从他们那里收到有关您的信息，例如商业伙伴，技术，支付和交付服务分包商，广告网络，大数据提供商，搜索信息提供商或信用参考机构。 当我们从他们收到有关您的信息以及我们打算使用该信息的目的时，我们会通知您。',
        lbl_1_2:'根据时间的必要，我们将保留个人资料。只要资料使用的目的仍然存在，资料就可以存档。',

        lbl_2:'二 信息的利用 ',
        lbl_2_1:'我们将有关您的信息用于以下目的： ',
        lbl_2_1_a:'提供，改进和发展我们的服务;',
        lbl_2_1_b:'研究，设计和推出新功能或新产品;',
        lbl_2_1_c:'以最有效的方式为您和您使用的设备在我们的网站和我们的应用程序中展示内容和信息;',
        lbl_2_1_d:'向您提供有关我们服务的警报﹑更新﹑素料或信息，或您所要求或申请的其他信息。',
        lbl_2_1_e:'收取逾期金额;',
        lbl_2_1_f:'在世界的任何地方;',
        lbl_2_1_g:'回应或参与法律程序，包括寻求专业意见;',
        lbl_2_1_h:'用于直接营销用途（请参阅下文第2.2条的进一步详情）;',
        lbl_2_1_i:'与您沟通并回答您的问题或要求;',
        lbl_2_1_j:'用于产品开发和/或一般营销目的的匿名数据和统计数据；和',
        lbl_2_1_k:'与上述直接相关或附带的目的。',
        lbl_2_2:'我们将会在营销中使用您的个人资料（过直接向您传达相关信息来提供或宣传产品或服务）。我们为此目的需要您的同意，您可以随时选择退出。本条而言：',
        lbl_2_2_a:'可用于直接营销的个人资料是您根据上述条款1.1向我们提供或我们收集的;',
        lbl_2_2_b:'可提供或宣传的服务或产品类型将是我们的产品和服务;',
        lbl_2_2_c:'相关信息可以通过但不限于电话，短信，电子邮件，应用内消息和/或社交媒体平台传达给您；',
        lbl_2_2_d:'您可以随时通过电子邮件到六云客服中心申请退出。 我们将不收费地停止向您发送营销信息。',

        lbl_3:'三 披露您的信息 ',
        lbl_3_text:'我们会将您的个人资料保密，但您同意我们可能会提供您的个人资料给：',
        lbl_3_a:'我们集团的任何成员，指我们的子公司，我们的主控股公司及其子公司， 根据“公司法”（第50章）第5节所定义；',
        lbl_3_b:'与我们的运营或服务相关的人员，代理人，顾问，审计人员，承包商，金融机构和服务提供商（例如从事履行您的订单，处理您的付款和提供支持服务的人员）;',
        lbl_3_c:'我们的海外办事处，分支机构，业务伙伴和对应方（仅在需要知道的基础上）;',
        lbl_3_d:'对我们有保密义务的人;',
        lbl_3_e:'我们被要求在世界任何地区的适用法律和法规下披露的人员; 或',
        lbl_3_f:'我们在世界任何地方业务实际或提议的受让人（或其实质部分）。',

        lbl_4:'四 小型文字档案 （Cookies）',
        lbl_4_1:'我们的网站使用cookies来区分您与本网站的其他用户。 这有助于我们在浏览我们的网站时为您提供良好的体验，并使我们能够改进我们的网站。',
        lbl_4_2:'如果您同意使用cookies，cookies就是我们在您的浏览器或电脑的硬盘上存储的一小段字母和   数字。 Cookies包含传输到您的电脑硬盘里的信息。',
        lbl_4_3:'我们使用持久性cookies和会话cookies。 持久性cookies保留在您的浏览器中，当您返回我们的网站或使用我们服务的合作伙伴网站时，我们会将其读取。 会话cookies只会根据会话的持续（通常是当访问某网站或在浏览器会话）。',
        lbl_4_4:'我们使用以下cookies： ',
        lbl_4_4_a:'完全必要的cookies - 这些是我们网站操作所需的cookies。它们包括，例如，使您能够登录到我们的网站的安全区域，使用购物车或使用电子计费服务的cookies。',
        lbl_4_4_b:'分析性/性能cookies - 它们使我们能够识别和统计访问者数量，并了解访问者如何在我们的网站上浏览。 这有助于我们改进网站的工作方式，例如确保用户能够轻松找到他们想要的内容。',
        lbl_4_4_c:'功能性cookies - 这些用于在您返回我们的网站时能够识别您。 这使我们能够为您个性化我们的内容，以您的名字称呼您，并记住您的偏好（例如，您选择的语言或地区）。',
        lbl_4_4_d:'定位性cookies - 这些cookies会记录您对我们网站的访问，您访问过的页面以及您关注的链接。 我们将使用这些信息使我们的网站及其上显示的信息与您的兴趣更相关。',
        lbl_4_5:'您可以通过激活浏览器上的设置来阻止cookies，从而拒绝设置所有或部分的cookies。 但是，如果您这样做，您可能无法访问我们网站的全部或部分内容。',
        lbl_4_6:'我们可以在我们的网站上使用第三方网络服务。 管理这些服务的服务提供商使用技术如cookies（可能是分析性/性能cookies或定位性cookies），服务器日志和网络信标等技术来帮助我们分析访问者如何使用我们的网站并使其上显示的信息多与您的兴趣相关。 通过这些方式收集的信息（包括IP地址）将披露给这些服务提供商。 这些分析服务可以使用所收集的资料来对其自己广告网络的营销素料进行情境化和个性化。',

        lbl_5:'五 第三方网站',
        lbl_5_text:'我们的网站，我们的应用程序或我们与您的沟通可能会不时包含我们无法控制的第三方网站链接。 如果您点击这些网站的链接，请注意他们有自己的惯例和政策。 我们鼓励您细读这些网站的隐私政策或声明来理解您的权利。 我们对任何第三方网站的惯例不承担任何责任或义务。',

        lbl_6:'六 安全政策 ',
        lbl_6_1:'您提供给我们的所有信息都存储在我们的安全服务器上。',
        lbl_6_2:'任何支付交易将使用TLS / SSL技术进行加密。',
        lbl_6_3:'我们向您（或您所选择的）提供了一个密码，以便您访问本网站的某些部分，您有责任对此密码保密。 我们要求您不要与任何人分享密码。',
        lbl_6_4:'我们限制我们的员工，服务提供商和承包商，严格保证仅在需要知情的情况下访问个人信息，并确保这些人员承担合同保密义务。',
        lbl_6_5:'我们定期审查我们的信息收集、存储和处理实践，以防止未经授权的访问、处理或使用',
        lbl_6_6:'请注意，通过互联网传输信息并非完全安全。 虽然我们会尽全力保护您的个人资料，但我们无法保证传送至我们网站您的资料的安全性; 任何传输都需要您自担风险。',

        lbl_7:'七 资料传输',
        lbl_7_1:'我们向您收集的资料可能会转移并存储到新加坡以外的目的地。 它也可能由上述第3条提到的在新加坡境外经营的人员处理。',
        lbl_7_2:'通过提交您的个人资料，您同意在新加坡境外进行此类转移，存储或处理资料。 我们将采取合理必要的一切措施，确保您的资料按照本隐私政策进行安全处理。',

        lbl_8:'八 您的权力',
        lbl_8_1:'您有权：',
        lbl_8_1_a:'检查我们是否持有关于您的个人资料;',
        lbl_8_1_b:'访问我们持有关于您的任何个人资料; 和',
        lbl_8_1_c:'要求我们纠正我们持有关于您的任何个人资料中的任何不准确或错误资料。',
        lbl_8_2:'根据8.1条款，以补贴我们处理您的要求的费用，我们可能收取小数的手续费用。',

        lbl_9:'九 隐私政策修改',
        lbl_9_text:'我们不时会通过我们的网站发布更新的政策以修改这份政策。在变更生效后继续使用我们的网站和我们的应用程序意味着您同意受修改政策的约束。',

        lbl_10:'十 语言',
        lbl_10_text:'倘若本政策的说明有分歧，就以英文版本为准。',

        lbl_11:          '十一 联络方式',
        lbl_11_text:     '如果您对个人资料有任何问题，意见或要求，请将其发送至我们的',
        lbl_updated_at:  '更新时间: 2020-05-20',
        lbl_the_company: '六云简介',
        lbl_core_values: '核心理念',     
        lbl_our_team:    '我们的团队',
        lbl_dot:         '。',
    }).translations('ru', {
        lbl_privacy_policy: 'ПОЛИТИКА КОНФИДЕНЦИАЛЬНОСТИ',
        lbl_this_policy_sets: 'Эта политика устанавливает базовые понятия, для SixClouds Pte Ltd, Сингапур, которая (вместе с нашими дочерними компаниями, холдинговой компанией, дочерними компаниями нашей холдинговой компании время от времени под общим названием “SIXCLOUDS” или под словом “Мы”) собирает личные данные о вас и обрабатывает такие данные.',
        lbl_by_visiting_our_website1: 'Вы принимаете и соглашаетесь с практикой, изложенной ниже, когда вы;',
        // lbl_by_visiting_our_website_url: 'www.sixclouds.net',
        // lbl_by_visiting_our_website2: '(далее "Сайт") или используя SIXCLOUDS или любые другие приложения или программное обеспечение, которое мы предоставляем время от времени (все вместе наше “Приложение”), вы принимаете и соглашаетесь с правилами, изложенными ниже.',
        lbl_by_visiting_our_website_a1:'Посещение любого веб-сайта, принадлежащий или связанный с SixClouds (“сайт”), где "принадлежность к SixClouds" означает любой веб-сайт или документ в управление SixClouds и “связанные с SixClouds" означает любой веб-сайт или документ, относящихся к каким-либо сторонним компаниям, используемые SixClouds за оказание этой услуги;',  
        // lbl_by_visiting_our_website_url:'www.sixclouds.net', 
        // lbl_by_visiting_our_website_a2:'(“Сайт”);',
        lbl_by_visiting_our_website_b:'Используйте наше приложение (наше “Приложение”);',
        lbl_by_visiting_our_website_c:'Использовать любые другие сторонние приложения, программное обеспечение и услуги, которые мы предоставляем;',
        lbl_by_visiting_our_website_d:'Участвовать в оффлайн мероприятиях, программах или услугах.',
        lbl_by_visiting_our_website_e:'Далее вместе именуемым наш “Сервис”.',

        lbl_1: '1 Сбор информации',
        lbl_1_1: 'Для целей, указанных в пункте 2, мы можем собирать и обрабатывать следующую информацию о вас:',
        lbl_1_1_a: 'Информация, которую вы нам предоставляете – это информация, которая может включать ваше имя, адрес, адрес электронной почты, номер телефона, информацию о кредитной карте и другую личную информацию. Вы предоставляете подобную информацию, заполняя формы при использовании наших услуг на нашем Сайте или в нашем Приложении или переписываясь с нами (по телефону, электронной почте или иным образом), например:',
        lbl_1_1_a_1: 'когда вы регистрируете аккаунт на нашем Сайте и в нашем Приложении;',
        lbl_1_1_a_2: 'когда вы сообщаете нам о какой-либо проблеме;',
        lbl_1_1_a_3: 'когда вы используете определенные функции наших услуг.',
        lbl_1_1_a_4: 'когда вы запрашиваете у нас поддержку;',
        lbl_1_1_a_5: 'при заполнении любого опроса или анкеты, которые мы вам отправим.',
        lbl_1_1_a_6: 'когда вы отправляете отзыв о наших продуктах и услугах.',
        
        lbl_1_1_b: 'Когда вы заполняете какие-либо опросные листы или тесты которые мы вам отправляем. Информация, которую мы собираем о вас при посещении нашего Сайта или использовании Приложения собирается автоматически, например:',
        lbl_1_1_b_1: 'техническая информация, включая адрес Интернет-протокола (IP), используемый для подключения вашего компьютера к Интернету и информацию о вашем входе в систему, тип и версию браузера, настройку часового пояса, типы и версии подключаемых модулей браузера, операционную систему и платформу;',
        lbl_1_1_b_2: 'детали любых транзакций, покупок и платежей, которые вы совершили на нашем Сайте или через Приложение; и',
        lbl_1_1_b_3: 'информация о вашем посещении, включающая полные унифицированные локаторы ресурсов (URL-адреса), переходы на наш сайт, через и с него (с указанием даты и времени), материалы, который вы просматривали или искали, время отклика страниц, ошибки загрузок, продолжительность посещений определенных страниц, информация о взаимодействии страниц (такие как прокрутка, щелчки и наведение мыши), методы, используемые для перехода со страницы, и любой номер телефона, используемый для вызова нашей службы поддержки клиентов.',
        
        lbl_1_1_c: 'Информация, которую мы получаем от третьих лиц – Мы работаем с третьими лицами и можем получать от них информацию о вас, например, от деловых партнеров, субподрядчиков в сфере технических и платежных услуг, услуг доставки, рекламных сетей, поставщиков аналитических услуг, поставщиков поисковой информации или кредитных агентств. Мы уведомим вас, когда получим от них информацию о вас и о целях, для которых мы намерены использовать эту информацию.',
        

        lbl_1_2: 'Мы сохраняем персональные данные так долго, как это необходимо. Данные могут архивироваться до тех пор, пока все еще существует цель, для которой они были использованы.',
        
        lbl_2: '2 Использование информации',
        lbl_2_1: 'Мы используем информацию о вас для следующих целей:',
        lbl_2_1_a: 'предоставление, улучшение и развитие наших услуг;',
        lbl_2_1_b: 'исследование, разработка и запуск новых функций или продукции;',
        lbl_2_1_c: 'представление для вас контента и информации на нашем Сайте и в Приложении наиболее эффективным образом и для устройства, которое вы используете;',
        lbl_2_1_d: 'предоставление предупреждений, обновлений, материалов или информации о наших услугах или других видах информации, которую вы запросили или зарегистрировали;',
        lbl_2_1_e: 'сбор просроченной задолженности;',
        lbl_2_1_f: 'отслеживание местоположения;',
        lbl_2_1_g: 'реагирование или участие в судебных разбирательствах, включая обращение за профессиональной консультацией;',
        lbl_2_1_h: 'в целях прямого маркетинга (подробности см. ниже, в пункте 2.2);',
        lbl_2_1_i: 'для общения с вами и ответов на ваши вопросы или запросы; и',
        lbl_2_1_j: 'Анонимизированные данные и статистические данные для разработки продуктов и/или общих маркетинговых задач.',
        
        lbl_2_2: 'Мы намерены использовать ваши персональные данные в прямом маркетинг(т.e. предлагать или рекламировать продукты или услуги, сообщая соответствующую информацию непосредственно вам). Мы просим вашего согласия специально для этой цели, вы можете отказаться от этого в любое время. Для целей обозначенных в следующих пунктах:',
        lbl_2_2_a: 'персональные данные, которые могут использоваться в прямом маркетинге, это те данные, которые вы предоставляете нам или мы получаем от вас в соответствии с пунктом 1.1, описанным выше;',
        lbl_2_2_b: 'вид услуг или продукции, которые могут быть предложены или рекламироваться, будут нашей продукцией и услугами;',
        lbl_2_2_c: 'соответствующая информация может быть сообщена ​​​вам спомощью,но не ограничиваясь этим телефона, SMS,электронной почты, сообщений в приложении и / или платформ социальныхсетей;',
        lbl_2_2_d: 'вы можете отказаться в любое время, написав в службу поддержки SixClouds. Мы прекратим присылать вам маркетинговую информацию и сделаем это абсолютно бесплатно.',
        

        lbl_3: '3 Раскрытие вашей информации',
        lbl_3_text: 'Мы храним ваши личные данные в тайне, но вы соглашаетесь с тем, что иногда мы можем раскрывать информацию о вас:',
        lbl_3_a: 'любому члену нашей группы, это означает наши дочерние компании, основную холдинговую компанию и ее дочерние компании, как определено в разделе 5, Закона о компаниях (Глава 50);',
        lbl_3_b: 'персоналу, агентам, консультантам, аудиторам, подрядчикам, финансовым учреждениям и поставщикам услуг, в связи с нашими операциями или услугами (например, персоналу, занятому выполнением вашего заказа, обработкой вашего платежа или предоставлением услуг поддержки);',
        lbl_3_c: 'нашим зарубежным офисам, филиалам, деловым партнерам и контрпартнерам (только для ознакомления);',
        lbl_3_d: 'лицам, обязанным соблюдать конфиденциальность по отношению к нам;',
        lbl_3_e: 'лицам, которым мы обязаны раскрывать информацию в соответствии с действующими законами и правилами в любой части мира; или',
        lbl_3_f: 'фактическим или предполагаемым получателем наших операций (или их части) в любой точке мира.',

        lbl_4: '4 Cookies',
        lbl_4_1: 'Наш Сайт использует файлы cookies, чтобы отличать вас от других пользователей Сайта. Это помогает нам предоставить вам качественный сервис при просмотре нашего Сайта, а также улучшить наш Сайт.',
        lbl_4_2: 'Файл сookie – это небольшой файл из букв и цифр, который мы сохраняем в вашем браузере или на жестком диске вашего компьютера, если вы соглашаетесь с использованием файлов cookie. Cookies содержат информацию, которая передается на жесткий диск вашего компьютера.',
        lbl_4_3: 'Мы используем постоянные cookies и сессионные файлы cookies. Постоянный файл cookie остается в вашем браузере и будет прочитан нами, когда вы вернетесь на наш Сайт или сайт партнера, который использует наши услуги. Сессионные cookies хранятся до конца сессии (обычно текущее посещение сайта или сеанса браузера) и потом удаляются.',
        lbl_4_4: 'Мы используем следующие типы файлов cookie:',
        lbl_4_4_a: 'Строго обязательные файлы cookie – Эти файлы необходимы для работы нашего Сайта. Например, они включают в себя cookie, которые позволяют входить в безопасные зоны сайта, использовать корзину для покупок или воспользоваться услугами электронного биллинга.',
        lbl_4_4_b: 'Аналитические / производительные cookie – Они позволяют нам распознавать и подсчитывать количество посетителей и видеть, как посетители перемещаются по Cайту, когда они его используют. Это помогает нам улучшить работу Cайта. Например, гарантировать, что пользователи легко находят, то что они ищут.',
        lbl_4_4_c: 'Функциональные cookies – Они используются, чтобы опознавать вас, при возвращении на наш Сайт. Это позволяет персонализировать для вас содержание сайта, приветствовать вас по имени и запоминать ваши предпочтения (например, выбранный язык или регион).',
        lbl_4_4_d: 'Целевые cookies – Эти файлы cookie записывают ваши посещения Сайта, страницы, которые вы посетили и ссылки, по которым вы следовали. Мы будем использовать эту информацию, чтобы наш Сайт, и отображаемая на нем информация более соответствовала вашим интересам.',
        lbl_4_5: 'Вы можете заблокировать файлы cookie, активировав настройку в своем браузере, которая позволяет отказаться от настройки всех или некоторых файлов cookie. Однако, если вы сделаете это, то не сможете получить доступ ко всему нашему Сайту или к его части.',
        lbl_4_6: 'Мы можем использовать сторонние веб-сервисы на нашем Сайте. Поставщики услуг, которые администрируют эти услуги, используют такие технологии, как файлы cookies (которые, вероятно, будут аналитическими/производительными или целевыми), журналы веб-сервера и веб-маяки помогают нам анализировать, как посетители используют наш сайт, и сделать информацию, отображаемую на нем, более актуальной для ваших интересов. Информация, собранная с помощью этих средств (включая IP-адреса), раскрывается этим поставщикам услуг. Эти аналитические службы могут использовать собранные данные для контекстуализации и персонализации маркетинговых материалов своей собственной рекламной сети.',

        lbl_5: '5 Сторонние веб-сайты',
        lbl_5_text: 'Наш Сайт, наше Приложение или наше общение с вами может время от времени содержать ссылки на сторонние веб-сайты, над которыми мы не имеем никакого контроля. Если вы перейдете по ссылке на любой из этих веб-сайтов, обратите внимание, что они имеют свои собственные методы и правила. Чтобы понять ваши права, мы рекомендуем вам ознакомиться с политикой конфиденциальности или заявлениями этих веб-сайтов. Мы не несем никакой ответственности за любые действия сторонних веб-сайтов.',
        
        lbl_6: '6 Безопасность',
        lbl_6_1: 'Вся информация, которую вы предоставляете, хранится на наших защищенных серверах.',
        lbl_6_2: 'Любые платежные транзакции будут зашифрованы с использованием технологии TLS/SSL.',
        lbl_6_3: 'Если мы предоставили вам (или вы выбрали) пароль, который позволяет вам получить доступ к определенным частям Сайта, вы несете ответственность за сохранение конфиденциальности этого пароля. Мы просим вас никому его не сообщать.',
        lbl_6_4: 'Мы ограничиваем доступ к личной информации для наших сотрудников, поставщиков услуг и подрядчиков на основе строгой информации и гарантируем, что эти лица подпадают под договорные обязательства о конфиденциальности. Время от времени мы пересматриваем наши методы сбора, хранения и обработки информации для защиты от несанкционированного доступа, обработки или использования.',
        lbl_6_5: 'Однако обратите внимание, что передача информации через Интернет не является полностью безопасной. И хотя мы делаем все возможное для защиты ваших личных данных, мы не можем полностью гарантировать безопасность ваших данных, передаваемых на наш сайт; любая передача информации осуществляется на ваш собственный риск.',
        lbl_6_6: 'Please note, however, the transmission of information via the Internet is not completely secure. Although we will do our best to protect your personal data, we cannot guarantee the security of your data transmitted to our Site; any transmission is at your own risk.',

        lbl_7: '7 Передача данных',
        lbl_7_1: 'Данные, которые мы собираем от вас, могут передаваться и храниться за пределами Сингапура. Они могут также обрабатываться лицами, указанными выше в пункте 3, работающими за пределами Сингапура.',
        lbl_7_2: 'Предоставляя свои персональные данные, вы соглашаетесь на передачу, хранение или обработку данных за пределами Сингапура. Мы предпримем все разумно необходимые шаги для обеспечения безопасного обращения с вашими данными в соответствии с настоящей Политикой конфиденциальности.',

        lbl_8: '8 Ваши права',
        lbl_8_1: 'У вас есть право:',
        lbl_8_1_a: 'проверить, храним ли мы персональные данные о вас;',
        lbl_8_1_b: 'иметь доступ к любым вашим персональным данным; и',
        lbl_8_1_c: 'потребовать от нас исправить любые неточности или ошибки в любых ваших персональных данных.',
        lbl_8_2: 'Любой запрос в соответствии с пунктом 8.1 может облагаться небольшим административным сбором для покрытия наших расходов при обработке вашего запроса.',

        lbl_9: '9 Изменения в нашей политике конфиденциальности',
        lbl_9_text: 'Время от времени мы можем вносить изменения в эту политику, размещая обновленную информацию на нашем Сайте. Продолжение использования нашего Сайта и Приложения после вступления изменений в силу означает, что вы соглашаетесь соблюдать пересмотренную политику.',
        
        lbl_10: '10 Языки',
        lbl_10_text: 'В случае каких-либо расхождений в толковании, преимущественную силу имеет текст на английском языке.',
        
        lbl_11: '11 Свяжитесь с нами',
        lbl_11_text: 'Если у вас есть какие-либо вопросы, комментарии или пожелания относительно персональных данных, к вашим услугам наша',
        lbl_customer_support: 'Группа поддержки',
        lbl_dot: '。',
        lbl_updated_at: 'Обновлено: Май 2020 года',

        
        lbl_about_us: 'О Нас',
        lbl_the_company: 'Компания',
        lbl_all_rights_reserved: 'Все права защищены',
        lbl_quick_links: 'Быстрые ссылки',
        lbl_core_values: 'Фундаментальные ценности',
        lbl_our_team: 'Наша команда',
    });
    $translateProvider.preferredLanguage(Cookies.get('language'));
});
/* To change the interolate provider need to change it's originonal brackets.*/
app.config(['$interpolateProvider', function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
}]);
/* for changing title */
app.run(['$location', '$rootScope', '$http', '$timeout', function($location, $rootScope, $http, $timeout) {
    $rootScope.$on('$routeChangeSuccess', function(event, current, previous) {
        if (current.$$route != undefined) $rootScope.title = current.$$route.title;
        document.body.scrollTop = document.documentElement.scrollTop = 0;
    });
}]);
app.controller('PrivacyPolicyController', ['$scope', '$translate', '$rootScope', '$timeout', function ($scope, $translate, $rootScope, $timeout){
	console.log("PrivacyPolicy Controller");
	
	var ctrl = this;
    if (Cookies.get('language') == 'undefined' || Cookies.get('language') == '' || Cookies.get('language') == undefined) {
        $scope.url = window.location.host;
        $scope.url = $scope.url.split('.');
        $scope.url = $scope.url[2];
        $timeout(function() {
            if($scope.url==undefined){
                $timeout(function() {
                    $scope.changeLang('en');
                },500);
            }
            if($scope.url == 'net' && $scope.url!=undefined){
                $timeout(function() {
                    $scope.changeLang('en');
                },500);
            }
            if($scope.url == 'cn' && $scope.url!=undefined){
                $timeout(function() {
                    $scope.changeLang('chi');
                },500);
            }
        },1000);
    }
    $scope.changeLangs = Cookies.get('language');
    $scope.changeLang = function(lang) {
        if (lang == 'en') $scope.changeLangs = 'en';
        else if (lang == 'chi') $scope.changeLangs = 'chi';
        else $scope.changeLangs = 'ru';
        var getLanguage = lang;
        ctrl.language = getLanguage;
        Cookies.set('language', ctrl.language);
        $translate.use(ctrl.language);
    }
}]);
