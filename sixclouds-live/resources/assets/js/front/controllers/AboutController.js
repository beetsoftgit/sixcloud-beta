
app = angular.module('sixcloudAboutApp', ['ngRoute','pascalprecht.translate']);
/*var myApp = angular.module('sixcloudAboutApp', []);*/

// if (Cookies.get('language') == 'undefined' || Cookies.get('language') == '' || Cookies.get('language') == undefined) Cookies.set('language', 'en');
app.config(function($routeProvider, $translateProvider) {
    $translateProvider.translations('en', {
    	
    	lbl_lang_chng:'中文',
        lbl_sixclouds:'SixClouds',
    	lbl_the_company:'The Company',
    	lbl_mission:'Mission',
    	lbl_vision:'Vision',
    	lbl_core_values:'Core Values',
        lbl_about_us:'About Us',
    	lbl_home:'Home',
    	lbl_company_text_1:'SixClouds specialises in delivering quality educational programmes through a variety of bespoke multi-media platforms. SixClouds combines practical and fun educational content with the ease of technology enabled access to bring knowledge to the masses.',
    	lbl_company_text_2:'SixClouds was founded with the vision to make knowledge and learning accessible and affordable to everyone. When people improve in their personal capacity through knowledge gaining and experience, the community in which they reside and contributes to, will also improve and become better.',
    	lbl_mission_text:'Bringing quality content and knowledge programmes through technology to learners, to help them build confidence and strong foundations in the pursuit of personal excellence.',
    	lbl_vision_text:'Making learning for all ages fun and joyful.',
    	lbl_confidence_building:'Confidence Building',
    	lbl_confidence_building_text:'Facilitate the intellectual growth of learners to make confident strides forward, in their education, career and life.',
    	lbl_quality_affordability:'Quality and Affordability',
    	lbl_quality_affordability_text:'Build and curate high quality knowledge-based content that is affordable and accessible to everyone.',
    	lbl_constant_positive_evolution:'Constant Positive Evolution',
    	lbl_constant_positive_evolution_text:'Consistently evolve by harnessing and utilising technologies and information to bridge the gaps between knowledge and learning.',
    	lbl_trust:'Trust',
    	lbl_trust_text:'To be the trusted source of knowledge for learners and knowledge partners.',
    	lbl_tansparency:'Transparency',
    	lbl_tansparency_text:'To build sincere, fair and honest relationships with learners and knowledge partners.',
    	lbl_social_responsibility:'Social Responsibility',
    	lbl_social_responsibility_text:'A commitment to behave ethically and contribute to economic development whilst creating a positive impact on society and the environment at large.',
    	lbl_ignite:'Ignite',
    	lbl_sixteen:'Sixteen',
        lbl_discover:'Discover',
        lbl_proof_reading:'Proof Reading',
        lbl_quick_links:'Quick Links',
        lbl_become_seller:'Become a Seller',
        lbl_become_buyer:'Become a Buyer',
        lbl_customer_support:'Customer Support',
        lbl_terms_service:'Terms of Service',
        lbl_privacy_policy:'Privacy Policy',
        lbl_all_rights_reserved:'All rights reserved',
        lbl_our_team:'Our Team',
        lbl_richart_text:'Richard spent a large part of his career in the financial services industry. Having worked in major banks, he gained invaluable experience in strategic planning and business development. With his vast corporate experience and wide network of contacts, he has helped set up many start-ups and businesses, and is passionate about bringing affordable learning to the masses.',
        lbl_benson_text:'A bona fide teacher, Benson has several teaching certifications under his belt. Combining his Cambridge CELTA (Certificate in Teaching English to Speakers of other Languages) and certified trainer qualifications with his experience developing and delivering training programs, Benson has both the content building knowledge and technology enabler expertise to bring an exciting and fun suite of lessons to learners.',
        lbl_terrence_text:'Behind the wide and ready smile of Terrence lies more than 25 years of knowledge and expertise in the education, hospitality services, financial advisory and charity program management sectors. Education is very close to his heart, having been a frequent guest lecturer in educational institutions. He sits on the advisory committee in a Singapore primary school and is Cambridge CELTA (Certificate in Teaching English to Speakers of other Languages) qualified. Not only does he involve himself in imparting knowledge, he strongly believes the key to being equipped for life is continuous learning.',
        lbl_calina_text:'An experienced practitioner in customer and employee management, Calina developed a strong affection for transferring knowledge to help others grow socially and economically. She embodies our core values of empowering learners with the knowledge and confidence to better themselves.',


    }).translations('chi', {
    	
    	lbl_lang_chng:'English',   
        lbl_sixclouds:'六云',
    	lbl_the_company:'六云简介',
    	lbl_mission:'任务',
    	lbl_vision:'愿景',
		lbl_core_values:'核心理念',    	
		lbl_about_us:'关于我们',
        lbl_home:'家',
		lbl_company_text_1:'SixClouds专注于通过各种定制的多媒体平台提供高质量的教育课程。 SixClouds利用科技，将实用和有趣的教育内容带给大众。',
		lbl_company_text_2:'SixClouds成立的愿景是以合理的价格，让每个人都能轻易的学习和增长知识。',
		lbl_mission_text:'通过科技，把优质内容和知识项目带给学者，帮助他们建立信心和巩固基础。',
		lbl_vision_text:'让所有年龄层的学者都能快乐和愉悦的学习。',
		lbl_confidence_building:'建立信心',
		lbl_confidence_building_text:'促进学习者的智力成长，让他们在学习、事业和生活中迈出自信的步伐。',
		lbl_quality_affordability:'高质量和可承受性',
		lbl_quality_affordability_text:'构建和管理高质量的知识内容，价格合理，所有人都可承受。',
		lbl_constant_positive_evolution:'正向进化 ',
		lbl_constant_positive_evolution_text:'利用科技和信息来弥合知识和学习之间的差距，持续进步。',
		lbl_trust:'信任',
		lbl_trust_text:'成为学习者和合作伙伴的可靠的信息来源。',
		lbl_tansparency:'透明',
		lbl_tansparency_text:'与学习者和知识合作伙伴建立真诚、公正和诚实的关系。',
		lbl_social_responsibility:'社会责任',
		lbl_social_responsibility_text:'致力于有道德的行为和为经济发展做贡献，同时在更大的层面上对社会和环境产生积极影响。',
		lbl_ignite:'点燃',
		lbl_sixteen:'十六',
        lbl_discover:'发现',
        lbl_proof_reading:'证明阅读',
        lbl_quick_links:'快速链接',
        lbl_become_seller:'成为卖家',
        lbl_become_buyer:'成为买家',
        lbl_customer_support:'客服中心',
        lbl_terms_service:'服务条款',
        lbl_privacy_policy:'隐私政策',
        lbl_all_rights_reserved:'版权所有',
        lbl_our_team:'我们的团队',
        lbl_richart_text:'Richard在金融服务行业有着丰厚的经验。他曾在各大银行里就业，因此他在策略规划和业务发展方面拥有宝贵的经验。 凭借着丰富的企业经验和广泛的人脉关系，他帮助建立了许多创业公司和企业，并热衷于为以价格合理的学习经验带给大众。',
        lbl_benson_text:'Benson是一位真诚的老师，他有多个教学证书。他同时拥有剑桥CELTA（外语使用者英语教学证书）和通过认证的培训师资格以及培训课程开发和提供的经验。Benson同时拥有内容构建和技术推动者所需要的专业知识，因此他可以为学习者带来一整套令人兴奋和有趣的课程。',
        lbl_terrence_text:'Terrence脸上微笑的背后是他在教育、酒店服务、财务顾问和慈善项目管理部门超过25年的专业知识和经验。 教育非常接近他的内心需求。他曾经在教育机构担任过客座讲师。 他在新加坡一所小学担任顾问委员会委员，同时他也拥有剑桥CELTA证书（外语使用者英语教学证书）。 他不但参与传授知识，而且坚信生活的关键在于不断学习。',
        lbl_calina_text:'Calina是一名经验丰富的客户和员工管理人员。她对传授知识来帮助他人在社交和经济上的成长有着强烈的兴趣。她体现了我们的核心价值观, 通过知识和自信对学习者进行赋能，从而让他们有所突破。',

    }).translations('ru', {
        
        lbl_lang_chng:'中文',
        lbl_sixclouds:'SixClouds',
        lbl_the_company:'Компания',
        lbl_mission:'Цель',
        lbl_vision:'Видение',
        lbl_core_values:'Фундаментальные ценности',
        lbl_about_us:'О Наc',
        lbl_home:'Домой',
        lbl_company_text_1:'SixClouds  специализируется на предоставлении качественных образовательных программ через различные заказные мультимедийные платформы. SixClouds сочетает в себе практический и веселый образовательный контент  технологии с легкостью  позволили получить доступ, чтобы принести знания в массы.',
        lbl_company_text_2:'SixClouds  была основана с видением, чтобы сделать знания и обучение доступным и понятным для всех. Когда люди совершенствуются в своем личном качестве посредством приобретения знаний и опыта, сообщество, в котором они проживают и в котором они участвуют, также станет лучше.',
        lbl_mission_text:'Доведение качественного контента и программ знаний с помощью технологий до учащихся, чтобы помочь им построить уверенность и прочные основы в стремлении к личному совершенству.',
        lbl_vision_text:'Делает обучение для любого возраста увлекательным и веселым.',
        lbl_confidence_building:'Укрепление доверия',
        lbl_confidence_building_text:'Способствовать интеллектуальному росту учащихся, чтобы сделать уверенные шаги вперед, в их образовании, карьере и жизни..',
        lbl_quality_affordability:'Качаство и доступность.',
        lbl_quality_affordability_text:'Создание и курирование высококачественного контента на основе знаний, который является понятным и доступным для всех..',
        lbl_constant_positive_evolution:'Постоянная положительная динамика',
        lbl_constant_positive_evolution_text:'Последовательно развиваться путем использования и применения технологий и информации для преодоления разрыва между знаниями и обучением.',
        lbl_trust:'Доверие',
        lbl_trust_text:'Быть надежным источником знаний для учащихся и партнеров по знаниям.',
        lbl_tansparency:'Прозрачность',
        lbl_tansparency_text:'Строить искренние, справедливые и честные отношения с учащимися и партнерами по знаниям.',
        lbl_social_responsibility:'Социальная ответственность',
        lbl_social_responsibility_text:'Обязательство вести себя этично и вносить свой вклад в экономическое развитие, создавая при этом положительное влияние на общество и окружающую среду в целом.',
        lbl_ignite:'Ignite',
        lbl_sixteen:'Sixteen',
        lbl_discover:'Discover',
        lbl_proof_reading:'Proof Reading',
        lbl_quick_links:'Быстрые ссылки',
        lbl_become_seller:'Become a Seller',
        lbl_become_buyer:'Become a Buyer',
        lbl_customer_support:'Поддержка клиентов',
        lbl_terms_service:'Условия обслуживания',
        lbl_privacy_policy:'Конфиденциальность и безопасность',
        lbl_all_rights_reserved:'Все  права защищены',
        lbl_our_team:'Наша команда',
        lbl_richart_text:'Richard большую часть своей карьеры провел в сфере финансовых услуг. Работая в крупных банках, он приобрел бесценный опыт стратегического планирования и развития бизнеса. Благодаря своему обширному корпоративному опыту и широкой сети контактов, он помог создать множество стартапов и предприятий и увлечен тем, чтобы принести доступное обучение в массы.',
        lbl_benson_text:'Добросовестный учитель, Benson имеет несколько сертификатов преподавания. Сочетая его Cambridge CELTA (сертификат в обучении английскому языку для носителей других языков) и сертифицированные квалификации тренера с его опытом разработки и предоставления учебных программ, Бенсон обладает как знаниями в области создания контента, так и технологиями, позволяющими получить захватывающий и интересный набор уроков для учащихся.',
        lbl_terrence_text:'За широкой и приветливой улыбкой Terrence лежит более 25 лет знаний и опыта в сфере образования, гостиничных услуг, финансового консультирования и управления благотворительными программами. Образование очень близко его сердцу, будучи частым гостем лектором в учебных заведениях. Он заседает в Консультативном комитете в сингапурской начальной школе и имеет квалификацию Cambridge CELTA (сертификат по обучению английскому языку для носителей других языков). Он не только участвует в передаче знаний, он твердо верит, что ключ к тому, чтобы быть готовым к жизни-это непрерывное обучение .',
        lbl_calina_text:'Опытный практик в области управления клиентами и сотрудниками, Calina развил сильную привязанность к передаче знаний, чтобы помочь другим расти социально и экономически. Она воплощает наши основные ценности расширения возможностей учащихся со знаниями и уверенностью, чтобы улучшить себя.',


    });
    $translateProvider.preferredLanguage(Cookies.get('language'));
});
/* To change the interolate provider need to change it's originonal brackets.*/
app.config(['$interpolateProvider', function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
}]);
/* for changing title */
app.run(['$location', '$rootScope', '$http', '$timeout', function($location, $rootScope, $http, $timeout) {
    $rootScope.$on('$routeChangeSuccess', function(event, current, previous) {
        if (current.$$route != undefined) $rootScope.title = current.$$route.title;
        document.body.scrollTop = document.documentElement.scrollTop = 0;
    });
}]);
app.controller('AboutController', ['$scope', '$translate', '$location', '$anchorScroll', '$rootScope', '$timeout', function ($scope, $translate, $location, $anchorScroll, $rootScope, $timeout){
	console.log("About Controller");
    $rootScope.$on('$routeChangeSuccess', function () {

        console.log('Route Change: ' + $location.url());
      
    });
	var ctrl = this;
    if (Cookies.get('language') == 'undefined' || Cookies.get('language') == '' || Cookies.get('language') == undefined) {
        $scope.url = window.location.host;
        $scope.url = $scope.url.split('.');
        $scope.url = $scope.url[2];
        $timeout(function() {
            if($scope.url==undefined){
                $timeout(function() {
                    $scope.changeLang('en');
                },500);
            }
            if($scope.url == 'net' && $scope.url!=undefined){
                $timeout(function() {
                    $scope.changeLang('en');
                },500);
            }
            if($scope.url == 'cn' && $scope.url!=undefined){
                $timeout(function() {
                    $scope.changeLang('chi');
                },500);
            }
        },1000);
    }
    $scope.changeLangs = Cookies.get('language');
    // $scope.image_path = 'resources/assets/images/';
    $timeout(function() {
        $scope.baseurl = BASEURL;
        $scope.valueUrl = window.location.href;
        $scope.valueUrl = $scope.valueUrl.replace($scope.baseurl,'');
        $scope.valueUrl = $scope.valueUrl.split('/');
        if($scope.valueUrl.length==1) {
            $scope.takeDot = false;
            $scope.image_path = 'resources/assets/images';
        } else {
            $scope.takeDot = true;
            $scope.image_path = '../resources/assets/images';
            $scope.valueUrl2 = $scope.valueUrl[1];
                $('#'+$scope.valueUrl2+'_click').trigger('click');
        }
    },500);
    $scope.changeLangs = Cookies.get('language');
    $scope.changeLang = function(lang) {
        if (lang == 'en') $scope.changeLangs = 'en';
        else if (lang == 'chi') $scope.changeLangs = 'chi';
        else $scope.changeLangs = 'ru';
        var getLanguage = lang;
        ctrl.language = getLanguage;
        Cookies.set('language', ctrl.language);
        $translate.use(ctrl.language);
    }
   

}]);
