/*
 Author  : Komal Kapadi (komal@creolestudios.com)
 Date    : 8th December 2017
 Name    : EltSectionDetailController
 Purpose : All the functions for elt page
 */
angular.module('sixcloudApp').controller('EltSectionDetailController', ['Geteltsections', '$http', '$scope', '$timeout', '$rootScope', '$filter', function (Geteltsections, $http, $scope, $timeout, $rootScope, $filter) {
        console.log('EltSectionDetailController loaded');
        $scope.eltdata = Geteltsections.data.data;
    }]);