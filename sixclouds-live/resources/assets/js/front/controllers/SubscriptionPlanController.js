/*
 Author  : Komal Kapadi (komal@creolestudios.com)
 Date    : 5th December 2017
 Name    : SubscriptionPlanController
 Purpose : All the functions for subscription
 */
angular.module('sixcloudApp').controller('SubscriptionPlanController', ['$http', '$scope', '$timeout', '$rootScope', '$filter', function ($http, $scope, $timeout, $rootScope, $filter) {
        console.log('subscription plan controller loaded');
        $scope.update = function (user) {
            console.log(user);
        }
        $('.select-plan.box-list .box-btn a.btn').click(function(){     
            $(this).parents('.box').siblings('.box').removeClass('select-the-plan');
            $(this).parents('.box').addClass('select-the-plan');
        });

        $(document).ready(function () {
            $(".input-label input").focus(function ()
            {
                $(this).parent(".input-label").addClass('typing');
            });
            $('.input-label input').blur(function () {
                if ($('.input-label input').val() != '') {
                    $(this).parent(".input-label").addClass('typing');
                } else {
                    $(this).parent(".input-label").removeClass('typing');
                }
            });
        });
        $(".single-datepicker").daterangepicker({
            singleDatePicker: true,
            showDropdowns: true
        }); 
    }]);