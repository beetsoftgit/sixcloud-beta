/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 9th Oct 2018
 Name    : IgniteMyCodesController
 Purpose : All the functions for distributor's codes/url
 */
angular.module('sixcloudApp').controller('IgniteMyCodesController', ['$sce', 'ignitefactory', '$location', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', function($sce, ignitefactory, $location, $routeParams, $http, $scope, $timeout, $rootScope, $filter) {
    /*urlObj = JSON.parse(localStorage.getItem('urlObj'));
    url_number_static = localStorage.getItem('url_number_static');
    url_number_static++;
    url = [{
        "url_number": url_number_static,
        "url": $location.url()
    }];
    last_url = urlObj.pop();
    localStorage.setItem('urlObj',JSON.stringify(urlObj));
    if(last_url[0].url_number !== url[0].url_number) {
        urlObj.push(last_url);
    }
    urlObj.push(url);
    localStorage.setItem("urlObj",JSON.stringify(urlObj));
    localStorage.setItem("url_number_static",url_number_static);*/
    $timeout(function(){
        /*if($rootScope.loginDistributorData == '' || $rootScope.loginDistributorData == undefined){
            window.location.replace("ignite");
        }*/
        $('.ignite-menu').find('li').removeClass('active');
        $('#my-codes').addClass('active');
        $scope.distributor = $rootScope.loginDistributorData;
    },1000);
    $timeout(function(){
        $scope.distributor = $rootScope.loginDistributorData;
    },1500);
    $scope.url = $location.url();

}]);