/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 28th Aug 2018
 Name    : IgniteMySubscriptionController
 Purpose : All the functions for ignite users subscription page
 */
angular.module('sixcloudApp').controller('IgniteMySubscriptionController', ['$sce', 'ignitefactory', '$location', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', 'Getuserzone', function($sce, ignitefactory, $location, $routeParams, $http, $scope, $timeout, $rootScope, $filter, Getuserzone) {
    /*urlObj = JSON.parse(localStorage.getItem('urlObj'));
    url_number_static = localStorage.getItem('url_number_static');
    url_number_static++;
    url = [{
        "url_number": url_number_static,
        "url": $location.url()
    }];
    last_url = urlObj.pop();
    localStorage.setItem('urlObj',JSON.stringify(urlObj));
    if(last_url[0].url_number !== url[0].url_number) {
        urlObj.push(last_url);
    }
    urlObj.push(url);
    localStorage.setItem("urlObj",JSON.stringify(urlObj));
    localStorage.setItem("url_number_static",url_number_static);*/
    console.log('IgniteMySubscriptionController loaded');
    $("#content").removeClass('hide');
    $rootScope.showiframe = false;
    $scope.url = $location.url();
    // getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
    getLanguage = Cookies.get('language');
    $scope.current_domain = current_domain;
    $scope.userZone = Getuserzone.data.data;
    if($scope.userZone.zone_name.toUpperCase() == 'CIS') {
        $scope.price_type = 2;
    } else {
        $scope.price_type = 1;
    }
    if($scope.userZone.is_subscription_allowed == 0) {
        swal({
            title: "",
            text: "Please contact to customer support",
            allowEscapeKey:false
        }, function() {
            window.location.href = "customer-support";
        });
    }
    ignitefactory.Getloginuserdatadistributor().then(function(response) {
        if (response.data.status == 1) {
            $rootScope.loginDistributorData = response.data.data;
        } else {
            $rootScope.loginDistributorData = response.data;
        }
    });

    ignitefactory.Getuserzone().then(function (response){
        $rootScope.userZone = response.data.data
    });
    $timeout(function(){
        changeLanguage(getLanguage);
        $('#chk_lang').change(function() {
            console.log("a");
            getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
            changeLanguage(getLanguage);
        });
    },500);
    function changeLanguage(lang) {
        if (lang == "chi") {
            $timeout(function(){
                $scope.changeLangs = 'chi';
            },500);
        } else if(lang == "en") {
            $timeout(function(){
                $scope.changeLangs = 'en';
            },500);
        } else {
            $timeout(function(){
                $scope.changeLangs = 'ru';
            },500);
        }
    }
    $scope.changeTab = function(whichTab){
        if(whichTab == 'SubscriptionPlanTab'){
            $('#SubscriptionPlanTabButton').addClass('active');
            $('#SubscriptionPlanTabButton').removeClass('hidden-arrow')
            $('.subjects').addClass('active')
            $('#TransactionsTabButton').removeClass('active');
            $scope.SubscriptionPlanTab = true;
            $scope.TransactionsTab = false;
        } else {
            $('#TransactionsTabButton').addClass('active');
            $('#TransactionsTabButton').removeClass('hidden-arrow')
            $('#SubscriptionPlanTabButton').removeClass('active');
            $scope.TransactionsTab = true;
            $scope.SubscriptionPlanTab = false;
        }
    }
    $scope.changeTab('SubscriptionPlanTab');
    $scope.cartTotal = 0;
    $scope.cartSubTotal = 0;
    $scope.selectedCategory = [];
    $scope.Changecart = function(amount,id,isPaid){
        console.log('here');
        if($scope.subscriberBuzzes&&!$scope.subscriberBuzzes.includes(id)){
            $scope.noPromoMatch = false;
            $scope.invoice = Math.floor(Math.random()*10);
            if($('#buzz_'+id).prop("checked")){
                $scope.cartSubTotal += amount;
                $scope.promo_code = undefined;
                $scope.cartTotal = $scope.cartSubTotal;
                $scope.selectedCategory.push(id);
            }
            if(!$('#buzz_'+id).prop("checked")){
                if($scope.cartTotal > 0){
                    $scope.cartSubTotal -= amount;
                    $scope.promo_code = undefined;
                    $scope.cartTotal = $scope.cartSubTotal;
                    $scope.selectedCategory.splice($scope.selectedCategory.indexOf(id),1);
                }
            }
            if($scope.wantPurchase){
                if($.inArray($scope.wantPurchase.id, $scope.selectedCategory) == -1) {
                    $scope.wantPurchase.id = '';
                }
            }
        }
    }

    $scope.changeCartValue = function() {
        $scope.cartTotal = 0;
        $scope.cartSubTotal = 0;
        for (var i = $scope.selectedCategory.length - 1; i >= 0; i--) {
            obj = $scope.buzzData.find(o => o.id === $scope.selectedCategory[i]);
            if($scope.price_type == 1) {
                $scope.cartTotal += obj.category_price;
            } else if($scope.price_type == 2) {
                $scope.cartTotal += obj.category_price_sgd;
            }
        }
        $scope.cartSubTotal = $scope.cartTotal;
    }

    $scope.GetloginuserandBuzzdata = function(){
        if($scope.userZone.is_subscription_allowed != 0) {
            ignitefactory.Mysubscriptiondata().then(function(response) {
                $scope.subscriberBuzzes = [];
                if(response.data.data.length > 0){
                    $scope.subscriptionData = response.data.data;
                    var temp = [];
                    for (var i = 0; i < $scope.subscriptionData.length; i++) {
                        temp = $scope.subscriptionData[i].video_category_id.split(',');
                        for (var j = 0; j < temp.length; j++) {    
                            $scope.subscriberBuzzes.push(parseInt(temp[j]));
                        }
                    }
                    jQuery.unique($scope.subscriberBuzzes);
                }
                else{
                    $scope.subscriptionData = [];
                }
            });
            $rootScope.subheader = '';
            ignitefactory.Getsubjects().then(function(response) {
                if(response.data.data.length > 0){
                    $scope.subjects = response.data.data;

                    $rootScope.isBUZZ=$scope.subjects.find(data=>{ return data.id==1 });
                    $rootScope.isBUZZRU=$scope.subjects.find(data=>{ return data.id==2 });
                    $rootScope.isMAZE=$scope.subjects.find(data=>{ return data.id==3 });
                    $rootScope.isSMILE=$scope.subjects.find(data=>{ return data.id==4 });
                    $rootScope.isSMILESG=$scope.subjects.find(data=>{ return data.id==7 });

                    $rootScope.isMAZE=$rootScope.isMAZE?true:false;
                    $rootScope.isSMILE=$rootScope.isSMILE?true:false;
                    $rootScope.isSMILESG=$rootScope.isSMILESG?true:false;
                    $rootScope.isBUZZ=$rootScope.isBUZZ?true:false;
                    $rootScope.isBUZZRU=$rootScope.isBUZZRU?true:false;

                    $scope.subjectsClass = 12/$scope.subjects.length;
                    $scope.Getcategories($scope.subjects[0].encoded_string, $scope.subjects[0].id,'wantPurchase');
                }
            });
        }
    }
    $scope.Getcategories = function(enc_id,id,frm=''){
        if($rootScope.subject_id!=undefined && $rootScope.url!=undefined){
            if($scope.userZone.is_subscription_allowed != 0) {
                ignitefactory.Getcategoriesforsubject({'id':$rootScope.url}).then(function(response) {
                    if(response.data.data.length > 0){
                        $scope.cartTotal = 0;
                        $scope.cartSubTotal = 0;
                        $scope.selectedCategory = [];
                        $('.subjects').removeClass('hidden-arrow');
                        $('.subjects').removeClass('active');
                        $('#subjectButton_'+$rootScope.subject_id).addClass('active');
                        $scope.showCart = true;
                        $scope.buzzData = response.data.data;
                    }
                });
            }
        }else{
            if($scope.userZone.is_subscription_allowed != 0) {
                ignitefactory.Getcategoriesforsubject({'id':enc_id}).then(function(response) {
                    if(response.data.data.length > 0){
                        if(frm=='wantPurchase'){
                            setTimeout(function() {
                                $('#buzz_'+$scope.wantPurchase.id).prop('checked', true);
                            }, 2000);
                            $scope.cartTotal = $scope.cartTotal!=0?$scope.cartTotal:0;
                            $scope.cartSubTotal = $scope.cartSubTotal!=0?$scope.cartSubTotal:0;
                            $scope.selectedCategory = $scope.selectedCategory.length!=0?$scope.selectedCategory:[];
                        }else{
                            if($scope.wantPurchase){
                                $scope.wantPurchase.id = '';
                            }
                            $scope.cartTotal = 0;
                            $scope.cartSubTotal = 0;
                            $scope.selectedCategory = [];
                        }
                        $('#SubscriptionPlanTabButton').removeClass('hidden-arrow');
                        $('.subjects').removeClass('hidden-arrow');
                        $('.subjects').removeClass('active');
                        $('#subjectButton_'+id).addClass('active');
                        $scope.showCart = true;
                        $scope.buzzData = response.data.data;
                    }
                });
            }
        }
    }
    $scope.GetloginuserandBuzzdata();
    $scope.Ignitepayemnt = function(){
        var ignitePayemnt = new FormData($("#ignite_payment_data")[0]);
        if(getLanguage=='en'){
            showLoader('.ignite_payment_data','Please wait');
        }else{
            showLoader('.ignite_payment_data','请稍候');
        }
        ignitePayemnt.append('promoStatus',$scope.promoStatus);
        ignitefactory.Ignitepayemnt(ignitePayemnt).then(function(response) {

            if(getLanguage=='en'){hideLoader('.ignite_payment_data','Add to Cart');}else{hideLoader('.ignite_payment_data','添加到购物车');}
            if (response.data.status == 2) window.location.replace("ignite-login");
            if (response.data.status == 1 && response.data.data.slug != '') {
                window.location = 'make-payment-ignite/'+response.data.data.slug;
            } else {
                if(getLanguage=='en'){hideLoader('.ignite_payment_data','Make Payment');}else{hideLoader('.ignite_payment_data','添加到购物车');}
            }
        });
    }
    $scope.Stripepayemnt = function(){
        $scope.language = Cookies.get('language');
        showLoader('.stripe_payment_btn',$scope.language=='en'?'Please wait':'请稍候')
        ignitefactory.Encryptdataforstripepayment({'price_type':$scope.price_type,'for_category':$scope.selectedCategory,'language':$scope.language=='en'?1:2,'promoStatus':$scope.promoStatus,'promo':$scope.promo_code}).then(function(response) {
            hideLoader('.stripe_payment_btn',$scope.language=='en'?'Make Payment':'添加到购物车');
            if(response.data.status==1){
                window.location.replace('ignite-payment/'+response.data.data);
            } else {
                simpleAlert('error', '', response.data.message);
            }
        });
    }

    $scope.changeSelectedLang = function (currLang) {
        console.log(currLang, 'currLangcurrLangcurrLangcurrLangcurrLangcurrLangcurrLangcurrLangcurrLangcurrLang')
        $scope.selectedLang = !currLang;
    }

    $scope.promoStatus = 0;

    $scope.Checkpromo = function(promo){
        if($scope.cartSubTotal==0){
            if(Cookies.get('language') == 'en') {
                simpleAlert('error', '', "Please select at least one plan.");
            } else if(Cookies.get('language') == 'chi') {
                simpleAlert('error', '', "未选购订阅计划。");
            }
            return;
        } else {
            showLoader('.apply_promo');
            $scope.checkforCategory = [];
            $('input[type=checkbox]:checked').each(function () {
                $scope.checkforCategory.push($(this).val());
            });
            if($scope.selectedLang==true)
                $scope.language = 1;
            else
                $scope.language = 0;
            ignitefactory.Checkpromo({'price_type':$scope.price_type,'promo':promo,'for_category':$scope.checkforCategory,'language':$scope.language,'already_paid':$scope.subscriberBuzzes,'cart_total':$scope.cartTotal}).then(function(response) {
                hideLoader('.apply_promo',$scope.selectedLang==true?'Apply':'应用');
                if(response.data.code==2) window.location.replace("ignite-login");
                $scope.promoStatus = response.data.status;
                if(response.data.status==1){
                    $scope.cartTotal = response.data.data;
                    $scope.noPromoMatch = false;
                    $scope.promo_code = promo;
                } else {
                    $scope.cartTotal = $scope.cartSubTotal;
                    $scope.noPromoMatch = true;
                    $scope.message = response.data.message;
                }
            });
        }
    }

    if($scope.url.split('/').length > 2) {
        var cat_id = $scope.url.split('/').pop();
        ignitefactory.WantToPurchaseData({'slug': cat_id}).then(function(response) {
            if(response.data.status){
                $scope.wantPurchase = response.data.data.purchase;
                $scope.cartTotal = 0;
                $scope.cartSubTotal = 0;
                $scope.selectedCategory = [];
                $scope.Getcategories($scope.wantPurchase.encoded_string,$scope.wantPurchase.subject_id,'wantPurchase')
                // $('.subjects').removeClass('active');  
                $('#subjectButton_'+$scope.wantPurchase.subject_id).addClass('active');
                $('#buzz_'+$scope.wantPurchase.id).prop('checked', true);
                $scope.showCart = true;

                $scope.promo_code = undefined;
                
                $timeout(function(){
                    $('#buzz_'+$scope.wantPurchase.id).prop('checked', true);
                    $('#buzzNew_'+$scope.wantPurchase.id).addClass('shake_custom');    
                    $scope.cartTotal = $scope.wantPurchase.category_price;
                    $scope.cartSubTotal = $scope.wantPurchase.category_price;
                    $scope.selectedCategory.push($scope.wantPurchase.id);
                }, 500);
                
                setTimeout(function(){
                    $('#buzzNew_'+$scope.wantPurchase.id).removeClass('shake_custom');
                }, 2500);

            }
        });
    }
}]);