/*
 Author  : Nivedita Mitra (nivedita@creolestudios.com)
 Date    : 21st Aug 2018
 Name    : IgniteMyTeamController
 Purpose : All the functions for Ignite Distributors team members
 */
angular.module('sixcloudApp').controller('IgniteMyTeamController', ['$sce', 'ignitefactory', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', '$location', function($sce, ignitefactory, $routeParams, $http, $scope, $timeout, $rootScope, $filter, $location) {
    /*urlObj = JSON.parse(localStorage.getItem('urlObj'));
    url_number_static = localStorage.getItem('url_number_static');
    url_number_static++;
    url = [{
        "url_number": url_number_static,
        "url": $location.url()
    }];
    last_url = urlObj.pop();
    localStorage.setItem('urlObj',JSON.stringify(urlObj));
    if(last_url[0].url_number !== url[0].url_number) {
        urlObj.push(last_url);
    }
    urlObj.push(url);
    localStorage.setItem("urlObj",JSON.stringify(urlObj));
    localStorage.setItem("url_number_static",url_number_static);*/
    console.log('Ignite my team controller loaded');
    $timeout(function(){
        $('.ignite-menu').find('li').removeClass('active');
        $('#my-team').addClass('active');
    },1000);

    getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
    changeLanguage(getLanguage);
    $('#chk_lang').change(function() {
        getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
        changeLanguage(getLanguage);
    });
 
    function changeLanguage(lang) {
        if (lang == "chi") {
            $scope.chinese = true;
            $scope.english = false;
        } else {
            $scope.chinese = false;
            $scope.english = true;
        }
    }

    $scope.Getallteamdata = function (data,pageNumber) {
        ignitefactory.Getallteamdata({'searchby':data},pageNumber).then(function (response) {                      
            if(response.data.status == 1){
                $scope.allTeamMembers          = response.data.data.data;                       
                $scope.totalTeamMembers        = response.data.data.total;
                $scope.paginate                = response.data.data;
                $scope.currentTeamMemberPaging = response.data.data.current_page;
                $scope.rangeTeamMemberpage     = _.range(1, response.data.data.last_page + 1);                
            }else if(response.data.status == 0){
                if(response.data.code==2)
                    window.location.replace("distributor");
            }            
        });
    }
    $scope.Getallteamdata($scope.searchby,1);
    
    $scope.search = function(search) {
        $scope.Getallteamdata(search,1);
    }

    $scope.Exportdata = function(export_type) {
        if(export_type!= undefined && export_type!= ''){
            window.location = BASEURL + 'ExportDistributorTeam/' + export_type;
        } else {
            simpleAlert('error', '', 'Select export type');
        }
    }

    $scope.correctCommission = function(what,text){
        var whatClass = what+'_class';
        var whattitle = what+'_title';
        var whatCheckClass = what+'_check_class';
        var whatCheck = what+'_check';
        if($scope[what]<100){
            $scope[whatClass] = "fa fa-check-circle out-icon";
            $scope[whattitle] = "";
            $scope[whatCheckClass] = 'has-success';
        } else {
            $scope[whatCheck] = false;
            $scope[whatClass] = "fa fa-exclamation-circle out-icon";
            $scope[whatCheckClass] = 'has-error';
            $scope[whattitle] = text+" should be less then 100";
        }
    }
    $scope.incorrectCommission = function(what,text){
        var whatClass = what+'_class';
        var whattitle = what+'_title';
        var whatCheckClass = what+'_check_class';
        var whatCheck = what+'_check';
        $scope[whatCheck] = false;
        $scope[whatCheckClass] = 'has-error';
        $scope[whatClass] = "fa fa-exclamation-circle out-icon";
        $scope[whattitle] = text+" is required";
    }

    $scope.countDecimals = function(value,scope) {
        if(value!=undefined && value!=''){
            if (Math.floor(value) != value){
                if(scope==1){
                    $scope.countValue = value.toString().split(".")[1].length || 0;
                    if($scope.countValue>2){
                        $scope.commission_percentage_class = "fa fa-exclamation-circle out-icon";
                        $scope.commission_percentage_check_class = 'has-error';
                        $scope.commission_percentage_title = "Commission Percentage allows only 2 decimals";
                    } else {
                        $scope.correctCommission('commission_percentage','Commission percentage');
                    }
                } else {
                    $scope.renewalcountValue = value.toString().split(".")[1].length || 0;
                    if($scope.renewalcountValue>2){
                        $scope.renewal_commission_percentage_class = "fa fa-exclamation-circle out-icon";
                        $scope.renewal_commission_percentage_check_class = 'has-error';
                        $scope.renewal_commission_percentage_title = "Renewal Commission Percentage allows only 2 decimals";
                    } else {
                        $scope.correctCommission('renewal_commission_percentage','Renewal Commission percentage');
                    }
                }
            } else {
                if(scope==1){
                    $scope.correctCommission('commission_percentage','Commission percentage');
                } else {
                    $scope.correctCommission('renewal_commission_percentage','Renewal Commission percentage');
                }
                return 0;
            }
        } else {
            if(scope==1)
                $scope.incorrectCommission('commission_percentage','Commission percentage');
            else
                $scope.incorrectCommission('renewal_commission_percentage','Renewal Commission percentage');
        }
    }
    $scope.AddTeamMember = function(){
        /*if($scope.team_first_name == undefined || $scope.team_last_name == undefined || $scope.team_email == undefined || $scope.team_contact == undefined||$scope.team_first_name == '' || $scope.team_last_name == '' || $scope.team_email == '' || $scope.team_contact == ''){*/
        if($scope.team_first_name == undefined || $scope.team_last_name == undefined || $scope.team_email == undefined || $scope.team_first_name == '' || $scope.team_last_name == '' || $scope.team_email == '' || $scope.commission_percentage == undefined || $scope.commission_percentage == '' || $scope.renewal_commission_percentage == undefined || $scope.renewal_commission_percentage == '' ){
            if($scope.team_first_name == undefined || $scope.team_first_name == ''){
                $scope.team_first_name_error = true;
            }
            if($scope.team_last_name == undefined || $scope.team_last_name == ''){
                $scope.team_last_name_error = true;
            }
            if($scope.team_email == undefined || $scope.team_email == ''){
                $scope.team_email_error = true;
            }
            if ($scope.commission_percentage == undefined||$scope.commission_percentage == '') {
                $scope.incorrectCommission('commission_percentage','Commission percentage');
            }
            if ($scope.renewal_commission_percentage == undefined||$scope.renewal_commission_percentage == '') {
                $scope.incorrectCommission('renewal_commission_percentage','Renewal Commission percentage');
            }
        }else{
            showLoader('.submit_call_team');
            var addTeamMember = new FormData($("#add_team_member")[0]);
            addTeamMember.append('language',Cookies.get('language'));
            if(getLanguage=='en'){
                text='Add ';
            }else{
                text='提交';
            }
            if($scope.editMember){
                var $from = new Date(Date.parse($('#effective_date').val(),'DD/MM/YYYY'));
                var $to = new Date(new Date().setMonth(new Date().getMonth() + 1,1));
                if($from.setHours(0,0,0,0) < $to.setHours(0,0,0,0)){
                    var dateError = Cookies.get('language')=='en'?'Effective Date should be from next month':'有效的日期应该是从下一个月的';
                    simpleAlert('error', '', dateError);
                    hideLoader('.submit_call_team', text);
                    return;
                }
                else
                    addTeamMember.append('effective_date',$('#effective_date').val());
            }
            ignitefactory.Addteammember(addTeamMember).then(function(response){
                hideLoader('.submit_call_team', text);
                if(response.data.status==1)
                {
                    $('#newTeamMember').modal('hide');
                    $scope.showEffectiveDatePicker = false;
                    $scope.editMember              = false;
                    window.location.replace("my-team");
                }
                if(response.data.status==0)
                {
                    simpleAlert('error','',response.data.message);
                }
            });
        }
    }
    $scope.viewQRCodeModal = function(qrfilename){
        $scope.qrfilename = qrfilename;
        $('#viewQRCode').modal('show');
    }
    function deletefunction(){
      ignitefactory.Deleteteammember({'id':$scope.team_member_id}).then(function(response) {
          if(response.data.code == 2)
              window.location.replace("distributor");
          if (response.data.status == 1)
          {
            $scope.Getallteamdata('',1);
          }
          else {
            simpleAlert('error','',response.data.message);
          }
      });
    }
    $scope.Deleteteammember = function(team_member_id){
        $scope.team_member_id = team_member_id;
        confirmDialogueDifferentButton('','','',deletefunction);
    }
    $scope.Edituserinfo = function(id,first_name,last_name,email_address,contact,commission_percentage,renewal_commission_percentage){
        $scope.editMember = true;
        $scope.editMemberId = id;
        $scope.team_first_name = first_name;
        $scope.team_last_name = last_name;
        $scope.team_email = email_address;
        $scope.commission_percentage = commission_percentage;
        $scope.renewal_commission_percentage = renewal_commission_percentage;
        $scope.showEffectiveDatePicker = true;
        $timeout(function(){        
            $('#effective_date').datepicker({
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                dateFormat: 'MM yy',
                onClose: function(dateText, inst) { 
                    $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                },
                minDate: new Date(new Date().setMonth(new Date().getMonth() + 1,1)),
            });
        },100);
        
        /*$scope.team_contact = contact;*/

        $('#newTeamMember').modal('show');
    }
    $scope.Openmodal = function(){
        $scope.editMember              = false;
        $scope.showEffectiveDatePicker = false;
        $('#newTeamMember').modal('show');
        $('#add_team_member')[0].reset();
        
    }
    $scope.Closemodal = function(){
        $('#newTeamMember').modal('hide');
        $('#add_team_member')[0].reset();
        $scope.team_first_name                           = undefined;
        $scope.team_last_name                            = undefined;
        $scope.team_email                                = undefined;
        $scope.commission_percentage                     = undefined;
        $scope.renewal_commission_percentage             = undefined;
        /*$scope.team_contact                            = undefined;*/
        $scope.team_first_name_error                     = false;
        $scope.team_last_name_error                      = false;
        $scope.team_email_error                          = false;
        $scope.team_contact_error                        = false;
        $scope.commission_percentage_check_class         = '';
        $scope.renewal_commission_percentage_check_class = '';
        $scope.commission_percentage_check               = undefined;
        $scope.renewal_commission_percentage_check       = undefined;
        $scope.showEffectiveDatePicker                   = false;
    }
    function activeFunction(){
      ignitefactory.Activeamember({'id':$scope.member_id}).then(function(response) {
          if(response.data.code == 2)
              window.location.replace("distributor");
          if (response.data.status == 1)
          {
            $scope.Getallteamdata('',1);
          }
          else {
            simpleAlert('error','',response.data.message);
          }
      });
    }
    $scope.activeMember = function(member_id){
        $scope.member_id = member_id;
        confirmDialogueDifferentButton('','','',activeFunction);
    }
}]);