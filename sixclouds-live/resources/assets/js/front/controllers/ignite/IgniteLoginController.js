/*
 Author  : Nivedita (nivedita@creolestudios.com)
 Date    :  2018
 Name    : IgniteLoginController
 Purpose : All the functions for Igite login page
 */
angular.module('sixcloudApp').controller('IgniteLoginController', ['$sce', 'ignitefactory', '$location', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter','MetaService', function($sce, ignitefactory, $location, $routeParams, $http, $scope, $timeout, $rootScope, $filter,MetaService) {
    /*urlObj = [];
    url_number_static = localStorage.getItem('url_number_static');
    url_number_static++;
    url = [{
        "url_number": -1,
        "url": '/ignite'
    }];
    urlObj.push(url);
    url = [{
        "url_number": url_number_static,
        "url": $location.url()
    }];
    last_url = urlObj.pop();
    localStorage.setItem('urlObj',JSON.stringify(urlObj));
    if(last_url) {
        if(last_url[0].url_number !== url[0].url_number) {
            urlObj.push(last_url);
        }
    }
    urlObj.push(url);
    localStorage.setItem("urlObj",JSON.stringify(urlObj));
    localStorage.setItem("url_number_static",url_number_static);*/
    console.log('IgniteLogin controller loaded');
    $rootScope.ignitePage = "";
    $timeout(function(){
        /*if($rootScope.loginDistributorData == '' || $rootScope.loginDistributorData == undefined){
            window.location.replace("ignite");
        }*/
        $('.ignite-menu').find('li').removeClass('active');
        $('#all-referral').addClass('active');
    },1000);

    // $rootScope.metaservice = MetaService;
    // $rootScope.metaservice.set("Sign In – Ignite Login","Sign in SixClouds Ignite and learn english with animated videos, interactive quizzes and worksheets.");

    $scope.url = $location.url();
    if($scope.url.includes('/ignite-login') != false){
        $scope.url = '/'+ $scope.url.split('/')[1];    
    }
    // else{
    //     $scope.url = '/'+ $scope.url.split('/')[1];
    // }
    //$scope.url = '/'+ $scope.url.split('/')[1];
    $scope.Dodistributorsignin = function() {
        if ($scope.password == undefined || $scope.email_address == undefined) {
            if ($scope.password == undefined) {
                $scope.password_error = true;
            }
            if ($scope.email_address == undefined) {
                $scope.email_address_error = true;
            }
        } else {
            login_data                  = {}
            login_data['email_address'] = $scope.email_address;
            login_data['password']      = $scope.password;
            login_data['check_for']     = $scope.Getpageasperurl($scope.url);
            login_data['language']      = Cookies.get('language');
            login_data['timezone']    = moment.tz.guess();
            ignitefactory.Dodistributorsignin(login_data).then(function(response) {
                if (response.data.code == 5) {
                    if($scope.url == '/ignite-login'){
                        window.location.replace("my-account");
                    }
                }
                if (response.data.status == 1) {
                    $rootScope.loginDistributorData = response.data.data;
                    if($scope.url == '/team-login'){
                        if(response.data.code == 200)
                            window.location.replace("my-referral");
                        else if(response.data.code == 401)
                            window.location.replace("verify-phone-number");
                    }
                    if($scope.url == '/distributor'){
                        if($rootScope.loginDistributorData.status == 1)
                            window.location.replace("subscribers");
                        if($rootScope.loginDistributorData.status == 0)
                            window.location.replace("verify-distributor");
                    }
                    if($scope.url == '/ignite-login'){
                        if (response.data.code == 5) {
                            window.location.replace("my-account");
                        } else {
                            if(response.data.data.video_preference=='' || response.data.data.video_preference==null) {
                                window.location.replace("select-categories");

                            } else {
                                ignitefactory.getCategoriesWithLanguages().then(function(response) {
                                    if (response.data.status == 1) {
                                        window.location.replace("ignite-categories");
                                    } else {
                                        $rootScope.categoryLanguages = response.data;
                                    }
                                });
                            }
                        }
                    }
                } else {
                    if(response.data.data.status==5){
                        window.location.replace("user-verify-otp/"+response.data.data.country_code+'/'+response.data.data.phone_number+'/'+response.data.data.email_address);
                    } else {
                        $scope.login_error = response.data.message;
                        $scope.show_error = true;
                        $timeout(function() {
                            $scope.show_error = false;
                        }, 5000);
                    }
                }
            });
        }
    }
    $scope.Forgetpassword = function() {
        if ($scope.email == undefined) {
            $scope.forgot_email_address_error = true;
        } else {
            showLoader('.forgot-password');
            if($scope.url == '/distributor/team-login')
                $scope.module = 'DistributorsTeamMembers';
            if($scope.url == '/distributor')
                $scope.module = 'Distributors';
            if($scope.url == '/ignite-login')
                $scope.module = 'User';
            ignitefactory.forgotpassword({'email_address':$scope.email,'module':$scope.module,'language':Cookies.get('language'),'redirectTo':'1'}).then(function(response) {
                hideLoader('.forgot-password', Cookies.get('language')=='en'?'Submit':(Cookies.get('language')=='chi'?'提交':'Отправить'));
                if (response.data.status == 1) {
                    $scope.forgot_email_address_error = false;
                    $('.close').click();
                    simpleAlert('success', Cookies.get('language')=='en'?'Forgot Password':(Cookies.get('language')=='chi'?'忘记密码':'Забыли пароль'), response.data.message,Cookies.get('language')=='en'?'Ok':(Cookies.get('language')=='chi'?'好。':'Ладно'));
                    $('#forgetPasswordForm')[0].reset();
                    $scope.email = "";
                } else {
                    $scope.login_error = response.data.message;
                    $scope.show_error = true;
                    $timeout(function() {
                        $scope.show_error = false;
                    }, 5000);
                }
            });
        }
    }

    $scope.Getpageasperurl = function(getUrl){
        if(getUrl == '/team-login')
            pageData      = 'team';
        if(getUrl == '/distributor')
            pageData      = 'distributor';
        if(getUrl == '/ignite-login')
            pageData      = 'users';

        return pageData;
    }
    $scope.pageData = $scope.Getpageasperurl($scope.url);
}]);