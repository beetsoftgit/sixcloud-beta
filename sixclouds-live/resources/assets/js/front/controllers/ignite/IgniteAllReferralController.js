/*
 Author  : Nivedita Mitra (nivedita@creolestudios.com)
 Date    : 20th Aug 2018
 Name    : IgniteAllReferralController
 Purpose : All the functions for distributors login
 */
angular.module('sixcloudApp').controller('IgniteAllReferralController', ['$sce', 'ignitefactory', '$location', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', function($sce, ignitefactory, $location, $routeParams, $http, $scope, $timeout, $rootScope, $filter) {
    /*urlObj = JSON.parse(localStorage.getItem('urlObj'));
    url_number_static = localStorage.getItem('url_number_static');
    url_number_static++;
    url = [{
        "url_number": url_number_static,
        "url": $location.url()
    }];
    last_url = urlObj.pop();
    localStorage.setItem('urlObj',JSON.stringify(urlObj));
    if(last_url[0].url_number !== url[0].url_number) {
        urlObj.push(last_url);
    }
    urlObj.push(url);
    localStorage.setItem("urlObj",JSON.stringify(urlObj));
    localStorage.setItem("url_number_static",url_number_static);*/
    $timeout(function(){
        /*if($rootScope.loginDistributorData == '' || $rootScope.loginDistributorData == undefined){
            window.location.replace("ignite");
        }*/
        $('.ignite-menu').find('li').removeClass('active');
        $('#my-referral').addClass('active');
        $('#all-referral').addClass('active');
    },1000);
    $scope.url = $location.url();
    $scope.GetallReferalData = function (data,pageNumber) {
        ignitefactory.GetallReferalData({'searchby':data},pageNumber).then(function (response) {                    	
            if(response.data.status == 1){
                $scope.allReferals = response.data.data.data;                           
                $scope.totalReferals = response.data.data.total;
                $scope.paginate = response.data.data;
                $scope.currentReferalsPaging = response.data.data.current_page;
                $scope.rangeReferalspage = _.range(1, response.data.data.last_page + 1);                
            }else if(response.data.status == 0){
                if(response.data.code==2){
                    if($scope.url == '/subscribers')
                        window.location.replace("distributor");
                    if($scope.url == '/my-referral')
                        window.location.replace("distributor/team-login");
                }         
            }            
        });
    }
    $scope.GetallReferalData($scope.searchby,1);

    $scope.search = function(search) {
            $scope.GetallReferalData(search,1);
    }

    $scope.Exportdata = function(export_type) {
        if(export_type!= undefined && export_type!= ''){
            window.location = BASEURL + 'ExportSubscribers/' + export_type;
        } else {
            simpleAlert('error', '', 'Select export type');
        }
    }

}]);