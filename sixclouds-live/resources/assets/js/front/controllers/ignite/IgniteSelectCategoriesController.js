/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 4th Feb, 2019
 Name    : IgniteSelectCategoriesController
 Purpose : All the functions after subscriber login to select the subject, categories, sub-categories and based on the last nth sub-category, redirect the subscriber to the video listing page
 */
angular.module('sixcloudApp').controller('IgniteSelectCategoriesController', ['$sce', 'ignitefactory', '$location', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', function($sce, ignitefactory, $location, $routeParams, $http, $scope, $timeout, $rootScope, $filter) {
    /*urlObj = JSON.parse(localStorage.getItem('urlObj'));
    url_number_static = localStorage.getItem('url_number_static');
    url_number_static++;
    url = [{
        "url_number": url_number_static,
        "url": $location.url()
    }];
    last_url = urlObj.pop();
    localStorage.setItem('urlObj',JSON.stringify(urlObj));
    if(last_url[0].url_number !== url[0].url_number) {
        urlObj.push(last_url);
    }
    urlObj.push(url);
    localStorage.setItem("urlObj",JSON.stringify(urlObj));
    localStorage.setItem("url_number_static",url_number_static);*/
    console.log('IgniteSelectCategories Controller loaded');
    $scope.changeLangs = Cookies.get('language');
    $("#content").removeClass('hide');
    $rootScope.showiframe = false;
    // $timeout(function(){
    //     getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
    //     changeLanguage(getLanguage);
    //     $('#chk_lang').change(function() {
    //         getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
    //         changeLanguage(getLanguage);
    //     });
    // },500);
    // function changeLanguage(lang) {
    //     if (lang == "chi") {
    //         $scope.english = false;
    //     } else {
    //         $scope.english = true;
    //     }
    // }
    $rootScope.subheader = '';
    ignitefactory.Getsubjects().then(function (response) {
        if(response.data.status == 1){
            $scope.subjects = response.data.data;

            $rootScope.isBUZZ=$scope.subjects.find(data=>{ return data.id==1 });
            $rootScope.isBUZZRU=$scope.subjects.find(data=>{ return data.id==2 });
            $rootScope.isMAZE=$scope.subjects.find(data=>{ return data.id==3 });
            $rootScope.isSMILE=$scope.subjects.find(data=>{ return data.id==4 });
            $rootScope.isSMILESG=$scope.subjects.find(data=>{ return data.id==7 });
            
            $rootScope.isMAZE=$rootScope.isMAZE?true:false;
            $rootScope.isSMILE=$rootScope.isSMILE?true:false;
            $rootScope.isSMILESG=$rootScope.isSMILESG?true:false;
            $rootScope.isBUZZ=$rootScope.isBUZZ?true:false;
            $rootScope.isBUZZRU=$rootScope.isBUZZRU?true:false;

            $scope.subjectsCount = 12/response.data.data.length;
        }else if(response.data.status == 0){
            $scope.message = response.data.message;
            if(response.data.code==2){
                window.location.replace("ignite-login");
            }
        }
    });
    $scope.selectLanguageForSubject = function (subjectId) {
        $(".imageSubjects").removeClass("image-subjects-select-categories");
        $("#image_"+subjectId).addClass("image-subjects-select-categories");
        ignitefactory.selectLanguageForSubject({'id': subjectId}).then(function (response)
        {
            if(response.data.code==308){
                $scope.seeVideosfor = response.data.data.seeVideosFor;
                if(($scope.oneLanguage = response.data.data.languageForZone.length) == 1) { //For Selecting default language if only 1 language is there
                    $scope.language = response.data.data.languageForZone[0].language;
                } else {
                    $scope.languageForZone = response.data.data.languageForZone;
                }
                $scope.selectLanguage = true;
                $timeout(function(){
                    $(function () {
                      $('[data-toggle="tooltip"]').tooltip();
                      $('#language_tooltip').tooltip('open');
                      $timeout(function(){
                        $('#language_tooltip').tooltip('close');
                      },3000);
                    });
                },500);
                
            }
        });
    }
    $scope.setLanguage = function(language) {
        $scope.language = language;
    }
    $scope.Seevideos = function(language){
        if(language==undefined||language==''||$scope.seeVideosfor==undefined){
            if($scope.seeVideosfor==undefined)
                simpleAlert('error', '', 'Select Sub-categories under categories');
            else
                simpleAlert('error', '', 'Select language in which you would like to see videos');
        } else {
            ignitefactory.Savecategoryandlanguage({'see_videos_in':language,'see_videos_for':$scope.seeVideosfor}).then(function (response) {
                if(response.data.status == 1){
                    ignitefactory.getCategoriesWithLanguages().then(function(response) {
                        if (response.data.status == 1) {
                            $rootScope.categoryLanguages = response.data.data;
                            // $scope.eng = $rootScope.categoryLanguages.find(data=>{ return data.subject_display_name.startsWith('BUZZ') })
                            
                            // ignitefactory.Getuserzone().then(function (response){
                            //     $scope.userZone = response.data.data

                            //     if($scope.userZone.id==1 && $scope.userZone.id==$scope.eng.zones){
                            //         $rootScope.INTL = $scope.userZone.is_subscription_allowed;
                            //         $rootScope.CIS = $scope.userZone.is_subscription_allowed;
                            //         if($scope.userZone.is_subscription_allowed==0){
                            //             window.location.replace("ignite");
                            //         }else{
                            //             window.location.replace('ignite-categories/'+$scope.seeVideosfor);
                            //         }
                            //     }else if($scope.userZone.id==2 && $scope.userZone.id==$scope.eng.zones){
                            //         $rootScope.INTL = $scope.userZone.is_subscription_allowed;
                            //         $rootScope.CIS = $scope.userZone.is_subscription_allowed;
                            //         if($scope.userZone.is_subscription_allowed==0){
                            //             window.location.replace("ignite");
                            //         }else{
                            //             window.location.replace('ignite-categories/'+$scope.seeVideosfor);
                            //         }
                            //     }else{
                            //         $rootScope.Upload_Cat = $scope.userZone.is_subscription_allowed;
                            //         if($scope.userZone.is_subscription_allowed==0){
                            //             window.location.replace("ignite");
                            //         }else{
                            //             window.location.replace('ignite-categories/'+$scope.seeVideosfor);
                            //         }
                            //     }
                            // });
                            window.location.replace('ignite-categories/'+$scope.seeVideosfor);
                        } else {
                            $rootScope.categoryLanguages = response.data;
                        }
                    });
                    // window.location.replace('ignite-categories/'+$scope.seeVideosfor);
                } else {
                    if(response.data.code==2)
                        window.location.replace("ignite-login");
                    else
                        simpleAlert('error', '', response.data.message);
                }
            });
        }
    }
    $scope.subjectName = 'BUZZ'; //subject name (static) to check the subj`aect name and display image accordingly
}]);