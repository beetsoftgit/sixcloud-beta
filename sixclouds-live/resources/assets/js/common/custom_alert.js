/* 
 * Author : Komal Kapadi
 * Date   : 5th Dec 2017
 * Custom common function for sweet alert
 * For this func need to include the weetalert files
 * Reference Doc : https://sweetalert.js.org/docs/#configuration
 */
function simpleAlert(type, title, text, isOkButton, buttonColor = "#8a74d0") {
    swal({
        icon: type,
        title: title,
        text: text,
        showConfirmButton: isOkButton,
        confirmButtonColor: buttonColor,
    });
}

function confirmDialogue(deleteText, DeleteSuccessText, CancelText, callback) {
    if (Cookies.get('language') == 'en'){
        title="Are you sure?";
        confirmButtonText= "Yes, delete it";
        cancelButtonText= "No, cancel please";
    }else{
        title="你确定？";
        confirmButtonText= "是的，删除它";
        cancelButtonText= "不，请取消";
    }

    swal({
        title: title,
        text: deleteText,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText:confirmButtonText,
        cancelButtonText:cancelButtonText,    
        closeOnConfirm: true,
        closeOnCancel: true
    }, function(isConfirm) {
        if (isConfirm) {
            callback();
        } else {
            // swal("Cancelled", CancelText, "error");
        }
    });
}
function confirmDialogueForResumeQuiz(deleteText, DeleteSuccessText, CancelText, callback,cancelCallback) {
    if (Cookies.get('language') == 'en'){
        title="Continue where you left off?";
        confirmButtonText= "Yes";
        cancelButtonText= "No";
    }else{
        title="Continue where you left off?";
        confirmButtonText= "Yes";
        cancelButtonText= "No";
    }

    swal({
        title: title,
        text: '',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText:confirmButtonText,
        cancelButtonText:cancelButtonText,    
        closeOnConfirm: true,
        closeOnCancel: true
    }, function(isConfirm) {
        if (isConfirm) {
            callback();
        } else {
            cancelCallback();
        }
    });
} 
function confirmDialogueDifferentButton(deleteText, DeleteSuccessText, CancelText, callback) {
    if (Cookies.get('language') == 'en'){
        title="Are you sure?";
        confirmButtonText= "Yes";
        cancelButtonText= "No, cancel please";
    }else{
        title="你确定？";
        confirmButtonText= "是的";
        cancelButtonText= "不，请取消";
    }

    swal({
        title: title,
        text: deleteText,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText:confirmButtonText,
        cancelButtonText:cancelButtonText,    
        closeOnConfirm: true,
        closeOnCancel: true
    }, function(isConfirm) {
        if (isConfirm) {
            callback();
        } else {
            // swal("Cancelled", CancelText, "error");
        }
    });
} 
function confirmDialoguePublish(deleteText, ConfirmButtonText, CancelText, callback) {
    swal({
        title: "Are you sure?",
        text: deleteText,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: ConfirmButtonText,
        cancelButtonText: "No, cancel please",
        closeOnConfirm: false,
        closeOnCancel: true
    }, function(isConfirm) {
        if (isConfirm) {
            callback();
        } else {
            // swal("Cancelled", CancelText, "error");
        }
    });
}
function confirmDialogueToAdd(deleteText, DeleteSuccessText, CancelText, callback, confirmButtonText="Yes, add it", cancelButtonText="No, cancel please",title="Are you sure?") {
    swal({
        title: title,
        text: deleteText,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#8a74d0",
        confirmButtonText: confirmButtonText,
        cancelButtonText: cancelButtonText,
        closeOnConfirm: true,
        closeOnCancel: true
    }, function(isConfirm) {
        if (isConfirm) {
            callback();
        } else {
            // swal("Cancelled", CancelText, "error");
        }
    });
}

function confirmDialogueToAddNew(deleteText, DeleteSuccessText, CancelText, callback, confirmButtonText="Proceed", cancelButtonText="Back",title="") {
    swal({
        title: title,
        text: deleteText,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#8a74d0",
        confirmButtonText: confirmButtonText,
        cancelButtonText: cancelButtonText,
        closeOnConfirm: true,
        closeOnCancel: true
    }, function(isConfirm) {
        if (isConfirm) {
            callback();
        } else {
            // swal("Cancelled", CancelText, "error");
        }
    });
}

function confirmDialogueAlert(Title, Text, type, showCancel, confirmButtonColor, confirmButtonText, cancelButtonText, closeOnConfirm, closeOnCancel, CancelText, callback) {
    swal({
        title: Title,
        text: Text,
        type: type,
        showCancelButton: showCancel,
        confirmButtonColor: confirmButtonColor,
        confirmButtonText: confirmButtonText,
        cancelButtonText: cancelButtonText,
        closeOnConfirm: closeOnConfirm,
        closeOnCancel: closeOnCancel
    },
    function (isConfirm) {
        if (isConfirm) {
            callback();
        } else {
            swal("Cancelled", CancelText, "error");
        }
    });
}

function showLoader(selector,text='Please wait') {
    if(text!=''){
        if(Cookies.get('language') == 'chi'){
            text='请稍候';
        }   
    }
        
    $(selector).text(' ');
    $(selector).append("<i class='fa fa-spinner fa-spin'></i> "+text);
    $(selector).attr("disabled", "disabled");
}

function hideLoader(selector, text) {
    $(selector).text(' ');
    $(selector).append(text);
    $(selector).removeAttr("disabled");
}

function hideLoaderWithLabel(selector) {
    $(selector).removeAttr("disabled");
}

function Disable(selector) {
    $(selector).attr("disabled", "disabled");
}

function Enable(selector) {
    $(selector).removeAttr("disabled");
}