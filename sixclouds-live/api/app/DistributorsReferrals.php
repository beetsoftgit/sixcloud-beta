<?php

//###############################################################
//File Name : DistributorsReferrals.php
//Author : Ketan Solanki <ketan@creolestudios.com>
//Purpose : Model file for the table `distributor_referrals`
//Date : 19th July 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class DistributorsReferrals extends Model
{
    protected $table = 'distributor_referrals';
    public function distributor_member()
    {
        return $this->belongsTo('App\DistributorsTeamMembers', 'distributor_team_member_id')->with('distributor');
    }
    public function referal_user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    public function distributor()
    {
        return $this->belongsTo('App\Distributors', 'distributors_id');
    }
    public function salesperson()
    {
        return $this->belongsTo('App\DistributorsTeamMembers', 'distributor_team_member_id');
    }
}
