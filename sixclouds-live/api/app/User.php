<?php

namespace App;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{

    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $table = 'users';
    public $rules    = array(
        'first_name'    => 'required',
        'last_name'     => 'required',
        'email_address' => 'required|email',
        'password'      => 'required',
        'country_id'    => 'required',
        'city_id'       => 'required',
    );    
    public function country()
    {
        return $this->hasOne('App\Country', 'id', 'country_id');
    }   
    public function city()
    {
        return $this->belongsTo('App\City', 'city_id')
            ->with('state');
    }
    public function countryadmin()
    {
        return $this->belongsTo('App\Country', 'country_id');
    }
}