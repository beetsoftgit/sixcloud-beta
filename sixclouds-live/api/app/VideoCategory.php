<?php

//###############################################################
//File Name : Countries.php
//Author : Komal Kapadi <komal@creolestudios.com>
//Purpose : to get list of countries
//Date : 5th Dec 2017
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Http\Controllers\UtilityController;

use URL;

class VideoCategory extends Model {

    public $rules = array(
        'category_name' => 'required'
    );
    protected $table = 'video_categories';

    public function videos() {
        return $this->hasMany('App\Videos', 'video_category_id');
    }

    public function quizzes() {
        return $this->hasMany('App\Quizzes', 'category_id');
    }

    public function worksheets() {
        return $this->hasMany('App\Worksheets', 'worksheet_category_id');
    }

    public function subjectName() {
        return $this->belongsTo('App\Subject', 'subject_id')->select('id','subject_display_name');
    }

    public function videos_of_subcategory() {
        return $this->hasMany('App\Videos', 'subcategory_id')
                    ->with([
                        'video_urls' => function($query) 
                        { 
                            $query->select('id','video_id','video_duration','video_language', 'video_language_id','video_oss_url', 'video_oss_url_mp4'); 
                        } 
                    ]);
    }

    public function quizzes_of_subcategory() {
        return $this->hasMany('App\Quizzes', 'subcategory_id');
    }

    public function worksheets_of_subcategory() {
        return $this->hasMany('App\Worksheets', 'subcategory_id')
                    ->select('*','thumbnail_160_90 AS worksheet_image','download_button_text AS worksheet_button_text')
                    ->with([
                        'category' => function($query) 
                        {
                            $query->select('id','category_name');   
                        }
                    ]);
    }

    public function subcategories() {
        return $this->hasMany('App\VideoCategory', 'parent_id')->with([
                        'videos_of_subcategory' => function($query) 
                        {
                            $query->where('status',1);   
                        },'quizzes_of_subcategory' => function($query) 
                        {
                            $query->where('status',1);   
                        },'worksheets_of_subcategory' => function($query) 
                        {
                            $query->where('status',1);   
                        }
                    ])->select('id','parent_id','subcategory_name','subcategory_name_chi','subcategory_name_ru','on_detail_page')->whereNotNull('parent_id')->where('on_detail_page',1);
    }

    public function getCategoryGifUrlAttribute($category_gif_url)
    {
        // $url = preg_replace('/\/[a][p][i]$/i', '', URL::to('/'));
        $url = "https://sixclouds.net/main";
        if($category_gif_url !== null) {
            return ($url.UtilityController::Getpath('GIF_URL').$category_gif_url);
        } else {
            return ($url.UtilityController::Getpath('GIF_URL').'Maze 1.gif');
        }
    }
}
