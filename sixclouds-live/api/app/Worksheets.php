<?php

//###############################################################
//File Name : Countries.php
//Author : Komal Kapadi <komal@creolestudios.com>
//Purpose : to get list of countries
//Date : 5th Dec 2017
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\VideoNotes;
use App\VideoCategory;
use App\WorksheetDownloads;

class Worksheets extends Model {

    protected $table = 'worksheets';

    public $rules = array(
        'worksheet_category_id' => 'required',
        'title'                 => 'required',
        'description'           => 'required',
        'worksheet_ordering'    => 'required',
        'status'                => 'required',
    );

    public function category() {
        return $this->belongsTo('App\VideoCategory', 'worksheet_category_id');
    }

    public function worksheet_downloads() {
        return $this->belongsTo('App\WorksheetDownloads', 'id','worksheet_id');
    }    
}
