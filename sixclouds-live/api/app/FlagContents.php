<?php

//###############################################################
//File Name : FlagContents.php
//Author : Nivedita Mitra <nivedita@creolestudios.com>
//Purpose : related to flaging the content from the app
//Date : 7th Sept 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class FlagContents extends Model
{
    protected $table = 'ignite_flag_content';
}
