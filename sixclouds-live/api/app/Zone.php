<?php

//###############################################################
//File Name : Zone.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : to get list of languages for creating zones
//Date : 28th Jan, 2019
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
    protected $table  = 'zones';
    //Added By ketan Solanki for coiuntry list with States and Cities
    public function states() {
        return $this->hasMany('App\State', 'country_id');
    }

}
