<?php

//###############################################################
//File Name : Countries.php
//Author : Komal Kapadi <komal@creolestudios.com>
//Purpose : to get list of countries
//Date : 5th Dec 2017
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model {

    protected $table = 'cities';

    public function state() {
        return $this->belongsTo('App\State', 'state_id');
    }

}
