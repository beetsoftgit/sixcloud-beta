<?php

//###############################################################
//File Name : MarketPlaceGalleryPurchase.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related to details of purchase made from gallery 
//Date : 19th Mar, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketPlaceGalleryPurchase extends Model
{
    protected $table  = 'mp_gallery_purchases'; 
    public $rules = array(
        'user_id'       => 'required',
        'mp_upload_id' => 'required',
        'gallery_purchase_date' => 'required',
        'gallery_purchase_amount' => 'required',
        'gallery_purchased_file_path' => 'required',
    ); 
}