<?php

//###############################################################
//File Name : MarketPlaceDesignerSuggestionProject.php
//Author : Nivedita <nivedita@creolestudios.com>
//Purpose : related to inserting log of the suggested designer in matching algo
//Date : 16th April, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketPlaceDesignerSuggestionProject extends Model
{
    protected $table = 'mp_designer_suggestion_projects';

}
