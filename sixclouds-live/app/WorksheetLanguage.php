<?php

//###############################################################
//File Name : WorksheetLanguage.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : Worksheet's title and description for different languages
//Date : 25th Feb, 2019
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorksheetLanguage extends Model
{
    protected $table  = 'worksheet_languages';
    
    public function worksheet() {
        return $this->hasOne('App\Worksheets', 'id','worksheet_id');
    }

}
