<?php

//###############################################################
//File Name : ZoneVideo.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : Pivot model between zone and video
//Date : 1st Feb, 2019
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class ZoneVideo extends Model
{
    protected $table  = 'zone_videos';
    //Added By ketan Solanki for coiuntry list with States and Cities
    public function videos() {
        return $this->hasOne('App\Videos', 'id','video_id');
    }

}
