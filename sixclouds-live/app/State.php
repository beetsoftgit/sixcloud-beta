<?php

//###############################################################
//File Name : Countries.php
//Author : Komal Kapadi <komal@creolestudios.com>
//Purpose : to get list of countries
//Date : 5th Dec 2017
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model {

    protected $table = 'states';

    public function cities() {
        return $this->belongsTo('App\City', 'state_id');
    }

    public function hascities() {
        return $this->hasMany('App\City', 'state_id');
    }


}
