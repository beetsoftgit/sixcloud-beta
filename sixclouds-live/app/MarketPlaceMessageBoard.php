<?php

//###############################################################
//File Name : MarketPlaceProject.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related to projects created by buyer from front end
//Date : 19th Mar, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketPlaceMessageBoard extends Model
{
    protected $table = 'mp_message_board';
    public $rules    = array(
        'user_id'       => 'required',
        'mp_project_id' => 'required',
        // 'message'       => 'required',
        'user_role'     => 'required',
    );

    public function user_details()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function message_board()
    {
        return $this->hasMany('App\MarketPlaceMessageBoard','group_with','id');
    }
}
