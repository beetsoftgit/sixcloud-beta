<?php

//###############################################################
//File Name : MarketPlaceProjectsDetailInfo.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related to projects assigned to details
//Date : 19th Mar, 2018
//###############################################################

namespace App;
use Auth;


use Illuminate\Database\Eloquent\Model;

class MarketPlaceProjectsDetailInfo extends Model
{
    protected $table = 'mp_projects_detail_info';
    public $rules    = array(
        'mp_project_id'        => 'required',
        'assigned_designer_id' => 'required',
    );
    
    public function project_details(){
        return $this->hasOne('App\MarketPlaceProject', 'id', 'mp_project_id')->with('projectCreator');
    }

    public function project_for_portfolio(){
        return $this->hasOne('App\MarketPlaceProject', 'id', 'mp_project_id')->with('message_board_for_portfolio','skills');
    }
    public function designer_details(){
    	return $this->hasOne('App\User', 'id', 'assigned_designer_id')->with('ratings','reviews');
    }
    public function projectdetail()
    {
        return $this->hasOne('App\MarketPlaceProject', 'id', 'mp_project_id');
    }
    public function project_details_designer(){
        return $this->hasOne('App\MarketPlaceProject', 'id', 'mp_project_id')->where('project_status',3)->with('projectCreator','rating_for_project','message_board','review_for_project');
    }
    public function project_details_with_skill()
    {
        return $this->hasOne('App\MarketPlaceProject', 'id', 'mp_project_id')->where('project_created_by_id',Auth::user()->id)->with('skill_detail');   
    } 
    public function project_details_with_skill_detail()
    {
        return $this->hasOne('App\MarketPlaceProject', 'id', 'mp_project_id')->with('projectCreator','skill_detail','rating_for_project','review_for_project');   
    }  
    public function payment_details()
    {
        return $this->hasOne('App\MarketPlacePayment', 'mp_project_id', 'mp_project_id');
    }
    public function designer_details_admin(){
        return $this->hasOne('App\User', 'id', 'assigned_designer_id')->with('ratings','reviews','services');
    }
    public function project_details_designer_admin(){
        return $this->hasOne('App\MarketPlaceProject', 'id', 'mp_project_id')->whereIn('project_status',array(3,10))->with('projectCreator','rating_for_project','message_board','review_for_project');
    }

}
