<?php

//###############################################################
//File Name : CustomerSupport.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related to issue generated in customer support
//Date : 19th Mar, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerSupport extends Model
{
    protected $table  = 'customer_support'; 
    public $rules = array(
        'ticket_type_id'     => 'required',
        'ticket_subject'     => 'required',
        'ticket_description' => 'required',
    );

    public function customer_ticket_type()
    {
        return $this->hasOne('App\TicketType','id','ticket_type_id');
    }
    public function customer_ticket_category()
    {
        return $this->hasOne('App\TicketCategory','id','ticket_category_id');
    }
}