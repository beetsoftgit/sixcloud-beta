<?php

//###############################################################
//File Name : MarketPlaceInspirationBankUserFavourite.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related to projects created by buyer from front end
//Date : 13th Apr, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketPlaceInspirationBankUserFavourite extends Model
{
    protected $table = 'mp_inspirationbank_user_favourites';
    
}
