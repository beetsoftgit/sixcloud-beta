<?php

//###############################################################
//File Name : State.php
//Author : Komal Kapadi <komal@creolestudios.com>
//Purpose : to get list of states
//Date : 6th Dec 2017
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table  = 'proviances';
}
