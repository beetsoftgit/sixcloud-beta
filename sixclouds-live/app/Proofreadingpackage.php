<?php

//###############################################################
//File Name : Proofreadingpackage.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related to PR cases package information
//Date : 7th Dec 2017
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proofreadingpackage extends Model
{
    protected $table  = 'proof_reading_packages';
}