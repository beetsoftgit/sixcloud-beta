<?php

//###############################################################
//File Name : ZoneQuiz.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : Pivot model between zone and quiz
//Date : 12th Feb, 2019
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class ZoneQuiz extends Model
{
    protected $table  = 'zone_quizzes';
    
    public function quizzes() {
        return $this->hasOne('App\Quizzes', 'id','quiz_id');
    }

}
