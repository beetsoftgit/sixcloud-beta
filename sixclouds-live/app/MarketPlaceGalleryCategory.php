<?php

//###############################################################
//File Name : MarketPlaceGalleryCategory.php
//Author : Komal Kapadi <komal@creolestudios.com>
//Purpose : to get gallery's category list
//Date : 5th Dec 2017
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketPlaceGalleryCategory extends Model
{

    protected $table = 'mp_gallery_category';

}
