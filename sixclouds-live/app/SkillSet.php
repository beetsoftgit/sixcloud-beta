<?php

//###############################################################
//File Name : SkillSet.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related to skill sets i.e category for market place
//Date : 19th Mar, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class SkillSet extends Model
{
    protected $table = 'skill_sets';
    public $rules    = array(
        'skill_name' => 'required',
    );

    public function adminskills()
    {
        return $this->hasMany('App\ProjectSkill', 'skill_set_id')->whereHas('projectskills', function ($query) {
                $query->where('project_status', 3);
            });
    }
    public function skillsbudget()
    {
        return $this->hasMany('App\ProjectSkill', 'skill_set_id')->with([
            'projectskills' => function ($query) {
                $query->select('*')->where('project_status', 3);
            },
        ]);
    }

    /*public function project_extras()
    {
        return $this->hasManyThrough('App\MarketPlaceSellerServiceSkill','App\SkillSet','id','skill_id')->where('mp_seller_service_skills.status',1)->join('mp_seller_service_packages','mp_seller_service_skills.id','mp_seller_service_packages.mp_seller_service_skill_id')->join('mp_seller_service_project_extras','mp_seller_service_packages.id','mp_seller_service_project_extras.mp_seller_service_pacakage_id')->select('mp_seller_service_project_extras.*');
    }

    public function package()
    {
        return $this->hasManyThrough('App\MarketPlaceSellerServiceSkill','App\SkillSet','id','skill_id')->where('mp_seller_service_skills.status',1)->join('mp_seller_service_packages','mp_seller_service_skills.id','mp_seller_service_packages.mp_seller_service_skill_id')->select('mp_seller_service_packages.service_title');
    }*/
}
