<?php

//###############################################################
//File Name : SphereSubjects.php
//Author : Jainam Shah
//Date : 22nd March, 2019
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class SphereSubjects extends Model
{
    protected $table  = 'sphere_subjects';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'subject_name',
    ];
}