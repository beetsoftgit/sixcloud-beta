<?php

//###############################################################
//File Name : MarketPlaceSellerServiceProjectExtra.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related seller service package extra revisions
//Date : 22nd May, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketPlaceSellerServiceProjectExtra extends Model
{
    protected $table  = 'mp_seller_service_project_extras'; 
    public $rules = array(
        'mp_seller_service_pacakage_id' => 'required',
        'no_of_revisions'               => 'required|numeric',
        'delivery_time_in_days'         => 'required|numeric|min:3',
        'price'                         => 'required|numeric',
        'status'                        => 'required',
    );

    public function package_detail()
    {
        return $this->hasOne('App\MarketPlaceSellerServicePackage', 'id', 'mp_seller_service_pacakage_id');
    }
}