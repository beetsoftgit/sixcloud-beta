<?php

//###############################################################
//File Name : Countries.php
//Author : Komal Kapadi <komal@creolestudios.com>
//Purpose : to get list of countries
//Date : 5th Dec 2017
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\VideoCategory;

class Quizzes extends Model
{
    protected $table = 'quizzes';
    public function category()
    {
        return $this->belongsTo('App\VideoCategory', 'category_id');
    }

    public function categoryWithSubject()
    {
        return $this->belongsTo('App\VideoCategory', 'category_id')->select('id', 'subject_id')->with('subjectZones');
    }

    public function quizquestions() {
        return $this->hasMany('App\QuizQuestions', 'quiz_id')->where('question_status',1);
    }

    public function total_questions() {
        return $this->hasMany('App\QuizAttempts', 'quiz_id');
    }

    public function correct_answers() {
        return $this->hasMany('App\QuizAttempts', 'quiz_id')->where('is_correct',1);
    }

    public function quiz_attempt() {
        return $this->hasOne('App\QuizAttempts', 'quiz_id');
    }
    public function quiz_attempt_highest() {
        return $this->hasMany('App\QuizAttempts', 'quiz_id');
    }
    public function zones() {
        return $this->hasOne('App\ZoneQuiz', 'quiz_id','id');
    }
    public function quizlanguages() {
        return $this->hasMany('App\QuizLanguage', 'quiz_id');
    }
    public function quizlanguage() {
        return $this->hasOne('App\QuizLanguage', 'quiz_id');
    }
    public function subcategoryWithOnlyName() {
        return $this->belongsTo('App\VideoCategory', 'subcategory_id')->select('id','subcategory_name','subcategory_name_chi','subcategory_name_ru');
    }
    public function categoryWithOnlyName() {
        return $this->belongsTo('App\VideoCategory', 'category_id')->select('id','category_name','category_name_chi','category_name_ru');
    }
    public function alreadyattemptedquiz()
    {
        return $this->hasmany('App\QuizAttempts', 'quiz_id');
    }
    
}
