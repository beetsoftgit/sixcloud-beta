<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoViewsHistory extends Model
{
    protected $table = 'video_views_history';

    public $rules = array(
        'video_views_id'	=> 'required',
    );
}
