<?php

//###############################################################
//File Name : MarketPlaceDesignerSignupInvitation.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related to inviation sent for designer signup
//Date : 8th May, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketPlaceDesignerSignupInvitation extends Model
{
    protected $table  = 'mp_designer_signup_invitation'; 
    public $rules = array(
        'invited_by'      => 'required',
        'email_address'   => 'required|email',
        'invited_by_type' => 'required',
    ); 
}