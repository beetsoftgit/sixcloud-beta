<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserEmailLog extends Model
{
    protected $table = 'mp_email_log';
}
