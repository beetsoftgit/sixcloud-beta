<?php

//###############################################################
//File Name : Userprcase.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related to PR cases
//Date : 6th Dec 2017
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prcaseassignment extends Model
{
    protected $table  = 'pr_case_assignments';

    public function case_assignment(){
    	return $this->hasMany('App\Prcaseassignment', 'user_pr_case_id', 'user_pr_case_id');
    }
    public function pr_case(){
    	return $this->hasOne('App\Userprcase', 'id', 'user_pr_case_id');
    }
}