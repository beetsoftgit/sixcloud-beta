<?php

//###############################################################
//File Name : MarketPlacePayment.php
//Author : Nivedita <nivedita@creolestudios.com>
//Purpose : related to MP alipay payemnt
//Date : 9th May, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;
use Carbon\carbon;

class SphereCourse extends Model
{
    // protected $table  = 'mp_payments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'course_name', 'grade_id', 'course_price', 'total_sesions', 'course_type', 'total_ratings', 'course_description', 'status', 'deleted_at','created_at', 'updated_at'
    ];

    // public function projectpaymentdetailinfo()
    // {
    //     return $this->hasOne('App\MarketPlaceProject', 'id', 'mp_project_id')->with('selected_designer','skills');
    // }
    
    // protected $appends = ['course_details'];
    
    // function getCourseDetailsAttribute() {
    //   return $this->hasOne('App\SphereCourse', 'id', 'course_id');
    // }
}

