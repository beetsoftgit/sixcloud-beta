<?php

//###############################################################
//File Name : MarketPlaceUpload.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related to files uploaded by designer for inspiration bank or gallery or portfolio
//Date : 19th Mar, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketPlaceUpload extends Model
{
    protected $table = 'mp_uploads';
    public $rules    = array(
        'uploaded_user_id'   => 'required',
        'upload_title'       => 'required',
        'upload_description' => 'required',
        'upload_of'          => 'required',
    );

    public function uploadedby()
    {
        return $this->hasOne('App\User', 'id', 'uploaded_user_id');
    }
    public function files()
    {
        return $this->hasMany('App\MarketPlaceGalleryFiles', 'id');
    }
    public function gallerypurchases()
    {
        return $this->hasMany('App\MarketPlaceGalleryPurchases', 'id');
    }

    public function attachments()
    {
        return $this->hasMany('App\MarketPlaceUploadAttachment', 'mp_upload_id')->where('status',1);
    }
    public function cover_image_attachments()
    {
        return $this->hasMany('App\MarketPlaceUploadAttachment', 'mp_upload_id')->where('status',3);
    }

    public function inspiration_bank_files_attachments()
    {
        return $this->hasMany('App\MarketPlaceUploadAttachment', 'mp_upload_id')->where('status',4);
    }

    public function portfolio_categories()
    {
        return $this->hasMany('App\MarketPlacePortfolioCategoryDetail', 'mp_upload_id')->with('category_name');
    }
    public function inspiration_categories()
    {
        return $this->hasMany('App\MarketPlaceInspirationBankCategoryDetail', 'mp_upload_id')->with('category_name');
    }
    public function uploadedMpDesignBy()
    {
        return $this->hasOne('App\User', 'id', 'uploaded_user_id');
    }

    public function user_favourite()
    {
        return $this->hasMany('App\MarketPlaceInspirationBankUserFavourite', 'mp_upload_id');
    }

}
