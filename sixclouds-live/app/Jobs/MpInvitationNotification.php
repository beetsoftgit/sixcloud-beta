<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use App\MarketPlaceProject;
use App\MarketPlaceProjectsDetailInfo;
use URL;

class MpInvitationNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $mp_project_id;
    protected $after_hours;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($mp_project_id,$after_hours)
    {
        $this->mp_project_id = $mp_project_id;
        $this->after_hours = $after_hours;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            $mp_project_id = $this->mp_project_id;
            $after_hours=$this->after_hours;

            $project = MarketPlaceProjectsDetailInfo::with('project_details','designer_details')->where('id',$mp_project_id)->first();
            
            $project = json_decode(json_encode($project),true);

            if($project['status'] == 4){

                $data['projectInfo'] = MarketPlaceProject::where('slug',$project['project_details']['slug'])->first();
               
                 
                $data['projectInfo'] = json_decode(json_encode($data['projectInfo']),true);
                $data['designer']['first_name']       = $project['designer_details']['first_name'];
                $data['designer']['last_name']        = $project['designer_details']['last_name'];
                $data['designer']['email_address']    = $project['designer_details']['email_address'];
                $data['designer']['current_language'] = $project['designer_details']['current_language'];
                $data['designer']['after_hours']      = $after_hours;
                
                $data['subject']=$data['designer']['current_language']==1?'Project Assigned':'项目已分配';
                $data['footer_content']=$data['designer']['current_language']==1?'From,<br /> SixClouds Sixteen':'六云十六 ';
                $data['footer']=$data['designer']['current_language']==1?'SixClouds':'六云';

                if($after_hours==12){
                    $data['content']=$data['designer']['current_language']==1?'
                        Hi, you have a Project pending your acceptance. <br/><br/>Click <a href="'.URL::to('/seller-project-info-invitation/'.$data['projectInfo']['slug']).'">here</a> to review the Project brief.
                        <br><br>
                        You have less than 12 hours to indicate your acceptance.

                        <br><br>
                        Don’t miss out on this opportunity！':'

                        您好，您有一个项目等待接受。<br><br>请点 <a href="'.URL::to('/seller-project-info-invitation/'.$data['projectInfo']['slug']).'">击此</a>处查看项目简介。<br><br>您有少过12小时来表明你的接受。不要错过这个机会！';
                }
                else if($after_hours==18){
                    $data['content']=$data['designer']['current_language']==1?'
                        Hi, you have a Project pending your acceptance.<br/><br/>Click <a href="'.URL::to('/seller-project-info-invitation/'.$data['projectInfo']['slug']).'">here</a> to review the Project brief.
                        <br><br>
                        You have less than 6 hours to indicate your acceptance.

                        <br><br>
                        Don’t miss out on this opportunity！':'

                        您好，您有一个项目等待接受。<br><br>请点 <a href="'.URL::to('/seller-project-info-invitation/'.$data['projectInfo']['slug']).'">击此</a>处查看项目简介。<br><br>您有少过6小时来表明你的接受。不要错过这个机会！';
                }
                else if($after_hours==24){
                    $data['content']=$data['designer']['current_language']==1?'
                        Oh no! You have missed out a great chance to showcase your talents. We will notify you if another opportunity arises in the future.
                        <br><br>':'

                        糟了！您已错过了展示自己才华的绝佳机会。 如果再有机会，我们会通知您';
                }
                Mail::send('emails.email_template', $data, function ($message) use ($data) {
                        $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                        $message->to($data['designer']['email_address'])->subject($data['subject']);
                });
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = \App\Http\Controllers\UtilityController::Sendexceptionmail($e);
        }
    }
}
