<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use App\MarketPlaceProject;
use App\MarketPlaceProjectsDetailInfo;
use URL;

class MpInvitationNotAccepted implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $mp_project_id;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($mp_project_id)
    {
        $this->mp_project_id = $mp_project_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    { 
        try{
            $mp_project_id = $this->mp_project_id;
            $project = MarketPlaceProjectsDetailInfo::with('project_details','designer_details')->where('id',$mp_project_id)->orderBy('id','DESC')->first();
            $project = json_decode(json_encode($project),true);
            if($project['status'] == 4){
                \DB::beginTransaction();
                $data['buyer']['first_name']       = $project['project_details']['project_creator']['first_name'];
                $data['buyer']['last_name']        = $project['project_details']['project_creator']['last_name'];
                $data['buyer']['email_address']    = $project['project_details']['project_creator']['email_address'];
                $data['buyer']['current_language'] = $project['project_details']['project_creator']['current_language'];
                $data['buyer']['project_title']    = $project['project_details']['project_title'];
                $data['buyer']['status']           = 5;

                $data['designer']['first_name']       = $project['designer_details']['first_name'];
                $data['designer']['last_name']        = $project['designer_details']['last_name'];
                $data['designer']['email_address']    = $project['designer_details']['email_address'];
                $data['designer']['current_language'] = $project['designer_details']['current_language'];
                $updateStatus = \App\Http\Controllers\UtilityController::Makemodelobject($data['buyer'],'MarketPlaceProjectsDetailInfo','',$mp_project_id);

                \DB::commit();
                if($updateStatus){
                    $data['project_number']=$project['project_details']['project_number'];

                    $data['subject']=$data['buyer']['current_language']==1?'Regarding: No action taken by selected designer for data':'关于：选定的设计师没有对项目采取任何行动';

                    $data['content']=$data['buyer']['current_language']==1?'Hello <strong>'.$data['buyer']['first_name'].' '.$data['buyer']['last_name'].' ,
                        </strong><br/>
                        <br/>
                        Thank you for posting a Project <strong>ID: '.$data['project_number'].' </strong> 
                        <br/><br/>Your selected Seller is unable to take on your Project.<br/><br/>
                        Please click <a href="'.URL::to('/suggested-sellers/'.$project['project_details']['slug']).'">here</a> to select another Seller.

                        ':'你好 <strong>'.$data['buyer']['first_name'].' '.$data['buyer']['last_name'].'</strong> ,
                        <br/>
                        <br/>
                        感谢您发布项目  <strong>ID: '.$data['project_number'].'</strong> ! <br/><br/>您选择的卖家无法执行您的项目。<br/><br/>请点击<a href="'.URL::to('/suggested-sellers/'.$project['project_details']['slug']).'">此处</a>选择另一个卖家。

                            <br/><br/>';

                    $data['footer_content']=$data['buyer']['current_language']==1?'From,<br /> SixClouds Sixteen':'六云十六 ';

                    $data['footer']=$data['buyer']['current_language']==1?'SixClouds':'六云';

                    Mail::send('emails.email_template', $data, function ($message) use ($data) {
                            $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                            $message->to($data['buyer']['email_address'])->subject($data['subject']);
                    });

                    /*Mail::send('emails.notify_seller_for_assigned_project_no_action', $data, function ($message) use ($data) {
                            $message->from('noreply@sixclouds.cn', 'SixClouds');
                            $message->to($data['designer']['email_address'])->subject('Regarding: No action taken by you for project');
                    });*/
                } else {
                    \DB::rollback();
                }
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = \App\Http\Controllers\UtilityController::Sendexceptionmail($e);
        }
    }
}
