<?php

namespace App;

use DB;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $table = 'users';
    public $rules    = array(
        'first_name'    => 'required',
        'last_name'     => 'required',
        'email_address' => 'required',
        'password'      => 'required',
        'country_id'    => 'required',
        'city_id'       => 'required',
    );

    public function ratings()
    {
        return $this->hasMany('App\MarketPlaceDesignerRating', 'rating_to_id');
    }
    public function reviews()
    {
        return $this->hasMany('App\MarketPlaceDesignerReview', 'review_to_id');
    }
    public function ratings_by()
    {
        return $this->hasMany('App\MarketPlaceDesignerRating', 'rating_by');
    }
    public function reviews_by()
    {
        return $this->hasMany('App\MarketPlaceDesignerReview', 'review_by');
    }
    public function marketplaceproject()
    {
        return $this->hasMany('App\MarketPlaceProject', 'project_created_by_id')->with('selected_designer', 'rating_for_project', 'review_for_project');
    }
    public function project_detail()
    {
        return $this->hasOne('App\MarketPlaceProject', 'project_created_by_id')->with('selected_designer', 'rating_for_project', 'review_for_project');
    }
    public function skills()
    {
        return $this->hasMany('App\DesignerSkill', 'user_id')->with('skill_details', 'ratings', 'reviews');
    }
    /*end*/
    public function completed_projects()
    {
        return $this->hasMany('App\MarketPlaceProjectsDetailInfo', 'assigned_designer_id', 'id')->with('project_details');
    }
    public function school()
    {
        return $this->hasOne('App\School', 'id', 'school_id');
    }
    public function country()
    {
        return $this->hasOne('App\Country', 'id', 'country_id');
    }
    /*public function designer_projects(){
    return $this->hasMany('App\MarketPlaceProjectsDetailInfo', 'assigned_designer_id', 'id')->with('project_details_designer')->where('status',7);
    }*/

    public function inspirationBankDesigns()
    {
        return $this->hasMany('App\MarketPlaceUpload', 'uploaded_user_id');
    }
    public function seller_unavailability()
    {
        return $this->hasMany('App\MarketPlaceSellerUnavailability', 'user_id');
    }
    public function payment()
    {
        return $this->hasMany('App\MarketplaceSellerPayments', 'user_id');
    }
    public function services()
    {
        return $this->hasMany('App\MarketPlaceSellerServiceSkill', 'seller_id')->where('status', 1)->with('service_details');
    }
    public function service_title()
    {
        return $this->hasManyThrough('App\MarketPlaceSellerServiceSkill','App\User','id','seller_id')->where('mp_seller_service_skills.status',1)->join('skill_sets','mp_seller_service_skills.skill_id','skill_sets.id')->where('skill_sets.status',1)->select('skill_name');
    }

    public function images()
    {
        return $this->hasManyThrough('App\MarketPlaceSellerServiceSkill','App\User','id','seller_id')->where('mp_seller_service_skills.status',1)->join('mp_seller_service_packages','mp_seller_service_skills.id','mp_seller_service_packages.mp_seller_service_skill_id')->where('mp_seller_service_packages.status',1)->join('mp_seller_service_images','mp_seller_service_packages.id','mp_seller_service_images.mp_seller_service_pacakage_id')->where('mp_seller_service_images.status',1)->select('mp_seller_service_images.*');
    }

    /***********************   admin      ************************/

    public function city()
    {
        return $this->belongsTo('App\City', 'city_id')
            ->with('state');
    }
    public function countryadmin()
    {
        return $this->belongsTo('App\Country', 'country_id');
    }
    public function adminmarketplaceprojectdetailinfo()
    {
        return $this->hasMany('App\MarketPlaceProjectsDetailInfo', 'assigned_designer_id');
    }
    public function adminmarketplaceprojectdetailinfowithstatus()
    {
        return $this->hasMany('App\MarketPlaceProjectsDetailInfo', 'assigned_designer_id')->where('status', 1);
    }
    public function rating()
    {
        return $this->hasMany('App\MarketPlaceDesignerRating', 'rating_to_id')
            ->selectRaw('cast(avg(rating) AS DECIMAL (10,2)) as data1 , rating_to_id')
            ->groupBy('rating_to_id');
    }
    public function adminskills()
    {
        return $this->hasMany('App\DesignerSkill', 'user_id');
    }

    public function adminprojects()
    {
        return $this->hasMany('App\MarketPlaceProject', 'project_created_by_id')->with('adminprojectdetailinfobuyer');
    }
    public function project_creator_payment()
    {
        return $this->hasMany('App\MarketPlacePayment', 'paid_by');
    }
    public function userpayments()
    {
        // return $this->hasMany('App\MarketPlacePayment', 'paid_by')->select('id', 'paid_by', DB::raw("SUM(alipay_total_amount_paid) as total_amount_spend"));
        return $this->hasMany('App\MarketPlacePayment', 'paid_by')->select('id', 'paid_by', DB::raw("SUM(payment_amount) as total_amount_spend"));
    }

    public function sumofratings()
    {
        return $this->hasMany('App\MarketPlaceDesignerRating', 'rating_to_id')->select('id', 'rating_to_id', 'rating');
    }

    public function admin_completed_projects()
    {
        return $this->hasMany('App\MarketPlaceProjectsDetailInfo', 'assigned_designer_id', 'id')->whereIn('status', array(7, 14));
    }

    public function subscriber_country(){
        return $this->belongsTo('App\Country', 'country_id')->select('id', 'name');
    }
    public function subscriber_state(){
        return $this->belongsTo('App\State', 'provience_id')->select('id', 'name');
    }
    public function subscriber_city(){
        return $this->belongsTo('App\City', 'city_id')->select('id', 'name');
    }

    public function distributorRefer(){
        return $this->hasOne('App\DistributorsReferrals', 'user_id', 'id')->with('distributor', 'salesperson');
    }

    public function view_views()
    {
        return $this->hasMany('App\VideoViews', 'user_id', 'id');
    }
    public function quiz_attempts()
    {
        return $this->hasMany('App\QuizAttempts', 'user_id', 'id');   
    }
    public function worksheet_download()
    {
        return $this->hasMany('App\WorksheetDownloads', 'user_id', 'id');
    }
    public function transactions()
    {
        return $this->hasMany('App\MarketPlacePayment', 'paid_by','id');
    }
}
