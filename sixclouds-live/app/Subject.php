<?php

//###############################################################
//File Name : Subject.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : Subjects model
//Date : 30th Jan, 2019
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $table  = 'subjects';
    //Added By ketan Solanki for coiuntry list with States and Cities
    public function categories() {
        return $this->hasMany('App\VideoCategory', 'subject_id');
    }

}
