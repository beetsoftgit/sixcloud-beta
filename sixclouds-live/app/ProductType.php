<?php

//###############################################################
//File Name : ProductType.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related to ticket types for customer support
//Date : 19th Mar, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{
    protected $table  = 'customer_support_products';
    
}

