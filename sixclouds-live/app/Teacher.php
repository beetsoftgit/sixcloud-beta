<?php

//###############################################################
//File Name : Teacher.php
//Author : Jainam Shah
//Date : 15th March, 2019
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Teacher extends Model
{
    protected $table  = 'teachers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'country_id', 'city_id', 'provience_id', 'education_id', 'email_address', 'first_name', 'last_name', 'display_name', 'dob', 'phonecode', 'contact', 'employment_status', 'teaching_experience', 'teaching_experience_duration', 'adhoc_session_availability', 'id_proof', 'graduation_certificate', 'profile_image', 'residence_proof', 'last_employment_proof', 'additional_certificates', 'introduction_video', 'trial_session_video', 'slug',
    ];

    /**
     * The list of fields which will be appended.
     *
     * @var array
     */
    protected $appends = ['display_image'];



    /**
     * Accessor for display_image attribute.
     *
     * @return returnType
     */
    public function getDisplayImageAttribute($value)
    {
        $user_image = $this->profile_image;
        $imageUrl   = url('/resources/assets/images/user-img.png');
        if (!$user_image) {
            return $imageUrl;
        }
        if ($this->isUrl($user_image)) {
            return $user_image;
        }
        if (file_exists(base_path('/resources/assets/images/') . $user_image)) {
            $imageUrl = url('/resources/assets/images/' . $user_image);
        }
        return $imageUrl;
    }

    /**
     * Check if parsed value is url
     *
     * @return Boolean
     */
    public function isUrl($value)
    {
        return preg_match('/http(s?)\:\/\//i', $value);
    }

    public function unreadMsg()
    {
        $userId = Auth::user()->id;
        return $this->hasManyThrough('App\SphereMessages', 'App\SphereMessageBoard')->where('status', 0)->where('to_id', $userId);
    }

    public function cityState()
    {
        return $this->belongsTo('App\City', 'city_id')
            ->with('state');
    }

    public function country()
    {
        return $this->belongsTo('App\Country', 'country_id');
    }

    public function subjects()
    {
        return $this->hasMany('App\TeacherSubjects', 'teacher_id');
    }

    public function education()
    {
        return $this->hasOne('App\Education', 'id');
    }
}