<?php

/**
 * Short description for file
 *
 * PHP version 5 and 7
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
/*
 * Place includes controller for login & forgot password.
 */
/**
Pre-Load all necessary library & name space
 */

namespace App\Http\Controllers;

//load required library by use
use App\MarketPlaceDesignerReview;
use App\MarketPlaceProject;
use App\MarketPlacePurchases;
use App\MarketPlaceUpload;
use App\School;
use App\SkillSet;
//load session & other useful library
use Auth;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
//define model
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

/**
 * @subpackage Controller
 * @author     Komal Kapadi <komal@creolestudios.com>
 */
class TempController extends BaseController
{

    public function __construct()
    {
        //Artisan::call('cache:clear');
    }

    public function index()
    {

    }

    //###############################################################
    //Function Name : Getprojectdetail
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get Project Detail for new project invitation page
    //In Params : Void
    //Return : json
    //Date : 20th March 2018
    //###############################################################
    public function Getprojectdetail(Request $request)
    {
        try {
            $validator  = UtilityController::ValidationRules($request->all(), 'MarketPlaceProject');
            $returnData = $validator;
            $input      = Input::all();

            $projectData = MarketPlaceProject::where('id', $input['id'])->with([
                'projectCreator' => function ($query) {
                    $query->select('id', 'first_name', 'last_name')->get();
                },
                'skills'         => function ($query) {
                    $query->with('skillDetail')->get();
                },
            ])->where('project_status', 1)->first();

            if ($projectData) {
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $projectData);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 400, '');
            }

            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Getschools
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get schools
    //In Params : Void
    //Return : json
    //Date : 21th March 2018
    //###############################################################
    public function Getschools(Request $request)
    {
        try {
            $schoolsData = School::select('id', 'name')->where('status', 1)->get();

            if ($schoolsData) {
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $schoolsData);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 400, '');
            }

            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Getskills
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get skills
    //In Params : Void
    //Return : json
    //Date : 21th March 2018
    //###############################################################
    public function Getskills(Request $request)
    {
        try {
            $skillData = SkillSet::select('id', 'skill_name')->where('status', 1)->get();

            if ($skillData) {
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $skillData);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 400, '');
            }

            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }
    // page in which buyer will add review for any desinger
    //###############################################################
    //Function Name : Adddesignerreview
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To add review of designer
    //In Params : Void
    //Return : json
    //Date : 21th March 2018
    //###############################################################
    public function Adddesignerreview(Request $request)
    {
        try {
            $Input     = Input::all();
            $validator = UtilityController::ValidationRules($Input, 'MarketPlaceDesignerReview');
            if (!$validator['status']) {
                $errorMessage = explode('.', $validator['message']);
                $returnData   = UtilityController::Generateresponse(false, $errorMessage['0'], 0, '');
            } else {
                $resultAddReview = UtilityController::Makemodelobject($Input, 'MarketPlaceDesignerReview');
                if (!empty($resultAddReview)) {
                    $returnData = UtilityController::Generateresponse(true, 'REVIEW_ADDED', 200, $resultAddReview['data']);
                } else {
                    $returnData = UtilityController::Generateresponse(false, 'REVIEW_NOT_ADDED', 400, '');
                }
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }
    // came in both user pages
    //###############################################################
    //Function Name : Getdesignerreview
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get reviews of designer
    //In Params : Void
    //Return : json
    //Date : 21th March 2018
    //###############################################################
    public function Getdesignerreview(Request $request)
    {
        try {
            $input   = Input::all();
            $reviews = MarketPlaceDesignerReview::select('id', 'review', 'review_by', 'review_to_id')->with([
                'Reviewgivenby' => function ($query) {
                    $query->select('id', 'first_name', 'last_name')->first();
                },
            ])->where('status', 1)->where('review_to_id', $input['id'])->get();
            if (!empty($reviews)) {
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $reviews);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }
    // designer
    //###############################################################
    //Function Name : Activeprojects
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get active projects
    //In Params : Void
    //Return : json
    //Date : 21th March 2018
    //###############################################################
    public function Activeprojects(Request $request)
    {
        try {
            $page          = 10;
            $input         = Input::all();
            $offset        = $input['offset'];
            $projectsQuery = MarketPlaceProject::select('*')->with([
                'projectCreator' => function ($query) {
                    $query->select('id', 'first_name', 'last_name')->get();
                },
            ])->where('project_status', 1);
            $projects['list'] = $projectsQuery
                ->orderby('id', 'DESC')
                ->offset($offset * $page)
                ->take($page)
                ->get();
            $projects['count'] = $projectsQuery->count();
            if (!empty($projects)) {
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $projects);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }
    // designer
    //###############################################################
    //Function Name : Openprojects
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get in projects invitations means which have open status
    //In Params : Void
    //Return : json
    //Date : 21th March 2018
    //###############################################################
    public function Openprojects(Request $request)
    {
        try {
            $page          = 10;
            $input         = Input::all();
            $offset        = $input['offset'];
            $projectsQuery = MarketPlaceProject::select('*')->with([
                'projectCreator' => function ($query) {
                    $query->select('id', 'first_name', 'last_name')->get();
                },
            ])->where('project_status', 2);
            $projects['list'] = $projectsQuery
                ->orderby('id', 'DESC')
                ->offset($offset * $page)
                ->take($page)
                ->get();
            $projects['count'] = $projectsQuery->count();
            if (!empty($projects)) {
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $projects);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Gallerydetail
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : Get gallery detail
    //In Params : Void
    //Return : json
    //Date : 22nd March 2018
    //###############################################################
    public function Gallerydetail(Request $request)
    {
        try {
            $input       = Input::all();
            $galleryData = MarketPlaceUpload::select('*')->with([
                'uploadedby' => function ($query) {
                    $query->select('id', 'first_name', 'last_name')->get();
                },
                'files'      => function ($query) {
                    $query->get();
                },
            ])->where('status', 1)->where('id', $input['id'])->first();
            if (!empty($galleryData)) {
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $galleryData);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }
    //###############################################################
    //Function Name : Getgallerylisting
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : Get gallery listing
    //In Params : Void
    //Return : json
    //Date : 22nd March 2018
    //###############################################################
    public function Getgallerylisting(Request $request)
    {
        try {
            ## Search string for saerch data.
            $page         = 10;
            $input        = Input::all();
            $offset       = $input['offset'];
            $galleryQuery = MarketPlaceUpload::select('*')->with([
                'uploadedby' => function ($query) {
                    $query->select('id', 'first_name', 'last_name')->get();
                },
                'files'      => function ($query) {
                    $query->get();
                },
            ]);
            if (!empty($input['search'])) {
                $galleryQuery->where('upload_title', 'like', '%' . $input['search'] . '%')
                    ->orWhere('upload_description', 'like', '%' . $input['search'] . '%');
            }
            $galleryQuery->where('status', 1);
            $galleryData['list'] = $galleryQuery
                ->orderby('id', 'DESC')
                ->offset($offset * $page)
                ->take($page)
                ->get();
            $galleryData['count'] = $galleryQuery->count();
            if (!empty($galleryData)) {
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $galleryData);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Getgallerypurchases
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : Get gallery purchases
    //In Params : Void
    //Return : json
    //Date : 22nd March 2018
    //###############################################################
    public function Getgallerypurchases(Request $request)
    {
        try {
            ## Search string for saerch data.
            $page         = 10;
            $input        = Input::all();
            $offset       = $input['offset'];
            $galleryQuery = MarketPlacePurchases::select('*')->with([
                'purchasedFiles' => function ($query) {
                    $query->select('*')->with([
                        'uploadedby' => function ($query) {
                            $query->select('id', 'first_name', 'last_name')->get();
                        },
                        'files'      => function ($query) {
                            $query->get();
                        },
                    ])->get();
                },
            ])->where('upload_of', 2)->where('user_id', $input['id']);
            $galleryData['list'] = $galleryQuery
                ->orderby('id', 'DESC')
                ->offset($offset * $page)
                ->take($page)
                ->get();
            $galleryData['count'] = $galleryQuery->count();    
            if (!empty($galleryData)) {
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $galleryData);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }
}
