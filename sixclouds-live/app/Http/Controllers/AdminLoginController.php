<?php

/**
 * Short description for file
 *
 * PHP version 5 and 7
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 */
/*
 * Place includes controller for login & forgot password.
 */

/**
Pre-Load all necessary library & name space
 */

namespace App\Http\Controllers;

//load required library by use
use App\Admin;
use App\User;
use Auth;
use Hash;
//load authorization library
use Illuminate\Http\Request;
//load session & other useful library
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Image;
//define model
use View;
use Carbon\Carbon;

/**
 * Photos
 * @package    AdminLoginController
 */
class AdminLoginController extends BaseController
{
    public function __construct()
    {
        //Artisan::call('cache:clear');
    }

    //###############################################################
    //Function Name : index
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To load login view
    //In Params : Void
    //Return : Login HTML View
    //###############################################################
    public function index()
    {
        if (Auth::guard('admins')->check()) {
            return redirect('manage/sixteen/all-projects');
        } else {
            return View::make("admin/login");
        }
    }

    //###############################################################
    //Function Name : Dologin
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To login the admin
    //In Params : email,password
    //Return : success & redirect to dashboard/Error on same page
    //###############################################################
    public function Dologin(Request $request)
    {
        try {
            if (Auth::guard('pr-admins')) {
                Auth::guard('pr-admins')->logout();
            }

            $validator  = UtilityController::ValidationRules($request->all(), 'Admin', array('first_name', 'last_name', 'phone', 'dob', 'password', 'image', 'modules'));
            $returnData = $validator;
            $input      = Input::all();
            $isLoggedin = 0;
            if (!$validator['status']) {
                $returnData = UtilityController::Setreturnvariables();
            } else {
                if (Auth::guard('admins')->check()) {
                    $isLoggedin = 1;
                }
                if ($isLoggedin == '1') {
                    //user already logged in then return success
                    $returnData = UtilityController::Generateresponse(true, 'LOGIN_SUCCESS', 200, Auth::guard('admins')->user());
                } else {
                    //check user's credentials
                    $credentials = [
                        "email_address" => Input::get("email_address"),
                        "password"      => Input::get("password"),
                    ];
                    if (Auth::guard('admins')->attempt($credentials)) {
                        $returnData = UtilityController::Generateresponse(true, 'LOGIN_SUCCESS', 200, Auth::guard('admins')->user());
                    } else {
                        $returnData = UtilityController::Generateresponse(false, 'LOGIN_FAIL', 204, Auth::guard('admins')->user());
                    }
                }
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Adminforgotpassword
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To send link for forgot password
    //In Params : email
    //Return : success/error message for emiail sent or not
    //###############################################################
    public function Adminforgotpassword(Request $request)
    {
        try {
            $validator  = UtilityController::ValidationRules($request->all(), 'Admin');
            $returnData = $validator;
            $input      = Input::all();
            if ($validator['status']) {
                $returnData = UtilityController::Setreturnvariables();
            } else {
                //update password token and send email for forgot password
                //get email address
                $userEmail = Input::get('email');
                /* Generate random string with 25 charater. */
                $forgetToken = str_random(25);
                /* Generate the link with parameter. */
                $emailParams = array('resetpwd_token' => $forgetToken, 'email' => base64_encode($userEmail));
                $resetURL    = action('AdminLoginController@AdminResetpassword', $emailParams);
                /* Update the user with forgot token. */
                //get user object for admin user
                $userObject = Admin::where('email_address', $userEmail)->first();
                //update forgot token
                $userObject->resetpwd_token = $forgetToken;
                //if token is saved then send email
                if ($userObject->save()) {
                    //prepare parameter to send in email
                    $data['username'] = $userObject->first_name . ' ' . $userObject->last_name;
                    $data['link']     = $resetURL;
                    $data['email']    = Input::get('email');
                    $mailResult       = Mail::send('front.emails.forgotpassword', $data, function ($message) use ($data) {
                        $message->from(Config('constants.messages.MAIL_ID'), 'Sixclouds');
                        $message->to($data['email'])->subject('Sixclouds : Forgot password');
                    });
                    $returnData = UtilityController::Generateresponse(true, 'FORGOT_PASSWORD_SUCCESS', 200, '');
                } else {
                    $returnData = UtilityController::Generateresponse(true, 'GENERAL_ERROR', 200, '');
                }
            }
            return $returnData;
        } catch (Exception $ex) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : AdminResetpassword
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To send reset password link for admin
    //In Params : Email
    //Return : success with email send/error message
    //###############################################################
    public function AdminResetpassword(Request $Request)
    {
        //get token and email from url
        $forgotToken = $Request->get('resetpwd_token');
        $email       = base64_decode($Request->get('email'));
        //check if the link is expired or not
        $userObject = Admin::where('email_address', $email)->where('resetpwd_token', $forgotToken)->exists();
        if ($userObject) {
            //user data is found, proceed for reset password
            return view::make("admin.reset")->with('email', $email);
        } else {
            //load view for url expired
            return view::make("admin.urlexpired");
        }
    }

    //###############################################################
    //Function Name : Doadminresetpassword
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To reset password for admin
    //In Params : password
    //Return : success/error message
    //###############################################################
    public function Doadminresetpassword(Request $Request)
    {
        try {
            $postData   = Input::all();
            $validator  = UtilityController::ValidationRules($postData, 'Admin');
            $returnData = $validator;

            if (!$validator['status']) {
                $returnData = UtilityController::Setreturnvariables();
            } else {
                //make password
                $newPassword = Hash::make(Input::get('password'));
                //update new password for user
                //get user object for admin user
                $userObject = Admin::where('email_address', $postData['email'])->first();
                //update forgot token
                $userObject->password       = $newPassword;
                $userObject->resetpwd_token = '';

                //if token is saved then send email
                if ($userObject->save()) {
                    $returnData = UtilityController::Generateresponse(true, 'SUCCESS_PASSWORD_CHANGE', 200, '');
                } else {
                    $returnData = UtilityController::Generateresponse(true, 'GENERAL_ERROR', 200, '');
                }
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Logout
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To logout user from admin panel
    //In Params : void
    //Return : logout & redirect user to login screen,
    //###############################################################
    public function Logout()
    {
        //logout user
        $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        if (!Auth::guard('admins')->logout()) {
            //redirect user to login screen
            $returnData = UtilityController::Generateresponse(true, 'LOGOUT_SUCCESS', 200, '');
        }
        return $returnData;
    }

    //###############################################################
    //Function Name : Userdata
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : to check if the user is already logged in or not
    //In Params : void
    //Return : logged in user details,
    //###############################################################
    public function userdata()
    {
        $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        if (Auth::guard('admins')->check()) {
            $responseArray = Admin::where('id', Auth::guard('admins')->user()->id)
            ->with([
                'reporting_officer' => function ($query) {
                    $query->get();
                },
            ])
            ->first();
            $responseArray['image'] = UtilityController::Fileexist($responseArray['image'], public_path() . config('constants.path.ADMIN_PROFILE_PATH'), url('/') . config('constants.path.ADMIN_PROFILE_URL'));
            $returnData             = UtilityController::Generateresponse(true, 'LOGIN_SUCCESS', 200, $responseArray);
        }
        else {
            $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
        }
        return $returnData;
    }

    //###############################################################
    //Function Name : Changemyaccountpassword
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To change company password
    //In Params : void
    //Return : change the logged in company password,
    //###############################################################
    public function Changemyaccountpassword(Request $request)
    {
        $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        $user       = Auth::guard('admins')->user();
        try {
            $Input           = Input::all();
            $currentPassword = Input::get('old_password');
            $newPassword     = Input::get('new_password');

            if (Hash::check($currentPassword, $user->password)) {
                $userId                    = $user->id;
                $newUserPassword           = Company::find($userId);
                $newUserPassword->password = Hash::make($newPassword);
                $result                    = $newUserPassword->save();
                if ($result) {
                    $returnData = UtilityController::Generateresponse(true, 'PASSWORD_CHANGE_SUCCESS', 200, '');
                } else {
                    $returnData = UtilityController::Generateresponse(false, 'PASSWORD_CHANGE_FAILED', 200, '');
                }
            } else {
                $returnData = UtilityController::Generateresponse(false, 'INCORRECT_OLD_PASSWORD', '', '');
            }
            return response()->json($ResponseArray);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Checkadminemailexists
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To check email exists or not for existing admin
    //In Params : Void
    //Return : json
    //Date : 5th December 2017
    //###############################################################
    public function Checkadminemailexists(Request $request)
    {
        $email       = (!empty($request->email_fp) ? $request->email_fp : $request->email_address);
        $emailExists = Admin::where('email_address', $email)->count();
        return ($emailExists > 0 ? 'true' : 'false');
    }

    //###############################################################
    //Function Name : Editadminprofile
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To edit profile
    //In Params : void
    //###############################################################
    public function Editadminprofile(Request $request)
    {
        $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        $user       = Auth::guard('admins')->user();
        try {
            $validator  = UtilityController::ValidationRules($request->all(), 'Admin', array('password', 'image', 'modules'));
            $returnData = $validator;
            if (!$validator['status']) {
                $returnData = UtilityController::Setreturnvariables();
            } else {
                $adminData = UtilityController::MakeObjectFromArray($request->all(), 'Admin')->toArray();
                $data      = '';
                // if (!empty(request('image'))) {
                //     $file = request('image')->getMimeType();
                //     // create image unique name
                //     $imageName = str_replace(" ", "_", $adminData['first_name']) . '_' . time() . '.' . $request->image->getClientOriginalExtension();
                //     // if file is moved to destination. store file name and type in array.
                //     if ($request->image->move(public_path() . config('constants.path.ADMIN_PROFILE_PATH'), $imageName)) {
                //         $adminData['image'] = $imageName;
                //         $data               = url('/') . config('constants.path.ADMIN_PROFILE_URL') . $adminData['image'];
                //     }
                // }
                if (!empty(request('image'))) {
                    $file = request('image')->getMimeType();
                    // create image unique name
                    $imageName = str_replace(" ", "_", $adminData['first_name']) . '_' . time() . '.' . $request->image->getClientOriginalExtension();
                    // if file is moved to destination. store file name and type in array.
                    // if ($request->image->move(public_path() . config('constants.path.ADMIN_PROFILE_PATH'), $imageName)) {
                    //     $adminData['image'] = $imageName;
                    //     $data               = url('/') . config('constants.path.ADMIN_PROFILE_URL') . $adminData['image'];

                    $DestinationPath = public_path() . config('constants.path.ADMIN_PROFILE_PATH');
                    // Image::make(Input::file('image'))->resize(100, 100, function ($constraint) {
                    //     $constraint->aspectRatio();
                    //     $constraint->upsize();
                    // })->save($DestinationPath . $imageName);
                    $adminData['image'] = $imageName;
                    $data               = url('/') . config('constants.path.ADMIN_PROFILE_URL') . $adminData['image'];

                    if (request('x1') && request('y1') && request('w') && request('h') == 0) {
                        $w = $h = 290;
                        $x = $y = 294;
                    } else {
                        $x = (int) request('x1');
                        $y = (int) request('y1');
                        $w = (int) request('w');
                        $h = (int) request('h');
                    }
                    Image::make(Input::file('image'))->crop($w, $h, $x, $y)->resize(300, 300, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    })->save($DestinationPath . $imageName);
                }
                $updateData = UtilityController::Makemodelobject($adminData, 'Admin', 'id', $idToEdit = Auth::guard('admins')->user()->id);
                $returnData = UtilityController::Generateresponse(true, 'UPDATED', 200, $data);
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '400', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Adminlisting
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get admin listing
    //In Params : void
    //###############################################################
    public function Adminlisting(Request $request)
    {
        $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        $user       = Auth::guard('admins')->user();

        try {
            $adminResult = Admin::with('admins')->where('status', 1)->get();
            if (!empty($adminResult)) {
                foreach ($adminResult as $key => $value) {
                    $adminResult[$key]['image'] = UtilityController::Imageexist($value['image'], public_path() . config('constants.path.ADMIN_PROFILE_PATH'), url('/') . config('constants.path.ADMIN_PROFILE_URL'), url('/') . config('constants.path.ADMIN_DEFAULT_IMAGE'));
                }
            }
            $returnData = UtilityController::Generateresponse(true, 'EMPTY_MESSAGE', 200, $adminResult);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '400', '');
        }
        return $returnData;
    }

    //###############################################################
    //Function Name : Addadmin
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : Add new admin
    //In Params : void
    //###############################################################
    public function Addadmin(Request $request)
    {
        $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        $user       = Auth::guard('admins')->user();
        try {
            $validator  = UtilityController::ValidationRules($request->all(), 'Admin', array('phone', 'image'));
            $returnData = $validator;
            if (!$validator['status']) {
                $returnData = UtilityController::Setreturnvariables();
            } else {
                $adminData = UtilityController::MakeObjectFromArray($request->all(), 'Admin')->toArray();
                if (!empty(request('image'))) {
                    $file = request('image')->getMimeType();
                    // create image unique name
                    $imageName = str_replace(" ", "_", $adminData['first_name']) . '_' . time() . '.' . $request->image->getClientOriginalExtension();
                    // if file is moved to destination. store file name and type in array.
                    if ($request->image->move(public_path() . config('constants.path.ADMIN_PROFILE_PATH'), $imageName)) {
                        $adminData['image'] = $imageName;
                    }
                }
                $emailExists = Admin::where('email_address', $request->email_address)->count();
                if (!empty($emailExists)) {
                    $returnData = UtilityController::Generateresponse(false, 'EMAIL_ALREADY_EXISTS', 400, '');
                } else {
                    $adminData['password']             = Hash::make($request->password);
                    $adminData['dob']                  = date('Y-m-d', strtotime($request->dob));
                    $adminData['reporting_officer_id'] = $request->reporting_officer_id; //$user->id;
                    $addedData                         = UtilityController::Makemodelobject($adminData, 'Admin', 'id');
                    $returnData                        = UtilityController::Generateresponse(true, 'ADDED', 200, '');
                }
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '400', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Deleteadmin
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : Delete admin based on admin id
    //In Params : void
    //###############################################################
    public function Deleteadmin(Request $request)
    {
        $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        try {
            $adminResult = Admin::where('id', $request->id)->update(array('status' => 2));
            $returnData  = UtilityController::Generateresponse(true, 'DELETED', 200, $adminResult);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '400', '');
        }
        return $returnData;
    }
    /*###############################################################
    Function Name : Changepassword
    Author : Komal Kapadi <komal@creolestudios.com>
    Purpose : To change admin password
    In Params : Void
    ###############################################################*/
    public function Changepassword(Request $request)
    {
        try {
            $user  = Auth::guard('admins')->user();
            $Input = Input::all();
            if (Hash::check($Input['passwords']['old_password'], $user->password)) {
                $userId                         = $user->id;
                $Input['passwords']['password'] = Hash::make($Input['passwords']['password']);
                $result                         = UtilityController::Makemodelobject($Input['passwords'], 'Admin', '', $userId);
                if ($result) {
                    $responseArray = UtilityController::Generateresponse(true, 'PASSWORD_CHANGE_SUCCESS', 1, $result);
                } else {
                    $responseArray = UtilityController::Generateresponse(false, 'PASSWORD_CHANGE_FAIL', 0);
                }

            } else {
                $responseArray = UtilityController::Generateresponse(false, 'PASSWORD_OLD_INCORRECT', 0);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }
}
