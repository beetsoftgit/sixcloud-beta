<?php

/**
 * Short description for file
 *
 * PHP version 5 and 7
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @package    UserController
 * @author     Komal Kapadi (komal@creolestudios.com)
 * @copyright  2017 Sixcloud Group
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @since      File available since Release 1.0.0
 * @deprecated N/A
 */
/*
 * Clients section related controller
 */

/**
Pre-Load all necessary library & name space
 */

namespace Trending\Http\Controllers\File;

//===================================
namespace App\Http\Controllers;

use App\Http\Controllers\UtilityController;
use Auth;
use Illuminate\Http\Request;

class AppUserController extends Controller
{
    //###############################################################
    //Function Name : login
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To authenticate user
    //In Params : Void
    //Return : json
    //Date : 22nd January 2018
    //###############################################################
    public function login(Request $request)
    {
        try {
                $responseData = [
                    'OBJECT' => (object) [],
                    'ARRAY'  => array(),
                ];
            if (Auth::guard('app')->attempt(['email' => request('email'), 'password' => request('password')])) {
                ## get logged in user details
                $User = Auth::guard('app')->user();
                $UserExists = User::where('id', $User->id)->where('status', 1)->get();
                if ($UserExists->isEmpty() == true) {
                    $Response = UtilityController::ResponseFormat(config('constants.api_code.STATUS.BAD_INPUT'), $responseData['OBJECT'], config('constants.admin_messages.USER_DELETED'));
                    //return response
                    return response()->json($Response);
                }
                if ($result) {
                    $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $result);
                } else {
                    $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 400, '');
                }
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }
}
