<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\VideoCategory;
use App\Videos;
use App\Worksheets;
use App\Quizzes;
use App\Subject;
use App\Zone;
use App\ZoneVideo;
use App\ZoneQuiz;
use App\ZoneWorksheet;
use Carbon\Carbon;

class ScriptForQuizzesVideosAndWorksheets extends Controller
{
    public function runScript()
    {
    	echo "string";
    	/*foreach (\File::allFiles(public_path('uploads/video_thumbnails/thumbnail_mobile_128_72')) as $filename) {
		    $file = \Illuminate\Support\Facades\Storage::get($filename);
		    // do whatever with $file;
		}*/
    	$images = \File::allFiles(public_path('uploads/video_thumbnails/thumbnail_mobile_128_72'));
    	//echo("<pre>");print_r($images);echo("</pre>");

    	foreach (\File::allFiles(public_path('uploads/video_thumbnails/thumbnail_mobile_128_72')) as $filename) {
		    echo "$filename size ";
		}
    	exit();
    	try {
	    	$quizzes = Quizzes::select('id','category_id')->where('status', 1)->where('quiz_published_status',2)->with('categoryWithSubject')->get()->toArray();
	    	// print_r($quizzes); die;
	    	$videos = Videos::select('id','video_category_id')->where('status', 1)->with('categoryWithSubject')->get()->toArray();
	    	$worksheets = Worksheets::select('id','worksheet_category_id')->where('status', 1)->with('categoryWithSubject')->get()->toArray();
	    	
	    	foreach ($quizzes as $key => $quiz) {
	    		$idExists = ZoneQuiz::where('quiz_id',$quiz['id'])->get()->toArray();
	    		if(!$idExists || is_null($idExists) || empty($idExists)) {
	    			$zoneArray = explode(',', $quiz['category_with_subject']['subject_zones']['zones']);
		    		foreach ($zoneArray as $key2 => $zone) {
		    			$data = [];
		    			if( ! empty($zone)) {
		    				$data = [
		    					'quiz_id' 		=> $quiz['id'], 
		    					'zone_id' 		=> $zone,
		    					'created_at' 	=> Carbon::now(),
		    					'updated_at' 	=> Carbon::now()
		    				];
		    				$insertDataQuiz[] = $data;
		    			}
	    			}
	    		}
	    	}
	    	
	    	foreach ($videos as $key => $video) {
	    		$idExists = ZoneVideo::where('video_id',$video['id'])->get()->toArray();
	    		if(!$idExists || is_null($idExists) || empty($idExists)) {
	    			$zoneArray = explode(',', $video['category_with_subject']['subject_zones']['zones']);
		    		foreach ($zoneArray as $key2 => $zone) {
		    			$data = [];
		    			if( ! empty($zone)) {
		    				$data = [
		    					'video_id' 		=> $video['id'], 
		    					'zone_id' 		=> $zone,
		    					'created_at' 	=> Carbon::now(),
		    					'updated_at' 	=> Carbon::now()
		    				];
		    				$insertDataVideo[] = $data;
		    			}
	    			}
	    		}
	    	}

	    	foreach ($worksheets as $key => $worksheet) {
	    		$idExists = ZoneWorksheet::where('worksheet_id',$worksheet['id'])->get()->toArray();
	    		if(!$idExists || is_null($idExists) || empty($idExists)) {
	    			$zoneArray = explode(',', $worksheet['category_with_subject']['subject_zones']['zones']);
		    		foreach ($zoneArray as $key2 => $zone) {
		    			$data = [];
		    			if( ! empty($zone)) {
		    				$data = [
		    					'worksheet_id' 	=> $worksheet['id'], 
		    					'zone_id' 		=> $zone,
		    					'created_at' 	=> Carbon::now(),
		    					'updated_at' 	=> Carbon::now()
		    				];
		    				$insertDataWorksheet[] = $data;
		    			}
	    			}
	    		}
	    	}

	    	if(isset($insertDataQuiz) && ZoneQuiz::insert($insertDataQuiz)) {
	    		$messageQuiz = 'Quiz: Success';
	    	} else {
				$messageQuiz = 'Quiz: Already Done';
			}
			if(isset($insertDataVideo) && ZoneVideo::insert($insertDataVideo)) {
				$messageVideo = 'Video: Success';
			} else {
				$messageVideo = 'Video: Already Done';
			}
	    	if(isset($insertDataWorksheet) && ZoneWorksheet::insert($insertDataWorksheet)) {
	    		$messageWorksheet = 'Worksheet: Success';
	    	} else {
				$messageWorksheet = 'Worksheet: Already Done';
			}
			$message = $messageQuiz ."; ". $messageVideo ."; ". $messageWorksheet;
			$returnData = UtilityController::Generateresponse(true, $message, Response::HTTP_OK);
	    } catch(\Exception $e) {
	    	$sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getMessage(), Response::HTTP_BAD_REQUEST);
	    }
	    return $returnData;
	}
}
