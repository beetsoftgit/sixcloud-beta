<?php

/**
 * Short description for file
 *
 * PHP version 5 and 7
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 */
/*
 * Place includes controller for login & forgot password.
 */

/**
  Pre-Load all necessary library & name space
 */

namespace App\Http\Controllers;

//load required library by use
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
//load authorization library
use Auth;
use View;
use Hash;
//load session & other useful library
use Session;
use Carbon\Carbon;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
//define model
use App\User;
use App\Accessor;


/**
 * Photos
 * @package    AdminLoginController
 */
class AdminPrLoginController extends BaseController {

    public function __construct() {
        //Artisan::call('cache:clear');        
    }

    //###############################################################
    //Function Name : index
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To load login view or redirect to particular page if already logged in
    //In Params : Void
    //Return : Login HTML View
    //###############################################################
    public function index() {
        if (Auth::guard('pr-admins')->check()) {
            return redirect('manage/my-jobs');
        } else {
            return View::make("admin/pr-login");
        }
    }

    //###############################################################
    //Function Name : Doprlogin
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To login the accessor
    //In Params : email,password
    //Return : success & redirect to my-jobs/Error on same page
    //###############################################################
    public function Doprlogin(Request $request) {
        try {
            if(Auth::guard('admins'))
                Auth::guard('admins')->logout();
            $validator = UtilityController::ValidationRules($request->all(), 'Accessor',['first_name','last_name','phone_number','team','level_type','accessor_id']);
            $input = Input::all();
            $isLoggedin = 0;
            if (!$validator['status']) {
                $returnData = $validator;
            } else {
                if (Auth::guard('pr-admins')->check()) {
                    $isLoggedin = 1;
                }
                if ($isLoggedin == '1') {
                    //user already logged in then return success
                    $returnData = UtilityController::Generateresponse(true, 'LOGIN_SUCCESS', 200, Auth::guard('pr-admins')->user());
                } else { //check user's credentials
                    $credentials = [
                        "email_address" => Input::get("email_address"),
                        "password" => Input::get("password")
                    ];
                    if (Auth::guard('pr-admins')->attempt($credentials)) {
                        $returnData = UtilityController::Generateresponse(true, 'LOGIN_SUCCESS', 200, Auth::guard('pr-admins')->user());
                    } else {
                        $returnData = UtilityController::Generateresponse(false, 'LOGIN_FAIL', 204, Auth::guard('pr-admins')->user());
                    }
                }
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Adminprforgotpassword
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To send link for forgot password
    //In Params : email
    //Return : success/error message for email sent or not
    //###############################################################
    public function Adminprforgotpassword(Request $request) {
        try {
            $validator = UtilityController::ValidationRules($request->all(), 'Accessor');
            $returnData = $validator;
            $input = Input::all();
            if ($validator['status']) {
               $returnData = UtilityController::Setreturnvariables();
            } else {
                $userEmail = Input::get('email');
                $forgetToken = str_random(25);
                $emailParams = array('resetpwd_token' => $forgetToken, 'email' => base64_encode($userEmail));
                $resetURL = action('AdminPrLoginController@Adminprresetpassword', $emailParams);
                $userObject = Accessor::where('email_address', $userEmail)->first();
                $userObject->resetpwd_token = $forgetToken;
                if ($userObject->save()) {
                    $data['username'] = $userObject->first_name . ' ' . $userObject->last_name;
                    $data['link'] = $resetURL;
                    $data['email'] = Input::get('email');
                    $mailResult = Mail::send('front.emails.forgotpassword', $data, function($message) use ($data) {
                                $message->from(Config('constants.messages.MAIL_ID'), 'Sixclouds');
                                $message->to($data['email'])->subject('Sixclouds : Forgot password');
                            });
                    $returnData = UtilityController::Generateresponse(true, 'FORGOT_PASSWORD_SUCCESS', 200, '');
                } else {
                    $returnData = UtilityController::Generateresponse(true, 'GENERAL_ERROR', 200, '');
                }
            }
            return $returnData;
        } catch (Exception $ex) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Adminprresetpassword
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To send reset password link for admin
    //In Params : Email
    //Return : success with email send/error message
    //###############################################################
    public function Adminprresetpassword(Request $Request) {
        $forgotToken = $Request->get('resetpwd_token');
        $email = base64_decode($Request->get('email'));
        $userObject = Accessor::where('email_address', $email)->where('resetpwd_token', $forgotToken)->exists();
        if ($userObject) {
            return view::make("admin.resetpr")->with('email', $email);
        } else {
            return view::make("admin.urlexpired");
        }
    }

    //###############################################################
    //Function Name : Doadminprresetpassword
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To reset password for admin
    //In Params : password
    //Return : success/error message
    //###############################################################
    public function Doadminprresetpassword(Request $Request) {
        try {
            $postData = Input::all();
            $newPassword = Hash::make(Input::get('password'));
            $userObject = Accessor::where('email_address', $postData['email'])->first();
            $userObject->password = $newPassword;
            $userObject->resetpwd_token = '';
            if ($userObject->save()) {
                $returnData = UtilityController::Generateresponse(true, 'SUCCESS_PASSWORD_CHANGE', 200, '');
            } else {
                $returnData = UtilityController::Generateresponse(true, 'GENERAL_ERROR', 200, '');
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }
        return $returnData;
    }

    //###############################################################
    //Function Name : Prlogout
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To logout user from pr section
    //In Params : void
    //Return : logout & redirect user to login screen,
    //###############################################################
    public function Prlogout() {
        $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        if (!Auth::guard('pr-admins')->logout()) {
            $returnData = UtilityController::Generateresponse(true, 'LOGOUT_SUCCESS', 200, '');
        }
        return $returnData;
    }

    //###############################################################
    //Function Name : Pruserdata
    //Author : Senil shah <senil@creolestudios.com>
    //Purpose : to check if the user is already logged in or not
    //In Params : void
    //Return : logged in user details,
    //###############################################################
    public function Pruserdata() {
        $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        if (Auth::guard('pr-admins')->check()) {
            $responseArray = Accessor::find(Auth::guard('pr-admins')->user()->id);
            $responseArray['image'] = UtilityController::Fileexist($responseArray['image'], public_path() . UtilityController::Getmessage('ACCESSOR_IMAGE_PATH'), url('/') . UtilityController::Getmessage('ACCESSOR_IMAGE_URL'));
            $returnData = UtilityController::Generateresponse(true, 'LOGIN_SUCCESS', 200, $responseArray);
        }
        return $returnData;
    }

    //###############################################################
    //Function Name : Checkpradminemailexists
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To check email exists or not for existing user
    //In Params : Void
    //Return : json
    //Date : 19th January 2018
    //###############################################################
    public function Checkpradminemailexists(Request $request) {
        $email = (!empty($request->email_fp) ? $request->email_fp : $request->email_address);
        $emailExists = Accessor::where('email_address', $email)->count();
        return ($emailExists > 0 ? 'true' : 'false');
    }
}
