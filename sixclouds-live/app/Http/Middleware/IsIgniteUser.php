<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;

use Closure;

class IsIgniteUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            if(Auth::user()->status==5){
                return redirect('/user-verify-otp/'.Auth::user()->phonecode.'/'.Auth::user()->contact.'/'.base64_encode(Auth::user()->email_address));
            }
            if(!isset(Auth::user()->country_id)||!isset(Auth::user()->city_id)||!isset(Auth::user()->provience_id)/*||!isset(Auth::user()->dob)*/) {
                return redirect('/my-account');
            }
        } else {
            if(Auth::guard('distributors')->check())
                return redirect('/subscribers');
            elseif(Auth::guard('distributor_team_members')->check())
                return redirect('/my-referral');
            else
                return redirect('/ignite-login');
        }
        return $next($request);
    }
}
