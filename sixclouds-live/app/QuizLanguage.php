<?php

//###############################################################
//File Name : QuizLanguage.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : Worksheet's title and description for different languages
//Date : 25th Feb, 2019
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuizLanguage extends Model
{
    protected $table  = 'quiz_languages';
    
    public function worksheet() {
        return $this->hasOne('App\Worksheets', 'id','worksheet_id');
    }

}
