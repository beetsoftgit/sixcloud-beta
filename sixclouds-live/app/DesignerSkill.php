<?php

//###############################################################
//File Name : DesignerSkill.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related to skills acquired by the designer
//Date : 19th Mar, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class DesignerSkill extends Model
{
    protected $table  = 'designer_skills'; 
    public $rules = array(
    	'user_id' => 'required',
        'skill_set_id'       => 'required'
    );

    public function ratings(){
        return $this->belongsTo('App\MarketPlaceDesignerRating','user_id', 'rating_to_id');
    }

    public function reviews(){
        return $this->belongsTo('App\MarketPlaceDesignerReview','user_id', 'review_to_id');
    }

    
    public function user_designer(){
        return $this->belongsTo('App\User','user_id','id')->with('ratings', 'reviews');
    }

    public function skill_details(){
        return $this->belongsTo('App\SkillSet','skill_set_id','id');
    }
}