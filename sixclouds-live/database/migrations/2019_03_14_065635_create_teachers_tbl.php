<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeachersTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teachers', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('country_id');
            $table->integer('city_id');
            $table->integer('provience_id');
            $table->integer('education_id');
            $table->string('email_address', 255)->unique();
            $table->string('first_name', 255);
            $table->string('last_name', 255);
            $table->string('display_name', 255);
            $table->date('dob');
            $table->integer('phonecode');
            $table->bigInteger('contact');
            $table->string('employment_status', 255);
            $table->tinyInteger('teaching_experience')->comment = "0:No; 1:Yes";
            $table->string('teaching_experience_duration', 255)->nullable();
            $table->tinyInteger('adhoc_session_availability')->comment = "0:No; 1:Yes";
            $table->string('id_proof', 255);
            $table->string('graduation_certificate', 255);
            $table->string('profile_image', 255)->nullable();
            $table->string('residence_proof', 255);
            $table->string('last_employment_proof', 255)->nullable();
            $table->string('additional_certificates', 255)->nullable();
            $table->string('introduction_video', 255);
            $table->string('trial_session_video', 255)->nullable();
            $table->text('slug')->nullable();
            $table->timestamps();

            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
            $table->foreign('provience_id')->references('id')->on('states')->onDelete('cascade');
            $table->foreign('education_id')->references('id')->on('educations')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teachers');
    }
}
