<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVideoLanguageIdInVideoLanguageUrls extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('video_language_urls', function(Blueprint $table)
        {
            $table->unsignedInteger('video_language_id')->default(0)->after('video_language');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('video_language_urls', function(Blueprint $table)
        {
            $table->dropColumn('video_language_id');
        });
    }
}
