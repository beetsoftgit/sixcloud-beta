<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSphereMessages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sphere_messages', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('sphere_message_board_id');
            $table->integer('from_id');
            $table->integer('to_id');
            $table->text('message');
            $table->tinyInteger('status')->comment = "0:Unread; 1:Read";
            $table->timestamps();

            $table->foreign('sphere_message_board_id')->references('id')->on('sphere_message_boards')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sphere_messages');
    }
}
