<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubcategoryIcon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('video_categories', function(Blueprint $table)
        {
            $table->string('sub_category_icon', 255)->after('category_gif_url')->nullable();
            $table->tinyInteger('sub_category_icon_position')->comment('0:Left 1:Right')->after('sub_category_icon')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('video_categories', function(Blueprint $table)
        {
            $table->dropColumn('sub_category_icon');
            $table->dropColumn('sub_category_icon_position');
        });
    }
}
