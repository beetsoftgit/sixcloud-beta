<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoryChiCategoryRuField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('video_categories', function(Blueprint $table)
        {
            $table->string('category_name_chi',255)->nullable()->after('category_name');
            $table->string('category_name_ru',255)->nullable()->after('category_name_chi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('video_categories', function(Blueprint $table)
        {
            $table->dropColumn('category_name_chi');
            $table->dropColumn('category_name_ru');
        });
    }
}
