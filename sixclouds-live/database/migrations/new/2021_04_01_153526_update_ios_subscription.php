<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateIosSubscription extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('unique_id', 255)->nullable()->comment("this id for guest user");
            $table->tinyInteger('user_type')->default('0')->comment("0:user,1:guest user");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('twinkle_pricing_plan', function(Blueprint $table) {
            $table->dropColumn('unique_id');
            $table->dropColumn('user_type');
        });
    }
}
