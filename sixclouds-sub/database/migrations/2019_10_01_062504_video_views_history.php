<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VideoViewsHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_views_history', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('video_views_id');
            $table->time('start_time');
            $table->time('stop_time');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('video_views_id')->references('id')->on('video_views')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_views_history');
    }
}
