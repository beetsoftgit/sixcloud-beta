<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\VideoCategory;

class UpdateCategoryGifUrlInVideoCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        VideoCategory::where('category_name', 'like','%BUZZ 1%')->update(['category_gif_url' => 'buzz_1.gif']);
        VideoCategory::where('category_name', 'like','%BUZZ 2%')->update(['category_gif_url' => 'buzz_2.gif']);
        VideoCategory::where('category_name', 'like','%BUZZ 3%')->update(['category_gif_url' => 'buzz_3.gif']);
        VideoCategory::where('category_name', 'like','%MAZE 1%')->update(['category_gif_url' => 'Maze 1.gif']);
        VideoCategory::where('category_name', 'like','%MAZE 2%')->update(['category_gif_url' => 'Maze 2.gif']);
        VideoCategory::where('category_name', 'like','%MAZE 3%')->update(['category_gif_url' => 'Maze 3.gif']);
        VideoCategory::where('category_name', 'like','%MAZE 4%')->update(['category_gif_url' => 'Maze 4.gif']);
        VideoCategory::where('category_name', 'like','%MAZE 5%')->update(['category_gif_url' => 'Maze 5.gif']);
        VideoCategory::where('category_name', 'like','%MAZE 6%')->update(['category_gif_url' => 'Maze 6.gif']);
        VideoCategory::where('category_name', 'like','%MAZE 7%')->update(['category_gif_url' => 'Maze 7.gif']);
        VideoCategory::where('category_name', 'like','%MAZE 8%')->update(['category_gif_url' => 'Maze 8.gif']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        VideoCategory::update(['category_gif_url' => NULL]);
    }
}
