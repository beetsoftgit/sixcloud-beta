<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeacherTrialSessions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teacher_trial_sessions', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('teacher_id');
            $table->dateTime('date_time');
            $table->date('date');
            $table->string('slot', 255);
            $table->tinyInteger('rescheduled')->comment = "0:No; 1:Yes";
            $table->timestamps();

            $table->foreign('teacher_id')->references('id')->on('teachers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teacher_trial_sessions');
    }
}
