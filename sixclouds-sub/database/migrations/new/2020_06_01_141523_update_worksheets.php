<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateWorksheets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('worksheets', function(Blueprint $table)
        {
            $table->string('thumbnail_web', 255)->after('original_file_name')->nullable();
            $table->string('thumbnail_160_90', 255)->after('thumbnail_web')->nullable();
            $table->string('thumbnail_128_72', 255)->after('thumbnail_160_90')->nullable();
            $table->string('download_button_text', 255)->after('thumbnail_128_72')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('worksheets', function(Blueprint $table)
        {
            $table->dropColumn('thumbnail_web');
            $table->dropColumn('thumbnail_160_90');
            $table->dropColumn('thumbnail_128_72');
            $table->dropColumn('download_button_text');
        });
    }
}
