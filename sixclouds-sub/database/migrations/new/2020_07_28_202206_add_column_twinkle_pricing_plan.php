<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnTwinklePricingPlan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('twinkle_pricing_plan', function (Blueprint $table) {
            $table->string('category_name_chi', 255)->nullable()->after('category_name');
            $table->string('category_name_ru', 255)->nullable()->after('category_name_chi');
            $table->string('bundle_category_id', 255)->nullable()->after('category_name_ru');
            $table->Integer('parent_id')->comment("Id of same tabel, if not null, will be indicating that it will be a sub-category under the category which this id will be pointing to")->after('bundle_category_id');
            $table->string('subcategory_name', 255)->nullable()->after('parent_id');
            $table->string('subcategory_name_chi', 255)->nullable()->after('subcategory_name');
            $table->string('subcategory_name_ru', 255)->nullable()->after('subcategory_name_chi');
            $table->string('description_chi', 255)->nullable()->after('description');
            $table->string('description_ru', 255)->nullable()->after('description_chi');
            $table->string('subcategory_description', 255)->nullable()->after('description_ru');
            $table->string('subcategory_description_chi', 255)->nullable()->after('subcategory_description');
            $table->string('subcategory_description_ru', 255)->nullable()->after('subcategory_description_chi');
            $table->tinyInteger('status')->default('1')->comment("1=>active 0=>inactive 2=>deleted")->after('sgd_price');
            $table->dateTime('start_date')->nullable()->after('status');
            $table->dateTime('end_date')->nullable()->after('start_date');
            $table->string('next_grade_id', 255)->nullable()->after('end_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('twinkle_pricing_plan', function(Blueprint $table)
        {
            $table->dropColumn('category_name_chi');
            $table->dropColumn('category_name_ru');
            $table->dropColumn('bundle_category_id');
            $table->dropColumn('parent_id');
            $table->dropColumn('subcategory_name');
            $table->dropColumn('subcategory_name_chi');
            $table->dropColumn('subcategory_name_ru');
            $table->dropColumn('description_chi');
            $table->dropColumn('description_ru');
            $table->dropColumn('subcategory_description');
            $table->dropColumn('subcategory_description_chi');
            $table->dropColumn('subcategory_description_ru');
            $table->dropColumn('start_date');
            $table->dropColumn('end_date');
            $table->dropColumn('next_grade_id');
        });
    }
}
