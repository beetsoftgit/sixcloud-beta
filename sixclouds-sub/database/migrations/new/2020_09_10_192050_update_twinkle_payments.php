<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTwinklePayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('twinkle_payments', function (Blueprint $table) {
            $table->integer('switch_id')->nullable()->comment("this id is parent id for user switch category")->after('twinkle');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('twinkle_payments', function(Blueprint $table)
        {
            $table->dropColumn('switch_id');
        });
    }
}
