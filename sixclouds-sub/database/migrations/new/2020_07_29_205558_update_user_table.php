<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->tinyInteger('twinkle_status')->default('0')->comment("1:active,0:inactive,2:deleted, 3: suspended account by admin, 4: Email Link not verified, 5: OTP not verified")->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('twinkle_pricing_plan', function(Blueprint $table)
        {
            $table->dropColumn('twinkle_status');
        });
    }
}
