<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VideoViewsHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_views_history', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('video_views_id');
            $table->dateTime('start_time')->nullable();
            $table->dateTime('stop_time')->nullable();
            $table->tinyInteger('status')->default('0')->comment = "0:Start Time & Stop Time Pending; 1:Start Time & Stop Time Not Pending";
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('video_views_id')->references('id')->on('video_views')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_views_history');
    }
}
