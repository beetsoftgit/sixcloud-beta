<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColomnJobManage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('job_manage', function(Blueprint $table) {
            $table->integer('ignite_category_id')->nullable()->after('twinkle_category_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('job_manage', function(Blueprint $table) {
            $table->dropColumn('ignite_category_id');    
        });
    }
}
