<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update2TwinklePringingPlan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('twinkle_pricing_plan', function (Blueprint $table) {
            $table->Integer('subject_id')->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('twinkle_pricing_plan', function(Blueprint $table)
        {
            $table->dropColumn('subject_id');
        });
    }
}
