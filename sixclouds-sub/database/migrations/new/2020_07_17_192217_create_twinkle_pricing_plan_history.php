<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTwinklePricingPlanHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('twinkle_pricing_plan_history', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('category_id', 255)->nullable();
            $table->string('category_name', 255)->nullable();
            $table->string('description', 255)->nullable();
            $table->integer('usd_price')->nullable();
            $table->integer('sgd_price')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('twinkle_pricing_plan_history');
    }
}
