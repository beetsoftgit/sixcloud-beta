<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTwinkleCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('twinkle_pricing_plan', function (Blueprint $table) {
            $table->string('category_name_un', 255)->nullable()->after('category_name_ru');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('twinkle_pricing_plan', function(Blueprint $table)
        {
            $table->dropColumn('category_name_un');
        });
    }
}
