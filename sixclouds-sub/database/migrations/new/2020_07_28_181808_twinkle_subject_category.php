<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TwinkleSubjectCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('twinkle_subject', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('subject_name', 255)->nullable();
            $table->string('subject_display_name', 255)->nullable();
            $table->string('subject_image', 255)->nullable();
            $table->tinyInteger('status')->default('1')->comment("0=> inactive; 1=> active;");
            $table->string('audio_language', 255)->nullable();
            $table->string('subtitle_language', 255)->nullable();
            $table->string('zones', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('twinkle_subject');
    }
}
