<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TwinklePayment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('twinkle_payments', function (Blueprint $table) {
            $table->integer('id', true)->unsigned();
            $table->tinyInteger('payment_method')->nullable()->comment("1: Alipay, 2: Paypal, 3: Credit, 4 : Wechat, 5: strip");
            $table->integer('paid_by')->unsigned()->nullable()->comment("User id of buyer who has paid for escrow or gallery");
            $table->string('video_category_id', 255)->nullable()->comment("this is for purchase of ignite product");
            $table->string('twinkle_history_id', 255)->nullable();
            $table->string('video_category_name', 255)->nullable();
            $table->tinyInteger('distributor_id')->unsigned()->nullable()->comment("this will be the id of distributors table for ignite");
            $table->tinyInteger('distributer_team_member_id')->unsigned()->nullable()->comment("this refers to distributor team members id");
            $table->dateTime('subscription_end_date')->nullable()->comment("this will be the end date of ignite buzz subscription");
            $table->string('remarks', 255)->nullable()->comment("this field will contatin that whether the subscription payment for igniter is first time or renewal or promocode");
            $table->tinyInteger('access_platform')->nullable()->comment("1: web, 2: ios, 3: android, 4: wechat");
            $table->string('alipay_out_trade_no', 255)->nullable()->comment("this is the project id, that we send to alipay for uniqness");
            $table->string('alipay_trade_no', 255)->nullable()->comment("this is the number sent by alipay in return url");
            $table->string('transaction_reference', 255)->nullable()->comment("this is the transaction reference for paypal");
            $table->string('promo_code', 255)->nullable();
            $table->tinyInteger('discount_type')->nullable()->comment("0:percentage: 1:price");
            $table->float('discount_of', 10,2)->nullable()->comment("0:percentage: 1:price");
            $table->string('promo_code_id', 30)->nullable();
            $table->tinyInteger('paid_for')->default(1)->comment("1: escrow; 2: gallery, 3: Ignite");
            $table->double('payment_amount',10,2);
            $table->integer('payment_amount_type')->nullable()->comment("1: USD 2: SGD");
            $table->integer('payment_status')->nullable()->comment("1:success,2:Pending,3:fail");
            $table->string('alipay_seller_id', 255)->nullable();
            $table->dateTime('alipay_timestamp')->nullable();
            $table->string('alipay_method', 255)->nullable();
            $table->string('alipay_auth_app_id', 255)->nullable();
            $table->double('alipay_version', 10,2)->nullable();
            $table->string('alipay_app_id', 255)->nullable();
            $table->string('alipay_sign_type', 255)->nullable();
            $table->string('alipay_sign', 255)->nullable();
            $table->string('system_transaction_number', 255)->nullable();
            $table->string('slug', 255)->nullable();
            $table->tinyInteger('twinkle')->default(0)->comment("0:Ignite; 1:Twinkle");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('twinkle_payments');
    }
}
