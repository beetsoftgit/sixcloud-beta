<?php

//###############################################################
//File Name : VideoLanguageUrl.php
//Author : Senil Shah <Senil@creolestudios.com>
//Purpose : to store the videos of different languages
//Date : 17th July 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\VideoNotes;
use App\VideoCategory;

class VideoLanguageUrl extends Model {

    protected $table = 'video_language_urls';

    public function notes() {
        return $this->hasMany('App\VideoNotes', 'video_id');
    }

    public $rules = array(
        'video_id'       => 'required',
        'video_oss_url'  => 'required',
        'video_language' => 'required',
        'video_size'     => 'required',
        'video_duration' => 'required',
    );

    public function category() {
        return $this->belongsTo('App\VideoCategory', 'video_category_id');
    }
    public function views_detail() {
        return $this->belongsTo('App\VideoViews', 'id','video_url_id');
    }
}
