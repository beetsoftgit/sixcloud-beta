<?php

//###############################################################
//File Name : ZoneWorksheet.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : Pivot model between zone and worksheet
//Date : 13th Feb, 2019
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class ZoneWorksheet extends Model
{
    protected $table  = 'zone_worksheets';
    
    public function worksheet() {
        return $this->hasOne('App\Worksheets', 'id','worksheet_id');
    }

}
