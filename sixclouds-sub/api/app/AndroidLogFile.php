<?php

//###############################################################
//File Name : AndroidLogFile.php
//Author : senil Shah <senil@creolestudios.com>
//Purpose : related to log files from mobile side
//Date : 28th Sept, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\VideoNotes;
use App\VideoCategory;
use App\WorksheetDownloads;

class AndroidLogFile extends Model {

    protected $table = 'android_log_files';
}