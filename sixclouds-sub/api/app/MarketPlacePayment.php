<?php

//###############################################################
//File Name : MarketPlacePayment.php
//Author : Nivedita <nivedita@creolestudios.com>
//Purpose : related to MP alipay payemnt
//Date : 9th May, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;

class MarketPlacePayment extends Model
{
    protected $table  = 'mp_payments';
    //protected $table  = 'payments_temp';

    public function projectpaymentdetailinfo()
    {
        return $this->hasOne('App\MarketPlaceProject', 'id', 'mp_project_id')->with('selected_designer','skills');
    }
    public function projectpaymentuserdetailinfo()
    {
        return $this->hasOne('App\User', 'id', 'paid_by');
    }
    public function userwalletdetail()
    {
        return $this->hasOne('App\MarketPlaceWallet', 'user_id', 'paid_by');
    }
    public function video_categories()
    {
        return $this->hasOne('App\VideoCategory', 'id', 'video_category_id');
    }
    public function distributor()
    {
        return $this->hasOne('App\Distributors', 'id', 'distributor_id');
    }
    public function distributor_team_member()
    {
        return $this->hasOne('App\DistributorsTeamMembers', 'id', 'distributer_team_member_id');
    }
}

