<?php

//###############################################################
//File Name : SphereMessageBoard.php
//Author : Jainam Shah
//Date : 15th March, 2019
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class SphereMessageBoard extends Model
{
    protected $table  = 'sphere_message_boards';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'teacher_id',
    ];
}