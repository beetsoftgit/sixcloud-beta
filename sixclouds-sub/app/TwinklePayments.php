<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TwinklePayments extends Model
{
    protected $table = 'twinkle_payments';

    public function video_categories()
    {
        return $this->hasOne('App\TwinklePricingPlan', 'id', 'video_category_id');
    }
    public function distributor()
    {
        return $this->hasOne('App\Distributors', 'id', 'distributor_id');
    }
    public function distributor_team_member()
    {
        return $this->hasOne('App\DistributorsTeamMembers', 'id', 'distributer_team_member_id');
    }
    public function twinkle_history()
    {
        return $this->hasMany('App\TwinklePricingPlanHistory', 'id', 'video_category_id');
    }
    public function twinkle_category()
    {
        return $this->hasOne('App\TwinklePricingPlan', 'id', 'video_category_id');
    }
}
