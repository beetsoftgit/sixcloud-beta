<?php

//###############################################################
//File Name : VideoViews.php
//Author : Senil Shah <Senil@creolestudios.com>
//Purpose : to store the videos of different languages
//Date : 17th July 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\VideoNotes;
use App\VideoCategory;
use App\VideoViewsHistory;

class VideoViews extends Model {

    protected $table = 'video_views';

    public $rules = array(
        'video_id'     => 'required',
        'video_url_id' => 'required',
        'user_id'      => 'required',
    );

    public function views_detail() {
        return $this->belongsTo('App\VideoCategory', 'video_category_id');
    }
    public function video_details() {
        return $this->belongsTo('App\Videos', 'video_id','id')->with('subject_details');
    }
    public function video_details_with_category() {
        return $this->belongsTo('App\Videos', 'video_id','id')->with('category','subcategoryWithOnlyName','subject_details');
    }
    public function video_views_history_detail() {
        return $this->hasMany('App\VideoViewsHistory');
    }
    
}
