<?php

//###############################################################
//File Name : MarketPlaceProjectCompetionReqRejectReason.php
//Author : Komal Kapadi <komal@creolestudios.com>
//Purpose : contains reason to reject project completion request.
//Date : 4th May, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketPlaceProjectCompetionReqRejectReason extends Model
{
    protected $table = 'mp_prj_completion_reject';
    public $rules    = array(
        'designer_id'   => 'required',
        'mp_project_id' => 'required',
        'reason'        => 'required',
    );
}
