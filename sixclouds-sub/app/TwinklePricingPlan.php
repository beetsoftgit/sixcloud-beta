<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TwinklePricingPlan extends Model
{
    protected $table = 'twinkle_pricing_plan';

    public function payment_usd() {
        return $this->hasMany('App\MarketPlacePayment', 'video_category_id')->where('payment_amount_type', 1)->where('twinkle',1)->where('payment_status',1);
    }
    public function payment_sgd() {
        return $this->hasMany('App\MarketPlacePayment', 'video_category_id')->where('payment_amount_type', 2)->where('twinkle',1)->where('payment_status',1);
    }
    public function twinkle_history(){
    	return $this->hasOne('App\TwinklePricingPlanHistory', 'category_id');
    }
    public function twinkle_history_details(){
    	return $this->hasMany('App\TwinklePricingPlanHistory', 'category_id');
    }
    public function subcategories() {
        return $this->hasMany('App\TwinklePricingPlan', 'parent_id');
    }
    public function subject() {
        return $this->belongsTo('App\TwinkleSubject');
    }
}
