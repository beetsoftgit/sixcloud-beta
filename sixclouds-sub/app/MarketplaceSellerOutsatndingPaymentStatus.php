<?php

//###############################################################
//File Name : MarketplaceSellerOutsatndingPaymentStatus.php
//Author :Ramya Iyer <ramya@creolestudios.com>
//Purpose : related to designer  outstanding payment
//Date : 25th May, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketplaceSellerOutsatndingPaymentStatus extends Model
{
   protected $table = 'mp_seller_outstanding_payment_status';

   
    public function user()
    {
        return $this->hasOne('App\User','id','user_id');
    }
}
