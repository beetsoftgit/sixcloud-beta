<?php

//###############################################################
//File Name : MarketPlaceProject.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related to projects created by buyer from front end
//Date : 19th Mar, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;
class MarketPlaceProject extends Model
{
    protected $table = 'mp_projects';
    public $rules    = array(
        'project_created_by_id' => 'required',
        'project_title'         => 'required',
        'project_description'   => 'required',
        'project_budget'        => 'required',
        'project_timeline'      => 'required',
    );

    public function projectCreator()
    {
        return $this->belongsTo('App\User', 'project_created_by_id');
    }
    public function buyer()
    {
        return $this->belongsTo('App\User', 'project_created_by_id');
    }
    public function skills()
    {
        return $this->hasMany('App\ProjectSkill', 'mp_project_id')->with('skillDetail');
    }
    public function selected_designer()
    {
        return $this->hasOne('App\MarketPlaceProjectsDetailInfo', 'mp_project_id')->orderBy('id','DESC')->with('designer_details');
    }
    public function rating_for_project()
    {
        return $this->hasOne('App\MarketPlaceDesignerRating', 'mp_project_id');
    }
    public function review_for_project()
    {
        return $this->hasOne('App\MarketPlaceDesignerReview', 'mp_project_id');
    }
    public function message_board()
    {
        return $this->hasMany('App\MarketPlaceMessageBoard', 'mp_project_id')->with('user_details')->orderBy('id', 'DESC');
    }
    public function message_board_for_portfolio()
    {
        return $this->hasMany('App\MarketPlaceMessageBoard', 'mp_project_id')->where('type',2)->limit(4);
    }

    public function projectinvitation()
    {
        return $this->hasMany('App\MarketPlaceProjectsDetailInfo', 'mp_project_id');
    }
    public function projectdetailinfo()
    {
        return $this->hasOne('App\MarketPlaceProjectsDetailInfo', 'mp_project_id')->where('assigned_designer_id', Auth::user()->id)->orderBy('id','DESC');
    }
    public function projectattachment()
    {
        return $this->hasOne('App\MarketPlaceProjectAttachment', 'mp_project_id');
    }
    public function extension_details()
    {
        return $this->hasOne('App\MarketPlaceProjectExtension', 'mp_project_id')->orderBy('id','DESC');
    }
    public function seller_payment()
    {
        return $this->hasOne('App\MarketplaceSellerPayments', 'mp_project_id')->where('payment_status',1);
    }
    public function infovor_buyer_seller_payment()
    {
        return $this->hasOne('App\MarketplaceSellerPayments', 'mp_project_id');
    }
    public function admin_seller_payment()
    {
        return $this->hasOne('App\MarketplaceSellerPayments', 'mp_project_id')->where('payment_status',5);
    }
    public function skill_detail()
    {
        return $this->hasMany('App\ProjectSkill', 'mp_project_id')->with('skillDetail');   
    }
    public function priority_actioins()
    {
        return $this->hasOne('App\MarketplaceAction', 'mp_project_id');
    }
    public function projectdetail()
    {
        return $this->hasMany('App\MarketPlaceProjectsDetailInfo', 'mp_project_id')->orderBy('id', 'DESC');
    }

    /************************  Admin ************************************/
    public function adminskills()
    {
        return $this->hasMany('App\ProjectSkill', 'mp_project_id');
    }
    public function adminprojectdetailinfo()
    {
        return $this->belongsTo('App\MarketPlaceProjectsDetailInfo','id' ,'mp_project_id');
    }
    public function adminprojectcreator()
    {
        return $this->belongsTo('App\User', 'project_created_by_id');
    }
    public function adminProjectDesigner()
    {
        return $this->hasOne('App\MarketPlaceProjectsDetailInfo', 'mp_project_id')->with('designer_details');
    }
    public function adminprojectdetailinfobuyer()
    {
        return $this->hasMany('App\MarketPlaceProjectsDetailInfo' ,'mp_project_id');
    }
    public function project_detail_info_for_admin_detail_page()
    {
        return $this->hasMany('App\MarketPlaceProjectsDetailInfo' ,'mp_project_id')->with('designer_details_admin');
    }
    public function reportdispute()
    {
        return $this->hasOne('App\MarketPlaceReportDispute' ,'mp_project_id')->with(['reportedfor','reportedby']);
    }
    public function adminprojectpayments()
    {
        return $this->hasMany('App\MarketPlacePayment' ,'mp_project_id')->select('id','mp_project_id',DB::raw("SUM(alipay_total_amount_paid) as total_amount_spend"));
    }
    public function designer_suggestions()
    {
        return $this->hasMany('App\MarketPlaceDesignerSuggestionProject' ,'mp_project_id');
    }
    public function cart()
    {
        return $this->hasMany('App\MarketPlaceCart' ,'mp_project_id');
    }
    

}
