<?php

//###############################################################
//File Name : Userprcaselogs.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related to PR cases logs
//Date : 7th Dec 2017
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userprcaselog extends Model
{
    protected $table  = 'user_pr_cases_logs';

    public function accessor_information(){
    	return $this->hasOne('App\Accessor', 'id', 'accessor_id');
    }
    public function pr_case(){
    	return $this->hasOne('App\Userprcase', 'id', 'user_pr_case_id');
    }
}