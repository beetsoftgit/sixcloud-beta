<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TwinkleSubject extends Model
{
    protected $table = 'twinkle_subject';

    public function categories() {
        return $this->hasMany('App\TwinklePricingPlan', 'subject_id');
    }
}
