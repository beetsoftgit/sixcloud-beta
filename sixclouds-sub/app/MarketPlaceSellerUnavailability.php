<?php

//###############################################################
//File Name : MarketPlaceSellerUnavailability.php
//Author : Zalak <zalak@creolestudios.com>
//Purpose : 
//Date : 24th May 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketPlaceSellerUnavailability extends Model
{
    protected $table  = 'mp_seller_unavailability';

}