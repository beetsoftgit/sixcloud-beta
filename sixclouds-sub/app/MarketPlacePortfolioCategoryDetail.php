<?php

//###############################################################
//File Name : MarketPlacePortfolioCategoryDetail.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related to projects created by buyer from front end
//Date : 13th Apr, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketPlacePortfolioCategoryDetail extends Model
{
    protected $table = 'mp_portfolio_category_details';
    public $rules    = array(
        'mp_upload_id' => 'required',
        'categoty_id'  => 'required',
    );

    public function category_name()
    {
        return $this->hasOne('App\SkillSet', 'id','categoty_id');
    }
}
