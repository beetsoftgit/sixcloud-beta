<?php

//###############################################################
//File Name : MarketPlaceGalleryPurchases.php
//Author : Komal Kapadi <komal@creolestudios.com>
//Purpose : to get gallery purchases
//Date : 5th Dec 2017
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketPlaceGalleryPurchases extends Model
{
    protected $table  = 'mp_gallery_purchases';
}