<?php

namespace App\Http\Controllers;
use App\User;
use App\Teacher;
use App\SphereMessageBoard;
use App\SphereMessages;
use App\SphereSubjects;
use App\TeacherSubjects;
use App\MarketPlacePayment;
use App\SphereCourse;
use App\SphereSubscribedCourse;
use App\UserHistory;
use Auth;
use Carbon\carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Validator;
use Hash;


class SphereController extends BaseController
{
    //###############################################################
    //Function Name: Signup
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       Sphere module signup/registration function
    //In Params:     user entered data
    //Return:        json
    //Date:          1st Jan 2019
    //###############################################################
    public function Signup(Request $request){     
        try {
            $Input = Input::all();
            $rules = [
                'email_address'       => 'required|email|unique:users',
                'first_name'          => 'required',
                'last_name'           => 'required',
                'country_id'          => 'required|numeric',
                'provience_id'        => 'required|numeric',
                'city_id'             => 'required|numeric',
                'dob'                 => 'required',
                'phonecode'           => 'required',
                'contact'             => 'required',
                'sphere_role'         => 'required',
                'report_card'         => 'required',
                'school_name'         => 'sometimes|required',
                'child_dob'           => 'sometimes|required',
                'child_first_name'    => 'sometimes|required',
                'child_last_name'     => 'sometimes|required',
                'child_school_name'   => 'sometimes|required',
                'child_email_address' => 'sometimes|required|email',
            ];
            $messages = [
                'required' => ':attribute is required.',
                'email'    => 'enter a valid :attribute.',
                'unique'   => ':attribute already exists.',
                'numeric'  => 'Select valid :attribute.',
            ];
            $validator = Validator::make($Input, $rules, $messages);
            if($validator->fails()){
                $messages = $validator->messages()->all();
                $returnData = UtilityController::Generateresponse(false, $messages[0], 0);
            } else{
                /* Check if child's email address already exists in our records or not. This is done seperately from above validatios as we don't have child_email_address as field in database and laravel unique field works on database field name */
                if(array_key_exists('child_email_address', $Input)){
                    if(User::where('email_address',$Input['child_email_address'])->exists()){
                        throw new \Exception("Child's Email address already exists.");
                        
                    }
                }
                $age = Carbon::createFromFormat('d/m/Y',$Input['dob'])->age;
                if($age<16 && $Input['sphere_role']==0)
                    throw new \Exception('You should be of or above 16 years.');
                if($age<18 && $Input['sphere_role']==1)
                    throw new \Exception('Parent should be of or above 18 years.');
                if($Input['sphere_role']==1){
                    $childAge = Carbon::createFromFormat('d/m/Y',$Input['child_dob'])->age;
                    if($childAge<16)
                        throw new \Exception('Child should be of or above 16 years.');
                }
                \DB::beginTransaction();
                    $Input['display_name'] = $Input['first_name'].' '.$Input['last_name'];
                    $Input['user_number']  = UtilityController::GenerateRunningNumber('User','user_number',"U");
                    $Input['dob'] = Carbon::createFromFormat('d/m/Y',$Input['dob']);
                    $Input['is_sphere']    = 1;
                    $Input['current_language'] = 1;
                    if(array_key_exists('report_card', $Input)){
                        if (!empty($Input['report_card']) && !is_string($Input['report_card'])) {
                            if ($Input['report_card']->getClientSize() <= UtilityController::Getmessage('FILE_SIZE_2')) {
                                $Input['student_id_proof_original'] = $Input['report_card']->getClientOriginalName();
                                $extension = \File::extension($Input['report_card']->getClientOriginalName());
                                if (in_array($extension, array('jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG', 'pdf', 'PDF'))) {
                                    $imageName = date("dmYHis").rand();
                                    $Input['report_card']->move(public_path('').UtilityController::Getpath('ID_PROOF_ATTACHMENT_PATH'), $imageName);
                                    chmod(public_path('').UtilityController::Getpath('ID_PROOF_ATTACHMENT_PATH').$imageName, 0777);
                                    $Input['student_id_proof'] = $imageName;
                                } else {
                                    throw new \Exception(UtilityController::Getmessage('ONLY_JPG_PNG_JPEG_PDF'));
                                }
                            } else {
                                throw new \Exception($Input['report_card']->getClientOriginalName() . UtilityController::Getmessage('PROFILE_PHOTO_SIZE_LARGE'));
                            }
                        }
                    }
                    $resultParent = UtilityController::Makemodelobject($Input,'User');
                    if($Input['sphere_role']==1) {
                        // $childAge             = Carbon::parse($Input['child_dob'])->age;
                        // if($childAge<16)
                        //     throw new \Exception('Child should be of or above 16 years.');
                        // if(User::where('email_address',$Input['child_email_address'])->exists())
                        //     throw new \Exception('Child email address already exists');
                        $Input['parent_id']     = $resultParent['id'];
                        $Input['first_name']    = $Input['child_first_name'];
                        $Input['last_name']     = $Input['child_last_name'];
                        $Input['display_name']  = $Input['child_first_name'].' '.$Input['child_last_name'];
                        $Input['email_address'] = $Input['child_email_address'];
                        $Input['dob']           = Carbon::createFromFormat('d/m/Y',$Input['child_dob']);
                        $Input['school_name']   = $Input['child_school_name'];
                        $Input['user_number']   = UtilityController::GenerateRunningNumber('User','user_number',"U");
                        $Input['sphere_role']   = 0;
                        $resultChild            = UtilityController::Makemodelobject($Input,'User');
                        $slugChild['slug']      = UtilityController::Makeslug($resultChild['display_name'], '-').':'.base64_encode($resultChild['id']);
                        $resultChild            = UtilityController::Makemodelobject($slugChild,'User','',$resultChild['id']);
                    }
                    if($resultParent['sphere_role']==1 && !empty($resultParent) && !empty($resultChild)){
                        $returnData              = UtilityController::Generateresponse(true, 'SIGNUP_SUCCESS', 1);
                        $registerEmailTokenChild = self::Generaldata($resultChild,$Input['current_language'],1);
                        $resultEmailToken        = UtilityController::Makemodelobject($registerEmailTokenChild,'User','',$resultChild['id']);
                        $childVerificationURL    = action('MarketPlaceController@Registrationaccountverification', $registerEmailTokenChild);
                        $sendChildMail           = self::Sendmail($resultChild['email_address'],'Account Verification',1,1,$resultChild,$childVerificationURL);
                        \DB::commit();
                    } elseif ($resultParent['sphere_role']==0 && !empty($resultParent)) {
                        $returnData = UtilityController::Generateresponse(true, 'SIGNUP_SUCCESS', 1);
                        \DB::commit();
                    } else {
                        $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 0);
                    }
                    $registerEmailTokenParent = self::Generaldata($resultParent,$Input['current_language'],1);
                    $resultEmailToken         = UtilityController::Makemodelobject($registerEmailTokenParent,'User','',$resultParent['id']);
                    $parentVerificationURL    = action('MarketPlaceController@Registrationaccountverification', $registerEmailTokenParent);
                    $sendParentMail           = self::Sendmail($resultParent['email_address'],'Account Verification',1,1,$resultParent,$parentVerificationURL);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->Getmessage(), '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: Generaldata
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       make general data
    //In Params:     details
    //Return:        json
    //Date:          3rd Jan 2019
    //###############################################################
    public function Generaldata($data,$language,$which){     
        try {
            switch ($which) {
                case 1:
                    $returnData['slug']     = UtilityController::Makeslug($data['display_name'], '-').':'.base64_encode($data['id']);
                    $returnData['account_verification_token'] = rand().base64_encode($data['email_address']).date('dmYHis').rand();
                    $returnData['email'] = base64_encode($data['email_address']);
                    $returnData['language'] = $language;
                    break;
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return $returnData;
    }

    //###############################################################
    //Function Name: Sendmail
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       General mail function for sphere function
    //In Params:     email address and subject
    //Return:        json
    //Date:          3rd Jan, 2019
    //###############################################################
    public function Sendmail($email,$subject,$which,$language,$allData='',$link=''){     
        try {
            switch ($which) {
                case 1:
                    $registerEmail['user_name'] = $allData['first_name'].' '.$allData['last_name'];
                    $registerEmail['registerLink'] = $link;
                    $registerEmail['email_address'] = $email;
                    $registerEmail['current_language'] = $language;
                    
                    $registerEmail['subject'] = $subject;
                    $supportEmail = (UtilityController::getMessage('CURRENT_DOMAIN')=='cn'?'support@sixclouds.cn':'support@sixclouds.net');

                    $registerEmail['content']=$registerEmail['current_language']==1?'
                        Your account has been created. Please click on the button below to complete your registration process. 
                        <br><br><a href="'.$registerEmail['registerLink'].'" class="" style="color: #fff;
                                               background-color: #009bdd;
                                               border-color: #bd383e;
                                               text-decoration: none;display: inline-block;
                                               padding: 5px 10px;
                                               margin-bottom: 0;
                                               margin-top: 3px;
                                               font-size: 14px;
                                               font-weight: 400;
                                               line-height: 1.42857143;
                                               text-align: center;
                                               white-space: nowrap;
                                               vertical-align: middle;
                                               -ms-touch-action: manipulation;
                                               touch-action: manipulation;
                                               cursor: pointer;
                                               -webkit-user-select: none;
                                               -moz-user-select: none;
                                               -ms-user-select: none;
                                               user-select: none;
                                               background-image: none;
                                               border: 1px solid transparent;
                                               border-radius: 4px;">Click Here</a><br><br>
                        If the link does not work, copy the below link to the browser address bar.
                        <br><br>'.$registerEmail['registerLink'].'<br><br>
                        If you have any queries, please contact '.$supportEmail:'

                        您的帐号已经建立。 请点击以下链接完成注册。
                        <br><br><a href="'.$registerEmail['registerLink'].'" class="" style="color: #fff;
                                               background-color: #009bdd;
                                               border-color: #bd383e;
                                               text-decoration: none;display: inline-block;
                                               padding: 5px 10px;
                                               margin-bottom: 0;
                                               margin-top: 3px;
                                               font-size: 14px;
                                               font-weight: 400;
                                               line-height: 1.42857143;
                                               text-align: center;
                                               white-space: nowrap;
                                               vertical-align: middle;
                                               -ms-touch-action: manipulation;
                                               touch-action: manipulation;
                                               cursor: pointer;
                                               -webkit-user-select: none;
                                               -moz-user-select: none;
                                               -ms-user-select: none;
                                               user-select: none;
                                               background-image: none;
                                               border: 1px solid transparent;
                                               border-radius: 4px;">请点击这里</a><br><br>
                        如果链接不起作用，请将链接复制到浏览器地址栏。
                        <br><br>'.$registerEmail['registerLink'].'<br><br>
                        如果您有任何疑问，请联系 '.$supportEmail;

                    $registerEmail['footer_content']=$registerEmail['current_language']==1?'From,<br /> SixClouds Customer Support':'六云十六 ';

                    $registerEmail['footer']=$registerEmail['current_language']==1?'SixClouds':'六云';
                    Mail::send('emails.email_template', $registerEmail, function ($message) use ($registerEmail) {
                        $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                        $message->to($registerEmail['email_address'])->subject($registerEmail['subject']);

                    });
                    $returnData = 1;
                    break;
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return $returnData;
    }

    //###############################################################
    //Function Name: Getteacherslist
    //Author:        Jainam Shah
    //Purpose:       to get all the teachers list
    //In Params:     N/A
    //Return:        json
    //Date:          15th March, 2019
    //###############################################################
    public function Getteacherslist(Request $request) {     
        try {
            // $data = Teacher::withCount('unreadMsg')->with('cityState', 'country')->paginate($request->filters['per_page'], ['*'], 'page', $request->pageNumber);
            // $data = Teacher::withCount('unreadMsg')->with('cityState', 'country', 'subjects');
            $data = Teacher::with('cityState', 'country', 'subjects');
            if(in_array($request->filters, $request->all())) {
                if(array_key_exists('searchText', $request->filters) && $request->filters['searchText'] != '' 
                    && !empty($request->filters['searchText'])) {
                    $data = $data->where('first_name', 'like', '%'.$request->filters['searchText'].'%')
                            ->orWhere('last_name', 'like', '%'.$request->filters['searchText'].'%')
                            ->orWhere('display_name', 'like', '%'.$request->filters['searchText'].'%');
                }
                if(array_key_exists('selectedSubject', $request->filters) && $request->filters['selectedSubject'] != '' 
                    && !empty($request->filters['selectedSubject'])) {

                    $subId = $request->filters['selectedSubject'];
                    $data = $data->whereHas('subjects', function ($query) use ($subId) {
                        $query->where('subject_id', $subId);
                    });
                }
            }
            $data = $data->paginate($request->per_page, ['*'], 'page', $request->pageNumber)->toArray();
            $returnData = UtilityController::Generateresponse(true, 'TEACHERS_DATA', 1, $data);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return $returnData;
    }

    //###############################################################
    //Function Name: Getteacherchat
    //Author:        Jainam Shah
    //Purpose:       to get messages of user with this teacher
    //In Params:     teacher_id
    //Return:        json
    //Date:          15th March, 2019
    //###############################################################
    public function Getteacherchat(Request $request) {
        try {
            $userId = Auth::user()->id;
            $messageData = array('user_id' => $userId,
                                 'teacher_id' => $request->teacher_id,
                                 'message' => $request->msg);

            if(SphereMessageBoard::where('user_id', $messageData['user_id'])->where('teacher_id', $messageData['teacher_id'])->exists()) {
                $messageBoard = SphereMessageBoard::where('user_id', $messageData['user_id'])
                                ->where('teacher_id', $messageData['teacher_id'])->first();
            } else {
                $messageBoard = UtilityController::Makemodelobject($messageData, 'SphereMessageBoard');
            }
            $messageDetails = SphereMessages::where('sphere_message_board_id', $messageBoard->id)->orderBy('created_at', 'desc')->paginate(5, ['*'], 'page', $request->pageNumber);
            $messageData['details'] = $messageDetails;
            $messageData['message_board_id'] = $messageBoard->id;
            $messageData['total_unread'] = SphereMessages::where('sphere_message_board_id', $messageBoard->id)->where('status', 0)->count();
            
            if($messageData) {
                $returnData = UtilityController::Generateresponse(true, 'CHAT_DATA_SUCCESS', 1, $messageData);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'CHAT_DATA_FAILURE', '');
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return $returnData;
    }

    //###############################################################
    //Function Name: Sendmessage
    //Author:        Jainam Shah
    //Purpose:       to send message
    //In Params:     msg, teacher_id, sphere_message_board_id
    //Return:        json
    //Date:          15th March, 2019
    //###############################################################
    public function Sendmessage(Request $request) {
        try {
            $userId = Auth::user()->id;
            $messageData = array('sphere_message_board_id' => $request->sphere_message_board_id,
                                 'from_id' => $userId,
                                 'to_id' => $request->teacher_id,
                                 'message' => $request->message,
                                 'status' => 0);
            $sendMsg = UtilityController::Makemodelobject($messageData, 'SphereMessages');
            if($sendMsg) {
                $returnData = UtilityController::Generateresponse(true, 'MESSAGE_SENT', 1);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'MESSAGE_SENT_FAILURE', '');
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return $returnData;
    }

    //###############################################################
    //Function Name: Makemessageread
    //Author:        Jainam Shah
    //Purpose:       to change status of messages from unread to read
    //In Params:     sphere_message_board_id
    //Return:        json
    //Date:          18th March, 2019
    //###############################################################
    public function Makemessageread(Request $request) {
        try {
            $userId = Auth::user()->id;
            $messageData = array('sphere_message_board_id' => $request->sphere_message_board_id,
                                 'status' => 1);
            $readMsg = SphereMessages::where('sphere_message_board_id', $request->sphere_message_board_id)
                                    ->where('to_id', $userId)->update($messageData);
            if($readMsg) {
                $returnData = UtilityController::Generateresponse(true, 'MESSAGE_READ', 1);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'MESSAGE_READ_FAILURE', '');
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return $returnData;
    }

    //###############################################################
    //Function Name: Getspheresubjects
    //Author:        Jainam Shah
    //Purpose:       to get all the subjects list of sphere messages
    //In Params:     N/A
    //Return:        json
    //Date:          22nd March, 2019
    //###############################################################
    public function Getspheresubjects() {
        try {
            $subjects = SphereSubjects::all();
            if($subjects) {
                $returnData = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $subjects);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '');
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return $returnData;
    }

    //###############################################################
    //Function Name: Getteacherdata
    //Author:        Jainam Shah
    //Purpose:       to get all the details about the teacher
    //In Params:     slug
    //Return:        json
    //Date:          29th March, 2019
    //###############################################################
    public function Getteacherdata(Request $request) {
        try {
            $teacherDetails = Teacher::whereSlug($request->slug)->with('country', 'cityState', 'subjects.subjectName', 'education')->first();
            if($teacherDetails) {
                $returnData = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $teacherDetails);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '');
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return $returnData;
    }

    //###############################################################
    //Function Name: Getspheretransactions
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To get all the transaction related to sphere product. Also, includes search functionality.
    //In Params:     search string, page number
    //Return:        json
    //Date:          9th April, 2019
    //###############################################################
    public function Getspheretransactions(Request $request){     
        try {
            $user = Auth::user();
            $Input = Input::all();
            $transactions = MarketPlacePayment::where('paid_by',$user->id)->where('paid_for',4)->where('payment_status',1);
            if(array_key_exists('filters', $Input)){
                $transactions = $transactions->where('remarks','like','%'.$Input['filters'].'%');
            }
            $transactions = $transactions->orderBy('created_at','DESC')->paginate(Config('constants.other.MP_PAYMENT_HISTORY_PAGINATE'))->toArray();
            $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $transactions);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: Getspherehistories
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To get all the histories related to sphere product. Also, includes search functionality.
    //In Params:     search string, page number
    //Return:        json
    //Date:          9th April, 2019
    //###############################################################
    public function Getspherehistories(Request $request){     
        try {
            $user = Auth::user();
            $Input = Input::all();
            $histories = UserHistory::where('user_id',$user->id);
            if(array_key_exists('filters', $Input)){
                if(array_key_exists('searchText', $Input['filters']) && $Input['filters']['searchText']!=''){
                    $histories = $histories
                    ->where('history_message','like','%'.$Input['filters']['searchText'].'%')                    
                    ->orWhere('event','like','%'.$Input['filters']['searchText'].'%');
                }
                if(array_key_exists('course', $Input['filters']) && $Input['filters']['course']!=0){
                    $histories = $histories->where('course_id',$Input['filters']['course']);
                }
                if(array_key_exists('fromDate', $Input['filters'])){
                    $histories = $histories->where('created_at','>=', Carbon::createFromFormat('d/m/Y',$Input['filters']['fromDate']));
                }
                if(array_key_exists('toDate', $Input['filters'])){
                    $histories = $histories->where('created_at','<=', Carbon::createFromFormat('d/m/Y',$Input['filters']['toDate']));
                }
            }
            $histories = $histories->orderBy('created_at','DESC')->paginate(Config('constants.other.MP_PAYMENT_HISTORY_PAGINATE'))->toArray();
            $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $histories);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: Getmycourses
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To get the courses subscribed by the user
    //In Params:     void
    //Return:        json
    //Date:          9th April, 2019
    //###############################################################
    public function Getmycourses(Request $request){     
        try {
            $user = Auth::user();
            $myCourses = SphereSubscribedCourse::where('user_id',$user->id)->get();
            $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $myCourses);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getMessage(), '', '');
        }            
        return response($returnData);
    }
}
