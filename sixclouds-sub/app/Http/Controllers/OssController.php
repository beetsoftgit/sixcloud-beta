<?php

/**
 * Short description for file
 *
 * PHP version 5 and 7
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 */
/*
 * Place includes controller for login & forgot password.
 */

/**
Pre-Load all necessary library & name space
 */

namespace App\Http\Controllers;

//load required library by use
//load session & other useful library
use Auth;
use Illuminate\Routing\Controller as BaseController;
//define model
use OSS\Core\OssException;
use OSS\Core\OssUtil;
use OSS\OssClient;
use Mail;
/**
 * Photos
 * @package    OssController
 * @subpackage Controller
 * @author     Nivedita <nivedita@creolestudios.com>
 */
class OssController extends BaseController
{

    public function __construct()
    {
        //Artisan::call('cache:clear');
    }

    //###############################################################
    //Function Name : index
    //Author : Nivedita <nivedita@creolestudios.com>
    //Purpose : Simple object creation on oss
    //In Params : Void
    //Return :
    //Date : 20th December 2017
    //###############################################################
    public function index()
    {

        $accessKeyId     = "LTAIVnGSnA682rff";
        $accessKeySecret = "ADc23as20R8E4SuOnQTwV2k0WMoAsw";
        $endpoint        = "http://oss-cn-beijing.aliyuncs.com";
        $bucket          = "sixclouds88-bucket";
        $object          = "sixclouds-object";
        $content         = "Hi, OSS.";

        try {
            $ossClient = new OssClient($accessKeyId, $accessKeySecret, $endpoint);
            $ossClient->putObject($bucket, $object, $content);
        } catch (OssException $e) {
            print $e->getMessage();
            printf(__FUNCTION__ . ": FAILED\n");
            printf($e->getMessage() . "\n");
            return;
        }
    }

    //###############################################################
    //Function Name : Uploadfile
    //Author : Nivedita <nivedita@creolestudios.com>
    //Purpose : Sample file upload on oss inside a folder
    //In Params : Void
    //Return :
    //Date : 20th December 2017
    //###############################################################
    public function Uploadfile()
    {

        $accessKeyId     = "LTAIVnGSnA682rff";
        $accessKeySecret = "ADc23as20R8E4SuOnQTwV2k0WMoAsw";
        $endpoint        = "http://oss-cn-beijing.aliyuncs.com";
        $bucket          = "sixclouds88-bucket";

        /*$object = "sample_pdf"; //this will create one folder named oss-php-sdk-test and will upload file inside it
        $filePath = Config('constants.path.PR_DOCUMENT_UPLOAD_PATH').'sample.pdf'; //this is a demo file

        try {
        $ossClient = new OssClient($accessKeyId, $accessKeySecret, $endpoint);
        $ossClient->uploadFile($bucket, $object, $filePath);
        } catch (OssException $e) {
        print $e->getMessage();
        printf(__FUNCTION__ . ": FAILED\n");
        printf($e->getMessage() . "\n");
        return;
        }*/

        $object   = "oss-php-sdk-test/upload-test-object-name-sample.pdf";
        $filePath = Config('constants.path.PR_DOCUMENT_UPLOAD_PATH') . "sample.pdf";
        try {
            $ossClient = new OssClient($accessKeyId, $accessKeySecret, $endpoint);
            $ossClient->uploadFile($bucket, $object, $filePath);
        } catch (OssException $e) {
            printf(__FUNCTION__ . ": FAILED\n");
            printf($e->getMessage() . "\n");
            return;
        }
        print(__FUNCTION__ . ": OK" . "\n");
    }

    //###############################################################
    //Function Name : Downloadfile
    //Author : Nivedita <nivedita@creolestudios.com>
    //Purpose : Sample file download on oss inside a folder
    //In Params : Void
    //Return :
    //Date : 20th December 2017
    //###############################################################
    public function Downloadfile()
    {

        $accessKeyId     = "LTAIVnGSnA682rff";
        $accessKeySecret = "ADc23as20R8E4SuOnQTwV2k0WMoAsw";
        $endpoint        = "http://oss-cn-beijing.aliyuncs.com";
        $bucket          = "sixclouds88-bucket";

        $object    = "oss-php-sdk-test/upload-test-object-name-sample.pdf";
        $localfile = Config('constants.path.PR_DOCUMENT_UPLOAD_PATH') . "test-sample.pdf";
        $options   = array(
            OssClient::OSS_FILE_DOWNLOAD => $localfile,
        );
        try {
            $ossClient = new OssClient($accessKeyId, $accessKeySecret, $endpoint);
            $ossClient->getObject($bucket, $object, $options);
        } catch (OssException $e) {
            printf(__FUNCTION__ . ": FAILED\n");
            printf($e->getMessage() . "\n");
            return;
        }
        print(__FUNCTION__ . ": OK, please check localfile: 'upload-test-object-name-sample.pdf'" . "\n");
    }

    //###############################################################
    //Function Name : Partuploadfile
    //Author : Nivedita <nivedita@creolestudios.com>
    //Purpose : Sample video upload on oss inside a folder
    //In Params : Void
    //Return :
    //Date : 20th December 2017
    //###############################################################
    public function Partuploadfile()
    {

        $accessKeyId     = "LTAIVnGSnA682rff";
        $accessKeySecret = "ADc23as20R8E4SuOnQTwV2k0WMoAsw";
        $endpoint        = "http://oss-cn-beijing.aliyuncs.com";
        $bucket          = "sixclouds88-bucket";

        try {
            $ossClient = new OssClient($accessKeyId, $accessKeySecret, $endpoint);
            //$ossClient->uploadFile($bucket, $object, $filePath);
        } catch (OssException $e) {
            print $e->getMessage();
            printf(__FUNCTION__ . ": FAILED\n");
            printf($e->getMessage() . "\n");
            return;
        }

        $object   = "oss-php-sdk-test/demo_video.mp4"; //this will create one folder named oss-php-sdk-test and will upload file inside it
        $filePath = Config('constants.path.PR_DOCUMENT_UPLOAD_PATH') . 'demo_video.mp4'; //this is a demo file

        /**
         *  step 1. Initialize a block upload event, which is initialized to upload Multipart, get the upload id
         */
        try {
            $uploadId = $ossClient->initiateMultipartUpload($bucket, $object);
        } catch (OssException $e) {
            printf(__FUNCTION__ . ": initiateMultipartUpload FAILED\n");
            printf($e->getMessage() . "\n");
            return;
        }
        print(__FUNCTION__ . ": initiateMultipartUpload OK" . "\n");
        /*
         * step 2. Upload shards
         */
        $partSize           = 10 * 1024 * 1024;
        $uploadFile         = $filePath;
        $uploadFileSize     = filesize($uploadFile);
        $pieces             = $ossClient->generateMultiuploadParts($uploadFileSize, $partSize);
        $responseUploadPart = array();
        $uploadPosition     = 0;
        $isCheckMd5         = true;

        foreach ($pieces as $i => $piece) {
            $fromPos   = $uploadPosition + (integer) $piece[$ossClient::OSS_SEEK_TO];
            $toPos     = (integer) $piece[$ossClient::OSS_LENGTH] + $fromPos - 1;
            $upOptions = array(
                $ossClient::OSS_FILE_UPLOAD => $uploadFile,
                $ossClient::OSS_PART_NUM    => ($i + 1),
                $ossClient::OSS_SEEK_TO     => $fromPos,
                $ossClient::OSS_LENGTH      => $toPos - $fromPos + 1,
                $ossClient::OSS_CHECK_MD5   => $isCheckMd5,
            );
            if ($isCheckMd5) {
                $contentMd5                             = OssUtil::getMd5SumForFile($uploadFile, $fromPos, $toPos);
                $upOptions[$ossClient::OSS_CONTENT_MD5] = $contentMd5;
            }
            //2. Upload each slice to OSS
            try {
                $responseUploadPart[] = $ossClient->uploadPart($bucket, $object, $uploadId, $upOptions);
            } catch (OssException $e) {
                printf(__FUNCTION__ . ": initiateMultipartUpload, uploadPart - part#{$i} FAILED\n");
                printf($e->getMessage() . "\n");
                return;
            }
            printf(__FUNCTION__ . ": initiateMultipartUpload, uploadPart - part#{$i} OK\n");
        }
        $uploadParts = array();
        foreach ($responseUploadPart as $i => $eTag) {
            $uploadParts[] = array(
                'PartNumber' => ($i + 1),
                'ETag'       => $eTag,
            );
        }
        /**
         * step 3. Complete the upload
         */
        try {
            $ossClient->completeMultipartUpload($bucket, $object, $uploadId, $uploadParts);
        } catch (OssException $e) {
            printf(__FUNCTION__ . ": completeMultipartUpload FAILED\n");
            printf($e->getMessage() . "\n");
            return;
        }
        printf(__FUNCTION__ . ": completeMultipartUpload OK\n");
    }

    public function UploadVideo()
    {
        $accessKeyId     = "LTAIVnGSnA682rff";
        $accessKeySecret = "ADc23as20R8E4SuOnQTwV2k0WMoAsw";
        $endpoint        = "http://oss-cn-beijing.aliyuncs.com";
        $bucket          = "sixclouds88-bucket";

        /*$object = "sample_pdf"; //this will create one folder named oss-php-sdk-test and will upload file inside it
        $filePath = Config('constants.path.PR_DOCUMENT_UPLOAD_PATH').'sample.pdf'; //this is a demo file

        try {
        $ossClient = new OssClient($accessKeyId, $accessKeySecret, $endpoint);
        $ossClient->uploadFile($bucket, $object, $filePath);
        } catch (OssException $e) {
        print $e->getMessage();
        printf(__FUNCTION__ . ": FAILED\n");
        printf($e->getMessage() . "\n");
        return;
        }*/

        $object   = "elt_videos/retail_space_comp.mp4";
        $filePath = 'public' . Config('constants.path.ELT_VIDEO_PATH') . "retail_space_comp.mp4";
        try {
            $ossClient = new OssClient($accessKeyId, $accessKeySecret, $endpoint);
            $test      = $ossClient->uploadFile($bucket, $object, $filePath);
            if (!empty($test) && $test['info']['url']) {
                $data['username'] = 'komal';
                $data['link']     = $test['info']['url'];
                $data['email']    = 'komal@creolestudios.com';
                $mailResult       = Mail::send('front.emails.forgotpassword', $data, function ($message) use ($data) {
                    $message->from(Config('constants.messages.MAIL_ID'), 'Sixclouds');
                    $message->to('komal@creolestudios.com')->subject('Sixclouds : Forgot password');
                });
                $returnData = UtilityController::Generateresponse(true, 'FORGOT_PASSWORD_SUCCESS', 200, '');
            }
        } catch (OssException $e) {
            printf(__FUNCTION__ . ": FAILED\n");
            printf($e->getMessage() . "\n");
            return;
        }
        print(__FUNCTION__ . ": OK" . "\n");
    }
}
