<?php

/**
 * Short description for file
 *
 * PHP version 5 and 7
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 */
/*
 * Place includes controller for login & forgot password.
 */

/**
  Pre-Load all necessary library & name space
 */

namespace App\Http\Controllers;

//load required library by use
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
//load authorization library
use Auth;
use View;
use Hash;
//load session & other useful library
use Session;
use Carbon\Carbon;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
//define model
use App\User;
use App\Distributors;
use App\DistributorsTeamMembers;

/**
 * Photos
 * @package    UserLoginController
 */
class UserLoginController extends BaseController {

    public function __construct() {
        //Artisan::call('cache:clear');        
    }

    //###############################################################
    //Function Name : index
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To load login view
    //In Params : Void
    //Return : Login HTML View
    //###############################################################
    public function index() {
        
    }

    //###############################################################
    //Function Name : Dosignin
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To login the admin
    //In Params : email,password
    //Return : success & redirect to dashboard/Error on same page
    //###############################################################
    public function Dosignin(Request $request) {
        try {
            $validator = UtilityController::ValidationRules($request->all(), 'User', array('first_name', 'last_name', 'country_id', 'city_id'));
            $Input = Input::all();
            $returnData = $validator;
            $isLoggedin = 0;
            if (!$validator['status']) {
                $returnData = UtilityController::Setreturnvariables();
            } else {
                if (Auth::check()) {
                    $isLoggedin = 1;
                }
                if ($isLoggedin == 1) {
                    $returnData = UtilityController::Generateresponse(true, 'LOGIN_SUCCESS', 200, Auth::user());
                } else { //check user's credentials
                    $credentials = [
                        "email_address" => Input::get("email_address"),
                        "password" => Input::get("password"),
                        "status" => 1
                    ];
                    if (Auth::attempt($credentials)) {
                        if(array_key_exists('timezone', $Input)){
                            $timezoneResult = User::where('id',Auth::user()->id)->update(array('timezone' => $Input['timezone']));
                        }
                        $returnData = UtilityController::Generateresponse(true, 'LOGIN_SUCCESS', 200, Auth::user());
                    } else {
                        $userdata = User::where('email_address',Input::get("email_address"))->where('status',0)->exists();
                        if ($userdata) {
                            $returnData = UtilityController::Generateresponse(false, 'ACCOUNT_NOT_VERIFIED', 0);
                        } else {
                            $returnData = UtilityController::Generateresponse(false, 'LOGIN_FAIL', 204, Auth::user());
                        }
                    }
                }
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Forgotpassword
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To send link for forgot password
    //In Params : email
    //Return : success/error message for emiail sent or not
    //###############################################################
    public function Forgotpassword(Request $request) {
        try {
            $Input = Input::all();
            if(!array_key_exists('module', $Input))
                $validator = UtilityController::ValidationRules($request->all(), 'User', array('first_name', 'last_name', 'password', 'country_id', 'city_id'));
            else
                if(array_key_exists('email_address', $Input))
                    $validator['status'] = 1;
                else
                    throw new \Exception("Enter Eamil Address");
            $returnData = $validator;
            if (!$validator['status']) {
                $errorMessage = explode('.', $validator['message']);
                $returnData = UtilityController::Generateresponse(false,$errorMessage['0'],0);
            } else {
                //update password token and send email for forgot password
                //get email address
                $userEmail = (!empty(Input::get('email_address'))?Input::get('email_address'):Input::get('email'));
                /* Generate random string with 25 charater. */
                $forgetToken = str_random(25);
                /* Generate the link with parameter. */
                $emailParams = array('reset_token' => $forgetToken, 'email' => base64_encode($userEmail),'language'=>$Input['language'],'redirectTo'=>$Input['redirectTo']);
                if(array_key_exists('module', $Input))
                    $emailParams['module'] = $Input['module'];
                $resetURL = action('UserLoginController@Resetpassword', $emailParams);
                /* Update the user with forgot token. */
                //get user object for admin user
                if(!array_key_exists('module', $Input))
                    $userObject = User::where('email_address', $userEmail)->first();
                else{
                    if($Input['module']=='User')
                        $userObject = User::where('email_address', $userEmail)->first();
                    if($Input['module']=='Distributors')
                        $userObject = Distributors::where('email_address', $userEmail)->first();
                    if($Input['module']=='DistributorsTeamMembers')
                        $userObject = DistributorsTeamMembers::where('email_address', $userEmail)->first();
                }
                if(!empty($userObject)){
                    if(array_key_exists('module', $Input)&&$Input['module']=='User'&&$userObject['status']==3){
                        $returnData = UtilityController::Generateresponse(false, 'ACCOUNT_SUSPENDED_BY_ADMIN', 0, '');
                    } else {
                        //update forgot token
                        $userObject->reset_token = $forgetToken;
                        //if token is saved then send email
                        if ($userObject->save()) {
                            //prepare parameter to send in email
                            $data['username'] = $userObject->first_name . ' ' . $userObject->last_name;
                            $data['link'] = $resetURL;
                            $data['email'] = (!empty(Input::get('email_address'))?Input::get('email_address'):Input::get('email'));
                            if(!array_key_exists('module', $Input))
                                $data['current_language'] = $userObject->current_language;
                            else
                                $data['current_language']= $Input['language']=='en'?1:($Input['language']=='chi'?2:3);

                            $data['subject']=$data['current_language']==1?'Your request to reset your password':($data['current_language']==2?'您重置密码的请求':'Ваш запрос на смену пароля');

                            $mailResult = Mail::send('front.emails.forgotpassword', $data, function($message) use ($data) {
                                        $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                                        $message->to($data['email'])->subject($data['subject']);
                                    });
                            
                            $returnMessage = $data['current_language']==1?'FORGOT_PASSWORD_SUCCESS':($data['current_language']==2?'FORGOT_PASSWORD_SUCCESS_CHINESE':'FORGOT_PASSWORD_SUCCESS_RU');

                            $returnData = UtilityController::Generateresponse(true, $returnMessage, 200, '');
                        } else {
                            $returnData = UtilityController::Generateresponse(true, 'GENERAL_ERROR', 200, '');
                        }
                    }
                } else {
                    //$returnMessage = $Input['language']=='en'?'INVALID_EMAIL':'INVALID_EMAIL_CHINESE';
                    $returnMessage = $Input['language']=='en'?'INVALID_EMAIL':($Input['language']=='chi'?'INVALID_EMAIL_CHINESE':'INVALID_EMAIL_RU');
                    $returnData = UtilityController::Generateresponse(false, $returnMessage, 0, '');
                }
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Resetpassword
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To send reset password link for admin
    //In Params : Email
    //Return : success with email send/error message
    //###############################################################
    public function Resetpassword(Request $Request) {
        //get token and email from url
        $ForgotToken = $Request->get('reset_token');
        $Email = base64_decode($Request->get('email'));
        $Input = Input::all();
        if(isset($Input['language']))
            $Input['current_language'] = $Input['language'];
        //check if the link is expired or not
        if(!array_key_exists('module', $Input)){
            $userObject = User::where('email_address', $Email)->where('reset_token', $ForgotToken)->exists();
        } else {
            if($Input['module']=='User')
                $userObject = User::where('email_address', $Email)->where('reset_token', $ForgotToken)->exists();
            if($Input['module']=='Distributors')
                $userObject = Distributors::where('email_address', $Email)->where('reset_token', $ForgotToken)->exists();
            if($Input['module']=='DistributorsTeamMembers')
                $userObject = DistributorsTeamMembers::where('email_address', $Email)->where('reset_token', $ForgotToken)->exists();
        }
        if ($userObject) {
            //user data is found, proceed for reset password
            if(!array_key_exists('module', $Input))
                return view::make("front.reset_password")->with('email', $Email);
            else
                return view::make("front.reset_password")->with('email', $Email)->with('module',$Input['module'])->with('language',$Input['language'])->with('redirectTo',$Input['redirectTo']);
        } else {
            //load view for url expired
            return view::make("front.urlexpired")->with('current_language',$Input['current_language'])->with('resetExpired',1);
        }
    }

    //###############################################################
    //Function Name : Doresetpassword
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To reset password for admin
    //In Params : password
    //Return : success/error message
    //###############################################################
    public function Doresetpassword(Request $Request) {
        try {
            $PostData = Input::all();
            if(!array_key_exists('module', $PostData))
                $validator = UtilityController::ValidationRules($PostData, 'User');
            else
                $validator['status'] = 0;
            $returnData = $validator;


            if ($validator['status']) {
                if(!array_key_exists('module', $PostData))
                    $client = UtilityController::MakeObjectFromArray($PostData, 'User');
                else
                    $client = UtilityController::MakeObjectFromArray($PostData, $PostData['module']);
            } else {
                //make password
                $NewPassword = Hash::make(Input::get('password'));
                $TwinklePassword = base64_encode(Input::get('password'));
                //update new password for user
                //get user object for admin user
                if(!array_key_exists('module', $PostData)){
                    $userObject = User::where('email_address', $PostData['email'])->first();
                } else {
                    if($PostData['module']=='User')
                        $userObject = User::where('email_address', $PostData['email'])->first();
                    if($PostData['module']=='Distributors')
                        $userObject = Distributors::where('email_address', $PostData['email'])->first();
                    if($PostData['module']=='DistributorsTeamMembers')
                        $userObject = DistributorsTeamMembers::where('email_address', $PostData['email'])->first();
                }
                //update forgot token
                $userObject->password = $NewPassword;
                $userObject->twinkle_password = $TwinklePassword;
                $userObject->reset_token = '';

                //if token is saved then send email
                if ($userObject->save()) {
                    // if(!array_key_exists('module', $PostData))
                    //     $PostData['module'] = 'sixteen';
                    if($userObject->is_ignite==1)
                        $PostData['module'] = 'User';
                    else
                        $PostData['module'] = 'sixteen';

                    $request_array = array();
                    $request_array["token"]    = Config('constants.proprofs.API_KEY');
                    $request_array["username"] = Config('constants.proprofs.USERNAME');
                    $request_array["email"]    = $userObject->email_address;
                    $request_array["password"] = Input::get('password');
                    $json = json_encode($request_array);
                    $curl_obj = curl_init();
                    curl_setopt($curl_obj, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($curl_obj, CURLOPT_URL, "https://api.proprofs.com/classroom/v3/create/register");
                    curl_setopt($curl_obj, CURLOPT_POSTFIELDS, $json);  
                    curl_setopt($curl_obj, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($curl_obj, CURLOPT_POST, 1);
                    curl_setopt($curl_obj, CURLOPT_HEADER, 0);
                    curl_setopt($curl_obj, CURLOPT_HTTPHEADER,
                        array(
                            'Content-Type:application/json',
                        ) 
                    );  
                    curl_exec($curl_obj);
                    $returnMessage = isset($PostData['language'])&&$PostData['language']=='en'?'PASSWORD_UPDATED_SUCCESSFULLY':'PASSWORD_UPDATED_SUCCESSFULLY_CHINESE';
                    $returnData = UtilityController::Generateresponse(true, $returnMessage, 200, $PostData['module']);
                } else {
                    $returnData = UtilityController::Generateresponse(true, 'GENERAL_ERROR', 200, '');
                }
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Logout
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To logout user from admin panel
    //In Params : void
    //Return : logout & redirect user to login screen,
    //###############################################################
    public function Logout() {
        //logout user
        $responseArray = array();
        $responseArray['success'] = false;
        if (!Auth::logout()) {
            $responseArray['status'] = true;
            $responseArray['message'] = UtilityController::Getmessage('LOGOUT_SUCCESS');
            //redirect user to login screen
        } else {
            $responseArray['status'] = true;
            $responseArray['message'] = UtilityController::Getmessage('LOGOUT_SUCCESS');
        }
        return $responseArray;
    }
}
