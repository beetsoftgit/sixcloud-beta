<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;

use Closure;

class IsDistributor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::guard('distributors')->check()){
            if(isset(Auth::guard('distributors')->user()->userType)&&Auth::guard('distributors')->user()->userType=='distributor') {
                return redirect('/subscribers');
            }
        } else {
            if(Auth::check())
                return redirect('/ignite-categories');
            elseif(Auth::guard('distributor_team_members')->check())
                return redirect('/my-referral');
            else
                return redirect('/distributor');
        }
        return $next($request);
    }
}
