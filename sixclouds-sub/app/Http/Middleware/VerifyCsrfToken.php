<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'Dosignup',
        'checkuseremail',
        'Getcities',
        'Dosignin',
        'forgotpassword',
        'Doresetpassword',
        'Fileupload',
        'Accessedfile',
        'testThumbnail',
        'Createnewproject',
        'Createnewdesigner',
        'Getdesigners',
        'Fileupload',
        'manage/Dologin',
        'manage/Doprlogin',
        'manage/logout',
        'Doadminresetpassword',
        'Doadminprresetpassword',
        'Addaccessor',
        'Addnewvideo',
        'Editadminprofile',
        'UploadVideo',
        'Getprojectdetail',
        'Getschools',
        'Getskills',
        'Adddesignerreview',
        'Getdesignerreview',
        'Activeprojects',
        'Openprojects',
        'Gallerydetail',
        'Getgallerylisting',
        'Getgallerypurchases',
        'Designerinsbankupload',
        'Designergallerynewupload',
        'Uploadmessagefile',
        'Uploadmessagefileadmin',
        'Getlanguages',
        'Designerpastprojects',
        'Actions',
        'Userdata',
        'paypal-payment-details',
        'paypal-payment-details-ipn',
        'Signup',
        'payment-process'
    ];
}
