<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TwinklePricingPlanHistory extends Model
{
    protected $table = 'twinkle_pricing_plan_history';

    public function payment_usd() {
    	return $this->hasMany('App\TwinklePayments', 'twinkle_history_id')->where('payment_amount_type', 1)->where('twinkle',1)->where('payment_status',1);
    }
    public function payment_sgd() {
        return $this->hasMany('App\TwinklePayments', 'twinkle_history_id')->where('payment_amount_type', 2)->where('twinkle',1)->where('payment_status',1);
    }
    public function twinkle_pricing_plan() {
        return $this->hasMany('App\TwinklePricingPlan', 'id','category_id');
    }
}
