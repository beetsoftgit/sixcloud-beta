<?php

namespace App\Jobs;
// namespace App\Http\Controllers;
use App\Userprcase;
use App\Userprcaselog;
use App\Prcaseassignment;
use App\Accessor;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Database\Eloquent\Collection;

class CaseAssignment implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $case_id;
    protected $accessor_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($case_id,$accessor_id)
    {
        $this->case_id = $case_id;
        $this->accessor_id = $accessor_id;
    }
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 50;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Prcaseassignment $prcaseassignment)
    {
        $freeAccessor = Accessor::where('level_type',1)->where('availability_status',1);
        if(!empty($this->accessor_id)){
            $notToConsiderAccessors                             = Prcaseassignment::select(['accessor_id'])->where('user_pr_case_id',$this->case_id)->get();
            $freeAccessor                                       = $freeAccessor->whereNotIn('id',$notToConsiderAccessors);
            $setAccessorAvailabilityData['availability_status'] = 1;
            $setAccessorAvailability                            = \App\Http\Controllers\UtilityController::Makemodelobject($setAccessorAvailabilityData,'Accessor','',$this->accessor_id);
        }
        $freeAccessor = $freeAccessor->first();
        if($freeAccessor == null){
            $statusChange['cases_type'] = 2;
            $specialCase                = \App\Http\Controllers\UtilityController::Makemodelobject($statusChange,'Userprcase','',$this->case_id);
        } else {
            $freeAccessor = json_decode(json_encode($freeAccessor),true);
            $assignment = array('accessor_id' => $freeAccessor['id'], 'user_pr_case_id'=> $this->case_id);
            $result = \App\Http\Controllers\UtilityController::Makemodelobject($assignment,'Prcaseassignment');
            $setAccessorAvailabilityData['availability_status'] = 2;
            $setAccessorAvailability                            = \App\Http\Controllers\UtilityController::Makemodelobject($setAccessorAvailabilityData,'Accessor','',$freeAccessor['id']);
            Mail::send('emails.notify_accessor', $freeAccessor, function($message) use ($freeAccessor) {
                    $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                    $message->to($freeAccessor['email_address'])->subject('You have been assigned a case');
                });
            $job = (new NextAccessorAssignment($assignment))->delay(Carbon::now()->addMinutes(1));
            dispatch($job);
        }
    }
}
