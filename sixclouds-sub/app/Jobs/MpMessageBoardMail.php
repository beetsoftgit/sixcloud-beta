<?php

namespace App\Jobs;

use App\MarketPlaceMessageBoard;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class MpMessageBoardMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;
    protected $messageData;
    protected $mp_project_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user, $messageData, $mp_project_id)
    {
        $this->user          = $user;
        $this->messageData   = $messageData;
        $this->mp_project_id = $mp_project_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {

            $messageQuery            = MarketPlaceMessageBoard::where('mp_project_id', $this->mp_project_id)->where('user_id', $this->messageData['sender_id']);

            $messageQueryFile            = MarketPlaceMessageBoard::where('mp_project_id', $this->mp_project_id)->where('user_id', $this->messageData['sender_id']);

            if($this->messageData['user_role'] == 2){
                $messageQuery->where('message_status_seller', 2);
                $messageQueryFile->where('message_status_seller', 2);
            }
            if($this->messageData['user_role'] == 1){
                $messageQuery->where('message_status_buyer', 2);
                $messageQueryFile->where('message_status_buyer', 2);
            }

            $data['unreadCount']     = $messageQuery->where('type', 1)->count();
            $data['unreadFileCount'] = $messageQueryFile->whereIn('type', [2,3])->count();

            $messages                = $messageQuery->get();
            
            $data['first_name']      = $this->user['first_name'];
            $data['last_name']       = $this->user['last_name'];
            if (!empty($messages)) {
                $data['current_language'] = $this->user['current_language'];
                Mail::send('emails.MpMessageBorad', $data, function ($message) use ($data) {
                    $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                    $message->to($this->user['email_address'])->subject('Unread messages');
                });
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = \App\Http\Controllers\UtilityController::Sendexceptionmail($e);
        }
    }
}
