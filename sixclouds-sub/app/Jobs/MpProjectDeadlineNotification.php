<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use App\MarketPlaceProject;
use App\MarketPlaceProjectsDetailInfo;
use Carbon\carbon;

class MpProjectDeadlineNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $mp_project_id;
    protected $delayDays;
    protected $diffDays;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($mp_project_id,$delayDays,$diffDays)
    {
        $this->mp_project_id = $mp_project_id;
        $this->delayDays = $delayDays;
        $this->diffDays = $diffDays;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            $mp_project_id = $this->mp_project_id;
            $delayDays = $this->delayDays;
            $diffDays = $this->diffDays;
            $project = MarketPlaceProjectsDetailInfo::with('project_details','designer_details')->where('id',$mp_project_id)->orderBy('id','DESC')->first();
            $project = json_decode(json_encode($project),true);
            if(($project['project_details']['project_timeline']+$project['project_details']['extended_days'])-\App\Http\Controllers\UtilityController::Getmessage($diffDays) == $delayDays) {
                if($project['status'] == 1) {
                    $data['designer']['first_name']       = $project['designer_details']['first_name'];
                    $data['designer']['last_name']        = $project['designer_details']['last_name'];
                    $data['designer']['email_address']    = $project['designer_details']['email_address'];
                    $data['designer']['project_title']    = $project['project_details']['project_title'];
                    $data['designer']['project_number']    = $project['project_details']['project_number'];
                    $data['designer']['current_language'] = $project['project_details']['current_language'];
                    $data['designer']['deadline_days']    = \App\Http\Controllers\UtilityController::Getmessage($diffDays);

                    $data['subject']=$data['designer']['current_language']==1?'Project Deadline Near':'项目截止日期临近';

                    $data['content']=$data['designer']['current_language']==1?'Hello <strong>'.$data['designer']['first_name'].' '.$data['designer']['last_name'].' ,
                        </strong><br/>
                        <br/>
                        Deadline for project <strong> ID : '.$data['designer']['project_number'].'</strong>  is after '.$data['designer']['deadline_days'].' days. <br/><br/><br/>':'你好 <strong>'.$data['designer']['first_name'].$data['designer']['last_name'].'</strong> ,
                        <br/>
                        <br/>
                        项目截止日期<strong>ID : '.$data['designer']['project_number'].'</strong>之后'.$data['designer']['deadline_days'].'天.<br/><br/><br/>';

                    $data['footer_content']=$data['designer']['current_language']==1?'From,<br /> SixClouds Sixteen':'六云十六 ';

                    $data['footer']=$data['designer']['current_language']==1?'SixClouds':'六云';
                    Mail::send('emails.email_template', $data, function ($message) use ($data) {
                            $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                            $message->to($data['designer']['email_address'])->subject($data['subject']);
                    });
                    $data['designer']['first_name']       = $project['project_details']['project_creator']['first_name'];
                    $data['designer']['last_name']        = $project['project_details']['project_creator']['last_name'];
                    $data['designer']['email_address']    = $project['project_details']['project_creator']['email_address'];
                    $data['designer']['project_title']    = $project['project_details']['project_title'];
                    $data['designer']['current_language'] = $project['project_details']['current_language'];
                    $data['designer']['deadline_days']    = \App\Http\Controllers\UtilityController::Getmessage($diffDays);

                    $data['subject']=$data['designer']['current_language']==1?'Project Deadline Near':'项目截止日期临近';

                    $data['content']=$data['designer']['current_language']==1?'Hello <strong>'.$data['designer']['first_name'].' '.$data['designer']['last_name'].' ,
                        </strong><br/>
                        <br/>
                        Deadline for project <strong> ID : '.$data['designer']['project_number'].'</strong>  is after '.$data['designer']['deadline_days'].' days. <br/><br/><br/>':'你好 <strong>'.$data['designer']['first_name'].$data['designer']['last_name'].'</strong> ,
                        <br/>
                        <br/>
                        项目截止日期<strong>ID : '.$data['designer']['project_number'].'</strong>之后'.$data['designer']['deadline_days'].'天.<br/><br/><br/>';

                    $data['footer_content']=$data['designer']['current_language']==1?'From,<br /> SixClouds Sixteen':'六云十六 ';

                    Mail::send('emails.email_template', $data, function ($message) use ($data) {
                            $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                            $message->to($data['designer']['email_address'])->subject($data['subject']);
                    });
                }
            } else {
                $delayDays = ($project['project_details']['project_timeline']+$project['project_details']['extended_days'])-\App\Http\Controllers\UtilityController::Getmessage($diffDays);
                $notifyDesigner = (new MpProjectDeadlineNotification($project['project_details']['id'],$delayDays,0))->delay(Carbon::now()->addDays($delayDays));
                dispatch($notifyDesigner);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = \App\Http\Controllers\UtilityController::Sendexceptionmail($e);
        }
    }
}
