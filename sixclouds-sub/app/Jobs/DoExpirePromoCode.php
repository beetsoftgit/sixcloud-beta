<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\PromoCode;

class DoExpirePromoCode implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $arrayId, $toDate;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($arrayId, $toDate)
    {
        $this->arrayId = $arrayId;
        $this->toDate = $toDate;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            $arrayId = $this->arrayId;
            $toDate = $this->toDate;
            PromoCode::whereIn('id',$arrayId)->where('to_date',$toDate)->update(['status'=>3]);
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = \App\Http\Controllers\UtilityController::Sendexceptionmail($e);
        }
    }
}
