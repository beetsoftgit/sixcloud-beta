<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\JobManage;

class SwichTwinkleCategory implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $next_grade_id;
    public $category_id;
    public $category_name;
    public $user_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->next_grade_id = $request['next_grade_id'];
        $this->category_id   = $request['category_id'];
        $this->category_name = $request['category_name'];
        if(isset($request['user_id']))
        {
            $this->user_id       = $request['user_id'];
        }   
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            $result['category_id']      = $this->category_id;
            $result['category_name']    = $this->category_name;
            $result['next_grade_id']    = $this->next_grade_id;
            if(!empty($this->user_id) && isset($this->user_id))
            {
                $result['user_id']      = $this->user_id;
            }

            #Controller call in AdminIgniteController
            \App\Http\Controllers\AdminIgniteController::JobManageId($result);
            
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = \App\Http\Controllers\UtilityController::Sendexceptionmail($e);
        }
    }
}
