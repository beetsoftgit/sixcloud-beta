<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class TaxRateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $result;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->result = $request;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            $result = $this->result;
            #Controller call in IgniteController
            \App\Http\Controllers\AdminIgniteController::IgniteTextRateUpdate($result);
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = \App\Http\Controllers\UtilityController::Sendexceptionmail($e);
        }
    }
}
