<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\TwinklePayments;
use App\TwinklePricingPlan;
use App\User;
use Carbon\carbon;

class RemoveTwinkleSubscription implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $id,$subscription_end_date,$category_name,$subscriberId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->id                    = $request['id'];
        $this->subscription_end_date = $request['subscription_end_date'];
        $this->subscriberId          = $request['subscriberId'];
        $this->category_name         = $request['category_name'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            $effectiveDate = Carbon::createFromFormat('d/m/Y',$this->subscription_end_date);
            $effectiveDate->hour = 23;
            $effectiveDate->minute = 59;
            $effectiveDate->second = 59;
            $storeAccess['subscription_end_date'] = $effectiveDate;
            $storeAccess['updated_at'] = Carbon::now();
            if(!empty($storeAccess)){
                \DB::beginTransaction();
                $user    = User::find($this->subscriberId)->toArray();
                $payment = TwinklePayments::find($this->id)->toArray();
                $unique_name = TwinklePricingPlan::where('id',$payment['video_category_id'])->get()->pluck('category_name_un')->toArray();
                $request_array = array();
                $request_array["token"]     = Config('constants.proprofs.API_KEY');
                $request_array["username"]  = Config('constants.proprofs.USERNAME');
                $request_array["email"]     = $user['email_address'];
                $request_array["id"]        = $user['user_number'];
                $request_array["status"]    = "active";
                $request_array["expire"]    = Carbon::parse($effectiveDate)->format('m/d/Y');
                $request_array["group"]     = Array('group1' => 
                                                Array(
                                                    'group_name' => $unique_name[0],     
                                                    'group_status' => 'active',       
                                                    'can_assign_user_to_group' => '0',
                                                    'can_grade_reports' => '0',
                                                    'is_part_of' => '0'
                                                )
                                            );
                $json = json_encode($request_array);

                $curl_obj = curl_init();
                curl_setopt($curl_obj, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($curl_obj, CURLOPT_URL, "https://api.proprofs.com/classroom/v3/create/register");
                curl_setopt($curl_obj, CURLOPT_POSTFIELDS, $json);  
                curl_setopt($curl_obj, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl_obj, CURLOPT_POST, 1);
                curl_setopt($curl_obj, CURLOPT_HEADER, 0);
                curl_setopt($curl_obj, CURLOPT_HTTPHEADER,
                    array(
                        'Content-Type:application/json',
                    ) 
                );  
                curl_exec($curl_obj);
                TwinklePayments::where('paid_by',$this->subscriberId)->where('id',$this->id)->update($storeAccess);
                \DB::commit();
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = \App\Http\Controllers\UtilityController::Sendexceptionmail($e);
        }   
    }
}
