<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoreEmail extends Model
{
    protected $table = 'store_email';
}
