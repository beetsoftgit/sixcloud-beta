<?php

//###############################################################
//File Name : ProjectSkill.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related to project stands under which category
//Date : 19th Mar, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectSkill extends Model
{
    protected $table = 'project_skills';
    public $rules    = array(
        'mp_project_id' => 'required',
        'skill_set_id'  => 'required',
    );

    public function skillDetail()
    {
        return $this->belongsTo('App\SkillSet', 'skill_set_id');
    }

    public function projectskills()
    {
        return $this->belongsTo('App\MarketPlaceProject', 'mp_project_id');
    }
    // public function skillDetail()
    // {
    //     return $this->belongsToMany('App\ProjectSkill', 'project_skills', 'skill_set_id', 'mp_project_id');
    // }
}
