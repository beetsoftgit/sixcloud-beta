<?php

//###############################################################
//File Name : MarketPlaceSellerBankDetails.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related to bank details of seller
//Date : 26th Jun, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketPlaceSellerBankDetails extends Model
{
    protected $table  = 'mp_seller_bank_details'; 
    public $rules = array(
    	
    	'user_id'    		   => 'required',
    	'country_id' 		   => 'required',
        'id_number' 		   => 'required',
        'bank_name' 		   => 'required',
		'account_name_chinese' => 'required',
		'account_name_eng'     => 'required',
		'account_number'       => 'required',
		'sub_bank_name'        => 'required',
		'sub_bank_province'    => 'required',
		'sub_bank_city'        => 'required',
		'store_front_url'      => 'required',
		'branch_code'          => 'required',
		'swift_bic'            => 'required',
		'bank_province'        => 'required',
		'bank_code'            => 'required',
		'account_type'         => 'required',
		'japan_field'          => 'required',
		'iban'                 => 'required',
		'pan'                  => 'required',
		'ifsc_code'            => 'required',
		'suffix'               => 'required',
		'sort_code'            => 'required',
		'routing_number'       => 'required',
		'branch_name'          => 'required',
    ); 
}
