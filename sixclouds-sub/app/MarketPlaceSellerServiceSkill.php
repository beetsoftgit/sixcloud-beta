<?php

//###############################################################
//File Name : MarketPlaceSellerServiceSkill.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related seller service skills
//Date : 17th May, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketPlaceSellerServiceSkill extends Model
{
    protected $table  = 'mp_seller_service_skills'; 
    public $rules = array(
        'seller_id' => 'required',
        'skill_id'  => 'required',
        'status'    => 'required',
    );

    public function skill_detail()
    {
        return $this->hasMany('App\SkillSet', 'id', 'skill_id');
    }

    public function service_skill_detail()
    {
        return $this->hasOne('App\SkillSet', 'id', 'skill_id');
    }

    public function service_details()
    {
        return $this->hasMany('App\MarketPlaceSellerServicePackage', 'mp_seller_service_skill_id', 'id')->with('service_extras','service_images')->withCount('service_extras','service_images')->orderBy('id','DESC')->whereIn('status',[1,3]);
    }

    public function packages()
    {
        return $this->hasMany('App\MarketPlaceSellerServicePackage', 'mp_seller_service_skill_id', 'id')->with('service_extras')->withCount('service_extras')->where('status',1);
    }

     /**
     * MarketPlaceSellerServiceSkill has many Images through Throughs.
     * Works for 1-1/1-m through 1-1/1-m
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function images()
    {
        return $this->hasManyThrough('App\MarketPlaceSellerServiceImage','App\MarketPlaceSellerServicePackage','mp_seller_service_skill_id','mp_seller_service_pacakage_id');
    }
}