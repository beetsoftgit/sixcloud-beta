/*
 Author  : Komal Kapadi (komal@creolestudios.com)
 Date    : 8th December 2017
 Name    : EltVideoController
 Purpose : All the functions for home page
 */
angular.module('sixcloudApp').controller('EltVideoController', ['$sce', '$routeParams', 'eltfactory', '$http', '$scope', '$timeout', '$rootScope', '$filter', function($sce, $routeParams, eltfactory, $http, $scope, $timeout, $rootScope, $filter) {
    console.log('EltVideoController loaded');
    $scope.video_id = $routeParams.id;
    $scope.BASEURL = BASEURL;
    getVideoData();

    function getVideoData() {
        eltfactory.GetVideodata({
            'video_id': $scope.video_id
        }).then(function(response) {
            $scope.videoData = response.data.data;
            if ($scope.videoData.video_status == 1) window.location.href = BASEURL + 'subscription-plan';
        });
    }
    /*
     * Author  : Komal Kapadi (komal@creolestudios.com)
     Date    : 8th December 2017
     Purpose : Submit New Note 
     */
    $scope.submitNote = function(note) {
        if ($scope.vm.form.$valid) {
            eltfactory.Addnote({
                'video_id': $scope.videoData.id,
                'notes': note
            }).then(function(response) {
                simpleAlert('success', 'Note', response.data.message, true);
                $scope.note = '';
                getVideoData();
            });
        }
    }
    /*
     * Author  : Komal Kapadi (komal@creolestudios.com)
     Date    : 12th December 2017
     Purpose : Delete Note
     */
    function deleteNote() {
        eltfactory.Deletenote({
            'note_id': $scope.deletedNote.id
        }).then(function(response) {
            simpleAlert('success', 'Note', response.data.message, true);
            getVideoData();
        });
    }
    $scope.deleteNote = function(note) {
        $scope.deletedNote = note;
        confirmDialogue('', '', '', deleteNote);
    }
    $(document).ready(function() {
        var showChar = 100; // How many characters are shown by default
        var ellipsestext = "...";
        var moretext = "View more";
        var lesstext = "View less";
        $('.more').each(function() {
            var content = $(this).html();
            if (content.length > showChar) {
                var c = content.substr(0, showChar);
                var h = content.substr(showChar, content.length - showChar);
                var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<div class="continue-reading text-center" ><a href="" class="btn morelink"> <i class="sci-View-More"> </i> &nbsp;' + moretext + '</a></div></span>';
                $(this).html(html);
            }
        });
        $('[data-toggle="tooltip"]').tooltip();
        $('[rel=popover]').popover({
            html: true,
            content: function() {
                return $('#popover_content_wrapper').html();
            }
        });
        $(".morelink").click(function() {
            if ($(this).hasClass("less")) {
                $(this).removeClass("less");
                $(this).html(moretext);
            } else {
                $(this).addClass("less");
                $(this).html(lesstext);
            }
            $(this).parent().prev().toggle();
            $(this).prev().toggle();
            return false;
        });
    });
}]);