
app = angular.module('sixcloudscustomersupportApp', ['ngRoute','pascalprecht.translate']);

// if (Cookies.get('language') == 'undefined' || Cookies.get('language') == '' || Cookies.get('language') == undefined) Cookies.set('language', 'en');
app.config(function($routeProvider, $translateProvider) {
    $translateProvider.translations('en', {
        lbl_lang_chng:'中文',
        lbl_sixclouds:                        'SixClouds',
        lbl_submit:                           'Submit Request',
        lbl_customer_support:                 'Customer Support',
        lbl_customer_support_fullname:        'Full Name',
        lbl_customer_support_email:           'Email Address',
        lbl_customer_support_phone_number:    'Phone Number',
        lbl_customer_support_optional:        '(Optional)',
        lbl_customer_support_country_code:    'Country Code',
        lbl_customer_support_ticket_type:     'Ticket Type',
        lbl_customer_support_product:         'Product',
        lbl_customer_support_select_ticket:   'Select Ticket',
        lbl_customer_support_select_product:  'Select Product',
        lbl_customer_support_ticket_category: 'Ticket Category',
        lbl_customer_support_select_category: 'Select Category',
        lbl_customer_support_subject:         'Subject',
        lbl_customer_support_description:     'Description',
        lbl_customer_support_fileupload:      'File Upload (up to 5mb/image max 5 images)',
        lbl_customer_support_note:            'Please expect an estimated response time of 24-48 hours. Please refrain from submitting multiple support requests.',
        lbl_all_rights_reserved:              'All rights reserved',
        lbl_about_us:                         'About Us',
        lbl_ignite:                           'Ignite',
        lbl_sixteen:                          'Sixteen',
        lbl_discover:                         'Discover',
        lbl_proof_reading:                    'Proof Reading',
        lbl_become_seller:                    'Become a seller',
        lbl_become_buyer:                     'Become a buyer',
        lbl_terms_service:                    'Terms of Service',
        lbl_privacy_policy:                   'Privacy Policy',
        lbl_quick_links:                      'Quick Links',
        lbl_the_company:                      'The Company',
        lbl_core_values:                      'Core Values',
        lbl_our_team:                         'Our Team',
        lbl_tandc1:                           'You have read, understood and agree to be bounded by ',
        lbl_tandc2:                           'and agree that we may collect, use and disclose your personal data as provided in this form for the purposes set out in the ',
        lbl_tandc3:                           ' Where you are providing us with the personal data of another individual, you warrant that you are authorised to consent to the ',
        lbl_tandc4:                           ' and provide us with such personal data on his/her behalf. ',
        lbl_sixclouds_privacy_policy:         'SixClouds’ Privacy Policy',
        lbl_confirm:                          'Please confirm that you have read and agreed to the Terms and Conditions',
    }).translations('chi', {
        lbl_lang_chng:                        'English',
        lbl_sixclouds:                        '六云',
        lbl_submit:                           '提交',
        lbl_customer_support:                 '客服中心',
        lbl_customer_support_fullname:        '全名',
        lbl_customer_support_email:           '电邮',
        lbl_customer_support_phone_number:    '手机号',
        lbl_customer_support_optional:        '（如有）',
        lbl_customer_support_country_code:    '国家/地区代码',
        lbl_customer_support_ticket_type:     '客服种类',
        lbl_customer_support_product:         '产品',
        lbl_customer_support_select_ticket:   '选择票证',
        lbl_customer_support_select_product:  '选择产品',
        lbl_customer_support_ticket_category: '客服类别',
        lbl_customer_support_select_category: '选择类别',
        lbl_customer_support_subject:         '主题',
        lbl_customer_support_description:     '描述问题',
        lbl_customer_support_fileupload:      '上传文件 (5MB /图像，最多5张图像)',
        lbl_customer_support_note:            '我们的客服专员会在24-48 小时以内回复您。请无提交重复请求。',
        lbl_all_rights_reserved:              '版权所有',
        lbl_about_us:                         '关于我们',
        lbl_ignite:                           '点燃',
        lbl_sixteen:                          '十六',
        lbl_discover:                         '发现',
        lbl_proof_reading:                    '证明阅读',
        lbl_become_seller:                    '成为卖家',
        lbl_become_buyer:                     '成为买家',
        lbl_terms_service:                    '服务条款',
        lbl_privacy_policy:                   '隐私政策',
        lbl_quick_links:                      '快速链接',
        lbl_the_company:                      '六云简介',
        lbl_core_values:                      '核心理念',     
        lbl_our_team:                         '我们的团队',
        lbl_tandc1:                           '您已阅读、理解并同意受',
        lbl_tandc2:                           '策的约束，并同意我们可以为',
        lbl_tandc3:                           '中规定的目的收集、使用和披露本表格中您提供的个人数据。如果您向我们提供另一个人的个人数据，您保证您有权同意',
        lbl_tandc4:                           '，并代表他/她向我们提供此类个人数据。',
        lbl_sixclouds_privacy_policy:         '六云隐私政策',
        lbl_confirm:                          '请确认您已阅读并同意本条款和条件。',
    }).translations('ru', {
        lbl_lang_chng:'中文',
        lbl_sixclouds:                        'SixClouds',
        lbl_submit:                           'Отправить запрос',
        lbl_customer_support:                 'Поддержка клиентов',
        lbl_customer_support_fullname:        'Полное Имя',
        lbl_customer_support_email:           'Адрес электронной почты',
        lbl_customer_support_phone_number:    'Номер телефона',
        lbl_customer_support_optional:        '(Необязательно)',
        lbl_customer_support_country_code:    'код страны',
        lbl_customer_support_ticket_type:     'Тип билета',
        lbl_customer_support_product:         'Товар',
        lbl_customer_support_select_ticket:   'выбрать Тип',
        lbl_customer_support_select_product:  'выбрать Товар',
        lbl_customer_support_ticket_category: 'билета категорию',
        lbl_customer_support_select_category: 'выбрать категорию',
        lbl_customer_support_subject:         'Предмет',
        lbl_customer_support_description:     'Описание',
        lbl_customer_support_fileupload:      'Загрузка файла (до 5 Мб / изображение макс 5 изображений)',
        lbl_customer_support_note:            'Пожалуйста, ожидайте приблизительное время ответа 24-48 часов. Пожалуйста, воздержитесь от подачи нескольких запросов поддержки.',
        lbl_all_rights_reserved:              'Все  права защищены',
        lbl_about_us:                         'О Нас',
        lbl_ignite:                           'Ignite',
        lbl_sixteen:                          'Sixteen',
        lbl_discover:                         'Discover',
        lbl_proof_reading:                    'Proof Reading',
        lbl_become_seller:                    'Become a seller',
        lbl_become_buyer:                     'Become a buyer',
        lbl_terms_service:                    'Условия обслуживания',
        lbl_privacy_policy:                   'Конфиденциальность и безопасность',
        lbl_quick_links:                      'Быстрые ссылки',
        lbl_the_company:                      'Компания',
        lbl_core_values:                      'Фундаментальные ценности',
        lbl_our_team:                         'Наша команда',
        lbl_tandc1:                           'Вы прочитали, поняли и согласны с тем, что вы обязаны соблюдать Политику конфиденциальности ',
        lbl_tandc2:                           'SixClouds, и соглашаетесь с тем, что мы можем собирать, использовать и раскрывать ваши персональные данные в соответствии с данной формой для целей, изложенных в Политике конфиденциальности SixClouds.',
        lbl_tandc3:                           ' Если вы предоставляете нам личные данные другого лица, вы гарантируете, что у вас есть разрешение на согласие с Политикой конфиденциальности SixClouds ',
        lbl_tandc4:                           ' и предоставляете нам такие личные данные от его / ее имени ',
        lbl_sixclouds_privacy_policy:         'Политике конфиденциальности SixClouds',
        lbl_confirm:                          'Please confirm that you have read and agreed to the Terms and Conditions',
    });
    $translateProvider.preferredLanguage(Cookies.get('language'));
});
/* To change the interolate provider need to change it's originonal brackets.*/
app.config(['$interpolateProvider', function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
}]);
/* for changing title */
app.run(['$location', '$rootScope', '$http', '$timeout', function($location, $rootScope, $http, $timeout) {
    $rootScope.$on('$routeChangeSuccess', function(event, current, previous) {
        if (current.$$route != undefined) $rootScope.title = current.$$route.title;
        document.body.scrollTop = document.documentElement.scrollTop = 0;
    });
}]);
app.controller('CustomerSupport', ['$scope', '$translate','customersupport', '$rootScope', '$timeout', function ($scope, $translate, customersupport, $rootScope,$timeout){
    console.log("Customer Support controller loaded");
    var ctrl = this;
    if (Cookies.get('language') == 'undefined' || Cookies.get('language') == '' || Cookies.get('language') == undefined) {
        $scope.url = window.location.host;
        $scope.url = $scope.url.split('.');
        $scope.url = $scope.url[2];
        $timeout(function() {
            if($scope.url==undefined){
                $timeout(function() {
                    $scope.changeLang('en');
                },500);
            }
            if($scope.url == 'net' && $scope.url!=undefined){
                $timeout(function() {
                    $scope.changeLang('en');
                },500);
            }
            if($scope.url == 'cn' && $scope.url!=undefined){
                $timeout(function() {
                    $scope.changeLang('chi');
                },500);
            }
        },1000);
    }
    $scope.changeLangs = Cookies.get('language');
    $scope.changeLang = function(lang) {
        if (lang == 'en') $scope.changeLangs = 'en';
        else if (lang == 'chi') $scope.changeLangs = 'chi';
        else $scope.changeLangs = 'ru';
        var getLanguage = lang;
        ctrl.language = getLanguage;
        Cookies.set('language', ctrl.language);
        $translate.use(ctrl.language);
    }

    customersupport.GetProducttype().then(function(response) {               
        if (response.data.status == 1){
            $scope.productTypes = response.data.data.productTypes;         
            $scope.userdata = response.data.data.userdata;
        }    
    });

    $scope.selectTitcketType = function(productId){
        customersupport.Gettickettype({'productId':productId}).then(function(response) {    
            if (response.data.status == 1){
                $scope.ticketTypes = response.data.data.ticketTypes;
                $scope.ticketCategories = response.data.data.ticketCategory;
                $scope.userdata = response.data.data.userdata;
            }            
        });

    }
    $scope.selectTitcketCategory = function(ticketId){
        customersupport.Getticketcategoriesforcustomersupport({'ticketId':ticketId}).then(function(response) {                
            if (response.data.status == 1){                
                $scope.ticketCategories = response.data.data.ticketCategories;
            }            
        });
    }
    var filesPortfolioToRemove = [];
    var fileSize = 0;
    filesPortfolio = [];
    var coverImageArray = [];
    $("#addFileDynamic").click(function(event) {
        event.stopPropagation();
        event.preventDefault();
        var nextID = 'id' + (new Date()).getTime();
        var html = "<div class='hide' id=\"div" + nextID + "\">";
        html += "<br><input multiple name=\"ticket_attachments[]\" id='" + nextID + "' type=\"file\" accept=\"*\" class=\"filestyle\" >";
        html += "<a href=\"javascript:void(0);\" id='rem" + nextID + "' class=\"remImg\">Remove</a></div></div>";
         // console.log(html); return;
        $("#divImagesportfolio").append(html);
        $('#' + nextID).click();
    });
    $(".portfolio_li").on('click', '.remImg', function(event) {
        event.stopPropagation();
        event.preventDefault();
        var fileName = $(this).attr('id');
        angular.forEach(filesPortfolio, function(value, key){
          if(value.name == fileName){
                fileSize = fileSize-value.size;
                filesPortfolio.splice(key,1);
          }
        });
        $(this).parent('.removeFile').remove();
        $(this).closest("br");
        filesPortfolioToRemove.push(fileName);
    });
    $(document).on("change", ".filestyle", function(event) {
        event.stopPropagation();
        for (var i = 0; i < event.target.files.length; i++) {
            filesPortfolio.push(event.target.files[i]);
            fileSize = fileSize+filesPortfolio[i].size;
            $('.portfolio_li').append("<li class='removeFile uploads_padding'>" + event.target.files[i].name + "<a class='remImg' href=\"javascript:void(0);\" id='" + event.target.files[i].name + "'><i class='fa fa-times'></i></a></li>");
        }
    });

    $scope.obj = {ticket_category: undefined};
    function deleteredirect(){
      /*if($scope.userdata==null)  {
            location.replace('sixteen');
      }else{
        if($scope.userdata.marketplace_current_role==1){
            location.replace('seller-dashboard');
        } else {
            location.replace('buyer-dashboard');
        }  
      }*/
      if($scope.product_type==1){
        location.replace('ignite');
      } else {
        location.replace('sixteen');        
      }
      
    }
    if($scope.changeLangs=='en'){
        text              = 'Submit Request';
        success           = 'Success';
        confirmButtonText = 'Ok';
    }
    else if($scope.changeLangs=='chi'){
        text              = '提交';   
        success           = '成功';
        confirmButtonText = '好。';
    }else if($scope.changeLangs=='ru'){
        text              = 'Отправить запрос';
        success           = 'Успех';
        confirmButtonText = 'Ok';
    }
   $scope.Createsupportticket = function() {
        if($scope.user_name==undefined||$scope.user_name==''||$scope.email_address==undefined||$scope.email_address==''||$scope.ticket_type==undefined||$scope.ticket_type==''||$scope.product_type==undefined||$scope.product_type==''||$scope.ticket_subject==undefined||$scope.ticket_subject==''||$scope.ticket_description==undefined||$scope.ticket_description==''||$scope.obj.ticket_category == undefined||$scope.obj.ticket_category == ''){
            
            if ($scope.user_name == undefined||$scope.user_name=='') {
                $scope.user_name_error = true;
            }
            if($scope.email_address == undefined||$scope.email_address==''){
                $scope.email_address_error = true;
            }
            if ($scope.product_type == undefined||$scope.product_type=='') {
                $scope.product_type_error = true;
            }
            if ($scope.ticket_type == undefined||$scope.ticket_type=='') {
                $scope.ticket_type_error = true;
            }
            if($scope.obj.ticket_category == undefined||$scope.obj.ticket_category == ''){
                $scope.ticket_category_error = true;
            }
            if ($scope.ticket_subject == undefined||$scope.ticket_subject=='') {
                $scope.ticket_subject_error = true;
            }
            if ($scope.ticket_description == undefined||$scope.ticket_description=='') {
                $scope.ticket_description_error = true;
            }
        } else {
            if(!$scope.tandc) {
                $scope.show_confirm_error = true;
            } else {
                $scope.show_confirm_error = false;
                var categoryNeededFor = ['1', '2', '4','5'];
                if (jQuery.inArray($scope.ticket_type, categoryNeededFor) !== -1) {
                    if($scope.obj.ticket_category == undefined){
                       $scope.ticket_category_error = true;
                       return;
                    }
                 }
                showLoader(".create-issue");
                var totalFiles = 0;
                totalFiles = filesPortfolio.length;
                if(totalFiles<=5){
                    if(fileSize<5000000){
                        var newTicket = new FormData($("#CustomerSupportForm")[0]);
                        newTicket.append('filesPortfolioToRemove',filesPortfolioToRemove);
                        newTicket.append('timezone',moment.tz.guess());
                        newTicket.append('language',$scope.changeLangs);
                        newTicket.append('ticket_for',$scope.product_type);
                        customersupport.Createnewticket(newTicket).then(function(response) {
                            hideLoader(".create-issue",text);
                            if (response.data.code == 2) window.location.replace("sixteen-login");
                            if (response.data.status == 1) {
                                hideLoader(".create-issue",text);
                                $scope.changeLangs = Cookies.get('language');
                                if($scope.changeLangs=='en'){
                                    text              = 'Submit Request';
                                    success           = 'Success';
                                    confirmButtonText = 'Ok';
                                }
                                else if($scope.changeLangs=='chi'){
                                    text              = '提交';   
                                    success           = '成功';
                                    confirmButtonText = '好。';
                                }else if($scope.changeLangs=='ru'){
                                    text              = 'Отправить запрос';
                                    success           = 'Успех';
                                    confirmButtonText = 'Ладно';
                                }
                                confirmDialogueAlert(success,response.data.message,"success",false,'#8972f0',confirmButtonText,'',true,'','',deleteredirect);
                            } else {
                                hideLoader(".create-issue",text);
                                if(response.data.message!=undefined){
                                    simpleAlert('error', '', response.data.message);
                                }else{
                                    if($scope.changeLangs=='en'){
                                        error = 'Oops, Something went wrong.'
                                    }else if($scope.changeLangs=='chi'){
                                        error = '哎呀,出了些问题。'
                                    }else{
                                        error = 'Ой,что-то пошло не так.'
                                    }
                                    simpleAlert('error', '', error);
                                }
                            }
                        });         
                    } else {
                       hideLoader(".create-issue",text);
                        if($scope.language=='en'){
                            simpleAlert('error','','Files size limit exceeded.\n(Only 5mb allowed combining all files)');
                        }
                        else{
                            simpleAlert('error','','超出文件大小限制.\n(只有5mb允许组合所有文件)');
                        }
                       
                       return;
                    }
                } else{
                    hideLoader(".create-issue",text);
                    if($scope.changeLangs=='en'){
                        simpleAlert('error','','File count limit exceeded.\n(Total of 5 images are allowed)');
                    }
                    else{
                        simpleAlert('error','','超出文件计数限制.\n(总共允许5张图片)');
                    }
                    return;
                }
            }
        }
   }
}]);
app.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }            
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});
