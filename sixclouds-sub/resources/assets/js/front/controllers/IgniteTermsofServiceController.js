
app = angular.module('sixcloudIgniteTermsofServiceApp', ['ngRoute','pascalprecht.translate']);
/*var myApp = angular.module('sixcloudAboutApp', []);*/

// if (Cookies.get('language') == 'undefined' || Cookies.get('language') == '' || Cookies.get('language') == undefined) Cookies.set('language', 'en');
app.config(function($routeProvider, $translateProvider) {
    $translateProvider.translations('en', {
        
        lbl_sixclouds_ignite_terms_of_service:           'SixClouds IGNITE Terms of Service',
        lbl_lang_chng:                                   '中文',
        lbl_sixclouds:                                   'SixClouds',
        lbl_the_company:                                 'The Company',
        lbl_the_following_terms:                         'The following terms and conditions (these "Terms of Service") is an agreement between you and SixClouds Pte Ltd (“SixClouds”, “We” or “Us”).',
        lbl_the_terms_of_service:                        'The Terms of Service is the entire agreement between SixClouds and User. We provide access to and use of the SixClouds IGNITE website and mobile application, including any general and personalised content, functionality and services, directly and through the website, mobile application and associated domains of ',
        lbl_www_sixclouds_net:                           'www.sixclouds.net/ignite',
        lbl_and:                                         'and',
        lbl_www_sixclouds_cn:                            'www.sixclouds.cn/ignite',
        lbl_the_site:                                    '(the "Site")',
        lbl_please_read_these:                           'Please read these Terms of Service carefully before you start to use the Site.',
        lbl_by_using_the_site_you_are_accepting:         'By using the Site, you are accepting and agreeing to be bound and abide by these Terms of Service and our Privacy Policy, any other legal notices or conditions or guidelines as uploaded and posted on the Site or incorporated herein by reference. If you do not wish to comply to these Terms of Service or the Privacy Policy, you must not access or use the Site. ',
        lbl_by_using_the_site_you_are_accepting_warrant: 'By using this Site, you represent and warrant that you are of legal age to form a binding contract and meet all the foregoing eligibility requirements. If you do not meet all these requirements, you must not access or use the Site.',
        lbl_key_terms_to_note:                           'Key Terms to Note',
        lbl_ignite:                                      ' IGNITE ',
        lbl_means_the_features:                          'means the content and services we make available through the website, mobile application, platform, associated domains at ',
        lbl_and_any_other_platform_or_websites:          'and any other platform or websites, including sub-domains, web and mobile versions, plugins and other media, services, software or networks now existing or which may be developed later.',
        lbl_users:                                       'Users',
        lbl_means_users_who_purchase:                    'refers to both Subscribers and Non-subscribers.',
        lbl_non_subscriber:                              'Non-subscriber',
        lbl_means_users_who_offer:                       'means an individual who accesses the site but does not have the right to access the subscription content.',
        lbl_subscriber:                                  'Subscriber',
        lbl_are_creative_capabilities:                   'means an individual user that has the right to access the subscription content via a paid subscription plan.',
        lbl_subscription_content:                        'Subscription Content',
        lbl_are_creative_services:                       ' means videos, images, audio, music, sounds, quizzes, worksheets and other files, information and content that are available on a paid subscription plan.',
        lbl_subscriber_generated_content:                'Subscriber Generated Content',
        lbl_are_additional_services:                     ' means all content that a Subscriber uploads, posts or otherwise make available or provide to the Site.',
        lbl_subs:                                        'Subscription',
        lbl_is_where_buyers_and_sellers:                 ' means the then-current subscription plan(s) for which a Subscriber will pay or have paid a subscription fee to use via the Site.',
        lbl_subs_term:                                   'Subscription Term',
        lbl_means_period_of:                             ' means the period of time that a Subscriber may utilize the applicable portion of subscription content on the Site.',
        lbl_general_overview_of_terms_of_service:        'Access, License and Right to Use',
        lbl_only_registered_users:                       'A User must be at least 18 years of age (and if a minor, have the permission of a parent or legal guardian to access the Site) and possess the legal right and ability to enter into binding contracts.',
        lbl_services_on_sixclouds_may_be:                'Subject to these Terms of Service, Subscriber will be granted a limited, non-exclusive, revocable, non-transferable, and non-sublicensable right to access that portion of the Subscription Content applicable to the Subscription type. By agreeing to grant such access, SixClouds do not obligate ourselves to maintain the website, web application and associated domains in its present form. We may upgrade, modify, change or enhance the services and convert a Subscriber to a new version thereof at any time in its sole discretion, to the extent that this is not detrimental to Subscriber’s use of the services and on reasonable prior notice to the Subscriber.',
        lbl_payment_is_to_be_made_by:                    'User will remain liable for all acts or omissions of its minor users with respect to access and use of the Site; furthermore, and for the avoidance of doubt, the User will be responsible for ensuring that its minor users remain fully compliant with these Terms of Service and Privacy Policy.',
        lbl_seller_must_complete_the_project:            'User agrees to abide by any rules or regulations that we publish with respect to conduct of Users of the Site, which rules and regulations are hereby incorporated into these Terms of Service by this reference. SixClouds reserves the right to deny a User access to the Site if, in the SixClouds sole discretion, the User has failed to abide by these Terms of Service or appear likely to do so.',
        lbl_buyers_and_sellers_are_encouraged:           'User accepts that SixClouds in its sole discretion may, but has no obligation to, monitor the Services or any portion thereof, and/or to oversee compliance with these Terms of Service.',
        lbl_users_agree_to_abide_to:                     'User promises, acknowledges and agrees that:',
        lbl_illegal_or_fraudulent_services:              'Access privileges may not be transferred to any third-parties;',
        lbl_copyright_infringement:                      'It will comply with all applicable laws and regulations with respect to use of the Site;',
        lbl_publish_or_distribute:                       'It will not rent, lease, sublicense, re-sell, distribute, transfer, copy or modify the contents on the Site or any component thereof;',
        lbl_intentional_copying:                         'It will not translate, decompile, or create or attempt to create, by reverse engineering or otherwise, the contents on the Site;',
        lbl_ignite_will_not_reproduce:                   'It will not reproduce, distribute, modify, create derivative works of, publicly display, publicly perform, republish, or transmit the contents of the Site or any portion thereof;',
        lbl_ignite_will_not_delete_alter:                'It will not delete or alter any copyright, trademark or other proprietary rights notices from copies of content from the Site;',
        lbl_ignite_will_not_use_site:                    'It will not use the Site in any manner, or in connection with any content, data, hardware, software or other materials that infringes upon or violates any patent, copyright, trade secret, trademark, or other intellectual property right of any third party, or that constitutes a defamation, libel, invasion of privacy, or violation of any right of publicity or other third party right, or that is threatening, harassing or malicious.',
        lbl_users_recognizes:                            'User recognizes that the traffic of data through the Internet may cause delays during the download of information from the Site and accordingly, it shall not hold SixClouds liable for delays that are ordinary in the course of Internet use. User further acknowledges and accepts that the Site will not be available on a continual twenty four hour basis due to such delays, or delays caused by upgrading, modification, or standard maintenance of the Site.',
        lbl_users_agrees_applicable:                     'User agrees, where applicable, to treat password, usernames, and other security information, as confidential and to not provide any other person with access to the Subscription Content or portions of it using User’s authentication information. User will notify SixClouds immediately of any unauthorized access to, or use of, authentication information. SixClouds cannot and will not be liable for any unauthorized access to your account or data that arises from your acts or omissions.',
        lbl_access_license_and_right_to_Use:             'Subscription Fees and Payment',
        lbl_subject_to_the_terms_of_service:             'Subscriber agrees to pay the appropriate fee corresponding to the particular Subscription plan that it wishes to access for the applicable Subscription Term. SixClouds reserves the right to change the Subscription fees. Unless otherwise expressly stated, all fees are non-cancellable and non-refundable.',
        lbl_you_hereby_agree_to_grant_us_a_worldwide:    'In addition to the Subscription price, you may be charged with indirect taxes such as Value Added Tax (VAT) or Government Service Tax (GST) in accordance with applicable laws your country of residency.',
        lbl_ignite_terms_termination:                    'Term and Termination',
        lbl_subscriber_may_subs:                         'Subscriber may subscribe to the Subscription Content for the applicable term of the specific Subscription commencing on the effective date.',
        lbl_ignite_reserves_right:                       'SixClouds reserves the right to terminate or suspend access to all or any portion of the Subscription Content for violation or suspected violation of these Terms of Service. Subscriber will have no further rights to access the Site. Termination will not affect the rights or liabilities of either party that accrued prior to termination.',
        lbl_when_account_subs:                           'When an account subscription ends (e.g., at the end of the term if the account has not been renewed or has been cancelled), the account no longer permits access to the Subscription Content.',
        lbl_once_the_terms_of:                           'Once the term of a subscription account ends, SixClouds may delete data relating to an account in accordance with this Terms of Service and our Privacy Policy. It is the Subscriber’s sole responsibility to request renewal of accounts that do not automatically renew to maintain continued access to the account and its associated content.',
        lbl_ignite_third_party:                          'Third Party Links or Information',
        lbl_ignite_may_contain:                          'This Site may contain links to other websites that are not operated by or related to SixClouds. SixClouds is not responsible for the content, accuracy or opinions expressed in such third-party websites, and does not investigate, monitor, or check these websites for accuracy or completeness. The inclusion of any linked website on this Site does not imply approval or endorsement of the linked website by SixClouds. A User who leaves the Site to access these third-party sites does so at its own risk.',
        lbl_ignite_prop_rights:                          'Proprietary Rights',
        lbl_ignite_user_ack:                             'User acknowledges and agrees that our Content and any necessary software used in connection with the Content contains proprietary and confidential information that is protected by applicable intellectual property and other laws. User further acknowledges and agrees that information presented to User through the Subscription is protected by copyrights, trademarks, service marks, patents or other proprietary rights and laws. User agrees not to copy, modify, rent, lease, loan, sell, distribute or create derivative works based on the Content, in whole or in part.',
        lbl_ignite_under_terms:                          'Under the terms of this Terms of Service, it is expressly forbidden to distribute or reproduce the content of the Service or any portion thereof by any means, including but not limited to electronic and print.',
        lbl_ignite_sixclouds_reserves:                   'SixClouds reserves the right to cancel Subscriber accounts without refund if it is determined that Subscriber has violated these terms.',
        lbl_ignite_general_terms:                        'General Terms',
        lbl_ignite_account_hold:                         'SixClouds reserves the right to put any account on hold or permanently disable accounts due to breach of these Terms of Service or due to any illegal or inappropriate use of the site or services. Users who have violated our Terms of Service and had their account disabled may contact our Customer Support team for more information surrounding the violation and status of the account.',
        lbl_ignite_account_ownership:                    'Users must be able to verify their account ownership through Customer Support by providing information and materials that prove ownership of the account.',
        lbl_ignite_changes_to_terms:                     'SixClouds may make changes to its Terms of Service from time to time. When these changes are made, SixClouds will make an updated copy of the Terms of Service available on the site. Users understand and agree that any use of the SixClouds IGNITE site after the date on which the Terms of Service have changed, shall be treated as acceptance of the updated Terms of Service.',
        lbl_ignite_no_joint_venture:                     'No joint venture, partnership, employment, or agency relationship exists between you, SixClouds or any third-party provider as a result of these Terms of Service.',
        lbl_ignite_provision_invalid:                    'If any provision of the Terms of Service is held to be invalid or unenforceable, such provision shall be struck and the remaining provisions shall be enforced to the fullest extent under law. This shall, without limitation, also apply to the applicable law and jurisdiction as stipulated above.',
        lbl_ignite_faliure_of_sixclouds:                 'The failure of SixClouds to enforce any right or provision in the Terms of Service shall not constitute a waiver of such right or provision unless acknowledged and agreed to by SixClouds in writing. The Terms of Service comprises the entire agreement between you and the Terms of Service and supersedes all prior or contemporaneous negotiations or discussions, whether written or oral (if any) between the parties regarding the subject matter contained herein.',
        lbl_disclamer_warr:                              'Disclaimer of Warranties',
        lbl_content_any_service:                         'Use of the SixClouds IGNITE site, its content and any services or items obtained through the website and or mobile application is at user’s own risk. The site, its content and any services or items obtained through the website and or mobile application are provided on an "as is" and "as available" basis, without any warranties of any kind, either express or implied. Neither SixClouds nor any person associated with SixClouds makes any warranty or representation with respect to the completeness, security, reliability, quality, accuracy or availability of the website. The foregoing does not affect any warranties which cannot be excluded or limited under applicable law.',
        lbl_ignite_limitation:                           'Limitation on Liability',
        lbl_ignite_affilates:                            'In no event will SixClouds, its affiliates or their licensors, service providers, employees, agents, officers or directors be liable for damages of any kind, under any legal theory, arising out of or in connection with user’s usage, or inability to use, the website and or mobile application, any websites linked to it, any content on the website and or mobile application or such other websites or any services or items obtained through the website and or mobile application or such other websites, including any direct, indirect, special, incidental, consequential or punitive damages, including but not limited to, personal injury, pain and suffering, emotional distress, loss of revenue, loss of profits, loss of business or anticipated savings, loss of use, loss of goodwill, loss of data, and whether caused by tort (including negligence), breach of contract or otherwise, even if foreseeable. The foregoing does not affect any liability which cannot be excluded or limited under applicable law.',
        lbl_ignite_governing_law:                        'Governing Law',
        lbl_ignite_governed_by:                          'This Terms of Service shall be governed by Singapore law without regard to the choice of law provisions of any jurisdiction, and any disputes, actions, claims or causes of action arising out of or in connection with the Terms of Service shall be subject to the exclusive jurisdiction of the court of Singapore to which you hereby agree to submit to.',
        lbl_all_rights_reserved:                         'All rights reserved',
        lbl_about_us:                                    'About Us',
        lbl_quick_links:                                 'Quick Links',
        lbl_customer_support:                            'Customer Support',
        lbl_terms_service:                               'Terms of Service',
        lbl_privacy_policy:                              'Privacy Policy',
        // lbl_updated_at:                                  'Updated: 14 Feb 2019',
        lbl_updated_at:                                  'Updated: Mar 2020',
        lbl_:                                            '',
        lbl_the_company:                                 'The Company',
        lbl_core_values:                                 'Core Values', 
        lbl_our_team:                                    'Our Team',
        lbl_ignite_distributor_access:                   'Distributor Access',

    }).translations('chi', {
        
        lbl_sixclouds_ignite_terms_of_service:           '六云智宝服务条款',
        lbl_lang_chng:                                   'English',   
        lbl_sixclouds:                                   '六云',
        lbl_the_company:                                 '六云简介',
        lbl_the_following_terms:                         '下列条款和条件（这些“服务条款”）是用户(“您”)和六云私人有限公司（“六云”或“我们”）之间的协议。',
        lbl_the_terms_of_service:                        '这服务条款是我们与用户之间的全部协议。我们提供访问和使用六云智宝网站和手机应用程序的服务，包括任何一般的和个性化的内容，功能和服务，直接通过网站以及',
        lbl_www_sixclouds_net:                           'www.sixclouds.net/ignite',
        lbl_and:                                         '和',
        lbl_www_sixclouds_cn:                            'www.sixclouds.cn/ignite',
        lbl_the_site:                                    '（ “本网站”）的相关领域。',
        lbl_please_read_these:                           '在开始使用本网站之前，请仔细阅读本服务条款。',
        lbl_by_using_the_site_you_are_accepting:         '通过使用本网站，您接受并同意受约束和遵守本《服务条款》和《隐私政策》，以及在本网站上传或发布的任何其他法律声明或条件或准则，或通过引用并入本文。如果您不愿意遵守本服务条款或隐私政策，则不得访问或使用本网站。',
        lbl_by_using_the_site_you_are_accepting_warrant: '通过使用本网站，您代表和保证您的法定年龄，形成一个有约束力的合同，并满足所有上述资格要求。如果您不满足所有以上需求，则不得访问或使用网站。',
        lbl_key_terms_to_note:                           '名词解释',
        lbl_ignite:                                      '智宝',
        lbl_means_the_features:                          '是指我们通过网站、移动应用程序、平台、',
        lbl_and_any_other_platform_or_websites:          '上的相关域和任何其他平台或网站提供的内容和服务，包括子域、网路和手机应用版本、插件和其他媒体、服务、软件或网络，现有和以后开发的媒体、服务、软件或网络。',
        lbl_users:                                       '用户',
        lbl_means_users_who_purchase:                    '是指 订户和非订户。',
        lbl_non_subscriber:                              '非订户',
        lbl_means_users_who_offer:                       '是指访问网站但没有访问订阅内容权利的用户。',
        lbl_subscriber:                                  '订户',
        lbl_are_creative_capabilities:                   '是指通过付费订阅计划访问订阅内容的 用户。',
        lbl_subscription_content:                        '订阅内容',
        lbl_are_creative_services:                       '是指视频、图像、音频、音乐、声音、测验、活页练习题 和其他文件、信息和内容，这些内容可在付费订阅计划中获得。',
        lbl_subscriber_generated_content:                '订户生成内容是',
        lbl_are_additional_services:                     '指订户上传、发布或以其他方式提供给网站的所有内容。',
        lbl_subs:                                        '订阅',
        lbl_is_where_buyers_and_sellers:                 '是指用户将支付或已支付订阅费以通过网站使用的订阅计划。',
        lbl_subs_term:                                   '订阅期限',
        lbl_means_period_of:                             '是指订户可以在本网站上使用订阅内容的适用部分的时间段。',
        lbl_general_overview_of_terms_of_service:        '访问，许可和使用权',
        lbl_only_registered_users:                       '用户必须至少18岁（如果未成年人，具有父母或法定监护人访问网站的许可），并具有签订具有约束力的合同的合法权利和能力。',
        lbl_services_on_sixclouds_may_be:                '根据这些服务条款，订户将被授予访问适用于订阅类型的订阅内容的该部分的受限、非排他性、可撤销、不可转让和不可再授权的权利。通过同意授予这种访问权限，六云没有义务以当前形式维护网站、网站应用程序和相关域。我们可以自行决定在任何时候升级、修改、更改或增强服务，并将订户转换为其新版本，只要这不妨碍订户使用服务并事先合理通知订户。',
        lbl_payment_is_to_be_made_by:                    '用户将继续对其未成年用户在访问和使用本网站方面的所有行为或不行为负责；此外，为了避免怀疑，用户将负责确保其未成年用户完全遵守本服务条款和隐私政策。',
        lbl_seller_must_complete_the_project:            '用户同意遵守我们发布的关于网站用户行为的任何规则或条例，这些规则和规章在此通过引用并入本服务条款。如果用户在六云的唯一自由裁量权下未能遵守这些服务条款或看起来未能能遵守这些服务条款，六云保留拒绝用户访问网站的权利。',
        lbl_buyers_and_sellers_are_encouraged:           '用户接受六云可以，但没有义务监视服务或其任何部分，和/或监督这些服务条款的遵守。',
        lbl_users_agree_to_abide_to:                     '用户承诺、确认并同意：',
        lbl_illegal_or_fraudulent_services:              '访问权限不得转让给任何第三方；',
        lbl_copyright_infringement:                      '将遵守有关网站使用的所有适用法律和法规；',
        lbl_publish_or_distribute:                       '不得出租、重新授权 、再销售、分发、转让、复制或修改网站或其任何组成部分的内容；',
        lbl_intentional_copying:                         '不会翻译，反编译，或创建或试图通过逆向工程或其他方式创建网站上的内容；',
        lbl_ignite_will_not_reproduce:                   '不会复制、分发、修改、制作衍生作品、公开展示、公开表演、重新出版或传送网站或其任何部分的内容；',
        lbl_ignite_will_not_delete_alter:                '不会删除或更改任何版权，商标或其他专有权利布告从内容的副本从网站；',
        lbl_ignite_will_not_use_site:                    '不会利用本网站以任何方式，或与任何内容、数据、硬件、软件或其他材料有关，侵犯任何第三方的专利、版权、商业秘密、商标或其他知识产权，或构成诽谤、侵犯隐私、侵犯公示权或者其他第三人权利，或者威胁、骚扰、恶意的。',
        lbl_users_recognizes:                            '用户认识到，通过网路的数据流量可能在从本网站下载信息期间造成延迟，因此，对于在网路使用过程中常见的延迟，六云公司不承担责任。用户进一步确认并接受由于网站升级、修改或标准维护造成的延误或延误，网站将不能连续24小时提供。',
        lbl_users_agrees_applicable:                     '如果适用，用户同意将密码、用户名和其他安全信息视为机密，并且不向任何其他人提供使用用户身份验证信息访问订阅内容或其部分的权限。用户将立即通知六云，任何未经授权的访问或使用认证信息。六云不能而且不会对任何未经授权的帐户或由你的行为或遗漏产生的数据负责',
        lbl_access_license_and_right_to_Use:             '订阅费与支付',
        lbl_subject_to_the_terms_of_service:             '订户同意支付与其希望访问的适用订阅条款的特定订阅计划相对应的适当费用。六云保留更改订阅费的权利。除非另有说明，所有费用都是不可取消和不退还的。',
        lbl_you_hereby_agree_to_grant_us_a_worldwide:    '除了订购价格之外，您还可以根据居住国的适用法律收取间接税，如增值税（VAT）或政府服务税（GST）。',
        lbl_ignite_terms_termination:                    '期限与终止条款',
        lbl_subscriber_may_subs:                         '订户可以订阅所选内容，订阅期限将从生效日期开始。',
        lbl_ignite_reserves_right:                       '六云保留因违反或怀疑违反这些服务条款而终止或暂停对订阅内容的所有或任何部分的访问的权利。用户将没有进一步访问网站的权限。终止不影响在终止前累积的任何一方的权利或负债。',
        lbl_when_account_subs:                           '当账户订阅期结束时（例如，订阅到期，如果账户未被更新或被取消），账户不再允许访问订阅内容。',
        lbl_once_the_terms_of:                           '一旦订阅期限结束，六云可以根据本服务条款和我们的隐私政策删除与帐户有关的数据。订户需要与六云请求更新不自动更新的帐户，以保持对帐户及其相关内容的持续访问。',
        lbl_ignite_third_party:                          '第三方链接或信息',
        lbl_ignite_may_contain:                          '该网站可能包含链接到其他网站的链接，这些网站不属于或与六云有关。六云不对此类第三方网站中表达的内容、准确性或意见负责，并且不调查、监视或检查这些网站的准确性或完整性。在这个网站上包含任何链接的网站并不意味着通过六云来批准或认可链接的网站。离开网站访问这些第三方网站的用户自己承担风险。',
        lbl_ignite_prop_rights:                          '所有权',
        lbl_ignite_user_ack:                             '用户确认并同意我们的内容和与内容相关的任何必要软件包含受适用的知识产权和其他法律保护的专利和机密信息。用户进一步确认并同意通过订阅提交给用户的信息受版权、商标、服务商标、专利或其他专有权利和法律的保护。用户同意不复制、修改、租、借用、销售、分发或创建基于内容的派生作品，全部或部分。',
        lbl_ignite_under_terms:                          '根据本服务条款的规定，禁止以任何方式，包括但不限于电子和印刷，分发或复制服务内容或其任何部分。',
        lbl_ignite_sixclouds_reserves:                   '如果确定订户违反了这些条款，六云保留取消订户账户而不退款的权利。',
        lbl_ignite_general_terms:                        '一般条款',
        lbl_ignite_account_hold:                         '六云保留因违反本服务条款或因非法或不适当使用本网站或服务而暂停或永久禁用帐户的权利。违反我们的服务条款并禁用帐户的用户可以联系我们的客服团队以获得更多有关违反和帐户状态的信息。',
        lbl_ignite_account_ownership:                    '用户必须能够通过客服中心提供证明帐户所有权的信息和资料来验证其帐户所有权。',
        lbl_ignite_changes_to_terms:                     '六云可以不时地改变其服务条款。当进行这些更改时，六个云将更新网站上的服务条款。用户理解并同意六云智宝网站在服务条款更改后的任何使用应视为接受更新的服务条款。',
        lbl_ignite_no_joint_venture:                     '由于这些服务条款，您、六云  或任何第三方供应商之间不存在合资、合伙、雇佣或代理关系。',
        lbl_ignite_provision_invalid:                    '如果服务条款的任何规定被认定无效或不可执行，则应予以撤销，其余规定应根据法律予以最大限度的执行。不限于此，也适用于上述规定的适用法律和管辖权。',
        lbl_ignite_faliure_of_sixclouds:                 '除非六云书面承认并同意，否则六云未能执行服务条款中的任何权利或规定不构成对该权利或规定的放弃。服务条款包括您与服务条款之间的全部协议，并取代所有先前或同期的谈判或讨论，无论是书面的还是口头的（如果有的话）关于本合同所包含的主题。',
        lbl_disclamer_warr:                              '免责声明',
        lbl_content_any_service:                         '使用六云智宝网站，其内容以及通过网站获得的任何服务或项目均由用户自担风险。 本网站，其内容以及通过网站获得的任何服务或项目均以“现状”和“现有”的基础提供，没有任何明示或暗示的担保。 无论是六云还是与六云相关的任何人都不对网站的完整性，安全性，可靠性，质量，准确性或可用性做出任何保证或陈述。上述内容不影响根据适用法律不能排除或限制的任何担保。',
        lbl_ignite_limitation:                           '责任限制',
        lbl_ignite_affilates:                            '根据任何法律理论，六云及其附属公司或其许可人，服务提供商，员工，代理人，管理人员或董事在任何情况下都不会因用户的使用或无法使用本网站，与之链接的任何网站，网站上的任何内容或其他网站，或通过网站或其他网站获得的任何服务或项目，包括任何直接，间接，特殊，附带，或惩罚性损害赔偿，包括但不 受限于人身伤害，伤痛和痛苦，情绪困扰，收入损失，利润损失，业务或预期储蓄损失，使用损失，商誉损失，数据丢失以及是否由侵权（包括疏忽）造成的损失，违反合同或其他规定， 即使可以预见。上述内容不影响根据适用法律不能排除或限制的任何责任。',
        lbl_ignite_governing_law:                        '管辖法律',
        lbl_ignite_governed_by:                          '您在此同意本服务条款受新加坡法律管辖，不考虑任何管辖权的法律规定，任何与服务条款有关的纠纷、诉讼、索赔或诉讼理受制于您同意的新加坡法院专属管辖。',
        lbl_all_rights_reserved:                         '版权所有',
        lbl_about_us:                                    '关于我们',
        lbl_quick_links:                                 '快速链接',
        lbl_customer_support:                            '客服中心',
        lbl_terms_service:                               '服务条款',
        lbl_privacy_policy:                              '隐私政策',
        lbl_updated_at:                                  '更新时间: 2020-03-20',
        lbl_:                                            '',
        lbl_the_company:                                 '六云简介',
        lbl_core_values:                                 '核心理念',     
        lbl_our_team:                                    '我们的团队',
        lbl_ignite_distributor_access:                   '分销商',
        
    }).translations('ru', {
        
        lbl_sixclouds_ignite_terms_of_service:           'Условия предоставления услуг SixClouds IGNITE',
        lbl_lang_chng:                                   '中文',
        lbl_sixclouds:                                   'SixClouds',
        lbl_the_company:                                 'The Company',
        lbl_the_following_terms:                         'Следующие условия и положения ("Условия предоставления услуг") – это соглашение между Пользователем (“Вы”) и SixClouds Pte Ltd, Сингапур(“SixClouds”, “Мы”).',
        lbl_the_terms_of_service:                        'Условия предоставления услуг – это полное соглашение между SixClouds и Пользователем. Мы предоставляем доступ и использование сайта SixClouds IGNITE и мобильного приложения, включая любой общий и персонализированный контент, функциональность и услуги, непосредственно и через веб-сайт, мобильное приложение и связанные домены ',
        lbl_www_sixclouds_net:                           'www.sixclouds.net/ignite',
        lbl_and:                                         'и',
        lbl_www_sixclouds_cn:                            'www.sixclouds.cn/ignite',
        lbl_the_site:                                    '("Сайт").',
        lbl_please_read_these:                           'Пожалуйста, внимательно прочитайте эти Условия предоставления услуг, прежде чем начать использовать Сайт.',
        lbl_by_using_the_site_you_are_accepting:         'Используя Сайт, вы принимаете и соглашаетесь соблюдать настоящие Условия предоставления услуг и нашу Политику конфиденциальности, любые другие юридические уведомления или условия или руководящие принципы, загруженные и размещенные на Сайте или включенные в настоящий документ по ссылке. Если вы не желаете соблюдать настоящие Условия обслуживания или Политику конфиденциальности, вы не должны заходить на Сайт или иным образом использовать его.',
        lbl_by_using_the_site_you_are_accepting_warrant: 'Используя этот Сайт, вы заявляете и гарантируете, что достигли совершеннолетия и можете заключать обязательный договор и выполнять все вышеуказанные требования. Если вы не соответствуете всем этим требованиям, то не должны заходить на Сайт или использовать его.',
        lbl_key_terms_to_note:                           'Ключевые термины',
        lbl_ignite:                                      ' IGNITE ',
        lbl_means_the_features:                          'означает контент и услуги, которые мы предоставляем через веб-сайт, мобильное приложение, платформу, связанные домены на ',
        lbl_and_any_other_platform_or_websites:          'и любые другие платформы, веб-сайты или мобильные приложения, включая поддомены, веб и мобильные версии, плагины и другие носители, услуги, программное обеспечение или сети, существующие в настоящее время или которые могут быть разработаны позже. ',
        lbl_users:                                       'Пользователь',
        lbl_means_users_who_purchase:                    'относится как к подписчикам нашего сайта, так и к лицам, не оформившим подписку.',
        lbl_non_subscriber:                              'Не подписчик',
        lbl_means_users_who_offer:                       'это физическое лицо, которое имеет доступ к сайту, но не имеет права доступа к содержанию подписки.',
        lbl_subscriber:                                  'Подписчик',
        lbl_are_creative_capabilities:                   'означает отдельного пользователя, который имеет право доступа к содержанию подписки через платный план подписки.',
        lbl_subscription_content:                        'Содержание подписки',
        lbl_are_creative_services:                       ' означает видео, изображения, аудио, музыку, звуки, викторины, рабочие листы и другие файлы, информацию и контент, которые доступны в платном плане подписки.',
        lbl_subscriber_generated_content:                'Контент, созданный подписчиком',
        lbl_are_additional_services:                     ' означает весь контент, который подписчик загружает, публикует или иным образом предоставляет Сайту.',
        lbl_subs:                                        'Подписка',
        lbl_is_where_buyers_and_sellers:                 ' означает действующий на этот момент план(ы) подписки, за который Подписчик заплатит или заплатил плату за подписку для использования через Сайт.',
        lbl_subs_term:                                   'Срок подписки',
        lbl_means_period_of:                             ' означает период времени, в течение которого Подписчик может использовать соответствующую часть содержимого подписки на Сайте.',
        lbl_general_overview_of_terms_of_service:        'Доступ, лицензия и право на использование',
        lbl_only_registered_users:                       'Пользователь должен быть не моложе 18 лет (а если он несовершеннолетний, то должен иметь разрешение от родителя или законного опекуна на доступ к Сайту) и обладать законным правом и способностью заключать обязательные договора.',
        lbl_services_on_sixclouds_may_be:                'В соответствии с настоящими Условиями предоставления услуг, Подписчику будет предоставлено ограниченное, неисключительное, подлежащее отмене, непередаваемое и не подлежащее сублицензированию право доступа к той части Содержимого подписки, которая применима к типу Подписки. Соглашаясь предоставить такой доступ, SixClouds не обязывает себя поддерживать веб-сайт, веб-приложение и связанные с ними домены в их нынешнем виде. Мы можем модернизировать, модифицировать, изменять или улучшать услуги и конвертировать Подписчика в его новый статус в любое время по своему усмотрению в той мере, в которой это не наносит ущерба использованию услуг Подписчиком и по разумному предварительному уведомлению Подписчика.',
        lbl_payment_is_to_be_made_by:                    'Пользователь несет ответственность за все действия или бездействие подопечных ему пользователей, моложе 18 лет в отношении доступа и использования Сайта; кроме того, во избежание сомнений, Пользователь несет ответственность за обеспечение того, чтобы его несовершеннолетние пользователи полностью соблюдали настоящие Условия предоставления услуг и Политику конфиденциальности.',
        lbl_seller_must_complete_the_project:            'Пользователь соглашается соблюдать любые правила или положения, которые мы публикуем в отношении поведения Пользователей Сайта, которые настоящим включены в текущие Условия предоставления услуг по этой ссылке. SixClouds оставляет за собой право отказать Пользователю в доступе к Сайту, если по собственному усмотрению SixClouds, Пользователь не соблюдает настоящие Условия предоставления услуг или очевидно не делает этого.',
        lbl_buyers_and_sellers_are_encouraged:           'Пользователь соглашается с тем, что SixClouds по своему усмотрению может, но не обязан отслеживать Услуги или любую их часть и/или следить за соблюдением настоящих Условий предоставления услуг.',
        lbl_users_agree_to_abide_to:                     'Пользователь обещает, признает и соглашается с тем, что:',
        lbl_illegal_or_fraudulent_services:              'Права доступа не могут быть переданы третьим лицам;',
        lbl_copyright_infringement:                      'Он будет соблюдать все применимые законы и правила в отношении использования Сайта;',
        lbl_publish_or_distribute:                       'Он не будет арендовать, сдавать в аренду, сублицензировать, перепродавать, распространять, передавать, копировать или изменять содержимое сайта или любого его компонента;',
        lbl_intentional_copying:                         'Он не будет переводить, декомпилировать, создавать или пытаться создать содержимое Cайта, путем обратного проектирования или иным образом;',
        lbl_ignite_will_not_reproduce:                   'Он не будет воспроизводить, распространять, изменять, создавать производные работы, публично демонстрировать, публично исполнять, переиздавать или передавать содержимое Сайта или любую его часть;',
        lbl_ignite_will_not_delete_alter:                'Он не будет удалять или изменять какие-либо уведомления об авторских правах, товарных знаках или других правах собственности из копий контента с Сайта;',
        lbl_ignite_will_not_use_site:                    'Он не будет использовать Cайт каким-либо образом или в связи с любым контентом, данными, оборудованием, программным обеспечением или другими материалами, которые нарушают или посягают на любой патент, авторское право, коммерческую тайну, товарный знак или иное право интеллектуальной собственности какой-либо третьей стороны, которое представляет собой дискредитацию, клевету, вторжение в частную жизнь или нарушение какого-либо права на публичность или права третьей стороны или  которое угрожает, преследует или приносит вред.',
        lbl_users_recognizes:                            'Пользователь признает, что трафик данных через Интернет может вызвать задержки при загрузке информации с Сайта и, соответственно, SixClouds не несет ответственности за задержки, которые являются обычными при использовании Интернета. Пользователь также признает и принимает, что Cайт не будет доступен на постоянной основе двадцать четыре часа в сутки из-за этих задержек или задержек, вызванных обновлением, модификацией или стандартными процедурами по обслуживанию Сайта.',
        lbl_users_agrees_applicable:                     'Пользователь соглашается, когда это применимо, рассматривать пароль, имена пользователей и другую информацию как конфиденциальную и не предоставлять любому другому лицу доступ к Содержимому подписки или его части с использованием аутентификационной информации Пользователя. Пользователь немедленно уведомит SixClouds о любом несанкционированном доступе к аутентификационной информации или о ее использовании. SixClouds не может и не будет нести ответственности за любой несанкционированный доступ к вашей учетной записи или данным, который возникает в результате ваших действий или бездействий.',
        lbl_access_license_and_right_to_Use:             'Абонентская плата и оплата',
        lbl_subject_to_the_terms_of_service:             'Подписчик соглашается оплатить плату, соответствующую конкретному Тарифному плану, к которому он хочет получить доступ на соответствующий Срок подписки. SixClouds оставляет за собой право изменять размер Платы за подписку, и если иное прямо не указано явно, то все сборы и оплаты не подлежат отмене и возврату.',
        lbl_you_hereby_agree_to_grant_us_a_worldwide:    'В дополнение к Цене подписки, возможно взимание косвенных налогов, таких как налог на добавленную стоимость (НДС) или налог на государственные услуги (GST) в соответствии с действующим законодательством страны вашего проживания. ',
        lbl_ignite_terms_termination:                    'Срок и прекращение действия договора',
        lbl_subscriber_may_subs:                         'Подписчик может подписаться на Содержание подписки на соответствующий срок конкретной Подписки, начинающийся с момента вступления в силу.',
        lbl_ignite_reserves_right:                       'SixClouds оставляет за собой право прекратить или приостановить доступ ко всем или любой части Содержимого подписки за нарушение или подозрение на нарушение этих Условий предоставления услуг. Подписчик не будет иметь никаких дополнительных прав на доступ к Сайту. Расторжение договора не влияет на права или обязательства любой из сторон, накопленные до расторжения договора.',
        lbl_when_account_subs:                           'Когда заканчивается подписка на аккаунт (например, в конце срока, если аккаунт не был продлен или был отменен), то такой аккаунт больше не дает права на доступ к Содержанию подписки.',
        lbl_once_the_terms_of:                           'По истечении срока действия подписки, SixClouds может удалять данные, относящиеся к учетной записи в соответствии с настоящими Условиями предоставления услуг и нашей Политикой конфиденциальности. Только Подписчик несет ответственность за запрос на обновление учетных записей, которые не обновляются автоматически, чтобы поддерживать постоянный доступ к учетной записи и содержимому, связанному с ней.',
        lbl_ignite_third_party:                          'Ссылки или информация третьих лиц',
        lbl_ignite_may_contain:                          'Этот Сайт может содержать ссылки на другие веб-сайты, которые не управляются или не связаны с SixClouds. SixClouds не несет ответственности за содержание, достоверность или мнения, выраженные на таких сторонних веб-сайтах, и не исследует, не контролирует и не проверяет эти сайты на достоверность или полноту. Включение любого связанного веб-сайта на этом Сайте не означает одобрения или утверждения связанного веб-сайта SixClouds. Пользователь, который покидает сайт для входа на эти сторонние сайты, делает это на свой страх и риск.',
        lbl_ignite_prop_rights:                          'Права собственности',
        lbl_ignite_user_ack:                             'Пользователь признает и соглашается с тем, что наш Контент и любое необходимое программное обеспечение, используемое в связи с Контентом, конфиденциальную информацию, которая защищена законами об интеллектуальной собственности и другими законами. Пользователь также признает и соглашается с тем, что информация, представленная Пользователю посредством Подписки, защищена авторскими правами, товарными знаками, знаками обслуживания, патентами или другими правами собственности и законами. Пользователь обязуется не копировать, не изменять, не сдавать в аренду, не одалживать, не продавать, не распространять и не проводить производные работы на основе Контента, полностью или частично.',
        lbl_ignite_under_terms:                          'В соответствии с положениями настоящих Условий предоставления услуг, категорически запрещается распространять или воспроизводить содержание Предоставления услуг или любой его части любыми средствами, включая, но не ограничиваясь электронными и печатными.',
        lbl_ignite_sixclouds_reserves:                   'SixClouds оставляет за собой право аннулировать счета Подписчика без возврата средств, если будет установлено, что Подписчик нарушил настоящие условия.',
        lbl_ignite_general_terms:                        'Общие условия',
        lbl_ignite_account_hold:                         'SixClouds оставляет за собой право заблокировать любую учетную запись или навсегда отключить учетные записи из-за нарушения настоящих Условий предоставления услуг или из-за любого незаконного или ненадлежащего использования сайта. Пользователи, которые нарушили настоящие Условия предоставления услуг и отключили свою учетную запись, могут обратиться в нашу службу поддержки для получения дополнительной информации о нарушении и статусе учетной записи.',
        lbl_ignite_account_ownership:                    'Пользователи должны иметь возможность подтвердить свое право собственности на учетную запись через Службу поддержки, предоставляя информацию и материалы, подтверждающие право собственности на учетную запись.',
        lbl_ignite_changes_to_terms:                     'SixClouds может время от времени вносить изменения в свои условия предоставления услуг. Когда эти изменения будут сделаны, SixClouds сделает обновленную копию Условий предоставления услуг доступной на сайте. Пользователи понимают и соглашаются с тем, что любое использование сайта SixClouds IGNITE после даты, когда изменились Условия предоставления услуг, будет рассматриваться, как принятие обновленных Условий  предоставления услуг.',
        lbl_ignite_no_joint_venture:                     'Никакие совместные предприятия, партнерства, трудовые или агентские отношения не существуют между вами, SixClouds или любым сторонним поставщиком в результате этих Условий предоставления услуг.',
        lbl_ignite_provision_invalid:                    'Если какое-либо положение Условий предоставления услуг будет признано недействительным или неисполнимым, то такое положение должно быть исключено, а остальные положения останутся в силе, в полном объеме и в соответствии с законодательством. Это также должно применяться без ограничения к применимому законодательству и юрисдикции, как указано выше.',
        lbl_ignite_faliure_of_sixclouds:                 'Неспособность SixClouds обеспечить соблюдение какого-либо права или положения в Условиях предоставления услуг не означает отказ от такого права или положения, если только SixClouds не подтвердит это и не согласится с ним в письменной форме. Условия предоставления услуг включают в себя полное ваше согласие с Условиями предоставления услуг и заменяют все предыдущие или одновременные переговоры или обсуждения, будь то письменные или устные (если таковые имеются) между сторонами относительно предмета, содержащегося в настоящем документе.',
        lbl_disclamer_warr:                              'Отказ от гарантий',
        lbl_content_any_service:                         'Использование сайта SixClouds IGNITE, его содержимого и любых услуг или элементов, полученных через веб-сайт и/или мобильное приложение, осуществляется на собственный риск пользователя. Сайт, его содержание и любые услуги или элементы, полученные через веб-сайт и/или мобильное приложение, предоставляются на условиях "как есть" и "как доступно", без каких-либо гарантий любого рода, явных или предполагаемых. Ни SixClouds, ни любое лицо, связанное с SixClouds, не дает никаких гарантий или заверений в отношении полноты, безопасности, надежности, качества, достоверности или доступности веб-сайта. Вышеизложенное не влияет на какие-либо гарантии, которые не могут быть исключены или ограничены в соответствии с применимым законодательством.',
        lbl_ignite_limitation:                           'Ограничение ответственности',
        lbl_ignite_affilates:                            'Ни при каких обстоятельствах компания SixClouds, ее дочерние компании или их лицензиары, поставщики услуг, сотрудники, агенты, должностные лица или директора не несут ответственности за ущерб любого рода, в соответствии с любой правовой теорией, возникающий из или в связи с использованием или невозможностью использования веб-сайта и/или мобильного приложения, любых веб-сайтов, связанных с ним, любого контента на веб-сайте и/или в мобильном приложении или на таких же других веб-сайтах или любые услуги или предметы, полученные через веб-сайт и/или мобильное приложение или такие другие веб-сайты, включая любые прямые, косвенные, специальные, случайные, косвенные или штрафные убытки, включая, но не ограничиваясь, травмами, болью и страданиями, эмоциональными расстройствами, потерей дохода, потерей прибыли, потерей бизнеса или ожидаемой экономией, потерей использования, потерей деловой репутации, потерей данных, а также вызвано ли это правонарушением (включая небрежность), нарушением договора или иным образом, даже если это предсказуемо. Вышеизложенное не затрагивает никакой ответственности, которая не может быть исключена или ограничена в соответствии с применимым законодательством.',
        lbl_ignite_governing_law:                        'Регулирующее законодательство',
        lbl_ignite_governed_by:                          'Настоящие Условия предоставления услуг регулируются законодательством Сингапура без учета положений о выборе права любой юрисдикции и любые споры, иски, претензии или основания для иска, вытекающие из или в связи с Условиями предоставления услуг подлежат исключительной юрисдикции суда Сингапура, которому вы соглашаетесь безоговорочно подчиниться. ',
        lbl_all_rights_reserved:                         'Все  права защищены',
        lbl_about_us:                                    'О Нас',
        lbl_quick_links:                                 'Быстрые ссылки',
        lbl_customer_support:                            'Поддержка клиентов',
        lbl_terms_service:                               'Условия обслуживания',
        lbl_privacy_policy:                              'Конфиденциальность и безопасность',
        lbl_updated_at:                                  'Обновлено: Март 2020',
        lbl_:                                            '',
        lbl_the_company:                                 'Компания',
        lbl_core_values:                                 'Фундаментальные ценности', 
        lbl_our_team:                                    'Наша команда',
        lbl_ignite_distributor_access:                   'Доступ к распределителю',
        
    });
    $translateProvider.preferredLanguage(Cookies.get('language'));
});
/* To change the interolate provider need to change it's originonal brackets.*/
app.config(['$interpolateProvider', function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
}]);
/* for changing title */
app.run(['$location', '$rootScope', '$http', '$timeout', function($location, $rootScope, $http, $timeout) {
    $rootScope.$on('$routeChangeSuccess', function(event, current, previous) {
        if (current.$$route != undefined) $rootScope.title = current.$$route.title;
        document.body.scrollTop = document.documentElement.scrollTop = 0;
    });
}]);
app.controller('IgniteTermsofServiceController', ['$scope', '$translate', '$rootScope', '$timeout', function ($scope, $translate, $rootScope, $timeout){
	console.log("Ignite terms of service Controller");
	
	var ctrl = this;
    if (Cookies.get('language') == 'undefined' || Cookies.get('language') == '' || Cookies.get('language') == undefined) {
        $scope.url = window.location.host;
        $scope.url = $scope.url.split('.');
        $scope.url = $scope.url[2];
        $timeout(function() {
            if($scope.url==undefined){
                $timeout(function() {
                    $scope.changeLang('en');
                },500);
            }
            if($scope.url == 'net' && $scope.url!=undefined){
                $timeout(function() {
                    $scope.changeLang('en');
                },500);
            }
            if($scope.url == 'cn' && $scope.url!=undefined){
                $timeout(function() {
                    $scope.changeLang('chi');
                },500);
            }
        },1000);
    }
    $scope.changeLangs = Cookies.get('language');
    $scope.changeLang = function(lang) {
        if (lang == 'en') $scope.changeLangs = 'en';
        else if (lang == 'chi') $scope.changeLangs = 'chi';
        else $scope.changeLangs = 'ru';
        var getLanguage = lang;
        ctrl.language = getLanguage;
        Cookies.set('language', ctrl.language);
        $translate.use(ctrl.language);
    }
}]);
