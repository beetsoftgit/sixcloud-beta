/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 24th Aug 2018
 Name    : IgniteSignUpController
 Purpose : All the functions for ignite user registration
 */
angular.module('sixcloudApp').controller('IgniteSignUpController', ['$sce', 'ignitefactory', 'mpfactory', '$location', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', function($sce, ignitefactory, mpfactory, $location, $routeParams, $http, $scope, $timeout, $rootScope, $filter) {
    /*urlObj = JSON.parse(localStorage.getItem('urlObj'));
    url_number_static = localStorage.getItem('url_number_static');
    url_number_static++;
    url = [{
        "url_number": url_number_static,
        "url": $location.url()
    }];
    last_url = urlObj.pop();
    localStorage.setItem('urlObj',JSON.stringify(urlObj));
    if(last_url[0].url_number !== url[0].url_number) {
        urlObj.push(last_url);
    }
    urlObj.push(url);
    localStorage.setItem("urlObj",JSON.stringify(urlObj));
    localStorage.setItem("url_number_static",url_number_static);*/
    console.log('IgnitesignupupController');
    $scope.slug = $routeParams.slug;
    $scope.mode = $routeParams.mode;
    $rootScope.ignitePage = "";

    $scope.url = $location.url();
    if($scope.url.split('/')[2] != undefined){
        $scope.url = '/'+ $scope.url.split('/')[2];
        if($scope.url == '/twinkle-ignite-signup')
        {
            $rootScope.ignitePage = "Twinkle";
        }    
    }else{
        $scope.url = '/'+ $scope.url.split('/')[1];
        if($scope.url == '/twinkle-ignite-signup')
        {
            $rootScope.ignitePage = "Twinkle";
        }
    }
    $scope.Getpageasperurl = function(getUrl){
        if(getUrl == '/twinkle-ignite-signup')
            pageData      = 'twinkle';
        if(getUrl == '/ignite-signup')
            pageData      = 'users';
        return pageData;
    }
    $scope.pageData = $scope.Getpageasperurl($scope.url);

    if($scope.pageData == 'twinkle')
    {
        $("#sign-up-form").addClass('sign-up-form-blue');
        $("#email_address").addClass('form-controlblue');
        $("#first_name").addClass('form-controlblue');
        $("#last_name").addClass('form-controlblue');
        $("#country_dropdown").addClass('form-controlblue');
        $("#state_dropdown").addClass('form-controlblue');
        $("#city_dropdown").addClass('form-controlblue');
        $("#datepicker").addClass('form-controlblue');
        $("#phonecode").addClass('form-controlblue');
        $("#contact").addClass('form-controlblue');
        $("#password").addClass('form-controlblue');
        $("#confirm_password").addClass('form-controlblue');
        $("#sigup_button").addClass('btn-blue');
        $("#select-dropdown-country").addClass('custom-select-dropdown-blue');
        $("#select-dropdown-state").addClass('custom-select-dropdown-blue');
        $("#select-dropdown-city").addClass('custom-select-dropdown-blue');
        $("#twinkle-button").addClass('btn-blue');
    }

    function sub_years(dt,n) 
    {
        return new Date(dt.setFullYear(dt.getFullYear() - n));
    }
    dt = new Date();
    strtDate = sub_years(dt, 18); /* Age should be more than 18 years */
    $( function() {
        $( "#datepicker" ).datepicker({
            format: 'dd/mm/yyyy',
            endDate: strtDate,
            autoclose: true
        });
    });
    getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
    changeLanguage(getLanguage);
    $('#chk_lang').change(function() {
        getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
        changeLanguage(getLanguage);
    });
    function changeLanguage(lang) {
        if (lang == "chi") {
            $timeout(function(){
                $scope.chinese = true;
                $scope.english = false;
                
            },500);
        } else {
            $timeout(function(){
                $scope.chinese = false;
                $scope.english = true;
            },500);
        }
    }
    mpfactory.Getcountry().then(function(response){
        if(response.data.status==1)
        {
            $scope.countries = response.data.data.country;
        }      
    });
    $scope.getstates = function(id){
        mpfactory.Getstatesforcountry({'id': id}).then(function(response){
            if(response.data.status==1)
            {
                $scope.states = response.data.data;
                
            }
        });
    }
    $scope.getcities = function(id){
        mpfactory.Getcitiesforstate({'id': id}).then(function(response){
            if(response.data.status==1)
            {
                $scope.cities = response.data.data;
            }
        });
    }
    /*$scope.getcountrystates = function(){
        var country = $('#country_dropdown').val();
        if(country!=''&&country!=undefined){
            country = JSON.parse(country);
            $scope.phonecode = country['phonecode'];
            country = country['id'];
            if(country!=''){
                $scope.getstates(country);
            } else {
                $scope.states = undefined;
                $scope.cities = undefined;
            }
            $('#state_dropdown').val('');
            $('#city_dropdown').val('');
        }
    }*/

    $('#country_dropdown').on('change',function(){
        var country = $('#country_dropdown').val();
        if(country!=''&&country!=undefined){
            country = JSON.parse(country);
            $scope.phonecode = country['phonecode'];
            country = country['id'];
            if(country!=''){
                $scope.getstates(country);
            } else {
                $scope.states = undefined;
                $scope.cities = undefined;
            }
            $('#state_dropdown').val('');
            $('#city_dropdown').val('');
        }
    });

    /*$scope.getstatecities = function(id){
        var state = $('#state_dropdown').val();
        if(state!=''){
            $scope.getcities(state);
        } else {
            $scope.cities = undefined;
        }
        $('#city_dropdown').val('');
    }*/

    $('#state_dropdown').on('change',function(){
        var state = $('#state_dropdown').val();
        if(state!=''){
            $scope.getcities(state);
        } else {
            $scope.cities = undefined;
        }
        $('#city_dropdown').val('');
    });

    var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,16})");
    $scope.Checkexpression = function(password){
        if(strongRegex.test(password)) {
                $scope.new_password_expression_error = false;
        } else {
                $scope.new_password_expression_error = true;
        }
    }

    $scope.Checkcnfpassword = function(password){
        if($scope.confirm_password != $scope.new_password){
            $scope.confirm_password_error_no_match = true;
            $scope.confirm_password_error = false;
        } else {
            $scope.confirm_password_error_no_match = false;
            $scope.confirm_password_error = false;
        }
    }

    function readURL(input) {
      if (input.files && input.files[0]&&input.files.length>0) {
        var reader = new FileReader();
        reader.onload = function(e) {
          $('#previewImage').css('background-image','url('+e.target.result+')');
        }
        reader.readAsDataURL(input.files[0]);
      } else {
        $('#previewImage').css('background-image','url(resources/assets/images/user-img.png)');
        $('#imagePic').val('');
      }
    }

    $("#imagePic").change(function() {
        readURL(this);
    });

    $scope.calculateAge = function(dob) {
        var date = dob;
        var datearray = date.split("/");
        var newdate = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
        var ageDifMs = Date.now() - new Date(newdate);
        var ageDate = new Date(ageDifMs);
        $scope.age = Math.abs(ageDate.getUTCFullYear() - 1970);
    }

    $scope.Ignitesignupuser = function(){
        if(Cookies.get('language') == 'en') {
            var emailAddress = 'Error: Please provide all required details';
            var firstName = 'Error: Please provide all required details.'
            var lastName = 'Error: Please provide all required details';
            var countryRequired = 'Error: Please provide all required details';
            var stateRequired = 'Error: Please provide all required details';
            var cityRequired = 'Error: Please provide all required details';
            var dobRequired = 'Error: Please provide all required details';
            var contactRequired = 'Error: Please provide all required details';
            var passwordRequired = 'Error: Please provide all required details';
            var passwordPattern = 'Please use a password which comprises of 8 to 16 characters, with at least 1 uppercase letter, 1 lowercase letter and 1 number.';
            var confirmPassword = 'Error: Please provide all required details';
            var samePassword = 'Password and confirm password should be the same.';
            var aboveAge = "You should be of 18 years or above to proceed.";
        } else if(Cookies.get('language') == 'chi') {
            var emailAddress = "请填写所有资料。";
            var firstName = "请填写所有资料。";
            var lastName = "请填写所有资料。";
            var countryRequired = "请填写所有资料。";
            var stateRequired = "请填写所有资料。";
            var cityRequired = "请填写所有资料。";
            var dobRequired = "请填写所有资料。";
            var contactRequired = "请填写所有资料。";
            var passwordRequired = "请填写所有资料。";
            var passwordPattern = "请使用由8到16个字符组成的密码，其中至少有1个大写字母、1个小写字母和1个数字。";
            var confirmPassword = "请填写所有资料。";
            var samePassword = "密码应该相同。";
            var aboveAge = "您应该年满18岁才能登记。";
        }else if(Cookies.get('language') == 'ru'){
            var emailAddress = 'Ошибка Пожалуйста, предоставьте все необходимые данные.';
            var firstName = 'Ошибка Пожалуйста, предоставьте все необходимые данные.'
            var lastName = 'Ошибка Пожалуйста, предоставьте все необходимые данные.';
            var countryRequired = 'Ошибка Пожалуйста, предоставьте все необходимые данные.';
            var stateRequired = 'Ошибка Пожалуйста, предоставьте все необходимые данные.';
            var cityRequired = 'Ошибка Пожалуйста, предоставьте все необходимые данные.';
            var dobRequired = 'Ошибка Пожалуйста, предоставьте все необходимые данные.';
            var contactRequired = 'Ошибка Пожалуйста, предоставьте все необходимые данные.';
            var passwordRequired = 'Ошибка Пожалуйста, предоставьте все необходимые данные.';
            var passwordPattern = 'Ошибка Пожалуйста, используйте пароль, который содержит от 8 до 16 символов, по крайней мере, 1 заглавная буква, 1 строчная буква и 1 цифра.';
            var confirmPassword = 'Ошибка Пожалуйста, предоставьте все необходимые данные.';
            var samePassword = 'Password and confirm password should be the same.';
            var aboveAge = "Вам должно быть 18 лет или выше, чтобы продолжить.";
        }

        $scope.country_id = $('#country_dropdown').val();
        if($scope.country_id!=''&&$scope.country_id!=undefined){
            $scope.country_id = JSON.parse($scope.country_id);
            $scope.country_id = $scope.country_id['id'];
        }else {
            $scope.country_id = undefined;
        }
        $scope.state_id = $('#state_dropdown').val();
        $scope.city_id = $('#city_dropdown').val();
        if($scope.email_address==undefined||$scope.email_address==''||$scope.first_name==undefined||$scope.first_name==''||$scope.last_name==undefined||$scope.last_name==''||$scope.contact==undefined||$scope.contact==''||$scope.country_id==undefined||$scope.country_id=='' ||$scope.state_id==undefined||$scope.state_id=='' ||$scope.city_id==undefined||$scope.city_id==''||$scope.password==undefined||$scope.password==''||$scope.confirm_password==undefined||$scope.confirm_password==''||$scope.new_password_expression_error == true||$scope.dob==undefined||$scope.dob==''||$scope.confirm_password != $scope.password){
            
            if($scope.email_address==undefined||$scope.email_address==''){
                simpleAlert('error','',emailAddress);
                return; 
            }
            
            if($scope.first_name==undefined||$scope.first_name==''){
                simpleAlert('error','',firstName);
                return; 
            }
            
            if($scope.last_name==undefined||$scope.last_name==''){
                simpleAlert('error','',lastName);
                return;
            }
            
            if($scope.country_id==undefined||$scope.country_id==''){
                simpleAlert('error','',countryRequired);
                return; 
            }
            
            if($scope.state_id==undefined||$scope.state_id==''){
                simpleAlert('error','',stateRequired);
                return; 
            }
            
            if($scope.city_id==undefined||$scope.city_id==''){
                simpleAlert('error','',cityRequired);
                return;
            }
            
            if($scope.dob==undefined||$scope.dob==''){
                simpleAlert('error','',dobRequired);
                return;
            }
            
            if($scope.contact==undefined||$scope.contact==''){
                simpleAlert('error','',contactRequired);
                return;
            }
            
            if($scope.password == undefined||$scope.password == ''){
                simpleAlert('error', '', passwordRequired);
                return;
            }
            
            if($scope.new_password_expression_error == true){
                simpleAlert('error', '', passwordPattern);
                return;
            }
            
            if($scope.confirm_password == undefined||$scope.confirm_password == ''){
                simpleAlert('error', '', confirmPassword);
                return;
            }
            
            if($scope.confirm_password != $scope.password){
                simpleAlert('error', '', samePassword);
                return;
            }
        } else {
            $('#signuptermsandconditionsmodal').modal('show');
        }
    }

    $scope.Dosignup = function(){
        if(!$("#signup_tandcAccept_part2").is(':checked')){
            if(Cookies.get('language') == 'en') {
                simpleAlert('info','','Please accept our Terms of Service and Privacy Policy.');
            } else if(Cookies.get('language') == 'chi') {
                simpleAlert('info','','请接受服务条款和隐私政策。');
            } else if(Cookies.get('language') == 'ru') {
                simpleAlert('info','','Пожалуйста, примите наши Условия обслуживания и Политику конфиденциальности.');
            }
        } else {
            $('#signuptermsandconditionsmodal').modal('hide');
            $('#signupemailmodal').modal('show');
        }  
    }

    $scope.Dosignupemail = function(){
        if($("#signup_tandcAccept_part2").is(':checked')){
            showLoader('.ignite_signup');
            $('#signupemailmodal').modal('hide');   
            var signupInfo = new FormData($("#ignite_signup_form")[0]);
            
            signupInfo.append('language',Cookies.get('language'));
            signupInfo.append("x1", 63);
            signupInfo.append("y1", 0);
            signupInfo.append("w", 176);
            signupInfo.append("h", 176);
            signupInfo.append("is_ignite", 1);
            signupInfo.append("country_id", $scope.country_id);
            signupInfo.append("provience_id", $scope.state_id);
            signupInfo.append("signup_for", 'ignite');
            signupInfo.append('phonecode',$scope.phonecode);
            signupInfo.append('timezone', moment.tz.guess())
            if($scope.pageData == 'twinkle')
            {
                signupInfo.append('twinkle',1);
            }
            if($scope.slug!=undefined){
                signupInfo.append("signup_through_url", $scope.slug);
            }
            if($scope.mode!=undefined){
                signupInfo.append("signup_mode", $scope.mode);
            }
            ignitefactory.Ignitesignupuser(signupInfo).then(function(response){
                hideLoader('.ignite_signup', 'Sign Up');
                if(response.data.status==1)
                {
                    $scope.data = response.data.data;
                    if($scope.data.status==4){
                        if($scope.pageData == 'twinkle'){
                            window.location.replace("twinkle-ignite-login");
                        } else {
                            window.location.replace("ignite-login");
                        }   
                    } else {
                        window.location.replace("user-verify-otp/"+$scope.data.phonecode+'/'+$scope.data.contact+'/'+$scope.data.email_address);
                    }
                }
                if(response.data.status==0) {
                    simpleAlert('error','',response.data.message);
                }
            });
        }
    }
}]);
