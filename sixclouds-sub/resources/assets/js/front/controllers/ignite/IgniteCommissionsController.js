/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 28th Aug 2018
 Name    : IgniteCommissionsController
 Purpose : All the functions for distributors login
 */
angular.module('sixcloudApp').controller('IgniteCommissionsController', ['$sce', 'ignitefactory', '$location', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', function($sce, ignitefactory, $location, $routeParams, $http, $scope, $timeout, $rootScope, $filter) {
    /*urlObj = JSON.parse(localStorage.getItem('urlObj'));
    url_number_static = localStorage.getItem('url_number_static');
    url_number_static++;
    url = [{
        "url_number": url_number_static,
        "url": $location.url()
    }];
    last_url = urlObj.pop();
    localStorage.setItem('urlObj',JSON.stringify(urlObj));
    if(last_url[0].url_number !== url[0].url_number) {
        urlObj.push(last_url);
    }
    urlObj.push(url);
    localStorage.setItem("urlObj",JSON.stringify(urlObj));
    localStorage.setItem("url_number_static",url_number_static);*/
    $timeout(function(){
        $('.ignite-menu').find('li').removeClass('active');
        $('#commissions').addClass('active');
        $scope.userData = $rootScope.loginDistributorData;
        if($scope.userData.userType=='distributor'){
            ignitefactory.Getteamforcommissions().then(function(response) {
                if(response.data.status==1){
                    $scope.teamData = response.data.data;
                } else {

                }
            });    
        }
    },1500);
    $scope.url = $location.url();
    $scope.type = 0;
    $scope.year = (new Date()).getFullYear();
    
    $scope.Getcommissions = function(year,type,team_member){
        ignitefactory.Getcommissions({'year':year,'type':type,'team_member':team_member}).then(function(response) {
            if(response.data.status==1){
                $scope.commissions = response.data.data;
            } else {

            }
        });
    }
    $scope.team = {};
    ignitefactory.Gettypeandyear().then(function(response) {
        if(response.data.status==1){
            $scope.years = response.data.data.years;
            $scope.types = response.data.data.type;
            $scope.Getcommissions($scope.year,$scope.type,$scope.team.member_name);
        }
    });

}]);