/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 7th Sept 2018
 Name    : IgnitePerformancesController
 Purpose : All the functions for subscribers performance page
 */
angular.module('sixcloudApp').controller('IgnitePerformancesController', ['$sce', 'ignitefactory', '$location', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', function($sce, ignitefactory, $location, $routeParams, $http, $scope, $timeout, $rootScope, $filter) {
    /*urlObj = JSON.parse(localStorage.getItem('urlObj'));
    url_number_static = localStorage.getItem('url_number_static');
    url_number_static++;
    url = [{
        "url_number": url_number_static,
        "url": $location.url()
    }];
    last_url = urlObj.pop();
    localStorage.setItem('urlObj',JSON.stringify(urlObj));
    if(last_url[0].url_number !== url[0].url_number) {
        urlObj.push(last_url);
    }
    urlObj.push(url);
    localStorage.setItem("urlObj",JSON.stringify(urlObj));
    localStorage.setItem("url_number_static",url_number_static);*/
    $timeout(function(){
    },600);
    $scope.url = $location.url();
    $scope.language = '';
    $rootScope.ignitePage='my-performance';
    $rootScope.Mathematics = "";
    language = Cookies.get('language');
    if($scope.language !== language) {
        $scope.language = language;
    }
    
    $("#content").removeClass('hide');
    $rootScope.showiframe = false;
    $rootScope.Twinklemenu = false;
    $rootScope.subheader = '';
    $rootScope.logoutTwinkle = false;
    ignitefactory.Getsubjects().then(function(response) {
        if(response.data.data.length > 0){
            $scope.subjects = response.data.data;

            $rootScope.isBUZZ=$scope.subjects.find(data=>{ return data.id==1 });
            $rootScope.isBUZZRU=$scope.subjects.find(data=>{ return data.id==2 });
            $rootScope.isMAZE=$scope.subjects.find(data=>{ return data.id==3 });
            $rootScope.isSMILE=$scope.subjects.find(data=>{ return data.id==4 });
            $rootScope.isSMILESG=$scope.subjects.find(data=>{ return data.id==7 });

            $rootScope.isMAZE=$rootScope.isMAZE?true:false;
            $rootScope.isSMILE=$rootScope.isSMILE?true:false;
            $rootScope.isSMILESG=$rootScope.isSMILESG?true:false;
            $rootScope.isBUZZ=$rootScope.isBUZZ?true:false;
            $rootScope.isBUZZRU=$rootScope.isBUZZRU?true:false;

        }
    });
    setInterval(function() {
        language = Cookies.get('language');
        if($scope.language !== language) {
            $scope.language = language;
            $scope.$apply()
        }
    },2000);
    ignitefactory.GetallPerformanceData().then(function (response) {                    	
        if(response.data.status == 1){
            $scope.totalCount         = response.data.data.totalVideoQuizCount;
            $scope.totalQuizCount     = response.data.data.quizCount.length;
            $scope.totalVideoCount    = response.data.data.videoCount;
            $scope.averageQuizPerformance = response.data.data.averageQuizPerformance;
            $scope.viewedVideos       = response.data.data.viewedVideos;
            $scope.attemptedQuiz       = response.data.data.attemptedQuiz;
        }else if(response.data.status == 0){
            if(response.data.code==2){
                window.location.replace("distributor");
            }         
        }            
    });

    $scope.redirect = function(categoryid,subcategoryid,subjectid,content_type,slug){
        angular.forEach($scope.subjects, function(values, keys) {
            if(values.id == subjectid) {
                $scope.subject_language = values.audio_language;
            }
        });
        ignitefactory.CategoryExist({'id':subcategoryid}).then(function (response) {
            if(response.data.status == 1){
                $scope.categoryExist = response.data.data;
                if($scope.categoryExist != undefined && $scope.categoryExist != 0 && $scope.categoryExist != null){
                    if($scope.categoryExist.on_detail_page == 1)
                    {
                        $scope.getData(subcategoryid,subjectid,content_type,slug);   
                    } else {
                        if(content_type == 2) {
                            $scope.error = "This quiz no longer exists";
                            simpleAlert('error', '', $scope.error);
                        } else if(content_type == 1) {
                            $scope.error = "This video no longer exists";
                            simpleAlert('error', '', $scope.error);
                        }
                    }
                } else {
                    $scope.getData(categoryid,subjectid,content_type,slug);
                }
            }else if(response.data.status == 0){
                if(response.data.code==2)
                    window.location.replace("ignite-login");
            }
        });
    }
    $scope.getData = function(categoryid,subjectid,content_type,slug){
        ignitefactory.Getallcontent({'categoryId':categoryid,'lang':$scope.language,'subject_language':$scope.subject_language,'subject_ids':subjectid}).then(function (response) {                      
            if(response.data.status == 1){
                $scope.allData = response.data.data.allData;
                $scope.category_purchase_status = response.data.data.category_purchase_status;
                if($scope.category_purchase_status == 1)
                {
                    angular.forEach($scope.allData, function(values, keys) {
                        if(values.slug == slug){
                            var index           = keys+1;
                            var string          = slug+"/"+content_type+"/"+index;
                            $scope.urlEncoded   = btoa(string);
                            $scope.subjectid    = btoa(subjectid);
                            $scope.check        = true;
                            window.location.replace("content-detail/"+$scope.urlEncoded+"/"+$scope.subjectid); 
                        } 
                    });
                    if($scope.check != true) {
                        if(content_type == 2) {
                            $scope.error = "This quiz no longer exists";
                            simpleAlert('error', '', $scope.error);
                        } else if(content_type == 1) {
                            $scope.error = "This video no longer exists";
                            simpleAlert('error', '', $scope.error);
                        }
                    }
                } else {
                    if($scope.category_purchase_status == 0) {
                        angular.forEach($scope.allData, function(values, keys) {
                            if(values.slug == slug && values.content_status == 2){
                                var index           = keys+1;
                                var string          = slug+"/"+content_type+"/"+index;
                                $scope.urlEncoded   = btoa(string);
                                $scope.subjectid    = btoa(subjectid);
                                $scope.checkIt      = true;
                                window.location.replace("content-detail/"+$scope.urlEncoded+"/"+$scope.subjectid);    
                            } 
                        });
                        if($scope.checkIt != true) {
                            $scope.error = "Please purchase this plan";
                            simpleAlert('error', '', $scope.error);
                        }
                    }
                }
            }else if(response.data.status == 0){
                if(response.data.code==2){
                    window.location.replace("ignite-login");
                }         
            }            
        });
    }

    $scope.ChangeTab = function(whichTab){
        if(whichTab == 'QuizzesAttemptedTab'){
            $('#'+whichTab).addClass('active');
            $('#VideosViewedTab').removeClass('active');
            $scope.QuizzesAttemptedTab = true;
            $scope.VideosViewedTab = false;
        } else {
            $('#'+whichTab).addClass('active');
            $('#QuizzesAttemptedTab').removeClass('active');
            $scope.VideosViewedTab = true;
            $scope.QuizzesAttemptedTab = false;
        }
    }
    $scope.ChangeTab('QuizzesAttemptedTab');
}]);