/*
 Author  : Karan Kantesariya (karan@creolestudios.com)
 Date    : 23th July 2020
 Name    : IgniteTwinkleDashbordController
 Purpose : All the functions for Subscriber load iframe login
 */
angular.module('sixcloudApp').controller('IgniteTwinkleDashbordController', ['$sce', 'ignitefactory', '$location', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', '$window', function($sce, ignitefactory, $location, $routeParams, $http, $scope, $timeout, $rootScope, $filter, $window) {
    /*  For Back Button for moving back to select-categories from ignite-categories  */
    $("#content").removeClass('hide');
    $rootScope.showiframe = false;
    $rootScope.Twinklemenu = true;
    $rootScope.twinkleClass = true;
    $rootScope.subheader = '';
    $rootScope.logoutTwinkle = true;
    $scope.url = $location.url();
    getLanguage = Cookies.get('language');
    $scope.current_domain = current_domain;
    $("body").removeClass("home-page");

    console.log('IgniteTwinkleDashbordController loaded');
    $scope.url = $location.url();

    ignitefactory.Getloginuserdatadistributor().then(function(response) {
        if (response.data.status == 1) {
            $rootScope.loginDistributorData = response.data.data;
            
            if($rootScope.loginDistributorData.twinkle_status == 0)
            {
                // confirmDialogueAlert('Your account are Inactive', '', 'error','','','Okay','','','','',inactive_status);
                window.location.replace("twinkle-subscription");    
            }
            else if($rootScope.loginDistributorData.twinkle_status == 3)
            {
                confirmDialogueAlert('Your account are Suspended', '', 'error','','','Okay','','','','',status);
                // window.location.replace("ignite-categories");   
            }
            else if($rootScope.loginDistributorData.twinkle_status == 1)
            {
                ignitefactory.TwinkleMysubscriptiondata().then(function(response) {
                    if(response.data.data.length > 0){
                        $scope.subscriptionData = response.data.data;
                        ignitefactory.GetUserTokenForTwinkle().then(function (response) {
                            if(response.data.status == 1){
                                $scope.twinkleRedirectToken = response.data.data;
                                $scope.user_md5str = $scope.twinkleRedirectToken.password;
                                $scope.email_address = window.encodeURIComponent($scope.twinkleRedirectToken.email_address);
                                if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test($scope.twinkleRedirectToken.email_address))
                                {
                                    $scope.url_iframe = "https://www.proprofs.com/classroom/?ID=2757837&cid=0&embed=1&AutoSubmit=Yes&w=450&username="+$scope.email_address+"&passkey="+$scope.user_md5str;
                                    $('#proprofs').attr('src',$scope.url_iframe);
                                }
                                else
                                {
                                    $scope.url_iframe = "https://www.proprofs.com/classroom/?ID=2757837&cid=0&embed=1&AutoSubmit=Yes&w=450&username="+$scope.twinkleRedirectToken.user_number+"&passkey="+$scope.user_md5str;
                                    $('#proprofs').attr('src',$scope.url_iframe);
                                }
                               
                            } else {
                                simpleAlert('error', '', 'Please try again');
                            }
                        });
                    }
                    else{
                        $scope.subscriptionData = [];
                        window.location.replace("twinkle-subscription");
                    }
                });
            }
            
        } else {
            $rootScope.loginDistributorData = response.data;
        }
    });

    function status(){
        window.location.replace("ignite-categories");
    }
}]);
