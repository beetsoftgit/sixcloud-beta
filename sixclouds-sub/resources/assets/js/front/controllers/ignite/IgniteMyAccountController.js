/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 22nd Aug 2018
 Name    : IgniteMyAccountController
 Purpose : All the functions for my-account page
 */
angular.module('sixcloudApp').controller('IgniteMyAccountController', ['$sce', 'ignitefactory', 'mpfactory', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', '$location', function($sce, ignitefactory, mpfactory, $routeParams, $http, $scope, $timeout, $rootScope, $filter, $location) {
    /*urlObj = JSON.parse(localStorage.getItem('urlObj'));
    url_number_static = localStorage.getItem('url_number_static');
    url_number_static++;
    url = [{
        "url_number": url_number_static,
        "url": $location.url()
    }];
    last_url = urlObj.pop();
    localStorage.setItem('urlObj',JSON.stringify(urlObj));
    if(last_url[0].url_number !== url[0].url_number) {
        urlObj.push(last_url);
    }
    urlObj.push(url);
    localStorage.setItem("urlObj",JSON.stringify(urlObj));
    localStorage.setItem("url_number_static",url_number_static);*/
    $("#content").removeClass('hide');
    $rootScope.showiframe = false;
    $rootScope.Twinklemenu = false;
    $rootScope.ignitePage='my-account';
    $rootScope.Mathematics = "";
    $scope.language = Cookies.get('language');
    //Card Details defulat set..
    $scope.months = [1,2,3,4,5,6,7,8,9,10,11,12];
    $scope.payment = {};
    $scope.payment.exp_month = 0;
    $scope.payment.exp_year = 0;
    $scope.years = [];
    for (var i = new Date().getFullYear(); i < new Date().getFullYear()+20; i ++) {
        $scope.years.push(i);
    }

    $timeout(function(){
        $timeout(function(){
            getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
            changeLanguage(getLanguage);
            $('#chk_lang').change(function() {
                getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
                changeLanguage(getLanguage);
            });
        },500);
        function changeLanguage(lang) {
            if (lang == "chi") {
                $timeout(function(){
                    $scope.english = false;
                },500);
            } else {
                $timeout(function(){
                    $scope.english = true;
                },500);
            }
        }
        /*if($rootScope.loginUserData.status == 0){
            window.location.replace("sixteen-login");
        }*/
        $('.ignite-menu').find('li').removeClass('active');
        // $('#all-referral').addClass('active');
        $rootScope.subheader = '';
        $rootScope.logoutTwinkle = false;
        ignitefactory.Getsubjects().then(function(response) {
            if(response.data.data.length > 0){
                $scope.subjects = response.data.data;

                $rootScope.isBUZZ=$scope.subjects.find(data=>{ return data.id==1 });
                $rootScope.isBUZZRU=$scope.subjects.find(data=>{ return data.id==2 });
                $rootScope.isMAZE=$scope.subjects.find(data=>{ return data.id==3 });
                $rootScope.isSMILE=$scope.subjects.find(data=>{ return data.id==4 });
                $rootScope.isSMILESG=$scope.subjects.find(data=>{ return data.id==7 });

                $rootScope.isMAZE=$rootScope.isMAZE?true:false;
                $rootScope.isSMILE=$rootScope.isSMILE?true:false;
                $rootScope.isSMILESG=$rootScope.isSMILESG?true:false;
                $rootScope.isBUZZ=$rootScope.isBUZZ?true:false;
                $rootScope.isBUZZRU=$rootScope.isBUZZRU?true:false;
            }
        });

        $scope.distributorData = $rootScope.loginDistributorData;

        $scope.fieldsInitialise = function(){
            if($scope.distributorData.userType == 'ignite_user'){
                $scope.distributorData.country_id = $scope.distributorData.country_id;
                $scope.distributorData.state_id   = $scope.distributorData.provience_id ;
                $scope.distributorData.city_id    = $scope.distributorData.city_id ;
                $scope.phonecode                  = $scope.distributorData.phonecode;
                if($scope.distributorData.country_id=='' && ($scope.distributorData.provience_id=='' || $scope.distributorData.city_id==''))/*||$scope.distributorData.dob == null*/{
                    simpleAlert('error', '', 'You need to first fill all your necessary details without which you will not be able to access anything.');
                }
                ignitefactory.Getzonelanguage().then(function(response){
                    if(response.data.status==1)
                    {
                        $scope.zoneLanguages = response.data.data.languageForZone;
                        $scope.seeVideosFor = response.data.data.see_videos_for;
                    } else{
                        if(response.data.code==2)
                            window.location.replace('ignite-login');
                    }
                });
            }
            if($scope.distributorData.userType == 'distributor'){
                $scope.distributorData.country_id = $scope.distributorData.country_id ;
                $scope.distributorData.state_id   = $scope.distributorData.state_id ;
                $scope.distributorData.city_id    = $scope.distributorData.city_id ;
                $scope.phonecode                  = $scope.distributorData.phonecode;
            }
            if($scope.distributorData.userType == 'distributor_team_member'){
                $scope.distributorData.country_id = $scope.distributorData.country_id ;
                $scope.distributorData.state_id   = $scope.distributorData.state_id ;
                $scope.distributorData.city_id    = $scope.distributorData.city_id ;
                $scope.phonecode                  = $scope.distributorData.phonecode;
            }
        }
        $scope.fieldsInitialise();
        if($scope.distributorData.country_id != '' || $scope.distributorData.city_id != '' || $scope.distributorData.state_id != ''){

                $scope.getstates = function(id){
                mpfactory.Getstatesforcountry({'id': id}).then(function(response){
                    if(response.data.status==1)
                    {
                        $scope.states = response.data.data;
                    }
                    });
                }
                $scope.getstates($scope.distributorData.country_id);
                $scope.getcities = function(id){
                    mpfactory.Getcitiesforstate({'id': id}).then(function(response){
                        if(response.data.status==1)
                        {
                            $scope.cities = response.data.data;
                        }
                    });
                }
                $scope.getcities($scope.distributorData.state_id);

                mpfactory.Getcountry().then(function(response){
                    if(response.data.status==1)
                    {
                        $scope.countries = response.data.data.country;
                        /*angular.forEach($scope.countries, function(value, key) {
                            if(value.id==$scope.distributorData.country_id)
                                $scope.phonecode= value.phonecode;
                        });*/
                    }      
                });
                // $scope.getcountrystates = function(id){
                $('#country_dropdown').on('change',function(){
                    var country    = $('#country_dropdown').val();
                    if(country!=''&&country!=undefined){
                        country = JSON.parse(country);
                        $scope.phonecode = country['phonecode'];
                        country = country['id'];
                        if(country!='') {
                            $scope.getstates(country);
                        } else {
                            $scope.states = undefined;
                            $scope.cities = undefined;
                        }
                        $('#state_dropdown').val('');
                        $('#city_dropdown').val('');
                    } else {
                        $('#state_dropdown').val('');
                        $('#city_dropdown').val('');
                    }
                    /*if(country!=''){
                        $scope.getstates(country);
                    } else {
                        $scope.states = undefined;
                        $scope.cities = undefined;
                    }
                    $('#state_dropdown').val('');
                    $('#city_dropdown').val('');*/
                });
                // $scope.getstatecities = function(id){
                $('#state_dropdown').on('change',function(){
                    var state = $('#state_dropdown').val();
                    if(state!=''){
                        $scope.getcities(state);
                    } else {
                        $scope.cities = undefined;
                    }
                    $('#city_dropdown').val('');
                });
        }
        $timeout(function(){
            $('#datepicker').datetimepicker({
                pickTime: false,
                format: 'DD/MM/YYYY',
                maxDate: new Date(),
            });
        },1000);
        function readURL(input) {
          if (input.files && input.files[0]&&input.files.length>0) {
            var reader = new FileReader();
            reader.onload = function(e) {
              $('#previewImage').css('background-image','url('+e.target.result+')');
            }
            reader.readAsDataURL(input.files[0]);
          } else {
            $('#previewImage').css('background-image','url(public/uploads/distributor_profile/'+$scope.distributorData.image+')');
            $('#imagePic').val('');
          }
        }

        $("#imagePic").change(function() {
            readURL(this);
        });
    },1500);
    

    

    $scope.Updatedistributorninfo = function(){
        // if($scope.distributorData.userType == 'ignite_user'||$scope.distributorData.userType == 'distributor'){
            $scope.country_id = $('#country_dropdown').val();
            if($scope.country_id!=''&&$scope.country_id!=undefined){
                $scope.country_id = JSON.parse($scope.country_id);
                $scope.country_id = $scope.country_id['id'];
            }else {
                $scope.country_id = undefined;
            }
            $scope.state_id = $('#state_dropdown').val();
            $scope.city_id = $('#city_dropdown').val();
        // }
            $scope.language = Cookies.get('language');
            if($scope.language=='en'){
                var email_required        = 'Email Address is Required';
                var country_required      = 'Country is Required';
                /*var state_required        = 'Province/State is Required';*/
                /*var city_required         = 'City is Required';*/
                /*var contact_required      = 'Phone Number is required';*/
                var first_name_required   = 'First Name is required';
                var last_name_required    = 'Last Name is required';
                /*var dob_required          = 'Date Of birth is required';*/
                var dob_age_validation    = 'You should be of 18 years or above to proceed';
                var company_name_required = 'Company Name is required';
                /*var phone_code_required   = 'Phonecode is required';*/
                var save_btn              = 'Save';
            } else {
                var email_required        = '请添加 “电邮“';
                var country_required      = '请添加 “国家”';
                /*var state_required        = '请添加 “省/州“';*/
                /*var city_required         = '请添加 “市“';*/
                /*var contact_required      = '请添加 “电话号“';*/
                var first_name_required   = '请添加 “名字“';
                var last_name_required    = '请添加 “姓“';
                /*var dob_required          = '请添加 “出生日期“';*/
                var dob_age_validation    = '你应该是 18 岁或以上的继续';
                var company_name_required = '请添加 “公司名称“';
                /*var phone_code_required   = '请添加 “电话代码“';*/
                var save_btn              = '储存';
            }
        if($scope.distributorData.email_address==undefined||$scope.distributorData.email_address==''||(($scope.country_id==undefined||$scope.country_id=='')))/*||$scope.distributorData.contact==undefined||$scope.distributorData.contact==''||$scope.state_id==undefined||$scope.state_id=='' ||$scope.city_id==undefined||$scope.city_id=='')*/{
            if($scope.distributorData.email_address==undefined||$scope.distributorData.email_address==''){
                simpleAlert('error','',email_required);
                return; 
            }
            if($scope.country_id==undefined||$scope.country_id==''){
                simpleAlert('error','',country_required);
                return; 
            }
            /*if($scope.state_id==undefined||$scope.state_id==''){
                simpleAlert('error','',state_required);
                return; 
            }
            if($scope.city_id==undefined||$scope.city_id==''){
                simpleAlert('error','',city_required);
                return;
            }
            if($scope.distributorData.contact==undefined||$scope.distributorData.contact==''){
                simpleAlert('error','',contact_required);
                return;
            }*/
        } else {
            showLoader('.submit_call');
            var updateInfo = new FormData($("#updateForm")[0]);
            updateInfo.append('country_id',$scope.country_id);
            updateInfo.append('language',Cookies.get('language'));
            if($scope.distributorData.userType == 'ignite_user'){
                if($scope.distributorData.first_name==undefined||$scope.distributorData.first_name==''){
                    simpleAlert('error','',first_name_required);
                    hideLoader('.submit_call', save_btn);
                    return; 
                }
                if($scope.distributorData.last_name==undefined||$scope.distributorData.last_name==''){
                    simpleAlert('error','',last_name_required);
                    hideLoader('.submit_call', save_btn);
                    return; 
                }
                /*if($scope.distributorData.dob == 'null' || $scope.distributorData.dob==undefined){
                    simpleAlert('error','',dob_required);
                    hideLoader('.submit_call', save_btn);
                    return;
                }*/
                if(($scope.distributorData.dob == 'null' || $scope.distributorData.dob==undefined) && $scope.age<18){
                    simpleAlert('error','',dob_age_validation);
                    hideLoader('.submit_call', save_btn);
                    return;
                }
                updateInfo.append('updateFor','User');
                if($scope.state_id!=undefined || $scope.state_id!='') {
                    updateInfo.append('provience_id',$scope.state_id);
                }
                if($scope.seeVideosFor!=undefined || $scope.seeVideosFor!='') {
                    updateInfo.append('see_videos_for',$scope.seeVideosFor);
                }

                // updateInfo.append('phonecode',$scope.phonecode);
            }
            if($scope.distributorData.userType == 'distributor'){
                if($scope.distributorData.company_name==undefined||$scope.distributorData.company_name==''){
                    simpleAlert('error','',company_name_required);
                    hideLoader('.submit_call', save_btn);
                    return;
                }
                updateInfo.append('updateFor','Distributors');
                // updateInfo.append('phonecode',$scope.phonecode);
            }
            if($scope.distributorData.userType == 'distributor_team_member'){
                if($scope.distributorData.first_name==undefined||$scope.distributorData.first_name==''){
                    simpleAlert('error','',first_name_required);
                    hideLoader('.submit_call', save_btn);
                    return; 
                }
                if($scope.distributorData.last_name==undefined||$scope.distributorData.last_name==''){
                    simpleAlert('error','',last_name_required);
                    hideLoader('.submit_call', save_btn);
                    return; 
                }
                updateInfo.append('updateFor','DistributorsTeamMembers');
                updateInfo.append('distributor_id',$scope.distributorData.distributor_id);
                updateInfo.append('status',1);
                // updateInfo.append('phonecode',$scope.phonecode);
            }
            /*if($scope.phonecode==undefined||$scope.phonecode==''){
                simpleAlert('error','',phone_code_required);
                hideLoader('.submit_call', save_btn);
                return; 
            }*/
            if($scope.phonecode != undefined && $scope.phonecode!='') {
                updateInfo.append('phonecode',$scope.phonecode);
            }
            ignitefactory.Updatedistributorninfo(updateInfo).then(function(response){
                hideLoader('.submit_call', save_btn);
                if(response.data.status==1)
                {
                    window.location.reload();
                }
                if(response.data.status==0)
                {
                    if(response.data.code==2){
                        if($scope.distributorData.userType == 'distributor')
                            window.location.replace('distributor');
                        if($scope.distributorData.userType == 'distributor_team_member')
                            window.location.replace('distributor/team-login');
                        if($scope.distributorData.userType == 'ignite_user')
                            window.location.replace('ignite-login');
                    }
                    simpleAlert('error','',response.data.message);
                }
            });
        }
    }
    $scope.ChangeTab = function(whichTab,button){
        if(whichTab == 'UpdateProfileTab'){
            $('#UpdateProfileButton').addClass('active');
            $('#ChangePasswordButton').removeClass('active');
            $('#CardUpdateButton').removeClass('active');
            $scope.UpdateProfileTab = true;
            $scope.ChangePasswordTab = false;
            $scope.CardUpdateTab = false;
        }else if(whichTab == 'CardUpdateTab'){
            $('#CardUpdateButton').addClass('active');
            $('#ChangePasswordButton').removeClass('active');
            $('#UpdateProfileButton').removeClass('active');
            $scope.UpdateProfileTab = false;
            $scope.ChangePasswordTab = false;
            $scope.CardUpdateTab = true;
        } else {
            $('#ChangePasswordButton').addClass('active');
            $('#UpdateProfileButton').removeClass('active');
            $('#CardUpdateButton').removeClass('active');
            $scope.ChangePasswordTab = true;
            $scope.UpdateProfileTab = false;
            $scope.CardUpdateTab = false;
        }
    }
    $scope.imageClick = function(){
        $('#imagePic').click();
    }
    $scope.ChangeTab('UpdateProfileTab','UpdateProfileButton');
    $scope.calculateAge = function(dob) {
        var date = dob;
        var datearray = date.split("/");
        var newdate = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
        var ageDifMs = Date.now() - new Date(newdate);
        var ageDate = new Date(ageDifMs);
        $scope.age = Math.abs(ageDate.getUTCFullYear() - 1970);
    }
    $scope.Passwordupdate = function(){
        $scope.language = Cookies.get('language');
            if($scope.language=='en'){
                var current_password_required = 'Please enter Current Password';
                var new_password_required     = 'Please enter New Password';
                var confirm_password_required = 'Please retype Password';
                var new_confirm_same          = 'New Password and Confirm Password should be the same';
                var new_current_same          = 'New Password and Current Password should not be the same';
                var regex_validation          = 'Please use a password which comprises of 8 to 16 characters, with at least 1 uppercase letter, 1 lowercase letter and 1 number.';
                var update_btn                = 'Update';
            } else {
                var current_password_required = '请输入当前密码';
                var new_password_required     = '请输入新密码';
                var confirm_password_required = '请确认密码';
                var new_confirm_same          = '新密码和旧密码不应该相同。';
                var new_current_same          = '新密码和旧密码不应该相同。';
                var regex_validation          = '请使用由8到16个字符组成的密码，其中至少有1个大写字母、1个小写字母和1个数字。';
                var update_btn                = '确认';
            }
        if($scope.old_password == undefined||$scope.old_password == ''|| $scope.new_password == undefined||$scope.new_password == ''|| $scope.confirm_password == undefined||$scope.confirm_password == ''||$scope.new_password_expression_error == true||$scope.confirm_password != $scope.new_password){
            if($scope.old_password == undefined||$scope.old_password == ''){
                    simpleAlert('error', '', current_password_required);
                return;
            }
            if($scope.new_password == undefined||$scope.new_password == ''){
                if($scope.new_password == undefined||$scope.new_password == '')
                    simpleAlert('error', '', new_password_required);
                return;
            }
            if($scope.new_password_expression_error == true){
                simpleAlert('error', '', regex_validation);
                return;
            }
            if($scope.confirm_password == undefined||$scope.confirm_password == ''){
                simpleAlert('error', '', confirm_password_required);
                return;
            }
            if($scope.confirm_password != $scope.new_password){
                simpleAlert('error', '', new_confirm_same);
                return;
            }
        } else {
            if($scope.new_password == $scope.old_password) {
                simpleAlert('error', '', new_current_same);
                return;
            }
            if($scope.new_password.length >= 8){
                showLoader('.password_update');
                var password                 = {};
                password['old_password']     = $scope.old_password;
                password['new_password']     = $scope.new_password;
                password['confirm_password'] = $scope.confirm_password;
                if($scope.distributorData.userType == 'ignite_user'){
                    password['updateFor'] = 'User';
                }
                if($scope.distributorData.userType == 'distributor'){
                    password['updateFor'] = 'Distributors';
                }
                if($scope.distributorData.userType == 'distributor_team_member'){
                    password['updateFor'] = 'DistributorsTeamMembers';
                }
                password['language'] = $scope.language;
                ignitefactory.Updatedistributorpassword(password).then(function(response){
                    hideLoader('.password_update',update_btn);
                    if(response.data.status==1)
                    {
                        simpleAlert('success', '', response.data.message,false);
                        $timeout(function(){
                            window.location.reload();
                        },1000);
                    }
                    if(response.data.status==0)
                    {
                        if(response.data.code==2){
                            if($scope.distributorData.userType == 'distributor')
                                window.location.replace('distributor');
                            if($scope.distributorData.userType == 'distributor_team_member')
                                window.location.replace('distributor/team-login');
                            if($scope.distributorData.userType == 'ignite_user')
                                window.location.replace('ignite-login');
                        }
                        simpleAlert('error','',response.data.message);
                    }
                });
            } else {
                simpleAlert('error', '', regex_validation);
            }
        }
    }

    var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,16})");
    $scope.Checkexpression = function(password,which){
        if(strongRegex.test(password)) {
                $scope.new_password_expression_error = false;
        } else {
                $scope.new_password_expression_error = true;
        }
    }
    $scope.Checkcnfpassword = function(password){
        if($scope.confirm_password != $scope.new_password){
            $scope.confirm_password_error_no_match = true;
            $scope.confirm_password_error = false;
        } else {
            $scope.confirm_password_error_no_match = false;
            $scope.confirm_password_error = false;
        }
    }

    $scope.Makepayment = function(payment){
        if(payment==undefined){
            simpleAlert('error', '', $scope.language=='en'?'Please fill data':'Please fill data');
        }
        if(payment.card_number==undefined||payment.card_number==''||payment.exp_month==0||payment.exp_year==0||payment.cvv==undefined||payment.cvv==''){
            if(payment.card_number==undefined||payment.card_number==''){
                simpleAlert('error', '', $scope.language=='en'?'Enter card number':'输入卡号');
                return;
            }
            if(payment.exp_month==0){
                simpleAlert('error', '', $scope.language=='en'?'Select month':'选择月份');
                return;
            }
            if(payment.exp_year==0){
                simpleAlert('error', '', $scope.language=='en'?'Select year':'选择年份');
                return;
            }
            if(payment.cvv==undefined||payment.cvv==''){
                simpleAlert('error', '', $scope.language=='en'?'Enter CVV/CVC':'输入安全码');
                return;
            }
        } else {
            showLoader('.ignite_payment_data',$scope.language=='en'?'Please wait':'请稍候')
            ignitefactory.CardUpdate(payment).then(function(response) {
                if($scope.language=='en'){hideLoader('.ignite_payment_data','Make Payment');}else{hideLoader('.ignite_payment_data','添加到购物车');}
                if (response.data.status == 2) window.location.replace("ignite-login");
                if (response.data.status == 1) {
                    window.location.replace('my-account');
                } else {
                    if($scope.language=='en'){hideLoader('.ignite_payment_data','Make Payment');}else{hideLoader('.ignite_payment_data','添加到购物车');}
                    if(response.data.message!=undefined){
                        simpleAlert('error', '', response.data.message);
                    }else{
                        if($scope.changeLangs=='en'){
                            error = 'Oops, Something went wrong.'
                        }else if($scope.changeLangs=='chi'){
                            error = '哎呀,出了些问题。'
                        }else{
                            error = 'Ой,что-то пошло не так.'
                        }
                        simpleAlert('error', '', error);
                    }
                }
            });
        }
    }
}]);