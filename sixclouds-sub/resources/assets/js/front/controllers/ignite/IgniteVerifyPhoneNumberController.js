/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 10th Oct 2018
 Name    : IgniteVerifyPhoneNumberController
 Purpose : All the functions for distributor's team member phone verification
 */
angular.module('sixcloudApp').controller('IgniteVerifyPhoneNumberController', ['$sce', 'ignitefactory', '$location', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', '$interval', function($sce, ignitefactory, $location, $routeParams, $http, $scope, $timeout, $rootScope, $filter, $interval) {
    /*urlObj = JSON.parse(localStorage.getItem('urlObj'));
    url_number_static = localStorage.getItem('url_number_static');
    url_number_static++;
    url = [{
        "url_number": url_number_static,
        "url": $location.url()
    }];
    last_url = urlObj.pop();
    localStorage.setItem('urlObj',JSON.stringify(urlObj));
    if(last_url[0].url_number !== url[0].url_number) {
        urlObj.push(last_url);
    }
    urlObj.push(url);
    localStorage.setItem("urlObj",JSON.stringify(urlObj));
    localStorage.setItem("url_number_static",url_number_static);*/
    console.log('IgniteCategories Controller loaded');
    $scope.url = $location.url();

    $scope.Sendotp = function(country_code,phone_number){
        if(country_code==undefined||country_code==''||phone_number==undefined||phone_number==''){
            if(country_code==undefined||country_code==''){
                // simpleAlert('error', '', 'Enter country code');
                $scope.country_code_error = true;
            }
            if(phone_number==undefined||phone_number==''){
                // simpleAlert('error', '', 'Enter phone number');
                $scope.phone_number_error = true;
            }
        } else {
            showLoader('.send_otp');
            $('.disable_input').attr('disabled','disabled');
            var data = {};
            data.country_code = country_code;
            data.phone_number = phone_number;
            ignitefactory.Sendotp(data).then(function (response) {
                if(response.data.status == 1){
                    hideLoader('.send_otp',Cookies.get('language')=='en'?"Resend otp":"重发验证码");
                    $scope.otpSent = true;
                    var intervalTime = 60000;
                    $scope.resendText = 60;
                    $scope.appendresend = Cookies.get('language')=='en'?"Resend otp":"重发验证码";
                    $scope.appendresendDot = '...';
                    $scope.timer = $interval( function(){ 
                        $('.resend_otp').attr('disabled','disabled');
                        intervalTime--;
                        $scope.resendText--;
                        if($scope.resendText==0){
                            $interval.cancel($scope.timer);
                            $scope.resendText = Cookies.get('language')=='en'?"Resend otp":"重发验证码";
                            $scope.appendresend = '';
                            $scope.appendresendDot = '';
                            $('.resend_otp').removeAttr('disabled');
                        }
                    }, 1000);
                }else if(response.data.status == 0){
                    if(response.data.code==2){
                        window.location.replace("distributor/team-login");
                    }
                    $scope.otpSent = false;
                    hideLoader('.send_otp','Send otp');
                    simpleAlert('error', '', response.data.message);
                    $('.disable_input').removeAttr('disabled');
                }            
            });
        }
    }
    $scope.otp = {otp:''};
    $scope.Verifyotp = function(country_code,phone_number,otp){
        var data = {};
        data.country_code = country_code;
        data.phone_number = phone_number;
        data.otp = $scope.otp.otp;
        showLoader('.verify_otp');
        $('.disable_otp').attr('disabled','disabled');
        ignitefactory.Verifyotp(data).then(function (response) {
            if(response.data.status == 1){
                window.location.replace('my-referral');
            }else if(response.data.status == 0){
                hideLoader('.verify_otp','Verify otp');
                $('.disable_otp').removeAttr('disabled');
                $scope.otp = {otp:''};
                if(response.data.code==401){
                    simpleAlert('error', '', response.data.message);
                }
                if(response.data.code==2){
                    window.location.replace("distributor/team-login");
                }         
            }            
        });
    }


}]);