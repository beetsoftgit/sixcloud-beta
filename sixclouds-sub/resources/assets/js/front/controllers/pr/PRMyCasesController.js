/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 6th December 2017
 Name    : PRMyCasesController
 Purpose : All the functions for proof reading My cases page
 */
angular.module('sixcloudApp').controller('PRMyCasesController', ['$http', 'prfactory', '$scope', '$timeout', '$rootScope', '$filter', function ($http, prfactory, $scope, $timeout, $rootScope, $filter) {
    console.log('PR My Cases controller loaded');
    $timeout(function(){
        if($rootScope.loginUserData.status == 0){
            location.replace(BASEURL + 'proof-reading');
        }
    },1500);
    $scope.radio_standard_price = function(id, price,hours,package_id,package_type){
        $scope.package_selected_price = price;
        $scope.package_selected_hour = hours;
        $scope.package_selected_id = package_id;
        $scope.package_selected_type = package_type;
        $('.accept_file').css('display','inline-block');
        $("#tr_"+id).addClass('details');
        $("#tr_"+id).siblings('tr').removeClass('details');
    }
    $scope.radio_premium_price = function(id, price,hours,package_id,package_type){
        $scope.package_selected_price = price;
        $scope.package_selected_hour = hours;
        $scope.package_selected_id = package_id;
        $scope.package_selected_type = package_type;
        $('.accept_file').css('display','inline-block');
        $("#tr_"+id).addClass('details');
        $("#tr_"+id).siblings('tr').removeClass('details');
    }
    $scope.Casesfunction = function(){
        prfactory.Getcases().then(function(response){
            if(response.data.status==1)
            {
              $scope.cases = response.data.data;
              $scope.myCaseLength = $scope.cases.length;
            }
        });   
    }
    $scope.Casesfunction();
    $(document).ready(function()
    {
        $('.package-table').css('display','none');
        var uploadObj = $("#fileuploader").uploadFile({
                            url:"Fileupload",
                            multiple:false,
                            showDone:true,
                            maxFileCount:1,
                            dragDrop:true,
                            acceptFiles:".docx, .doc, .txt, .rtf",
                            dragDropStr: "<span class='sci-upload'><span class='path1'></span><span class='path2'></span></span> <span><b>DROP FILE(S) TO SUBMIT NEW CASES</b></span>",
                            fileName:"file_uploaded",
                            abortStr:"Cancel",
                            cancelStr:"Cancel",
                            onSuccess:function(files,data,xhr,pd)
                            {
                                if(data.status == 1) {
                                    $scope.$apply(function () {
                                        $scope.uploadedFileDetails = data.data;
                                        $('.ajax-file-upload-statusbar').css('display','none');
                                        $('.package-table').css('display','block');
                                        $('.accept_file').css('display','none');
                                        $('.file-upload-div').css('display','none');
                                    });
                                }
                                if(data.status == 0) {
                                    simpleAlert('error','Error',data.message);
                                    $('.ajax-file-upload-statusbar').css('display','none');
                                    $('.package-table').css('display','none');
                                    $('.accept_file').css('display','none');
                                    uploadObj.reset();
                                }
                            }
                        });
        $scope.reject_file = function(file){
            prfactory.Rejectcase({'file': file}).then(function (response) {
                if (response.data.status == 1)
                {
                    simpleAlert('success','Success',response.data.message);
                    $('.ajax-file-upload-statusbar').css('display','none');
                    $('.package-table').css('display','none');
                    $('.accept_file').css('display','none');
                    $('.file-upload-div').css('display','block');
                    uploadObj.reset(); 
                }
            });
        }
        $scope.accept_file = function(uploadedFileDetails){
            uploadedFileDetails['total_price'] = $scope.package_selected_price;
            uploadedFileDetails['package_selected_hour'] = $scope.package_selected_hour ;
            uploadedFileDetails['proof_reading_package_id'] = $scope.package_selected_id ;
            uploadedFileDetails['package_type'] = $scope.package_selected_type ;
            $(".accept_file").text(' ');
            $(".accept_file").append("<i class='fa fa-spinner fa-spin'></i> Uploading");
            $(".accept_file").attr("disabled", "disabled");
            $(".reject_file").attr("disabled", "disabled");
            prfactory.Addcase({'uploadedFileDetails': uploadedFileDetails}).then(function (response) {
                if (response.data.status == 1)
                {
                    simpleAlert('success','Success',response.data.message);
                    $('.ajax-file-upload-statusbar').css('display','none');
                    $('.package-table').css('display','none');
                    $('.file-upload-div').css('display','block');
                    $(".accept_file").text(' ');
                    $(".accept_file").append("<i class='sci-accept'></i> Accept");
                    $(".accept_file").removeAttr("disabled");
                    $(".reject_file").removeAttr("disabled");
                    uploadObj.reset();
                    $scope.Casesfunction();
                } else {
                    simpleAlert('error','Error',response.data.message);
                    $('.ajax-file-upload-statusbar').css('display','block');
                    $('.package-table').css('display','none');
                    $('.file-upload-div').css('display','block');
                    $(".accept_file").text(' ');
                    $(".accept_file").append("<i class='sci-accept'></i> Accept");
                    $(".accept_file").removeAttr("disabled");
                    $(".reject_file").removeAttr("disabled");
                    uploadObj.reset();
                }
            });
        }
        $scope.download = function(pr_case,index,whichFile=""){
            if(whichFile == 1){
                pr_case['whichFile'] = whichFile;
                pr_case['accessed_file'] = pr_case.accessment_2_file_name;
                $("#download-accessed_"+index).text(' ');
                $("#download-accessed_"+index).append("<i class='fa fa-spinner fa-spin'></i> Downloading");
                $("#download-accessed_"+index).attr("disabled", "disabled");
            }else{
                pr_case['whichFile'] = 2;
                $("#download_o_"+index).text(' ');
                $("#download_o_"+index).append("<i class='fa fa-spinner fa-spin'></i> Downloading");
                $("#download_o_"+index).attr("disabled", "disabled");
            }
            prfactory.Download(pr_case).then(function (response) {
                if (response.data.status == 1)
                {
                    var url = response.data.data.url;
                    if (whichFile == 1) {
                        $('#download_accessed_file_'+pr_case.id).attr('href',response.data.data.url);
                        $('#download_accessed_file_'+pr_case.id).attr('target','_self');
                        $('#download_accessed_file_'+pr_case.id)[0].click();
                    } else {
                        $('#download_file_'+pr_case.id).attr('href',response.data.data.url);
                        $('#download_file_'+pr_case.id).attr('target','_self');
                        $('#download_file_'+pr_case.id)[0].click();    
                    }
                    prfactory.Unlinkdownloadedfile({'file_name': response.data.data.file_name}).then(function (response) {
                        if (response.data.status == 1)
                        {
                            var file_unlinked_success = response.data.message;
                        }
                    });
                    $scope.Casesfunction();
                } else {
                    if(whichFile == 1){
                        pr_case['whichFile'] = whichFile;
                        $("#download-accessed_"+index).text(' ');
                        $("#download-accessed_"+index).append("<i class='sci-download text-white'></i> Download file");
                        $("#download-accessed_"+index).removeAttr("disabled");
                    }else{
                        pr_case['whichFile'] = 2;
                        $("#download_o_"+index).text(' ');
                        $("#download_o_"+index).append("<i class='sci-download text-white'></i> Original file");
                        $("#download_o_"+index).removeAttr("disabled");
                    }
                    simpleAlert('error','Error',response.data.message);
                }
            });
        }
    });
    $('.new-cases-details .radio-button input[type="radio"]').click(function(){
        if ($(this).is(':checked'))
        {
          $(this).parents('tr').addClass('details');
           $(this).parents('tr').siblings('tr').removeClass('details');
        }
      });
 
    $(".icon-dd i").click(function(){

        $('.my-cases ul li .media').removeClass('remove'); 
        $(this).parents('.media').addClass('remove');
        if($(this).parents('.media').siblings(".collapse").hasClass('in'))
        {
            $(this).parents('.media').removeClass('remove');
        }
    });
    $('[data-toggle="tooltip"]').tooltip(); 

    /*$('.cases-list .expend-btn').click(function() { 
        $(this).parent('div').parent('li').toggleClass('open');
         $(this).parent('div').next('.expand-content').slideToggle();
    });*/

    $scope.openCase = function(caseId){
        $('#case_'+caseId).parent('div').parent('li').toggleClass('open');
        $('#case_'+caseId).parent('div').next('.expand-content').slideToggle(); 
    }

}]);