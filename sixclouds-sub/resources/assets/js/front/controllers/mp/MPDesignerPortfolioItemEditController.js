/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 30th March 2018
 Name    : MPDesignerPortfolioItemEditController
 Purpose : All the functions for MP login page
 */
angular.module('sixcloudApp').controller('MPDesignerPortfolioItemEditController', ['$sce', 'mpfactory', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', function($sce, mpfactory, $routeParams, $http, $scope, $timeout, $rootScope, $filter) {
    console.log('MPDesignerPortfolioItemDetails controller loaded');
    $('body').css('background','#e3eefd');
    initSample();
    var getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
    changeLanguage(getLanguage);

    $('#chk_lang').change(function() {
        var getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
        changeLanguage(getLanguage);
    });
    function changeLanguage(lang){
        if(lang=="chi"){
          $timeout(function() {
            $scope.english = false;
            $scope.chinese = true;
            getLanguage='chi';
          }, 500);
        }    
        else{
          $timeout(function() {
            $scope.english = true;
            $scope.chinese = false;
            getLanguage='en';
          }, 500);
      }
    }

    if(getLanguage=='en'){
      confirmButtonText = 'Ok';
    }else{
      confirmButtonText = '好。';
    }
    $timeout(function(){
        if($rootScope.loginUserData.status == 0){
            window.location.replace("sixteen-login");
        }
        $('.designer-dashboard-menu').find('li').removeClass('active');
        $('#designer_portfolio').addClass('active');
    },500);
    $scope.mp_portfolio_id = $routeParams.slug;
    $scope.Portfoliofunction = function(){
        mpfactory.Portfolioinfo({'mp_portfolio_id':$scope.mp_portfolio_id}).then(function(response) {
            if(response.data.code == 2)
                window.location.replace("sixteen-login");
            if (response.data.status == 1)
            {
                $scope.portfolioInfo = response.data.data;
                $scope.availableSlot = $scope.portfolioInfo.attachments.length;
                $scope.availableSlot = 4-$scope.availableSlot;
                $timeout(function(){
                  CKEDITOR.instances.editor.setData($scope.portfolioInfo.upload_description);
                },500);     

                //$('#editor').html($scope.portfolioInfo.upload_description);
                //$scope.upload_description = $sce.trustAsHtml($scope.portfolioInfo.upload_description);
                $('.all-remove').remove();
                $timeout(function(){
                    for (var i = 0; i < $scope.availableSlot; i++) {
                      input = jQuery('<div class="col-xs-6 col-sm-4 all-remove"><div class="uploading-album-image new-class-inspiration-image file_attachment_'+i+'"><div class="fafa_'+i+'"><i class="fa fa-picture-o fafaremove_'+i+'" aria-hidden="true"></i></div><input type="file" id="attachments_'+i+'" name="attachments[]"></div></div>');
                      jQuery('.image-slot').append(input);

                      function readURL1(input) {
                        if (input.files && input.files[0]) {
                          var reader = new FileReader();
                          reader.onload = function(e) {
                            var extension = e.target.result.substring("data:image/".length, e.target.result.indexOf(";base64"))                             
                            if(extension == "mp4"){
                              var id_number = input.id;
                              var id_number = id_number.split('_').pop();                                                            
                              var html = "<div class='video-class-edit-portfolio'><video src='"+e.target.result+"' controls  style='width:100%; height:auto; margin:auto;'></video></div>";
                              $('.file_attachment_'+id_number).css('background-image', '');
                              $('.file_attachment_'+id_number).append(html);
                              $('.fafaremove_'+id_number).remove();
                              $('.fafa_'+id_number).remove();
                            }else{
                              var id_number = input.id;
                              var id_number = id_number.split('_').pop();                              
                              $('.file_attachment_'+id_number).css('background-image', 'url('+e.target.result+')');
                              $('.fafaremove_'+id_number).remove();  
                            }                            
                          }
                          reader.readAsDataURL(input.files[0]);
                        }
                      }
                      $("#attachments_"+i).change(function() {
                        readURL1(this);
                      });
                    }
                },500);
            }
            else {
              window.location.replace('seller-portfolio');
            }
        });
    }
    $scope.Portfoliofunction();

    mpfactory.Portfoliocategory({'language':getLanguage}).then(function(response) {
        if (response.data.status == 1)
        {
          $scope.categories = response.data.data;
        } else {
        }
    });
    function readURL(input) {
      if (input.files && input.files[0] && input.files.length>0) {
        var reader = new FileReader();
        reader.onload = function(e) {
          $('.cover-image').css('background-image', 'url('+e.target.result+')');
        }
        reader.readAsDataURL(input.files[0]);
      } else {
        $('.cover-image').css('background-image', 'url('+$scope.portfolioInfo.cover_image+')');
      }
    }
    $("#cover_image").change(function() {
      readURL(this);
    });
    function deleteredirect(){
      window.location.replace('seller-portfolio-item-details/'+$scope.mp_portfolio_id);
    }
    $scope.cancel = function(){
        confirmDialogueDifferentButton('','','',deleteredirect);
    }
    function deletefunction(){
      mpfactory.Deleteportfolioimage({'image_id':$scope.image_id,'project_id':$scope.project_id,'language':getLanguage}).then(function(response) {
          if(response.data.code == 2)
              window.location.replace("sixteen-login");
          if (response.data.status == 1)
          {
            $scope.Portfoliofunction();
            simpleAlert('success','',response.data.message);
          }
          else {
            simpleAlert('error','',response.data.message);
          }
      });
    }
    function deleteVideofunction(){
      mpfactory.DeleteportfolioVideo({'image_id':$scope.image_id,'project_id':$scope.project_id,'language':getLanguage}).then(function(response) {
          if(response.data.code == 2)
              window.location.replace("sixteen-login");
          if (response.data.status == 1)
          {
            $scope.Portfoliofunction();
            simpleAlert('success','',response.data.message);
          }
          else {
            simpleAlert('error','',response.data.message);
          }
      });
    }
    $scope.Deleteportfolioimage = function(image_id,project_id){
      $scope.image_id = image_id;
      $scope.project_id = project_id;
      confirmDialogue('','Image Deleted','',deletefunction);  
    }
    $scope.DeleteportfolioVideo = function(image_id,project_id){
      $scope.image_id = image_id;
      $scope.project_id = project_id;
      confirmDialogue('','Image Deleted','',deleteVideofunction);  
    }

    $scope.Saveportfolio = function(){
          var categoryClickCount = 0;
          $scope.upload_title = $scope.portfolioInfo.upload_title;
          $scope.upload_description = CKEDITOR.instances.editor.getData();
          if ($scope.upload_title == '' || $scope.upload_title == undefined) {
              if(getLanguage=='en'){
                text='The "Title" is required.';         
              }else{
                text='请输入“标题”。';          
              }
            simpleAlert('error', '', text);
            return;
          }
          if ($scope.upload_description == '' || $scope.upload_description == undefined) {
              if(getLanguage=='en'){
                  simpleAlert('error','','The "Description" is required.');
              } else {
                  simpleAlert('error','','“描述”是必需的。');
              }
              return;
          }
          $("input[name='skills[]']").each( function () {
              if($(this)[0].checked == true){
                  categoryClickCount++;
              }
          });  
          if(categoryClickCount==0){
              if(getLanguage=='en'){
                  simpleAlert('error','','Please Select Atleast 1 Category.');
              } else {
                  simpleAlert('error','','请选择至少1个类别。');
              }
              return;
          }
          showLoader('.edit-portfolio','Please wait');
          var newPortfolio = new FormData($("#formupload")[0]);
          newPortfolio.append("upload_description", CKEDITOR.instances.editor.getData());
          newPortfolio.append('portfolioInfo',JSON.stringify($scope.portfolioInfo));
          newPortfolio.append('language',getLanguage);
     
          mpfactory.Editportfolio(newPortfolio).then(function(response) {
              hideLoader('.edit-portfolio',hideLoaderText);
              if (response.data.status == 1)
              {
                window.location.replace('seller-portfolio');
              } else {
                simpleAlert('error','',response.data.message);
              }
          });
    }      
}]);