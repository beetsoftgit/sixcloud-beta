/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 30th March 2018
 Name    : MPDesignerPortfolioItemDetailsController
 Purpose : All the functions for MP login page
 */
angular.module('sixcloudApp').controller('MPDesignerInspirationBankItemDetailsController', ['$sce', 'mpfactory', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', function($sce, mpfactory, $routeParams, $http, $scope, $timeout, $rootScope, $filter) {
    console.log('MPDesignerPortfolioItemDetails controller loaded');
    $('body').css('background','#e3eefd');
    $timeout(function(){
      $('.designer-dashboard-menu').find('li').removeClass('active');
      $('#designer_myuploads').addClass('active');
    },500);
    $timeout(function(){
      var galleryTop = new Swiper('.gallery-top', {
            spaceBetween: 10,
            observer: true,
            navigation: {
              nextEl: '.swiper-button-next',
              prevEl: '.swiper-button-prev',
            },
      });
      var galleryThumbs = new Swiper('.gallery-thumbs', {
        spaceBetween: 10,
        centeredSlides: true,
        slidesPerView: 3,
        touchRatio: 0.2,
        slideToClickedSlide: true,
        observer: true,
        breakpoints: {
        767: {
            direction:'horizontal',
            slidesPerView: 1.5,
            spaceBetween: 15,
          },
        480: {
           direction:'horizontal',
            slidesPerView: 1.5,
            spaceBetween: 15,
          }
        }
      });
      galleryTop.controller.control = galleryThumbs;
      galleryThumbs.controller.control = galleryTop;
    },700);
    var getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
    changeLanguage(getLanguage);
    $('#chk_lang').change(function() {
      var getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
        changeLanguage(getLanguage);
    });    
    function changeLanguage(lang){ 
        if(lang=="chi"){
            $("#chi_portfolio_data").show();
            $("#en_portfolio_data").hide();
        }    
        else{
            $("#en_portfolio_data").show();
            $("#chi_portfolio_data").hide();
      }
    }
    $scope.getSlug         = $routeParams.slug;
    $scope.getSlugArray    = $routeParams.slug.split(':')
    $scope.inspiration_bank_details_title = $scope.getSlugArray[0];
    $scope.category_id  = $scope.getSlugArray[1];        
    $scope.InspirationBankfunction = function(){
      if(getLanguage=='en'){
        var confirmButtonText = 'Ok';
      }else{
        var confirmButtonText = '好。';
      }
        mpfactory.InspirationBankinfo({'slug':$scope.getSlug,'inspiration_bank_details_title':$scope.inspiration_bank_details_title, 'category_id' : $scope.category_id}).then(function(response) {                    
            if(response.data.code == 2)
                window.location.replace("sixteen-login");
            if (response.data.status == 1)
            {
              $scope.inspirationBankInfo = response.data.data;                           
            }
            else {
              confirmDialogueAlert("Error",response.data.message,"error",false,'#8972f0',confirmButtonText,'',true,'','',deleteredirect);
            }
        });
    }
    $scope.InspirationBankfunction();    
    function deleteredirect(){       
      window.location.replace('seller-inspiration-bank');
    }
    function deletefunction(){
      mpfactory.DeleteinspirationBank({'inspiration_bank_details_title':$scope.slug,'language':getLanguage}).then(function(response) {        
          if(response.data.code == 2)
              window.location.replace("sixteen-login");
          if (response.data.status == 1)
          {
              if(getLanguage=='en'){
                  text='Success';
              }else{
                  text='成功';
              }
              simpleAlert(text,'',response.data.message);
              // confirmDialogueAlert(text,response.data.message,"success",false,'#8972f0','Ok','',false,'','',deleteredirect);
              window.location.replace('seller-inspiration-bank');
          }
          else {
            simpleAlert('error','',response.data.message);
          }
      });
    }
    $scope.DeleteInspirationBankItem = function(slug,title){      
        $scope.slug = slug;
        if(getLanguage=='en'){
            text='Are You sure you want to delete '+ title +'?';
        }else{
            text='你确定你要删除 '+ title +'?';
        }
        confirmDialogue(text,'Inspiration Bank Item Deleted','',deletefunction);        
    }
}]);

