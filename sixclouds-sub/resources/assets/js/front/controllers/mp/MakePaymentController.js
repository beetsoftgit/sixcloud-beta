/*
 Author  : Nivedita (nivedita@creolestudios.com)
 Date    : 11th July 2018
 Name    : MakePaymentController
 Purpose : All functions mrelated to payments
 */
angular.module('sixcloudApp').controller('MakePaymentController', ['$sce', 'mpfactory', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', function($sce, mpfactory, $routeParams, $http, $scope, $timeout, $rootScope, $filter) {
    console.log('MakePaymentController Controller loaded');
    $('body').css('background', '#e3eefd');
    $timeout(function() {
        if ($rootScope.loginUserData.status == 0) {
            window.location.replace("sixteen-login");
        }
        $('.buyer-dashboard-menu').find('li').removeClass('active');
        $('#dashboard').addClass('active');
    }, 500);
    $scope.make_payment_id = $routeParams.slug.substring(0,$routeParams.slug.length - 1);
    credit_status= $routeParams.slug[$routeParams.slug.length -1];

    getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
    changeLanguage(getLanguage);
    $('#chk_lang').change(function() {
        getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
        changeLanguage(getLanguage);
    });
    
    function changeLanguage(lang){ 
        if(lang=="chi"){
            $scope.english = false;
            $scope.chinese = true;
            getLanguage=='chi';
        }    
        else{
            $scope.english = true;
            $scope.chinese = false;
            getLanguage=='en';
      }
    }
    //console.log(credit_status);
    if(credit_status!=1){
        
        $scope.payment_credit=0;

        $scope.Paymentinfodata = function() {
            
            mpfactory.Paymentinfodata({'payment_id': $scope.make_payment_id}).then(function(response) {
                
                if (response.data.code == 2) window.location.replace("sixteen-login");
                if (response.data.status == 1) {
                    $scope.paymentInfo = response.data.data[0];
                    $scope.domainInfo  = response.data.data.domain;
                    $scope.invoice  = response.data.data.invoice; 
                    
                } else {}
            });
        }
        $scope.Paymentinfodata();

        $timeout(function () {
            $('#create_and_pay').click();
        }, 2000);       
    }
    else{
        $scope.payment_credit=1;
    }

    $scope.returnFromCreditPayment=function(){

        if(getLanguage=='en'){
            var loaderText = 'Click Here';
            var waitText   = 'Please wait';
        }else{
            var loaderText = '点击这里';
            var waitText   = '请稍候';
        }

        showLoader('.click-here',waitText);
       
        mpfactory.Returnfromcreditpayment({'payment_id': $scope.make_payment_id}).then(function(response) {
                if(response.data.status==1){
                   $("#credit").text(response.data.data.credit);
                   hideLoader('.click-here',loaderText);
                   window.location.replace("buyer-dashboard");
                }
        });
    }
    
}]);