/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 16th May 2018
 Name    : MPSellerMyServicesController
 Purpose : All the functions for MP seller services page
 */
angular.module('sixcloudApp').controller('MPSellerMyServicesController', ['$sce', 'mpfactory', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', '$compile', function($sce, mpfactory, $routeParams, $http, $scope, $timeout, $rootScope, $filter, $compile) {
    console.log('MPSellerMyServices controller loaded');
    $scope.fileUploaded = false;
    CKEDITOR.replace( 'service_details' );

    $('body').css('background','#e3eefd');
    $timeout(function(){
        $('.designer-dashboard-menu').find('li').removeClass('active');
        $('#my_services').addClass('active');
    },500);
    
    (function(){
        $('.FlowupLabels').FlowupLabels({
            feature_onInitLoad: true,
            class_focused:      'focused',
            class_populated:    'populated'
          });
    })();

    // $(document).ready(function() {
        $scope.category_tab = function(categoryName="",categoryId){
            $('.tab-div').find('button').removeClass('active');
            $('#tab_'+categoryId).addClass('active');
            $scope.forCategory = categoryId;
            $('#'+$scope.lastService).remove();
            $scope.detailsForCategory($scope.forCategory);
        }

        $scope.detailsForCategory = function(id){
            mpfactory.Getexistingservices({'id':id}).then(function(response) {
                if (response.data.code == 2) window.location.replace("sixteen-login");
                if (response.data.status == 1) {
                    $('#services').show();
                    $('.add-new-services').show();
                    $scope.existingData = response.data.data;
                    //$scope.skill_name = $scope.existingData[0]['skill_detail'][0]['skill_name'];
                    $timeout(function(){
                        for (var i = 0; i < $scope.existingData[0]['service_details'].length; i++) {
                            var serviceId = $scope.existingData[0]['service_details'][i]['id'];
                            CKEDITOR.replace('service_details_'+serviceId);
                            if(CKEDITOR.instances['service_details_'+serviceId])
                                CKEDITOR.instances['service_details_'+serviceId].setData($scope.existingData[0]['service_details'][i]['service_description']);  
                        }  
                    },500); 
                    $scope.reinitializeTimeout();
                } else {
                    simpleAlert('error', '', response.data.message);
                }
            });
        }
    // });

    getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');

    changeLanguage(getLanguage);
    $('#chk_lang').change(function() {
        getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
        changeLanguage(getLanguage);
    });
    function changeLanguage(lang) {
        if (lang == "chi") {
            $timeout(function(){
                $scope.chinese = true;
                $scope.english = false;
                
            },500);
        } else {
            $timeout(function(){
                $scope.chinese = false;
                $scope.english = true;
            },500);
        }
    }
    $scope.current_domain = current_domain;
    
    $scope.titleRequired = "[['lbl_title_is_required'|translate]]";
    $scope.detailRequired = "[['lbl_details_is_required'|translate]]";
    $scope.revisionsRequired = "[['lbl_no_of_revisions_is_required'|translate]]";
    $scope.timelineRequired = "[['lbl_timeline_is_required'|translate]]";
    $scope.priceRequired = "[['lbl_price_is_required'|translate]]";
    $scope.imageRequired = "[['lbl_image_is_required'|translate]]";
    $scope.minimumTimeline = "[['lbl_min_timeline'|translate]]";
    $scope.minimumBudget = "[['lbl_min_budget'|translate]]";
    /*if($scope.current_domain=='net'){
        $scope.minimumBudget = "[['lbl_min_budget_singapore_dollar'|translate]]";   
    }
    else{
         $scope.minimumBudget = "[['lbl_min_budget_yen'|translate]]";
    }
    */


    $('#services').hide();
    $('.add-new-services').hide();
    mpfactory.Getserviceskills().then(function(response) {
        if (response.data.code == 2) window.location.replace("sixteen-login");
        if (response.data.status == 1) {
            $scope.selectedCategory = response.data.data.category_details;
            $scope.currentDomain = response.data.data.current_domain;
            if($scope.selectedCategory != 0){
                $timeout(function(){ 
                    $scope.category_tab($scope.selectedCategory[0].skill_name,$scope.selectedCategory[0].id);
                },500);
                $scope.categorySelectedCheckbox = response.data.data.category_selected;
            }
            $scope.skills = response.data.data.skills;
        } else {
            // simpleAlert('error', '', response.data.message);

        }
    });
    $scope.Addserviceskills = function(){
        var newSkill = new FormData($("#addSkillForm")[0]);
        newSkill.append('language',getLanguage);
        // showLoader('.add-skills');
        if(getLanguage=='en'){
                   showLoader(".add-skills",'Please wait');
            }else{
                   showLoader(".add-skills",'请稍候');
            }
        mpfactory.Addserviceskills(newSkill).then(function(response) {
            if(getLanguage=='en'){
                hideLoader(".add-skills",'Update');
            }else{
                hideLoader(".add-skills",'更新');
            }
            if (response.data.code == 2) window.location.replace("sixteen-login");
            if (response.data.status == 1) {
                $scope.selectedCategory = response.data.data;
                $timeout(function(){
                    $scope.category_tab($scope.selectedCategory[0].skill_name,$scope.selectedCategory[0].id);
                },500);
            } else {
                simpleAlert('error', '', response.data.message);
            }
        });
    }

    $(document).ready(function() {
        $(".only_number").on("keypress keyup blur", function(event) {
            //this.value = this.value.replace(/[^0-9\.]/g,'');
            $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
            $scope.project_budget_error = false;
        });
        $(".float_number").on("keypress keyup blur", function(event) {
            $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
            $scope.project_timeline_error = false;
        });
    });

    function readURL(input,id) {
        // console.log('hello');
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
           var newpreviewid = id;
          $('#'+newpreviewid).css('background-image','url('+e.target.result+')');
          $('#'+newpreviewid).addClass('service_images');
          var addRemove = id;
          addRemove = addRemove.replace("fileClick_",'remove_img_');
          console.log(addRemove);
          $('#'+addRemove+'').removeClass('hide');
          $scope.fileUploaded = true;
        }
        reader.readAsDataURL(input.files[0]);
      }
    }

    function changeImg(fileid,id) {
        $('#'+fileid).change(function() {
            readURL(this,id);
        });
    }

    $(".a_img").click(function() {
        console.log('hello');
        var id = $(this).attr('id');
        var previewId = id;
        id = id.replace("fileClick_",'');
        $timeout(function(){
            $('#'+id).click();
            changeImg(id,previewId);
        },300);
    });

    // $scope.fileClick = function(id){
    //     $('#'+id).click();
    // }

    $(function(){
       $('.remove_image').click(function(e){
            var id = $(this).attr('id');
            var previewId = $(this).attr('id');
            var addRemove = id;
            var id = id.replace("remove_img_",'');
            $('#'+id).val('');
            previewId = previewId.replace("remove_img_",'fileClick_');
            $('#'+previewId).css('background-image','url(resources/assets/images/add.png)');
            $('#'+addRemove+'').addClass('hide');
            $scope.fileUploaded = false;
            return false;
       });
    });

    $scope.uniqueId = 1;
    $scope.extraid =1;

    $scope.CloneService = function(){

        /*destroying ckeditor*/
        CKEDITOR.instances['service_details'].destroy();
        
        for (var i = 1; i <= $scope.uniqueId; i++) {
            if(CKEDITOR.instances['service_details_'+i])
                CKEDITOR.instances['service_details_'+i].destroy();
        }
        /*destroying ckeditor*/

        var copy = $("#services_original_"+$scope.uniqueId).clone(true);        
        $("#services_original_"+$scope.uniqueId+" :input").prop("disabled", false);
        $("#services_original_"+$scope.uniqueId).find('input:text').val('');
        $("#services_original_"+$scope.uniqueId).find('textarea').attr('id', 'service_details_'+$scope.uniqueId);
        $("#services_original_"+$scope.uniqueId).find('input:file').removeClass('hide');
        var $buttons = $("#services_original_"+$scope.uniqueId).find('.remove');
        $buttons.removeClass('hide');
        $timeout(function(){
            $("#services_original_"+$scope.uniqueId).find('input:file').addClass('hide');
        },500);
        $("#fileClick_image1_"+$scope.uniqueId).css('background-image','url(resources/assets/images/add.png)');
        $("#fileClick_image2_"+$scope.uniqueId).css('background-image','url(resources/assets/images/add.png)');
        $("#fileClick_image3_"+$scope.uniqueId).css('background-image','url(resources/assets/images/add.png)');

        $("#remove_img_image1_"+$scope.uniqueId).addClass('hide');
        $("#remove_img_image2_"+$scope.uniqueId).addClass('hide');
        $("#remove_img_image3_"+$scope.uniqueId).addClass('hide');

        $("#services_original_"+$scope.uniqueId).find('textarea').val('');
        $("#services_original_"+$scope.uniqueId).find("input:hidden").remove();
        $("#div_service_details_"+$scope.uniqueId).find("i").addClass('hide');
        $('#services').append(copy);

        $("#services_original_"+$scope.uniqueId+" input[name=service_title]").attr("id",'service_title_'+$scope.uniqueId);
        /*reinitializing ckeditor*/
        for (var j = 1; j <= $scope.uniqueId; j++) {
            CKEDITOR.replace('service_details_'+j);
        }
        CKEDITOR.replace('service_details');
        /*reinitializing ckeditor*/
        for (var i in CKEDITOR.instances) {
            CKEDITOR.instances[i].on('change', function() {                            
                var data  = CKEDITOR.instances[i].getData();
                var ckeId = $(this).parent().prevObject[0].name;
                var ckeId = $('#'+ckeId).parent().attr('id');
                if( data == "" || data == undefined){
                    // $(".success-call-desc").css("display","none");
                    // $(".class-for-description").css("display","block");
                    // $(".class-for-description").removeClass("hide");
                    // $(".class-for-description").css("color","#ed7154");
                    $('#'+ckeId).find(".success-call-desc").css("display","none");
                    $('#'+ckeId).find(".class-for-description").css("display","block");
                    $('#'+ckeId).find(".class-for-description").removeClass("hide");
                    $('#'+ckeId).find(".class-for-description").css("color","#ed7154");
                }else{
                    // $(".added-from-publish").css("display","none");
                    // $(".class-for-description").css("display","none");
                    // $(".success-call-desc").css("display","block");
                    // $(".success-call-desc").removeClass("hide");
                    $('#'+ckeId).find(".added-from-publish").css("display","none");
                    $('#'+ckeId).find(".class-for-description").css("display","none");
                    $('#'+ckeId).find(".success-call-desc").css("display","block");
                    $('#'+ckeId).find(".success-call-desc").removeClass("hide");
                }
            });
        }

        $scope.uniqueId++;
        $('html, body').animate({
            scrollTop: $("#services").offset().top
        }, 500);
    }

    $(function(){
       $('.remove').click(function(){
            var id = $(this).attr('id');
            var id = id.replace("delete_",'');
            $("#"+id).remove();
            $scope.uniqueId--;
       });
    });

    $(function(){
       $('.add-extra').click(function(){
            var id = $(this).attr('id');
            var id = id.replace("addextra_",'');
            id = id.split('_').pop();
            var copy = $("#clone_revisions").clone(true);
            copy.removeClass('hide');
            $('#clone_revisions_extra_'+id).append(copy);
            $('#clone_revisions_extra_'+id).children().last().find('input:text').val('');
            $scope.extraid++;
       });
    });

    $(function(){
       $('.extra_remove').click(function(){
            $(this).closest("#clone_revisions").remove();
       });
    });

    $(function(){
        $('.save_as_draft').click(function(){
            var id = $(this).attr('id');
            var id = id.replace("draft_",'');
            var num = id.split('_').pop();
            $scope["serviceTitleError"+num] = false;            
            $scope["serviceDetailError"+num] = false;
            $scope["serviceRevisionError"+num] = false;
            $scope["serviceDeliveryError"+num] = false;
            $scope["servicePriceError"+num] = false;
            $scope["serviceExtraRevisionError"+num] = false;
            $scope["serviceExtraDeliveryError"+num] = false;
            $scope["serviceExtraPriceError"+num] = false;
            var serviceTitle = $("#service_title_"+num).val();                        
            // if(CKEDITOR.instances['service_details_'+num]){
            //     var serviceDetails = CKEDITOR.instances['service_details_'+num].getData();
            // }else{
            //     var serviceDetails = CKEDITOR.instances['service_details'].getData();                             
            // }
            if(num > 1){
                var  newNum = num - 1;                                
                var serviceDetails = CKEDITOR.instances['service_details_'+newNum].getData();
            }else{
                var serviceDetails = CKEDITOR.instances['service_details'].getData();                             
            }  
            
            var serviceRevisions = $("#service_revisions_"+num).val();
            var serviceDelivery = $("#service_delivery_time_"+num).val();
            var servicePrice = $("#service_price_"+num).val();
            var serviceExtraRevision = $("#service_revisions_extra_"+num).val();
            var serviceExtraDelivery = $("#service_delivery_time_extra_"+num).val();
            var serviceExtraPrice = $("#service_price_extra_"+num).val();            
            if(serviceTitle == "" || serviceTitle == null || serviceDetails == "" || serviceDetails == null || serviceRevisions == "" || serviceRevisions == null || serviceDelivery == "" || serviceDelivery == null || servicePrice == "" || servicePrice == null || $scope.fileUploaded === false || (serviceExtraDelivery != "" &&  serviceExtraDelivery <= 3) || (serviceExtraPrice != "" &&  serviceExtraPrice <= 0)){
                if(serviceTitle == "" || serviceTitle == null){
                    $scope["serviceTitleError"+num] = true;          
                        var html='<i class="fa fa-exclamation-circle out-icon new-error-class" style="display:block; color: #ed7154;" title="'+$scope.titleRequired+'">';
                        var el = document.getElementById('service_title_'+num);
                        angular.element(el).after( $compile(html)($scope) )                    
                        $scope.$apply();                                                        
                } 
                if(serviceDetails == "" || serviceDetails == null){                                  
                    $scope["serviceDetailError"+num] = true;
                    var html1='<i class="fa fa-exclamation-circle class-for-description out-icon new-class-icon new-error-class added-from-publish"  title="'+$scope.detailRequired+'">',
                        el1 = document.getElementById('div_service_details_'+num);
                        angular.element(el1).append( $compile(html1)($scope) )                    
                        $scope.$apply();                                     
                }
                if(serviceRevisions == "" || serviceRevisions == null){
                    $scope["serviceRevisionError"+num] = true;
                    var html='<i class="fa fa-exclamation-circle out-icon new-error-class" style="display:block; color: #ed7154;" title="'+$scope.revisionsRequired+'">',
                        el = document.getElementById('service_revisions_'+num);
                        angular.element(el).after( $compile(html)($scope) )                    
                        $scope.$apply();                                                        
                } 
                if(serviceDelivery == "" || serviceDelivery == null || serviceDelivery <= 2){
                    $scope["serviceDeliveryError"+num] = true;
                    if( serviceDelivery != "" && serviceDelivery <= 2){
                        var html='<i class="fa fa-exclamation-circle out-icon new-error-class" style="display:block; color: #ed7154;" title="'+$scope.minimumTimeline +'">';
                    }else{
                        var html='<i class="fa fa-exclamation-circle out-icon new-error-class" style="display:block; color: #ed7154;" title="'+$scope.timelineRequired+'">';    
                    }                    
                    var el = document.getElementById('service_delivery_time_'+num);
                        angular.element(el).after( $compile(html)($scope) )                    
                        $scope.$apply();                                                        
                } 
                if(servicePrice == "" || servicePrice == null || servicePrice <= 0){
                    $scope["servicePriceError"+num] = true;
                    if( serviceDelivery != "" && serviceDelivery <= 0 ){
                        var html='<i class="fa fa-exclamation-circle out-icon new-error-class" style="display:block; color: #ed7154;" title="'+$scope.minimumBudget+'">';
                    }else{
                        var html='<i class="fa fa-exclamation-circle out-icon new-error-class" style="display:block; color: #ed7154;" title="'+$scope.priceRequired +'">';
                    } 
                    
                    var el = document.getElementById('service_price_'+num);
                        angular.element(el).after( $compile(html)($scope) )                    
                        $scope.$apply();                                                        
                }                 
                if( serviceExtraDelivery != "" && serviceExtraDelivery <= 2){
                    $scope["serviceExtraDeliveryError"+num] = true;
                    var html='<i class="fa fa-exclamation-circle out-icon new-error-class" style="display:block; color: #ed7154;" title="'+$scope.minimumTimeline +'">';    
                    var el = document.getElementById('service_delivery_time_extra_'+num);
                    angular.element(el).after( $compile(html)($scope) )                    
                    $scope.$apply();                                                                            
                }
                // if(serviceExtraPrice != "" && serviceExtraPrice < 31){
                //     $scope["serviceExtraPriceError"+num] = true;
                //     var html='<i class="fa fa-exclamation-circle out-icon new-error-class" style="display:block; color: #ed7154;" title="'+$scope.minimumBudget+'">';
                //     var el = document.getElementById('service_price_extra_'+num);
                //     angular.element(el).after( $compile(html)($scope) )                    
                //     $scope.$apply();                                                                          
                // }   
                if($scope.fileUploaded === false){                                                            
                    $("#errorDisplayClassImages").remove(".errorDisplayClass");
                    var html='<li class="errorDisplayClass" id="errorDisplayClassImages"><i class="fa fa-exclamation-circle out-icon new-error-class" style="display:block; color: #ed7154;" title="'+$scope.imageRequired+'"></li>';
                    var el = document.getElementById('imageDivID'+num);
                    angular.element(el).append( $compile(html)($scope) )                    
                    $scope.$apply();
                }                               
                return false;
            }
            var newService = new FormData($("#"+id)[0]);   
            newService.append('status','3');
            newService.append('mp_seller_service_skill_id',$scope.forCategory);
            newService.append('language',getLanguage);
            // if(CKEDITOR.instances['service_details_'+num])
            //     newService.append('service_details', CKEDITOR.instances['service_details_'+num].getData());
            // else
            //     newService.append('service_details', CKEDITOR.instances['service_details'].getData());
            if(num > 1){
                    var  newNum = num - 1;                        
                    newService.append('service_details',CKEDITOR.instances['service_details_'+newNum].getData());
                }else{
                    newService.append('service_details',CKEDITOR.instances['service_details'].getData());
                }
            if(getLanguage=='en'){showLoader('.draft_'+num,'Please wait');}else{showLoader('.draft_'+num,'请稍候');}
            Disable('.publish_'+num);
            Disable('.delete_'+num);
                mpfactory.Addservices(newService).then(function(response) {
                    if(getLanguage=='en'){hideLoader('.draft_'+num,'Save as Draft');}else{hideLoader('.draft_'+num,'保存为草稿');} 
                    Enable('.publish_'+num);
                    Enable('.delete_'+num);
                    if (response.data.code == 2) window.location.replace("sixteen-login");
                    if (response.data.status == 1) {
                        $scope.lastService = id;
                        $timeout(function(){
                            $scope.CloneService();
                        },500);
                        $('<input>').attr({type: 'hidden',id: 'draftFormID',name: 'draftFormID', value:''+response.data.data.id+''}).appendTo('form');
                        simpleAlert('success','',response.data.message);
                        $timeout(function(){ 
                            location.reload();
                        },500);
                    } else {
                        simpleAlert('error', '', response.data.message);
                    }
                });
            
        });
    });

    $timeout(function(){      
        // $(".form-control").on("change keyup paste mouseup keypress blur",function(){  
        $(".form-control").on("keyup paste mouseup blur",function(){
            var id = $(this).attr('id');
            var splittedData = id.split("_");
            var num = id.split('_').pop();                                
            var data = $(this).val();
            if(splittedData[1] == "title"){                  
                if(data == ""){ 
                    $scope["serviceTitleError"+num] = true;
                     $(this).parent().addClass("has-error");
                     $(this).parent().removeClass("has-success");
                     var html='<i class="fa fa-exclamation-circle out-icon new-error-class" style="display:block; color: #ed7154;" title="'+$scope.titleRequired+'">';
                        var el = document.getElementById('service_title_'+num);
                        angular.element(el).after( $compile(html)($scope) )                    
                        $scope.$apply(); 
                }else{
                    $scope["serviceTitleError"+num] = false;
                    $(this).parent().removeClass("has-error");
                    $(this).parent().addClass("has-success");
                    $(this).next(".new-error-class").remove();        
                }                
            }else if( splittedData[1] == "revisions"){   
                if(splittedData[2] == "extra"){
                    if(data == "" ){ 
                        $scope["serviceExtraRevisionError"+num] = true;
                         $(this).parent().addClass("has-error");
                         $(this).parent().removeClass("has-success");
                         $(this).next("i").remove();        
                         var html='<i class="fa fa-exclamation-circle out-icon new-error-class" style="display:block; color: #ed7154;" title="'+$scope.revisionsRequired +'">',
                            el = document.getElementById('service_revisions_extra_'+num);
                            angular.element(el).after( $compile(html)($scope) )                    
                            $scope.$apply(); 
                    }else{
                        $scope["serviceExtraRevisionError"+num] = false;
                        $(this).parent().removeClass("has-error");
                        $(this).parent().addClass("has-success");
                        $(this).next(".new-error-class").remove();        
                    }
                } else {
                    if(data == ""){ 
                        $scope["serviceRevisionError"+num] = true;
                         $(this).parent().addClass("has-error");
                         $(this).parent().removeClass("has-success");
                         var html='<i class="fa fa-exclamation-circle out-icon new-error-class" style="display:block; color: #ed7154;" title="'+$scope.revisionsRequired+'">',
                            el = document.getElementById('service_revisions_'+num);
                            angular.element(el).after( $compile(html)($scope) )                    
                            $scope.$apply(); 
                    }else{
                        $scope["serviceRevisionError"+num] = false;
                        $(this).parent().removeClass("has-error");
                        $(this).parent().addClass("has-success");
                        $(this).next(".new-error-class").remove();        
                    }
                }
            }else if( splittedData[1] == "delivery"){  
                if(splittedData[3] == "extra"){
                    if(data <= 2 ){ 
                        $scope["serviceExtraDeliveryError"+num] = true;
                         $(this).parent().addClass("has-error");
                         $(this).parent().removeClass("has-success");
                         $(this).next("i").remove();        
                         var html='<i class="fa fa-exclamation-circle out-icon new-error-class" style="display:block; color: #ed7154;" title="'+$scope.minimumTimeline +'">',
                            el = document.getElementById('service_delivery_time_extra_'+num);
                            angular.element(el).after( $compile(html)($scope) )                    
                            $scope.$apply(); 
                    }else if(data > 2){
                        $scope["serviceExtraDeliveryError"+num] = false;
                        $(this).parent().removeClass("has-error");
                        $(this).parent().addClass("has-success");
                        $(this).next(".new-error-class").remove();        
                    }
                } else {                           
                    if(data <= 2 ){ 
                        $scope["serviceDeliveryError"+num] = true;
                         $(this).parent().addClass("has-error");
                         $(this).parent().removeClass("has-success");
                         $(this).next("i").remove();        
                         var html='<i class="fa fa-exclamation-circle out-icon new-error-class" style="display:block; color: #ed7154;" title="'+$scope.minimumTimeline +'">',
                            el = document.getElementById('service_delivery_time_'+num);
                            angular.element(el).after( $compile(html)($scope) )                    
                            $scope.$apply(); 
                    }else if(data > 2){
                        $scope["serviceDeliveryError"+num] = false;
                        $(this).parent().removeClass("has-error");
                        $(this).parent().addClass("has-success");
                        $(this).next(".new-error-class").remove();        
                    } 
                }
            }else if( splittedData[1] == "price"){       
                if(splittedData[2] == "extra"){
                    if(data <= 0 ) {
                        $scope["serviceExtraPriceError"+num] = true;
                         $(this).parent().addClass("has-error");
                         $(this).parent().removeClass("has-success");
                         $(this).next("i").remove();        
                          var html='<i class="fa fa-exclamation-circle out-icon new-error-class" style="display:block; color: #ed7154;" title="'+$scope.minimumBudget+'">',
                            el = document.getElementById('service_price_extra_'+num);
                            angular.element(el).after( $compile(html)($scope) )                    
                            $scope.$apply(); 
                    } else if(data > 0) {
                        $scope["serviceExtraPriceError"+num] = false;
                        $(this).parent().removeClass("has-error");
                        $(this).parent().addClass("has-success");
                        $(this).next(".new-error-class").remove();        
                    } 
                } else {
                    if(data <= 0 ){ 
                        $scope["servicePriceError"+num] = true;
                         $(this).parent().addClass("has-error");
                         $(this).parent().removeClass("has-success");
                         $(this).next("i").remove();        
                          var html='<i class="fa fa-exclamation-circle out-icon new-error-class" style="display:block; color: #ed7154;" title="'+$scope.minimumBudget+'">',
                            el = document.getElementById('service_price_'+num);
                            angular.element(el).after( $compile(html)($scope) )                    
                            $scope.$apply(); 
                    }else if(data > 0){
                        $scope["servicePriceError"+num] = false;
                        $(this).parent().removeClass("has-error");
                        $(this).parent().addClass("has-success");
                        $(this).next(".new-error-class").remove();        
                    } 
                }
            }                        
        });
        for (var i in CKEDITOR.instances) {
            CKEDITOR.instances[i].on('change', function() {                            
                var data  = CKEDITOR.instances[i].getData();
                var ckeId = $(this).parent().prevObject[0].name;
                var ckeId = $('#'+ckeId).parent().attr('id');
                if( data == "" || data == undefined){
                    // $(".success-call-desc").css("display","none");
                    // $(".class-for-description").css("display","block");
                    // $(".class-for-description").removeClass("hide");
                    // $(".class-for-description").css("color","#ed7154");
                    $('#'+ckeId).find(".success-call-desc").css("display","none");
                    $('#'+ckeId).find(".class-for-description").css("display","block");
                    $('#'+ckeId).find(".class-for-description").removeClass("hide");
                    $('#'+ckeId).find(".class-for-description").css("color","#ed7154");
                }else{
                    // $(".added-from-publish").css("display","none");
                    // $(".class-for-description").css("display","none");
                    // $(".success-call-desc").css("display","block");
                    // $(".success-call-desc").removeClass("hide");
                    $('#'+ckeId).find(".added-from-publish").css("display","none");
                    $('#'+ckeId).find(".class-for-description").css("display","none");
                    $('#'+ckeId).find(".class-for-description").addClass("hide");
                    $('#'+ckeId).find(".success-call-desc").css("display","block");
                    $('#'+ckeId).find(".success-call-desc").removeClass("hide");
                }
            });
        }
    },100);     
    $scope.$watch("fileUploaded", function() {        
        if($scope.fileUploaded == true){            
            $(".errorDisplayClass").remove();
        }
    });

    $(function(){
       $('.publish').click(function(){
            var id = $(this).attr('id');
            var id = id.replace("publish_",'');
            var num = id.split('_').pop();        
            $scope["serviceTitleError"+num] = false;
            $scope["serviceDetailError"+num] = false;
            $scope["serviceRevisionError"+num] = false;
            $scope["serviceDeliveryError"+num] = false;
            $scope["servicePriceError"+num] = false;
            $scope["serviceExtraRevisionError"+num] = false;
            $scope["serviceExtraDeliveryError"+num] = false;
            $scope["serviceExtraPriceError"+num] = false;
            var serviceTitle = $("#service_title_"+num).val();
            if(num > 1){
                var  newNum = num - 1;
                var serviceDetails = CKEDITOR.instances['service_details_'+newNum].getData();
            }else{
                var serviceDetails = CKEDITOR.instances['service_details'].getData();
            }            
            var serviceRevisions = $("#service_revisions_"+num).val();
            var serviceDelivery = $("#service_delivery_time_"+num).val();
            var servicePrice = $("#service_price_"+num).val();
            var serviceExtraRevision = $("#service_revisions_extra_"+num).val();
            var serviceExtraDelivery = $("#service_delivery_time_extra_"+num).val();
            var serviceExtraPrice = $("#service_price_extra_"+num).val();            
            if(serviceTitle == "" || serviceTitle == null || serviceDetails == "" || serviceDetails == null || serviceRevisions == "" || serviceRevisions == null || serviceDelivery == "" || serviceDelivery == null || servicePrice == "" || servicePrice == null || $scope.fileUploaded === false || (serviceExtraDelivery != "" &&  serviceExtraDelivery <= 3) || (serviceExtraPrice != "" &&  serviceExtraPrice <= 0)){
                if(serviceTitle == "" || serviceTitle == null){
                    $scope["serviceTitleError"+num] = true;                            
                    var html='<i class="fa fa-exclamation-circle out-icon new-error-class" style="display:block; color: #ed7154;" title="'+$scope.titleRequired+'">';
                        var el = document.getElementById('service_title_'+num);
                        angular.element(el).after( $compile(html)($scope) )                    
                        $scope.$apply();                                                        
                } 
                if(serviceDetails == "" || serviceDetails == null){                                  
                    $scope["serviceDetailError"+num] = true;                    
                    var html1='<i class="fa fa-exclamation-circle out-icon class-for-description new-error-class new-class-icon added-from-publish"  title="'+$scope.detailRequired+'">',
                        el1 = document.getElementById('div_service_details_'+num);
                        console.log(num);
                        angular.element(el1).append( $compile(html1)($scope) )                    
                        $scope.$apply();                                     
                }
                if(serviceRevisions == "" || serviceRevisions == null){
                    $scope["serviceRevisionError"+num] = true;
                    var html='<i class="fa fa-exclamation-circle out-icon new-error-class" style="display:block; color: #ed7154;" title="'+$scope.revisionsRequired+'">',
                        el = document.getElementById('service_revisions_'+num);
                        angular.element(el).after( $compile(html)($scope) )                    
                        $scope.$apply();                                                        
                } 
                if(serviceDelivery == "" || serviceDelivery == null || serviceDelivery <= 2){
                    $scope["serviceDeliveryError"+num] = true;
                    if( serviceDelivery != "" && serviceDelivery <= 2){
                        var html='<i class="fa fa-exclamation-circle out-icon new-error-class" style="display:block; color: #ed7154;" title="'+$scope.minimumTimeline +'">';
                    }else{
                        var html='<i class="fa fa-exclamation-circle out-icon new-error-class" style="display:block; color: #ed7154;" title="'+$scope.timelineRequired+'">';    
                    }                    
                    var el = document.getElementById('service_delivery_time_'+num);
                        angular.element(el).after( $compile(html)($scope) )                    
                        $scope.$apply();                                                        
                } 
                if(servicePrice == "" || servicePrice == null || servicePrice <= 0){
                    $scope["servicePriceError"+num] = true;
                    if( serviceDelivery != "" && serviceDelivery <= 0){
                         var html='<i class="fa fa-exclamation-circle out-icon new-error-class" style="display:block; color: #ed7154;" title="'+$scope.minimumBudget+'">';
                    }else{
                        var html='<i class="fa fa-exclamation-circle out-icon new-error-class" style="display:block; color: #ed7154;" title="'+$scope.priceRequired +'">';
                    } 
                    
                    var el = document.getElementById('service_price_'+num);
                        angular.element(el).after( $compile(html)($scope) )                    
                        $scope.$apply();                                                        
                }                 
                if( serviceExtraDelivery != "" && serviceExtraDelivery <= 2){
                    $scope["serviceExtraDeliveryError"+num] = true;
                    var html='<i class="fa fa-exclamation-circle out-icon new-error-class" style="display:block; color: #ed7154;" title="'+$scope.minimumTimeline +'">';    
                    var el = document.getElementById('service_delivery_time_extra_'+num);
                    angular.element(el).after( $compile(html)($scope) )                    
                    $scope.$apply();                                                                            
                }
                // if(serviceExtraPrice != "" && serviceExtraPrice < 31){
                //     $scope["serviceExtraPriceError"+num] = true;
                //     var html='<i class="fa fa-exclamation-circle out-icon new-error-class" style="display:block; color: #ed7154;" title="'+$scope.minimumBudget+'">';
                //     var el = document.getElementById('service_price_extra_'+num);
                //     angular.element(el).after( $compile(html)($scope) )                    
                //     $scope.$apply();                                                                          
                // }   
                if($scope.fileUploaded === false){                                                      
                    typeof($scope.fileUploaded);
                    $("#errorDisplayClassImages").remove(".errorDisplayClass");
                    var html='<li class="errorDisplayClass" id="errorDisplayClassImages"><i class="fa fa-exclamation-circle out-icon new-error-class" style="display:block; color: #ed7154;" title="'+$scope.imageRequired +'"></li>';
                    var el = document.getElementById('imageDivID'+num);
                    angular.element(el).append( $compile(html)($scope) )                    
                    $scope.$apply();
                }                                
                return false;
            }else{
                var newService = new FormData($("#"+id)[0]);   
                newService.append('status','1');
                newService.append('mp_seller_service_skill_id',$scope.forCategory);
                newService.append('language',getLanguage);
                if(num > 1){
                    var  newNum = num - 1;                        
                    newService.append('service_details',CKEDITOR.instances['service_details_'+newNum].getData());
                }else{
                    newService.append('service_details',CKEDITOR.instances['service_details'].getData());
                }
                // showLoader('.publish_'+num);
                if(getLanguage=='en'){showLoader('.publish_'+num,'Please wait');}else{showLoader('.publish_'+num,'请稍候');}
                Disable('.draft_'+num);
                Disable('.delete_'+num);
                    mpfactory.Addservices(newService).then(function(response) {
                        // hideLoader('.publish_'+num,'Publish');
                        if(getLanguage=='en'){hideLoader('.publish_'+num,'Publish');}else{hideLoader('.publish_'+num,'发布');} 
                        Enable('.draft_'+num);
                        Enable('.delete_'+num);
                        if (response.data.code == 2) window.location.replace("sixteen-login");
                        if (response.data.status == 1) {
                            $scope.lastService = id;
                            $timeout(function(){
                                $scope.CloneService();
                            },500);
                            $('<input>').attr({type: 'hidden',id: 'draftFormID',name: 'draftFormID', value:''+response.data.data.id+''}).appendTo('form');
                            simpleAlert('success','',response.data.message);
                            $("#"+id+" :input").prop("disabled", true);
                            $('#deletediv_'+id).hide();
                            Enable('.delete_'+num);
                            $timeout(function(){ 
                                location.reload();
                            },500);
                        } else {
                            simpleAlert('error', '', response.data.message);
                        }
                    });
            }
       });
    });
    $scope.reinitializeTimeout = function(){
        $timeout(function(){
            (function(){
                $('.FlowupLabels').FlowupLabels({
                    feature_onInitLoad: true,
                    class_focused:      'focused',
                    class_populated:    'populated'
                  });
            })();
            function readURL1(input,id,image_number) {
              if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                  // $('#'+id).attr('src', e.target.result);
                    newpreviewid = id;
                    newpreviewid = newpreviewid.replace('img_existing','existing_fileClick');
                    newpreviewid = newpreviewid+'_'+image_number;
                    $('#'+newpreviewid).css('background-image','url('+e.target.result+')');
                }
                reader.readAsDataURL(input.files[0]);
              }
            }

            function changeImg1(fileid,id,image_number) {
                $('#'+fileid).change(function() {
                    readURL1(this,id,image_number);
                });
            }

            $(".existing_a_img").click(function() {
                var id = $(this).attr('id');
                var id = id.replace("fileClick_",'');
                var image_number = id.split('_').pop();
                var id = id.replace("_"+image_number,'');
                var formID = $(this).closest("form").attr('id');
                $('<input>').attr({type: 'hidden',id: 'remove_image',name: 'remove_image[]', value:''+image_number+''}).appendTo('#'+formID);
                $timeout(function(){
                    $('#'+id).click();
                    var previewId = 'img_'+id;
                    changeImg1(id,previewId,image_number);
                },300);
            });

            $(function(){
               $('.add-extra').click(function(){
                    var id = $(this).attr('id');
                    var id = id.replace("addextra_",'');
                    id = id.split('_').pop();
                    var copy = $("#clone_revisions").clone(true);
                    copy.removeClass('hide');
                    $('#existing_clone_revisions_extra_'+id).append(copy);
                    $('#existing_clone_revisions_extra_'+id).children().last().find('input:text').val('');
                    $("#existing_clone_revisions_extra_"+id+" :input").prop("disabled", false);
                    $scope.extraid++;
               });
            });

            $(function(){
               $('.extra_remove').click(function(){
                    $(this).closest("#clone_revisions").remove();
               });
            });

           $(function(){
                $timeout(function(){
                    $('.publish_existing').click(function(){
                        var id = $(this).attr('id');
                        var id = id.replace("publish_",'');
                        var num = id.split('_').pop();
                        var newService = new FormData($("#"+id)[0]);
                        newService.append('status','1');
                        newService.append('mp_seller_service_skill_id',$scope.forCategory);
                        newService.append('images','');
                        newService.append('language',getLanguage);
                        newService.append('service_details', CKEDITOR.instances['service_details_'+num].getData());
                        if(getLanguage=='en'){showLoader('.publish_'+num,'Please wait');}else{showLoader('.publish_'+num,'请稍候');}
                        mpfactory.Addservices(newService).then(function(response) {
                            if(getLanguage=='en'){hideLoader('.publish_'+num,'Publish');}else{hideLoader('.publish_'+num,'发布');}
                            if (response.data.code == 2) window.location.replace("sixteen-login");
                            if (response.data.status == 1) {
                                simpleAlert('success','',response.data.message);
                                $scope.detailsForCategory($scope.forCategory);
                                $timeout(function(){ 
                                    location.reload();
                                },500);
                            } else {
                                simpleAlert('error', '', response.data.message);
                            }
                        });
                    });
                },500);
            });

           $(function(){
               $('.save_as_draft_existing').click(function(){
                    var id = $(this).attr('id');
                    var id = id.replace("draft_",'');
                    var num = id.split('_').pop();
                    var newService = new FormData($("#"+id)[0]);   
                    newService.append('status','3');
                    newService.append('mp_seller_service_skill_id',$scope.forCategory);
                    newService.append('language',getLanguage);
                    newService.append('service_details', CKEDITOR.instances['service_details_'+num].getData());
                    if(getLanguage=='en'){showLoader('.draft_'+num,'Please wait');}else{showLoader('.draft_'+num,'请稍候');}
                    mpfactory.Addservices(newService).then(function(response) {
                        if(getLanguage=='en'){hideLoader('.draft_'+num,'Save as Draft');}else{hideLoader('.draft_'+num,'保存为草稿');} 
                        if (response.data.code == 2) window.location.replace("sixteen-login");
                        if (response.data.status == 1) {
                            simpleAlert('success','',response.data.message);
                            $scope.detailsForCategory($scope.forCategory);
                            $timeout(function(){ 
                                location.reload();
                            },500);
                        } else {
                            simpleAlert('error', '', response.data.message);
                        }
                    });
               });
            });

           $(function(){
               $('.exesting_delete').click(function(){
                    var id = $(this).attr('id');
                    var id = id.replace("delete_",'');
                    var newService = new FormData($("#"+id)[0]);
                    newService.append('language',getLanguage);
                    if(getLanguage=='en'){
                        title="Are you sure?";
                        text="Your will not be able to recover this service.";
                        confirmButtonText="Yes, delete it";
                        cancelButtonText="Cancel";
                    }else{
                        title="你确定？";
                        text="您将无法恢复此服务.";
                        confirmButtonText="是的，删除它";
                        cancelButtonText="取消";
                    }
                    swal({
                      title: title,
                      text: text,
                      type: "warning",
                      showCancelButton: true,
                      cancelButtonText:cancelButtonText,
                      confirmButtonColor: "#8a74d0",
                      confirmButtonText: confirmButtonText,
                      closeOnConfirm: true
                    },
                    function(isConfirm){
                        if (isConfirm) {
                            mpfactory.Deleteservices(newService).then(function(response) {
                                if (response.data.code == 2) window.location.replace("sixteen-login");
                                if (response.data.status == 1) {
                                    simpleAlert('success','',response.data.message);
                                    $scope.detailsForCategory($scope.forCategory);
                                } else {
                                    simpleAlert('error', '', response.data.message);
                                }
                            });
                        }
                    });
               });
            });
        },2000);
    }
    // $scope.reinitializeTimeout();
    
}]);