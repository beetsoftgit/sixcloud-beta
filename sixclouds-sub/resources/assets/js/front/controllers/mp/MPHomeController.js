/*
 Author  : Komal Kapadi (komal@creolestudios.com)
 Date    : 26th March 2018
 Name    : MPHomeController
 Purpose : All the functions for MP home page
 */
angular.module('sixcloudApp').controller('MPHomeController', ['$sce', '$http', '$scope', '$timeout', '$rootScope', '$filter','mpfactory','MetaService', function($sce, $http, $scope, $timeout, $rootScope, $filter,mpfactory,MetaService) {
    // $timeout(function(){
        // $rootScope.metaservice = MetaService;
        // $rootScope.metaservice.set("SixClouds Sixteen – A Creative Platfrom To Hire Freelancers & Get Freelance Jobs Online","Sixteen is a creative platfrom to show your creative skills and hire the best talents. Explore services like logo, web, 2D/3D, app design, video animation and more.");
    // },1000);

    console.log('MPhome controller loaded');
    // create a message to display in our view
    $scope.message = 'Everyone come and see how good I look!';
    var swiper = new Swiper('.header-swiper-container', {
        effect: 'fade',
        autoplay: {
            delay:5000,
            disableOnInteraction: false,
        },
        navigation: {
            // nextEl: '.swiper-button-next',
            // prevEl: '.swiper-button-prev',
        },
    });
    var swiper = new Swiper('.graphic-deign-swiper', {
        loop: false,
        slidesPerView: 5,
        slidesPerColumn: 2,
        spaceBetween: 30,
        navigation: {
            nextEl: '.graphicD-swiper-button-next',
            prevEl: '.graphicD-swiper-button-prev',
        },
        breakpoints: {
            1024: {
                slidesPerView: 4,
                spaceBetween: 40,
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 30,
            },
            640: {
                slidesPerColumn: 1,
                slidesPerView: 2,
                spaceBetween: 20,
            },
            320: {
                slidesPerColumn: 1,
                slidesPerView: 1,
                spaceBetween: 10,
            }
        }
    });
    var swiper = new Swiper('.testimonial-swiper', {
        //loop:true,
        slidesPerView: 4,
        spaceBetween: 30,
        navigation: {
            nextEl: '.testimonial-swiper-button-next',
            prevEl: '.testimonial-swiper-button-prev',
        },
        breakpoints: {
            1024: {
                slidesPerView: 3,
                spaceBetween: 30,
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 30,
            },
            640: {
                slidesPerView: 1,
                spaceBetween: 20,
            },
            320: {
                slidesPerView: 1,
                spaceBetween: 10,
            }
        }
    });
    $scope.showSellersonLandingpage=function() {
        mpfactory.Showsellersonlandingpage().then(function(response){
            if(response.data.status==1)
            {
                $scope.sellerData=response.data.data;

            }      
        });
    }
    $scope.showSellersonLandingpage();

    $scope.swiperfunction=function()
    {
        var swiper = new Swiper('.browse-designers-swiper', {
            slidesPerView: 1,
            spaceBetween: 0,
            loop: false,
             navigation: {
               nextEl: '.browse-designers-button-next',
               prevEl: '.browse-designers-button-prev',
             },
           });
    }
    $timeout(function(){
        $scope.swiperfunction();
    }, 2000);
}]);