/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 30th March 2018
 Name    : MPLoginController
 Purpose : All the functions for MP login page
 */
angular.module('sixcloudApp').controller('MPLoginController', ['$sce', 'mpfactory', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter','MetaService', function($sce, mpfactory, $routeParams, $http, $scope, $timeout, $rootScope, $filter,MetaService) {
    console.log('MPLogin controller loaded');
    $('body').css('background', '#fff');
    // $rootScope.metaservice = MetaService;
    // $rootScope.metaservice.set("Sign In – SixClouds Sixteen","Log in at SixClouds Sixteen, the world\'s top multi-media platforms – Sixteen.");
    (function() {
        $('.FlowupLabels').FlowupLabels({
            /*
             * These are all the default values
             * You may exclude any/all of these options
             * if you won't be changing them
             */
            // Handles the possibility of having input boxes prefilled on page load
            feature_onInitLoad: true,
            // Class when focusing an input
            class_focused: 'focused',
            // Class when an input has text entered
            class_populated: 'populated'
        });
    })();
    $scope.show_error = false;
    $scope.Dosignin = function() {

        if ($scope.password == undefined || $scope.email_address == undefined) {
            if ($scope.password == undefined) {
                $scope.password_error = true;
            }
            if ($scope.email_address == undefined) {
                $scope.email_address_error = true;
            }
        } else {
            login_data = {}
            login_data['email_address'] = $scope.email_address;
            login_data['password'] = $scope.password;
            login_data['timezone'] = moment.tz.guess();
            mpfactory.Dosignin(login_data).then(function(response) {
                if (response.data.status == 1) {
                    if (response.data.data.marketplace_current_role == 1) {
                        if(response.data.data.account_varification_status==2){
                            mpfactory.Myservicepageshown().then(function(response) {
                                if (response.data.status == 1) {
                                    window.location.replace("my-services");
                                } else {}
                            });                
                        } else {
                            window.location.replace("seller-dashboard");
                        }
                    } else {
                        window.location.replace("buyer-dashboard");
                    }
                } else {
                    $scope.login_error = response.data.message;
                    $scope.show_error = true;
                    $timeout(function() {
                        $scope.show_error = false;
                    }, 5000);
                }
            });
        }
    }
    $scope.Forgetpassword = function() {
        if ($scope.email == undefined) {
            $scope.forgot_email_address_error = true;
        } else {
            showLoader('.forgot-password');
            mpfactory.forgotpassword({'email_address':$scope.email}).then(function(response) {
                hideLoader('.forgot-password', 'SUBMIT');
                if (response.data.status == 1) {
                    $scope.forgot_email_address_error = false;
                    $('.close').click();
                    simpleAlert('success', 'Success', response.data.message);
                    $('#forgetPasswordForm')[0].reset();
                    $scope.email = "";
                } else {
                    $scope.login_error = "Incorrect Email Address or Password"
                    $scope.show_error = true;
                    $timeout(function() {
                        $scope.show_error = false;
                    }, 5000);
                }
            });
        }
    }
}]);