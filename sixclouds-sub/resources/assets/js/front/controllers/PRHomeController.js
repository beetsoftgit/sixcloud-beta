/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 6th December 2017
 Name    : PRHomeController
 Purpose : All the functions for proof reading home page
 */
angular.module('sixcloudApp').controller('PRHomeController', ['$http', '$scope', '$timeout', '$rootScope', '$filter', function ($http, $scope, $timeout, $rootScope, $filter) {
    console.log('PR home controller loaded');
     	new WOW().init();
        
}]);