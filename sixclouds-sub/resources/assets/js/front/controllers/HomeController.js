/*
 Author  : Komal Kapadi (komal@creolestudios.com)
 Date    : 4th December 2017
 Name    : HomeController
 Purpose : All the functions for home page
 */
angular.module('sixcloudApp').controller('HomeController', ['$sce', 'Geteltsections', '$http', '$scope', '$timeout', '$rootScope', '$filter', function($sce, Geteltsections, $http, $scope, $timeout, $rootScope, $filter) {
    console.log('home controller loaded');
    $scope.eltdata = Geteltsections.data.data;
}]);