
app = angular.module('sixcloudbuyerApp', ['ngRoute','pascalprecht.translate']);
/*var myApp = angular.module('sixcloudAboutApp', []);*/

// if (Cookies.get('language') == 'undefined' || Cookies.get('language') == '' || Cookies.get('language') == undefined) Cookies.set('language', 'en');
app.config(function($routeProvider, $translateProvider) {
    $translateProvider.translations('en', {
    	lbl_lang_chng:'中文',
        lbl_sixclouds:                    'SixClouds',
        lbl_find:                         'FIND',
        lbl_work:                         'WORK',
        lbl_revise:                       'REVISE',
        lbl_feedback:                     'FEEDBACK',
        lbl_find_quality:                 'Easily find quality seller',
        lbl_start_register:               'Start by Registering',
        lbl_start_register_text:          'Register yourself on SixClouds to get things done in few clicks.',
        lbl_post_job:                     'Post a Job',
        lbl_post_job_text:                'Tell us about your project and the specific skills required.',
        lbl_browse_sellers:               'Browse Sellers',
        lbl_browse_sellers_text:          'Search for your prefect match by viewing their work history, ratings etc. and select him to get your work.',
        lbl_or:                           'OR',
        lbl_select_sugggestions:          'Select from what we suggest',
        lbl_select_sugggestions_text:     'Our proprietary algorithm helps us to suggest you the one who fit your requirements.',
        lbl_word_efficiently_effectively: 'Work efficiently, effectively',
        lbl_connect_seller:               'Connect with your seller',
        lbl_connect_seller_text:          'Start communicating with your seller through our chat board via text, image or video.',
        lbl_send_receive_files:           'Send and receive files',
        lbl_send_receive_files_text:      'Deliver digital assets in a secure environment.',
        lbl_satisfaction_important:       'Your satisfaction is important to us',
        lbl_request_revision:             'Request a revision',
        lbl_request_revision_text:        'If not satisfied with the finished product you can always ask for a revision.',
        lbl_top_up_revision:              'Top-up for the revision',
        lbl_top_up_revision_text:         'Just pay the amount you both agree upon and get an additional revision.',
        lbl_share_experience:             'Share your experience with us',
        lbl_rating_testimonial:           'Ratings and Testimonial',
        lbl_rating_testimonial_text:      'Rate and leave a testimonial for the seller you worked with.',
        lbl_review_count:                 'Your reviews count ',
        lbl_review_count_text:            'We value your reviews as they help us know about our strengths and weaknesses. ',
        lbl_about_us:                     'About Us',
        lbl_ignite:                       'Ignite',
        lbl_sixteen:                      'Sixteen',
        lbl_discover:                     'Discover',
        lbl_proof_reading:                'Proof Reading',
        lbl_quick_links:                  'Quick Links',
        lbl_become_seller:                'Become a Seller',
        lbl_become_buyer:                 'Become a Buyer',
        lbl_customer_support:             'Customer Support',
        lbl_terms_service:                'Terms of Service',
        lbl_privacy_policy:               'Privacy Policy',
        lbl_all_rights_reserved:          'All rights reserved',
        lbl_the_company:                  'The Company',
        lbl_core_values:                  'Core Values',
        lbl_our_team:                     'Our Team',
        lbl_allrights:                    'All rights reserved',

        


    }).translations('chi', {
        lbl_lang_chng:                    'English',
        lbl_sixclouds:                    '六云',
        lbl_find:                         '寻找',
        lbl_work:                         '工作',
        lbl_revise:                       '修改',
        lbl_feedback:                     '评价',
        lbl_find_quality:                 '轻松找到高质量卖家',
        lbl_start_register:               '简单注册',
        lbl_start_register_text:          '在六云注册户口，只需简单点击几下即可完成。',
        lbl_post_job:                     '发布项目',
        lbl_post_job_text:                '告诉我们您的项目所需的具体技能。',
        lbl_browse_sellers:               '浏览卖家',
        lbl_browse_sellers_text:          '通过查看他们的工作历史，评分等来搜索您的完美卖家，并选择他来完成您的项目。',
        lbl_or:                           '或',
        lbl_select_sugggestions:          '从我们的建议中选择',
        lbl_select_sugggestions_text:     '我们专有的匹配算法可以向您推荐最符合您要求的卖家。',
        lbl_word_efficiently_effectively: '有效和高效地工作',
        lbl_connect_seller:               '与您的卖家联系',
        lbl_connect_seller_text:          '我们的聊天板能通过文字，图片或视频与您的卖家沟通。',
        lbl_send_receive_files:           '发送和接收文件 ',
        lbl_send_receive_files_text:      '在安全的环境下发送数字资产。',
        lbl_satisfaction_important:       '您的满意程度对我们很重要',
        lbl_request_revision:             '要求修改 ',
        lbl_request_revision_text:        '如果对成品不满意，您可以随时要求修改。',
        lbl_top_up_revision:              '额外修改',
        lbl_top_up_revision_text:         '只需支付您双方同意的金额并获得额外的修改。',
        lbl_share_experience:             '与我们分享您的经验',
        lbl_rating_testimonial:           '评分和评价',
        lbl_rating_testimonial_text:      '给与您合作的卖家评分和评价。',
        lbl_review_count:                 '我们重视您的评论 ',
        lbl_review_count_text:            '这可以帮助我们了解我们的优势和劣势。',
        lbl_about_us:                     '关于我们',
        lbl_ignite:                       '点燃',
        lbl_sixteen:                      '十六',
        lbl_discover:                     '发现',
        lbl_proof_reading:                '证明阅读',
        lbl_quick_links:                  '快速链接',
        lbl_become_seller:                '成为卖家',
        lbl_become_buyer:                 '成为买家',
        lbl_customer_support:             '客服中心',
        lbl_terms_service:                '服务条款',
        lbl_privacy_policy:               '隐私政策',
        lbl_all_rights_reserved:          '版权所有',
        lbl_the_company:                  '六云简介',
        lbl_core_values:                  '核心理念',
        lbl_our_team:                     '我们的团队',  
        lbl_allrights:                    '版权所有',     

    });
    $translateProvider.preferredLanguage(Cookies.get('language'));
});
/* To change the interolate provider need to change it's originonal brackets.*/
app.config(['$interpolateProvider', function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
}]);
/* for changing title */
app.run(['$location', '$rootScope', '$http', '$timeout', function($location, $rootScope, $http, $timeout) {
    $rootScope.$on('$routeChangeSuccess', function(event, current, previous) {
        if (current.$$route != undefined) $rootScope.title = current.$$route.title;
        document.body.scrollTop = document.documentElement.scrollTop = 0;
    });
}]);
app.controller('BuyerController', ['$scope', '$translate', '$rootScope', '$timeout', function ($scope, $translate, $rootScope, $timeout){
	console.log("Buyer Controller");
	
	var ctrl = this;
    if (Cookies.get('language') == 'undefined' || Cookies.get('language') == '' || Cookies.get('language') == undefined) {
        $scope.url = window.location.host;
        $scope.url = $scope.url.split('.');
        $scope.url = $scope.url[2];
        $timeout(function() {
            if($scope.url==undefined){
                $timeout(function() {
                    $scope.changeLang('en');
                },500);
            }
            if($scope.url == 'net' && $scope.url!=undefined){
                $timeout(function() {
                    $scope.changeLang('en');
                },500);
            }
            if($scope.url == 'cn' && $scope.url!=undefined){
                $timeout(function() {
                    $scope.changeLang('chi');
                },500);
            }
        },1000);
    }
    $scope.changeLang = function(lang) {
        if (lang == 'en') $rootScope.eng = false;
        else if (lang == 'chi') $rootScope.eng = true;
        var getLanguage = lang;
        ctrl.language = getLanguage;
        Cookies.set('language', ctrl.language);
        $translate.use(ctrl.language);
    }
}]);
