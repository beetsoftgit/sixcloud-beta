/* 
 * Author: Senil Shah
 * Date  : 9th Jan, 2019
 * sphere factory file
 */
angular.module('sixcloudApp').factory('spherefactory', ['$http', '$rootScope',
    function ($http, $rootScope) {
        return {
            Gettickettype: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Gettickettype'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getcountry: function(data) {
                return $http({
                    method: 'GET',
                    data: data,
                    url: 'Getcountry'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getstatesforcountry: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getstatesforcountry'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getcitiesforstate: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getcitiesforstate'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Spheresignup: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Spheresignup',
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                });
            },
            Getteacherslist: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getteacherslist'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getteacherchat: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getteacherchat'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Sendmessage: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Sendmessage'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Makemessageread: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Makemessageread'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getspheresubjects: function() {
                return $http({
                    method: 'GET',
                    url: 'Getspheresubjects'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getteacherdata: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getteacherdata'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getspheretransactions: function (data, pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getspheretransactions?page=' + pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getspherehistories: function (data, pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getspherehistories?page=' + pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getmycourses: function() {
                return $http({
                    method: 'GET',
                    url: 'Getmycourses'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
        };
    }
]);