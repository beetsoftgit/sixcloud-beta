/*
 Author  : Komal Kapadi (komal@creolestudios.com)
 Date    : 21st December 2017
 Name    : TransactionController
 Purpose : All the functions for transaction page
 */
angular.module('sixcloudAdminApp').controller('TransactionController', ['subscriptionfactory', 'Getsubscriptions', '$http', '$scope', '$timeout', '$rootScope', '$filter', function (subscriptionfactory, Getsubscriptions, $http, $scope, $timeout, $rootScope, $filter) {
        $scope.subscriptions = Getsubscriptions.data.data;
        console.log('transaction controller loaded');
        /*
         * Pagination start
         */
        $scope.gap = 2;
        $scope.message = '';
        $scope.itemsPerPage = 2;
        $scope.currentPage = 0;
        $scope.offset = 0;
        $scope.startfrom = 1;
        $scope.endto = $scope.startfrom + 1;
        /*=================== Fetches user's question related details ===========*/
        getResult({'offset': 0});
        /* When page loads defaults first 10 records loads with out any record filteration. */
        function getResult($data) {
            subscriptionfactory.Getpayments($data).then(function (response) {
                $scope.transactions = response.data.data;
                $scope.items = response.data.data;
                $scope.count_total = response.data.count_total;
                $scope.pagedItems = $scope.items;
                $scope.message = response.data.message;
                $scope.pageinate = Math.ceil($scope.count_total / $scope.itemsPerPage);
                $scope.range = function (size, start, end)
                {
                    var ret = [];
                    if (size < end) {
                        end = size;
                        start = size - end;
                    }
                    for (var i = start; i < end; i++) {
                        ret.push(i);
                    }
                    return ret;
                };
                /* FUNCTION CALLED ON CLICK ON PREVIOUS PAGE */
                $scope.prevPage = function () {
                    if ($scope.currentPage > 0) {
                        $scope.currentPage--;
                    }
                    $scope.getdata();
                };
                /* FUNCTION CALLED ON CLICK ON NEXT PAGE  */
                $scope.nextPage = function () {
                    if ($scope.currentPage < $scope.pageinate - 1) {
                        $scope.currentPage++;
                    }
                    $scope.getdata();
                };
                /* TO SET THE PAGE */
                $scope.setPage = function () {
                    $scope.currentPage = this.n;
                    $scope.getdata();
                };
                /*FUNCTION FETCHED RECOERDS FROM THE HTTP CALL ON EVERY CLICK EVENT */
                $scope.getdata = function ()
                {
                    $scope.offset = $scope.currentPage;
                    $scope.startfrom = ($scope.itemsPerPage * $scope.offset) + 1;
                    $scope.endto = $scope.startfrom + 1;
                    $data = {'offset': $scope.offset};
                    subscriptionfactory.Getpayments($data).then(function (response) {
                        $scope.pagedItems = [];
                        $scope.pagedItems = response.data;
                        $scope.message = response.data.message;
                        $scope.transactions = response.data.data;
                        if ($scope.pagedItems.length < $scope.endto) {
                            $scope.endto = $scope.count_total;
                        } else {
                            $scope.endto = $scope.startfrom + 1;
                        }
                    });
                }
            });
        }

        /*
         * Search by User's first_name or last_name
         */
        $scope.searchByName = function (searchString) {
            getResult({'offset': 0, 'search': searchString});
        }

        $scope.addNewSection = function (category_name, description) {
            $scope.submitted = true;
            subscriptionfactory.Addnewsection({'category_name': category_name, 'description': description}).then(function (response) {
                if (response.data.status == 1) {
                    $scope.videoData = response.data.data;
                    $('#section_form')[0].reset();
                    simpleAlert('success', 'New Section', response.data.message, true);
                    eltfactory.Geteltsections().then(function (res) {
                        $scope.sections = res.data.data;
                    });
                }
            });
        }

        $scope.changePlans = function (planid) {
            getResult({'offset': 0, 'subscription_id': planid});
        }
    }]);