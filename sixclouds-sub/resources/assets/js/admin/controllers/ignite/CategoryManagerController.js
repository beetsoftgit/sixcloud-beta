/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 18th Jan 2018
 Name    : CategoryManagerController
 Purpose : realted to creating/updating categories
 */
angular.module('sixcloudAdminApp').controller('CategoryManagerController', [ '$http', 'adminignitefactory', '$scope', '$timeout', '$rootScope', '$filter', function( $http, adminignitefactory, $scope, $timeout, $rootScope, $filter) {
    console.log('CategoryManager controller loaded');
    $(".navbar-nav li").removeClass("active");
    $('#adminHeaderIgnite').addClass("active"); 
    $timeout(function () {
        $(".ignite-header li").removeClass("active");
        $('#video_category_manger').addClass("active");
        $('.select2-container').css('display','unset');
    }, 1500);

    $scope.openImageSelector = function(category_id, prependString,parentIndex) {
        // console.log(prependString,'prependString')
        // console.log(category_id,'category_id')
        // console.log(parentIndex,'parentIndex')
        if(parentIndex!=undefined){
            console.log("#"+prependString+category_id+'_'+parentIndex,'here');
            $scope.parentIndex = parentIndex;    
            $scope.categoryidused = category_id;
            $("#"+prependString+parentIndex+'_'+category_id).click();

            $("#"+prependString+parentIndex+'_'+$scope.categoryidused).change(function () {
                if(event.target.files.length>0) {
                    if(event.target.files[0].type == 'image/svg+xml' || event.target.files[0].type == 'image/gif' || event.target.files[0].type == 'image/jpeg' || event.target.files[0].type == 'image/png'||event.target.files[0].type == 'image/jpg') {
                        readURL(this, prependString);
                    } else {
                        simpleAlert('error', '', 'Only GIF, JPEG, PNG & JPG file formats are allowed for Subject Image / Category GIF');
                    }
                } else {
                    $('#'+prependString+parentIndex+'_'+$scope.categoryidused+'_select').css('background-image', "url(resources/assets/images/up-img.png)");
                    $('#'+prependString+parentIndex+'_'+$scope.categoryidused+'_select').removeClass('quiz_image_option');
                    $('#'+prependString+parentIndex+'_'+$scope.categoryidused+'_select').val('');
                }
            });
        }else{
            $scope.categoryidused = category_id;
            $("#"+prependString+category_id).click();

            $("#"+prependString+$scope.categoryidused).change(function () {
                if(event.target.files.length>0) {
                    if(event.target.files[0].type == 'image/svg+xml' || event.target.files[0].type == 'image/gif' || event.target.files[0].type == 'image/jpeg' || event.target.files[0].type == 'image/png'||event.target.files[0].type == 'image/jpg') {
                        readURL(this, prependString);
                    } else {
                        simpleAlert('error', '', 'Only GIF, JPEG, PNG & JPG file formats are allowed for Subject Image / Category GIF');
                    }
                } else {
                    $('#'+prependString+$scope.categoryidused+'_select').css('background-image', "url(resources/assets/images/up-img.png)");
                    $('#'+prependString+$scope.categoryidused+'_select').removeClass('quiz_image_option');
                    $('#'+prependString+$scope.categoryidused+'_select').val('');
                }
            });
        }
        
        // $("#"+prependString+$scope.categoryidused).change(function () {
        //     if(event.target.files.length>0) {
        //         if(event.target.files[0].type == 'image/gif' || event.target.files[0].type == 'image/jpeg' || event.target.files[0].type == 'image/png'||event.target.files[0].type == 'image/jpg') {
        //             readURL(this, prependString);
        //         } else {
        //             simpleAlert('error', '', 'Only GIF, JPEG, PNG & JPG file formats are allowed for Subject Image / Category GIF');
        //         }
        //     } else {
        //         $('#'+prependString+$scope.categoryidused+'_select').css('background-image', "url(resources/assets/images/up-img.png)");
        //         $('#'+prependString+$scope.categoryidused+'_select').removeClass('quiz_image_option');
        //         $('#'+prependString+$scope.categoryidused+'_select').val('');
        //     }
        // });
    }

    $scope.checkOnLanding = function(value){
        if(value){
            $scope.onLandingPage = 1;
        }else{
            $scope.onLandingPage = 0;
        }
    }

    $scope.checkonDetail = function(value){
        if(value){
            $scope.onDetailPage = 1;
        }else{
            $scope.onDetailPage = 0;
        }
    }

    function readURL(input, prependString) {
        if (input.files && input.files[0]&&input.files.length>0) {
            setTimeout(function() {
                var reader = new FileReader();
                reader.onload = function(e) {
                    
                    if($scope.parentIndex!=undefined){
                        $('#'+prependString+$scope.parentIndex+'_'+$scope.categoryidused+'_select').css('background-image', "url("+e.target.result+")");
                        $('#'+prependString+$scope.parentIndex+'_'+$scope.categoryidused+'_select').addClass('quiz_image_option');
                    }else{
                        $('#'+prependString+$scope.categoryidused+'_select').css('background-image', "url("+e.target.result+")");
                        $('#'+prependString+$scope.categoryidused+'_select').addClass('quiz_image_option');
                    }
                }
                reader.readAsDataURL(input.files[0]);
            }, 1000);
        } else {
            $('#'+prependString+$scope.categoryidused+'_select').css('background-image', "url(resources/assets/images/up-img.png)");
            $('#'+prependString+$scope.categoryidused+'_select').removeClass('quiz_image_option');
            $('#'+prependString+$scope.categoryidused+'_select').val('');
        }
    }

    /*$scope.generateBundleIdentifier = function(category_id){
        adminignitefactory.addOrEditBundleCategoryId().then((response) => {
            if(response.data.code == 200 && response.data.status == 1) {
                $(".bundle_category_id_"+category_id).val('');
                $(".bundle_category_id_"+category_id).val(response.data.data);
                console.log($(".bundle_category_id_"+category_id).val())
            }
        });
    }*/

    $scope.changeTab = function(whichTab,category_id){
        if(whichTab == 'DetailsTab'){
            $('#details_'+category_id).show();
            $('#history_'+category_id).hide();
            $('#DetailsTabButton_'+category_id).addClass('active');
            $('#HistoryTabButton_'+category_id).removeClass('active');
            $('#DetailsTabButton_'+category_id).removeClass('hidden-arrow');
            $scope.SubscriptionPlanTab = true;
            $scope.TransactionsTab = false;
        } else {
            $('#details_'+category_id).hide();
            $('#history_'+category_id).show();
            $('#HistoryTabButton_'+category_id).addClass('active');
            $('#DetailsTabButton_'+category_id).removeClass('active');
            $('#HistoryTabButton_'+category_id).removeClass('hidden-arrow')
            $scope.TransactionsTab = true;
            $scope.SubscriptionPlanTab = false;
            $scope.Loaddata(category_id);
        }
    }
    $('#details').show();
    $('#history').hide();
    $scope.changeTab('DetailsTab');
    $scope.Loaddata = function(category_id){
        adminignitefactory.GetIgnitehistory(category_id).then(function (response) {
            if (response.data.code == 2) window.location.replace("manage/login");
            if (response.data.status == 1) {
                $scope.sgd = response.data.data.sgd;
                $scope.sgd_size = $scope.sgd.length;
                $scope.usd = response.data.data.usd;
                $scope.usd_size = $scope.usd.length;
                $scope.total_active_user = response.data.data.total_active_user;
                $active = 'TOTAL ACTIVE USERS '+$scope.total_active_user;
                $('#total_active_user_'+category_id).html($active);
                $scope.usdHtml = '';
                $scope.usd.forEach(function(element,index) {
                    $scope.usdIndex = $scope.usd_size-index;
                    if(element.admin_email != null){
                        $scope.shortHtml = '<li> Revised By: '+element.admin_email+'</li>'; 
                    } else {
                        $scope.shortHtml = '';
                    }
                    $scope.usdHtml += '<ul class="info-list-with-icon-twinkle col-sm-12 column border"><li> 12 Month USD Price : '+element.category_price+'</li><li> 6 Month USD Price : '+element.category_price_6month+'</li><li> 3 Month USD Price : '+element.category_price_3month+'</li>'+$scope.shortHtml+'<li> Date : '+element.created_at+'</li><li> Active User : '+element.payment_usd_count+'</li><li> Version : '+$scope.usdIndex+'</li></ul>';
                });
                $('#historyUsdDetails_'+category_id).html($scope.usdHtml);
                $scope.sgdHtml = '';
                $scope.sgd.forEach(function(element,index) {
                    $scope.sgdIndex = $scope.sgd_size-index;
                    if(element.admin_email != null){
                        $scope.shortHtml = '<li> Revised By: '+element.admin_email+'</li>'; 
                    } else {
                        $scope.shortHtml = '';
                    }
                    $scope.sgdHtml += '<ul class="info-list-with-icon-twinkle col-sm-12 column border pull-right"><li> 12 Month SGD Price : '+element.category_sgd_price+'</li><li> 6 Month SGD Price : '+element.category_sgd_price_6month+'</li><li> 3 Month SGD Price : '+element.category_sgd_price_3month+'</li>'+$scope.shortHtml+'<li> Date : '+element.created_at+'</li><li> Active User : '+element.payment_sgd_count+'</li><li> Version : '+$scope.sgdIndex+'</li></ul>';
                });
                $('#historySgdDetails_'+category_id).html($scope.sgdHtml);
            } else {
                simpleAlert('error','',response.data.message);
            }
        });
    }

    $scope.addOrEditBundleCategoryId = function (category_id, category_name = null) {
        if(category_name==null)
            $(".bundle_category_id_"+category_id).val('');
        adminignitefactory.addOrEditBundleCategoryId().then((response) => {
            if(response.data.code == 200 && response.data.status == 1) {
                if($(".bundle_category_id_"+category_id).val() == '')
                    $(".bundle_category_id_"+category_id).val(response.data.data);
                /*if(category_name !== null) {
                    category_name = category_name.replace(/\s/g, '_');
                    $("#bundle_category_id_"+category_id).val(response.data.data+category_name);
                } else {
                    category_name = $("#category_name_"+category_id).val().replace(/\s/g, '_');
                    $("#bundle_category_id_"+category_id).val(response.data.data+category_name);
                }*/
            }
        });
    }

    $scope.Addnewzone = function(){
        $scope.nextFormNumber = $scope.forms[$scope.forms.length-1]+1;
        if(!isNaN($scope.nextFormNumber)){
            console.log($scope.nextFormNumber);
            var addFormNumber = $scope.nextFormNumber++;
            $scope.forms.push(addFormNumber);
            $timeout(function () {
                angular.element("#category_div_"+addFormNumber).scope().category_count = [{'id':addFormNumber,'subcategories':[1]}];
            }, 500);
        } else {
            if($scope.zoneExists){
                $scope.nextFormNumber = $scope.zones.length+1;
            } else {
                $scope.nextFormNumber = 1;
            }
            $scope.forms.push($scope.nextFormNumber);
            $timeout(function () {
                angular.element("#category_div_"+$scope.nextFormNumber).scope().category_count = [{'id':$scope.nextFormNumber,'subcategories':[1]}];
            }, 500);
        }
    }
    $scope.Addnewcategory = function(form,whatType, index){
        if(whatType==0){
            allCategory = angular.element("#category_div_"+form).scope().category_count;
            if(allCategory.length>0){
                previousCategory = angular.element("#category_div_"+form).scope().category_count[0];
                previousCategoryId = previousCategory.id*parseInt(Math.random()*10);
            } else {
                previousCategoryId = parseInt(Math.random()*100);
            }
            newCategory = {'id':previousCategoryId+'_subcat','subcategories':[1]};
            var abc = angular.element("#category_div_"+form).scope().category_count.push(newCategory);
        } else {
            allCategory = angular.element("#category_div_"+index).scope().zone.categories;
            if(allCategory.length>0){
                previousCategory = angular.element("#category_div_"+index).scope().zone.categories[0];
                previousCategoryId = previousCategory.id*parseInt(Math.random()*10);
            } else {
                previousCategoryId = parseInt(Math.random()*100);
            }
            newCategory = {'id':previousCategoryId+'_subcat','subcategories':[1]};
            var abc = angular.element("#category_div_"+index).scope().zone.categories.push(newCategory);
        }
    }
    $scope.Addsubcategory = function(subjectid,categoryid,whatType){
        if(whatType==1)
            divId = '#subcategory_div_';
        else
            divId = '#new_subcategory_div_';
        if(whatType==1){
            var length = angular.element(divId+categoryid).scope().category.subcategories.length;
            allSubCategory = angular.element(divId+categoryid).scope().category.subcategories;
            allSubCategory.forEach(function(element) {
                if(length+1==element)
                    length++
            });
            var abc = angular.element(divId+categoryid).scope().category.subcategories.push(length+1);
        } else {
            categoryid = categoryid.id;
            var length = angular.element(divId+categoryid).scope().category.subcategories.length;
            allSubCategory = angular.element(divId+categoryid).scope().category.subcategories;
            allSubCategory.forEach(function(element) {
                if(length+1==element)
                    length++
            });
            var abc = angular.element(divId+categoryid).scope().category.subcategories.push(length+1);
        }
    }
    $scope.Removesubcategory = function(subjectid,categoryid,whatType,index){
        if(whatType==1)
            divId = '#subcategory_div_';
        else
            divId = '#new_subcategory_div_';
        subcategories = angular.element(divId+categoryid).scope().category.subcategories;
        subcategories.splice(index,1);
        $timeout(function(){
            if(subcategories.length==0){
                angular.element(divId+categoryid).scope().category.subcategories = [{'id':1,'subcategories':[1]}];
            }
        },500);
    }
    
    $scope.zoneExists = false;
    $scope.getCategories = function(){
        adminignitefactory.Getcategories().then(function (response) {
            if (response.data.code == 2) window.location.replace("manage/login");
            if (response.data.status == 1) {
                $scope.zones = response.data.data.categories;
                if($scope.zones.length==0){
                    $scope.forms = [1];
                    $timeout(function(){
                        angular.element("#category_div_"+1).scope().category_count = [{'id':1,'subcategories':[1]}];
                    },1500);
                } else {
                    $scope.zoneExists = true;
                    var formCount = $scope.zones.length+1;
                    $scope.forms = [];
                    $scope.forms.push(formCount);
                    $timeout(function(){
                        $scope.zones.forEach(function(element, index) {
                            element.saveDisable = 1;
                            element.audio_language    = element.audio_language.split(',');
                            element.subtitle_language = element.subtitle_language.split(',');
                            $('#audio_language_'+index).val(element.audio_language).trigger('change');
                            $('#subtitle_language_'+index).val(element.subtitle_language).trigger('change');
                            element.zone = element.zones.split(',');
                            element.zone.forEach(function(toInt){
                                $('#zone_'+element.id+'_'+parseInt(toInt)).attr('checked', 'checked');
                            });
                            element.categories.forEach(function(elementInner) {
                                if(elementInner.subcategories.length==0){
                                    angular.element("#subcategory_div_"+elementInner.id).scope().category.subcategories = [{'id':'','subcategories':[]}];
                                }
                            });
                        });
                        angular.element("#category_div_"+formCount).scope().category_count = [{'id':1,'subcategories':[1]}];
                    },1500);
                }
            }
        });
    }
    $scope.getCategories();  
    $scope.Savenewcategory = function(formNumber,whatType,zoneEdit='',index){
        if(whatType == 1) {
            var zoneForm = new FormData($("#zoneform_"+index)[0]);
        } else {
            var zoneForm = new FormData($("#zoneform_"+formNumber)[0]);
        }
        zoneForm.append('zoneNumber',formNumber);
        zoneForm.append('whatType',whatType);
        adminignitefactory.Savenewcategory(zoneForm).then(function (response) {
            if (response.data.code == 2) window.location.replace("manage/login");
            if (response.data.status == 1) {
                $scope.getCategories();
                if(whatType==1)
                    $scope.disables(zoneEdit);
            } else {
                simpleAlert('error', '', response.data.message);
            }
        });
    }
    $scope.Edit = function(zone,whatType,index){
        $('#saveZone_'+zone.id).removeAttr('disabled');
        $('#zone_name_'+zone.id).removeAttr('disabled');
        $('#country_zone_'+zone.id).removeAttr('disabled');
        $('#audio_language_'+zone.id).removeAttr('disabled');
        $('#subtitle_language_'+zone.id).removeAttr('disabled');
        $('#cancelZone_'+zone.id).removeClass('hide');
        $('#editZone_'+zone.id).addClass('hide');
    }
    $scope.Cancel = function(zone,whatType,index){
        $scope.getCategories();
        $scope.disables(zone);
    }
    $scope.disables = function(zone){
        $('#saveZone_'+zone.id).attr('disabled','disabled');
        $('#zone_name_'+zone.id).attr('disabled','disabled');
        $('#country_zone_'+zone.id).attr('disabled','disabled');
        $('#audio_language_'+zone.id).attr('disabled','disabled');
        $('#subtitle_language_'+zone.id).attr('disabled','disabled');
        $('#cancelZone_'+zone.id).addClass('hide');
        $('#editZone_'+zone.id).removeClass('hide');
    }
    function deleteZone(){
        adminignitefactory.Deletesubject({'subject_id':$scope.deleteId}).then(function (response) {
                if (response.data.code == 2) window.location.replace("manage/login");
                if (response.data.status == 1) {
                    $scope.getCategories();
                }
            }); 
    }
    $scope.Deletezone = function(id,whatType){
        if(whatType==1) {
            $scope.deleteId = id;
            confirmDialogue('Are you sure you want to delete?','','Cancel',deleteZone);
        } else {
            $scope.forms.splice($scope.forms.indexOf(id),1);
        }
    }
    function deleteCategory(){
        categoryArray = angular.element("#category_div_"+$scope.parentIndex).scope().zone;
        categoryArray = categoryArray.categories;
        categoryArray.splice($scope.deleteCategoryId,1);
        $scope.$apply();
    }
    $scope.Deletecategory = function(subjectid,categoryid,whatType,index,parentIndex){
        if(whatType==1) {
            $scope.parentIndex = parentIndex;
            $scope.deleteCategoryId = index;
            $scope.deletefromSubject = subjectid;
            confirmDialogue('Are you sure you want to delete?','','Cancel',deleteCategory);
        } else {
            var categoryArray = angular.element("#category_div_"+subjectid).scope().category_count;
            categoryArray.splice(categoryArray.indexOf(categoryid),1);
        }
    }

    adminignitefactory.Getlanguagesforzones().then(function (response) {
        if (response.data.code == 2) window.location.replace("manage/login");
        if (response.data.status == 1) {
            $scope.languages = response.data.data;
        }
    });

    adminignitefactory.Getzones().then(function (response) {
        if (response.data.code == 2) window.location.replace("manage/login");
        if (response.data.status == 1) {
            $scope.zoneList = response.data.data.zones;
        }
    });

    $scope.decimalPlaces = function(id, value) {
        var FullId  = value+id;
        var value   = $('#'+FullId).val();
        // var data    = Number.parseFloat(value).toFixed(2);
        // $('#'+FullId).val(data);
        var regex   =/^[0-9]+(\.[0-9]{0,2})?$/;
        var data    = regex.test(value);
        if(data){
            $('#'+FullId).val(value);
        } else {
            value = value.slice(0, -1); 
            $('#'+FullId).val(value);
        }
    }

}]);