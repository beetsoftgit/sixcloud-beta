/*
 Author  : Karan Kantesariya (karan@creolestudios.com)
 Date    : 28th July 2020
 Name    : TwinkleCategoryManagerController
 Purpose : realted to creating/updating categories
 */
angular.module('sixcloudAdminApp').controller('TwinkleCategoryManagerController', [ '$http', 'adminignitefactory', '$scope', '$timeout', '$rootScope', '$filter', function( $http, adminignitefactory, $scope, $timeout, $rootScope, $filter) {
    console.log('TwinkleCategoryManager controller loaded');
    $(".navbar-nav li").removeClass("active");
    $('#adminHeaderIgnite').addClass("active"); 
    $timeout(function () {
        $(".ignite-header li").removeClass("active");
        $('#twinkle_category_manger').addClass("active");
        $('.select2-container').css('display','unset');
    }, 1500);

    $scope.openDateSelector = function() {
        $('.datetimepicker').datetimepicker({
            minDate: new Date(),
        });  
    }

    $scope.openImageSelector = function(category_id, prependString,parentIndex) {
        if(parentIndex!=undefined){
            console.log("#"+prependString+category_id+'_'+parentIndex,'here');
            $scope.parentIndex = parentIndex;    
            $scope.categoryidused = category_id;
            $("#"+prependString+parentIndex+'_'+category_id).click();

            $("#"+prependString+parentIndex+'_'+$scope.categoryidused).change(function () {
                if(event.target.files.length>0) {
                    if(event.target.files[0].type == 'image/svg+xml' || event.target.files[0].type == 'image/gif' || event.target.files[0].type == 'image/jpeg' || event.target.files[0].type == 'image/png'||event.target.files[0].type == 'image/jpg') {
                        readURL(this, prependString);
                    } else {
                        simpleAlert('error', '', 'Only GIF, JPEG, PNG & JPG file formats are allowed for Subject Image / Category GIF');
                    }
                } else {
                    $('#'+prependString+parentIndex+'_'+$scope.categoryidused+'_select').css('background-image', "url(resources/assets/images/up-img.png)");
                    $('#'+prependString+parentIndex+'_'+$scope.categoryidused+'_select').removeClass('quiz_image_option');
                    $('#'+prependString+parentIndex+'_'+$scope.categoryidused+'_select').val('');
                }
            });
        }else{
            $scope.categoryidused = category_id;
            $("#"+prependString+category_id).click();

            $("#"+prependString+$scope.categoryidused).change(function () {
                if(event.target.files.length>0) {
                    if(event.target.files[0].type == 'image/svg+xml' || event.target.files[0].type == 'image/gif' || event.target.files[0].type == 'image/jpeg' || event.target.files[0].type == 'image/png'||event.target.files[0].type == 'image/jpg') {
                        readURL(this, prependString);
                    } else {
                        simpleAlert('error', '', 'Only GIF, JPEG, PNG & JPG file formats are allowed for Subject Image / Category GIF');
                    }
                } else {
                    $('#'+prependString+$scope.categoryidused+'_select').css('background-image', "url(resources/assets/images/up-img.png)");
                    $('#'+prependString+$scope.categoryidused+'_select').removeClass('quiz_image_option');
                    $('#'+prependString+$scope.categoryidused+'_select').val('');
                }
            });
        }
        
        // $("#"+prependString+$scope.categoryidused).change(function () {
        //     if(event.target.files.length>0) {
        //         if(event.target.files[0].type == 'image/gif' || event.target.files[0].type == 'image/jpeg' || event.target.files[0].type == 'image/png'||event.target.files[0].type == 'image/jpg') {
        //             readURL(this, prependString);
        //         } else {
        //             simpleAlert('error', '', 'Only GIF, JPEG, PNG & JPG file formats are allowed for Subject Image / Category GIF');
        //         }
        //     } else {
        //         $('#'+prependString+$scope.categoryidused+'_select').css('background-image', "url(resources/assets/images/up-img.png)");
        //         $('#'+prependString+$scope.categoryidused+'_select').removeClass('quiz_image_option');
        //         $('#'+prependString+$scope.categoryidused+'_select').val('');
        //     }
        // });
    }

    $scope.checkOnLanding = function(value){
        if(value){
            $scope.onLandingPage = 1;
        }else{
            $scope.onLandingPage = 0;
        }
    }

    $scope.checkonDetail = function(value){
        if(value){
            $scope.onDetailPage = 1;
        }else{
            $scope.onDetailPage = 0;
        }
    }

    function readURL(input, prependString) {
        if (input.files && input.files[0]&&input.files.length>0) {
            setTimeout(function() {
                var reader = new FileReader();
                reader.onload = function(e) {
                    
                    if($scope.parentIndex!=undefined){
                        $('#'+prependString+$scope.parentIndex+'_'+$scope.categoryidused+'_select').css('background-image', "url("+e.target.result+")");
                        $('#'+prependString+$scope.parentIndex+'_'+$scope.categoryidused+'_select').addClass('quiz_image_option');
                    }else{
                        $('#'+prependString+$scope.categoryidused+'_select').css('background-image', "url("+e.target.result+")");
                        $('#'+prependString+$scope.categoryidused+'_select').addClass('quiz_image_option');
                    }
                }
                reader.readAsDataURL(input.files[0]);
            }, 1000);
        } else {
            $('#'+prependString+$scope.categoryidused+'_select').css('background-image', "url(resources/assets/images/up-img.png)");
            $('#'+prependString+$scope.categoryidused+'_select').removeClass('quiz_image_option');
            $('#'+prependString+$scope.categoryidused+'_select').val('');
        }
    }

    /*$scope.generateBundleIdentifier = function(category_id){
        adminignitefactory.addOrEditBundleCategoryId().then((response) => {
            if(response.data.code == 200 && response.data.status == 1) {
                $(".bundle_category_id_"+category_id).val('');
                $(".bundle_category_id_"+category_id).val(response.data.data);
                console.log($(".bundle_category_id_"+category_id).val())
            }
        });
    }*/

    $scope.addOrEditBundleCategoryId = function (category_id, category_name = null) {
        if(category_name==null)
            $(".bundle_category_id_"+category_id).val('');
        adminignitefactory.addOrEditBundleCategoryId().then((response) => {
            if(response.data.code == 200 && response.data.status == 1) {
                if($(".bundle_category_id_"+category_id).val() == '')
                    $(".bundle_category_id_"+category_id).val(response.data.data);
                /*if(category_name !== null) {
                    category_name = category_name.replace(/\s/g, '_');
                    $("#bundle_category_id_"+category_id).val(response.data.data+category_name);
                } else {
                    category_name = $("#category_name_"+category_id).val().replace(/\s/g, '_');
                    $("#bundle_category_id_"+category_id).val(response.data.data+category_name);
                }*/
            }
        });
    }

    $scope.Addnewzone = function(){
        $scope.nextFormNumber = $scope.forms[$scope.forms.length-1]+1;
        if(!isNaN($scope.nextFormNumber)){
            console.log($scope.nextFormNumber);
            var addFormNumber = $scope.nextFormNumber++;
            $scope.forms.push(addFormNumber);
            $timeout(function () {
                angular.element("#category_div_"+addFormNumber).scope().category_count = [{'id':addFormNumber,'subcategories':[1]}];
            }, 500);
        } else {
            if($scope.zoneExists){
                $scope.nextFormNumber = $scope.zones.length+1;
            } else {
                $scope.nextFormNumber = 1;
            }
            $scope.forms.push($scope.nextFormNumber);
            $timeout(function () {
                angular.element("#category_div_"+$scope.nextFormNumber).scope().category_count = [{'id':$scope.nextFormNumber,'subcategories':[1]}];
            }, 500);
        }
    }
    $scope.Addnewcategory = function(form,whatType, index){
        if(whatType==0){
            allCategory = angular.element("#category_div_"+form).scope().category_count;
            if(allCategory.length>0){
                previousCategory = angular.element("#category_div_"+form).scope().category_count[0];
                previousCategoryId = previousCategory.id*parseInt(Math.random()*10);
            } else {
                previousCategoryId = parseInt(Math.random()*100);
            }
            newCategory = {'id':previousCategoryId+'_subcat','subcategories':[1]};
            var abc = angular.element("#category_div_"+form).scope().category_count.push(newCategory);
        } else {
            allCategory = angular.element("#category_div_"+index).scope().zone.categories;
            if(allCategory.length>0){
                previousCategory = angular.element("#category_div_"+index).scope().zone.categories[0];
                previousCategoryId = previousCategory.id*parseInt(Math.random()*10);
            } else {
                previousCategoryId = parseInt(Math.random()*100);
            }
            newCategory = {'id':previousCategoryId+'_subcat','subcategories':[1]};
            var abc = angular.element("#category_div_"+index).scope().zone.categories.push(newCategory);
        }
    }
    // $scope.Addsubcategory = function(subjectid,categoryid,whatType){
    //     if(whatType==1)
    //         divId = '#subcategory_div_';
    //     else
    //         divId = '#new_subcategory_div_';
    //     if(whatType==1){
    //         var length = angular.element(divId+categoryid).scope().category.subcategories.length;
    //         allSubCategory = angular.element(divId+categoryid).scope().category.subcategories;
    //         allSubCategory.forEach(function(element) {
    //             if(length+1==element)
    //                 length++
    //         });
    //         var abc = angular.element(divId+categoryid).scope().category.subcategories.push(length+1);
    //     } else {
    //         categoryid = categoryid.id;
    //         var length = angular.element(divId+categoryid).scope().category.subcategories.length;
    //         allSubCategory = angular.element(divId+categoryid).scope().category.subcategories;
    //         allSubCategory.forEach(function(element) {
    //             if(length+1==element)
    //                 length++
    //         });
    //         var abc = angular.element(divId+categoryid).scope().category.subcategories.push(length+1);
    //     }
    // }
    // $scope.Removesubcategory = function(subjectid,categoryid,whatType,index){
    //     if(whatType==1)
    //         divId = '#subcategory_div_';
    //     else
    //         divId = '#new_subcategory_div_';
    //     subcategories = angular.element(divId+categoryid).scope().category.subcategories;
    //     subcategories.splice(index,1);
    //     $timeout(function(){
    //         if(subcategories.length==0){
    //             angular.element(divId+categoryid).scope().category.subcategories = [{'id':1,'subcategories':[1]}];
    //         }
    //     },500);
    // }
    
    $scope.zoneExists = false;
    $scope.getCategories = function(){
        adminignitefactory.GetTwinklecategory().then(function (response) {
            if (response.data.code == 2) window.location.replace("manage/login");
            if (response.data.status == 1) {
                $scope.zones = response.data.data.categories;
                $scope.categoryName = [];
                $timeout(function(){
                    $scope.zones.forEach(function(element, index) {
                        element.categories.forEach(function(elementInner) {
                            $scope.categoryName.push(elementInner); 
                        });
                    });
                },1500);
                if($scope.zones.length==0){
                    $scope.forms = [1];
                    $timeout(function(){
                        angular.element("#category_div_"+1).scope().category_count = [{'id':1,'subcategories':[1]}];
                    },1500);
                } else {
                    $scope.zoneExists = true;
                    var formCount = $scope.zones.length+1;
                    $scope.forms = [];
                    $scope.forms.push(formCount);
                    $timeout(function(){
                        $scope.zones.forEach(function(element, index) {
                            element.saveDisable = 1;
                            // element.audio_language    = element.audio_language.split(',');
                            // element.subtitle_language = element.subtitle_language.split(',');
                            // $('#audio_language_'+index).val(element.audio_language).trigger('change');
                            // $('#subtitle_language_'+index).val(element.subtitle_language).trigger('change');
                            element.zone = element.zones.split(',');
                            element.zone.forEach(function(toInt){
                                $('#zone_'+element.id+'_'+parseInt(toInt)).attr('checked', 'checked');
                            });
                            element.categories.forEach(function(elementInner) {
                                if(elementInner.subcategories.length==0){
                                    $scope.start_date = elementInner.start_date;
                                    $scope.end_date = elementInner.end_date;
                                    // angular.element("#subcategory_div_"+elementInner.id).scope().category.subcategories = [{'id':'','subcategories':[]}];
                                }
                            });
                        });
                        angular.element("#category_div_"+formCount).scope().category_count = [{'id':1,'subcategories':[1]}];
                    },1500);
                }
            }
        });
    }
    $scope.getCategories();  
    $scope.Savenewcategory = function(formNumber,whatType,zoneEdit='',index){
        if(whatType == 1) {
            var zoneForm = new FormData($("#zoneform_"+index)[0]);
        } else {
            var zoneForm = new FormData($("#zoneform_"+formNumber)[0]);
        }
        zoneForm.append('zoneNumber',formNumber);
        zoneForm.append('whatType',whatType);
        adminignitefactory.SavenewTwinklecategory(zoneForm).then(function (response) {
            if (response.data.code == 2) window.location.replace("manage/login");
            if (response.data.status == 1) {
                $scope.getCategories();
                if(whatType==1)
                    $scope.disables(zoneEdit);
            } else {
                simpleAlert('error', '', response.data.message);
            }
        });
    }
    $scope.Edit = function(zone,whatType,index){
        $('#saveZone_'+zone.id).removeAttr('disabled');
        $('#zone_name_'+zone.id).removeAttr('disabled');
        $('#country_zone_'+zone.id).removeAttr('disabled');
        $('#audio_language_'+zone.id).removeAttr('disabled');
        $('#subtitle_language_'+zone.id).removeAttr('disabled');
        $('#cancelZone_'+zone.id).removeClass('hide');
        $('#editZone_'+zone.id).addClass('hide');
    }
    $scope.Cancel = function(zone,whatType,index){
        $scope.getCategories();
        $scope.disables(zone);
    }
    $scope.disables = function(zone){
        $('#saveZone_'+zone.id).attr('disabled','disabled');
        $('#zone_name_'+zone.id).attr('disabled','disabled');
        $('#country_zone_'+zone.id).attr('disabled','disabled');
        $('#audio_language_'+zone.id).attr('disabled','disabled');
        $('#subtitle_language_'+zone.id).attr('disabled','disabled');
        $('#cancelZone_'+zone.id).addClass('hide');
        $('#editZone_'+zone.id).removeClass('hide');
    }
    function deleteZone(){
        adminignitefactory.DeleteTwinklesubject({'subject_id':$scope.deleteId}).then(function (response) {
                if (response.data.code == 2) window.location.replace("manage/login");
                if (response.data.status == 1) {
                    $scope.getCategories();
                }
            }); 
    }
    $scope.Deletezone = function(id,whatType){
        if(whatType==1) {
            $scope.deleteId = id;
            confirmDialogue('Are you sure you want to delete?','','Cancel',deleteZone);
        } else {
            $scope.forms.splice($scope.forms.indexOf(id),1);
        }
    }
    function deleteCategory(){
        categoryArray = angular.element("#category_div_"+$scope.parentIndex).scope().zone;
        categoryArray = categoryArray.categories;
        categoryArray.splice($scope.deleteCategoryId,1);
        $scope.$apply();
    }
    $scope.Deletecategory = function(subjectid,categoryid,whatType,index,parentIndex){
        if(whatType==1) {
            $scope.parentIndex = parentIndex;
            $scope.deleteCategoryId = index;
            $scope.deletefromSubject = subjectid;
            confirmDialogue('Are you sure you want to delete?','','Cancel',deleteCategory);
        } else {
            var categoryArray = angular.element("#category_div_"+subjectid).scope().category_count;
            categoryArray.splice(categoryArray.indexOf(categoryid),1);
        }
    }

    adminignitefactory.Getlanguagesforzones().then(function (response) {
        if (response.data.code == 2) window.location.replace("manage/login");
        if (response.data.status == 1) {
            $scope.languages = response.data.data;
        }
    });

    adminignitefactory.Getzones().then(function (response) {
        if (response.data.code == 2) window.location.replace("manage/login");
        if (response.data.status == 1) {
            $scope.zoneList = response.data.data.zones;
        }
    });
}]);