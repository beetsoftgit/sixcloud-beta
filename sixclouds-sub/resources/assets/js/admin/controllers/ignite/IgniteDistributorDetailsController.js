/*
 Author  : Ketan Solanki (ketan@creolestudios.com)
 Date    : 19th July 2017
 Name    : IgniteDistributorController
 Purpose : All the functions for ignite distributor page
 */
angular.module('sixcloudAdminApp').controller('IgniteDistributorDetailsController', ['$http', '$scope', '$timeout', '$routeParams', '$rootScope', '$filter', 'adminignitefactory', function ($http, $scope, $timeout, $routeParams, $rootScope, $filter, adminignitefactory) {
    $(".navbar-nav li").removeClass("active");
    $('#adminHeaderIgnite').addClass("active");
    $timeout(function () {
        $(".ignite-header li").removeClass("active");
        $('#ignite_distributors').addClass("active");
    }, 500);
    initSample();
    $scope.params = $routeParams;
    $scope.current_domain = current_domain;
    adminignitefactory.GetDistributorDetailsData({ 'slug': $scope.params.slug }).then(function (response) {
        if (response.data.status == 1) {
            $scope.distributorDetails = response.data.data;
            $scope.GetDistributorMembers({ 'distributorId': $scope.distributorDetails.id }, 1);
            $scope.GetDistributorReferalls({ 'distributorId': $scope.distributorDetails.id }, 1);
        }
    });
    $scope.GetDistributorMembers = function (data, pageNumber) {
        adminignitefactory.GetDistributorMembers(data, pageNumber).then(function (response) {            
            if (response.data.status == 1) {
                $scope.allMembers = response.data.data.data;                
                $scope.totalMembers = response.data.data.total;
                $scope.paginate = response.data.data;
                $scope.currentMemberPaging = response.data.data.current_page;
                $scope.rangeMemberspage = _.range(1, response.data.data.last_page + 1);
            }
        });
    }
    $scope.GetDistributorReferalls = function (data, pageNumber) {
        adminignitefactory.GetDistributorReferalls(data, pageNumber).then(function (response) {
            if (response.data.status == 1) {
                $scope.allReferalls = response.data.data.data;
                $scope.totalReferalls = response.data.data.total;
                $scope.paginateReferals = response.data.data;
                $scope.currentReferallsPaging = response.data.data.current_page;
                $scope.rangeReferallspage = _.range(1, response.data.data.last_page + 1);
            }
        });
    }

    $scope.RegenerateQRCode = function(slug,whose){
     adminignitefactory.RegenerateQRCode({ 'slug':slug,'whose':whose}).then(function (response) {                 
            if (response.data.status == 1) {
                simpleAlert('success', '', response.data.message);
                if(whose=='Distributors'){
                    $scope.distributorDetails.qrcode_file_path = response.data.data.file_path;
                    $scope.distributorDetails.qrcode_file_name = response.data.data.file_name;
                } else {
                    $scope.teamMemberDetail.qrcode_file_path = response.data.data.file_path;
                    $scope.teamMemberDetail.qrcode_file_name = response.data.data.file_name;
                    $scope.allMembers.forEach(function(element) {
                        if(element.slug==slug){
                            element.qrcode_file_path = response.data.data.file_path;
                            element.qrcode_file_name = response.data.data.file_name;
                        }
                    });
                }
            }else{
                simpleAlert('error', '', response.data.message);
            }
        });   
    }
    $scope.RegenerateReferalCode = function(slug,whose){         
     adminignitefactory.RegenerateReferalCode({ 'slug':slug,'whose':whose}).then(function (response) {
            if (response.data.status == 1) {
                simpleAlert('success', '', response.data.message);
                if(whose=='Distributors'){
                    $scope.distributorDetails.referal_code = response.data.data.referal_code;
                } else {
                    $scope.teamMemberDetail.referal_code = response.data.data.referal_code;
                    $scope.allMembers.forEach(function(element) {
                        if(element.slug==slug){
                            element.referal_code = response.data.data.referal_code;
                        }
                    });
                }
            }else{
                simpleAlert('error', '', response.data.message);
            }
        });   
    }
    $scope.RegenerateUniqueLink = function(slug,whose){         
     adminignitefactory.RegenerateUniqueLink({ 'slug':slug,'whose':whose}).then(function (response) {                              
            if (response.data.status == 1) {
                simpleAlert('success', '', response.data.message);
                if(whose=='Distributors'){
                    $scope.distributorDetails.unique_link = response.data.data.unique_link;
                } else {
                    $scope.teamMemberDetail.unique_link = response.data.data.unique_link;
                }
            }else{
                simpleAlert('error', '', response.data.message);
            }
        });   
    }

    $scope.DownLoadQrCodeFile = function(fileName){        
        var filePath = BASEURL + "public/uploads/distributor_qr_codes/" + fileName;
        var http = new XMLHttpRequest();         
        http.open('HEAD', filePath, false);
        http.send();
        if (http.status == 200) {
            window.location = 'download-qrcode-file?filename=' + fileName;
        } else {
             simpleAlert('error', 'Some Error Occured', 'File Not Found', true);
        }
    }
    function suspendredirect() {
        window.location.replace(BASEURL + '/manage/ignite/distributors');
    }

    $scope.EmailDistributor = function (id) {
        $('#emailDistributorModal').modal('show');
        CKEDITOR.instances.editor;
    }
    $scope.changePassword = function (id) {        
        $('#changePasswordModal').modal('show');    
    }

    if(Cookies.get('language')=='en')
        $scope.email_subject = "You have a new message";
    else
        $scope.email_subject = "您有新信息";
    $scope.sendEmailToDistributor = function () {
        var message = CKEDITOR.instances.editor.getData();
        if ($scope.email_subject == undefined || message == "" || message == undefined) {
            if ($scope.email_subject == undefined) {
                simpleAlert('error', '', 'Subject is required.');
                return;
            }
            if (message == "" || message == undefined) {
                simpleAlert('error', '', 'Message is required.');
                return;
            }
        } else {
            var formData = new FormData($("#sendEmailForm")[0]);
            formData.append("message", CKEDITOR.instances.editor.getData());
            showLoader('.submit_bank', 'Please wait');
            adminignitefactory.sendEmailToDistributor(formData).then(function (response) {
                hideLoader(".submit_bank", 'Submit');
                if (response.data.code == 2) window.location.replace("manage/login");
                if (response.data.status == 1) {
                    $('#emailDistributorModal').modal('hide');
                    simpleAlert('success', '', response.data.message);
                } else {
                    simpleAlert('error', '', response.data.message);
                }
            });
        }
    }

    $scope.changeDistributorPassword = function () {
        var passwordLength = 0;
        if($scope.change_password != undefined){
            var passwordLength = $scope.change_password.length;    
        }
        var confirmPasswordLength = 0;
        if($scope.change_confirm_password != undefined){
            var confirmPasswordLength = $scope.change_confirm_password.length;    
        }                        
        if ($scope.change_password == undefined || $scope.change_password == "" || $scope.change_confirm_password == undefined || $scope.change_confirm_password == "" || $scope.change_password != $scope.change_confirm_password || passwordLength < 8 || confirmPasswordLength < 8) {
            if ($scope.change_password == undefined || $scope.change_password == "") {
                simpleAlert('error', '', 'Password is required.');
                return;
            }
            if (passwordLength < 8) {
                simpleAlert('error', '', 'Minimum 8 Charachters are required for Password.');
                return;
            }
            if ($scope.change_confirm_password == undefined || $scope.change_confirm_password == "") {
                simpleAlert('error', '', 'Confirm Password is required.');
                return;
            }
            if (confirmPasswordLength < 8) {
                simpleAlert('error', '', 'Minimum 8 Charachters are required for Confirm Password.');
                return;
            }
            if ($scope.change_password != $scope.change_confirm_password) {
                simpleAlert('error', '', 'Password and Confirm Password should be same.');
                return;
            }
        } else {
            var formData = new FormData($("#changePasswordForm")[0]);            
            showLoader('.change_password', 'Please wait');
            adminignitefactory.changeDistributorPassword(formData).then(function (response) {
                hideLoader(".change_password", 'Submit');
                if (response.data.code == 2) window.location.replace("manage/login");
                if (response.data.status == 1) {
                    $('#changePasswordModal').modal('hide');
                    simpleAlert('success', '', response.data.message);
                } else {
                    simpleAlert('error', '', response.data.message);
                }
            });
        }
    }

    $scope.suspendAccount = function (id) {
        adminignitefactory.CheckOutstandingCommisions({ 'distributorId': id }).then(function (response) {
            if (response.data.status == 1) {
                adminignitefactory.suspendAccount({ 'distributorId': id }).then(function (response) {
                    if (response.data.status == 1) {
                        confirmDialogueAlert("Success", response.data.message, "success", false, '#8972f0', 'Ok', '', true, '', '', suspendredirect);
                    } else {
                        simpleAlert('error', '', response.data.message);
                    }
                });
            } else {
                simpleAlert('error', '', response.data.message);
            }
        });
    }

    function suspendredirectDetail() {
        window.location.replace(BASEURL + '/manage/ignite/ignite-distributor-details/'+$scope.distributorDetails.slug);
    }

    $scope.teamAccountAction = function (memberId,actionType) {
        showLoader(".team_action_type"+memberId);  
        adminignitefactory.teamAccountAction({ 'id': memberId, 'action_type' : actionType }).then(function (response) {
            hideLoader(".team_action_type"+memberId,'Re-Activate Account');
            if (response.data.status == 1) {
                confirmDialogueAlert("Success", response.data.message, "success", false, '#8972f0', 'Ok', '', true, '', '', suspendredirectDetail);
            } else {
                simpleAlert('error', '', response.data.message);
            }
        });
            
    }

    $scope.viewQRCodeModal = function(qrfilename){
        $scope.qrfilename = qrfilename;
        $('#viewQRCode').modal('show');
    }

    $scope.Showteammemberinfo = function(memberId){
        adminignitefactory.Showteammemberinfo({'id': memberId}).then(function (response) {
            if (response.data.status == 1) {
                $scope.teamMemberDetail = response.data.data;
                $('#show_team_detail_modal').modal('show');
            } else {
                simpleAlert('error', '', response.data.message);
            }
        });
    }

}]);