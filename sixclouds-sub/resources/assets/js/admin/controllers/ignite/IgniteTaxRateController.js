/*
 Author  : Karan Kantesariya (karan@creolestudios.com)
 Date    : 13th May 2020
 Name    : IgniteTaxRateController
Purpose : All the functions for admin panel taxrate page
 */
angular.module('sixcloudAdminApp').controller('IgniteTaxRateController', ['$sce', 'adminignitefactory', '$location', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', function($sce, adminignitefactory, $location, $routeParams, $http, $scope, $timeout, $rootScope, $filter) {
    $scope.url = $location.url();
    // console.log('IgniteTaxRateController loaded');
    $(".navbar-nav li").removeClass("active");
    $('#adminHeaderIgnite').addClass("active"); 
    $timeout(function () {
        $(".ignite-header li").removeClass("active");
        $('#ignite_taxrate').addClass("active");
        $('.select2-container').css('display','unset');
    }, 1500);
    
    $( function() {
        $( "#date" ).datetimepicker({
            minDate: new Date(),
        });
    });

    $scope.sort = 'no';

    $scope.GetAllTaxRate = function (pageNumber, propertyName) {
        adminignitefactory.GetTaxrate({'page': pageNumber, 'propertyName': $scope.propertyName, 'sort' : $scope.sort}).then(function(response) {
            if(response.data.status==1){
                $scope.transactions = response.data.data.data;
                $scope.taxrate = response.data.data.latest_taxrate;
                $scope.totalTransactions = response.data.data.total;
                $scope.paginateTransactions = response.data.data;
                $scope.currentTransactionsPaging = response.data.data.current_page;
                // $scope.rangeTransactionspage = _.range(1, response.data.data.last_page + 1);
                // $scope.reverse = true;
                // default to first page
                var currentPage = $scope.currentTransactionsPaging || 1;

                // default page size is 10
                var pageSize = pageSize || 10;

                var totalPages = $scope.paginateTransactions.last_page;

                var startPage, endPage;

                if (totalPages <= 10) {
                    startPage = 1;
                    endPage = totalPages;
                } else {
                    if (currentPage <= 6) {
                        startPage = 1;
                        endPage = 10;
                    } else if (currentPage + 4 >= totalPages) {
                        startPage = totalPages - 9;
                        endPage = totalPages;
                    } else {
                        startPage = currentPage - 5;
                        endPage = currentPage + 4;
                    }
                }
                $scope.rangeTransactionspage = _.range(startPage, endPage + 1);
            }
        });
    }
    $scope.GetAllTaxRate(1);

    
    $scope.ordersort = function(propertyName) {
        $scope.sort = $scope.sort == 'yes' ? 'no' : 'yes';
        $scope.propertyName = propertyName;
        $scope.GetAllTaxRate(1, $scope.propertyName);
    };
    $scope.checkButton = false;
    $scope.getDisable = function(){
        $scope.checkButton = true;
    }

    jQuery('#gst_number').on('keydown', function(e) {
        var charCode = e.key.charCodeAt(0);
        if (!((charCode >= 0x30 && charCode <= 0x39) || (charCode >= 0x41 && charCode <= 0x7a))) {
            e.preventDefault();
        }
    });

    jQuery('#gst_number').on('keyup', function(e) {
        jQuery(this).val(jQuery(this).val().toUpperCase());
    });

    $scope.getTaxrate = function(taxrate,date,gst_number) {
        // Check Integer and Floating value type
        function isInt(n){
            var er = /^(?=.+)(?:[1-9]\d*|0)?(?:\.\d+)?$/
            return er.test(n);
        }
        var int = isInt(taxrate);
        if(int){
            if(taxrate!= undefined && taxrate!= ''){
                if(date!= undefined && date!= ''){
                    if(gst_number!= undefined && gst_number!= ''){
                        adminignitefactory.Getignitetaxrate({'tax_rate': taxrate,'date': date,'gst_number': gst_number}).then(function(response) {
                            if (response.data.code == 2) window.location.replace("manage/login");
                            if(response.data.status==1){
                                $scope.taxrateData = response.data.data;
                                $scope.GetAllTaxRate(1);
                                $scope.taxrate = undefined;
                                $scope.date     = undefined;
                                $scope.gst_number = undefined;
                            } else if (response.data.status==0) {
                                simpleAlert('error', '', response.data.message);     
                            }
                        });
                    } else {
                        simpleAlert('error', '', 'Please fill GST Number filed');
                    }
                } else {
                    simpleAlert('error', '', 'Please fill Date filed');
                }
            } else {
                simpleAlert('error', '', 'Please fill Tax Rate filed');
            }
        } else {
            simpleAlert('error', '', 'Please fill Tax Rate Real number');
        }
    }

}]);