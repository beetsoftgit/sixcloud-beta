/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 24th Dec 2018
 Name    : IgniteTransactionsListingController
Purpose : All the functions for admin panel transactions page
 */
angular.module('sixcloudAdminApp').controller('IgniteTransactionsListingController', ['$sce', 'adminignitefactory', '$location', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', function($sce, adminignitefactory, $location, $routeParams, $http, $scope, $timeout, $rootScope, $filter) {
    $scope.url = $location.url();
    $(".navbar-nav li").removeClass("active");
    $('#adminHeaderIgnite').addClass("active");
    $timeout(function(){
        $(".ignite-header li").removeClass("active");
        $('#ignite_transactions').addClass("active");
    },500);
    
    $scope.sort = 'no';

    $( function() {
        $( "#created_from" ).datetimepicker({
            pickTime: false,
            format: 'YYYY/MM/DD',
            maxDate: new Date(),
        });
        $( "#created_to" ).datetimepicker({
            pickTime: false,
            format: 'YYYY/MM/DD',
            maxDate: new Date(),
        });
    });
    $scope.search = '';

    $scope.GetAllTransactions = function (pageNumber, propertyName, created_from, created_to, invoice) {
        adminignitefactory.Getignitetransactions({'page': pageNumber, 'propertyName': $scope.propertyName, 'sort' : $scope.sort,'created_from' : created_from,'created_to' : created_to,'invoice' : invoice}).then(function(response) {
            if(response.data.status==1){
                $scope.transactions = response.data.data.data;
                $scope.totalTransactions = response.data.data.total;
                $scope.paginateTransactions = response.data.data;
                $scope.currentTransactionsPaging = response.data.data.current_page;
                // $scope.rangeTransactionspage = _.range(1, response.data.data.last_page + 1);
                // default to first page
                var currentPage = $scope.currentTransactionsPaging || 1;

                // default page size is 10
                var pageSize = pageSize || 10;

                var totalPages = $scope.paginateTransactions.last_page;

                var startPage, endPage;

                if (totalPages <= 10) {
                    startPage = 1;
                    endPage = totalPages;
                } else {
                    if (currentPage <= 6) {
                        startPage = 1;
                        endPage = 10;
                    } else if (currentPage + 4 >= totalPages) {
                        startPage = totalPages - 9;
                        endPage = totalPages;
                    } else {
                        startPage = currentPage - 5;
                        endPage = currentPage + 4;
                    }
                }
                $scope.rangeTransactionspage = _.range(startPage, endPage + 1); 
                // $scope.reverse = true;
            }
        });
    }
    $scope.GetAllTransactions(1);

    $scope.filterData = function (created_from,created_to,invoice) {
        var invoice = $('#dropdownMenuButton').val();
        $scope.GetAllTransactions(1,$scope.propertyName,created_from,created_to,invoice);
    }
    
    $scope.ordersort = function(propertyName) {
        /*$scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
        $scope.propertyName = propertyName;*/
        $scope.sort = $scope.sort == 'yes' ? 'no' : 'yes';
        $scope.propertyName = propertyName;
        $scope.GetAllTransactions(1, $scope.propertyName);
    };

    $scope.ExportPromos = function(export_type, created_from, created_to, invoice) {
        var invoice = $('#dropdownMenuButton').val();
        if(created_from != undefined && created_from != ''){
            $scope.created_from = created_from;
        } else {
            $scope.created_from = '';            
        }
        if(created_to != undefined && created_to != ''){
            $scope.created_to = created_to
        } else {
            $scope.created_to = '';            
        }
        if(invoice != undefined && invoice != ''){
            $scope.invoice = invoice;
        } else {
            $scope.invoice = '';            
        }
        if(export_type!= undefined && export_type!= ''){
            window.location = BASEURL + 'ExportTransactions?export_type=' + export_type+'&created_from='+$scope.created_from+'&created_to='+$scope.created_to+'&invoice='+$scope.invoice;
        } else {
            simpleAlert('error', '', 'Select export type');
        }
    }
    $scope.arrList = [];
    $scope.getList = function($event,value) {
        var checkbox    = $event.target;
        var action      = (checkbox.checked ? 'add' : 'remove');
        var dataGet     = $('#dropdownMenuButton').val();
        angular.forEach($scope.arrList, function(value, key) {
            var keyIndex = value.split(':');
            var keyIndex = $.trim(keyIndex[0]);
            if(valued == keyIndex){
                $scope.index = key;
            }
        });
        if(action == 'add'){
            // $.each($("input[type='checkbox']:checked"), function(){
            //     $scope.getValue = $(this).val() + ':';
            // });
            $scope.getValue = value + ':';
            if($scope.arrList.length == 0){
                $scope.arrList.push($scope.getValue);
                $('#dropdownMenuButton').val($scope.arrList.join(" , "));
            } else {
                $scope.arrList = [];
                $scope.getValues = $("#dropdownMenuButton").val();
                $scope.arrList.push($scope.getValues);
                $scope.arrList.push($scope.getValue);
                var lastChar = $scope.getValues.substr($scope.getValues.length - 1);
                if(lastChar == ','){
                    $('#dropdownMenuButton').val($scope.arrList.join(" "));
                } else {
                    $('#dropdownMenuButton').val($scope.arrList.join(" , "));
                }
            }
        } else if(action == 'remove') {
            var valued  = value;
            var valued  = $.trim(valued);
            var data    = $('#dropdownMenuButton').val();
            $scope.arrList      = []; 
            var value      = data.split(',');
            angular.forEach(value, function(value, key) {
                $scope.arrList.push($.trim(value));
            });
            angular.forEach($scope.arrList, function(value, key) {
                var keyIndex = value.split(':');
                var keyIndex = $.trim(keyIndex[0]);
                if(valued == keyIndex){
                    $scope.index = key;
                }
            });
            $scope.arrList.splice($scope.index, 1);
            $('#dropdownMenuButton').val($scope.arrList.join(" , "));
        }
    }

    $(".dropdown-menu").click(function(e){
       e.stopPropagation();
    })

    $( "#dropdownMenuButton" ).keyup(function() {
        var myString = $(this).val();
        var lastChar = myString.substr(myString.length - 1);
        if(lastChar == ','){
            $('#show').addClass('open');
        }
    });

}]);