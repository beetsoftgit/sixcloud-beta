/*
 Author  : Karan Kantesariya (karan@creolestudios.com)
 Date    : 25th May 2020
 Name    : IgniteTwinklePricingPlansController
 Purpose : All the functions for home page
 */
angular.module('sixcloudAdminApp').controller('IgniteTwinklePricingPlansController', ['adminignitefactory','$http', '$scope', '$timeout', '$rootScope', '$filter', function(adminignitefactory,$http, $scope, $timeout, $rootScope, $filter) {
    // $scope.sections = Geteltsections.data.data;
    console.log('IgniteTwinklePricingPlans controller loaded');
    $(".navbar-nav li").removeClass("active");
    $('#adminHeaderIgnite').addClass("active");
    $timeout(function(){
        $(".ignite-header li").removeClass("active");
        $('#twinkle_ignite_price_plan').addClass("active");
    },500);
    $scope.current_domain = current_domain;

    // $scope.getData = function(category){
    //     if(category == '' || category == undefined)
    //     {
    //         $scope.categoryData.description = '';
    //         $scope.categoryData.usd_price = '';
    //         $scope.categoryData.sgd_price = '';
    //         $scope.categories = '';
    //         $scope.sgd = '';
    //         $scope.usd = '';
    //     }
    //     else
    //     {   
    //         adminignitefactory.GetTwinklecategories(category).then(function (response) {
    //             if (response.data.code == 2) window.location.replace("manage/login");
    //             if (response.data.status == 1) {
    //                 $scope.categories = response.data.data.category_details;
    //                 $scope.sgd = response.data.data.sgd;
    //                 $scope.sgd_size = $scope.sgd.length;
    //                 $scope.usd = response.data.data.usd;
    //                 $scope.usd_size = $scope.usd.length;
    //                 $scope.categoryData.description = $scope.categories.description;
    //                 $scope.categoryData.usd_price = $scope.categories.usd_price;
    //                 $scope.categoryData.sgd_price = $scope.categories.sgd_price;
    //             } else {
    //                 simpleAlert('error','',response.data.message);
    //             }
    //         });
    //     }
    // }

    $scope.GetData = function(){
        adminignitefactory.GetTwinklecategories().then(function (response) {
            if (response.data.code == 2) window.location.replace("manage/login");
            if (response.data.status == 1) {
                $scope.categories = response.data.data;
            } else {
                simpleAlert('error','',response.data.message);
            }
        });
    }
    $scope.GetData();
    $scope.Loaddata = function(category){
        $scope.categoryUpdate = category;
        if(category!=''&&category!=undefined){
            category                              = JSON.parse(category); 
            $scope.categoryData                   = {};
            $scope.categoryData.description       = category.description;
            $scope.categoryData.category_price    = category.category_price;
            $scope.categoryData.usd_price         = category.usd_price;
            $scope.categoryData.sgd_price         = category.sgd_price;
            $scope.categoryData.category_id       = category.id;

            adminignitefactory.GetTwinklehistory(category.id).then(function (response) {
                if (response.data.code == 2) window.location.replace("manage/login");
                if (response.data.status == 1) {
                    $scope.sgd = response.data.data.sgd;
                    $scope.sgd_size = $scope.sgd.length;
                    $scope.usd = response.data.data.usd;
                    $scope.usd_size = $scope.usd.length;
                } else {
                    simpleAlert('error','',response.data.message);
                }
            });
        } else {
            $scope.categoryData = {};
            $scope.sgd = '';
            $scope.usd = '';
        }
    }

    $scope.changeLoad = function(category){
        category = JSON.parse(category);
        adminignitefactory.GetTwinklecategoryLoad(category.id).then(function (response) {
            if (response.data.code == 2) window.location.replace("manage/login");
            if (response.data.status == 1) {
                $scope.cat = response.data.data[0];
                $scope.categoryData.description       = $scope.cat.description;
                $scope.categoryData.category_price    = $scope.cat.category_price;
                $scope.categoryData.usd_price         = $scope.cat.usd_price;
                $scope.categoryData.sgd_price         = $scope.cat.sgd_price;
                $scope.categoryData.category_id       = $scope.cat.id;
            } else {
                simpleAlert('error','',response.data.message);
            }
        });
    }

     $scope.changeTab = function(whichTab){
        if(whichTab == 'DetailsTab'){
            $('#details').show();
            $('#history').hide();
            $('#DetailsTabButton').addClass('active');
            $('#HistoryTabButton').removeClass('active');
            $('#DetailsTabTabButton').removeClass('hidden-arrow');
            $scope.SubscriptionPlanTab = true;
            $scope.TransactionsTab = false;
            if($scope.categoryUpdate != '' && $scope.categoryUpdate != undefined)
            {
                $scope.changeLoad($scope.categoryUpdate);
            }
        } else {
            $('#details').hide();
            $('#history').show();
            $('#HistoryTabButton').addClass('active');
            $('#DetailsTabButton').removeClass('active');
            $('#HistoryTabButton').removeClass('hidden-arrow')
            $scope.TransactionsTab = true;
            $scope.SubscriptionPlanTab = false;
            $scope.Loaddata($scope.categoryUpdate);
        }
    }
    $('#details').show();
    $('#history').hide();
    $scope.changeTab('DetailsTab');

    $(document).ready(function() {
        $("#usd_price").on("keypress keyup blur", function(event) {
            //this.value = this.value.replace(/[^0-9\.]/g,'');
            $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
            $scope.project_budget_error = false;
        });
        $("#sgd_price").on("keypress keyup blur", function(event) {
            //this.value = this.value.replace(/[^0-9\.]/g,'');
            $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
            $scope.project_budget_error = false;
        });
    });

    $scope.Savecategory = function(details){
        if(category==undefined){
            simpleAlert('error', '', 'Select Category.');
            return;
        }
        if(details.description==undefined||details.description==''||details.usd_price==undefined||details.usd_price==''||details.sgd_price==undefined||details.sgd_price=='') {
            if(details.description==undefined||details.description==''){
                simpleAlert('error', '', '"Description" is required.');
                return;
            }
            if(details.usd_price==undefined||details.usd_price==''){
                simpleAlert('error', '', '"USD Price" is required.');
                return;
            }
            if(details.sgd_price==undefined||details.sgd_price==''){
                simpleAlert('error', '', '"SGD Price" is required.');
                return;
            }
        } else {
            showLoader('.saveCategory');
            adminignitefactory.SaveTwinklecategory(details).then(function (response) {
                if (response.data.code == 2) window.location.replace("manage/login");
                if (response.data.status == 1) {
                    hideLoader('.saveCategory','Save');
                    simpleAlert('success', '', response.data.message);
                } else {
                    hideLoader('.saveCategory','Save');
                    simpleAlert('error', '', response.data.message);
                }
            });
        }
    }
}]);