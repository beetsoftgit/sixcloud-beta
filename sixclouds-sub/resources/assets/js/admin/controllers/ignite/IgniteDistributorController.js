/*
 Author  : Ketan Solanki (ketan@creolestudios.com)
 Date    : 19th July 2017
 Name    : IgniteDistributorController
 Purpose : All the functions for ignite distributor page
 */
angular.module('sixcloudAdminApp').controller('IgniteDistributorController', ['$http', '$scope', '$timeout', '$rootScope', '$filter', 'adminignitefactory', function ($http, $scope, $timeout, $rootScope, $filter, adminignitefactory) {
    $(".navbar-nav li").removeClass("active");
    $('#adminHeaderIgnite').addClass("active");
    $timeout(function () {
        $(".ignite-header li").removeClass("active");
        $('#ignite_distributors').addClass("active");
    }, 500);
    $scope.sort = 'referralAsc';    
    $scope.search = '';
    $scope.filterData = function (sort,search) {
        $scope.GetallDistributorData({
            'sort': sort,            
            'search': search
        }, 1);
    }
    $scope.GetallDistributorData = function (data, pageNumber) {
        adminignitefactory.GetallDistributorData(data, pageNumber).then(function (response) {            

            if(response.data.status == 1){
                $scope.allDistributors = response.data.data.data;           
                $scope.totalDistributors = response.data.data.total;
                $scope.paginate = response.data.data;
                $scope.currentDistributorsPaging = response.data.data.current_page;
                // $scope.rangeDistributorspage = _.range(1, response.data.data.last_page + 1);
                // default to first page
                var currentPage = $scope.currentDistributorsPaging || 1;

                // default page size is 10
                var pageSize = pageSize || 10;

                var totalPages = $scope.paginate.last_page;

                var startPage, endPage;

                if (totalPages <= 10) {
                    startPage = 1;
                    endPage = totalPages;
                } else {
                    if (currentPage <= 6) {
                        startPage = 1;
                        endPage = 10;
                    } else if (currentPage + 4 >= totalPages) {
                        startPage = totalPages - 9;
                        endPage = totalPages;
                    } else {
                        startPage = currentPage - 5;
                        endPage = currentPage + 4;
                    }
                }
                $scope.rangeDistributorspage = _.range(startPage, endPage + 1);                
            }else if(response.data.status == 0){
                $scope.allDistributors = response.data.data.data;           
                $scope.totalDistributors = 0;
                $scope.paginate = {};
                $scope.currentDistributorsPaging = 0;
                $scope.rangeDistributorspage = _.range(1, 1);                
            }            
        });
    }
    $scope.GetallDistributorData({ 'sort': $scope.sort, 'search': $scope.search}, 1);     
}]);