/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 30th Nov, 2018
 Name    : IgnitePromoCodesDetailsController
 Purpose : All the functions for promocode details page
 */
angular.module('sixcloudAdminApp').controller('IgnitePromoCodesDetailsController', ['adminignitefactory','$http', '$scope', '$timeout', '$rootScope', '$filter', '$routeParams', function(adminignitefactory,$http, $scope, $timeout, $rootScope, $filter, $routeParams) {
    // $scope.sections = Geteltsections.data.data;
    console.log('IgnitePricingPlans controller loaded');
    $(".navbar-nav li").removeClass("active");
    $('#adminHeaderIgnite').addClass("active");
    $timeout(function(){
        $(".ignite-header li").removeClass("active");
        $('#ignite_promo_code').addClass("active");
    },500);
    $scope.current_domain = current_domain;
    $scope.slug = $routeParams.slug;
    $scope.getData = function(pageNumber){
	    adminignitefactory.GetPromoDetail({'slug':$scope.slug}, pageNumber).then(function (response) {
	        if (response.data.status == 1) {
	            $scope.promoContent = response.data.data.content.data;
	            $scope.totalpromo = response.data.data.content.total;
	            $scope.paginate = response.data.data.content;
	            $scope.currentPromoPaging = response.data.data.content.current_page;
	            $scope.rangePromopage = _.range(1, response.data.data.content.last_page + 1);    
	            $scope.promoCode = response.data.data.promo_code;
	            $scope.promoCodeStatus = response.data.data.promo_code_status;
	        }
	    });
    }
    $scope.getData(1);

}]);
