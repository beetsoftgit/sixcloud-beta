/*
 Author  : Karan Kantesariya (karan@creolestudios.com)
 Date    : 16th July 2020
 Name    : IgniteAddTwinkleSubscriberController
 Purpose : All the functions for adding ignite twinkle distributor
 */
angular.module('sixcloudAdminApp').controller('IgniteAddTwinkleSubscriberController', ['$http', '$scope', '$timeout', '$rootScope', '$filter', 'adminignitefactory', function ($http, $scope, $timeout, $rootScope, $filter, adminignitefactory) {
    // valid_email = /^[a-zA-Z0-9+]+\.*[a-zA-Z0-9+]+\@[a-zA-Z]+(\.[a-zA-Z]+)?$/i;   
    valid_email = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    $(".navbar-nav li").removeClass("active");
    $('#adminHeaderIgnite').addClass("active");
    $timeout(function () {
        $(".ignite-header li").removeClass("active");
        $('#twnikle_ignite_subscribers').addClass("active");
    }, 500);  
    $( function() {
        $( "#datepicker" ).datetimepicker({
            pickTime: false,
            format: 'DD/MM/YYYY',
            maxDate: new Date(),
        });
    });
    $scope.calculateAge = function(dob) {
        var date = dob;
        var datearray = date.split("/");
        var newdate = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
        var ageDifMs = Date.now() - new Date(newdate);
        var ageDate = new Date(ageDifMs);
        $scope.age = Math.abs(ageDate.getUTCFullYear() - 1970);
    }
    adminignitefactory.Getcountry().then(function (response) {
        if (response.data.status == 1) {
            $scope.countrycitydata = response.data.data;
        }
    });
    $('#country_id').on('change', function () {
        var id = $(this).val();
        adminignitefactory.Getstatesforcountry({ 'id': id }).then(function (response) {
            if (response.data.status == 1) {
                $scope.states = response.data.data;
            }
        });
    });
    $(document).on('change', '#state_id', function () {
        var id = $(this).val();
        adminignitefactory.Getcitiesforstate({ 'id': id }).then(function (response) {
            if (response.data.status == 1) {
                $scope.cities = response.data.data;
            }
        });
    });

    $scope.addNewDistributor = function () {
        if ($scope.first_name == undefined || $scope.first_name == '') {
            simpleAlert('success', '', 'First Name is required');
            return;
        }
        if ($scope.last_name == undefined || $scope.last_name == '') {
            simpleAlert('success', '', 'Last Name is required');
            return;
        }
        if ($scope.is_email == undefined) {
            simpleAlert('success', '', 'Type of registration (Email / Username) is required');
            return;
        } else if($scope.is_email == 0 && ($scope.generated_password == undefined || $scope.generated_password == '')) {
            simpleAlert('success', '', 'Password should be generated when you have entered Username');
            return;
        } else if((! valid_email.test($scope.email_address)) && ($scope.is_email == 1)) {
            simpleAlert('success', '', 'Please enter a valid Email Address');
            return;
        }
        if ($scope.email_address == undefined || $scope.email_address == '') {
            simpleAlert('success', '', 'Email Address is required');
            return;
        }
        if ($scope.country_id == undefined || $scope.country_id == '') {
            simpleAlert('success', '', 'Country is required');
            return;
        }
        newSubscriber = new FormData($("#newSubscriberForm")[0]);
        showLoader(".add-new-subscriber");
        /*if($scope.age!= undefined && $scope.age<18){
            simpleAlert('error','','"Date of Birth" should be greater than 18 years.');
            hideLoader('.submit_call', 'Save');
            return;
        }*/
        adminignitefactory.addNewTwinkleSubscriber(newSubscriber).then(function (response) {                
            hideLoader(".add-new-subscriber", 'Submit');
            if (response.data.code == 2) window.location.replace("manage/login");
            if (response.data.status == 1) {
                window.location.replace("manage/ignite/twinkle-subscribers");
            } else {
                simpleAlert('error','',response.data.message);
            }
        });
    }

    $scope.generatePassword = function(){
        $('#dice_shake').addClass('shake_custom');
        adminignitefactory.generatePassword().then(function (response) {
            $('#dice_shake').removeClass('shake_custom');
            if (response.data.code == 2) window.location.replace("manage/login");
            if (response.data.status == 1) {
                $scope.generated_password = response.data.data;
            } else {
                simpleAlert('error','',response.data.message);
            }
        });
    }

    $scope.changeLabel = function (type) {
        if(type == 1) {
            $('#email_address').attr('type','email');
            $('#email_label').html("Email Address");
        } else if(type == 0) {
            $('#email_address').attr('type','text');
            $('#email_label').html("Username");
        } else {
            $('#email_label').html("Email Address OR Username");
        }
    }
}]);
