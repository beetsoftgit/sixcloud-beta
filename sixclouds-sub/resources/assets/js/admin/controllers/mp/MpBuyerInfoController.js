/*
 Author  : Ketan Solanki (ketan@creolestudios.com)
 Date    : 23rd july 2018
 Name    : MpBuyerInfoController
 Purpose : View designer profile page
 */
angular.module('sixcloudAdminApp').controller('MpBuyerInfoController', [ '$scope','adminmpfactory','$routeParams','$timeout','$sce', function ( $scope,adminmpfactory,$routeParams,$timeout,$sce) {
        console.log('MpBuyerInfoController loaded');
        $('.accordion-div2 h5').click(function() {
    		$(this).toggleClass('active');
           $(this).next('.inner-div-content').slideToggle();
        });
		$scope.params=$routeParams;
        adminmpfactory.Getbuyerdetail({'id':$scope.params.slug}).then(function(response) {
            if(response.data.status==1){   
                $scope.buyerDetail=response.data.data;
                $scope.trustedHtml = $sce.trustAsHtml($scope.buyerDetail.about_us);
            }
        });

		$scope.GetBuyerProjects = function(data,pageNumber) {
		    adminmpfactory.GetBuyerProjects({'id':$scope.params.slug},pageNumber).then(function(response) {
		    	if (response.data.status == 1)
		        {
		   			$scope.buyerProjectData = response.data.data.data;
		   			// console.log($scope.buyerProjectData)
		   			$scope.paginate                = response.data.data;
		   			$scope.projectTotal            = response.data.data.total;
		        	$scope.currentProjectPaging = response.data.data.current_page;
			        $scope.rangeProjectpage = _.range(1, response.data.data.last_page + 1);
		        }
		        else {
		        }
		    });
		}
	    $scope.GetBuyerProjects({'id':$scope.params.slug},1);

		// Designerworkhistoryandreviews

		$scope.strLimit = 100;
	    // Event trigger on click of the Show more button.
	    $scope.showMore = function(getReview) {
	        $scope.strLimit = getReview.length;
	    };
	    // Event trigger on click on the Show less button.
	    $scope.showLess = function() {
	        $scope.strLimit = 100;
	    };
        $scope.expand = function(service_id){
	        $('#service_'+service_id).toggleClass('active');
	        $('#service_'+service_id).next('.inner-div-content').slideToggle();
	    }
	    $scope.changeTab = function(evt, jobsStatus) {
		    var i, tabcontent, tablinks;
		    tabcontent = document.getElementsByClassName("tabcontent");
		    for (i = 0; i < tabcontent.length; i++) {
		        tabcontent[i].style.display = "none";
		    }
		    tablinks = document.getElementsByClassName("tablinks");
		    for (i = 0; i < tablinks.length; i++) {
		        tablinks[i].className = tablinks[i].className.replace(" active", "");
		    }
		    document.getElementById(jobsStatus).style.display = "block";
		    evt.currentTarget.className += " active";
		}

		$timeout(function () {
	        angular.element('#defaultOpen').triggerHandler('click');
	    });
        $scope.Getservices = function() {
	        adminmpfactory.Getservices({'id':$scope.params.slug}).then(function(response) {
	            if (response.data.status == 1)
	            {
	                $scope.services = response.data.data;
	            }
	        });
	    }
	    $scope.Getservices({'id':$scope.params.slug},1);

	    $scope.Getportfoliodata = function(data,pageNumber) {
		    adminmpfactory.Getportfoliodata({'id':$scope.params.slug},pageNumber).then(function(response) {
		    	if (response.data.status == 1)
		        {
		   			$scope.designerPortfolio = response.data.data.data;
		   			$scope.baseurl = BASEURL;
		   			$scope.paginatePortfolio = response.data.data;
		   			$scope.protfolioTotal=response.data.data.total;
		        	$scope.currentProfilePaging = response.data.data.current_page;
			        $scope.rangeProfilepage = _.range(1, response.data.data.last_page + 1);
		        }
		        else {
		        	$scope.protfolioTotal=response.data.data.total;
		        }
		    });
		}
	    $scope.Getportfoliodata({'id':$scope.params.slug},1);

	    $scope.returnRatingAvg = function(getRating, getID){
	        /*showing overall rating*/
	        var ratingValue = getRating, rounded = (ratingValue | 0) ;
	        var  str  = '';
	        $timeout(function() {  
	            for (var j = 0; j < 5; j++) {
	                
	              str = '<span class="star-yellow"><i class="fa ';
	              if (j < rounded) {
	                str += "fa-star";
	              } else if ((ratingValue - j) > 0 && (ratingValue - j) < 1) {
	                str += "fa-star-half-o";
	              } else {
	                str += "fa-star-o";
	              }
	              str += '" aria-hidden="true"></i></span>';
	              
	                $("#overAllRating_"+getID).append(str);
	            }
	        }, 500);
	        /*showing overall rating end*/
	    }
}]);