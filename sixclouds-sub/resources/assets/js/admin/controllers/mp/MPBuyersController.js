angular.module('sixcloudAdminApp').controller('MPBuyersController', ['$scope', 'adminmpfactory', '$timeout', function ($scope, adminmpfactory, $timeout) {
    console.log('Buyer controller loaded');
    $scope.message = 'Contact us JK. This is just a demo.';
    $scope.sort = 'asc';
    $scope.spendings = '';
    $scope.search = '';
    $timeout(function () {
        $('.admin-header').find('li').removeClass('active');
        $('#buyers').addClass('active');
    }, 500);
    $scope.filterData = function (sort,spendings,search) {
        $scope.Getallbuyerdata({
            'sort': sort,
            'spendings':spendings,
            'search': search
        }, 1);
    }
    $scope.Getallbuyerdata = function (data, pageNumber) {
        adminmpfactory.Getallbuyerdata(data, pageNumber).then(function (response) {
            $scope.allBuyers = response.data.data.data;
            $scope.totalBuyer = response.data.data.total;
            $scope.paginate = response.data.data;
            $scope.currentBuyerPaging = response.data.data.current_page;
            $scope.rangeBuyerpage = _.range(1, response.data.data.last_page + 1);
        });
    }
    $scope.Getallbuyerdata({
        'sort': $scope.sort,
        'spendings':$scope.spendings,
        'search': $scope.search
    }, 1);
    //Code done by Ketan Solanki for EXCEL and CSV EXport DATE: 28th June 2018
    $scope.Exportdata = function (Exportvalue) {
        var sort = $scope.sort;
        var search = $scope.search;
        if ($scope.sort === "") {
            var sort = "no-sort";
        }
        if ($scope.search === "") {
            var search = "no-search";
        }
        if ((Exportvalue) && Exportvalue != '' && Exportvalue != undefined) {
            window.location = BASEURL + 'ExportBuyerData/' + Exportvalue + '/' + sort + '/' + search;
        } else {
            simpleAlert('error', 'Error', "Please select Export type");
        }
    };
}]);