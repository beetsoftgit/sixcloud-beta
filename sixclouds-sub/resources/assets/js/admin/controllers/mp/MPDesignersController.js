/*
 Author  : Zalak Kapadia (zalak@creolestudios.com)
 Date    : 25th June 2018
 Name    : MPDesignersController
 Purpose : To get all Designers data at admin side
 */
angular.module('sixcloudAdminApp').controller('MPDesignersController', ['$scope', 'Getallcategories', 'adminmpfactory', '$timeout', function ($scope, Getallcategories, adminmpfactory, $timeout) {
    console.log('Designers controller loaded');
    $scope.message = 'Look I am an about page.';
    //  $('body').css('background','#fff');
    $timeout(function () {
        $('.admin-header').find('li').removeClass('active');
        $('#designers').addClass('active');
    }, 500);
    $scope.availability = [{
        id: 1,
        text: 'Fulltime'
    }, {
        id: 2,
        text: 'Parttime'
    }, {
        id: 3,
        text: 'Hobbyist'
    }, {
        id: 4,
        text: 'Casual'
    }];
    $scope.ratings = [{
        id: 4,
        value: 4
    }, {
        id: 3,
        value: 3
    }, {
        id: 2,
        value: 2
    }, {
        id: 1,
        value: 1
    }];
    $scope.category = 'all';
    $scope.sort = '';
    $scope.search = '';
    $scope.proj_availability = [];
    $scope.rate = 0;
    $scope.availability_long_term = 2;
    $scope.name = '';
    $scope.review = '';
    $scope.complete_project = '';
    $scope.earnings = '';
    $scope.value = [0, 5000];
    $scope.resetData = function () {
        $scope.category = 'all';
        $scope.sort = '';
        $scope.search = '';
        $scope.rate = 0;
        $scope.availability_long_term = 2;
        $scope.name = '';
        $scope.review = '';
        $scope.complete_project = '';
        $scope.earnings = '';
        $scope.availability = [{
            id: 1,
            text: 'Fulltime'
        }, {
            id: 2,
            text: 'Parttime'
        }, {
            id: 3,
            text: 'Hobbyist'
        }, {
            id: 4,
            text: 'Casual'
        }];
        $scope.proj_availability = [];
        $('.availability-check-box').prop('checked', false);
        $('.ratings').prop('checked', false);
        $timeout(function () {
            $(".range-example-input-2").asRange('set', [0, 5000]);
        }, 500);
        $scope.value = [0, 5000];
        $scope.Getalldesignerdata({ 'category': $scope.category, "sort": $scope.sort, 'search': $scope.search, 'proj_availability': $scope.proj_availability, 'rate': $scope.rate, 'availability_long_term': $scope.availability_long_term, 'budget': $scope.value, 'name': $scope.name, 'review': $scope.review, 'complete_project': $scope.complete_project, 'earnings': $scope.earnings }, 1)

    }
    $scope.categories = Getallcategories.data.data;
    $(".range-example-input-2").asRange({
        range: true,
        limit: false,
    });
    $(".range-example-input-2").on('asRange::change', function (e) {
        $scope.value = $(".range-example-input-2").asRange('val');
    });
    $scope.filterData = function (category = 'all', sort = 'asc', search = '', rate, availability_long_term = 2, name = '', review = '', complete_project = '', earnings = '') {
        $scope.rate = $('input[name=rate]:checked').val();
        $scope.name = name;
        $scope.review = review;
        $scope.complete_project = complete_project;
        $scope.earnings = earnings;
        $scope.proj_availability = [];
        for (var i = 0; i < $scope.availability.length; i++) {
            if ($scope.availability[i].Selected) {
                var id = $scope.availability[i].id;
                $scope.proj_availability.push(id);
            }
        }
        $scope.Getalldesignerdata({
            'category': category,
            "sort": sort,
            'search': search,
            'proj_availability': $scope.proj_availability,
            'rate': $scope.rate,
            'availability_long_term': availability_long_term,
            'budget': $scope.value,
            'name': name,
            'review': review,
            'complete_project': complete_project,
            'earnings': earnings
        }, 1);
    }
    $scope.Getalldesignerdata = function (data, pageNumber) {
        data['budget'] = $scope.value;
        data['proj_availability'] = $scope.proj_availability;
        data['rate'] = $scope.rate;
        adminmpfactory.Getalldesignerdata(data, pageNumber).then(function (response) {
            $scope.allDesigners = response.data.data.data;
            if (response.data.data.total > 0) {
                $scope.totalDesigner = response.data.data.total;
            } else {
                $scope.totalDesigner = 0;
            }
            $scope.paginate = response.data.data;
            $scope.currentDesignerPaging = response.data.data.current_page;
            if (response.data.status == 1) {
                $scope.rangeDesignerpage = _.range(1, response.data.data.last_page + 1);
            }
        });
    }
    $scope.Getalldesignerdata({
        'category': $scope.category,
        "sort": $scope.sort,
        'search': $scope.search,
        'proj_availability': $scope.proj_availability,
        'rate': $scope.rate,
        'availability_long_term': $scope.availability_long_term,
        'budget': $scope.value,
        'name': $scope.name,
        'review': $scope.review,
        'complete_project': $scope.complete_project,
        'earnings': $scope.earnings
    }, 1);
    $scope.Exportdata = function (Exportvalue) {
        var category = $scope.category;
        var sort = $scope.sort;
        var search = $scope.search;
        var proj_availability = "no-proj_availability";
        var rate = $scope.rate;
        var availability_long_term = $scope.availability_long_term;
        var budget = $scope.value;
        var name = $scope.name;
        var review = $scope.review;
        var complete_project = $scope.complete_project;
        var earnings = $scope.earnings;
        if ($scope.category === "") {
            var category = "no-category";
        }
        if ($scope.sort === "") {
            var sort = "no-sort";
        }
        if ($scope.search === "") {
            var search = "no-search";
        }
        if ($scope.proj_availability === "") {
            var proj_availability = $scope.proj_availability;
        }
        if ($scope.rate === "") {
            var rate = "no-rate";
        }
        if ($scope.availability_long_term === "") {
            var availability_long_term = "no-availability_long_term";
        }
        if ($scope.budget === "") {
            var budget = "no-budget";
        }
        if ($scope.name === "") {
            var name = "no-name";
        }
        if ($scope.review === "") {
            var review = "no-review";
        }
        if ($scope.complete_project === "") {
            var complete_project = "no-complete_project";
        }
        if ($scope.earnings === "") {
            var earnings = "no-earnings";
        }
        if ((Exportvalue) && Exportvalue != '' && Exportvalue != undefined) {
            window.location = BASEURL + 'ExportSellerData/' + Exportvalue + '/' + category + '/' + sort + '/' + search + '/' + proj_availability + '/' + rate + '/' + availability_long_term + '/' + budget + '/' + name + '/' + review + '/' + complete_project + '/' + earnings;
        } else {
            simpleAlert('error', 'Error', "Please select Export type");
        }
    };
    //Code Added by ketan Solanki for downloading ID proofs
    $scope.DownloadStudentIDProof = function (whichFile, whichClass, originalFileName, designerId) {
        $("." + whichClass + "_" + designerId).removeClass('hide');
        $("." + whichClass + "_" + designerId).text(' ');
        $("." + whichClass + "_" + designerId).append("<span class='font'><i class='fa fa-spinner fa-spin'></i> Downloading</span>");
        $("." + whichClass + "_" + designerId).attr("disabled", "disabled");
        adminmpfactory.Download({ 'file_name': whichFile }).then(function (response) {
            if (response.data.status == 1) {
                var url = response.data.data.url;
                $('#student_id_proof_dynamic').attr('href', url);
                $('#student_id_proof_dynamic').attr('target', '_self');
                $('#student_id_proof_dynamic').attr('download', originalFileName);
                $('#student_id_proof_dynamic')[0].click();
                $("." + whichClass + "_" + designerId).addClass('hide');
                $timeout(function () {
                    adminmpfactory.Unlinkdownloadedfile({ 'file_name': response.data.data.file_name }).then(function (response) {
                        if (response.data.status == 1) {
                            var file_unlinked_success = response.data.message;
                        }
                    });
                }, 1000);
            }
        });
    }
    $scope.DownloadIDProof = function (whichFile, whichClass, originalFileName, designerId) {
        $("." + whichClass + "_" + designerId).removeClass('hide');
        $("." + whichClass + "_" + designerId).text(' ');
        $("." + whichClass + "_" + designerId).append("<span class='font'><i class='fa fa-spinner fa-spin'></i> Downloading</span>");
        $("." + whichClass + "_" + designerId).attr("disabled", "disabled");
        adminmpfactory.Download({ 'file_name': whichFile }).then(function (response) {

            if (response.data.status == 1) {
                var url = response.data.data.url;

                $('#id_proof_dynamic').attr('href', url);
                $('#id_proof_dynamic').attr('target', '_self');
                $('#id_proof_dynamic').attr('download', originalFileName);
                $('#id_proof_dynamic')[0].click();                
                $("." + whichClass + "_" + designerId).addClass('hide');
                $timeout(function () {
                    adminmpfactory.Unlinkdownloadedfile({ 'file_name': response.data.data.file_name }).then(function (response) {

                        if (response.data.status == 1) {
                            var file_unlinked_success = response.data.message;
                        }
                    });
                }, 1000);
            }
        });
    }
    $scope.DownloadGurdianIDProof = function (whichFile, whichClass, originalFileName, designerId) {        
        $("." + whichClass + "_" + designerId).removeClass('hide');
        $("." + whichClass + "_" + designerId).text(' ');
        $("." + whichClass + "_" + designerId).append("<span class='font'><i class='fa fa-spinner fa-spin'></i> Downloading</span>");
        $("." + whichClass + "_" + designerId).attr("disabled", "disabled");
        adminmpfactory.Download({ 'file_name': whichFile }).then(function (response) {

            if (response.data.status == 1) {
                var url = response.data.data.url;

                $('#id_proof_dynamic').attr('href', url);
                $('#id_proof_dynamic').attr('target', '_self');
                $('#id_proof_dynamic').attr('download', originalFileName);
                $('#id_proof_dynamic')[0].click();
                $("." + whichClass + "_" + designerId).addClass('hide');
                $timeout(function () {
                    adminmpfactory.Unlinkdownloadedfile({ 'file_name': response.data.data.file_name }).then(function (response) {

                        if (response.data.status == 1) {
                            var file_unlinked_success = response.data.message;
                        }
                    });
                }, 1000);
            }
        });
    }
}]);