/*
 Author  : Zalak Kapadia (zalak@creolestudios.com)
 Date    : 30th July 2018
 Name    : MPTransactionListingController
 Purpose : To get Transaction data at admin side
 */
app.controller('MPTransactionListingController', ['$scope','adminmpfactory','$timeout', function ($scope,adminmpfactory,$timeout) {
    console.log('MPTransactionListingController loaded.');
   
   	$scope.status='';
   	$scope.amount='';
   	$scope.resetData=function(){
   		$scope.status='';
   		$scope.amount='';
    	$scope.getTransactionList({'status':$scope.status,'amount':$scope.amount},1);
   	}

    $scope.getTransactionList=function(data,pageNumber){
    	adminmpfactory.Gettransactionlist(data,pageNumber).then(function(response) {
	        if(response.data.status==1){
	        	$scope.transactionData=response.data.data.data;
	        	$scope.paginate = response.data.data;
            	$scope.currentPaging = response.data.data.current_page;
	        	$scope.rangepage = _.range(1, response.data.data.last_page + 1);
	        }else{
	        	$scope.transactionData=null;
	        	$scope.paginate = null;
	        }  
	    });
    }
    $scope.getTransactionList({'status':$scope.status,'amount':$scope.amount},1);

    $scope.filterData=function(status,amount){
    	$scope.status=status;
    	$scope.amount=amount;
    	$scope.getTransactionList({'status':status,'amount':amount},1);
    }

    $scope.doPayment=function(transaction_id,amount,user_id){
    	$scope.transaction_id=transaction_id;
    	$scope.user_id=user_id;
    	confirmDialogueToAdd('Do you want to paid '+amount+'  to user?', 'Yes', 'No',withdrawAmountPaidByAdmin,'Yes','No');
    }

    function withdrawAmountPaidByAdmin(){
    	adminmpfactory.Withdrawamountpaidbyadmin({'transaction_id':$scope.transaction_id,'user_id':$scope.user_id}).then(function(response) {
	        if(response.data.status==1){
	        	$("#status_pending").html("Paid successfully to this user <br>"+response.data.data);
	        	$timeout(function(){
	        		$scope.getTransactionList(1);
	        	},2000);
	        }
	    });
    }

    $scope.Exportdata = function (Exportvalue) {
        var status = $scope.status;
        var amount = $scope.amount;

        if ($scope.status === "") {
            var status = "no-status";
        }
        if ($scope.amount === "") {
            var amount = "no-amount";
        }
        if ((Exportvalue) && Exportvalue != '' && Exportvalue != undefined) {
            window.location = BASEURL + 'ExportTransactionData/' + Exportvalue + '/' + status + '/' + amount;
        } else {
            simpleAlert('error', 'Error', "Please select Export type");
        }
    };
	    
}]);