/*
 Author  : Komal Kapadi (komal@creolestudios.com)
 Date    : 24th May 2018
 Name    : MPCategoriesController
 Purpose : All the functions for categories page
 */
angular.module('sixcloudAdminApp').controller('MPCategoriesController', ['$scope', 'adminmpfactory','$timeout', function($scope, adminmpfactory,$timeout) {
    console.log('MPCategories Controller loaded');
    $timeout(function() {
        $('.admin-header').find('li').removeClass('active');
        $('#categories_menu').addClass('active');
    }, 500);
    $scope.displaydelete=0;
    $scope.displaydeletedinscategory=0;

    /*************************************** For Inspiration Bank  ****************************************/    
    $scope.addEditInsCategory=function(){
        $scope.status=1;
        $scope.ins_category_name_en='';
        $('#add_edit_ins_category').modal('show');
    }
    $scope.addEditInsCategoryData=function(){

        var data = new FormData($("#addEditInsCategoryForm")[0]);
        data.append("status", $scope.status);
        adminmpfactory.Addeditinscategorydata(data).then(function(response) {
            if(response.data.status==1){  
                simpleAlert('success', 'Success', response.data.message);
                $scope.Getinspirationcategorydata({'status':1},1);
                $('#add_edit_ins_category').modal('hide');
            }else{
                simpleAlert('error', 'Error', response.data.message);
            }
        });
    }
    $scope.updateInsCategory=function(category){
        $scope.status=2;
        $scope.ins_category_name_en=category.category_name_en;
        $('#add_edit_ins_category').modal('show');
        $scope.ins_cat_id=category.id;
    } 
    $scope.deleteInsCategory=function(category){
        $scope.category=category;
        adminmpfactory.Getinspirationuploadsbycategory({'cat_id':category.id}).then(function(response) {
            
            if(response.data.status==1){ 
        
                if(response.data.data>0){
                    confirmDialogueAlert('Are you sure?', 'You have '+response.data.data+' uploads in this category. So you can not delete this category.', 'error', true, '#8972f0', 'Ok', 'Cancel', true, true, '', Cancelinscategorydelete);    
                }else{
                    confirmDialogueAlert('Are you sure?', 'You have 0 uploads in this category.', 'error', true, '#8972f0', 'Ok', 'Cancel', true, true, '', Deleteinscategory);    
                }
                   
            }
        });
        
    }
    function Cancelinscategorydelete(){
        return false;
    }
    function Deleteinscategory(){
        $scope.category['new_status']=2;
        adminmpfactory.Enabledeleteinscategory($scope.category).then(function(response) {
            if(response.data.status==1){  
                simpleAlert('success', 'Success', response.data.message);
                $scope.Getinspirationcategorydata({'status':1},1);
                $('#add_edit_category').modal('hide');
            }else{
                simpleAlert('error', 'Error', response.data.message);
            }
        });
    }

    $scope.enableInsCategory=function(category){
        category['new_status']=1;
        adminmpfactory.Enabledeleteinscategory(category).then(function(response) {
            if(response.data.status==1){  
                simpleAlert('success', 'Success', response.data.message);
                $scope.displaydelete=0;
                $scope.displaydeletedinscategory=0;
                $scope.Getinspirationcategorydata({'status':1},1);
            }else{
                simpleAlert('error', 'Error', response.data.message);
            }
        });   
    }

    $scope.Getinspirationcategorydata = function(data,pageNumber) {
        
        adminmpfactory.Getinspirationcategorydata(data, pageNumber).then(function(response) {
            
            if(response.data.status==1){
                if($scope.displaydeletedinscategory==0){
                    $scope.insCategories = response.data.data.data;
                    $scope.totalInsCategory = response.data.data.total;
                    $scope.paginateIns = response.data.data;
                    $scope.currentInsPaging = response.data.data.current_page;
                    $scope.rangeInspage = _.range(1, response.data.data.last_page + 1);    
                }else{
                    $scope.insDeletedCategories = response.data.data.data;
                    $scope.totalDeletedInsCategory = response.data.data.total;
                    $scope.paginateDeletedIns = response.data.data;
                    $scope.currentDeletedInsPaging = response.data.data.current_page;
                    $scope.rangeDeletedInspage = _.range(1, response.data.data.last_page + 1);    
                }
            }
            else{
                $scope.insDeletedCategories='';
                $scope.totalDeletedInsCategory = 0;
                $scope.paginateDeletedIns = '';
                $scope.currentDeletedInsPaging = 0;
                $scope.rangeDeletedInspage = 1;   
            }

            
        });
    }
    $scope.Getinspirationcategorydata({'status':1},1);   

    $scope.showDeletedInsCategories=function(){
        $scope.displaydeletedinscategory=1;
        $scope.displaydelete=0;
        $scope.Getinspirationcategorydata({'status':2},1);   
    }

    $scope.showInsCategories =function(){
        $scope.displaydeletedinscategory=0;
        $scope.displaydelete=0;
    }

    /*************************************** For skill  ****************************************/
    
    $scope.addEditCategory=function(){
        $scope.status=1;
        $scope.category_name_en='';
        $scope.category_name_chi='';
        $('#add_edit_category').modal('show');
    }

    $scope.addEditCategoryData=function(){

        var data = new FormData($("#addEditCategoryForm")[0]);
        data.append("status", $scope.status);
        adminmpfactory.Addeditcategorydata(data).then(function(response) {
            if(response.data.status==1){  
                simpleAlert('success', 'Success', response.data.message);
                $scope.Getskillcategorydata({'status':1},1);
                $('#add_edit_category').modal('hide');
            }else{
                simpleAlert('error', 'Error', response.data.message);
            }
        });
    }    
    $scope.updateCategory=function(skill){
        $scope.status=2;
        $scope.category_name_en=skill.skill_name;
        $scope.category_name_chi=skill.skill_name_chinese;
        $('#add_edit_category').modal('show');
        $scope.skill_id=skill.id;
    }

    $scope.deleteCategory=function(skills){
        $scope.skills=skills;
        adminmpfactory.Getactiveprojectsbyskill({'skill_id':skills.id}).then(function(response) {
            if(response.data.status==1){ 
                if(response.data.data.activeProjects== 0 && response.data.data.completeProjects==0  && response.data.data.totalServices==0)    {
                    confirmDialogueAlert('Are you sure?', 'You have 0 projects and 0 services in this skill type.', 'error', true, '#8972f0', 'Ok', 'Cancel', true, true, '', Deleteskill);    
                } 
                else{
                    if(response.data.data.activeProjects>0 || response.data.data.totalServices>0){
                        confirmDialogueAlert('Are you sure?', 'You have '+response.data.data.activeProjects+' active project(s) and '+response.data.data.totalServices +' service(s) in this skill type. So you can not delete this skill.', 'error', true, '#8972f0', 'Ok', 'Cancel', true, true, '', Canceldelete);    
                    }else{
                        confirmDialogueAlert('Are you sure?', 'You have '+response.data.data.completeProjects+' completed project(s) or cancelled project(s) in this skill type.', 'error', true, '#8972f0', 'Ok', 'Cancel', true, true, '', Deleteskill);    
                    }
                }   
            }
        });
        
    }
    function Canceldelete(){
        return false;
    }
    function Deleteskill(){
        $scope.skills['new_status']=2;
        adminmpfactory.Enabledeleteskill($scope.skills).then(function(response) {
            if(response.data.status==1){  
                simpleAlert('success', 'Success', response.data.message);
                $scope.Getskillcategorydata({'status':1},1);
                $('#add_edit_category').modal('hide');
            }else{
                simpleAlert('error', 'Error', response.data.message);
            }
        });
    }

    $scope.enableCategory=function(skills){
        skills['new_status']=1;
        adminmpfactory.Enabledeleteskill(skills).then(function(response) {
            if(response.data.status==1){  
                simpleAlert('success', 'Success', response.data.message);
                $scope.displaydelete=0;
                $scope.Getskillcategorydata({'status':1},1);
            }else{
                simpleAlert('error', 'Error', response.data.message);
            }
        });   
    }

    $scope.skillCategories = '';
    $scope.Getskillcategorydata = function(data,pageNumber) {
        adminmpfactory.Getadmincategories(data, pageNumber).then(function(response) {

            if(response.data.status==1){
                if($scope.displaydelete==0){
                    
                    $scope.skillcategories = response.data.data.data;   
                    $scope.totalSkillCategory = response.data.data.total;
                    $scope.paginate = response.data.data;
                    $scope.currentSkillPaging = response.data.data.current_page;
                    $scope.rangeSkillpage = _.range(1, response.data.data.last_page + 1); 
                }else{
                    $scope.skilldeletedcategories = response.data.data.data; 
                    $scope.totalDeletedSkillCategory = response.data.data.total;
                    $scope.paginateDeleteSkill = response.data.data;
                    $scope.currentDeletedSkillPaging = response.data.data.current_page;
                    $scope.rangeDeletedSkillpage = _.range(1, response.data.data.last_page + 1);    
                }    
            }
            
            
        });
    }
    $scope.Getskillcategorydata({'status':1},1);

    $scope.showDeletedCategories =function(){
        $scope.displaydelete=1;
        $scope.displaydeletedinscategory=0;
        $scope.Getskillcategorydata({'status':2},1);
    }
    
    $scope.showCategories =function(){
        $scope.displaydelete=0;
        $scope.displaydeletedinscategory=0;
    }
}]);