/*
 Author  : Zalak Kapadia (zalak@creolestudios.com)
 Date    : 23 april 2018
 Name    : MpDashboardController
 Purpose : All the functions for rootscope
 */

/* app.controller('MPDashboardController', [ '$scope', function ( $scope) {
        console.log('dashboard controller loaded');
    }]);*/

    angular.module('sixcloudAdminApp').controller('MPDashboardController', ['$http', '$scope', 'adminmpfactory','$timeout', function($http, $scope, adminmpfactory, $timeout ) {
        console.log('dashboard controller loaded');
        $timeout(function () {
            $('.admin-header').find('li').removeClass('active');
            $('#dashboard').addClass('active');
        }, 500);
        $scope.category = 'all';
    $scope.sort = '';
    $scope.search = '';
    $scope.price = '';
    $scope.timeleft = '';
    $scope.status = [1];    
        $scope.Getalljobsdata = function(category, sort, search, price, timeleft, pageNumber, status) {
            adminmpfactory.Getalljobsdata(category, sort, search, price,timeleft, pageNumber, status).then(function(response) {
                if(response.data.status==1){
                    $scope.allJobs = response.data.data.data;
                    $scope.openLength = response.data.data.total_open_jobs;
                    $scope.activeLength = response.data.data.total_active_jobs;
                    $scope.completeLength = response.data.data.total_completed_jobs;
                    $scope.cancelledLength = response.data.data.total_cancelled_jobs;
                    $scope.escrowLength = response.data.data.total_escrow_jobs;
                    $scope.total_completed_project = response.data.data.total_completed_project;
                    $scope.total_cancelled_project = response.data.data.total_cancelled_project;
                    $scope.total_completed_project_month = response.data.data.total_completed_project_month;
                    $scope.paginate = response.data.data;
                    $scope.currentJobPaging = response.data.data.current_page;
                    $scope.rangeJobpage = _.range(1, response.data.data.last_page + 1);
                }
            });
        }
        $scope.Getalljobsdata($scope.category, $scope.sort, $scope.search, $scope.price, $scope.timeleft, 1, $scope.status);
    	
    }]);