/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 26th December 2017
 Name    : PrAllAccessorController
 Purpose : All the functions for All the Accessors
 */
angular.module('sixcloudAdminApp').controller('PrAllAccessorController', ['$http', '$scope', 'prfactory', '$timeout', '$rootScope', '$filter', function ($http, $scope, prfactory, $timeout, $rootScope, $filter) {
    console.log('PrClosedCase controller loaded');
    $timeout(function(){
        $(".navbar-nav li").removeClass("active");
        $('#adminHeaderPr').addClass("active");
        $(".menu-list li").removeClass("active");
        $('#all_accesors').addClass("active");
    },1000);
    $scope.value='all';
    $( function() {
        $( ".datepicker" ).datetimepicker({
            pickTime: false,
            format: 'DD/MM/YYYY',
            maxDate: new Date(),
        });
    });
    $scope.calculateAge = function(dob) {
        var date = dob;
        var datearray = date.split("/");
        var newdate = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
        var ageDifMs = Date.now() - new Date(newdate);
        var ageDate = new Date(ageDifMs);
        $scope.age = Math.abs(ageDate.getUTCFullYear() - 1970);
        console.log($scope.age);
    }
    $('.parent-accessor').css('display','none');
    $scope.type1 = function(level_type){
        $('.parent-accessor').css('display','inline-block');
        $scope.level_type = level_type;
    }
    $scope.type2 = function(level_type,newaccessor){
        $('.parent-accessor').css('display','none');
        if(newaccessor == 1) {
            $scope.newaccessor.accessor_id = '';
        }
        $scope.level_type = level_type;
    }
    var reportingOfficer = [];
    $scope.sort = function(sort_by){
        prfactory.Getaccessors({'sort_by':sort_by}).then(function(response){
            if(response.data.status==1)
            {
                $rootScope.accessors = response.data.data;
                $scope.accessors_count = response.data.count;
                for($i=0;$i<$scope.accessors.length;$i++ )
                    if($scope.accessors[$i].level_type == 2)
                        reportingOfficer.push($scope.accessors[$i]);
            }
        });
    }
    $scope.sort('all');
    $scope.parentAccessors = reportingOfficer;
    $scope.teams = [{'value':'Team 1','name':'Team 1'},{'value':'Team 2','name':'Team 2'},{'value':'Team 3','name':'Team 3'},{'value':'Team 4','name':'Team 4'}];
    $scope.Addaccessor = function(newaccessor){
        if($scope.age!=undefined&&$scope.age>18){   
            newaccessor['level_type'] = $scope.level_type;
            newaccessor['validation_type'] = 'new';
            $(".add-accessor").text(' ');
            $(".add-accessor").append("<i class='fa fa-spinner fa-spin'></i> Please wait");
            $(".add-accessor").attr("disabled", "disabled");
            prfactory.Addaccessor(newaccessor).then(function (response) {
                if (response.data.status == 1)
                {
                    simpleAlert('success','Success',response.data.message);
                    $('.close').click();
                    $scope.newaccessor = {};
                    reportingOfficer = [];
                    $('.parent-accessor').css('display','none');
                    $scope.sort('all');
                    $(".add-accessor").text(' ');
                    $(".add-accessor").append("<i class='sci-add'></i>Add");
                    $(".add-accessor").removeAttr("disabled");
                } else {
                    simpleAlert('error','Error',response.data.data);
                    $(".add-accessor").text(' ');
                    $(".add-accessor").append("<i class='sci-add'></i>Add");
                    $(".add-accessor").removeAttr("disabled");
                }
            });
        } else {
            simpleAlert('error', '', "Accessor's age must be above 18 years");
        }
    }
    $scope.newAccessorModal = function(){
        $scope.newaccessor = {};
        reportingOfficer = [];
        $('input[name=level_type]').prop("checked", false);
        $('.parent-accessor').css('display','none');
        $('#addNewModal').modal('show');
    }
    $(document).ready(function () {
      $("#phone_number").keypress(function (e) {
         if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
       });
    });
}]);