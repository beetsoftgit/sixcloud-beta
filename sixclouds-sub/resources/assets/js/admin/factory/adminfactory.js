/* 
 * Author: Komal Kapadi
 * Date  : 28th Dec 2017
 * Admin factory file
 */
angular.module('sixcloudAdminApp').factory('adminfactory', ['$http', '$rootScope',
    function($http, $rootScope) {
        return {
            // check if user logged in or not
            Userdata: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Userdata'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Pruserdata: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Pruserdata'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Editadminprofile: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Editadminprofile',
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Adminlisting: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Adminlisting',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Addadmin: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Addadmin',
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Deleteadmin: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Deleteadmin',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getreportingofficers: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getreportingofficers',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Changepassword: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Changepassword',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getcountry: function(data) {
                return $http({
                    method: 'GET',
                    data: data,
                    url: 'Getcountry'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getstatesforcountry: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getstatesforcountry'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getcitiesforstate: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getcitiesforstate'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
        };
    }
]);