/* 
 * Author: Komal Kapadi
 * Date  : 4th Dec 2017
 * user factory file
 */
angular.module('sixcloudAdminApp').factory('userfactory', ['$http', '$rootScope',
    function ($http, $rootScope) {
        return {
            Getcases: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getcases'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            /*check if user logged in or not*/
            Getloginuserdata: function (data) {
                return $http({
                    method: 'GET',
                    data: data,
                    url: 'Getloginuserdata'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Logout: function (data) {
                return $http({
                    method: 'GET',
                    data: data,
                    url: 'Logout'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            
        };
    }
]);