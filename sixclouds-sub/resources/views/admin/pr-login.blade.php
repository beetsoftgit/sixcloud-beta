<!DOCTYPE html>
<head>
    <title>Sixclouds</title>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    {{ HTML::style('resources/assets/plugins/bootstrap/css/bootstrap.min.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/plugins/font-awesome/css/font-awesome.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/admin/default.min.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/admin/style.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/common/sweet-alert.min.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/admin/custom.css',array('rel'=>'stylesheet')) }}
    {{ HTML::script('resources/assets/js/common/jquery-3.2.1.min.js') }}
    <link rel="shortcut icon" href="resources/assets/images/Logo-large.png"/>
    <base href="<?= url('/') . '/' ?>">
</head>
<body>
    <div class="login-bg">
        <div class="login">
            <div class="logo text-center margin-bt20 margin-tp30">
                <img src="resources/assets/images/Logo-large.png">
            </div>
            <h3 class="text-black ">Log In</h3>
            <div class="form">
                <form method="post" id="prlogin-form">
                    <div class="form-group input-label">
                        <input type="text" id="email_address" name="email_address" class="form-control" placeholder="Email Address">
                    </div>
                    <div class="form-group input-label margin-bt10">
                        <input type="password" id="password" name="password" class="form-control" placeholder="Password">
                    </div>
                    <div class="forget inline-block">
                        <div class="remember pull-left">
                            <span class="checkbox-section">
                                <span class="checkbox-btn">
                                    <input name="site_visit" type="checkbox" class="">
                                    <span class="check-box">
                                    </span>
                                </span>
                            </span>
                            <span>Remember Me</span>
                        </div>
                        <div class="pull-right">
                            <a id="forgot_link" href="javascript:void(0)" class="" data-target="#forgot-pwd" data-toggle="modal">Forgot Password ?</a>
                        </div>
                    </div>
                    <div class="text-center margin-tp15">
                        <button class="btn" type="submit">Log In</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="forgot-pwd" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Forgot Password</h4>
                </div>
                <div class="modal-body form">
                    <form class="text-center forget-form" name="forget-form" method="post">
                        <div class="form-group input-label">
                            <input type="text" name="email_fp" id="email_fp" class="form-control" placeholder="Email Address">
                        </div>
                        <button type="submit" class="btn" value="Submit">
                            Submit
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Core files -->
    {{ HTML::script('resources/assets/js/admin/core/bootstrap.min.js') }}
    {{ HTML::script('resources/assets/js/common/custom_alert.js') }}
    {{ HTML::script('resources/assets/js/common/jquery.validate.min.js') }}
    {{ HTML::script('resources/assets/js/common/sweetalert.min.js') }}
    {{ HTML::script('resources/assets/js/admin/prbasic.js') }}
    <script>
        // set MODE for enable and disable console.log
            var project_mode = 'development'; //'production';
        var BASEURL = "<?= url('/') . '/' ?>";
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.login-bg').css({'height': (($(window).height())) + 'px'});
        });
    </script>
</body>
</html>