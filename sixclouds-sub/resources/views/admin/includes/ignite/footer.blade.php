<!--Footer Start-->
  <footer class="animatedParent animateOnce">
    <div class="footer-bg-repeat animated fadeIn slowest"></div>
    <div class="main-footer animated fadeIn slowest">
      <div class="container">
        <div class="row">
          <div class="col-sm-4">
            <div class="h-div">
              <div class="footer-logo"> <a href="javascript:;"><img src="../resources/assets/images/logo.png" width="173" height="70"></a> </div>
              <p>sixclouds.com</p>
              <p>(+86 12 345 6789)</p>
            </div>
            <div class="bottom-div"> <span class="copy">&copy;</span><a href="javascript:;">SixClouds</a> All rights reserved </div>
          </div>
          <div class="col-sm-4">
            <table cellpadding="0" cellspacing="0" width="100%">
              <tr>
                <td width="50%" valign="top"><div class="h-div">
                    <h3>About Us</h3>
                    <ul>
                      <li><a href="javascript:;">About SixClouds</a></li>
                      <li><a href="javascript:;">Video Lerning</a></li>
                      <li><a href="javascript:;">Market PLace</a></li>
                      <li><a href="javascript:;">Proof Reading</a></li>
                      <li><a href="javascript:;">Digital Library</a></li>
                    </ul>
                  </div>
                  <div class="bottom-div"> <a href="javascript:;"><strong>50041 Members</strong></a> </div></td>
                <td valign="top"><div class="h-div">
                    <h3>Qick Links</h3>
                    <ul>
                      <li><a href="javascript:;">How it Works</a></li>
                      <li><a href="javascript:;">Security</a></li>
                      <li><a href="javascript:;">Report Bug</a></li>
                      <li><a href="javascript:;">Pricing</a></li>
                    </ul>
                  </div>
                  <div class="bottom-div"> <a href="javascript:;"><strong>50041 Members</strong></a> </div></td>
              </tr>
            </table>
          </div>
          <div class="col-sm-4">
            <div  class="h-div">
              <h3>Subscribe Us</h3>
              <div class="sub-div">
              <form>
                <input type="text" class="form-control" placeholder="Enter your email">
                <button class="btn subscribe-btn"><i class="fa fa-search fa-w-16"></i></button>
              </form>
              </div>
              <div class="social-icon"> <a href="javascript:;"><i class="fa fa-facebook"></i></a> <a href="javascript:;"><i class="fa fa-twitter"></i></a> <a href="javascript:;"><i class="fa fa-linkedin"></i></a> </div>
            </div>
            <div class="bottom-div"> <a href="javascript:;"><strong>Term And Condition</strong></a> </div>
          </div>
        </div>
      </div>
    </div>
  </footer>