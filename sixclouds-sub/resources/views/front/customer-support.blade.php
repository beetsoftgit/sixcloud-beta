<!DOCTYPE html>
<html  ng-app="sixcloudscustomersupportApp">
<head>
  <title>{{$title}}</title>
  <meta name="description" content="{{$meta}}">
  <meta name="robots" content="index,follow" />
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1" name="viewport"/>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="alternate" href="{{$alternate}}" hreflang="{{$alternatelang}}" />
  <link rel="canonical" href="{{$canonical}}" />
  <meta name="google-site-verification" content="F0XMjfssK8hcmTCfqJJcQFq80vAe8lHHegJBFA84XAQ" />
  <meta name="msvalidate.01" content="5789F571110EF5249F1B42276844821B" />
  <meta name="p:domain_verify" content="8475f8bbc093c663df8ead38f223dfac"/>

  <!-- Google Analytics -->
  <script>
  window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
  ga('create', 'UA-149665763-1');
  ga('send', 'pageview');
  </script>
  <script async src='https://www.google-analytics.com/analytics.js'></script>
  <!-- End Google Analytics -->

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127371237-1"></script>
  <script>
   window.dataLayer = window.dataLayer || [];
   function gtag(){dataLayer.push(arguments);}
   gtag('js', new Date());

   gtag('config', 'UA-127371237-1');
  </script>

  {{ HTML::script('resources/assets/js/common/jquery.min.js') }}
  <link rel="shortcut icon" type="image/x-icon" href="resources/assets/images/favicon.ico">
  <!--Animation Style-->
  {{ HTML::style('resources/assets/css/front/mp/animations.css',array('rel'=>'stylesheet')) }}

  <!--Slider Style-->
  {{ HTML::style('resources/assets/css/front/mp/swiper.min.css',array('rel'=>'stylesheet')) }}

  <!--Fonts Style-->
  {{ HTML::style('resources/assets/css/front/mp/custom-font.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/font-awesome.min.css',array('rel'=>'stylesheet')) }}


  <!--jquery.FlowupLabels Style-->
  {{ HTML::style('resources/assets/css/front/mp/jquery.FlowupLabels.css',array('rel'=>'stylesheet')) }}

  <!--bootstrap-datetimepicker Style-->
  {{ HTML::style('resources/assets/css/front/mp/bootstrap-datetimepicker.min.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/asRange.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/common/star-rating.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/bootstrap.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/common/sweet-alert.min.css',array('rel'=>'stylesheet')) }}

  <!--Custom Style-->
  {{ HTML::style('resources/assets/css/front/mp/style.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/mp_custom.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/responsive.css',array('rel'=>'stylesheet')) }}

  {{ HTML::script('resources/assets/js/common/angular.min.js') }}
  {{ HTML::script('resources/assets/js/common/angular-route.js') }}
  {{ HTML::script('resources/assets/js/common/angular-translate.min.js') }}
  {{ HTML::script('resources/assets/js/common/js.cookie.min.js') }}

    <!--[if lte IE 9]>
          <link href='assets/css/animations-ie-fix.css' rel='stylesheet'>
    <![endif]-->

  <!-- <script src="script.js"></script> -->
</head>
<body ng-controller="CustomerSupport">
  <div class="body-div">
    <!--Header section Started-->
    <header class="header-design2 header-customer-support" data-spy="affix" >
      <div class="container">
        <nav class="navbar navbar-custom">
         <div class="container-fluid">
           <!-- Brand and toggle get grouped for better mobile display -->
           <div class="navbar-header">
             <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
             </button>
           </div>

           <!-- Collect the nav links, forms, and other content for toggling -->
           <div class="collapse navbar-collapse hMenu" id="bs-example-navbar-collapse-1">
              <!-- <ul class=" before-login nav navbar-right header-mid-btn about-us-btn">
                <li class="become-a-buyer"><a href="javascript:;" ng-if="eng == true" ng-click="changeLang('en')" class="btn  uppercase login-btn">[['lbl_lang_chng'|translate]]</a>
                  <a href="javascript:;" ng-if="!eng" ng-click="changeLang('chi')" class="btn uppercase login-btn">[['lbl_lang_chng'|translate]]</a>
                </li>
              </ul> -->
              <select ng-model="changeLangs" ng-change="changeLang(changeLangs)" class="before-login nav navbar-right header-mid-btn about-us-btn">
                <option value="chi" ng-selected="changeLangs=='chi'">中文</option>
                <option value="en" ng-selected="changeLangs=='en'">English</option>
                <option value="ru" ng-selected="changeLangs=='ru'">Русский</option>
              </select>
           </div><!-- /.navbar-collapse -->
         </div><!-- /.container-fluid -->
        </nav>

      </div>
    </header>
    <!--Header section End-->
    <div id="main">
      <section class="customer-support-section">
        <div class="container">
          <div class="cs-logo text-center"> <a href="<?=url('/')?>"><img src="resources/assets/images/logo-blue.png"></a> </div>
          <div class="head-sub-text text-center">
            <h3>[['lbl_customer_support' | translate]]</h3>
          </div>
          <div class="customer-support-form">
            <div class="flowup-labels-form">
              <form class="FlowupLabels" id="CustomerSupportForm" name="CustomerSupportForm" ng-submit="CustomerSupportForm.Createsupportticket()" novalidate="">
                <div class="form-group">
                  <div class="fl_wrap [[!user_name?user_name_error?'has-error':'':'has-success']]">
                    <label class="fl_label" for="user_name">[['lbl_customer_support_fullname' | translate]]</label>
                    <input class="fl_input form-control" id="user_name" name="user_name" ng-model="user_name" type="text"/>
                    <i ng-if="user_name" class="fa fa-check-circle out-icon"></i>
                    <i ng-if="user_name_error && !user_name" class="fa fa-exclamation-circle out-icon"  title="Your name is required"></i>
                  </div>
                </div>
                <div class="form-group">
                  <div class="fl_wrap [[!email_address?email_address_error?'has-error':'':'has-success']]">
                    <label class="fl_label" for="email_address">[['lbl_customer_support_email' | translate]]</label>
                    <input class="fl_input form-control" id="email_address" name="email_address" ng-model="email_address" type="email"/>
                    <i ng-if="email_address" class="fa fa-check-circle out-icon"></i>
                    <i ng-if="email_address_error && !email_address" class="fa fa-exclamation-circle out-icon"  title="Email address is required"></i>
                  </div>
                </div>
                <div class="row">
                   <div class="col-sm-4 col-xs-1">
                    <div class="form-group">
                      <div class="fl_wrap [[!country_code?country_code_error?'has-error':'':'has-success']]">
                        <label class="fl_label" for="country_code">[['lbl_customer_support_country_code' | translate]]</label>
                        <input class="fl_input form-control" id="country_code" numbers-only placeholder="+" name="country_code" ng-model="country_code" type="text"  />
                        <i ng-if="country_code" class="fa fa-check-circle out-icon"></i>
                        <i ng-if="country_code_error && !country_code" class="fa fa-exclamation-circle out-icon"  title="Country Code is required"></i>
                      </div>
                    </div>
                  </div> 
                  <div class="col-sm-8 col-xs-1">
                    <div class="form-group">
                      <div class="fl_wrap [[!contact?contact_error?'has-error':'':'has-success']]">
                        <label class="fl_label" for="contact">[['lbl_customer_support_phone_number' | translate]]</label>
                        <input class="fl_input form-control" id="contact" name="contact" numbers-only placeholder="[['lbl_customer_support_optional' | translate]]" ng-model="contact" type="text"/>
                        <i ng-if="contact" class="fa fa-check-circle out-icon"></i>
                        <i ng-if="contact_error && !contact" class="fa fa-exclamation-circle out-icon"  title="Contact number is required"></i>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="fl_wrap select-dropdown [[!product_type?product_type_error?'has-error':'':'has-success']]">
                    <label class="fl_label" for="product_type">[['lbl_customer_support_product' | translate]]</label>
                    <select class="fl_input form-control" type="text" id="product_type" name="product_type" ng-model="product_type" ng-change="selectTitcketType(product_type)">
                      <option value="">[['lbl_customer_support_select_product' | translate]]</option>
                      <option ng-if="changeLangs=='en'" ng-repeat="product in productTypes" value="[[product.id]]">[[product.product_name]]</option>
                      <option ng-if="changeLangs=='chi'" ng-repeat="product in productTypes" value="[[product.id]]">
                        [[product.product_name_chi]]
                      </option>
                      <option ng-if="changeLangs=='ru'" ng-repeat="product in productTypes" value="[[product.id]]">
                        [[product.product_name_ru]]
                      </option>
                    </select>
                    <i ng-if="ticket_type" class="fa fa-check-circle out-icon"></i>
                    <i ng-if="product_type_error && !product_type" class="fa fa-exclamation-circle out-icon" title="Product type is required"></i>
                  </div>
                </div>
                <div class="form-group">
                  <div class="fl_wrap select-dropdown [[!ticket_type?ticket_type_error?'has-error':'':'has-success']]">
                    <label class="fl_label" for="ticket_type">[['lbl_customer_support_ticket_type' | translate]]</label>
                    <select class="fl_input form-control" type="text" id="ticket_type" name="ticket_type" ng-model="ticket_type" ng-change="selectTitcketCategory(ticket_type)">
                      <option value="">[['lbl_customer_support_select_ticket' | translate]]</option>
                      <option ng-if="changeLangs=='en'" ng-repeat="type in ticketTypes" value="[[type.id]]">[[type.ticket_type_name]]</option>
                      <option ng-if="changeLangs=='chi'" ng-repeat="type in ticketTypes" value="[[type.id]]">
                        [[type.ticket_type_name_chi]]
                      </option>
                      <option ng-if="changeLangs=='ru'" ng-repeat="type in ticketTypes" value="[[type.id]]">
                        [[type.ticket_type_name_ru]]
                      </option>
                    </select>
                    <i ng-if="ticket_type" class="fa fa-check-circle out-icon"></i>
                    <i ng-if="ticket_type_error && !ticket_type" class="fa fa-exclamation-circle out-icon" title="Ticket type is required"></i>
                  </div>
                </div>
                <div class="form-group"  ng-if="ticket_type!='3' && ticket_type!='6'">
                <!-- <div class="form-group" ng-if="ticket_type=='1'||ticket_type=='2'||ticket_type=='4'||ticket_type=='5'"> -->
                  <div class="fl_wrap select-dropdown [[!obj.ticket_category?ticket_category_error?'has-error':'':'has-success']]">
                    <label class="fl_label" for="ticket_category">[['lbl_customer_support_ticket_category' | translate]]</label>
                    <select class="fl_input form-control" type="text" id="ticket_category" name="ticket_category" ng-model="obj.ticket_category">
                      <option value="">[['lbl_customer_support_select_category' | translate]]</option>
                      <option ng-repeat="category in ticketCategories" ng-if="category.ticket_type_id==ticket_type && changeLangs=='en'" value="[[category.id]]">[[category.ticket_category_name]]</option>
                      <option ng-repeat="category in ticketCategories" ng-if="category.ticket_type_id==ticket_type && changeLangs=='chi'" value="[[category.id]]">[[category.ticket_category_name_chi]]</option>
                      <option ng-repeat="category in ticketCategories" ng-if="category.ticket_type_id==ticket_type && changeLangs=='ru'" value="[[category.id]]">[[category.ticket_category_name_ru]]</option>
                    </select>
                    <i ng-if="obj.ticket_category" class="fa fa-check-circle out-icon"></i>
                    <i ng-if="ticket_category_error && !obj.ticket_category" class="fa fa-exclamation-circle out-icon"  title="Ticket category is required"></i>
                  </div>
                </div>
                <div class="form-group">
                  <div class="fl_wrap [[!ticket_subject?ticket_subject_error?'has-error':'':'has-success']]">
                    <label class="fl_label" for="Subject">[['lbl_customer_support_subject' | translate]]</label>
                    <input class="fl_input form-control" id="ticket_subject" name="ticket_subject" ng-model="ticket_subject" type="text"/>
                    <i ng-if="ticket_subject" class="fa fa-check-circle out-icon"></i>
                    <i ng-if="ticket_subject_error && !ticket_subject" class="fa fa-exclamation-circle out-icon"  title="Ticket subject is required"></i>
                  </div>
                </div>
                <div class="form-group">
                  <div class="fl_wrap textarea [[!ticket_description?ticket_description_error?'has-error':'':'has-success']]">
                    <label class="fl_label" for="ticket_description">[['lbl_customer_support_description' | translate]]</label>
                    <textarea class="fl_input form-control" type="text" id="ticket_description" name="ticket_description" ng-model="ticket_description" ></textarea>
                    <i ng-if="ticket_description" class="fa fa-check-circle out-icon"></i>
                    <i ng-if="ticket_description_error && !ticket_description" class="fa fa-exclamation-circle out-icon"  title="Ticket description is required"></i>
                  </div>
                </div>


                <div class="form-group">
                  <div class="file-upload" id="addFileDynamic">
                    [['lbl_customer_support_fileupload' | translate]]
                    <div class="right-up-icon">
                      <i class="fa fa-arrow-circle-o-up" aria-hidden="true"></i>
                      <!-- <input type="file" name="ticket_attachments[]" id="ticket_attachments" multiple="multiple"> -->
                    </div>
                  </div>
                  <ul class="portfolio_li"></ul>
                </div>
                <div class="form-group">
                  <span class="custom-checkbox2">
                    <input id="tandc" name="tandc" ng-model="tandc" type="checkbox" value="1" />
                    <label for="tandc">
                      [['lbl_tandc1'|translate]]
                      <a class="text_decoration_underline" target="_blank" href="<?=url('/') . '/privacy-policy'?>">[['lbl_sixclouds_privacy_policy'|translate]]</a>
                      [['lbl_tandc2'|translate]]
                      <a class="text_decoration_underline" target="_blank" href="<?=url('/') . '/privacy-policy'?>">[['lbl_sixclouds_privacy_policy'|translate]]</a>
                      [['lbl_tandc3'|translate]]
                      <a class="text_decoration_underline" target="_blank" href="<?=url('/') . '/privacy-policy'?>">[['lbl_sixclouds_privacy_policy'|translate]]</a>
                      [['lbl_tandc4'|translate]]
                    </label>
                  </span>
                  <label class="lbl_color_red" ng-if="show_confirm_error">[['lbl_confirm'|translate]]</label>
                </div>
                <div class="form-bottom-btn">
                  <div class="row">
                      <div class="col-sm-12">
                        <button class="btn submit-btn create-issue" ng-click="Createsupportticket()"> [['lbl_submit'|translate]]</button>
                      </div>

                  </div>
                </div>
                <div id="divImagesportfolio"></div>
              </form>
            </div> 
            <p>[['lbl_customer_support_note' | translate]]</p>
          </div>
        </div>

      </section>

    </div>

    <!--Footer Start-->
    <footer class="animatedParent animateOnce blue-footer">
      <div class="footer-bg-repeat animated fadeIn slowest"></div>
      <div class="main-footer animated fadeIn slowest">
        <div class="container">
          <div class="row">
            <div class="col-sm-4">
              <div class="h-div">
                <div class="footer-logo"> <a href="<?=url('/')?>"><img ng-src="resources/assets/images/logo-blue.png"></a> </div>
                <p class="l-link"><a href="<?=url('/') ?>">www.sixclouds.[[ @if(strpos(url('/'), 'sixclouds.cn') !== false ||  strpos(url('/'), 'localhost') !== false) ]]cn[[ @else ]]net[[ @endif ]]</a></p>
                <div class="bottom-div"> <span class="copy">&copy;</span>[['lbl_all_rights_reserved'|translate]] </div>
                <p>[[ @if(strpos(url('/'), 'sixclouds.cn') !== false ||  strpos(url('/'), 'localhost') !== false) ]] ICP号为：渝ICP备18001226号 [[  @endif ]]</p>
              </div>

            </div>
            <div class="col-sm-7">
              <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                  <td width="50%" valign="top"><div class="h-div" ng-class="{ru_text:changeLangs=='ru'}">
                      <h3>[["lbl_about_us" | translate]]</h3>
                      <ul>
                        <li><a target="_blank" href="<?=url('/') . '/about-us/the-company'?>">[['lbl_the_company'|translate]]</a></li>
                        <li><a target="_blank" href="<?=url('/') . '/about-us/core-values'?>">[['lbl_core_values'|translate]]</a></li>
                        <li><a target="_blank" href="<?=url('/') . '/about-us/our-team'?>">[['lbl_our_team'|translate]]</a></li>
                        <!-- <li><a href="javascript:;">[["lbl_ignite" | translate]] </a></li>
                        <li><a href="javascript:;">[["lbl_sixteen" | translate]] </a></li>
                        <li><a href="javascript:;">[["lbl_discover" | translate]] </a></li>
                        <li><a href="javascript:;">[["lbl_proof_reading" | translate]] </a></li> -->
                      </ul>
                    </div>
                  </td>
                  <td valign="top"><div class="h-div" ng-class="{ru_text:changeLangs=='ru'}">
                      <h3>[["lbl_quick_links" | translate]]</h3>
                      <ul>
                        <li><a target="_blank" href="<?=url('/') . '/customer-support'?>">[["lbl_customer_support" | translate]] </a></li>
                        <li><a target="_blank" href="<?=url('/') . '/privacy-policy'?>">[["lbl_privacy_policy" | translate]]</a></li>
                      </ul>
                    </div>
                     </td>
                </tr>
              </table>
            </div>

          </div>
        </div>
      </div>
    </footer>
  </div>

  {{ HTML::script('resources/assets/js/front/mp/popper.min.js') }}
  {{ HTML::script('resources/assets/js/front/mp/bootstrap.js') }}
  {{ HTML::script('resources/assets/js/front/mp/swiper.min.js') }}
  {{ HTML::script('resources/assets/js/front/mp/css3-animate-it.js') }}
  {{ HTML::script('resources/assets/js/front/mp/jquery.FlowupLabels.js') }}
  {{ HTML::script('resources/assets/js/common/star-rating.js') }}

    <!--bootstrap-datetimepicker Start-->
  {{ HTML::script('resources/assets/js/front/mp/moment.js') }}
  {{ HTML::script('resources/assets/js/front/mp/bootstrap-datetimepicker.js') }}
  {{ HTML::script('resources/assets/js/front/mp/jquery-asRange.js') }}
  {{ HTML::script('resources/assets/js/common/sweetalert.min.js') }}
  {{ HTML::script('resources/assets/js/common/custom_alert.js') }}

  {{ HTML::script('resources/assets/js/common/moment-timezone.js') }}
    {{ HTML::script('resources/assets/js/common/moment-timezone-with-data.js') }}

  {{ HTML::script('resources/assets/js/front/controllers/CustomerSupport.js') }}

  {{ HTML::script('resources/assets/js/front/factory/customersupport.js') }}

  <!-- <script type="text/javascript" src="assets/js/script.js"></script> -->
  <script type="text/javascript">
    $('body').css('background','#fff');

      $(window).load(function() {
       if($(window).width() > 767) {

       } else {
         $(function(){
             var navMain = $(".hMenu");
             navMain.on("click", "a", null, function () {
                 navMain.collapse('hide');
             });
         });
       }
    });

      $('.hMenu a').click(function() {
        $('.hMenu li').removeClass('active');
            var datanameC = $(this).attr('data-name');
            var headerHei = $('header').height();
           $(this).parent('li').addClass('active');
            $('html, body').animate({
                scrollTop: $("#"+datanameC).offset().top-headerHei
              }, 1000);
      });
  </script>

</body>
</html>
</body>
</html>
