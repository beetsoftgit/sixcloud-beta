<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sixclouds Web "Ignite"</title>
    {{ HTML::script('resources/assets/js/common/jquery.min.js') }}
    <!--<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" />
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.0/css/font-awesome.css" />-->
    {{ HTML::script('resources/assets/js/common/angular.min.js') }}
     

    <!--Slider Style-->
    {{ HTML::style('resources/assets/css/front/mp/swiper.min.css',array('rel'=>'stylesheet')) }}

    <!--Fonts Style-->
    {{ HTML::style('resources/assets/css/front/mp/custom-font.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/front/mp/font-awesome.min.css',array('rel'=>'stylesheet')) }}

    <!--DOB Style-->
    <!-- <link rel="stylesheet" type="text/css" href="assets/css/dob-ui.css"/> -->

    <!--Bootstrap Style-->
    {{ HTML::style('resources/assets/css/front/mp/bootstrap.css',array('rel'=>'stylesheet')) }}


    <!--Custom Style-->
    {{ HTML::style('resources/assets/css/front/ignite/style.css',array('rel'=>'stylesheet')) }}
    <!-- <link rel="stylesheet" type="text/css" href="assets/css/responsive.css">
    <script src="script.js"></script> -->
</head>
<body data-ng-app="sixcloudswidgetApp" ng-controller="myCtrl" ng-cloak>               
    <div class="body-div sign-up-page display-table"> 
      <div class="display-cell">
            <div class="sign-up-form">
                <div class="logo"><img src="resources/assets/images/logo.svg"></div>
                <h3>SIGN UP</h3>
                <div class="form-group">
                    <input type="email" class="form-control" placeholder="Email Address">
                </div>
                 <div class="form-group">
                    <input type="text" class="form-control" placeholder="First Name">
                </div>
                 <div class="form-group">
                    <input type="text" class="form-control" placeholder="Last Name">
                </div>
                 <div class="form-group">
                    <a href="https://sixclouds.net/membership" class="btn btn-pink btn-block">SIGN UP</a>
                </div>
                <div class="form-group already-have-account">
                    Already have an account? <a href="#">Login</a>
                </div>
          </div>
        </div>
    </div>
    {{ HTML::script('resources/assets/js/front/mp/bootstrap.js') }} 
    {{ HTML::script('resources/assets/js/front/mp/swiper.min.js') }}  
    <script src='assets/js/dob-ui.js'></script> 
    <script>
        app = angular.module('sixcloudswidgetApp', []);   
        app.config(['$interpolateProvider', function ($interpolateProvider) {
            $interpolateProvider.startSymbol('[[');
            $interpolateProvider.endSymbol(']]');
        }]);
        app.controller('myCtrl', function($scope) {
           $scope.widgetinfo        = <?php echo json_encode($widgetinfo) ?>;
           $scope.widgetprojectinfo = <?php echo json_encode($widgetprojectinfo) ?>;
           
           
          // $scope.widgetinfo
          setTimeout(function(){
            var total_project_length = $scope.widgetprojectinfo.length;
            if($scope.widgetinfo.length > 0 ) 
            {

                var height = "19";
                if( $scope.widgetinfo[0].no_of_projects == 1 )
                {
                    if(total_project_length > 0)
                    {
                        height  = "85";
                    }
                    else{
                        height  = "85";
                    }
                }
                else if( $scope.widgetinfo[0].no_of_projects == 2)
                {
                    if(total_project_length > 1)
                    {
                        height  = "170";
                    }
                    else if(total_project_length > 0){
                        height  = "85";
                    }

                }
                else if( $scope.widgetinfo[0].no_of_projects == 3)
                {
                    if(total_project_length > 2)
                    {
                        height  = "255";
                    }
                    else if(total_project_length > 1)
                    {
                        height  = "170";
                    }
                    else if(total_project_length > 0)
                    {
                        height  = "85";
                    }                                
                }
                else if( $scope.widgetinfo[0].no_of_projects == 4)
                {
                    if(total_project_length > 3)
                    {
                        height  = "340";
                    }
                    else if(total_project_length > 2)
                    {
                        height  = "255";
                    }
                    else if(total_project_length > 1)
                    {
                        height  = "170";
                    }
                    else if(total_project_length > 0)
                    {
                        height  = "85";
                    }  
                }
                else if( $scope.widgetinfo[0].no_of_projects == 5)
                {
                    if(total_project_length > 4)
                    {
                        height  = "425";
                    }
                    else if(total_project_length > 3)
                    {
                        height  = "340";
                    }
                    else if(total_project_length > 2)
                    {
                        height  = "255";
                    }
                    else if(total_project_length > 1)
                    {
                        height  = "170";
                    }
                    else if(total_project_length > 0)
                    {
                        height  = "85";
                    } 
                }
                $('#mainwidgetwrap').css('height',height);
                $('#widgetbody').marquee({

                        // enable the plugin
                        enable : true,  //plug-in is enabled

                        // scroll direction
                        // 'vertical' or 'horizontal'
                        direction: 'vertical',

                        // children items
                        itemSelecter : 'div.rotate', 

                        // animation delay
                        delay: 3000,

                        // animation speed
                        speed: 1,

                        // animation timing
                        timing: 1,

                        // mouse hover to stop the scroller
                        mouse: true

                    });
            }
        }, 10);
      });
    </script>
</body>
</html>
