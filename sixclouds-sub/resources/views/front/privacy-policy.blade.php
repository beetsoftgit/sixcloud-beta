<!DOCTYPE html>
<html ng-app="sixcloudPrivacyPolicyApp">
<head>
  <title>{{$title}}</title>
  <meta name="description" content="{{$meta}}">
  <meta name="robots" content="index,follow" />
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1" name="viewport"/>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="alternate" href="{{$alternate}}" hreflang="{{$alternatelang}}" />
  <link rel="canonical" href="{{$canonical}}" />
  <meta name="google-site-verification" content="F0XMjfssK8hcmTCfqJJcQFq80vAe8lHHegJBFA84XAQ" />
  <meta name="msvalidate.01" content="5789F571110EF5249F1B42276844821B" />
  <meta name="p:domain_verify" content="8475f8bbc093c663df8ead38f223dfac"/>

  <!-- Google Analytics -->
  <script>
  window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
  ga('create', 'UA-149665763-1');
  ga('send', 'pageview');
  </script>
  <script async src='https://www.google-analytics.com/analytics.js'></script>
  <!-- End Google Analytics -->

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127371237-1"></script>
  <script>
   window.dataLayer = window.dataLayer || [];
   function gtag(){dataLayer.push(arguments);}
   gtag('js', new Date());

   gtag('config', 'UA-127371237-1');
  </script>
  
  {{ HTML::script('resources/assets/js/common/jquery.min.js') }}
  <link rel="shortcut icon" type="image/x-icon" href="resources/assets/images/favicon.ico">
  <!--Animation Style-->
  {{ HTML::style('resources/assets/css/front/mp/animations.css',array('rel'=>'stylesheet')) }}

  <!--Slider Style-->
  {{ HTML::style('resources/assets/css/front/mp/swiper.min.css',array('rel'=>'stylesheet')) }}

  <!--Fonts Style-->
  {{ HTML::style('resources/assets/css/front/mp/custom-font.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/font-awesome.min.css',array('rel'=>'stylesheet')) }}


  <!--jquery.FlowupLabels Style-->
  {{ HTML::style('resources/assets/css/front/mp/jquery.FlowupLabels.css',array('rel'=>'stylesheet')) }}

  <!--bootstrap-datetimepicker Style-->
  {{ HTML::style('resources/assets/css/front/mp/bootstrap-datetimepicker.min.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/asRange.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/common/star-rating.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/bootstrap.css',array('rel'=>'stylesheet')) }}

  <!--Custom Style-->
  {{ HTML::style('resources/assets/css/front/mp/style.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/mp_custom.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/responsive.css',array('rel'=>'stylesheet')) }}
  {{ HTML::script('resources/assets/js/common/angular.min.js') }}
  {{ HTML::script('resources/assets/js/common/angular-route.js') }}
  {{ HTML::script('resources/assets/js/common/angular-translate.min.js') }}
  {{ HTML::script('resources/assets/js/common/js.cookie.min.js') }}

  <!--{{ HTML::script('resources/assets/js/common/angular-sanitize.min.js') }} -->

    <!--[if lte IE 9]>
          <link href='assets/css/animations-ie-fix.css' rel='stylesheet'>
    <![endif]-->
  <!-- <script src="script.js"></script> -->
</head>
<body ng-controller="PrivacyPolicyController">
<div class="body-div">
  <!--Header section Started-->
  <header class="header-design2" data-spy="affix" data-offset-top="60">
    <div class="container">
      <nav class="navbar navbar-custom">
       <div class="container-fluid">
         <!-- Brand and toggle get grouped for better mobile display -->
         <div class="navbar-header">
           <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
             <span class="sr-only">Toggle navigation</span>
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
           </button>
           <a class="navbar-brand" href="<?=url('/')?>"><img style="height: auto;" src="resources/assets/images/logo-privacy-policy.png" width="170"  class="white-logo"><img style="height: auto;" src="resources/assets/images/logo-blue.png" width="170"  class="blue-logo"></a>
         </div>

         <!-- Collect the nav links, forms, and other content for toggling -->
         <div class="collapse navbar-collapse hMenu" id="bs-example-navbar-collapse-1">
            <!-- <ul class=" before-login nav navbar-right header-mid-btn about-us-btn">
              <li class="become-a-buyer"><a href="javascript:;" ng-if="eng == true" ng-click="changeLang('en')" class="btn  uppercase login-btn">[['lbl_lang_chng'|translate]]</a>
                <a href="javascript:;" ng-if="!eng" ng-click="changeLang('chi')" class="btn uppercase login-btn">[['lbl_lang_chng'|translate]]</a>
              </li>
            </ul> -->
            <select ng-model="changeLangs" ng-change="changeLang(changeLangs)" class="before-login nav navbar-right header-mid-btn about-us-btn">
              <option value="chi" ng-selected="changeLangs=='chi'">中文</option>
              <option value="en" ng-selected="changeLangs=='en'">English</option>
              <option value="ru" ng-selected="changeLangs=='ru'">Русский</option>
            </select>
         </div><!-- /.navbar-collapse -->
       </div><!-- /.container-fluid -->
     </nav>

    </div>
  </header>
  <!--Header section End-->

  <div id="main">
    <section class="page-head">
      <!-- <div style="background-image:url(resources/assets/images/about-img.jpg);" class="img-div"></div> -->
      <div class="container">
        <h1>[["lbl_privacy_policy" | translate]]</h1>
      </div>
    </section>
    <section class="the-company-section" id="TheCompany">
      <div class="container">
        <div class="inner-padding">
          <p>[['lbl_this_policy_sets'|translate]]</p>

          <!-- <p>[['lbl_by_visiting_our_website1'|translate]] <a href="www.sixclouds.net">[['lbl_by_visiting_our_website_url'|translate]]</a> [['lbl_by_visiting_our_website2'|translate]]</p> -->
          <p>[['lbl_by_visiting_our_website1'|translate]]</p>
          <p>(a) [['lbl_by_visiting_our_website_a1'|translate]]</p>
          <p>(b) [['lbl_by_visiting_our_website_b'|translate]]</p>
          <p>(c) [['lbl_by_visiting_our_website_c'|translate]]</p>
          <p>(d) [['lbl_by_visiting_our_website_d'|translate]]</p>
          <p>[['lbl_by_visiting_our_website_e'|translate]]</p>
          <h3>[['lbl_1'|translate]]</h3>
            <h4 class="tab-subheading">1.1 [['lbl_1_1'|translate]]</h4>
          
              <p class="subheading-content">(a) [['lbl_1_1_a'|translate]]</p>
                <p class="privacy-policy-inner-content">(i) [['lbl_1_1_a_1'|translate]]</p>
                <p class="privacy-policy-inner-content">(ii) [['lbl_1_1_a_2'|translate]]</p>
                <p class="privacy-policy-inner-content">(iii) [['lbl_1_1_a_3'|translate]]</p>
                <p class="privacy-policy-inner-content">(iv) [['lbl_1_1_a_4'|translate]]</p>
                <p class="privacy-policy-inner-content">(v) [['lbl_1_1_a_5'|translate]]</p>
                <p class="privacy-policy-inner-content">(vi) [['lbl_1_1_a_6'|translate]]</p>
              
              <p class="subheading-content">(b) [['lbl_1_1_b'|translate]]</p>
                <p class="privacy-policy-inner-content">(i) [['lbl_1_1_b_1'|translate]]</p>
                <p class="privacy-policy-inner-content">(ii) [['lbl_1_1_b_2'|translate]]</p>
                <p class="privacy-policy-inner-content">(iii) [['lbl_1_1_b_3'|translate]]</p>

              <p class="subheading-content">(c) [['lbl_1_1_c'|translate]]</p>

            <h4 class="tab-subheading">1.2 [['lbl_1_2'|translate]]</h4>

          <h3>[['lbl_2'|translate]]</h3>
            <h4 class="tab-subheading">2.1 [['lbl_2_1'|translate]]</h4>

              <p class="subheading-content">(a) [['lbl_2_1_a'|translate]]</p>
              <p class="subheading-content">(b) [['lbl_2_1_b'|translate]]</p>
              <p class="subheading-content">(c) [['lbl_2_1_c'|translate]]</p>
              <p class="subheading-content">(d) [['lbl_2_1_d'|translate]]</p>
              <p class="subheading-content">(e) [['lbl_2_1_e'|translate]]</p>
              <p class="subheading-content">(f) [['lbl_2_1_f'|translate]]</p>
              <p class="subheading-content">(g) [['lbl_2_1_g'|translate]]</p>
              <p class="subheading-content">(h) [['lbl_2_1_h'|translate]]</p>
              <p class="subheading-content">(i) [['lbl_2_1_i'|translate]]</p>
              <p class="subheading-content">(j) [['lbl_2_1_j'|translate]]</p>
              <p class="subheading-content" ng-if="changeLangs == 'en'||changeLangs == 'chi'">(k) [['lbl_2_1_k'|translate]]</p>

            <h4 class="tab-subheading">2.2 [['lbl_2_2'|translate]]</h4>
              <p class="subheading-content">(a) [['lbl_2_2_a'|translate]]</p>
              <p class="subheading-content">(b) [['lbl_2_2_b'|translate]]</p>
              <p class="subheading-content">(c) [['lbl_2_2_c'|translate]]</p>
              <p class="subheading-content">(d) [['lbl_2_2_d'|translate]]</p>
          
          <h3>[['lbl_3'|translate]]</h3>
            <p class="subheading-content">[['lbl_3_text'|translate]]</p>
            <p class="subheading-content">(a) [['lbl_3_a'|translate]]</p>
            <p class="subheading-content">(b) [['lbl_3_b'|translate]]</p>
            <p class="subheading-content">(c) [['lbl_3_c'|translate]]</p>
            <p class="subheading-content">(d) [['lbl_3_d'|translate]]</p>
            <p class="subheading-content">(e) [['lbl_3_e'|translate]]</p>
            <p class="subheading-content">(f) [['lbl_3_f'|translate]]</p>

          <h3>[['lbl_4'|translate]]</h3>
            <h4 class="tab-subheading">4.1 [['lbl_4_1'|translate]]</h4>
            <h4 class="tab-subheading">4.2 [['lbl_4_2'|translate]]</h4>
            <h4 class="tab-subheading">4.3 [['lbl_4_3'|translate]]</h4>
            <h4 class="tab-subheading">4.4 [['lbl_4_4'|translate]]</h4>
            <p class="subheading-content">(a) [['lbl_4_4_a'|translate]]</p>
            <p class="subheading-content">(b) [['lbl_4_4_b'|translate]]</p>
            <p class="subheading-content">(c) [['lbl_4_4_c'|translate]]</p>
            <p class="subheading-content">(d) [['lbl_4_4_d'|translate]]</p>
            <h4 class="tab-subheading">4.5 [['lbl_4_5'|translate]]</h4>
            <h4 class="tab-subheading">4.6 [['lbl_4_6'|translate]]</h4>

          <h3>[['lbl_5'|translate]]</h3>
            <p class="subheading-content">[['lbl_5_text'|translate]]</p>

          <h3>[['lbl_6'|translate]]</h3>
            <p class="subheading-content">6.1 [['lbl_6_1'|translate]]</p>
            <p class="subheading-content">6.2 [['lbl_6_2'|translate]]</p>
            <p class="subheading-content">6.3 [['lbl_6_3'|translate]]</p>
            <p class="subheading-content">6.4 [['lbl_6_4'|translate]]</p>
            <p class="subheading-content">6.5 [['lbl_6_5'|translate]]</p>
            <p class="subheading-content">6.6 [['lbl_6_6'|translate]]</p>

          <h3>[['lbl_7'|translate]]</h3>
            <p class="subheading-content">7.1 [['lbl_7_1'|translate]]</p>
            <p class="subheading-content">7.2 [['lbl_7_2'|translate]]</p>

          <h3>[['lbl_8'|translate]]</h3>
            <p class="subheading-content">8.1 [['lbl_8_1'|translate]]</p>
              <p class="subheading-content">(a) [['lbl_8_1_a'|translate]]</p>
              <p class="subheading-content">(b) [['lbl_8_1_b'|translate]]</p>
              <p class="subheading-content">(c) [['lbl_8_1_c'|translate]]</p>
            <p class="subheading-content">8.2 [['lbl_8_2'|translate]]</p>

          <h3>[['lbl_9'|translate]]</h3>
            <p class="subheading-content">[['lbl_9_text'|translate]]</p>

          <h3>[['lbl_10'|translate]]</h3>
            <p class="subheading-content">[['lbl_10_text'|translate]]</p>

          <h3>[['lbl_11'|translate]]</h3>
            <p class="subheading-content">[['lbl_11_text'|translate]]<a href="<?=url('/') . '/customer-support'?>"> [['lbl_customer_support'|translate]]</a>[['lbl_dot'|translate]]</p>

          <p>[['lbl_updated_at'|translate]]</p>
        </div>
      </div>
    </section>
    
  </div>

  <!--Footer Start-->
  <footer class="animatedParent animateOnce blue-footer">
    <div class="footer-bg-repeat animated fadeIn slowest"></div>
    <div class="main-footer animated fadeIn slowest">
      <div class="container">
        <div class="row">
          <div class="col-sm-4">
            <div class="h-div">
              <div class="footer-logo"> <a href="<?=url('/')?>"><img ng-src="resources/assets/images/logo-blue.png"></a> </div>
              <p class="l-link"><a href="<?=url('/') ?>">www.sixclouds.[[ @if(strpos(url('/'), 'sixclouds.cn') !== false ||  strpos(url('/'), 'localhost') !== false) ]]cn[[ @else ]]net[[ @endif ]]</a></p>
              <div class="bottom-div"> <span class="copy">&copy;</span>[['lbl_all_rights_reserved'|translate]] </div>
              <p>[[ @if(strpos(url('/'), 'sixclouds.cn') !== false ||  strpos(url('/'), 'localhost') !== false) ]] ICP号为：渝ICP备18001226号 [[  @endif ]]</p>
            </div>

          </div>
          <div class="col-sm-7">
            <table cellpadding="0" cellspacing="0" width="100%">
              <tr>
                <td width="50%" valign="top"><div class="h-div" ng-class="{ru_text:changeLangs=='ru'}">
                    <h3>[["lbl_about_us" | translate]]</h3>
                    <ul>
                      <li><a target="_blank" href="<?=url('/') . '/about-us/the-company'?>">[['lbl_the_company'|translate]]</a></li>
                      <li><a target="_blank" href="<?=url('/') . '/about-us/core-values'?>">[['lbl_core_values'|translate]]</a></li>
                      <li><a target="_blank" href="<?=url('/') . '/about-us/our-team'?>">[['lbl_our_team'|translate]]</a></li>
                      <!-- <li><a href="javascript:;">[["lbl_ignite" | translate]] </a></li>
                      <li><a href="javascript:;">[["lbl_sixteen" | translate]] </a></li>
                      <li><a href="javascript:;">[["lbl_discover" | translate]] </a></li>
                      <li><a href="javascript:;">[["lbl_proof_reading" | translate]] </a></li> -->
                    </ul>
                  </div>
                </td>
                <td valign="top"><div class="h-div" ng-class="{ru_text:changeLangs=='ru'}">
                    <h3>[["lbl_quick_links" | translate]]</h3>
                    <ul>
                      <li><a target="_blank" href="<?=url('/') . '/customer-support'?>">[["lbl_customer_support" | translate]] </a></li>
                      <li><a target="_blank" href="<?=url('/') . '/privacy-policy'?>">[["lbl_privacy_policy" | translate]]</a></li>
                    </ul>
                  </div>
                   </td>
              </tr>
            </table>
          </div>

        </div>
      </div>
    </div>
  </footer>
</div>

{{ HTML::script('resources/assets/js/front/mp/popper.min.js') }}
{{ HTML::script('resources/assets/js/front/mp/bootstrap.js') }}
{{ HTML::script('resources/assets/js/front/mp/swiper.min.js') }}
{{ HTML::script('resources/assets/js/front/mp/css3-animate-it.js') }}
{{ HTML::script('resources/assets/js/front/mp/jquery.FlowupLabels.js') }}
{{ HTML::script('resources/assets/js/common/star-rating.js') }}

  <!--bootstrap-datetimepicker Start-->
{{ HTML::script('resources/assets/js/front/mp/moment.js') }}
{{ HTML::script('resources/assets/js/front/mp/bootstrap-datetimepicker.js') }}
{{ HTML::script('resources/assets/js/front/mp/jquery-asRange.js') }}

{{ HTML::script('resources/assets/js/front/controllers/PrivacyPolicyController.js') }}

<!-- <script type="text/javascript" src="assets/js/script.js"></script> -->
<script type="text/javascript">
  $('body').css('background','#fff');
    $(window).load(function() {
     if($(window).width() > 767) {

     } else {
       $(function(){
           var navMain = $(".hMenu");
           navMain.on("click", "a", null, function () {
               navMain.collapse('hide');
           });
       });
     }
  });

    $('.hMenu a').click(function() {
      $('.hMenu li').removeClass('active');
          var datanameC = $(this).attr('data-name');
          var headerHei = $('header').height();
         $(this).parent('li').addClass('active');
          $('html, body').animate({
              scrollTop: $("#"+datanameC).offset().top-headerHei
            }, 1000);
    });
</script>

</body>
</html>
</body>
</html>
