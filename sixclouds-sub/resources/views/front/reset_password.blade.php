<!DOCTYPE html>
<head>
    <title>SixClouds::Reset password</title>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    {{ HTML::style('resources/assets/css/front/mp/animations.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/front/mp/swiper.min.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/front/mp/custom-font.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/front/mp/font-awesome.min.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/front/mp/jquery.FlowupLabels.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/front/mp/bootstrap-datetimepicker.min.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/front/mp/asRange.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/common/sweet-alert.min.css',array('rel'=>'stylesheet')) }}

    {{ HTML::style('resources/assets/css/front/mp/bootstrap.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/front/mp/style.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/front/mp/mp_custom.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/front/mp/responsive.css',array('rel'=>'stylesheet')) }}

    {{ HTML::script('resources/assets/js/common/jquery-3.2.1.min.js') }}
    {{ HTML::script('resources/assets/js/common/angular.min.js') }}
    {{ HTML::script('resources/assets/js/common/angular-route.js') }}
    {{ HTML::script('resources/assets/js/common/angular-sanitize.min.js') }}
    {{ HTML::script('resources/assets/js/common/angular-page-loader.min.js') }}
    <link rel="shortcut icon" href="<?php echo Config('constants.path.ASSETS_IMAGE') . 'Large-logo.png' ?>"/>
    <base href="<?= url('/') . '/' ?>">
</head>
<body class="margin-zero">
    <!-- <div class="main user-signup-page">
        <div class="user-content">
            <div class="container">
                <div class="logo text-center">
                    <img src="resources/assets/images/logo-large.png">
                </div>
                <div class="options text-center">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6">
                            <div class="icon">
                                <span class="sci-startup-1"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span></span>
                            </div>
                            <h2 class="title text-black margin-bt30">Reset Password</h2>
                            <div class="form typing-effect">
                                <form id="form_reset_pwd" name="form_reset_pwd" method="post" action="javascript:;">
                                    <div class="form-group input-label">
                                        <input type="password" name="password" id="password" class="form-control">
                                        <input type="hidden" name="email" id="email" value="<?php echo $email; ?>" class="form-control">
                                        <span>Password:</span>
                                    </div>
                                    <div class="form-group input-label">
                                        <input type="password" name="password_confirmation" id="password_confirmation" class="form-control">
                                        <span>Confirm Password:</span>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn">Reset</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <section class="login-section animatedParent animateOnce">


      <!-- <div class="left-g-circle animated bounceInLeft go"><img src="resources/assets/images/gradient-circle.png"></div>
      <div class="right-g-sm-circle animated growIn go"><img src="resources/assets/images/gradient-circle.png" width="60"></div>
      <div class="right-g-trangle animated bounceInRight go"><img src="resources/assets/images/gradient-trangle.png"></div> -->
    @if(!isset($module))
        <div class="container">
            <div class="sign-up-logo text-center animated fadeInDown go"> <a href="<?=url('/')?>"><img src="resources/assets/images/logo-blue.png" width="173" height="auto"></a> </div>
            <div class="head-sub-text font_color text-center animated fadeInUp go">
                @if(isset($language)&&$language=='en')
                    <h3>Reset Password</h3>
                @elseif(isset($language)&&$language=='chi')
                    <h3>重置密码</h3>
                @else
                    <h3>Reset Password</h3>
                @endif                
            </div>
            <!-- <div class="head-sub-text text-center animated fadeInUp go" ng-if="show_error">
              <h5>[[login_error]]</h5>
            </div> -->
          <!--Step first-->
            <div class="flowup-labels-form animated fadeInUp go">
                <div class="panel-body">
                    <div class="content-div">
                        <div class="sign-up-form">
                            <form id="form_reset_pwd" name="form_reset_pwd" method="post" class="FlowupLabels" action="javascript:;">
                                <div class="form-group">
                                  <div class="fl_wrap focused">
                                    <label class="fl_label" for="password">{{ isset($language)&&$language=='en'?'New Password':(isset($language)&&$language=='chi'?'新密码':'Новый пароль') }} </label>
                                    <input type="password" name="password" id="password" class="fl_input  form-control">
                                    <input type="hidden" name="email" id="email" value="<?php echo $email; ?>" class="form-control">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="fl_wrap focused">
                                    <label class="fl_label " for="password_confirmation">{{ isset($language)&&$language=='en'?'Confirm Password':(isset($language)&&$language=='chi'?'确认密码':'Подтвердите Пароль') }}</label>
                                    <input type="password" name="password_confirmation" id="password_confirmation" class="fl_input  form-control">
                                  </div>
                                </div>
                                <div class="form-bottom-btn">
                                  <div class="row">
                                      <div class="col-sm-4">
                                        <button class="btn button_blue_custom submit-btn" type="submit" >{{ isset($language)&&$language=='en'?'Reset':(isset($language)&&$language=='chi'?'重置':'Сброс') }}</button>
                                      </div>
                                  </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="container">
            <div class="sign-up-logo text-center animated fadeInDown go"> <a href="<?=url('/')?>"><img src="resources/assets/images/logo-blue.png" width="173" height="auto"></a> </div>
            <div class="head-sub-text font_color text-center animated fadeInUp go">
              @if(isset($language)&& $language=='en')
                    <h3>Reset Password</h3>
              @elseif(isset($language)&& $language=='chi')
                   <h3>重置密码</h3>
              @else
                  <h3>Reset Password</h3>
              @endif
            </div>
            <!-- <div class="head-sub-text text-center animated fadeInUp go" ng-if="show_error">
              <h5>[[login_error]]</h5>
            </div> -->
          <!--Step first-->
            <div class="flowup-labels-form animated fadeInUp go">
                <div class="panel-body">
                    <div class="content-div">
                        <div class="sign-up-form">
                            @if(isset($language)&& $language=='en')
                                <form id="form_reset_pwd_ignite" name="form_reset_pwd" method="post" class="FlowupLabels" action="javascript:;">
                                    <div class="form-group">
                                      <div class="fl_wrap focused">
                                        <label class="fl_label" for="password">{{ isset($language)&&$language=='en'?'New Password':(isset($language)&&$language=='chi'?'新密码':'Новый пароль') }}</label>
                                        <input type="password" name="password" id="password" class="fl_input fl_input_custom form-control">
                                        <input type="hidden" name="email" id="email" value="<?php echo $email; ?>" class="form-control">
                                        <input type="hidden" name="module" id="module" value="<?php echo $module; ?>" class="form-control">
                                        <input type="hidden" name="language" id="language" value="<?php echo $language; ?>" class="form-control">
                                        <input type="hidden" name="redirectTo" id="redirectTo" value="<?php echo $redirectTo; ?>" class="form-control">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <div class="fl_wrap focused">
                                        <label class="fl_label " for="password_confirmation">{{ isset($language)&&$language=='en'?'Confirm Password':(isset($language)&&$language=='chi'?'确认密码':'Подтвердите Пароль') }}</label>
                                        <input type="password" name="password_confirmation" id="password_confirmation" class="fl_input fl_input_custom form-control">
                                      </div>
                                    </div>
                                    <div class="form-bottom-btn">
                                      <div class="row">
                                          <div class="col-sm-4">
                                            <button class="btn button_blue_custom submit-btn" type="submit" >{{ isset($language)&&$language=='en'?'Reset':(isset($language)&&$language=='chi'?'重置':'Сброс') }}</button>
                                          </div>
                                      </div>
                                    </div>
                                </form>
                            @elseif(isset($language)&& $language=='chi')
                                <form id="form_reset_pwd_ignite_chinese" name="form_reset_pwd" method="post" class="FlowupLabels" action="javascript:;">
                                    <div class="form-group">
                                      <div class="fl_wrap focused">
                                        <label class="fl_label" for="password">{{ isset($language)&&$language=='en'?'New Password':(isset($language)&&$language=='chi'?'新密码':'Новый пароль') }}</label>
                                        <input type="password" name="password" id="password" class="fl_input fl_input_custom form-control">
                                        <input type="hidden" name="email" id="email" value="<?php echo $email; ?>" class="form-control">
                                        <input type="hidden" name="module" id="module" value="<?php echo $module; ?>" class="form-control">
                                        <input type="hidden" name="language" id="language" value="<?php echo $language; ?>" class="form-control">
                                        <input type="hidden" name="redirectTo" id="redirectTo" value="<?php echo $redirectTo; ?>" class="form-control">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <div class="fl_wrap focused">
                                        <label class="fl_label " for="password_confirmation">{{ isset($language)&&$language=='en'?'Confirm Password':(isset($language)&&$language=='chi'?'确认密码':'Подтвердите Пароль') }}</label>
                                        <input type="password" name="password_confirmation" id="password_confirmation" class="fl_input fl_input_custom form-control">
                                      </div>
                                    </div>
                                    <div class="form-bottom-btn">
                                      <div class="row">
                                          <div class="col-sm-4">
                                            <button class="btn button_blue_custom submit-btn" type="submit" >{{ isset($language)&&$language=='en'?'Reset':(isset($language)&&$language=='chi'?'重置':'Сброс') }}</button>
                                          </div>
                                      </div>
                                    </div>
                                </form>
                            @else
                                <form id="form_reset_pwd_ignite" name="form_reset_pwd" method="post" class="FlowupLabels" action="javascript:;">
                                    <div class="form-group">
                                      <div class="fl_wrap focused">
                                        <label class="fl_label" for="password">{{ isset($language)&&$language=='en'?'New Password':(isset($language)&&$language=='chi'?'新密码':'Новый пароль') }}</label>
                                        <input type="password" name="password" id="password" class="fl_input fl_input_custom form-control">
                                        <input type="hidden" name="email" id="email" value="<?php echo $email; ?>" class="form-control">
                                        <input type="hidden" name="module" id="module" value="<?php echo $module; ?>" class="form-control">
                                        <input type="hidden" name="language" id="language" value="<?php echo $language; ?>" class="form-control">
                                        <input type="hidden" name="redirectTo" id="redirectTo" value="<?php echo $redirectTo; ?>" class="form-control">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <div class="fl_wrap focused">
                                        <label class="fl_label " for="password_confirmation">{{ isset($language)&&$language=='en'?'Confirm Password':(isset($language)&&$language=='chi'?'确认密码':'Подтвердите Пароль') }}</label>
                                        <input type="password" name="password_confirmation" id="password_confirmation" class="fl_input fl_input_custom form-control">
                                      </div>
                                    </div>
                                    <div class="form-bottom-btn">
                                      <div class="row">
                                          <div class="col-sm-4">
                                            <button class="btn button_blue_custom submit-btn" type="submit" >{{ isset($language)&&$language=='en'?'Reset':(isset($language)&&$language=='chi'?'重置':'Сброс') }}</button>
                                          </div>
                                      </div>
                                    </div>
                                </form>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    </section>
    <script>
        // set MODE for enable and disable console.log
        var project_mode = 'development'; //'production';
        var BASEURL = "<?=url('/') . '/'?>";
        var VIDEO_PLACEHOLDER = "<?=url('/') . config('constants.path.ELT_VIDOE_PLACEHOLDER')?>";
    </script>
    <!-- Core files -->
    {{ HTML::script('resources/assets/js/front/mp/bootstrap.js') }}
    {{ HTML::script('resources/assets/js/front/mp/swiper.min.js') }}
    {{ HTML::script('resources/assets/js/front/mp/css3-animate-it.js') }}
    {{ HTML::script('resources/assets/js/front/mp/jquery.FlowupLabels.js') }}
    {{ HTML::script('resources/assets/js/front/mp/moment.js') }}
    {{ HTML::script('resources/assets/js/front/mp/bootstrap-datetimepicker.js') }}
    {{ HTML::script('resources/assets/js/front/mp/jquery-asRange.js') }}

    {{ HTML::script('resources/assets/js/front/customangular.js') }}
    {{ HTML::script('resources/assets/js/common/custom_alert.js') }}
    {{ HTML::script('resources/assets/js/common/jquery.validate.min.js') }}
    {{ HTML::script('resources/assets/js/common/sweetalert.min.js') }}
    {{ HTML::script('resources/assets/js/front/basic.js') }}
    {{ HTML::script('resources/assets/js/common/js.cookie.min.js') }}
    {{ HTML::script('resources/assets/js/common/lang-label.js') }}
    {{ HTML::script('resources/assets/js/common/angular-translate.min.js') }}
    <!-- Controller files -->
    {{ HTML::script('resources/assets/js/front/controllers/MainController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/mp/MPHomeController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/mp/MPDesignerSignupController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/mp/MPBuyerSignUpController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/mp/MPCreateNewProjectController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/mp/MPSuggestedDesignerController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/mp/MPLoginController.js') }}
    <!-- factory -->
    {{ HTML::script('resources/assets/js/front/factory/userfactory.js') }}
    {{ HTML::script('resources/assets/js/front/factory/mpfactory.js') }}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-sanitize/1.6.9/angular-sanitize.min.js">
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".input-label input").focus(function ()
            {
                $(this).parent(".input-label").addClass('typing');

            });
            $('.input-label input').blur(function () {
                if ($('.input-label input').val() != '') {
                    $(this).parent(".input-label").addClass('typing');
                } else {
                    $(this).parent(".input-label").removeClass('typing');
                }
            });
        });
    </script>
</body>
</html>
</body>
</html>