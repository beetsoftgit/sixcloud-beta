<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sixclouds Web "Ignite"</title>
    {{ HTML::script('resources/assets/js/common/jquery.min.js') }}
    <!--<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" />
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.0/css/font-awesome.css" />-->
    {{ HTML::script('resources/assets/js/common/angular.min.js') }}
     

    <!--Slider Style-->
    {{ HTML::style('resources/assets/css/front/mp/swiper.min.css',array('rel'=>'stylesheet')) }}

    <!--Fonts Style-->
    {{ HTML::style('resources/assets/css/front/mp/custom-font.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/front/mp/font-awesome.min.css',array('rel'=>'stylesheet')) }}

    <!--DOB Style-->
    <!-- <link rel="stylesheet" type="text/css" href="assets/css/dob-ui.css"/> -->

    <!--Bootstrap Style-->
    {{ HTML::style('resources/assets/css/front/mp/bootstrap.css',array('rel'=>'stylesheet')) }}


    <!--Custom Style-->
    {{ HTML::style('resources/assets/css/front/ignite/style.css',array('rel'=>'stylesheet')) }}
    <!-- <link rel="stylesheet" type="text/css" href="assets/css/responsive.css">
    <script src="script.js"></script> -->
</head>
<body data-ng-app="sixcloudswidgetApp" ng-controller="myCtrl" ng-cloak>               
    <div class="body-div"> 
      <!--Header section Started-->
      <header class="header">
      
      
        <div class="container">
          <div class="row">
            <div class="col-sm-6">
                <div class="logo">
                    <a href="<?=url('/')?>"><img src="resources/assets/images/logo.svg"></a>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="dropdown head-right-dropdown">
                </div>
            </div>
          </div>
        </div>
      </header>
      <!--Header section End--> 
        <div id="main">   
            <div class="container">
              <section class="my-membership-plan-section">
                <div class="white-block">
                    <h2 class="page-title text-center">MY MEMBERSHIP PLAN</h2>
                    <div class="inner-div">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="membership-box">
                                    <div class="title-box">
                                        <div class="b1">
                                            <div class="b2">
                                                <div class="b3">
                                                  FREE
                                                 </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="b1">
                                        <div class="b2">
                                            <div class="b3">
                                                <div class="inner-content">
                                                    <h2>All videos and all quizzes.</h2>
                                                        
                                                </div>
                                                <h1>FREE</h1>
                                             </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="membership-box">
                                    <div class="title-box">
                                        <div class="b1">
                                            <div class="b2">
                                                <div class="b3">
                                                  STANDARD
                                                 </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="b1">
                                        <div class="b2">
                                            <div class="b3">
                                                <div class="inner-content">
                                                    <h2>All videos & quizzes, with offline viewing options..</h2>
                                                        
                                                </div>
                                                <h1>BUY 99/yr</h1>
                                             </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="membership-box">
                                    <div class="title-box">
                                        <div class="b1">
                                            <div class="b2">
                                                <div class="b3">
                                                  PREMIUM
                                                 </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="b1">
                                        <div class="b2">
                                            <div class="b3">
                                                <div class="inner-content">
                                                    <h2>All videos & quizzes, with offline viewing options, and detailed performance stats.</h2>
                                                        
                                                </div>
                                                <h1>BUY 199/yr</h1>
                                             </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="bottom-btn text-center">
                            <a target="_blank" href="https://sixclouds.net/sixteen-login" class="btn btn-medium-pink btn-sm">Payments</a>
                        </div>
                    </div>
                </div>
              </section>
              
            </div>
        </div> 
      <!--Footer Start-->
      <footer class="animatedParent animateOnce">
        
      </footer>
    </div>
    {{ HTML::script('resources/assets/js/front/mp/bootstrap.js') }} 
    {{ HTML::script('resources/assets/js/front/mp/swiper.min.js') }}  
    <script src='assets/js/dob-ui.js'></script> 
    <script>
        app = angular.module('sixcloudswidgetApp', []);   
        app.config(['$interpolateProvider', function ($interpolateProvider) {
            $interpolateProvider.startSymbol('[[');
            $interpolateProvider.endSymbol(']]');
        }]);
        app.controller('myCtrl', function($scope) {
           $scope.widgetinfo        = <?php echo json_encode($widgetinfo) ?>;
           $scope.widgetprojectinfo = <?php echo json_encode($widgetprojectinfo) ?>;
           
           
          // $scope.widgetinfo
          setTimeout(function(){
            var total_project_length = $scope.widgetprojectinfo.length;
            if($scope.widgetinfo.length > 0 ) 
            {

                var height = "19";
                if( $scope.widgetinfo[0].no_of_projects == 1 )
                {
                    if(total_project_length > 0)
                    {
                        height  = "85";
                    }
                    else{
                        height  = "85";
                    }
                }
                else if( $scope.widgetinfo[0].no_of_projects == 2)
                {
                    if(total_project_length > 1)
                    {
                        height  = "170";
                    }
                    else if(total_project_length > 0){
                        height  = "85";
                    }

                }
                else if( $scope.widgetinfo[0].no_of_projects == 3)
                {
                    if(total_project_length > 2)
                    {
                        height  = "255";
                    }
                    else if(total_project_length > 1)
                    {
                        height  = "170";
                    }
                    else if(total_project_length > 0)
                    {
                        height  = "85";
                    }                                
                }
                else if( $scope.widgetinfo[0].no_of_projects == 4)
                {
                    if(total_project_length > 3)
                    {
                        height  = "340";
                    }
                    else if(total_project_length > 2)
                    {
                        height  = "255";
                    }
                    else if(total_project_length > 1)
                    {
                        height  = "170";
                    }
                    else if(total_project_length > 0)
                    {
                        height  = "85";
                    }  
                }
                else if( $scope.widgetinfo[0].no_of_projects == 5)
                {
                    if(total_project_length > 4)
                    {
                        height  = "425";
                    }
                    else if(total_project_length > 3)
                    {
                        height  = "340";
                    }
                    else if(total_project_length > 2)
                    {
                        height  = "255";
                    }
                    else if(total_project_length > 1)
                    {
                        height  = "170";
                    }
                    else if(total_project_length > 0)
                    {
                        height  = "85";
                    } 
                }
                $('#mainwidgetwrap').css('height',height);
                $('#widgetbody').marquee({

                        // enable the plugin
                        enable : true,  //plug-in is enabled

                        // scroll direction
                        // 'vertical' or 'horizontal'
                        direction: 'vertical',

                        // children items
                        itemSelecter : 'div.rotate', 

                        // animation delay
                        delay: 3000,

                        // animation speed
                        speed: 1,

                        // animation timing
                        timing: 1,

                        // mouse hover to stop the scroller
                        mouse: true

                    });
            }
        }, 10);
      });
    </script>
</body>
</html>
