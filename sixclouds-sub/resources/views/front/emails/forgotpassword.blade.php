<html>
    <body>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,300' rel='stylesheet' type='text/css'>
        <table width="614" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:12px;color:#656565;background: #FDFDFD;border: 1px solid #D6D5D5;-webkit-border-radius: 8px;-moz-border-radius: 8px;border-radius: 8px;-webkit-box-shadow: 0px -1px 5px #DDD;-moz-box-shadow: 0px -1px 3px #DDD;box-shadow: 0px -1px 5px #DDD;width: 168px;">
            <tbody style="border-color: #D3D3D3">
                <tr>
                    <td style="border-radius: 8px 8px 0 0; position: relative; text-align:center; background: #fff; padding: 10px">
                        <a href="<?php echo '#'; ?>" target="_blank">                            
                            <img src="{{ URL::to('resources/assets/images/Corporate_Color.png') }}" alt="sixclouds" width="150" border="0" style="padding:0px;max-width: 200px">
                        </a>
                    </td>
                </tr>
                <tr>
                    <td style="padding:10px; background: #fff;">            
                        <table width="576" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#656565">
                            <tbody>
                                <tr>
                                    <td style="padding:0 10px 20px 10px;">
                                        <table width="554" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        @if($current_language==1)
                                                            <div style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size: 14px; color: #6C6C6C;">
                                                                <span style="font-size: 17px;">Dear {{ $username}}</span>, <br>We received a request to reset your SixClouds account password.
                                                                <br>
                                                                To reset, please click on the link below. The link will expire in 12 hours.
                                                            </div>
                                                        @elseif($current_language==2)
                                                            <div style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size: 14px; color: #6C6C6C;">
                                                                <span style="font-size: 17px;">您好 {{ $username}}</span>, <br>我们收到重置您的六云帐户密码的请求。
                                                                <br>
                                                                请点击以下链接更改您的密码。链接会在12小时后失效。

                                                            </div>
                                                        @else
                                                            <div style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size: 14px; color: #6C6C6C;">
                                                                <span style="font-size: 17px;">Уважаемый {{ $username}}</span>, <br>Мы получили запрос на сброс пароля вашей учетной записи SixClouds.
                                                                <br>
                                                                Для сброса, пожалуйста, нажмите на ссылку ниже. Срок действия ссылки истекает через 12 часов.
                                                            </div>
                                                        @endif
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>        
                                    </td>
                                </tr>
                            </tbody>
                        </table>


                        <table width="576" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#6c6c6c;padding:10px">
                            <tbody>
                                <tr>

                                    <td>
                                        <div style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size: 14px; color: #fff; text-align: center;">
                                            <a href="{{$link}}" class="" style="color: #fff;
                                               background-color: #009bdd;
                                               border-color: #bd383e;
                                               text-decoration: none;display: inline-block;
                                               padding: 5px 10px;
                                               margin-bottom: 0;
                                               margin-top: 3px;
                                               font-size: 14px;
                                               font-weight: 400;
                                               line-height: 1.42857143;
                                               text-align: center;
                                               white-space: nowrap;
                                               vertical-align: middle;
                                               -ms-touch-action: manipulation;
                                               touch-action: manipulation;
                                               cursor: pointer;
                                               -webkit-user-select: none;
                                               -moz-user-select: none;
                                               -ms-user-select: none;
                                               user-select: none;
                                               background-image: none;
                                               border: 1px solid transparent;
                                               border-radius: 4px;">
                                                @if($current_language==1) 
                                                    Click Here 
                                                @elseif($current_language==2) 
                                                    点击 
                                                @else 
                                                    Нажмите здесь 
                                                @endif
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                         <table width="576" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#656565">
                            <tbody>
                                <tr>
                                    <td style="padding:0 10px 20px 10px;">
                                        <table width="554" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        @if($current_language==1)
                                                            <div style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size: 14px; color: #6C6C6C;">
                                                                <span>If you are not able to click the link successfully, try the link below or copy the link to the browser address bar.</span> <br><a href="{{$link}}" target="_blank">{{$link}}</a>
                                                            </div>
                                                        @elseif($current_language==2)
                                                            <div style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size: 14px; color: #6C6C6C;">
                                                                <span>如果链接不起作用，请将链接网址复制到浏览器地址栏。</span> <br><a href="{{$link}}" target="_blank">{{$link}}</a>
                                                            </div>
                                                        @else
                                                            <div style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size: 14px; color: #6C6C6C;">
                                                                <span>Если вы не можете успешно перейти по ссылке, попробуйте перейти по ссылке ниже или скопируйте ссылку в адресную строку браузера.</span> <br><a href="{{$link}}" target="_blank">{{$link}}</a>
                                                            </div>
                                                        @endif
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>        
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table width="576" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#656565">
                            <tbody>
                                <tr>
                                    <td style="padding:0 10px 20px 10px;">
                                        <table width="554" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        @if($current_language==1)
                                                            <div style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size: 14px; color: #6C6C6C;">
                                                                <span>For any further queries, please contact Support@sixclouds.net
                                                                </span> 
                                                            </div>
                                                        @elseif($current_language==2)
                                                            <div style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size: 14px; color: #6C6C6C;">
                                                                <span>如果您有任何疑问，请联系 Support@sixclouds.net
                                                                </span> 
                                                            </div>
                                                        @else
                                                            <div style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size: 14px; color: #6C6C6C;">
                                                                <span>Для дальнейших запросов, пожалуйста, свяжитесь с support@sixclouds.net
                                                                </span> 
                                                            </div>
                                                        @endif

                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>        
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table width="576" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#6c6c6c">
                            <tbody>
                                <tr>
                                    <td style="padding:0 10px 20px 10px;">
                                        <table width="554" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size: 14px; color: #6C6C6C;">
                                                            <?php //echo $html; ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>        
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:14px;padding:10px;">
                                        @if($current_language==1)
                                            Sincerely,<br /> SixClouds Customer Support
                                        @elseif($current_language==2)
                                            六云客服
                                        @else
                                            С уважением, Служба поддержки SixClouds 
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>            
                    </td>
                </tr>
                <tr>
                    <td style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size:11px; line-height:16px; padding:15px 18px; text-align:center; background-color: #fff; border-top: 3px solid #6c6c6c; color: #009eda;">
                        <?php echo date('Y'); ?>&copy; SixClouds
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>