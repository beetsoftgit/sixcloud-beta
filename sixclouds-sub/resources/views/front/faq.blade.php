<!DOCTYPE html>
<html  ng-app="sixcloudfaqApp">
<head>
  <title>{{$title}}</title>
  <meta name="description" content="{{$meta}}">
  <meta name="robots" content="index,follow" />
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1" name="viewport"/>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="alternate" href="{{$alternate}}" hreflang="{{$alternatelang}}" />
  <link rel="canonical" href="{{$canonical}}" />
  <meta name="google-site-verification" content="F0XMjfssK8hcmTCfqJJcQFq80vAe8lHHegJBFA84XAQ" />
  <meta name="msvalidate.01" content="5789F571110EF5249F1B42276844821B" />
  <meta name="p:domain_verify" content="8475f8bbc093c663df8ead38f223dfac"/>

  <!-- Google Analytics -->
  <script>
  window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
  ga('create', 'UA-149665763-1');
  ga('send', 'pageview');
  </script>
  <script async src='https://www.google-analytics.com/analytics.js'></script>
  <!-- End Google Analytics -->

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127371237-1"></script>
  <script>
   window.dataLayer = window.dataLayer || [];
   function gtag(){dataLayer.push(arguments);}
   gtag('js', new Date());

   gtag('config', 'UA-127371237-1');
  </script>
  
  {{ HTML::script('resources/assets/js/common/jquery.min.js') }}
  <link rel="shortcut icon" type="image/x-icon" href="resources/assets/images/favicon.ico">
  <!--Animation Style-->
  {{ HTML::style('resources/assets/css/front/mp/animations.css',array('rel'=>'stylesheet')) }}

  <!--Slider Style-->
  {{ HTML::style('resources/assets/css/front/mp/swiper.min.css',array('rel'=>'stylesheet')) }}

  <!--Fonts Style-->
  {{ HTML::style('resources/assets/css/front/mp/custom-font.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/font-awesome.min.css',array('rel'=>'stylesheet')) }}


  <!--jquery.FlowupLabels Style-->
  {{ HTML::style('resources/assets/css/front/mp/jquery.FlowupLabels.css',array('rel'=>'stylesheet')) }}

  <!--bootstrap-datetimepicker Style-->
  {{ HTML::style('resources/assets/css/front/mp/bootstrap-datetimepicker.min.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/asRange.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/common/star-rating.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/bootstrap.css',array('rel'=>'stylesheet')) }}

  <!--Custom Style-->
  {{ HTML::style('resources/assets/css/front/mp/style.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/mp_custom.css',array('rel'=>'stylesheet')) }}
  
  {{ HTML::style('resources/assets/css/front/mp/responsive.css',array('rel'=>'stylesheet')) }}

  {{ HTML::script('resources/assets/js/common/angular.min.js') }}
  {{ HTML::script('resources/assets/js/common/angular-route.js') }}
  {{ HTML::script('resources/assets/js/common/angular-translate.min.js') }}
  {{ HTML::script('resources/assets/js/common/js.cookie.min.js') }}

    <!--[if lte IE 9]>
          <link href='assets/css/animations-ie-fix.css' rel='stylesheet'>
    <![endif]-->

  <!-- <script src="script.js"></script> -->
</head>
<body ng-controller="FaqController">
<div class="body-div buyer-and-seller-page">
  <!--Header section Started-->
  <header class="header-design3" data-spy="affix" data-offset-top="60">
    <div class="container">
      <nav class="navbar navbar-custom">
       <div class="container-fluid">
         <!-- Brand and toggle get grouped for better mobile display -->
         <div class="navbar-header">
           <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
             <span class="sr-only">Toggle navigation</span>
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
           </button>
           <a class="navbar-brand" href="<?=url('/')?>"><img style="height: auto;" src="resources/assets/images/logo.svg" width="170"   class="white-logo"><img style="height: auto;" src="resources/assets/images/logo.svg" width="170" class="blue-logo"></a>
         </div>

         <!-- Collect the nav links, forms, and other content for toggling -->
         <div class="collapse navbar-collapse hMenu" id="bs-example-navbar-collapse-1">
          
           <!-- <ul class="nav navbar-nav navbar-right">
              <li class="become-a-buyer buyer-button-length">
                <a href="javascript:;" ng-if="!eng" ng-click="changeLang('chi')" class="uppercase btn-medium-pink login-btn">[['lbl_lang_chng'|translate]]</a>
                <a href="javascript:;" ng-if="eng == true" ng-click="changeLang('en')" class="uppercase btn-medium-pink login-btn">[['lbl_lang_chng'|translate]]</a>  
              </li>
           </ul> -->

            <select ng-model="changeLangs" ng-change="changeLang(changeLangs)" class="before-login nav navbar-right header-mid-btn about-us-btn">
              <option value="chi" ng-selected="changeLangs=='chi'">中文</option>
              <option value="en" ng-selected="changeLangs=='en'">English</option>
              <option value="ru" ng-selected="changeLangs=='ru'">Русский</option>
            </select>

         </div><!-- /.navbar-collapse -->
       </div><!-- /.container-fluid -->
     </nav>

    </div>
  </header>

  <!--Header section End-->
  <div id="main">
    <section class="page-head">
      <div style="background-image:url(resources/assets/images/head-bg01.png); " class="img-div"></div>
      <div class="container">
        <h1 class="uppercase">[["lbl_sixclouds_faq" | translate]] </h1>
      </div>
    </section>
    <section class="the-company-section faq-section" id="TheCompany">
      <div class="container">
        <div class="inner-padding">
          <h3 class="bold faq-heading">[['lbl_how_do_sign'|translate]]</h3>
          <ol>
            <li>[['lbl_press_on'|translate]]<b class="bold">[['lbl_sign_up'|translate]]</b>[['lbl_button'|translate]]</li>
            <li>[['lbl_key_in_your'|translate]]<b class="bold">[['lbl_name'|translate]]</b>[['lbl_and'|translate]]<b class="bold">[['lbl_dob'|translate]]</b>[['lbl_after_reg'|translate]]</li>
            <li>[['lbl_check_box'|translate]]<b class="bold">[['lbl_terms_service'|translate]]</b>[['lbl_and_sign'|translate]]<b class="bold">[['lbl_privacy_policy'|translate]]</b>[['lbl_dot'|translate]]</li>
            <li>[['lbl_press_on'|translate]]<b class="bold">[['lbl_sign_up'|translate]]</b>[['lbl_btn_to_complete'|translate]]</li>
          </ol>

          <h3 class="bold faq-heading">[['lbl_forgot_pwd'|translate]]</h3>
          <ol>
            <li>[['lbl_launch'|translate]]</li>
            <li>[['lbl_press_on'|translate]]<b class="bold">[['lbl_forgot_password'|translate]]</b>[['lbl_buttonn'|translate]]</li>
            <li>[['lbl_key_in'|translate]]<b class="bold">[['lbl_email_add'|translate]]</b>[['lbl_that_youhave'|translate]]<b class="bold">[['lbl_submit'|translate]]</b>[['lbl_button'|translate]]</li>
            <li>[['lbl_you_will_receive_email'|translate]]</li>
            <li>[['lbl_key_in_pwd'|translate]]<b class="bold">[['lbl_new_pwd3'|translate]]</b> [['lbl_retype_it'|translate]]</li>
            <li>[['lbl_press_on'|translate]]<b class="bold">[['lbl_reset'|translate]]</b>[['lbl_btn'|translate]]</li>
            <li>[['lbl_youcan_login'|translate]]</li>
          </ol>

          <h3 class="bold faq-heading">[['lbl_howdo_change_pwd'|translate]]</h3>
          <ol>
            <li>[['lbl_login_into_your_ac'|translate]]</li>
            <li>[['lbl_press_display_pic'|translate]]</li>
            <li>[['lbl_select'|translate]]<b class="bold">[['lbl_change_password'|translate]]</b>[['lbl_dot'|translate]]</li>
            <li>[['lbl_key_in_current_pwd'|translate]]<b class="bold">[['lbl_current_pwd'|translate]]</b>[['lbl_dot'|translate]]</li>
            <li>[['lbl_key_in_current_pwd'|translate]]<b class="bold">[['lbl_new_pwd2'|translate]]</b> [['lbl_and_confirm'|translate]]<b class="bold">[['lbl_new_pwd2'|translate]]</b>[['lbl_dot'|translate]]</li>
            <li>[['lbl_press'|translate]]<b class="bold">[['lbl_save'|translate]]</b>[['lbl_dot'|translate]]</li>
          </ol>

          <h3 class="bold faq-heading">[['lbl_update_details'|translate]]</h3>
          <ol>
            <li>[['lbl_login_into_your_ac'|translate]]</li>
            <li>[['lbl_press_display_pic'|translate]]</li>
            <li>[['lbl_select'|translate]]<b class="bold">[['lbl_update_profile'|translate]]</b>[['lbl_dot'|translate]]</li>
            <li>[['lbl_update_personal'|translate]]</li>
            <li>[['lbl_press'|translate]]<b class="bold">[['lbl_save'|translate]]</b>[['lbl_dot'|translate]]</li>
          </ol>
          <p>[['lbl_disclaimer_change_name'|translate]]<b class="bold">[['lbl_name_dis'|translate]]</b>[['lbl_or'|translate]]<b class="bold">[['lbl_dob_dis'|translate]]</b></p>

          <h3 class="bold faq-heading">[['lbl_how_watch_video'|translate]]</h3>
          <ol>
            <li>[['lbl_press_thumbnail'|translate]]</li>
            <li>[['lbl_press_the_watch_video'|translate]]<b class="bold">[['lbl_play'|translate]]</b>[['lbl_button_with_chinese'|translate]]</li>
          </ol>
          <p>[['lbl_unable_to_watch'|translate]]<b class="bold">[['lbl_cant_watch_video'|translate]]</b>[['lbl_contact_our'|translate]]<b class="bold">[['lbl_cstm_support'|translate]]</b>[['lbl_dot'|translate]]</p>

          <h3 class="bold faq-heading">[['lbl_how_to_download'|translate]]</h3>
          <ol>
            <li>[['lbl_click_thumbnail_video'|translate]]</li>
            <li>[['lbl_select_the'|translate]]<b class="bold">[['lbl_dwnload_video'|translate]]</b>[['lbl_button_with_chinese'|translate]]</li>
            <li>[['lbl_video_fully_download'|translate]]<b class="bold">[['lbl_vide_downloads'|translate]]</b>[['lbl_section'|translate]]</li>
          </ol>
          <p>[['lbl_number_of_downloads'|translate]]</p>

          <h3 class="bold faq-heading">[['lbl_how_to_play_download'|translate]]</h3>
          <ol>
            <li>[['lbl_press_display_pic_play_video'|translate]]</li>
            <li>[['lbl_select'|translate]]<b class="bold">[['lbl_vide_downloads'|translate]]</b>[['lbl_dot'|translate]]</li>
            <li>[['lbl_select_video_you_watch'|translate]]</li>
          </ol>
          <p>[['lbl_download_videos_played'|translate]]</p>

          <h3 class="bold faq-heading">[['lbl_cant_watch_video_head'|translate]]</h3>
          <p>[['lbl_our_contents_only'|translate]]</p>
          <!-- <p>[['lbl_watch_video_in_seq'|translate]]<b class="bold">[['lbl_lesson_video'|translate]]</b>[['lbl_download_worksheet'|translate]]<b class="bold">[['lbl_revision_video'|translate]]</b>[['lbl_have_to_complete'|translate]]</p> -->

          <h3 class="bold faq-heading">[['lbl_how_to_download_worksheet'|translate]]</h3>
          <!-- <p ng-if="transLang=='ru'">[['lbl_after_lesson_video'|translate]]</p> -->
          <ol>
            <li>[['lbl_press_thumbnail_worksheet'|translate]]</li>
            <li>[['lbl_press_icon'|translate]]<b class="bold">[['lbl_dwnld_worksheet'|translate]]</b>[['lbl_button'|translate]]</li>
            <li>[['lbl_once_worksheet_been'|translate]]<b class="bold">[['lbl_worksheet_downloaded'|translate]]</b>[['lbl_dot'|translate]]</li>
            <li>[['lbl_dwnload_worksheet_again_then'|translate]]<b class="bold">[['lbl_icon'|translate]]</b>[['lbl_dot'|translate]]</li>
          </ol>

          <h3 class="bold faq-heading">[['lbl_where_can_I_find'|translate]]</h3>
          <p>[['lbl_search_your_device'|translate]]</p>
          <p>[['lbl_answer_to_each'|translate]]</p>

          <h3 class="bold faq-heading">[['lbl_how_to_do_quiz'|translate]]</h3>
          <!-- <p>[['lbl_only_allowed_to'|translate]]</p> -->
          <ol>
            <!-- <li>[['lbl_watch_the'|translate]]<b class="bold">[['lbl_revision_video'|translate]]</b>[['lbl_dot'|translate]]</li> -->
            <li>[['lbl_press_on'|translate]]<b class="bold">[['lbl_take_quiz'|translate]]</b>[['lbl_thumb'|translate]]</li>
            <li>[['lbl_press_start_q'|translate]]<b class="bold">[['lbl_start_quiz'|translate]]</b>[['lbl_button'|translate]]</li>
            <li>[['lbl_select_correct_ans'|translate]]</li>
            <li>[['lbl_wait_few_Seconds'|translate]]</li>
            <li>[['lbl_press_the'|translate]]<b class="bold"> > </b>[['lbl_btn_to_proced'|translate]]</li>
            <li>[['lbl_finished_all_questions'|translate]]</li>
          </ol>
          <p>[['lbl_wish_to_stop_quiz'|translate]]<b class="bold">[['lbl_end_quiz'|translate]]</b>[['lbl_button_at_top'|translate]]</p>
          <p>[['lbl_high_score'|translate]]<b class="bold">[['lbl_my_performance'|translate]]</b>[['lbl_tab'|translate]]</p>

          <h3 class="bold faq-heading">[['lbl_see_quiz_score'|translate]]</h3>
          <p>[['lbl_quiz_score_each_attempt'|translate]]<b class="bold">[['lbl_my_performance'|translate]]</b>[['lbl_tab'|translate]]</p>
          <ol>
            <li>[['lbl_press_display_pic_play_video'|translate]]</li>
            <li>[['lbl_select'|translate]]<b class="bold">[['lbl_my_performance_quiz'|translate]]</b>[['lbl_dot'|translate]]</li>
            <li>[['lbl_under'|translate]]<b class="bold">[['lbl_quiz_attempted'|translate]]</b>[['lbl_review_highest_score'|translate]]</li>
          </ol>

          <h3 class="bold faq-heading">[['lbl_how_sub_work'|translate]]</h3>
          <p>[['lbl_how_sub_work_detail'|translate]]</p>

          <h3 class="bold faq-heading">[['lbl_purchase_new_plan'|translate]]</h3>
          <ol>
            <li>[['lbl_press_display_pic_play_video'|translate]]</li>
            <li>[['lbl_select'|translate]]<b class="bold">[['lbl_my_subscription'|translate]]</b>[['lbl_dot'|translate]]</li>
            <li>[['lbl_check_the_box'|translate]]</li>
            <li>[['lbl_press_start_q'|translate]]<b class="bold">[['lbl_pay'|translate]]</b>[['lbl_button'|translate]]</li>
            <li>[['lbl_select_payment_mode'|translate]]</li>
            <li>[['lbl_key_in_credentials'|translate]]</li>
          </ol>

          <h3 class="bold faq-heading">[['lbl_what_payment_method_have'|translate]]</h3>
          <p>[['lbl_gateway_available'|translate]]</p>
        </div>
      </div>
    </section>
  </div>

  <!--Footer Start-->
  <footer>
    <div class="footer-bg-repeat "></div>
    <div class="main-footer">
      <div class="container">
        <div class="row">
          <!-- <div class="col-sm-5">
            <div class="h-div">
              <div class="footer-logo"> <a href="ignite"><img src="resources/assets/images/logo.svg" width="173" height="70"></a> </div>
              <p class="l-link"><a href="#">sixclouds.com</a></p> 
              <div class="bottom-div"> <span class="copy">&copy;</span>All rights reserved </div>
            </div>
            
          </div> -->
          <div class="col-sm-4">
            <div class="h-div">
              <div class="footer-logo"> <a href="ignite"><img ng-src="resources/assets/images/logo.svg"></a> </div>
              <p class="l-link"><a class="hyper-color" target="_blank" href="<?=url('/') ?>">www.sixclouds.[[ @if(strpos(url('/'), 'sixclouds.cn') !== false ||  strpos(url('/'), 'localhost') !== false) ]]cn[[ @else ]]net[[ @endif ]]</a></p>
              <div class="bottom-div"> <span class="copy">&copy;</span>[['lbl_all_rights_reserved'|translate]] </div>
              <p>[[ @if(strpos(url('/'), 'sixclouds.cn') !== false ||  strpos(url('/'), 'localhost') !== false) ]] ICP号为：渝ICP备18001226号 [[  @endif ]]</p>
            </div>
          </div>
          <div class="col-sm-7">
            <table cellpadding="0" cellspacing="0" width="100%">
              <tr>
                <td width="50%" valign="top"><div class="h-div" ng-class="{ru_text:changeLangs=='ru'}">
                    <h3>[['lbl_about_us'|translate]]</h3>
                    <ul>
                      <!-- <li><a target="_blank" href="<?=url('/') . '/about-us'?>">[['lbl_about_us'|translate]]</a></li> --> 
                      <li><a target="_blank" href="<?=url('/') . '/about-us/the-company'?>">[['lbl_the_company'|translate]]</a></li>
                      <li><a target="_blank" href="<?=url('/') . '/about-us/core-values'?>">[['lbl_core_values'|translate]]</a></li>
                      <li><a target="_blank" href="<?=url('/') . '/about-us/our-team'?>">[['lbl_our_team'|translate]]</a></li>
                    </ul>
                  </div>
                  </td>
                <td valign="top"><div class="h-div" ng-class="{ru_text:changeLangs=='ru'}">
                    <h3>[['lbl_quick_links'|translate]]</h3>
                    <ul>
                      <li><a target="_blank" href="<?=url('/') . '/customer-support'?>">[["lbl_customer_support" | translate]] </a></li>
                      <li><a target="_blank" href="<?=url('/') . '/ignite-terms-of-service'?>">[["lbl_terms_service" | translate]]  </a></li>
                      <li><a target="_blank" href="<?=url('/') . '/faq'?>">FAQ</a></li>
                      <li><a target="_blank" href="<?=url('/') . '/privacy-policy'?>">[["lbl_privacy_policy" | translate]]</a></li>
                      <li><a href="<?=url('/') . '/distributor'?>">[["lbl_ignite_distributor_access" | translate]]</a></li>
                    </ul>
                  </div>
                  </td>
              </tr>
            </table>
          </div>
          
        </div>
      </div>
    </div>
  </footer>
</div>

{{ HTML::script('resources/assets/js/front/mp/popper.min.js') }}
{{ HTML::script('resources/assets/js/front/mp/bootstrap.js') }}
{{ HTML::script('resources/assets/js/front/mp/swiper.min.js') }}
{{ HTML::script('resources/assets/js/front/mp/css3-animate-it.js') }}
{{ HTML::script('resources/assets/js/front/mp/jquery.FlowupLabels.js') }}
{{ HTML::script('resources/assets/js/common/star-rating.js') }}

  <!--bootstrap-datetimepicker Start-->
{{ HTML::script('resources/assets/js/front/mp/moment.js') }}
{{ HTML::script('resources/assets/js/front/mp/bootstrap-datetimepicker.js') }}
{{ HTML::script('resources/assets/js/front/mp/jquery-asRange.js') }}

{{ HTML::script('resources/assets/js/front/controllers/FaqController.js') }}

<!-- <script type="text/javascript" src="assets/js/script.js"></script> -->
<script type="text/javascript">
  $('body').css('background','#fff');

    $(window).load(function() {
     if($(window).width() > 767) {

     } else {
       $(function(){
           var navMain = $(".hMenu");
           navMain.on("click", "a", null, function () {
               navMain.collapse('hide');
           });
       });
     }
  });

    $('.hMenu a').click(function() {
      $('.hMenu li').removeClass('active');
          var datanameC = $(this).attr('data-name');
          var headerHei = $('header').height();
         $(this).parent('li').addClass('active');
          $('html, body').animate({
              scrollTop: $("#"+datanameC).offset().top-headerHei
            }, 1000);
    });
</script>

</body>
</html>
</body>
</html>
