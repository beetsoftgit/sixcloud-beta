<footer>
    <div class="container">
        <div class="">
            <div class="list-section">
                <ul class="list-unstyled">
                    <li class="ttl">WHO WE ARE</li>
                    <li><a href="">Company</a></li>
                    <li><a href="">Community</a></li>
                    <li><a href="">Careers</a></li>
                    <li><a href="">Terms of Use</a></li>
                </ul>
                <ul class="list-unstyled">
                    <li class="ttl">SUPPORT</li>
                    <li><a href="">Tutorials</a></li>
                    <li><a href="">Guides</a></li>
                    <li><a href="">Video</a></li>
                </ul>
                <ul class="list-unstyled">
                    <li class="ttl">PRODUCTS</li>
                    <li><a href="">SixClouds App</a></li>
                    <li><a href="">SixClouds Desktop</a></li>
                    <li><a href="">SixClouds Cloud</a></li>
                </ul>
                <ul class="list-unstyled">
                    <li class="ttl">CONTACT US</li>
                    <li><a href="">hello@123.com</a></li>
                    <li><a href="">880 Johns Route</a></li>
                    <li><a href="">China</a></li>
                </ul>
                <ul class="list-unstyled">
                    <li class="ttl">SUBSCRIBE</li>
                    <li class="email"><a class="a-uline-remove">Enter your email to get notified about our new Solutions</a></li>
                    <li class="mail-input">
                        <input type="" name="" placeholder="Email" class="form-control">
                        <a class="btn">Submit</a>
                    </li>
                </ul>

            </div>
        </div>
        <div class="sub-footer margin-tp20">
            <div class="pull-left social-text">
                &copy; 2017 SixClouds, All rights reserved.
            </div>
            <div class="pull-right">
                <span class="social-text">Yes, We are social</span>
                <ul class="nav navbar-nav social-media pull-right">
                    <li class="insta">
                        <a href="#">
                            <i class="fa fa-instagram"></i>
                        </a>
                    </li>
                    <li class="fb">
                        <a href="#">
                            <i class="fa fa-facebook"></i>
                        </a>
                    </li>
                    <li class="twit">
                        <a href="#">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>